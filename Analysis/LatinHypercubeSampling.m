/*
 LatinHypercubeSampling.m
 
 Perform Latin hypercube sampling of parameter space for a given model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Sept. 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "LatinHypercubeSampling.h"
#import "ReactionModel.h"
#import <Swarm/MT19937gen.h>
#import <Swarm/UniformIntegerDist.h>
#import <Swarm/UniformDoubleDist.h>

double ltqnorm(double);

@implementation LatinHypercubeSampling

- initWithModelIndividual: aModel
{
  [super init];
  
  modelIndividual = aModel;

  RNG = [MT19937gen create: globalZone setStateFromSeed: DEFAULTSEED];
  randomInt = [UniformIntegerDist create: globalZone setGenerator: RNG];
  //randomDouble = [UniformDoubleDist create: globalZone setGenerator: RNG];
  //[randomDouble setDoubleMin:0.0 setMax:1.0];
    
  return self;
}

- (void)dealloc
{
  if (permSet1) free(permSet1);
  if (permSet2) free(permSet2);
  if (varSet1) free(varSet1);
  if (varSet2) free(varSet2);
  if (samplePopulation) [samplePopulation release];

  [super dealloc];
}

- (void)setDelegate: aDelegate { delegate = aDelegate; }
- delegate { return delegate; }

- (void)randomPerm: (int)aNum into: (int *)buf
{
  int i;
  NSMutableArray *perm = [NSMutableArray new];
  for (i = 0; i < aNum; ++i) [perm addObject: [NSNumber numberWithInt: i]];
  
  for (i = 0; i < aNum; ++i) {
    int idx = [randomInt getIntegerWithMin:0 withMax: [perm count]-1];
    buf[i] = [[perm objectAtIndex: idx] intValue];
    [perm removeObjectAtIndex: idx];
  }

  //for (i = 0; i < aNum; ++i) printf("%d ", buf[i]);
  //printf("\n");
}

- (void)createSamples: (int)aNum withSD: (double)sd systemProperties: (int)sysNum
{
  int i, j;
  int numParams = [modelIndividual numOfParameters];
  numOfSamples = aNum;
  sigma = sd;
  numOfSystemProperties = sysNum;

  // generate random permutations
  permSet1 = malloc(sizeof(int) * numParams * numOfSamples);
  int (*perm1)[numParams][numOfSamples] = permSet1;
  permSet2 = malloc(sizeof(int) * numParams * numOfSamples);
  int (*perm2)[numParams][numOfSamples] = permSet2;

  for (i = 0; i < numParams; ++i) {
    [self randomPerm: numOfSamples into: &((*perm1)[i][0])];
    [self randomPerm: numOfSamples into: &((*perm2)[i][0])];
  }

  for (i = 0; i < numParams; ++i) {
    for (j = 0; j < numOfSamples; ++j) {
      printf("%d ", (*perm1)[i][j]);
    }
    printf("\n");
  }
  
  // random variations
  varSet1 = malloc(sizeof(double) * numParams * numOfSamples);
  double (*kappa1)[numParams][numOfSamples] = varSet1;
  varSet2 = malloc(sizeof(double) * numParams * numOfSamples);
  double (*kappa2)[numParams][numOfSamples] = varSet2;
  
  for (j = 0; j < numOfSamples; ++j) {
    for (i = 0; i < numParams; ++i) {
      (*kappa1)[i][j] = ((double)(*perm1)[i][j] + [RNG getDoubleSample]) / (double)numOfSamples;
      (*kappa1)[i][j] = sigma * ltqnorm((*kappa1)[i][j]);

      (*kappa2)[i][j] = ((double)(*perm2)[i][j] + [RNG getDoubleSample]) / (double)numOfSamples;
      (*kappa2)[i][j] = sigma * ltqnorm((*kappa2)[i][j]);
    }

    //for (i = 0; i < aNum; ++i) printf("%f ", (*kappa1)[i][j]);
    //printf("\n");
  }    
  
  for (i = 0; i < numParams; ++i) {
    for (j = 0; j < numOfSamples; ++j) {
      printf("%f ", (*kappa1)[i][j]);
    }
    printf("\n");
  }
  
  // system property means and variances
  propertyMeans = malloc(sizeof(double) * numOfSystemProperties * numParams);
  propertyVariances = malloc(sizeof(double) * numOfSystemProperties * numParams);
}

- (BOOL)performSampling: (int)simSet withGPUFunctions: (void *)gpuFunctions
{
  int i, j, k;
  id m;
  
  //int speciesCount = [modelIndividual numOfSpecies];
  int numParams = [modelIndividual numOfParameters];

  // simSet indicates how many simulations can be run at same time
  // sampleSet is how many samples can be performed at one time
  int sampleSet = simSet / (2*numParams+2);
  
  // Create individuals to hold simulation parameters
  // 2M+2 simulations to be performed for each sample where M is number of parameters
  // Create at least one sample's worth of individuals
  // more if we can do multiple samples at a time.
  samplePopulation = [NSMutableArray new];
  NSMutableArray *indPopulation = [NSMutableArray new];
  [samplePopulation addObject: indPopulation];
  for (j = 0; j < 2*numParams+2; ++j) {
    m = [modelIndividual newFromModelIndividual];
    [m setupSimulationWithEncode: [Grid2DArray floatEncode] dataFile: 0];
    [indPopulation addObject: m];
  }
  for (i = 1; i < sampleSet; ++i) {
    indPopulation = [NSMutableArray new];
    [samplePopulation addObject: indPopulation];
    for (j = 0; j < 2*numParams+2; ++j) {
      m = [modelIndividual newFromModelIndividual];
      [m setupSimulationWithEncode: [Grid2DArray floatEncode] dataFile: 0];
      [indPopulation addObject: m];
    }
  }
  
  // either we do multiple models at a time or just one
  int maxSims = sampleSet * (2*numParams+2);
  if (maxSims == 0) maxSims = 1;
  printf("maxSims = %d\n", maxSims);

  // work variables
  float *indParameters = (float *)malloc(sizeof(float) * numParams);
  float *origParameters = (float *)malloc(sizeof(float) * numParams);

  // get original parameters
  indPopulation = [samplePopulation objectAtIndex: 0];
  m = [indPopulation objectAtIndex: 0];
  [m assignParameters: origParameters toIndividual: NO];
  printf("orig params: ");
  for (j = 0; j < numParams; ++j) printf("%f ", origParameters[j]);
  printf("\n");
  
  // allocate GPU data for model
  void *gpuData = [modelIndividual allocGPUData: maxSims withGPUFunctions: gpuFunctions];
  [modelIndividual allocGPUKernel: gpuData];
  
  // random variations for sampling
  double (*kappa1)[numParams][numOfSamples] = varSet1;
  double (*kappa2)[numParams][numOfSamples] = varSet2;
  
  // run it
  BOOL done = NO;
  int sampleCount = 0;
  int simCount = 0;
  while (!done) {

    // setup parameters and initial conditions
    if (sampleSet == 0) {
      // we cannot do a whole sample at a time
      indPopulation = [samplePopulation objectAtIndex: 0];

      m = [indPopulation objectAtIndex: simCount];
      if (simCount == 0) {
        for (j = 0; j < numParams; ++j) indParameters[j] = (float)(*kappa1)[j][sampleCount] + origParameters[j];
        [m assignParameters: indParameters toIndividual: YES];
        [m assignData: gpuData ofNumber: 0 toGPU: YES];
        [m assignParameters: gpuData ofNumber: 0 toGPU: YES];
      } else if (simCount == 1) {
        for (j = 0; j < numParams; ++j) indParameters[j] = (float)(*kappa2)[j][sampleCount] + origParameters[j];
        [m assignParameters: indParameters toIndividual: YES];
        [m assignData: gpuData ofNumber: 0 toGPU: YES];
        [m assignParameters: gpuData ofNumber: 0 toGPU: YES];
      } else if (simCount < (numParams+2)) {
        int k = simCount - 2;
        for (j = 0; j < numParams; ++j) indParameters[j] = (float)(*kappa2)[j][sampleCount] + origParameters[j];
        indParameters[k] = (float)(*kappa1)[k][sampleCount];
        [m assignParameters: indParameters toIndividual: YES];
        [m assignData: gpuData ofNumber: 0 toGPU: YES];
        [m assignParameters: gpuData ofNumber: 0 toGPU: YES];
      } else {
        int k = simCount - numParams - 2;
        for (j = 0; j < numParams; ++j) indParameters[j] = (float)(*kappa1)[j][sampleCount] + origParameters[j];
        indParameters[k] = (float)(*kappa2)[k][sampleCount];
        [m assignParameters: indParameters toIndividual: YES];
        [m assignData: gpuData ofNumber: 0 toGPU: YES];
        [m assignParameters: gpuData ofNumber: 0 toGPU: YES];
      }

      printf("sample: %d sim: %d params: ", sampleCount, simCount);
      for (j = 0; j < numParams; ++j) printf("%f ", indParameters[j]);
      printf("\n");

    } else {
      // we can run at least one sample's worth of simulations at a time
      simCount = 0;
      for (i = 0; i < sampleSet; ++i) {
        indPopulation = [samplePopulation objectAtIndex: i];
        
        m = [indPopulation objectAtIndex: 0];
        for (j = 0; j < numParams; ++j) indParameters[j] = (float)(*kappa1)[j][sampleCount] + origParameters[j];
        [m assignParameters: indParameters toIndividual: YES];
        [m assignData: gpuData ofNumber: simCount toGPU: YES];
        [m assignParameters: gpuData ofNumber: simCount toGPU: YES];
        ++simCount;
        
        m = [indPopulation objectAtIndex: 1];
        for (j = 0; j < numParams; ++j) indParameters[j] = (float)(*kappa2)[j][sampleCount] + origParameters[j];
        [m assignParameters: indParameters toIndividual: YES];
        [m assignData: gpuData ofNumber: simCount toGPU: YES];
        [m assignParameters: gpuData ofNumber: simCount toGPU: YES];
        ++simCount;
        
        for (k = 0; k < numParams; ++k) {
          m = [indPopulation objectAtIndex: k+2];
          for (j = 0; j < numParams; ++j) indParameters[j] = (float)(*kappa2)[j][sampleCount] + origParameters[j];
          indParameters[k] = (float)(*kappa1)[k][sampleCount];
          [m assignParameters: indParameters toIndividual: YES];
          [m assignData: gpuData ofNumber: simCount toGPU: YES];
          [m assignParameters: gpuData ofNumber: simCount toGPU: YES];
          ++simCount;
        }
        
        for (k = 0; k < numParams; ++k) {
          m = [indPopulation objectAtIndex: k+numParams+2];
          for (j = 0; j < numParams; ++j) indParameters[j] = (float)(*kappa1)[j][sampleCount] + origParameters[j];
          indParameters[k] = (float)(*kappa2)[k][sampleCount];
          [m assignParameters: indParameters toIndividual: YES];
          [m assignData: gpuData ofNumber: simCount toGPU: YES];
          [m assignParameters: gpuData ofNumber: simCount toGPU: YES];
          ++simCount;
        }
        
        ++sampleCount;
        if (sampleCount == numOfSamples) --sampleCount;
      }
    }
    
    // init GPU data
    [modelIndividual initGPUKernel: gpuData];
    
    // invoke GPU
    [modelIndividual invokeGPUKernel: gpuData];
    
    // copy results from GPU back to individuals
    if (sampleSet == 0) {
      indPopulation = [samplePopulation objectAtIndex: 0];
      m = [indPopulation objectAtIndex: simCount];
      [m assignData: gpuData ofNumber: 0 toGPU: NO];
      ++simCount;
    } else {
      simCount = 0;
      for (i = 0; i < sampleSet; ++i) {
        indPopulation = [samplePopulation objectAtIndex: i];

        m = [indPopulation objectAtIndex: 0];
        [m assignData: gpuData ofNumber: simCount toGPU: NO];
        ++simCount;

        m = [indPopulation objectAtIndex: 1];
        [m assignData: gpuData ofNumber: simCount toGPU: NO];
        ++simCount;

        for (k = 0; k < numParams; ++k) {
          m = [indPopulation objectAtIndex: k+2];
          [m assignData: gpuData ofNumber: simCount toGPU: NO];
          ++simCount;
        }

        for (k = 0; k < numParams; ++k) {
          m = [indPopulation objectAtIndex: k+numParams+2];
          [m assignData: gpuData ofNumber: simCount toGPU: NO];
          ++simCount;
        }
      }
    }

    // evaluate properties for individuals
    if (sampleSet == 0) {
      // if only running one sim at a time
      // then run a whole sample's worth before evaluating
      if (simCount >= (2*numParams+2)) {
        simCount = 0;
        ++sampleCount;

        indPopulation = [samplePopulation objectAtIndex: 0];

        m = [indPopulation objectAtIndex: 0];
        for (k = 0; k < numOfSystemProperties; ++k) {
          [delegate calculate: self systemProperty: k forIndividual: m];
        }

      }
    } else {
      simCount = 0;
      for (i = 0; i < sampleSet; ++i) {
        indPopulation = [samplePopulation objectAtIndex: i];
        
        m = [indPopulation objectAtIndex: 0];
        
        for (k = 0; k < numOfSystemProperties; ++k) {
          [delegate calculate: self systemProperty: k forIndividual: m];
        }
      }
    }

    // are we done?
    if (sampleCount >= numOfSamples) done = YES;
  }
  
  // release GPU
  [modelIndividual releaseGPUKernel: gpuData];
  
  free(indParameters);
  free(origParameters);
  return YES;
}

@end
