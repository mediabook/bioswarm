/*
 ReactionStructureMCMC.h
 
 MCMC over model structure for DynamicReactionRuleModel.
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Dec. 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "MCMC.h"
#import "SMCSampler.h"
#import <BioCocoa/BCMersenneTwister.h>

@interface ReactionStructureMCMC : MCMC
{
  int networkEquilibration;
  int networkSamples;
  int maxIncoming;
  
  NSData *xmlModel;
  NSMutableArray *geneList;
  NSMutableArray *metaboliteList;
  NSMutableArray *inputList;
  NSMutableArray *speciesList;
  NSMutableDictionary *baseReactions;
  NSMutableDictionary *baseReactionParameters;
  
  NSMutableArray *proposalSpecies;
  NSMutableDictionary *proposalReactions;
  NSMutableDictionary *proposalReactionParameters;
  NSInteger proposalIndex;
  NSInteger proposalTransitions;
  NSData *prevProposalReactions;
  NSData *prevProposalReactionParameters;
  NSInteger prevProposalIndex;
  NSInteger prevProposalTransitions;

  NSMutableArray *reactionList;
  NSMutableDictionary *speciesRules;
  
  SMCSampler *modelSampler;
  NSArray *samplerParameterNames;
  NSArray *samplerParameterIndexes;
  NSArray *samplerParameterModelNumbers;
  NSArray *samplerParameterModelKeys;
  double *samplerParameterMinRange;
  double *samplerParameterMaxRange;
  void *samplerAcceptParametersValues;
  void *samplerRejectParametersValues;

  NSMutableArray *acceptedModels;
  NSMutableArray *acceptedModelSettings;

  BCMersenneTwister *rnGen;
  double bestPosterior;
  NSMutableDictionary *bestParameters;
}

- initWithModel: (NSDictionary *)aModel andManager: anObj;

- (BOOL)proposeMove;

@end
