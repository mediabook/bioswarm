/*
 MCMC.h
 
 Markov Chain Monte Carlo for Bayesian analysis.
 
 Copyright (C) 2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Sept. 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "BioSwarmController.h"
#import "DataHandler.h"

// Notifications
extern NSString *BioSwarmBestMCMCIndividualNotification;
extern NSString *BioSwarmMCMCParameterInfoNotification;


@interface MCMC : BioSwarmController {
  id baseModel;

  BOOL respondsToWillCalc;
  BOOL respondsToCalcExpectation;

  //BOOL isDebug;
  int numOfParticles;
  int equilibration;
  int samples;
  int deltaAdjust;
  double acceptanceTarget;
  double deltaTheta;
  NSArray *mcmcParameters;
  NSArray *notMCMCParameters;
  NSString *parameterSetFile;
  int parameterSetStart;
  int parameterSetEnd;
  
  double *parameterMinRange;
  double *parameterMaxRange;
  void *parameterValues;
  void *parameterNewValues;
  double *sumMag;
  double *avgMag;
}

- initWithModel: (NSDictionary *)aModel andManager: anObj;

- (BOOL)setupParameters;
- (BOOL)proposeMove;

- (BOOL)calculatePosteriorDensity: (double *)posteriorDensity experiment: (NSString *)expName;

- (void)performMCMC;

@end
