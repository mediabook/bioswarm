/*
 FluxBalance.m
 
 Copyright (C) 2011 Scott Christley
 
 Written by: Scott Christley <schristley@mac.com>
 Created: February 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 
 */

#import "FluxBalance.h"

@implementation FluxBalance

- (void)dealloc
{
  if (geneLevels) [geneLevels release];

  [super dealloc];
}

- (void)setReactionDatabase: (NSDictionary *)aDatabase { reactionDatabase = aDatabase; }
- (void)setCompoundDatabase: (NSDictionary *)aDatabase { compoundDatabase = aDatabase; }
- (void)setBiomassDatabase: (NSDictionary *)aDatabase { biomassDatabase = aDatabase; }

- (NSDictionary *)doAnalysisOfModel: (BCMetabolicModel *)aModel withMedia: (NSDictionary *)media
{
  int i, j;
  
  NSDictionary *model = [aModel metabolicModel];
  
  // Create a new linear programming problem
  optProb = [BCLinearOptimization new];
  
  // Adding variables to the problem
  variablesIndex = [NSMutableDictionary dictionary];
  metabolitesIndex = [NSMutableDictionary dictionary];
  metabolites = [NSMutableArray array];
  NSArray *modelReactions = [model objectForKey: @"REACTIONS"];
  for (i = 0; i < [modelReactions count]; ++i) {
    NSDictionary *modelRule = [modelReactions objectAtIndex: i];
    NSString *reactionID = [modelRule objectForKey: @"DATABASE"];
    
    [optProb addColumn: reactionID lowerBound:[[modelRule objectForKey: @"LOWER"] doubleValue]
         upperBound:[[modelRule objectForKey: @"UPPER"] doubleValue] objective:0];
    [variablesIndex setObject: [NSNumber numberWithInt: i] forKey: reactionID];
    
    // Keep track of reactions for each metabolite
    NSDictionary *reaction = [reactionDatabase objectForKey: reactionID];
    if (!reaction) {
      // check biomass database
      reaction = [biomassDatabase objectForKey: reactionID];
    }
    //printf("%s\n", [[reaction description] UTF8String]);
    
    NSArray *reactants = [reaction objectForKey: @"REACTANTS"];
    for (j = 0; j < [reactants count]; ++j) {
      NSDictionary *compound = [reactants objectAtIndex: j];
      NSString *compoundID = [compound objectForKey: @"DATABASE"];
      
      NSMutableDictionary *compartmentIndex;
      NSString *compartmentID = [compound objectForKey: @"COMPARTMENT"];
      if (!compartmentID) compartmentID = @"c";
      compartmentIndex = [metabolitesIndex objectForKey: compartmentID];
      if (!compartmentIndex) {
        compartmentIndex = [NSMutableDictionary dictionary];
        [metabolitesIndex setObject: compartmentIndex forKey: compartmentID];
      }
      
      NSMutableDictionary *metab;
      NSMutableArray *rules;
      NSNumber *n = [compartmentIndex objectForKey: compoundID];
      if (!n) {
        metab = [NSMutableDictionary dictionary];
        rules = [NSMutableArray array];
        [metab setObject: compoundID forKey: @"DATABASE"];
        [metab setObject: rules forKey: @"RULES"];
        [metab setObject: compartmentID forKey: @"COMPARTMENT"];
        n = [NSNumber numberWithInt: [metabolites count]];
        [compartmentIndex setObject: n forKey: compoundID];
        [metabolites addObject: metab];
      }
      metab = [metabolites objectAtIndex: [n intValue]];
      rules = [metab objectForKey: @"RULES"];
      
      NSMutableDictionary *ed = [NSMutableDictionary dictionary];
      [ed setObject: reactionID forKey: @"DATABASE"];
      [ed setObject: [compound objectForKey: @"COEFFICIENT"] forKey: @"COEFFICIENT"];
      [rules addObject: ed];
    }
    
    NSArray *products = [reaction objectForKey: @"PRODUCTS"];
    for (j = 0; j < [products count]; ++j) {
      NSDictionary *compound = [products objectAtIndex: j];
      NSString *compoundID = [compound objectForKey: @"DATABASE"];
      
      NSMutableDictionary *compartmentIndex;
      NSString *compartmentID = [compound objectForKey: @"COMPARTMENT"];
      if (!compartmentID) compartmentID = @"c";
      compartmentIndex = [metabolitesIndex objectForKey: compartmentID];
      if (!compartmentIndex) {
        compartmentIndex = [NSMutableDictionary dictionary];
        [metabolitesIndex setObject: compartmentIndex forKey: compartmentID];
      }
      
      NSMutableDictionary *metab;
      NSMutableArray *rules;
      NSNumber *n = [compartmentIndex objectForKey: compoundID];
      if (!n) {
        metab = [NSMutableDictionary dictionary];
        rules = [NSMutableArray array];
        [metab setObject: compoundID forKey: @"DATABASE"];
        [metab setObject: rules forKey: @"RULES"];
        [metab setObject: compartmentID forKey: @"COMPARTMENT"];
        n = [NSNumber numberWithInt: [metabolites count]];
        [compartmentIndex setObject: n forKey: compoundID];
        [metabolites addObject: metab];
      }
      metab = [metabolites objectAtIndex: [n intValue]];
      rules = [metab objectForKey: @"RULES"];
      
      NSMutableDictionary *ed = [NSMutableDictionary dictionary];
      [ed setObject: reactionID forKey: @"DATABASE"];
      [ed setObject: [compound objectForKey: @"COEFFICIENT"] forKey: @"COEFFICIENT"];
      [rules addObject: ed];
    }

    if ([reactionID isEqualToString: @"rxn00541"]) {
      NSMutableDictionary *compartmentIndex = [metabolitesIndex objectForKey: @"c"];
      NSMutableDictionary *metab = [NSMutableDictionary dictionary];
      NSMutableArray *rules = [NSMutableArray array];
      [metab setObject: @"gene123_enzyme" forKey: @"DATABASE"];
      [metab setObject: rules forKey: @"RULES"];
      [metab setObject: @"c" forKey: @"COMPARTMENT"];
      NSNumber *n = [NSNumber numberWithInt: [metabolites count]];
      [compartmentIndex setObject: n forKey: @"gene123_enzyme"];
      [metabolites addObject: metab];

      NSMutableDictionary *ed = [NSMutableDictionary dictionary];
      [ed setObject: reactionID forKey: @"DATABASE"];
      [ed setObject: [NSNumber numberWithDouble: -1] forKey: @"COEFFICIENT"];
      [rules addObject: ed];

      metab = [NSMutableDictionary dictionary];
      rules = [NSMutableArray array];
      [metab setObject: @"gene123_enzyme_used" forKey: @"DATABASE"];
      [metab setObject: rules forKey: @"RULES"];
      [metab setObject: @"c" forKey: @"COMPARTMENT"];
      n = [NSNumber numberWithInt: [metabolites count]];
      [compartmentIndex setObject: n forKey: @"gene123_enzyme_used"];
      [metabolites addObject: metab];
      
      ed = [NSMutableDictionary dictionary];
      [ed setObject: reactionID forKey: @"DATABASE"];
      [ed setObject: [NSNumber numberWithDouble: 1] forKey: @"COEFFICIENT"];
      [rules addObject: ed];
    }

  }
  
  //id obj = [metabolites objectAtIndex: 1];
  //printf("%s\n", [[obj description] UTF8String]);
  
#if 1
  // simple integration of gene enzyme into rxn00541
  {
  [optProb addColumn: @"gene123" lowerBound: 0 upperBound: 10 objective:0];
  [variablesIndex setObject: [NSNumber numberWithInt: [modelReactions count]] forKey: @"gene123"];

  NSMutableDictionary *compartmentIndex = [metabolitesIndex objectForKey: @"c"];

  NSNumber *n = [compartmentIndex objectForKey: @"gene123_enzyme"];
  NSMutableDictionary *metab = [metabolites objectAtIndex: [n intValue]];
  NSMutableArray *rules = [metab objectForKey: @"RULES"];
  NSMutableDictionary *ed = [NSMutableDictionary dictionary];
  [ed setObject: @"gene123" forKey: @"DATABASE"];
  [ed setObject: [NSNumber numberWithDouble: 1] forKey: @"COEFFICIENT"];
  [rules addObject: ed];

  n = [compartmentIndex objectForKey: @"gene123_enzyme_used"];
  metab = [metabolites objectAtIndex: [n intValue]];
  rules = [metab objectForKey: @"RULES"];
  ed = [NSMutableDictionary dictionary];
  [ed setObject: @"gene123" forKey: @"DATABASE"];
  [ed setObject: [NSNumber numberWithDouble: -1] forKey: @"COEFFICIENT"];
  [rules addObject: ed];
  }
#endif
  
  // add constraints to the problem
  for (i = 0; i < [metabolites count]; ++i) {
    NSDictionary *metab = [metabolites objectAtIndex: i];
    NSString *compartmentID = [metab objectForKey: @"COMPARTMENT"];
    NSMutableArray *rules = [metab objectForKey: @"RULES"];
    NSString *compoundID = [metab objectForKey: @"DATABASE"];
    
    // Creating exchange fluxes
    if ([compartmentID isEqualToString: @"e"]) {
      compoundID = [NSString stringWithFormat: @"%@_%@", [metab objectForKey: @"DATABASE"], compartmentID];

      double lowerBound = [metabolicModel lowerBound];
      double upperBound = [metabolicModel upperBound];

      if (media) {
        // use defined media
        NSDictionary *mediaID = [media objectForKey: [metab objectForKey: @"DATABASE"]];
        if (mediaID) {
          lowerBound = [[mediaID objectForKey: @"LOWER"] doubleValue];
          upperBound = [[mediaID objectForKey: @"UPPER"] doubleValue];
        }
      }
      
      [optProb addColumn: compoundID lowerBound:lowerBound upperBound:upperBound objective:0];
      [variablesIndex setObject: [NSNumber numberWithInt: [[variablesIndex allKeys] count]] forKey: compoundID];
      
      // add rule for new external variable
      NSMutableDictionary *ed = [NSMutableDictionary dictionary];
      [ed setObject: compoundID forKey: @"DATABASE"];
      [ed setObject: [NSNumber numberWithInt: 1] forKey: @"COEFFICIENT"];
      [rules addObject: ed];
      //printf("%s\n", [[rules description] UTF8String]);
      
    }
    
    //printf("%s\n", [compoundID UTF8String]);
    
    // add variables and rules for biomass objective
    if ([compoundID isEqualToString: @"cpd11416"]) {
      NSString *maxID = [NSString stringWithFormat: @"%@_c", compoundID];
      
      [optProb addColumn: maxID lowerBound:[metabolicModel lowerBound] upperBound:0.0 objective:-1.0];
      [variablesIndex setObject: [NSNumber numberWithInt: [[variablesIndex allKeys] count]] forKey: maxID];
      
      // add rule for new external variable
      NSMutableDictionary *ed = [NSMutableDictionary dictionary];
      [ed setObject: maxID forKey: @"DATABASE"];
      [ed setObject: [NSNumber numberWithInt: 1] forKey: @"COEFFICIENT"];
      [rules addObject: ed];
      //printf("%s\n", [[rules description] UTF8String]);
    }

    int numRules = [rules count];
    NSMutableArray *varIndex = [NSMutableArray array];
    NSMutableArray *varValues = [NSMutableArray array];    
    for (j = 0; j < numRules; ++j) {
      NSDictionary *aRule = [rules objectAtIndex: j];
      NSNumber *n = [variablesIndex objectForKey: [aRule objectForKey: @"DATABASE"]];
      [varIndex addObject: n];
      n = [aRule objectForKey: @"COEFFICIENT"];
      [varValues addObject: n];
    }

    [optProb addRow:[NSString stringWithFormat: @"r_%d", i] rhs:0.0 sense:'E' index:varIndex values:varValues];
  }
    
  // run the optimization
  NSArray *res = [optProb doOptimize];
  
#if 1
  for (i = 0; i < [modelReactions count]; ++i) {
    NSDictionary *modelRule = [modelReactions objectAtIndex: i];
    NSString *reactionID = [modelRule objectForKey: @"DATABASE"];
    int col = [[variablesIndex objectForKey: reactionID] intValue];
    double x = [[res objectAtIndex: col] doubleValue];
    if (x != 0.0)
      printf ("Column %s (%d):  Value = %10f\n",
              [reactionID UTF8String], col, x);
  }
#endif
  
  return nil;
}

//
// setup for multiple calls
//

- (BOOL)setupFluxBalanceForModel: (BCMetabolicModel *)aModel withMedia: (NSDictionary *)media andGeneLevels: (NSDictionary *)levels
{
  int i, j, k;
  
  metabolicModel = aModel;
  NSDictionary *model = [metabolicModel metabolicModel];
  
  if (mediaLevels) [mediaLevels release];
  mediaLevels = nil;
  if (media) mediaLevels = [[NSMutableDictionary alloc] initWithDictionary: media];
  
  if (geneLevels) [geneLevels release];
  geneLevels = nil;
  if (levels) geneLevels = [[NSMutableDictionary alloc] initWithDictionary: levels];

  // Create a new linear programming problem
  optProb = [BCLinearOptimization new];
  
  variablesIndex = [NSMutableDictionary dictionary];
  metabolitesIndex = [NSMutableDictionary dictionary];
  metabolites = [NSMutableArray array];

  // Adding variables to the problem
  NSArray *modelReactions = [model objectForKey: @"REACTIONS"];
  for (i = 0; i < [modelReactions count]; ++i) {
    NSDictionary *modelRule = [modelReactions objectAtIndex: i];
    NSString *reactionID = [modelRule objectForKey: @"DATABASE"];
    
    // Keep track of reactions for each metabolite
    NSDictionary *reaction = [reactionDatabase objectForKey: reactionID];
    if (!reaction) {
      // check biomass database
      reaction = [biomassDatabase objectForKey: reactionID];
    }
    //printf("%s\n", [[reaction description] UTF8String]);

    //
    // determine if the gene enzyme should be included
    //
    NSArray *geneSet = [modelRule objectForKey: @"PEGS"];
    int geneSetNum = 0;
    do {
      NSArray *pegs = nil;
      if ([geneSet count] != 0) pegs = [geneSet objectAtIndex: geneSetNum];

      // If using gene levels, need to create a separate reaction for each OR
      // otherwise just a single reaction
      if (geneLevels)
        reactionID = [NSString stringWithFormat:@"%@_%d", [modelRule objectForKey: @"DATABASE"], geneSetNum];
      else
        reactionID = [modelRule objectForKey: @"DATABASE"];

      // create column
      [optProb addColumn: reactionID lowerBound:[[modelRule objectForKey: @"LOWER"] doubleValue]
              upperBound:[[modelRule objectForKey: @"UPPER"] doubleValue] objective:0];
      [variablesIndex setObject: [NSNumber numberWithInt: [[variablesIndex allKeys] count]] forKey: reactionID];
      
      // go through reactants in the reaction
      NSArray *reactants = [reaction objectForKey: @"REACTANTS"];
      for (j = 0; j < [reactants count]; ++j) {
        NSDictionary *compound = [reactants objectAtIndex: j];
        NSString *compoundID = [compound objectForKey: @"DATABASE"];
        
        NSMutableDictionary *compartmentIndex;
        NSString *compartmentID = [compound objectForKey: @"COMPARTMENT"];
        if (!compartmentID) compartmentID = @"c";
        compartmentIndex = [metabolitesIndex objectForKey: compartmentID];
        if (!compartmentIndex) {
          compartmentIndex = [NSMutableDictionary dictionary];
          [metabolitesIndex setObject: compartmentIndex forKey: compartmentID];
        }
        
        // add the rule to the metabolite
        NSMutableDictionary *metab;
        NSMutableArray *rules;
        NSNumber *n = [compartmentIndex objectForKey: compoundID];
        if (!n) {
          metab = [NSMutableDictionary dictionary];
          rules = [NSMutableArray array];
          [metab setObject: compoundID forKey: @"DATABASE"];
          [metab setObject: rules forKey: @"RULES"];
          [metab setObject: compartmentID forKey: @"COMPARTMENT"];
          n = [NSNumber numberWithInt: [metabolites count]];
          [compartmentIndex setObject: n forKey: compoundID];
          [metabolites addObject: metab];
        }
        metab = [metabolites objectAtIndex: [n intValue]];
        rules = [metab objectForKey: @"RULES"];
        
        NSMutableDictionary *ed = [NSMutableDictionary dictionary];
        [ed setObject: reactionID forKey: @"DATABASE"];
        [ed setObject: [compound objectForKey: @"COEFFICIENT"] forKey: @"COEFFICIENT"];
        [rules addObject: ed];
      }
      
      // go through products in the reaction
      NSArray *products = [reaction objectForKey: @"PRODUCTS"];
      for (j = 0; j < [products count]; ++j) {
        NSDictionary *compound = [products objectAtIndex: j];
        NSString *compoundID = [compound objectForKey: @"DATABASE"];
        
        NSMutableDictionary *compartmentIndex;
        NSString *compartmentID = [compound objectForKey: @"COMPARTMENT"];
        if (!compartmentID) compartmentID = @"c";
        compartmentIndex = [metabolitesIndex objectForKey: compartmentID];
        if (!compartmentIndex) {
          compartmentIndex = [NSMutableDictionary dictionary];
          [metabolitesIndex setObject: compartmentIndex forKey: compartmentID];
        }
        
        // add the rule to the metabolite
        NSMutableDictionary *metab;
        NSMutableArray *rules;
        NSNumber *n = [compartmentIndex objectForKey: compoundID];
        if (!n) {
          metab = [NSMutableDictionary dictionary];
          rules = [NSMutableArray array];
          [metab setObject: compoundID forKey: @"DATABASE"];
          [metab setObject: rules forKey: @"RULES"];
          [metab setObject: compartmentID forKey: @"COMPARTMENT"];
          n = [NSNumber numberWithInt: [metabolites count]];
          [compartmentIndex setObject: n forKey: compoundID];
          [metabolites addObject: metab];
        }
        metab = [metabolites objectAtIndex: [n intValue]];
        rules = [metab objectForKey: @"RULES"];
        
        NSMutableDictionary *ed = [NSMutableDictionary dictionary];
        [ed setObject: reactionID forKey: @"DATABASE"];
        [ed setObject: [compound objectForKey: @"COEFFICIENT"] forKey: @"COEFFICIENT"];
        [rules addObject: ed];
      }
            
      // stop if not doing gene levels
      if (!geneLevels) break;
      
      // for each AND gene, add active and inactive forms into reaction
      // and create recycle reaction
      for (k = 0; k < [pegs count]; ++k) {
      //if ([pegs count] == 1) {
        NSString *peg = [pegs objectAtIndex: k];
        NSNumber *level = [geneLevels objectForKey:peg];
        //if (!level) continue;

        printf("Using peg: %s\n", [peg UTF8String]);
        
        // construct new metabolite for active version of peg
        NSMutableDictionary *metab;
        NSMutableArray *rules;
        NSMutableDictionary *compartmentIndex = [metabolitesIndex objectForKey: @"c"];
        NSString *activePeg = [NSString stringWithFormat: @"%@_active", peg];
        NSNumber *n = [compartmentIndex objectForKey: activePeg];
        if (!n) {
          metab = [NSMutableDictionary dictionary];
          rules = [NSMutableArray array];
          [metab setObject: activePeg forKey: @"DATABASE"];
          [metab setObject: rules forKey: @"RULES"];
          [metab setObject: @"c" forKey: @"COMPARTMENT"];
          n = [NSNumber numberWithInt: [metabolites count]];
          [compartmentIndex setObject: n forKey: activePeg];
          [metabolites addObject: metab];
        }
        printf("%s %d\n", [activePeg UTF8String], [n intValue]);
        metab = [metabolites objectAtIndex: [n intValue]];
        rules = [metab objectForKey: @"RULES"];
        
        // stoichiometry to consume active peg metabolite
        NSMutableDictionary *ed = [NSMutableDictionary dictionary];
        [ed setObject: reactionID forKey: @"DATABASE"];
        [ed setObject: [NSNumber numberWithDouble: -1] forKey: @"COEFFICIENT"];
        [rules addObject: ed];
        
        // construct new metabolite for inactive version of peg
        NSString *inactivePeg = [NSString stringWithFormat: @"%@_inactive", peg];
        n = [compartmentIndex objectForKey: inactivePeg];
        if (!n) {
          metab = [NSMutableDictionary dictionary];
          rules = [NSMutableArray array];
          [metab setObject: inactivePeg forKey: @"DATABASE"];
          [metab setObject: rules forKey: @"RULES"];
          [metab setObject: @"c" forKey: @"COMPARTMENT"];
          n = [NSNumber numberWithInt: [metabolites count]];
          [compartmentIndex setObject: n forKey: inactivePeg];
          [metabolites addObject: metab];
        }
        printf("%s %d\n", [inactivePeg UTF8String], [n intValue]);
        metab = [metabolites objectAtIndex: [n intValue]];
        rules = [metab objectForKey: @"RULES"];
        
        // stoichiometry to produce inactive peg metabolite
        ed = [NSMutableDictionary dictionary];
        [ed setObject: reactionID forKey: @"DATABASE"];
        [ed setObject: [NSNumber numberWithDouble: 1] forKey: @"COEFFICIENT"];
        [rules addObject: ed];
        
        // add variable for peg for new conversion reaction from active to inactive metabolite
        // constrained by the defined gene level
        if (![variablesIndex objectForKey: peg]) {
          if (level)
            [optProb addColumn: peg lowerBound: (0.0 - [level doubleValue]) upperBound: [level doubleValue] objective:0];
          else
            [optProb addColumn: peg lowerBound: [metabolicModel lowerBound] upperBound: [metabolicModel upperBound] objective:0];
          [variablesIndex setObject: [NSNumber numberWithInt: [[variablesIndex allKeys] count]] forKey: peg];
          
          NSNumber *n = [compartmentIndex objectForKey: activePeg];
          NSMutableDictionary *metab = [metabolites objectAtIndex: [n intValue]];
          NSMutableArray *rules = [metab objectForKey: @"RULES"];
          NSMutableDictionary *ed = [NSMutableDictionary dictionary];
          [ed setObject: peg forKey: @"DATABASE"];
          [ed setObject: [NSNumber numberWithDouble: 1] forKey: @"COEFFICIENT"];
          [rules addObject: ed];
          
          n = [compartmentIndex objectForKey: inactivePeg];
          metab = [metabolites objectAtIndex: [n intValue]];
          rules = [metab objectForKey: @"RULES"];
          ed = [NSMutableDictionary dictionary];
          [ed setObject: peg forKey: @"DATABASE"];
          [ed setObject: [NSNumber numberWithDouble: -1] forKey: @"COEFFICIENT"];
          [rules addObject: ed];
        }          
      }
      ++geneSetNum;
      //geneSetNum = [geneSet count];
    } while (geneSetNum < [geneSet count]);
  }
  
  
  // add constraints to the problem
  NSString *biomassReaction = [metabolicModel biomassReaction];
  for (i = 0; i < [metabolites count]; ++i) {
    NSDictionary *metab = [metabolites objectAtIndex: i];
    NSString *compartmentID = [metab objectForKey: @"COMPARTMENT"];
    NSMutableArray *rules = [metab objectForKey: @"RULES"];
    NSString *compoundID = [metab objectForKey: @"DATABASE"];
    
    // Creating exchange fluxes
    if ([compartmentID isEqualToString: @"e"]) {
      compoundID = [NSString stringWithFormat: @"%@_%@", [metab objectForKey: @"DATABASE"], compartmentID];
      
      double lowerBound = [metabolicModel lowerBound];
      double upperBound = [metabolicModel upperBound];
      
      if (media) {
        // use defined media
        NSDictionary *mediaID = [media objectForKey: [metab objectForKey: @"DATABASE"]];
        if (mediaID) {
          lowerBound = [[mediaID objectForKey: @"LOWER"] doubleValue];
          upperBound = [[mediaID objectForKey: @"UPPER"] doubleValue];
        }
      }
      
      [optProb addColumn: compoundID lowerBound:lowerBound upperBound:upperBound objective:0];
      [variablesIndex setObject: [NSNumber numberWithInt: [[variablesIndex allKeys] count]] forKey: compoundID];
      
      // add rule for new external variable
      NSMutableDictionary *ed = [NSMutableDictionary dictionary];
      [ed setObject: compoundID forKey: @"DATABASE"];
      [ed setObject: [NSNumber numberWithInt: 1] forKey: @"COEFFICIENT"];
      [rules addObject: ed];
      //printf("%s\n", [[rules description] UTF8String]);
      
    }
    
    //printf("%s\n", [compoundID UTF8String]);
    
    // add variables and rules for biomass objective
    //if ([compoundID isEqualToString: @"cpd11416"]) {
    if ([compoundID isEqualToString: biomassReaction]) {
      NSString *maxID = [NSString stringWithFormat: @"%@_c", compoundID];
      
      [optProb addColumn: maxID lowerBound:[metabolicModel lowerBound] upperBound:0.0 objective:-1.0];
      [variablesIndex setObject: [NSNumber numberWithInt: [[variablesIndex allKeys] count]] forKey: maxID];
      
      // add rule for new external variable
      NSMutableDictionary *ed = [NSMutableDictionary dictionary];
      [ed setObject: maxID forKey: @"DATABASE"];
      [ed setObject: [NSNumber numberWithInt: 1] forKey: @"COEFFICIENT"];
      [rules addObject: ed];
      //printf("%s\n", [[rules description] UTF8String]);
    }
    
    printf("Adding (%s) row: ", [compoundID UTF8String]);
    int numRules = [rules count];
    NSMutableArray *varIndex = [NSMutableArray array];
    NSMutableArray *varValues = [NSMutableArray array];    
    for (j = 0; j < numRules; ++j) {
      NSDictionary *aRule = [rules objectAtIndex: j];
      NSNumber *n = [variablesIndex objectForKey: [aRule objectForKey: @"DATABASE"]];
      [varIndex addObject: n];
      n = [aRule objectForKey: @"COEFFICIENT"];
      [varValues addObject: n];
      printf("%s(%f) ", [[aRule objectForKey: @"DATABASE"] UTF8String], [n doubleValue]);
    }
    printf("\n");
    
    [optProb addRow:[NSString stringWithFormat: @"r_%d_%@", i, compoundID] rhs:0.0 sense:'E' index:varIndex values:varValues];
  }
  
  return YES;
}

- (void)setLevel: (double)aLevel forGene: (NSString *)aGene
{
}

- (void)setGeneLevels: (NSDictionary *)levels
{
}

- (void)setLevel: (double)aLevel forMediaCompound: (NSString *)aCompound
{
}

- (void)setMedia: (NSDictionary *)media
{
}

- (NSDictionary *)doFluxBalance
{
  int i;
  NSDictionary *model = [metabolicModel metabolicModel];  
  NSArray *modelReactions = [model objectForKey: @"REACTIONS"];

  // run the optimization
  NSArray *res = [optProb doOptimize];
  
#if 1
  NSMutableDictionary *resultFluxes = [NSMutableDictionary dictionary];
  for (i = 0; i < [modelReactions count]; ++i) {
    NSDictionary *modelRule = [modelReactions objectAtIndex: i];
    NSArray *geneSet = [modelRule objectForKey: @"PEGS"];

    if (geneLevels) {
      double finalValue = 0.0;
      int geneSetNum = 0;
      do {
        NSArray *pegs = nil;
        if ([geneSet count] != 0) pegs = [geneSet objectAtIndex: geneSetNum];

        NSString *reactionID = [NSString stringWithFormat:@"%@_%d", [modelRule objectForKey: @"DATABASE"], geneSetNum];
        int col = [[variablesIndex objectForKey: reactionID] intValue];
        double x = [[res objectAtIndex: col] doubleValue];
        finalValue += x;
        [resultFluxes setObject: [NSNumber numberWithDouble:x] forKey:reactionID];      
        
        if (x != 0.0) printf ("Reaction %s (%d):  Value = %10f\n", [reactionID UTF8String], col, x);
        //printf ("Column %s (%d):  Value = %10f\n", [reactionID UTF8String], col, x);

        ++geneSetNum;
      } while (geneSetNum < [geneSet count]);
      [resultFluxes setObject: [NSNumber numberWithDouble:finalValue] forKey:[modelRule objectForKey: @"DATABASE"]];
      if (finalValue != 0.0) printf ("Reaction %s:  Value = %10f\n", [[modelRule objectForKey: @"DATABASE"] UTF8String], finalValue);

    } else {
      NSString *reactionID = [modelRule objectForKey: @"DATABASE"];
      
      int col = [[variablesIndex objectForKey: reactionID] intValue];
      double x = [[res objectAtIndex: col] doubleValue];
      
#if 1
      if (x != 0.0) printf ("Reaction %s (%d):  Value = %10f\n", [reactionID UTF8String], col, x);
#endif
      
      [resultFluxes setObject: [NSNumber numberWithDouble:x] forKey:reactionID];      
    }
  }

  if (geneLevels) {
    NSArray *keys = [geneLevels allKeys];
    for (i = 0; i < [keys count]; ++i) {
      NSString *reactionID = [keys objectAtIndex: i];
      int col = [[variablesIndex objectForKey: reactionID] intValue];
      double x = [[res objectAtIndex: col] doubleValue];
      [resultFluxes setObject: [NSNumber numberWithDouble:x] forKey:reactionID];
      if (x != 0.0) printf ("Reaction %s (%d):  Value = %10f\n", [reactionID UTF8String], col, x);
    }
  }    
#endif
  
  return resultFluxes;
}

@end
