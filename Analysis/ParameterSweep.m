/*
 ParameterSweep.m
 
 Sweep through parameter values.
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Sept. 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "ParameterSweep.h"

#import "internal.h"

@implementation ParameterSweep

- initWithModel: (NSDictionary *)aModel andManager: anObj
{
  [super initWithModel: aModel andManager: anObj];
  BOOL done = YES;
  
  modelIndividual = [model retain];

  sweep = [modelIndividual objectForKey: @"sweepParameters"];
  CHECK_PARAMF(sweep, "sweepParameters", done);
  if (!done) return nil;
  
  sweepParams = [sweep objectForKey: @"parameters"];
  CHECK_PARAMF(sweepParams, "parameters", done);
  if (!done) return nil;
  
  return self;
}

- (void)dealloc
{
  [modelIndividual release];
  
  if (paramValue) free(paramValue);
  if (paramStart) free(paramStart);
  if (paramStep) free(paramStep);
  if (paramEnd) free(paramEnd);
  
  [super dealloc];
}

- (BOOL)setupParameters
{
  int i;
  id s;
  BOOL done = YES;
  
  // collect parameters
  BOOL flag = [self constructParameterList:sweepParams exclude:nil
                                     names:&parameterNames indexes:&parameterIndexes
                              modelNumbers:&parameterModelNumbers modelKeys: &parameterModelKeys];
  int totalParameters = [parameterNames count];
  
  if ((totalParameters == 0) || (!flag)) {
    printf("ERROR: Unable to setup sweep parameters.\n"); 
    return NO;
  }
  if ([self isDebug]) printf("totalParameters = %d\n", totalParameters);

  paramValue = (double *)malloc(sizeof(double) * totalParameters);
  paramStart = (double *)malloc(sizeof(double) * totalParameters);
  paramStep = (double *)malloc(sizeof(double) * totalParameters);
  paramEnd = (double *)malloc(sizeof(double) * totalParameters);

  for (i = 0; i < totalParameters; ++i) {
    NSString *p = [parameterNames objectAtIndex: i];

    NSString *pn = [NSString stringWithFormat: @"%@_start", p];
    s = [sweep objectForKey: pn];
    CHECK_PARAMF(s, [pn UTF8String], done);
    if (!done) return NO;
    paramStart[i] = [s doubleValue];
    paramValue[i] = paramStart[i];
    
    pn = [NSString stringWithFormat: @"%@_end", p];
    s = [sweep objectForKey: pn];
    CHECK_PARAMF(s, [pn UTF8String], done);
    if (!done) return NO;
    paramEnd[i] = [s doubleValue];
    
    pn = [NSString stringWithFormat: @"%@_step", p];
    s = [sweep objectForKey: pn];
    CHECK_PARAMF(s, [pn UTF8String], done);
    if (!done) return NO;
    paramStep[i] = [s doubleValue];
  }
  
  return flag;
}

- (BOOL)performSweep
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  
  // load up the model
  [self loadModelHierarchy];

  if (![self setupParameters]) return NO;

  int totalParameters = [parameterNames count];
  int i;
  int sweepRun = startRun + 1;
  BOOL done = NO;
  while (!done) {
    NSError *error = nil;
    NSData *xmlData = 
    [NSPropertyListSerialization dataWithPropertyList: modelIndividual
                                               format: NSPropertyListXMLFormat_v1_0
                                              options: 0
                                                error: &error];
    if (error) return NO;
    
    NSMutableDictionary *simParameters =
    [NSPropertyListSerialization propertyListWithData: xmlData
                                              options: NSPropertyListMutableContainersAndLeaves
                                               format: nil
                                                error: &error];
    if (error) return NO;

    for (i = 0; i < totalParameters; ++i) {
      NSString *p = [parameterNames objectAtIndex: i];
      //NSNumber *nmod = [parameterModelNumbers objectAtIndex: pidx];
      
      NSString *key = [parameterModelKeys objectAtIndex: i];
      NSMutableDictionary *modelDict = [simParameters objectForKey: key];

      [modelDict setObject: [NSNumber numberWithDouble: paramValue[i]] forKey: p];
      if ([self isDebug]) printf("%lf ", paramValue[i]);
    }
    if ([self isDebug]) printf("\n");
    
    // create model
    NSMutableDictionary *simControl = [simParameters objectForKey: @"simulationControl"];
    [simControl setObject: [NSNumber numberWithInt: rand()] forKey: @"randomSeed"];
    [simControl setObject: [NSNumber numberWithInt: sweepRun] forKey: @"outputRun"];
    outputRun = sweepRun;
    [self newInstanceForModel: simParameters];
    
    // save parameter set
    [self saveModelSpecification: simParameters forRun: sweepRun];
    
    // next parameter values
    paramValue[0] += paramStep[0];
    for (i = 0; i < totalParameters; ++i) {
      if (paramValue[i] > paramEnd[i]) {
        if (i == (totalParameters - 1)) {
          done = YES;
          break;
        } else {
          paramValue[i+1] += paramStep[i+1];
          paramValue[i] = paramStart[i];
        }
      }
    }
    
    ++sweepRun;
  }
  
  // run simulation
  [self doSimulationRun];
  
  [pool drain];
  
  // load the base model back
  [self releaseModel];
  [self loadModel: modelIndividual];

  return YES;
}

@end
