/*
 SymbolicRegression.h
 
 Symbolic regression is genetic programming of arbitrary function trees.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>

@interface SymbolicRegression : NSObject
{
  NSArray *functions;
  NSArray *terms;
  NSMutableArray *population;
  int populationSize;
  double *fitness;
  int *sortIndex;
  id delegate;

  // GP parameters
  double crossoverProb;
  double mutationProb;
  int minInitialHeight;
  int maxHeight;
  int newIndividuals;
}

+ (NSArray *)builtinArithmeticFunctions;
//+ (NSArray *)builtinBooleanFunctions;

- initWithFunctions: (NSArray *)f andTerms: (NSArray *)t;

- (void)setDelegate: aDelegate;
- delegate;
- (void)setMinInitialHeight: (int)aHeight;
- (int)minInitialHeight;
- (void)setMaxHeight: (int)aHeight;
- (int)maxHeight;

- (void)createPopulation: (int)numIndividuals;

- (int)numberOfNodesForIndividual: (NSDictionary *)ind;

- (double)evaluateIndividual: (NSDictionary *)aNode forData: (int)anIndex;
- (void)calculateFitness;
- performEvolution: (int)maxGen;

@end

@protocol SymbolicRegressionDelegate
- (double)evaluateTerm: aTerm forData: (int)anIndex;
- (double)calculateFitness: (SymbolicRegression *)controller forIndividual: (id)anIndividual;
@end

//
// Builtin functions
//
typedef enum _BioSwarmFunctions {
  ConstantFunction = 0,
  AddFunction = 1,
  SubtractFunction = 2,
  MultiplyFunction = 3,
  DivideFunction = 4,
  SqrtFunction = 5,
  AbsFunction = 6,

  BuiltinArithmeticCount = 7, // not a function, used as count
  
  AndFunction = 7,
  OrFunction = 8,
  NotFunction = 9,
  
  BuiltinBooleanCount = 10 // not a function, used as count
} BioSwarmFunctions;

