/*
 EvolutionaryStrategy.m
 
 Optimization algorithm to evolve a population of individuals.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Sept. 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */


#import "EvolutionaryStrategy.h"
#import "BiologicalModel.h"
#import "BioSwarmModel.h"
#include <math.h>

#include "internal.h"

#define RAND_NUM ((double)rand() / (double)RAND_MAX )

static int float_compare(const void *e1, const void *e2)
{
  const float *f1 = e1;
  const float *f2 = e2;
  if (*f1 < *f2) return -1;
  if (*f1 > *f2) return 1;
  return 0;
}

@implementation EvolutionaryStrategy

- initWithModel: (NSDictionary *)aModel andManager: anObj
{
  NSString *param;
  BOOL good = YES;

  [super initWithModel: aModel andManager: anObj];

  modelIndividual = [model retain];

  // look for evolutionary strategy setup
  evolveParameters = nil;
  notEvolveParameters = nil;
  NSDictionary *es = [modelIndividual objectForKey: @"evolutionaryStrategy"];
  CHECK_PARAMF(es, "evolutionaryStrategy", good);
  if (!good) return nil;
  
  // Specify which parameters to evolve?
  evolveParameters = [es objectForKey: @"evolveParameters"];
  if ([evolveParameters count] == 0) evolveParameters = nil;
  
  // Specify which parameters not to randomize?
  notEvolveParameters = [es objectForKey: @"notEvolveParameters"];
  if ([notEvolveParameters count] == 0) notEvolveParameters = nil;
  
  param = [es objectForKey: @"maxGenerations"];
  CHECK_PARAMF(param, "maxGenerations", good);
  if (!good) return nil;
  maxGenerations = [param intValue];
  
  param = [es objectForKey: @"populationSize"];
  CHECK_PARAMF(param, "populationSize", good);
  if (!good) return nil;
  populationSize = [param intValue];
  
  saveTop = 0;
  param = [es objectForKey: @"saveTop"];
  if (param) saveTop = [param intValue];
  if (saveTop > populationSize) {
    printf("WARNING: saveTop > populationSize\n");
    saveTop = populationSize;
  }
  
  equilibration = 0;
  param = [es objectForKey: @"equilibration"];
  if (param) equilibration = [param intValue];
  
  return self;
}

- (void)dealloc
{
  [modelIndividual release];
  //[theController release];

  [parameterNames release];
  [parameterIndexes release];
  [parameterModelNumbers release];
  [parameterModelKeys release];
    
  if (parameterMinRange) free(parameterMinRange);
  if (parameterMaxRange) free(parameterMaxRange);
  if (parameterValues) free(parameterValues);
  if (parameterNextValues) free(parameterNextValues);
  if (parameterDeltas) free(parameterDeltas);
  if (parameterNextDeltas) free(parameterNextDeltas);
  if (fitness) free(fitness);
  
  [super dealloc];
}

- (BOOL)setupParameters
{
  int i, j;
  
  //NSDictionary *modelSet = [modelIndividual objectForKey: @"models"];
  //NSArray *modelKeys = [modelSet allKeys];
  //NSArray *modelInstances = [theController modelInstances];
  //NSArray *subModels = [modelInstances objectAtIndex: 0];
  
  // collect parameters
  BOOL flag = [self constructParameterList:evolveParameters exclude:notEvolveParameters
                                     names:&parameterNames indexes:&parameterIndexes
                              modelNumbers:&parameterModelNumbers modelKeys: &parameterModelKeys];
  int totalParameters = [parameterNames count];
  
  if ((totalParameters == 0) || (!flag)) {
    printf("ERROR: Unable to setup parameters to evolve.\n"); 
    return NO;
  }
  if ([self isDebug]) printf("totalParameters = %d\n", totalParameters);
  [self setupParameterRanges: nil checkNames: parameterNames];
  
  // for evolving parameters
  parameterMinRange = (double *)malloc(sizeof(double) * totalParameters);
  parameterMaxRange = (double *)malloc(sizeof(double) * totalParameters);
  parameterValues = (float *)malloc(sizeof(float) * populationSize * totalParameters);
  parameterNextValues = malloc(sizeof(float) * populationSize * totalParameters);
  maxDeltas = malloc(sizeof(float) * totalParameters);
  parameterDeltas = malloc(sizeof(float) * populationSize * totalParameters);
  parameterNextDeltas = (float *)malloc(sizeof(float) * populationSize * totalParameters);
  fitness = (float *)malloc(sizeof(float) * populationSize);
  
  // save parameter ranges and initial values
  id modelObject = [modelInstances objectAtIndex: 0];
  float (*iParameters)[populationSize][totalParameters] = parameterValues;
  for (j = 0; j < totalParameters; ++j) {
    NSString *pn = [parameterNames objectAtIndex: j];
    NSNumber *nidx = [parameterIndexes objectAtIndex: j];
    NSNumber *nmod = [parameterModelNumbers objectAtIndex: j];
    
    id aModel = [modelObject modelWithNumber: [nmod intValue]];
    double *paramRange = [aModel parameterMinRange];
    parameterMinRange[j] = paramRange[[nidx intValue]];
    paramRange = [aModel parameterMaxRange];
    parameterMaxRange[j] = paramRange[[nidx intValue]];
    if ([self isDebug]) printf("Param %s, min = %f, max = %f\n", [pn UTF8String], parameterMinRange[j], parameterMaxRange[j]);

    NSDictionary *modelDict = [aModel model];
    for (i = 0; i < populationSize; ++i) (*iParameters)[i][j] = [[modelDict objectForKey: pn] doubleValue];
    if ([self isDebug]) printf("Initial value %s = %f\n", [pn UTF8String], (*iParameters)[0][j]);
  }
  
  return flag;
}

- (BOOL)calculateFitness: (double *)fitnessValues experiment: (NSString *)expName
{
  int j, k;
  
  //printf("calculatePosteriorDensityForExperiment: %s\n", [expName UTF8String]);
  
  // need data handler to automatically calculate posterior density
  if (!dataHandler) return NO;
  
  int modelInstance;
  int numOfInstances = [self numModelInstances];
  NSDictionary *experiment = [dataSets objectForKey: expName];
  for (modelInstance = 0; modelInstance < numOfInstances; ++modelInstance) {
    NSDictionary *compareMatrices = [compareMatrixList objectAtIndex: modelInstance];
    BCDataMatrix *compareMatrix = [compareMatrices objectForKey: expName];
    BCDataMatrix *rm = [dataHandler compareData: compareMatrix forExperiment: experiment];
    if (!rm) {
      printf("ERROR: Problem occurred while comparing simulation data to experimental data.\n");
      return NO;
    }
    
    int nr = [rm numberOfRows];
    int nc = [rm numberOfColumns];
    if ([self isDebug]) printf("nr = %d, nc = %d\n", nr, nc);
    double (*RM)[nr][nc] = [rm dataMatrix];
    
    // zero out entries which are not in model
    NSArray *idxArray = [dataIndexes objectForKey: expName];
    for (k = 0; k < nc; ++k) {
      NSUInteger idx = [[idxArray objectAtIndex: k] unsignedIntegerValue];
      if (idx != NSNotFound) continue;
      for (j = 0; j < nr; ++j) (*RM)[j][k] = 0.0;
    }
    
    if ([self isDebug]) {
      printf("RM = \n");
      for (j = 0; j < nr; ++j) {
        for (k = 0; k < nc; ++k) printf("%f ", (*RM)[j][k] * (*RM)[j][k]);
        printf("\n");
      }
    }
    
    fitnessValues[modelInstance] = 0.0;
    for (j = 0; j < nr; ++j)
      for (k = 0; k < nc; ++k) fitnessValues[modelInstance] += (*RM)[j][k] * (*RM)[j][k];
    if ([self isDebug]) printf("fitness(%d) = %f\n", modelInstance, fitnessValues[modelInstance]);

    if (isnan(fitnessValues[modelInstance])) {
      printf("ERROR: Got NaN for fitness value. Someting is wrong. Aborting.\n");
      return NO;
    }
  }
  
  return YES;
}

- (BOOL)createPopulation
{
  int i, j;

  // release the old models
  [self releaseModel];

  // create population
  int totalParameters = [parameterNames count];
  float (*iParameters)[populationSize][totalParameters] = parameterValues;
  for (i = 0; i < populationSize; ++i) {
    NSError *error = nil;
    NSData *xmlData = 
    [NSPropertyListSerialization dataWithPropertyList: modelIndividual
                                               format: NSPropertyListXMLFormat_v1_0
                                              options: 0
                                                error: &error];
    if (error) return NO;

    NSMutableDictionary *simParameters =
    [NSPropertyListSerialization propertyListWithData: xmlData
                                              options: NSPropertyListMutableContainersAndLeaves
                                               format: nil
                                                error: &error];
    if (error) return NO;

    //NSDictionary *modelSet = [simParameters objectForKey: @"models"];
    //NSArray *modelKeys = [modelSet allKeys];

    // set each parameter value in appropriate submodel
    for (j = 0; j < [parameterNames count]; ++j) {
      NSString *pn = [parameterNames objectAtIndex: j];
      NSNumber *nmod = [parameterModelNumbers objectAtIndex: j];
      
      NSString *key = [parameterModelKeys objectAtIndex: [nmod intValue]];
      //NSString *val = [modelSet objectForKey: key];
      NSMutableDictionary *modelDict = [simParameters objectForKey: key];

      [modelDict setObject: [NSNumber numberWithDouble: (*iParameters)[i][j]] forKey: pn];
      if ([self isDebug]) printf("(%d): %s = %f\n", i, [pn UTF8String], (*iParameters)[i][j]);
    }
  
    // new random seed
    NSMutableDictionary *simControl = [simParameters objectForKey: @"simulationControl"];
    [simControl setObject: [NSNumber numberWithInt: rand()] forKey: @"randomSeed"];

    //printf("%s\n", [[simParameters description] UTF8String]);
    
    // create model individual
    if (i == 0) [self loadModel: simParameters];
    else [self newInstanceForModel: simParameters];
  }

  // do not write output files
  [self setFileState: BCMODEL_NOFILE_STATE];
  
  return YES;
}

- (float *)performEvolution
{
  int i, j, k;
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  
  // load up the model
  [self loadModelHierarchy];

  NSDictionary *es = [modelIndividual objectForKey: @"evolutionaryStrategy"];
  if (![self setupDataSets: es matrixCount: populationSize]) return NULL;

  if (![self setupParameters]) return NULL;
  
  for (i = 0; i < [speciesNames count]; ++i) {
    printf("species: %s (%d) model: %s (%d)\n", [[speciesNames objectAtIndex: i] UTF8String],
           [[speciesIndexes objectAtIndex: i] intValue], [[speciesModelKeys objectAtIndex: i] UTF8String],
           [[speciesModelNumbers objectAtIndex: i] intValue]);
  }

  if (!dataHandler) {
    if (!delegate) {
      printf("ERROR: no delegate.\n");
      return NULL;
    }
    if (![delegate respondsToSelector: @selector(calculateFitness:forIndividual:)]) {
      printf("ERROR: delegate does not respond to calculateFitness:forIndividual: method.\n");
      return NULL;
    }
  }
  BOOL respondsToBestFitness = NO;
  if ([delegate respondsToSelector: @selector(bestFitness:forGeneration:controller:individual:)])
    respondsToBestFitness = YES;
  
  int totalParameters = [parameterNames count];
  float (*iParameters)[populationSize][totalParameters] = parameterValues;
  float (*iNextParameters)[populationSize][totalParameters] = parameterNextValues;
  float (*iDelta)[populationSize][totalParameters] = parameterDeltas;
  float (*iNextDelta)[populationSize][totalParameters] = parameterNextDeltas;

  float lr1 = 1.0 / sqrt(2 * totalParameters);
  float lr2 = 1.0 / sqrt(2 * sqrt(totalParameters));

  // initial delta
  for (i = 0; i < populationSize; ++i)
    for (j = 0; j < totalParameters; ++j) {
      (*iDelta)[i][j] = fabs((parameterMaxRange[j] - parameterMinRange[j]) / sqrt(totalParameters));
      maxDeltas[j] = (*iDelta)[i][j];
    }
  
  // initial parameter set
  float n1 = normal_dist_rand(0, 1);
  for (i = 0; i < populationSize; ++i) {
    float n2 = normal_dist_rand(0, 1);
    
    for (j = 0; j < totalParameters; ++j) {
      (*iNextParameters)[i][j] = (*iParameters)[i][j];
      float np = (*iParameters)[i][j] + (*iDelta)[i][j] * exp(lr1 * n1 + lr2 * n2) * normal_dist_rand(0, 1);
      if ((np >= parameterMinRange[j]) && (np <= parameterMaxRange[j])) (*iNextParameters)[i][j] = np;
      (*iParameters)[i][j] = (*iNextParameters)[i][j];
      if ([self isDebug]) printf("(%d, %d): %f %f %f %f %f -> %f, %f\n", i, j, lr1, n1, lr2, n2, (*iDelta)[i][j], (*iParameters)[i][j], np);
    }    
  }
  
  // for sorting by fitness
  sort_helper *sFitness = (sort_helper *)malloc(populationSize * sizeof(sort_helper));
  double *tmpFitness = (double *)malloc(populationSize * sizeof(double));

  // create file and write header
  FILE *saveTopFile = NULL;
  if (saveTop) {
    NSString *saveTopFilename = [NSString stringWithFormat: @"%@_best_individuals.dat", [self modelName]];
    saveTopFile = fopen([saveTopFilename UTF8String], "w");

    for (i = 0; i < totalParameters; ++i) {
      if (i != 0) fprintf(saveTopFile, " ");
      fprintf(saveTopFile, "%s", [[parameterNames objectAtIndex: i] UTF8String]);
    }
    fprintf(saveTopFile, "\n");
  }

  // run it
  BOOL done = NO;
  int gen = 0;
  int currentSave = 1;
  while (!done) {
    printf("generation = %d\n", gen);
    [pool release];
    pool = [NSAutoreleasePool new];

    // create population
    if (![self createPopulation]) return NULL;

    if (experiments) {
      for (i = 0; i < populationSize; ++i) fitness[i] = 0;

      int expIndex;
      for (expIndex = 0; expIndex < [experiments count]; ++expIndex) {
        // run simulation
        NSString *anExp = [experiments objectAtIndex: expIndex];
        printf("experiment: %s(%d of %ld)\n", [anExp UTF8String], expIndex+1, (unsigned long)[experiments count]);
        [self doSimulationRunForExperiment: anExp];

        // evaluate individuals
        if (![self calculateFitness: tmpFitness experiment: anExp]) return NULL;
        for (i = 0; i < populationSize; ++i) fitness[i] += tmpFitness[i];
      }

    } else {

      // run simulation
      [self doSimulationRun];

      // evaluate individuals
      for (i = 0; i < populationSize; ++i) fitness[i] = [delegate calculateFitness: self forIndividual: i];
    }

    // sort individuals by fitness
    for (i = 0; i < populationSize; ++i) {
      sFitness[i].dValue = fitness[i];
      sFitness[i].idx = i;
    }    
    qsort(sFitness, populationSize, sizeof(sort_helper), double_cmp_sort_helper);
    if ([self isDebug]) {
      printf("sorted fitness\n");
      for (i = 0; i < populationSize; ++i) printf("%d(%f) ", sFitness[i].idx, sFitness[i].dValue);
      printf("\n");
    }
    
    // generate next generation
    int mu = populationSize / 7;
    if ([self isDebug]) {
      printf("top %d individuals\n", mu);
      for (i = 0; i < mu; ++i) printf("%d(%f) ", sFitness[i].idx, fitness[sFitness[i].idx]);
      printf("\n");
    }

    // save top individuals
    if ((saveTop) && (gen >= equilibration)) {
      for (i = 0; i < saveTop; ++i) {
        [self saveSpecificationForModel: sFitness[i].idx forRun: (currentSave + i)];
        
        for (j = 0; j < totalParameters; ++j) {
          if (j != 0) fprintf(saveTopFile, " ");
          fprintf(saveTopFile, "%f", (*iParameters)[sFitness[i].idx][j]);
        }
        fprintf(saveTopFile, "\n");
      }
      fflush(saveTopFile);
      currentSave += saveTop;
    }
    
    // record best so far
    if ((gen == 0) || (sFitness[0].dValue < bestFitness)) {
      bestFitness = sFitness[0].dValue;
      if (respondsToBestFitness) [delegate bestFitness: bestFitness forGeneration: gen controller: self individual: sFitness[0].idx];
    }

    if ([self isDebug])printf("next delta\n");
    // calculate next deltas from the top individuals
    for (i = 0; i < populationSize; ++i) {
      int h = i / 7;
      for (j = 0; j < totalParameters; ++j) {
        int kj = (int)(RAND_NUM * mu);
        (*iNextDelta)[i][j] = ((*iDelta)[h][j] + (*iDelta)[sFitness[kj].idx][j]) / 2.0;
        if ([self isDebug]) printf("(%d %d %d %d %f %f %f) ", j, h, kj, sFitness[kj].idx, (*iDelta)[h][j],
                                   (*iDelta)[sFitness[kj].idx][j], (*iNextDelta)[i][j]);
      }
    }
    if ([self isDebug]) printf("\n");
    
    //printf("delta\n");
    float n1 = normal_dist_rand(0, 1);
    for (i = 0; i < populationSize; ++i) {
      for (j = 0; j < totalParameters; ++j) {
        for (k = 0; k < 10; ++k) {
          // ten tries to get delta within bounds
          float n2 = normal_dist_rand(0, 1);
          float np = (*iNextDelta)[i][j] * exp(lr1 * n1 + lr2 * n2);
          if (np <= maxDeltas[j]) {
            (*iDelta)[i][j] = np;
            break;
          }
          //printf("(%d %d %f %f) ", i, j, (*iDelta)[i][j], exp(lr1 * n1 + lr2 * n2));
        }
      }
    }
    //printf("\n");
    
    // next population parameters from the top individuals
    for (i = 0; i < populationSize; ++i) {
      int h = i / 7;
      for (j = 0; j < totalParameters; ++j) {
        (*iNextParameters)[i][j] = (*iParameters)[sFitness[h].idx][j] + (*iDelta)[i][j] * normal_dist_rand(0, 1);
        if ([self isDebug]) printf("(%d, %d): %f %f, %f %f\n", i, j, (*iNextDelta)[i][j], (*iDelta)[i][j], (*iNextParameters)[i][j], (*iParameters)[i][j]);
      }
    }

    for (i = 0; i < populationSize; ++i) {
      for (j = 0; j < totalParameters; ++j) {
        if (((*iNextParameters)[i][j] >= parameterMinRange[j]) && ((*iNextParameters)[i][j] <= parameterMaxRange[j]))
          (*iParameters)[i][j] = (*iNextParameters)[i][j];
      }
    }
    
    ++gen;
    if (maxGenerations && (gen == maxGenerations)) done = YES;
  }

  if (saveTop) fclose(saveTopFile);

  [pool release];

  return NULL;
}

- (float)performErrorPrediction
{
  int j, k;
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  
  // load up the model
  [self loadModelHierarchy];
  
  NSDictionary *es = [modelIndividual objectForKey: @"evolutionaryStrategy"];
  if (![self setupDataSets: es matrixCount: 1]) return 0;
  
  if (![self setupParameters]) return 0;

  double singleFitness = 0;
  double tmpFitness;

  if (experiments) {
    int expIndex;
    for (expIndex = 0; expIndex < [experiments count]; ++expIndex) {
      // run simulation
      NSString *anExp = [experiments objectAtIndex: expIndex];
      printf("experiment: %s(%d of %ld)\n", [anExp UTF8String], expIndex+1, (unsigned long)[experiments count]);
      [self doSimulationRunForExperiment: anExp];
      
      // evaluate individuals
      if (![self calculateFitness: &tmpFitness experiment: anExp]) return 0;
      singleFitness += tmpFitness;

      NSDictionary *experiment = [dataSets objectForKey: anExp];
      NSDictionary *compareMatrices = [compareMatrixList objectAtIndex: 0];
      BCDataMatrix *compareMatrix = [compareMatrices objectForKey: anExp];
      BCDataMatrix *rm = [dataHandler compareData: compareMatrix forExperiment: experiment];
      int nr = [rm numberOfRows];
      int nc = [rm numberOfColumns];
      double (*RM)[nr][nc] = [rm dataMatrix];
      
      // zero out entries which are not in model
      NSArray *idxArray = [dataIndexes objectForKey: anExp];
      for (k = 0; k < nc; ++k) {
        NSUInteger idx = [[idxArray objectAtIndex: k] unsignedIntegerValue];
        if (idx != NSNotFound) continue;
        for (j = 0; j < nr; ++j) (*RM)[j][k] = 0.0;
      }

      printf("RM = \n");
      for (j = 0; j < nr; ++j) {
        for (k = 0; k < nc; ++k) printf("%f ", (*RM)[j][k] * (*RM)[j][k]);
        printf("\n");
      }
      printf("experiment (%s) error = %lf\n", [anExp UTF8String], tmpFitness);
    }
  } else {
    // run simulation
    [self doSimulationRun];
    
    // evaluate individual
    singleFitness = [delegate calculateFitness: self forIndividual: 0];
  }

  [pool release];

  printf("totalError = %lf\n", singleFitness);
  
  return singleFitness;
}

@end
