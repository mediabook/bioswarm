/*
 MCMC.m
 
 Markov Chain Monte Carlo for Bayesian analysis.
 
 Copyright (C) 2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Sept. 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "MCMC.h"

#import "BiologicalModel.h"
#import "BioSwarmModel.h"
#import "internal.h"

// Notifications
NSString *BioSwarmBestMCMCIndividualNotification = @"BioSwarmBestMCMCIndividualNotification";
NSString *BioSwarmMCMCParameterInfoNotification = @"BioSwarmMCMCParameterInfoNotification";


@implementation MCMC

- initWithModel: (NSDictionary *)aModel andManager: anObj
{
  id param;
  int i, j;
  BOOL good = YES;
  
  [super initWithModel: aModel andManager: anObj];

  // create mutable copy of model
  NSError *error = nil;
  NSData *xmlData = 
  [NSPropertyListSerialization dataWithPropertyList: aModel
                                             format: NSPropertyListXMLFormat_v1_0
                                            options: 0
                                              error: &error];
  if (error) return nil;
  
  baseModel =
  [NSPropertyListSerialization propertyListWithData: xmlData
                                            options: NSPropertyListMutableContainersAndLeaves
                                             format: nil
                                              error: &error];
  if (error) return nil;
  [baseModel retain];
  
  // look for MCMC settings
  mcmcParameters = nil;
  notMCMCParameters = nil;
  NSDictionary *mcmc = [baseModel objectForKey: @"MCMC"];
  CHECK_PARAMF(mcmc, "MCMC", good);
  if (!good) return nil;
  
  param = [mcmc objectForKey: @"debug"];
  if (param) isDebug = [param boolValue];
  //printf("DEBUG = %d\n", [self isDebug]);

  numOfParticles = 1;

  equilibration = 100;
  param = [mcmc objectForKey: @"equilibration"];
  if (param) equilibration = [param intValue];
  
  samples = 1000;
  param = [mcmc objectForKey: @"samples"];
  if (param) samples = [param intValue];
  
  deltaAdjust = 50;
  param = [mcmc objectForKey: @"deltaAdjust"];
  if (param) deltaAdjust = [param intValue];
  
  acceptanceTarget = 0.3;
  param = [mcmc objectForKey: @"acceptanceTarget"];
  if (param) acceptanceTarget = [param doubleValue];
  
  deltaTheta = 0.1;
  param = [mcmc objectForKey: @"deltaTheta"];
  if (param) deltaTheta = [param doubleValue];
  
  // Specify which parameters for MC integration
  mcmcParameters = [mcmc objectForKey: @"mcmcParameters"];
  if ([mcmcParameters count] == 0) mcmcParameters = nil;
  
  // Specify which parameters not not for MC integration
  notMCMCParameters = [mcmc objectForKey: @"notMCMCParameters"];
  if ([notMCMCParameters count] == 0) notMCMCParameters = nil;
  
  // set of parameter files
  parameterSetFile = [mcmc objectForKey: @"parameterSetFile"];
  if (parameterSetFile) {
    param = [mcmc objectForKey: @"parameterSetStart"];
    CHECK_PARAMF(mcmc, "parameterSetStart", good);
    if (!good) return nil;
    parameterSetStart = [param intValue];
    
    param = [mcmc objectForKey: @"parameterSetEnd"];
    CHECK_PARAMF(mcmc, "parameterSetEnd", good);
    if (!good) return nil;
    parameterSetEnd = [param intValue];
  }
  
  return self;
}

- (void)dealloc
{
  [parameterNames release];
  [parameterIndexes release];
  [parameterModelNumbers release];
  [parameterModelKeys release];
  
  if (parameterMinRange) free(parameterMinRange);
  if (parameterMaxRange) free(parameterMaxRange);
  if (parameterValues) free(parameterValues);
  if (parameterNewValues) free(parameterNewValues);

  [baseModel release];
  
  [super dealloc];
}

- (BOOL)setupParameters
{
  int i, j;
  
  BOOL flag = [self constructParameterList:mcmcParameters exclude:notMCMCParameters
                                     names:&parameterNames indexes:&parameterIndexes
                              modelNumbers:&parameterModelNumbers modelKeys: &parameterModelKeys];
  int totalParameters = [parameterNames count];

  if ((totalParameters == 0) || (!flag)) {
    printf("ERROR: Unable to setup parameters for MCMC.\n"); 
    return NO;
  }
  [self setupParameterRanges: nil checkNames: parameterNames];

  // for evolving parameters
  parameterMinRange = (double *)malloc(sizeof(double) * totalParameters);
  parameterMaxRange = (double *)malloc(sizeof(double) * totalParameters);
  sumMag = malloc(sizeof(double) * totalParameters);
  avgMag = malloc(sizeof(double) * totalParameters);

  // save parameter ranges and initial values
  id modelObject = [modelInstances objectAtIndex: 0];
  for (j = 0; j < totalParameters; ++j) {
    NSString *pn = [parameterNames objectAtIndex: j];
    NSNumber *nidx = [parameterIndexes objectAtIndex: j];
    NSNumber *nmod = [parameterModelNumbers objectAtIndex: j];
    NSNumber *key = [parameterModelKeys objectAtIndex: j];
    
    //id aModel = [subModels objectAtIndex: [nmod intValue]];
    id aModel = [modelObject modelWithNumber: [nmod intValue]];
    double *paramRange = [aModel parameterMinRange];
    parameterMinRange[j] = paramRange[[nidx intValue]];
    paramRange = [aModel parameterMaxRange];
    parameterMaxRange[j] = paramRange[[nidx intValue]];
    if ([self isDebug]) printf("Param %s, min = %f, max = %f\n", [pn UTF8String], parameterMinRange[j], parameterMaxRange[j]);
    
    //NSString *key = [modelKeys objectAtIndex: [nmod intValue]];
    //NSString *val = [modelSet objectForKey: key];
    NSMutableDictionary *modelDict = [baseModel objectForKey: key];
    sumMag[j] = [[modelDict objectForKey: pn] doubleValue];    
    sumMag[j] = fabs(sumMag[j]);
    avgMag[j] = sumMag[j];
    if (avgMag[j] < sqrt(EPS)) avgMag[j] = sqrt(EPS);
    if ([self isDebug]) printf("Default value %s = %f, %f\n", [pn UTF8String], sumMag[j], avgMag[j]);
  }
  
  return flag;
}

- (BOOL)calculatePosteriorDensity: (double *)posteriorDensity experiment: (NSString *)expName
{
  int j, k;
  
  //printf("calculatePosteriorDensityForExperiment: %s\n", [expName UTF8String]);
  
  // need data handler to automatically calculate posterior density
  if (!dataHandler) return NO;
    
  int modelInstance;
  int numOfInstances = [self numModelInstances];
  NSDictionary *experiment = [dataSets objectForKey: expName];
  for (modelInstance = 0; modelInstance < numOfInstances; ++modelInstance) {
    NSDictionary *compareMatrices = [compareMatrixList objectAtIndex: modelInstance];
    BCDataMatrix *compareMatrix = [compareMatrices objectForKey: expName];
    BCDataMatrix *rm = [dataHandler compareData: compareMatrix forExperiment: experiment];
    if (!rm) {
      printf("ERROR: Problem occurred while comparing simulation data to experimental data.\n");
      return NO;
    }

    if ([self isDebug]) {
      int cnr = [rm numberOfRows];
      int cnc = [rm numberOfColumns];
      printf("cnr = %d, cnc = %d\n", cnr, cnc);
      double (*CM)[cnr][cnc] = [compareMatrix dataMatrix];
      printf("CM = \n");
      for (j = 0; j < cnr; ++j) {
	for (k = 0; k < cnc; ++k) printf("%f ", (*CM)[j][k]);
	printf("\n");
      }
    }

    int nr = [rm numberOfRows];
    int nc = [rm numberOfColumns];
    if ([self isDebug]) printf("nr = %d, nc = %d\n", nr, nc);
    double (*RM)[nr][nc] = [rm dataMatrix];
    
    // zero out entries which are not in model
    NSArray *idxArray = [dataIndexes objectForKey: expName];
    for (k = 0; k < nc; ++k) {
      NSUInteger idx = [[idxArray objectAtIndex: k] unsignedIntegerValue];
      if (idx != NSNotFound) continue;
      for (j = 0; j < nr; ++j) (*RM)[j][k] = 0.0;
    }
    
    if ([self isDebug]) {
      printf("RM = \n");
      for (j = 0; j < nr; ++j) {
	for (k = 0; k < nc; ++k) printf("%f ", (*RM)[j][k]);
	printf("\n");
      }
    }
    
    double totError = 0.0;
    for (j = 0; j < nr; ++j)
      for (k = 0; k < nc; ++k) totError += (*RM)[j][k] * (*RM)[j][k];
    if ([self isDebug]) printf("totError = %f\n", totError);
    
    //double pd = pow(totError, - ((double)nr) / 2.0);
    posteriorDensity[modelInstance] = -0.5 * nr * log(totError);
    if ([self isDebug]) printf("(%s) pd = %e\n", [expName UTF8String], posteriorDensity[modelInstance]);
  }
  
  return YES;
}

- (BOOL)proposeMove
{
  int i, k;

  // release the old model
  [self releaseModel];

  int totalParameters = [parameterNames count];
  double (*iParameters)[totalParameters] = parameterValues;
  double (*iNewParameters)[totalParameters] = parameterNewValues;
  for (i = 0; i < totalParameters; ++i) {
    // set each parameter value in appropriate submodel
    for (k = 0; k < 10; ++k) {
      // ten tries to get parameter within bounds
      (*iNewParameters)[i] = (*iParameters)[i];
      double nr = normal_dist_rand(0, 1);
      double np = (*iParameters)[i] + deltaTheta * avgMag[i] * nr;
      if ([self isDebug]) printf("%d: %f %f %f %f = %f\n", k, (*iParameters)[i], deltaTheta, avgMag[i], nr, np);
      if ((np >= parameterMinRange[i]) && (np <= parameterMaxRange[i])) {
        (*iNewParameters)[i] = np;
        break;
      }
    }
    
    NSString *pn = [parameterNames objectAtIndex: i];
    NSNumber *nmod = [parameterModelNumbers objectAtIndex: i];
    NSString *key = [parameterModelKeys objectAtIndex: i];
    NSMutableDictionary *modelDict = [baseModel objectForKey: key];
    
    [modelDict setObject: [NSNumber numberWithDouble: (*iNewParameters)[i]] forKey: pn];
    if ([self isDebug]) printf("(%d): %s = %f\n", i, [pn UTF8String], (*iNewParameters)[i]);
  }

  // new random seed
  NSMutableDictionary *simControl = [baseModel objectForKey: @"simulationControl"];
  [simControl setObject: [NSNumber numberWithInt: rand()] forKey: @"randomSeed"];

  // load model
  [self loadModel: baseModel];
  [self setFileState: BCMODEL_NOFILE_STATE];
  
  return YES;
}

- (void)performMCMCWithModel: (NSDictionary *)mcmcModel setNumber: (int)setNum
{
  int i;
  NSNotificationCenter *noticeCenter = [NSNotificationCenter defaultCenter];
  
  // setup parameters
  if (![self setupParameters]) return;
  
  int totalParameters = [parameterNames count];
  parameterValues = malloc(sizeof(double) * totalParameters);
  double (*iParameters)[totalParameters] = parameterValues;
  parameterNewValues = malloc(sizeof(double) * totalParameters);
  double (*iNewParameters)[totalParameters] = parameterNewValues;
  for (i = 0; i < totalParameters; ++i) {
    (*iParameters)[i] = sumMag[i];
    (*iNewParameters)[i] = sumMag[i];
  }

  // inform delegate of parameter information
  if ([delegate respondsToSelector: @selector(parameterInfo:names:indexes:modelNumbers:modelKeys:minRanges:maxRange:acceptValues:rejectValues:)])
    [delegate parameterInfo: self names: parameterNames indexes: parameterIndexes modelNumbers: parameterModelNumbers modelKeys: parameterModelKeys
                  minRanges: parameterMinRange maxRange: parameterMaxRange acceptValues: parameterNewValues rejectValues: parameterValues];

  // post notification about parameter information
  [noticeCenter postNotificationName: BioSwarmMCMCParameterInfoNotification object: self
                            userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                                       parameterNames, @"names",
                                       parameterIndexes, @"indexes",
                                       parameterModelNumbers, @"modelNumbers",
                                       parameterModelKeys, @"modelKeys",
                                       parameterMinRange, @"minRanges",
                                       parameterMaxRange, @"maxRanges",
                                       parameterNewValues, @"acceptValues",
                                       parameterValues, @"rejectValues",
                                       nil]];

  // inform delegate about to perform MCMC
  if ([delegate respondsToSelector: @selector(willPerformMCMC:setNumber:)])
    [delegate willPerformMCMC: self setNumber: setNum];

  // initialize counters
  double thetaTry = 0;
  double thetaAccept = 0;
  int samplesTaken = 0;
  double posteriorDensity, newPosteriorDensity, calcPosteriorDensity;

  // initial simulation
  [self setFileState: BCMODEL_NOFILE_STATE];
  if (respondsToWillCalc) [delegate willCalculatePosteriorDensity: self forIndividual: 0];
  if (!experiments) {
    [self doSimulationRun];
    posteriorDensity = [delegate calculatePosteriorDensity: self forIndividual: 0];
    if (isnan(posteriorDensity)) return;
  } else {
    posteriorDensity = 0.0;
    for (i = 0; i < [experiments count]; ++i) {
      NSString *anExp = [experiments objectAtIndex: i];
      [self doSimulationRunForExperiment: anExp];
      if (![self calculatePosteriorDensity: &calcPosteriorDensity experiment: anExp]) return;
      posteriorDensity += calcPosteriorDensity;
    }
  }

  if (![delegate respondsToSelector: @selector(initExpectation:)]) {
    [delegate initExpectation: self];
  }

  // Monte Carlo iterations
  int mcIter, mcTot = equilibration + samples;
  for (mcIter = 1; mcIter <= mcTot; ++mcIter) {

    // adjust step sizes
    if (thetaTry > deltaAdjust) {
      double fracAccept = thetaAccept / thetaTry;
      if ([self isDebug]) printf("fracAccept = %f / %f = %f\n", thetaAccept, thetaTry, fracAccept);
      if (fracAccept < acceptanceTarget)
        deltaTheta = deltaTheta * 0.9;
      else if (fracAccept > acceptanceTarget)
        deltaTheta = deltaTheta * 1.1;
      thetaTry = 0;
      thetaAccept = 0;
    }
    
    // propose move
    [self proposeMove];
    ++thetaTry;
    
    // perform simulation
    double acceptanceProb;
    if (respondsToWillCalc) [delegate willCalculatePosteriorDensity: self forIndividual: 0];
    if (!experiments) {
      [self doSimulationRun];
      newPosteriorDensity = [delegate calculatePosteriorDensity: self forIndividual: 0];
      acceptanceProb = newPosteriorDensity / posteriorDensity;
    } else {
      newPosteriorDensity = 0.0;
      for (i = 0; i < [experiments count]; ++i) {
        NSString *anExp = [experiments objectAtIndex: i];
        [self doSimulationRunForExperiment: anExp];
        if (![self calculatePosteriorDensity: &calcPosteriorDensity experiment: anExp]) return;
        newPosteriorDensity += calcPosteriorDensity;
      }
      acceptanceProb = exp(newPosteriorDensity - posteriorDensity);
    }
    
    // calculate posterior density
    //if (!dataHandler) newPosteriorDensity = [delegate calculatePosteriorDensity: theController forIndividual: 0];
    //else newPosteriorDensity = [self calculatePosteriorDensityForExperiment: @"control"];
    
    // accept or reject
    //double acceptanceProb = newPosteriorDensity / posteriorDensity;
    if ([self isDebug]) printf("newPosteriorDensity / posteriorDensity = acceptanceProb = %e / %e = %f\n",
			       newPosteriorDensity, posteriorDensity, acceptanceProb);
    BOOL acceptFlag = NO;
    if (RAND_NUM <= acceptanceProb) {
      if ([self isDebug]) printf("accept\n");
      acceptFlag = YES;
    } else {
      if ([self isDebug]) printf("reject\n");
    }

    // evalulate expectation function for new state
    if (mcIter > equilibration) {
      if (respondsToCalcExpectation) [delegate calculateExpectation: self weights: nil accept: &acceptFlag];
      ++samplesTaken;
    }

    // take new values if accepted
    if (acceptFlag) {
      for (i = 0; i < totalParameters; ++i) {
        (*iParameters)[i] = (*iNewParameters)[i];
        posteriorDensity = newPosteriorDensity;
      }
      ++thetaAccept;
    }
    
    // increment the step averages
    for (i = 0; i < totalParameters; ++i) {
      sumMag[i] = sumMag[i] + fabs((*iParameters)[i]);
      avgMag[i] = sumMag[i]/(double)(mcIter+1);
      if (avgMag[i] < sqrt(EPS)) avgMag[i] = sqrt(EPS);
    }
  }
  
  if ([delegate respondsToSelector: @selector(finishExpectation:)]) {
    [delegate finishExpectation: self];
  }

  if ([delegate respondsToSelector: @selector(didPerformMCMC:setNumber:)])
    [delegate didPerformMCMC: self setNumber: setNum];
  
  //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)performMCMC
{
  int i;
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  
  // load up the model
  [self loadModelHierarchy];

  // setup data sets
  NSDictionary *mcmc = [baseModel objectForKey: @"MCMC"];
  if (![self setupDataSets: mcmc matrixCount: numOfParticles]) return;
  
  // check delegate
  //id delegate = [theController delegate];
  if (!delegate) {
    printf("WARNING: no delegate.\n");
  }
  if (!dataHandler)
    if (![delegate respondsToSelector: @selector(calculatePosteriorDensity:forIndividual:)]) {
      printf("ERROR: delegate does not respond to calculatePosteriorDensity:forIndividual: method.\n");
      return;
    }
  
  respondsToWillCalc = NO;
  if ([delegate respondsToSelector: @selector(willCalculatePosteriorDensity:forIndividual:)])
    respondsToWillCalc = YES;
  
  respondsToCalcExpectation = NO;
  if ([delegate respondsToSelector: @selector(calculateExpectation:weights:accept:)])
    respondsToCalcExpectation = YES;
  
  // setup parameters
  if (![self setupParameters]) return;
  
  if (!parameterSetFile) {
    // single run
    [self performMCMCWithModel: baseModel setNumber: 0];
    
  } else {
    
    // multiple runs
    for (i = parameterSetStart; i <= parameterSetEnd; ++i) {
      // load model file
      NSString *mcmcFilename = [NSString stringWithFormat: @"%@_%d.bioswarm", parameterSetFile, i];
      NSMutableDictionary *mcmcModel = [[NSMutableDictionary dictionaryWithContentsOfFile: mcmcFilename] retain];
      if (!mcmcModel) {
        printf("ERROR: Could not open model file: %s\n", [mcmcFilename UTF8String]);
        break;
      }
      
      [self performMCMCWithModel: mcmcModel setNumber: i];
    }
  }
  
  [pool release];
}

@end
