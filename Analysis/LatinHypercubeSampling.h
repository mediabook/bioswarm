/*
 LatinHypercubeSampling.h
 
 Perform Latin hypercube sampling of parameter space for a given model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Sept. 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Cocoa/Cocoa.h>


@interface LatinHypercubeSampling : NSObject {
  id modelIndividual;
  id delegate;
  int numOfSystemProperties;

  id RNG;
  id randomInt;
  id randomDouble;
  
  double sigma;
  int numOfSamples;
  void *permSet1;
  void *permSet2;
  void *varSet1;
  void *varSet2;
  NSMutableArray *samplePopulation;

  void *propertyMeans;
  void *propertyVariances;
}

- initWithModelIndividual: aModel;

- (void)setDelegate: aDelegate;
- delegate;

- (void)createSamples: (int)aNum withSD: (double)sd systemProperties: (int)sysNum;

//- (void)drawSamples: (int)numSamples withDistribution: (id)aDistribution;

- (BOOL)performSampling: (int)sampleSet withGPUFunctions: (void *)gpuFunctions;

@end

@protocol LatinHypercubeSamplingDelegate
- (double)calculate: (LatinHypercubeSampling *)controller systemProperty: (int)aNum forIndividual: (id)anIndividual;
@end
