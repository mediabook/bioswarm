/*
 FluxBalance.h

 Copyright (C) 2011 Scott Christley
 
 Written by: Scott Christley <schristley@mac.com>
 Created: February 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 
*/

#import <Foundation/Foundation.h>
#import <BioCocoa/BCMetabolicModel.h>
#import "BCLinearOptimization.h"

@interface FluxBalance : NSObject {
  NSDictionary *compoundDatabase;
  NSDictionary *reactionDatabase;
  NSDictionary *biomassDatabase;
  
  BCMetabolicModel *metabolicModel;
  NSMutableDictionary *geneLevels;
  NSMutableDictionary *mediaLevels;

  BCLinearOptimization *optProb;
  NSMutableDictionary *variablesIndex;
  NSMutableDictionary *metabolitesIndex;
  NSMutableArray *metabolites;
}

- (void)setReactionDatabase: (NSDictionary *)aDatabase;
- (void)setCompoundDatabase: (NSDictionary *)aDatabase;
- (void)setBiomassDatabase: (NSDictionary *)aDatabase;

// one shot deal
- (NSDictionary *)doAnalysisOfModel: (BCMetabolicModel *)aModel withMedia: (NSDictionary *)media;

// setup for multiple calls
- (BOOL)setupFluxBalanceForModel: (BCMetabolicModel *)aModel withMedia: (NSDictionary *)media andGeneLevels: (NSDictionary *)levels;

- (void)setLevel: (double)aLevel forGene: (NSString *)aGene;
- (void)setGeneLevels: (NSDictionary *)levels;

- (void)setLevel: (double)aLevel forMediaCompound: (NSString *)aCompound;
- (void)setMedia: (NSDictionary *)media;

- (NSDictionary *)doFluxBalance;

@end
