/*
 SymbolicRegression.m
 
 Symbolic regression is genetic programming of arbitrary function trees.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "SymbolicRegression.h"

#define RAND_NUM ((double)rand() / (double)RAND_MAX )

static NSMutableArray *builtin_functions = nil;
static int builtin_function_ary[BuiltinArithmeticCount] = {0, 2, 2, 2, 2, 1, 1};

// used to sort individuals by fitness
static int fitness_compare(void *obj, const void *e1, const void *e2)
{
  double *fitness = obj;
  const int *i1 = e1;
  const int *i2 = e2;
  if (isfinite(fitness[*i1]) && isfinite(fitness[*i2])) {
    if (fitness[*i1] < fitness[*i2]) return -1;
    if (fitness[*i1] > fitness[*i2]) return 1;
    return 0;
  } else {
    if (isfinite(fitness[*i1])) return -1;
    if (isfinite(fitness[*i2])) return 1;
    return 0;
  }
}

@implementation SymbolicRegression

+ (NSArray *)builtinArithmeticFunctions
{
  int i;

  if (!builtin_functions) {
    builtin_functions = [NSMutableArray new];

    for (i = 0; i < BuiltinArithmeticCount; ++i) {
      NSMutableDictionary *d = [NSMutableDictionary dictionary];
      [d setObject: [NSNumber numberWithInt: i] forKey: @"builtin"];
      [d setObject: [NSNumber numberWithInt: builtin_function_ary[i]] forKey: @"ary"];
      [builtin_functions addObject: d];
    }
  }

  return builtin_functions;
}

- initWithFunctions: (NSArray *)f andTerms: (NSArray *)t
{
  [super init];

  functions = [f retain];
  terms = [t retain];

  // default parameters
  crossoverProb = .9;
  mutationProb = 0.01;
  minInitialHeight = 6;
  maxHeight = 20;
  newIndividuals = 5;

  return self;
}

- (void)dealloc
{
  [population release];
  [functions release];
  [terms release];
  if (fitness) free(fitness);
  if (sortIndex) free(sortIndex);

  [super dealloc];
}

- (void)setDelegate: aDelegate
{
  delegate = aDelegate;
}

- delegate { return delegate; }
- (void)setMinInitialHeight: (int)aHeight { minInitialHeight = aHeight; }
- (int)minInitialHeight { return minInitialHeight; }
- (void)setMaxHeight: (int)aHeight { maxHeight = aHeight; }
- (int)maxHeight { return maxHeight; }

- (NSMutableDictionary *)createRandomTree
{
  int i;
  int numFunctions = [functions count];
  int tot = numFunctions + [terms count];

  NSMutableDictionary *newNode = nil;

  // recursively add subtrees
  int o = RAND_NUM * tot;
  if (o >= numFunctions) {
    // term
    newNode = [NSMutableDictionary dictionaryWithDictionary: [terms objectAtIndex: (o - numFunctions)]];
  } else {
      // function
      id s = [[functions objectAtIndex: o] objectForKey: @"builtin"];
      if (s && ([s intValue] == ConstantFunction)) {
	if (RAND_NUM < 0.5) {
	  newNode = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: o]];
	  [newNode setObject: [NSNumber numberWithDouble: (RAND_NUM * 2.0 - 1.0)] forKey: @"value"];
	} else {
	  newNode = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: o]];
	  [newNode setObject: [NSNumber numberWithInt: (int)(RAND_NUM * 10 + 1)] forKey: @"value"];
	}
      } else {
	newNode = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: o]];
	int ary = [[newNode objectForKey: @"ary"] intValue];
	for (i = 0; i < ary; ++i) {
	  NSString *s = [NSString stringWithFormat: @"%d", i];
	  [newNode setObject: [self createRandomTree] forKey: s];
	}
      }
  }

  return newNode;
}

- (void)createRandomTree: (NSMutableDictionary *)aNode minDepth: (int)minDepth depth: (int)aDepth isFull: (BOOL)aFlag
{
  int i;
  int numFunctions = [functions count];
  int tot = numFunctions + [terms count];

  int currDepth = aDepth;
  ++currDepth;

  // recursively add subtrees
  int ary = [[aNode objectForKey: @"ary"] intValue];

  if (currDepth < minDepth) {
    if (aFlag) {
      // all are functions
      for (i = 0; i < ary; ++i) {
	NSString *key = [NSString stringWithFormat: @"%d", i];
	BOOL found = NO;
	while (!found) {
	  int f = RAND_NUM * numFunctions;
	  // skip the constant builtin function
	  id s = [[functions objectAtIndex: f] objectForKey: @"builtin"];
	  if (s && ([s intValue] == ConstantFunction)) continue;
	  else found = YES;
	  NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: f]];
	  [aNode setObject: d forKey: key];
	  [self createRandomTree: d minDepth: minDepth depth: currDepth isFull: aFlag];
	}
      }
    } else {
      // at least one is function
      int f = RAND_NUM * ary;
      for (i = 0; i < ary; ++i) {
	NSString *key = [NSString stringWithFormat: @"%d", i];
	int o;
	if (f == i) {
	  BOOL found = NO;
	  while (!found) {
	    o = RAND_NUM * numFunctions;
	    // skip the constant builtin function
	    id s = [[functions objectAtIndex: o] objectForKey: @"builtin"];
	    if (s && ([s intValue] == ConstantFunction)) continue;
	    else found = YES;
	  }
	} else
	  o = RAND_NUM * tot;

	if (o >= numFunctions) {
	  // term
	  [aNode setObject: [NSMutableDictionary dictionaryWithDictionary: [terms objectAtIndex: (o - numFunctions)]] forKey: key];
	} else {
	  // function
	  id s = [[functions objectAtIndex: o] objectForKey: @"builtin"];
	  if (s && ([s intValue] == ConstantFunction)) {
	    if (RAND_NUM < 0.5) {
	      NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: o]];
	      [d setObject: [NSNumber numberWithDouble: (RAND_NUM * 10 + 1)] forKey: @"value"];
	      [aNode setObject: d forKey: key];
	    } else {
	      NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: o]];
	      [d setObject: [NSNumber numberWithInt: (int)(RAND_NUM * 2.0 - 1.0)] forKey: @"value"];
	      [aNode setObject: d forKey: key];
	    }
	  } else {
	    NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: o]];
	    [aNode setObject: d forKey: key];
	    [self createRandomTree: d minDepth: minDepth depth: currDepth isFull: aFlag];
	  }
	}
      }
    }
  } else {
    // anything once of minimum depth
    for (i = 0; i < ary; ++i) {
      NSString *key = [NSString stringWithFormat: @"%d", i];
      int o = RAND_NUM * tot;
      if (o >= numFunctions) {
	// term
	[aNode setObject: [NSMutableDictionary dictionaryWithDictionary: [terms objectAtIndex: (o - numFunctions)]] forKey: key];
      } else {
	// function
	id s = [[functions objectAtIndex: o] objectForKey: @"builtin"];
	if (s && ([s intValue] == ConstantFunction)) {
	  NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: o]];
	  [d setObject: [NSNumber numberWithDouble: (RAND_NUM * 2.0 - 1.0)] forKey: @"value"];
	  [aNode setObject: d forKey: key];
	} else {
	  NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: o]];
	  [aNode setObject: d forKey: key];
	  [self createRandomTree: d minDepth: minDepth depth: currDepth isFull: aFlag];
	}
      }
    }
  }
}

- createRandomIndividual
{
  NSMutableDictionary *ind = [NSMutableDictionary dictionary];

  // make root a function
  BOOL found = NO;
  while (!found) {
    int numFunctions = [functions count];
    int f = RAND_NUM * numFunctions;
    // skip the constant builtin function
    id s = [[functions objectAtIndex: f] objectForKey: @"builtin"];
    if (s && ([s intValue] == ConstantFunction)) continue;
    else found = YES;
    [ind setObject: [NSMutableDictionary dictionaryWithDictionary: [functions objectAtIndex: f]] forKey: @"root"];
  }

  // create random tree
  //[self createRandomTree: [ind objectForKey: @"root"]];
  [self createRandomTree: [ind objectForKey: @"root"] minDepth: minInitialHeight depth: 0 isFull: YES];

  return ind;
}

- (void)createPopulation: (int)numIndividuals
{
  int i;

  if (population) [population release];
  population = [NSMutableArray new];
  if (fitness) free(fitness);
  fitness = (double *)malloc(sizeof(double) * (numIndividuals + 2));
  if (sortIndex) free(sortIndex);
  sortIndex = (int *)malloc(sizeof(int) * (numIndividuals + 2));

  populationSize = numIndividuals;

  for (i = 0; i < numIndividuals; ++i) {
    id ind = [self createRandomIndividual];
    [population addObject: ind];
    //NSLog(@"%@", ind);
  }
}

- (void)print: (NSMutableString *)aString tree: (NSDictionary *)aNode
{
  id s = [aNode objectForKey: @"term"];
  if (s) {
    [aString appendFormat: @"p%d", [s intValue]];
    return;
  }
  
  s = [aNode objectForKey: @"builtin"];
  if (s) {
    switch ([s intValue]) {
    case ConstantFunction: 
      [aString appendFormat: @"%@", [aNode objectForKey: @"value"]];
      return;
    case AddFunction: {
      [aString appendFormat: @"("];
      [self print: aString tree: [aNode objectForKey: @"0"]];
      [aString appendFormat: @" + "];
      [self print: aString tree: [aNode objectForKey: @"1"]];
      [aString appendFormat: @")"];
      return;
    }
    case SubtractFunction: {
      [aString appendFormat: @"("];
      [self print: aString tree: [aNode objectForKey: @"0"]];
      [aString appendFormat: @" - "];
      [self print: aString tree: [aNode objectForKey: @"1"]];
      [aString appendFormat: @")"];
      return; 
    }
    case MultiplyFunction: {
      [aString appendFormat: @"("];
      [self print: aString tree: [aNode objectForKey: @"0"]];
      [aString appendFormat: @" * "];
      [self print: aString tree: [aNode objectForKey: @"1"]];
      [aString appendFormat: @")"];
      return; 
    }
    case DivideFunction: {
      [aString appendFormat: @"("];
      [self print: aString tree: [aNode objectForKey: @"0"]];
      [aString appendFormat: @" / "];
      [self print: aString tree: [aNode objectForKey: @"1"]];
      [aString appendFormat: @")"];
      return; 
    }
    case SqrtFunction: {
      [aString appendFormat: @"sqrt("];
      [self print: aString tree: [aNode objectForKey: @"0"]];
      [aString appendFormat: @")"];
      return; 
    }
    case AbsFunction: {
      [aString appendFormat: @"abs("];
      [self print: aString tree: [aNode objectForKey: @"0"]];
      [aString appendFormat: @")"];
      return; 
    }
    default:
      NSLog(@"Unknown builtin function: %d", [s intValue]);
    }
  }

  // we shouldn't get here
  NSLog(@"Unable to evaluate node in tree: %@", aNode);
  abort();
}

- (double)evaluateIndividual: (NSDictionary *)aNode forData: (int)anIndex
{
  id s = [aNode objectForKey: @"term"];
  if (s) {
    return [delegate evaluateTerm: aNode forData: anIndex];
  }

  s = [aNode objectForKey: @"builtin"];
  if (s) {
    switch ([s intValue]) {
    case ConstantFunction: return [[aNode objectForKey: @"value"] doubleValue];
    case AddFunction: {
      double t0 = [self evaluateIndividual: [aNode objectForKey: @"0"] forData: anIndex];
      double t1 = [self evaluateIndividual: [aNode objectForKey: @"1"] forData: anIndex];
      return t0 + t1;
    }
    case SubtractFunction: {
      double t0 = [self evaluateIndividual: [aNode objectForKey: @"0"] forData: anIndex];
      double t1 = [self evaluateIndividual: [aNode objectForKey: @"1"] forData: anIndex];
      return t0 - t1;
    }
    case MultiplyFunction: {
      double t0 = [self evaluateIndividual: [aNode objectForKey: @"0"] forData: anIndex];
      double t1 = [self evaluateIndividual: [aNode objectForKey: @"1"] forData: anIndex];
      return t0 * t1;
    }
    case DivideFunction: {
      double t0 = [self evaluateIndividual: [aNode objectForKey: @"0"] forData: anIndex];
      double t1 = [self evaluateIndividual: [aNode objectForKey: @"1"] forData: anIndex];
      return t0 / t1;
    }
    case SqrtFunction: {
      double t0 = [self evaluateIndividual: [aNode objectForKey: @"0"] forData: anIndex];
      return sqrt(t0);
    }
    case AbsFunction: {
      double t0 = [self evaluateIndividual: [aNode objectForKey: @"0"] forData: anIndex];
      return fabs(t0);
    }
    default:
      NSLog(@"Unknown builtin function: %d", [s intValue]);
    }
  }

  // we shouldn't get here
  NSLog(@"Unable to evaluate node in tree: %@", aNode);
  abort();
  return 0.0;
}

- (void)calculateFitness
{
  int i;
  for (i = 0; i < [population count]; ++i) {
    id ind = [population objectAtIndex: i];
    sortIndex[i] = i;
    fitness[i] = [delegate calculateFitness: self forIndividual: [ind objectForKey: @"root"]];
  }

#if 0
  printf("fitness\n");
  for (i = 0; i < [population count]; ++i) {
    printf("%f\n", fitness[i]);
  }
#endif

  // sort by fitness, we sort indexes
  qsort_r(sortIndex, [population count], sizeof(int), fitness, fitness_compare);

#if 0
  printf("sorted fitness\n");
  for (i = 0; i < [population count]; ++i) {
    printf("%f\n", fitness[sortIndex[i]]);
  }
#endif
}

- (NSMutableDictionary *)deepCopyTree: (NSDictionary *)aNode
{
  NSMutableDictionary *newNode = [NSMutableDictionary dictionaryWithDictionary: aNode];

  // recursively deep copy subtrees
  int i;
  int ary = [[aNode objectForKey: @"ary"] intValue];
  for (i = 0; i < ary; ++i) {
    NSString *s = [NSString stringWithFormat: @"%d", i];
    [newNode setObject: [self deepCopyTree: [aNode objectForKey: s]] forKey: s];
  }

  return newNode;
}

- (void)numberOfNodes: (NSDictionary *)aNode count: (int *)aCount
{
  int i;

  // count ourselves
  ++(*aCount);

  // recursively count subtrees
  int ary = [[aNode objectForKey: @"ary"] intValue];
  for (i = 0; i < ary; ++i) {
    NSString *s = [NSString stringWithFormat: @"%d", i];
    [self numberOfNodes: [aNode objectForKey: s] count: aCount];
  }
}

- (int)numberOfNodesForIndividual: (NSDictionary *)ind
{
  int cnt = 0;
  [self numberOfNodes: [ind objectForKey: @"root"] count: &cnt];
  return cnt;
}

- (int)height: (int)aHeight forTree: (NSDictionary *)aNode
{
  int height = aHeight + 1;
  int currentHeight = height;

  // recurse subtrees
  int i;
  int ary = [[aNode objectForKey: @"ary"] intValue];
  for (i = 0; i < ary; ++i) {
    NSString *s = [NSString stringWithFormat: @"%d", i];
    int h = [self height: currentHeight forTree: [aNode objectForKey: s]];
    if (h > height) height = h;
  }

  return height;
}

- subTree: (NSDictionary *)aNode at: (int)n count: (int *)aCount
{
  if (*aCount == n) return aNode;

  ++(*aCount);

  // recurse subtrees
  int i;
  id subTree = nil;
  int ary = [[aNode objectForKey: @"ary"] intValue];
  for (i = 0; i < ary; ++i) {
    NSString *s = [NSString stringWithFormat: @"%d", i];
    subTree = [self subTree: [aNode objectForKey: s] at: n count: aCount];
    if (subTree) break;
  }

  return subTree;
}

- (NSMutableDictionary *)crossoverTree: (NSDictionary *)aNode at: (int)n count: (int *)aCount withTree: (id)aTree
{
  if (*aCount == n) return aTree;

  ++(*aCount);
  NSMutableDictionary *newNode = [NSMutableDictionary dictionaryWithDictionary: aNode];

  // recursively deep copy subtrees
  int i;
  int ary = [[aNode objectForKey: @"ary"] intValue];
  for (i = 0; i < ary; ++i) {
    NSString *s = [NSString stringWithFormat: @"%d", i];
    [newNode setObject: [self crossoverTree: [aNode objectForKey: s] at: n count: aCount withTree: aTree] forKey: s];
  }

  return newNode;
}

- crossoverIndividual: (NSDictionary *)ind1 at: (int)n1 withIndividual: (NSDictionary *)ind2 at: (int)n2
{
  int cnt = 0;
  id subTree = [self subTree: [ind2 objectForKey: @"root"] at: n2 count: &cnt];
  subTree = [self deepCopyTree: subTree];
  //NSLog(@"at %d subtree: %@", n2, subTree);

  NSMutableDictionary *newInd = [NSMutableDictionary dictionaryWithDictionary: ind1];
  cnt = 0;
  id newRoot = [self crossoverTree: [newInd objectForKey: @"root"] at: n1 count: &cnt withTree: subTree];
  [newInd setObject: newRoot forKey: @"root"];
  return newInd;
}

- (id)mutateTree: (NSDictionary *)aNode at: (int)n count: (int *)aCount
{
  if (*aCount == n) {
    // generate random tree for mutation
    return [self createRandomTree];
  }

  ++(*aCount);
  NSMutableDictionary *newNode = [NSMutableDictionary dictionaryWithDictionary: aNode];

  // recursively deep copy subtrees
  int i;
  int ary = [[aNode objectForKey: @"ary"] intValue];
  for (i = 0; i < ary; ++i) {
    NSString *s = [NSString stringWithFormat: @"%d", i];
    [newNode setObject: [self mutateTree: [aNode objectForKey: s] at: n count: aCount] forKey: s];
  }

  return newNode;
}

- (void)mutateIndividual: (NSMutableDictionary *)ind at: (int)n
{
  int cnt = 0;
  id newRoot = [self mutateTree: [ind objectForKey: @"root"] at: n count: &cnt];
  [ind setObject: newRoot forKey: @"root"];
}

- (NSMutableArray *)produceNextGeneration
{
  int i, j, k;
  NSMutableArray *newPopulation = [NSMutableArray new];

  int genSize = populationSize - newIndividuals;

  // population generated from crossover and reproduction
  for (i = 0; i < genSize; ++i) {
    double r = RAND_NUM;
    if (r < crossoverProb) {
      // perform crossover
      // randomly pick two individuals
      //r = RAND_NUM;
      j = RAND_NUM * [population count];
      k = RAND_NUM * [population count];
#if 0
      for (j = 0; j < [population count]; ++j) {
	if (r < fitness[j]) break;
	else r -= fitness[j];
      }
      r = RAND_NUM;
      for (k = 0; k < [population count]; ++k) {
	if (r < fitness[k]) break;
	else r -= fitness[k];
      }
#endif
      id ind1 = [population objectAtIndex: j];
      id ind2 = [population objectAtIndex: k];

      // perform crossover at random nodes
      int n1 = 0, n2 = 0;
      [self numberOfNodes: [ind1 objectForKey: @"root"] count: &n1];
      [self numberOfNodes: [ind2 objectForKey: @"root"] count: &n2];
      n1 = RAND_NUM * n1;
      n2 = RAND_NUM * n2;
      //printf("%d(%d) %d(%d)\n", n1, j, n2, k);
      id newInd1 = [self crossoverIndividual: ind1 at: n1 withIndividual: ind2 at: n2];
      int h = [self height: 0 forTree: [newInd1 objectForKey: @"root"]];
      if (h > maxHeight) {
	//NSLog(@"height = %d", h);
	//NSLog(@"%@", newInd1);
	// if new individual exceeds maximum height, just reproduce parent
	newInd1 = [NSMutableDictionary dictionaryWithDictionary: ind1];
	id subTree = [self deepCopyTree: [newInd1 objectForKey: @"root"]];
	[newInd1 setObject: subTree forKey: @"root"];
      }
      [newPopulation addObject: newInd1];

      id newInd2 = [self crossoverIndividual: ind2 at: n2 withIndividual: ind1 at: n1];
      h = [self height: 0 forTree: [newInd2 objectForKey: @"root"]];
      if (h > maxHeight) {
	//NSLog(@"height = %d", h);
	//NSLog(@"%@", newInd2);
	// if new individual exceeds maximum height, just reproduce parent
	newInd2 = [NSMutableDictionary dictionaryWithDictionary: ind2];
	id subTree = [self deepCopyTree: [newInd2 objectForKey: @"root"]];
	[newInd2 setObject: subTree forKey: @"root"];
      }
      [newPopulation addObject: newInd2];

      ++i;

    } else {

      // perform reproduction
      // randomly pick one individual
      r = RAND_NUM;
      for (j = 0; j < [population count]; ++j) {
	if (r < fitness[j]) {
	  id newInd = [NSMutableDictionary dictionaryWithDictionary: [population objectAtIndex: j]];
	  id subTree = [self deepCopyTree: [newInd objectForKey: @"root"]];
	  [newInd setObject: subTree forKey: @"root"];
	  [newPopulation addObject: newInd];
	  break;
	} else {
	  r -= fitness[j];
	}
      }
    }
  }

  // random mutations
  for (i = 0; i < genSize; ++i) {
    double r = RAND_NUM;
    if (r < mutationProb) {
      // mutate individual
      id ind = [newPopulation objectAtIndex: i];
      // perform mutation at random node
      int n1 = 0;
      [self numberOfNodes: [ind objectForKey: @"root"] count: &n1];
      n1 = RAND_NUM * n1;
      [self mutateIndividual: ind at: n1];
      //NSLog(@"mutate: %@", ind);
    }
  }

  genSize = populationSize - [newPopulation count];
  for (i = 0; i < genSize; ++i) {
    id ind = [self createRandomIndividual];
    [newPopulation addObject: ind];
  }

  printf("count = %ld\n", (unsigned long)[newPopulation count]);
  return newPopulation;
}

- performEvolution: (int)maxGen
{
  int i;
  if (!population) return nil;

  // determine most fit individual
  int gen = 1;
  id mostFit = nil;
  NSMutableString *fitString = nil;
  double bestFitness = 1000000.0;
  BOOL done = NO;
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    printf("gen = %d\n", gen);

    for (i = 0; i < [population count]; ++i) {
      //NSLog(@"height = %d", [self height: 0 forTree: [[population objectAtIndex: i] objectForKey: @"root"]]);
      //NSLog(@"%@", [population objectAtIndex: i]);
    }

    // evaluate fitness of individuals
    [self calculateFitness];

    if (fitness[sortIndex[0]] < bestFitness) {
      bestFitness = fitness[sortIndex[0]];
      mostFit = [population objectAtIndex: sortIndex[0]];
      if (fitString) [fitString release];
      fitString = [NSMutableString new];
      //NSLog(@"mostFit:\n%@", mostFit);
      [self print: fitString tree: [[population objectAtIndex: sortIndex[0]] objectForKey: @"root"]];
      NSLog(@"%@", fitString);
    }
    printf("best fitness = %f\n", fitness[sortIndex[0]]);

    if (fitness[sortIndex[0]] < 1.0) {
      done = YES;
      NSLog(@"mostFit:\n%@", mostFit);
      NSLog(@"%@", fitString);
    }

    // normalize fitness values
    double totFitness = 0.0;
    for (i = 0; i < [population count]; ++i) {
      if (isfinite(fitness[i]))	fitness[i] = 1 / (1 + fitness[i]);
      else fitness[i] = 0;
      totFitness += fitness[i];
    }
    for (i = 0; i < [population count]; ++i) fitness[i] = fitness[i] / totFitness;

#if 0
    printf("normalized fitness\n");
    for (i = 0; i < [population count]; ++i) {
      printf("%f\n", fitness[sortIndex[i]]);
    }
#endif

    // produce next generation
    id newPop = [self produceNextGeneration];
    [population release];
    population = newPop;
    free(fitness);
    fitness = (double *)malloc(sizeof(double) * [population count]);
    free(sortIndex);
    sortIndex = (int *)malloc(sizeof(int) * [population count]);


#if 0
    for (i = 0; i < [population count]; ++i) {
      NSMutableString *s = [NSMutableString new];
      //NSLog(@"%@", [population objectAtIndex: i]);
      [self print: s tree: [[population objectAtIndex: i] objectForKey: @"root"]];
      NSLog(@"%@", s);
      [s release];
    }
#endif

    ++gen;
    if (maxGen && (gen == maxGen)) done = YES;

    [pool release];
  }

  return mostFit;
}

@end
