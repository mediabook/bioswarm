/*
 SMCSampler.m
 
 Sequential Monte Carlo Sampler for Bayesian analysis.
 
 Copyright (C) 2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Oct. 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "SMCSampler.h"
#import "BiologicalModel.h"
#import "BioSwarmModel.h"

#import "internal.h"

@implementation SMCSampler

- initWithModel: (NSDictionary *)aModel andManager: anObj
{
  int i, j;
  NSString *param;
  BOOL good = YES;
  
  if (![super initWithModel: aModel andManager: anObj]) return nil;
  
  // look for MCMC settings
  NSDictionary *mcmc = [baseModel objectForKey: @"MCMC"];
  CHECK_PARAMF(mcmc, "MCMC", good);
  if (!good) return nil;
  
  numOfParticles = 1000;
  param = [mcmc objectForKey: @"particles"];
  if (param) numOfParticles = [param intValue];
  
  ESSThreshold = 0.5;
  param = [mcmc objectForKey: @"ESSThreshold"];
  if (param) ESSThreshold = [param doubleValue];
  if ((ESSThreshold <= 0) || (ESSThreshold > 1)) {
    printf("ERROR: Effective sample size threshold must be in range (0,1] but value is %f\n", ESSThreshold);
    return nil;
  }
  
  saveBest = NO;
  param = [mcmc objectForKey: @"saveBest"];
  if (param) saveBest = [param boolValue];
  
  initialRandom = YES;
  param = [mcmc objectForKey: @"initialRandom"];
  if (param) initialRandom = [param boolValue];
  
#if 0
  // set up compare matrices for each particle
  if (dataSets) {
    NSMutableDictionary *compareMatrices = [compareMatrixList objectAtIndex: 0];
    NSArray *keys = [compareMatrices allKeys];
    for (i = 1; i < numOfParticles; ++i) {
      NSMutableDictionary *d = [NSMutableDictionary new];
      for (j = 0; j < [keys count]; ++j) {
        NSString *key = [keys objectAtIndex: j];
        BCDataMatrix *nm = [compareMatrices objectForKey: key];
        [d setObject: [BCDataMatrix dataMatrixWithDataMatrix: nm] forKey: key];
      }
      [compareMatrixList addObject: d];
    }
  }
#endif

  return self;
}

- (void)dealloc
{
  if (weights) free(weights);
  
  [super dealloc];
}

- (BOOL)proposeMoveWithModel: (NSDictionary *)mcmcModel
{
  int i, j, k;
  
  // release the old model
  [self releaseModel];
  
  int totalParameters = [parameterNames count];
  double (*iParameters)[numOfParticles][totalParameters] = parameterValues;
  double (*iNewParameters)[numOfParticles][totalParameters] = parameterNewValues;
  for (i = 0; i < numOfParticles; ++i) {
    NSError *error = nil;
    NSData *xmlData = 
    [NSPropertyListSerialization dataWithPropertyList: mcmcModel
                                               format: NSPropertyListXMLFormat_v1_0
                                              options: 0
                                                error: &error];
    if (error) return NO;
    
    NSMutableDictionary *simParameters =
    [NSPropertyListSerialization propertyListWithData: xmlData
                                              options: NSPropertyListMutableContainersAndLeaves
                                               format: nil
                                                error: &error];
    if (error) return NO;
    
    //NSDictionary *modelSet = [simParameters objectForKey: @"models"];
    //NSArray *modelKeys = [modelSet allKeys];
    NSMutableDictionary *simControl = [simParameters objectForKey: @"simulationControl"];

    for (j = 0; j < totalParameters; ++j) {
      NSString *pn = [parameterNames objectAtIndex: j];
      NSNumber *nmod = [parameterModelNumbers objectAtIndex: j];
      NSString *key = [parameterModelKeys objectAtIndex: j];
      //NSString *key = [parameterModelKeys objectAtIndex: [nmod intValue]];
      
      // set each parameter value in appropriate submodel
      for (k = 0; k < 10; ++k) {
        // ten tries to get parameter within bounds
        (*iNewParameters)[i][j] = (*iParameters)[i][j];
        double nr = normal_dist_rand(0, 1);
        double np = (*iParameters)[i][j] + deltaTheta * avgMag[j] * nr;
        if ([self isDebug]) printf("%d: %f %f %f %f = %f\n", k, (*iParameters)[i][j], deltaTheta, avgMag[j], nr, np);
        if ((np >= parameterMinRange[j]) && (np <= parameterMaxRange[j])) {
          (*iNewParameters)[i][j] = np;
          break;
        }
      }
      if (k == 10) {
        // randomize if cannot get within bounds
        //NSArray *modelInstances = [theController modelInstances];
        //NSArray *subModels = [modelInstances objectAtIndex: 0];
        NSNumber *nidx = [parameterIndexes objectAtIndex: j];
        //id aModel = [subModels objectAtIndex: [nmod intValue]];
        id modelObject = [modelInstances objectAtIndex: 0];
        id aModel = [modelObject modelWithNumber: [nmod intValue]];
        (*iNewParameters)[i][j] = [aModel randomValueforParameter: [nidx intValue]];
      }
      
      //NSString *key = [modelKeys objectAtIndex: [nmod intValue]];
      //NSString *val = [modelSet objectForKey: key];
      //NSMutableDictionary *modelDict = [simParameters objectForKey: val];
      NSMutableDictionary *modelDict = [simParameters objectForKey: key];
      
      [modelDict setObject: [NSNumber numberWithDouble: (*iNewParameters)[i][j]] forKey: pn];
      if ([self isDebug]) printf("(%d): %s = %f, min = %f, max = %f\n", i, [pn UTF8String],
				 (*iNewParameters)[i][j], parameterMinRange[j], parameterMaxRange[j]);

    }

    // new random seed
    [simControl setObject: [NSNumber numberWithInt: rand()] forKey: @"randomSeed"];

    // create model individual
    if (i == 0) [self loadModel: simParameters];
    else [self newInstanceForModel: simParameters];    
  }
  
  // do not write output files
  [self setFileState: BCMODEL_NOFILE_STATE];
  
  return YES;
}

- (BOOL)saveBestParticle: (int)aParticle sample: (int)sampleNum
{
  int j;
  NSError *error = nil;
  int totalParameters = [parameterNames count];
  double (*iParameters)[numOfParticles][totalParameters] = parameterValues;
  
  // current model specification may not have correct parameter values
  // so create one from scratch
  NSData *xmlData = 
  [NSPropertyListSerialization dataWithPropertyList: baseModel
                                             format: NSPropertyListXMLFormat_v1_0
                                            options: 0
                                              error: &error];
  if (error) return NO;
  
  NSMutableDictionary *simParameters =
  [NSPropertyListSerialization propertyListWithData: xmlData
                                            options: NSPropertyListMutableContainersAndLeaves
                                             format: nil
                                              error: &error];
  if (error) return NO;
  
  //NSDictionary *modelSet = [simParameters objectForKey: @"models"];
  //NSArray *modelKeys = [modelSet allKeys];
  
  for (j = 0; j < totalParameters; ++j) {
    // set each parameter value in appropriate submodel
    NSString *pn = [parameterNames objectAtIndex: j];
    NSNumber *nmod = [parameterModelNumbers objectAtIndex: j];
    NSString *key = [parameterModelKeys objectAtIndex: j];
    //NSString *key = [parameterModelKeys objectAtIndex: [nmod intValue]];
    
    //NSString *key = [modelKeys objectAtIndex: [nmod intValue]];
    //NSString *val = [modelSet objectForKey: key];
    NSMutableDictionary *modelDict = [simParameters objectForKey: key];
    
    [modelDict setObject: [NSNumber numberWithDouble: (*iParameters)[aParticle][j]] forKey: pn];
  }
  
  // random seed from the particle
  id simControl = [[self getSpecificationForModel: aParticle] objectForKey: @"simulationControl"];
  id randNum = [simControl objectForKey: @"randomSeed"];
  simControl = [simParameters objectForKey: @"simulationControl"];
  [simControl setObject: randNum forKey: @"randomSeed"];
  
  return [self saveModelSpecification: simParameters forRun: sampleNum];
}

- (void)performMCMCWithModel: (NSDictionary *)mcmcModel setNumber: (int)setNum
{
  int i, j, k, expIndex;
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  NSNotificationCenter *noticeCenter = [NSNotificationCenter defaultCenter];

  int totalParameters = [parameterNames count];
  parameterValues = malloc(sizeof(double) * numOfParticles * totalParameters);
  double (*iParameters)[numOfParticles][totalParameters] = parameterValues;
  parameterNewValues = malloc(sizeof(double) * numOfParticles * totalParameters);
  double (*iNewParameters)[numOfParticles][totalParameters] = parameterNewValues;
  //NSDictionary *modelSet = [mcmcModel objectForKey: @"models"];
  //NSArray *modelKeys = [modelSet allKeys];
  //NSArray *modelInstances = [theController modelInstances];
  id modelObject = [modelInstances objectAtIndex: 0];
  for (i = 0; i < numOfParticles; ++i) {
    for (j = 0; j < totalParameters; ++j) {
      NSString *pn = [parameterNames objectAtIndex: j];
      NSNumber *nidx = [parameterIndexes objectAtIndex: j];
      NSNumber *nmod = [parameterModelNumbers objectAtIndex: j];
      NSString *key = [parameterModelKeys objectAtIndex: j];
      //NSString *key = [parameterModelKeys objectAtIndex: [nmod intValue]];
      //id aModel = [subModels objectAtIndex: [nmod intValue]];
      id aModel = [modelObject modelWithNumber: [nmod intValue]];

      if (initialRandom) {
        (*iParameters)[i][j] = [aModel randomValueforParameter: [nidx intValue]];
      } else {
        //NSString *key = [modelKeys objectAtIndex: [nmod intValue]];
        //NSString *val = [modelSet objectForKey: key];
        NSMutableDictionary *modelDict = [mcmcModel objectForKey: key];
        (*iParameters)[i][j] = [[modelDict objectForKey: pn] doubleValue];    
      }

      (*iNewParameters)[i][j] = (*iParameters)[i][j];
      if ([self isDebug]) printf("Initial value %s = %f\n", [pn UTF8String], (*iParameters)[i][j]);
    }
  }
  
  // inform delegate of parameter information
  if ([delegate respondsToSelector: @selector(parameterInfo:names:indexes:modelNumbers:modelKeys:minRanges:maxRange:acceptValues:rejectValues:)])
    [delegate parameterInfo: self names: parameterNames indexes: parameterIndexes modelNumbers: parameterModelNumbers modelKeys: parameterModelKeys
                  minRanges: parameterMinRange maxRange: parameterMaxRange acceptValues: parameterNewValues rejectValues: parameterValues];

  // post notification about parameter information
  [noticeCenter postNotificationName: BioSwarmMCMCParameterInfoNotification object: self
                            userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                                       parameterNames, @"names",
                                       parameterIndexes, @"indexes",
                                       parameterModelNumbers, @"modelNumbers",
                                       parameterModelKeys, @"modelKeys",
                                       [NSValue valueWithPointer: parameterMinRange], @"minRanges",
                                       [NSValue valueWithPointer: parameterMaxRange], @"maxRanges",
                                       [NSValue valueWithPointer: parameterNewValues], @"acceptValues",
                                       [NSValue valueWithPointer: parameterValues], @"rejectValues",
                                       nil]];

  // inform delegate
  if ([delegate respondsToSelector: @selector(willPerformMCMC:setNumber:)])
    [delegate willPerformMCMC: self setNumber: setNum];

  // particle weights
  weights = malloc(sizeof(double) * numOfParticles);
  for (i = 0; i < numOfParticles; ++i) weights[i] = 1.0 / (double)numOfParticles;

  // initialize counters
  double thetaTry = 0;
  double thetaAccept = 0;
  int samplesTaken = 0;
  double posteriorDensity[numOfParticles];
  double newPosteriorDensity[numOfParticles];
  double calcPosteriorDensity[numOfParticles];
  double acceptanceProb[numOfParticles];
  BOOL acceptFlag[numOfParticles];
  
  // initial simulation
  [self proposeMoveWithModel: mcmcModel];
  if (respondsToWillCalc)
    for (i = 0; i < numOfParticles; ++i) [delegate willCalculatePosteriorDensity: self forIndividual: i];
  if (!experiments) {
    [self doSimulationRun];
    for (i = 0; i < numOfParticles; ++i) posteriorDensity[i] = [delegate calculatePosteriorDensity: self forIndividual: i];
    //if (isnan(posteriorDensity)) return;
  } else {
    for (i = 0; i < numOfParticles; ++i) posteriorDensity[i] = 0.0;
    for (expIndex = 0; expIndex < [experiments count]; ++expIndex) {
      NSString *anExp = [experiments objectAtIndex: expIndex];
      [self doSimulationRunForExperiment: anExp];
      if (![self calculatePosteriorDensity: calcPosteriorDensity experiment: anExp]) return;
      for (i = 0; i < numOfParticles; ++i) posteriorDensity[i] += calcPosteriorDensity[i];
    }
  }

  // accept first simulation
  for (i = 0; i < numOfParticles; ++i) {
    for (j = 0; j < totalParameters; ++j) {
      (*iParameters)[i][j] = (*iNewParameters)[i][j];
    }
  }

  // let delegate perform any initialization for expectation calculation
  if ([delegate respondsToSelector: @selector(initExpectation:)]) {
    [delegate initExpectation: self];
  }
  
  // Monte Carlo iterations
  int mcIter, mcTot = equilibration + samples;
  for (mcIter = 1; mcIter <= mcTot; ++mcIter) {
    //[theController releaseModel];
    [pool release];
    pool = [NSAutoreleasePool new];
    
#if 1
    // adjust step sizes
    if (thetaTry > deltaAdjust) {
      double fracAccept = thetaAccept / thetaTry;
      if (fracAccept < acceptanceTarget)
        deltaTheta = deltaTheta * 0.9;
      else if (fracAccept > acceptanceTarget)
        deltaTheta = deltaTheta * 1.1;
      if (deltaTheta > 1.0) deltaTheta = 1.0;
      if ([self isDebug]) printf("fracAccept = %f, deltaTheta = %f\n", fracAccept, deltaTheta);
      thetaTry = 0;
      thetaAccept = 0;
    }
#endif
    
    // propose move
    [self proposeMoveWithModel: mcmcModel];
    thetaTry += numOfParticles;
    
    // perform simulation
    if (respondsToWillCalc)
      for (i = 0; i < numOfParticles; ++i) [delegate willCalculatePosteriorDensity: self forIndividual: i];
    if (!experiments) {
      [self doSimulationRun];
      for (i = 0; i < numOfParticles; ++i) {
        newPosteriorDensity[i] = [delegate calculatePosteriorDensity: self forIndividual: i];
        acceptanceProb[i] = newPosteriorDensity[i] / posteriorDensity[i];
      }
    } else {
      for (i = 0; i < numOfParticles; ++i) newPosteriorDensity[i] = 0.0;
      for (expIndex = 0; expIndex < [experiments count]; ++expIndex) {
        NSString *anExp = [experiments objectAtIndex: expIndex];
        printf("experiment: %s(%d of %ld)\n", [anExp UTF8String], expIndex+1, (unsigned long)[experiments count]);
        [self doSimulationRunForExperiment: anExp];
        if (![self calculatePosteriorDensity: calcPosteriorDensity experiment: anExp]) return;
        for (i = 0; i < numOfParticles; ++i) newPosteriorDensity[i] += calcPosteriorDensity[i];
      }
      for (i = 0; i < numOfParticles; ++i) acceptanceProb[i] = exp(newPosteriorDensity[i] - posteriorDensity[i]);
    }
    
    // accept or reject
    for (i = 0; i < numOfParticles; ++i) {
      if ([self isDebug]) printf("(%d) pd = %e, new pd = %e, acceptanceProb = %f\n", i, posteriorDensity[i], newPosteriorDensity[i], acceptanceProb[i]);
      if (RAND_NUM <= acceptanceProb[i]) {
        if ([self isDebug]) printf("accept\n");
        acceptFlag[i] = YES;
      } else {
        if ([self isDebug]) printf("reject\n");
        acceptFlag[i] = NO;
      }
    }
    
    // take new values if accepted
    double bestPosterior = -INFINITY;
    int bestParticle = 0;
    for (i = 0; i < numOfParticles; ++i) {
      if (acceptFlag[i]) {
        ++thetaAccept;
        posteriorDensity[i] = newPosteriorDensity[i];
      }
      if (posteriorDensity[i] > bestPosterior) {
        bestPosterior = posteriorDensity[i];
        bestParticle = i;
      }
    }
    
    // update weights and calculate effective sample size
    double totalWeight = 0.0;
    double ESS = 0.0;
    for (i = 0; i < numOfParticles; ++i) {
      weights[i] *= exp(posteriorDensity[i]);
      //weights[i] *= acceptanceProb[i];
      totalWeight += weights[i];
    }
    for (i = 0; i < numOfParticles; ++i) {
      weights[i] = weights[i] / totalWeight;
      if ([self isDebug]) printf("(%d) weight = %e\n", i, weights[i]);
      ESS += weights[i] * weights[i];
    }
    ESS = 1.0 / ESS;
    if ([self isDebug]) printf("totalWeight = %e, ESS = %f\n", totalWeight, ESS);
    
    // evalulate expectation function
    if (mcIter > equilibration) {
      if (respondsToCalcExpectation)
        [delegate calculateExpectation: self weights: weights accept: acceptFlag];
      ++samplesTaken;
    }
    
    // update accepted parameters
    for (i = 0; i < numOfParticles; ++i) {
      if (acceptFlag[i]) {
        for (j = 0; j < totalParameters; ++j) {
          (*iParameters)[i][j] = (*iNewParameters)[i][j];
        }
      }
    }
    
    // save best particle
    if (saveBest) {
      if ([self isDebug]) printf("bestParticle: %d posterior: %e\n", bestParticle, bestPosterior);
      if (![self saveBestParticle: bestParticle sample: mcIter])
        printf("WARNING: Could not save model specification for best particle.\n");
    }

    // post notification about best individual
    [noticeCenter postNotificationName: BioSwarmBestMCMCIndividualNotification object: self
                              userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                                         [NSNumber numberWithInt: bestParticle], @"particle",
                                         [NSNumber numberWithDouble:bestPosterior], @"posterior", nil]];

    // Determine if should resample
    if (ESS < ((double)numOfParticles * ESSThreshold)) {
      if ([self isDebug]) printf("resampling\n");

      // hold values
      for (i = 0; i < numOfParticles; ++i) {
        newPosteriorDensity[i] = posteriorDensity[i];
        for (j = 0; j < totalParameters; ++j)
          (*iNewParameters)[i][j] = (*iParameters)[i][j];
      }
      
      // resample
      double c1 = 0.0;
      int sampleSize;
      int p = 0;
      for (i = 0; i < numOfParticles; ++i) {
        //c2 = c1;
        c1 = weights[i] * (double)numOfParticles + c1;
        sampleSize = (int)(c1 + 0.5) - p;
        //sampleSize = (int)(weights[i] * (double)numOfParticles + 0.5);
        if ((p + sampleSize) > numOfParticles) sampleSize = numOfParticles - p;
        if ([self isDebug]) printf("(%d, %f) weight = %f, sampleSize = %d, p = %d\n", i, c1, weights[i], sampleSize, p);
        
        weights[i] = 1.0 / (double)numOfParticles;
        if (sampleSize > 0) {
          for (k = 0; k < sampleSize; ++k) {
            posteriorDensity[p+k] = newPosteriorDensity[i];
            for (j = 0; j < totalParameters; ++j)
              (*iParameters)[p+k][j] = (*iNewParameters)[i][j];
          }
          p += sampleSize;
        }
      }
    }

    // increment the step averages
    for (i = 0; i < totalParameters; ++i) {
      double paramSum = 0.0;
      for (j = 0; j < numOfParticles; ++j) {
        paramSum += fabs((*iParameters)[j][i]);
      }
      paramSum = paramSum / (double)numOfParticles;
      if ([self isDebug]) printf("paramSum = %f\n", paramSum);
      sumMag[i] = sumMag[i] + paramSum;
      avgMag[i] = sumMag[i]/(double)(mcIter+1);
      if (avgMag[i] < sqrt(EPS)) avgMag[i] = sqrt(EPS);
    }
  }
  
  if ([delegate respondsToSelector: @selector(finishExpectation:)]) {
    [delegate finishExpectation: self];
  }

  if ([delegate respondsToSelector: @selector(didPerformMCMC:setNumber:)])
    [delegate didPerformMCMC: self setNumber: setNum];
    
  //[[NSNotificationCenter defaultCenter] removeObserver:self];

  [pool release];
}

@end
