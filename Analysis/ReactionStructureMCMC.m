/*
 ReactionStructureMCMC.m
 
 MCMC over model structure for DynamicReactionRuleModel.
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Dec. 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "ReactionStructureMCMC.h"
#import "DynamicReactionRuleModel.h"

#import "internal.h"

@implementation ReactionStructureMCMC

- initWithModel: (NSDictionary *)aModel andManager: anObj
{
  int i, j;
  NSString *param;
  BOOL good = YES;
  
  if (![super initWithModel: aModel andManager: anObj]) return nil;
  
  // look for MCMC settings
  NSDictionary *mcmc = [baseModel objectForKey: @"MCMC"];
  CHECK_PARAMF(mcmc, "MCMC", good);
  if (!good) return nil;
  
  geneList = [mcmc objectForKey: @"genes"];
  metaboliteList = [mcmc objectForKey: @"metabolites"];
  inputList = [mcmc objectForKey: @"inputs"];

  numOfParticles = 1;

  networkEquilibration = 100;
  param = [mcmc objectForKey: @"networkEquilibration"];
  if (param) networkEquilibration = [param intValue];
  
  networkSamples = 1000;
  param = [mcmc objectForKey: @"networkSamples"];
  if (param) networkSamples = [param intValue];

  maxIncoming = 3;
  param = [mcmc objectForKey: @"maxIncoming"];
  if (param) maxIncoming = [param intValue];

  NSError *error = nil;
  xmlModel = [NSPropertyListSerialization dataWithPropertyList: aModel
                                             format: NSPropertyListXMLFormat_v1_0
                                            options: 0
                                              error: &error];
  if (error) return nil;
  [xmlModel retain];
  //printf("xmlModel = %s\n", [xmlModel bytes]);
  //printf("baseModel = %s\n", [[baseModel description] UTF8String]);

  rnGen = [[BCMersenneTwister alloc] initWithSeed: randomSeed];

  // setup base species and reactions
  speciesList = [NSMutableArray new];
  baseReactions = [NSMutableDictionary new];
  baseReactionParameters = [NSMutableDictionary new];
  proposalReactions = [NSMutableDictionary new];
  proposalReactionParameters = [NSMutableDictionary new];
  proposalSpecies = [NSMutableArray new];
  for (i = 0; i < [geneList count]; ++i) {
    NSString *gene = [geneList objectAtIndex: i];
    NSString *protein = [gene uppercaseString];
    NSMutableDictionary *rule;
    NSString *ruleName;
    NSMutableArray *ruleList;
    NSMutableDictionary *parameterInfo;
    NSMutableArray *paramList, *mcmcParams;
    NSMutableDictionary *paramAttributes, *pa;
    NSString *paramName;

    // base rules for mRNA
    [speciesList addObject: gene];
    ruleList = [NSMutableArray array];
    [baseReactions setObject: ruleList forKey: gene];

    // basal transcription
    // {type=signal; name=r0; products=(lasI); reactants=(); rate=MassAction;}
    rule = [NSMutableDictionary dictionary];
    [rule setObject: @"signal" forKey: @"type"];
    [rule setObject: [NSArray arrayWithObject: gene] forKey: @"products"];
    [rule setObject: [NSArray array] forKey: @"reactants"];
    [rule setObject: @"MassAction" forKey: @"rate"];
    ruleName = [NSString stringWithFormat: @"br%ld_0", (unsigned long)[speciesList count] - 1];
    [rule setObject: ruleName forKey: @"name"];
    [ruleList addObject: rule];
    // parameters for reaction rule
    parameterInfo = [NSMutableDictionary dictionary];
    paramList = [NSMutableArray array];
    mcmcParams = [NSMutableArray array];
    paramAttributes = [NSMutableDictionary dictionary];
    [parameterInfo setObject: paramList forKey: @"parameters"];
    [parameterInfo setObject: mcmcParams forKey: @"mcmcParameters"];
    [parameterInfo setObject: paramAttributes forKey: @"attributes"];
    [baseReactionParameters setObject: parameterInfo forKey: ruleName];

    paramName = [NSString stringWithFormat: @"%@_%@_k", ruleName, gene];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: 0.01] forKey: @"value"];
    [paramAttributes setObject: pa forKey: paramName];

    // mRNA decay
    // {type=signal; name=r1; products=(lasI); reactants=(lasI); rate=MassAction;}
    rule = [NSMutableDictionary dictionary];
    [rule setObject: @"signal" forKey: @"type"];
    [rule setObject: [NSArray arrayWithObject: gene] forKey: @"products"];
    [rule setObject: [NSArray arrayWithObject: gene] forKey: @"reactants"];
    [rule setObject: @"MassAction" forKey: @"rate"];
    ruleName = [NSString stringWithFormat: @"br%ld_1", (unsigned long)[speciesList count] - 1];
    [rule setObject: ruleName forKey: @"name"];
    [ruleList addObject: rule];
    // parameters for reaction rule
    parameterInfo = [NSMutableDictionary dictionary];
    paramList = [NSMutableArray array];
    mcmcParams = [NSMutableArray array];
    paramAttributes = [NSMutableDictionary dictionary];
    [parameterInfo setObject: paramList forKey: @"parameters"];
    [parameterInfo setObject: mcmcParams forKey: @"mcmcParameters"];
    [parameterInfo setObject: paramAttributes forKey: @"attributes"];
    [baseReactionParameters setObject: parameterInfo forKey: ruleName];

    paramName = [NSString stringWithFormat: @"%@_%@_k", ruleName, gene];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: -0.1] forKey: @"value"];
    [paramAttributes setObject: pa forKey: paramName];

    // base rules for protein
    [speciesList addObject: protein];
    ruleList = [NSMutableArray array];
    [baseReactions setObject: ruleList forKey: protein];

    // translation
    // {type=signal; name=r2; products=(LASI); reactants=(lasI); rate=Hill;}
    rule = [NSMutableDictionary dictionary];
    [rule setObject: @"signal" forKey: @"type"];
    [rule setObject: [NSArray arrayWithObject: protein] forKey: @"products"];
    [rule setObject: [NSArray arrayWithObject: gene] forKey: @"reactants"];
    [rule setObject: @"Hill" forKey: @"rate"];
    ruleName = [NSString stringWithFormat: @"br%ld_0", (unsigned long)[speciesList count] - 1];
    [rule setObject: ruleName forKey: @"name"];
    [ruleList addObject: rule];
    // parameters for reaction rule
    parameterInfo = [NSMutableDictionary dictionary];
    paramList = [NSMutableArray array];
    mcmcParams = [NSMutableArray array];
    paramAttributes = [NSMutableDictionary dictionary];
    [parameterInfo setObject: paramList forKey: @"parameters"];
    [parameterInfo setObject: mcmcParams forKey: @"mcmcParameters"];
    [parameterInfo setObject: paramAttributes forKey: @"attributes"];
    [baseReactionParameters setObject: parameterInfo forKey: ruleName];
    
    paramName = [NSString stringWithFormat: @"%@_%@_pmax_%@", ruleName, protein, gene];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
    [paramAttributes setObject: pa forKey: paramName];

    paramName = [NSString stringWithFormat: @"%@_%@_c_%@", ruleName, protein, gene];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
    [paramAttributes setObject: pa forKey: paramName];

    paramName = [NSString stringWithFormat: @"%@_%@_h_%@", ruleName, protein, gene];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: -1.0] forKey: @"value"];
    [paramAttributes setObject: pa forKey: paramName];

    // protein degradation
    // {type=signal; name=r3; products=(LASI); reactants=(LASI); rate=MassAction;}
    rule = [NSMutableDictionary dictionary];
    [rule setObject: @"signal" forKey: @"type"];
    [rule setObject: [NSArray arrayWithObject: protein] forKey: @"products"];
    [rule setObject: [NSArray arrayWithObject: protein] forKey: @"reactants"];
    [rule setObject: @"MassAction" forKey: @"rate"];
    ruleName = [NSString stringWithFormat: @"br%ld_1", (unsigned long)[speciesList count] - 1];
    [rule setObject: ruleName forKey: @"name"];
    [ruleList addObject: rule];

    // parameters for reaction rule
    parameterInfo = [NSMutableDictionary dictionary];
    paramList = [NSMutableArray array];
    mcmcParams = [NSMutableArray array];
    paramAttributes = [NSMutableDictionary dictionary];
    [parameterInfo setObject: paramList forKey: @"parameters"];
    [parameterInfo setObject: mcmcParams forKey: @"mcmcParameters"];
    [parameterInfo setObject: paramAttributes forKey: @"attributes"];
    [baseReactionParameters setObject: parameterInfo forKey: ruleName];
    
    paramName = [NSString stringWithFormat: @"%@_%@_k", ruleName, protein];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: -0.1] forKey: @"value"];
    [paramAttributes setObject: pa forKey: paramName];
    
    //
    // null model is for each gene to autoregulate itself
    //
    ruleList = [NSMutableArray array];
    [proposalReactions setObject: ruleList forKey: gene];
    
    // autoregulation
    // {type=signal; name=r10; products=(lasI); reactants=(LASI); rate=Hill;}
    rule = [NSMutableDictionary dictionary];
    [rule setObject: @"signal" forKey: @"type"];
    [rule setObject: [NSMutableArray arrayWithObject: gene] forKey: @"products"];
    [rule setObject: [NSMutableArray arrayWithObject: protein] forKey: @"reactants"];
    [rule setObject: @"Hill" forKey: @"rate"];
    //ruleName = [NSString stringWithFormat: @"r%d_10", [speciesList count] - 1];
    [ruleList addObject: rule];
    ruleName = [self determineRuleName: ruleList forSpecies: [speciesList count] - 2];
    [rule setObject: ruleName forKey: @"name"];

    // parameters for reaction rule
    parameterInfo = [NSMutableDictionary dictionary];
    paramList = [NSMutableArray array];
    //mcmcParams = [NSMutableArray array];
    paramAttributes = [NSMutableDictionary dictionary];
    [parameterInfo setObject: paramList forKey: @"parameters"];
    //[parameterInfo setObject: mcmcParams forKey: @"mcmcParameters"];
    [parameterInfo setObject: paramAttributes forKey: @"attributes"];
    [proposalReactionParameters setObject: parameterInfo forKey: ruleName];
    
    paramName = [NSString stringWithFormat: @"%@_%@_pmax_%@", ruleName, gene, protein];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
    [paramAttributes setObject: pa forKey: paramName];
    
    paramName = [NSString stringWithFormat: @"%@_%@_c_%@", ruleName, gene, protein];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
    [pa setObject: [NSNumber numberWithFloat: 0.01] forKey: @"min"];
    [pa setObject: [NSNumber numberWithFloat: 20.0] forKey: @"max"];
    [paramAttributes setObject: pa forKey: paramName];
    //[mcmcParams addObject: paramName];
    
    paramName = [NSString stringWithFormat: @"%@_%@_h_%@", ruleName, gene, protein];
    [paramList addObject: paramName];
    pa = [NSMutableDictionary dictionary];
    [pa setObject: [NSNumber numberWithFloat: -1.0] forKey: @"value"];
    [paramAttributes setObject: pa forKey: paramName];

    //
    // Potential new species are:
    // 1) protein bind with metabolite
    // 2) form dimer
    //
    for (j = 0; j < [metaboliteList count]; ++j) {
      [speciesList addObject: [NSString stringWithFormat: @"%@_%@", protein, [metaboliteList objectAtIndex: j]]];
      [speciesList addObject: [NSString stringWithFormat: @"%@_%@_dimer", protein, [metaboliteList objectAtIndex: j]]];
    }
  }

  // metabolites
  [speciesList addObjectsFromArray: metaboliteList];

  printf("speciesList = %s\n", [[speciesList description] UTF8String]);
  //printf("baseReactions = %s\n", [[baseReactions description] UTF8String]);
  printf("baseReactionParameters = %s\n", [[baseReactionParameters description] UTF8String]);

  acceptedModels = [NSMutableArray new];
  acceptedModelSettings = [NSMutableArray new];
  
  return self;
}

- (void)dealloc
{
  [xmlModel release];
  [speciesList release];
  [baseReactions release];
  [baseReactionParameters release];

  [acceptedModels release];
  
  [super dealloc];
}

//
// We need to generate a unique name for the set of regulation rules for a gene,
// to avoid parameter name conflict, and to more easily determine when a model is the
// same as one we have tested before.
//
// Method involves:
// 1) Ordering the species, will use their index #
// 2) Describe structure by species order
//
// Possible rule structures for gene regulatory rules, with 3 max incoming
// A, B, C, A+B, A+C, B+C, A+B+C
// A*B, A*C, B*C, A*B*C
// A*B+C, A*C+B, B*C+A
//
// We need to follow ANSI-C variable naming guidelines, so just letters, numbers and _
//
- (NSString *)determineRuleName: (NSArray *)ruleList forSpecies: (int)speciesNum
{
  int i, j;
  
  // gather list of species and order them
  id orderedSpecies = [NSMutableArray array];
  for (i = 0; i < [ruleList count]; ++i) {
    NSDictionary *rule = [ruleList objectAtIndex: i];
    NSArray *reactants = [rule objectForKey: @"reactants"];
    for (j = 0; j < [reactants count]; ++j) {
      NSString *s = [reactants objectAtIndex: j];
      NSUInteger idx = [speciesList indexOfObject: s];
      printf("%d: %s %ld\n", i, [s UTF8String], (unsigned long)idx);
      [orderedSpecies addObject: [NSNumber numberWithInt: idx]];
    }
  }
  orderedSpecies = [orderedSpecies sortedArrayUsingSelector: @selector(compare:)];
  orderedSpecies = [NSMutableArray arrayWithArray: orderedSpecies];
  printf("ordered: %s\n", [[orderedSpecies description] UTF8String]);
  
  NSMutableString *ruleName = [NSMutableString stringWithFormat: @"r%d", speciesNum];

  BOOL done = NO;
  while (!done) {

    // processed all species so we are done
    if ([orderedSpecies count] == 0) {
      done = YES;
      continue;
    }

    [ruleName appendString: @"_"];

    // find rule with next ordered species
    NSUInteger currentRule = NSNotFound;
    NSNumber *n = [orderedSpecies objectAtIndex: 0];
    NSString *speciesName = [speciesList objectAtIndex: [n intValue]];
    for (j = 0; j < [ruleList count]; ++j) {
      NSDictionary *rule = [ruleList objectAtIndex: j];
      NSArray *reactants = [rule objectForKey: @"reactants"];
      if ([reactants indexOfObject: speciesName] != NSNotFound) {
        currentRule = j;
        break;
      }
    }

    // problem
    if (currentRule == NSNotFound) {
      printf("ERROR: Could not find rule for species: %d %s\n", [n intValue], [speciesName UTF8String]);
      done = YES;
      continue;
    }
    
    // order the species within the single rule
    NSDictionary *rule = [ruleList objectAtIndex: currentRule];
    NSArray *reactants = [rule objectForKey: @"reactants"];
    id ruleSpecies = [NSMutableArray array];
    for (j = 0; j < [reactants count]; ++j) {
      NSString *s = [reactants objectAtIndex: j];
      NSUInteger idx = [speciesList indexOfObject: s];
      printf("%d: %s %ld\n", i, [s UTF8String], (unsigned long)idx);
      [ruleSpecies addObject: [NSNumber numberWithInt: idx]];
    }
    ruleSpecies = [ruleSpecies sortedArrayUsingSelector: @selector(compare:)];

    for (j = 0; j < [ruleSpecies count]; ++j) {
      NSNumber *sn = [ruleSpecies objectAtIndex: j];
      NSString *s = [speciesList objectAtIndex: [sn intValue]];
      if (j == 0) [ruleName appendFormat: @"%d", [sn intValue]];
      else [ruleName appendFormat: @"x%d", [sn intValue]];
      [orderedSpecies removeObject: sn];
    }
  }

  [ruleName appendString: @"_"];
  printf("ruleName: %s\n", [ruleName UTF8String]);
  return ruleName;
}

- (void)reassign: (NSArray *)ruleList withRuleName: (NSString *)ruleName
{
  int i, j;
  NSString *oldRuleName;
  
  printf("ruleList = %s\n", [[ruleList description] UTF8String]);

  // find old rule name
  for (i = 0; i < [ruleList count]; ++i) {
    NSMutableDictionary *rule = [ruleList objectAtIndex: i];
    oldRuleName = [rule objectForKey: @"name"];
    printf("oldRuleName: %s\n", [oldRuleName UTF8String]);
    if (oldRuleName) {
      [oldRuleName retain];
      break;
    }
  }

  // set new rule name
  for (i = 0; i < [ruleList count]; ++i) {
    NSMutableDictionary *rule = [ruleList objectAtIndex: i];
    [rule setObject: ruleName forKey: @"name"];
  }

  // if no old rule names, then must not be any old rules
  // so no parameter info to copy
  if (oldRuleName) {
    printf("oldRuleName: %s\n", [oldRuleName UTF8String]);
    
    NSDictionary *oldParameterInfo = [proposalReactionParameters objectForKey: oldRuleName];
    NSArray *oldParamList = [oldParameterInfo objectForKey: @"parameters"];
    NSDictionary *oldParamAttributes = [oldParameterInfo objectForKey: @"attributes"];
    
    NSMutableDictionary *parameterInfo = [NSMutableDictionary dictionary];
    NSMutableArray *paramList = [NSMutableArray array];
    NSMutableDictionary *paramAttributes = [NSMutableDictionary dictionary];
    [parameterInfo setObject: paramList forKey: @"parameters"];
    [parameterInfo setObject: paramAttributes forKey: @"attributes"];
    [proposalReactionParameters setObject: parameterInfo forKey: ruleName];
    
    // go through each old parameter name, copy info to new parameters
    for (j = 0; j < [oldParamList count]; ++j) {
      NSString *oldParamName = [oldParamList objectAtIndex: j];
      NSRange r = [oldParamName rangeOfString: @"__"];
      if (r.location == NSNotFound) {
        printf("ERROR: invalid parameter name, missing __: %s\n", [oldParamName UTF8String]);
        return;
      }
      
      // copy info to new parameters
      NSString *suffix = [oldParamName substringFromIndex: r.location + 1];
      NSString *newParamName = [NSString stringWithFormat: @"%@%@", ruleName, suffix];
      [paramList addObject: newParamName];
      NSMutableDictionary *pa = [NSMutableDictionary dictionaryWithDictionary: [oldParamAttributes objectForKey: oldParamName]];
      [paramAttributes setObject: pa forKey: newParamName];
    }

  }

  // remove old rule parameter info
  if (oldRuleName) {
    [proposalReactionParameters removeObjectForKey: oldRuleName];
    [oldRuleName release];
  }

  printf("proposalReactionParameters = \n%s\n", [[proposalReactionParameters description] UTF8String]);
}

- (BOOL)doRulesContainReactant: (NSString *)speciesName
{
  int i, j;
  NSArray *keys = [proposalReactions allKeys];
  for (i = 0; i < [keys count]; ++i) {
    NSString *key = [keys objectAtIndex: i];
    if ([key isEqualToString: speciesName]) continue; // skip the species itself
    NSMutableArray *ruleList = [proposalReactions objectForKey: key];
    for (j = 0; j < [ruleList count]; ++j) {
      NSDictionary *rule = [ruleList objectAtIndex: j];
      NSArray *reactants = [rule objectForKey: @"reactants"];
      NSUInteger idx = [reactants indexOfObject: speciesName];
      if (idx != NSNotFound) {
        printf("found reactant: %s rule: %s\n", [speciesName UTF8String], [key UTF8String]);
        return YES;
      }
    }
  }
  
  return NO;
}

- (NSInteger)determineAcceptedModel
{
  int i, j;

  printf("determineAcceptedModel %ld\n", (unsigned long)[acceptedModels count]);

  // Go through the list of accepted models
  // and determine if current model is equal to one of them
  // so we properly evaluate expectation for model
  NSInteger found = NSNotFound;
  for (i = 0; i < [acceptedModels count]; ++i) {
    NSDictionary *aModel = [acceptedModels objectAtIndex: i];
    NSDictionary *reactions = [aModel objectForKey: @"reactions"];
    
    // must have same number of reactions
    printf("compare reaction count (%d): %ld == %ld?\n", i, (unsigned long)[reactions count], (unsigned long)[proposalReactions count]);
    if ([reactions count] != [proposalReactions count]) continue;

    BOOL same = YES;
    NSArray *keys = [proposalReactions allKeys];
    for (j = 0; j < [keys count]; ++j) {
      NSString *key = [keys objectAtIndex: j];
      NSArray *rl1 = [proposalReactions objectForKey: key];
      NSArray *rl2 = [reactions objectForKey: key];
      
      // same number of rules
      printf("compare rule count (%d): %ld == %ld?\n", i, (unsigned long)[rl1 count], (unsigned long)[rl2 count]);
      if ([rl1 count] != [rl2 count]) {
        same = NO;
        break;
      }
      
      // rule should have same name
      NSString *rn1 = [[rl1 objectAtIndex: 0] objectForKey: @"name"];
      NSString *rn2 = [[rl2 objectAtIndex: 0] objectForKey: @"name"];
      printf("compare rule name (%d): %s == %s?\n", i, [rn1 UTF8String], [rn2 UTF8String]);
      if (![rn1 isEqualToString: rn2]) {
        same = NO;
        break;
      }
    }
    
    if (same) {
      printf("Found equal model!\n");
      printf("accepted = %s\n", [[aModel description] UTF8String]);
      printf("current = %s\n", [[proposalReactions description] UTF8String]);
      found = i;
      break;
    }
  }
  
  return found;
}

- (BOOL)saveAcceptedModel: (NSDictionary *)theModel
{
  // save by making a copy through serialization
  NSData *xmlData;
  NSString *error = nil;
  NSMutableDictionary *d = [NSMutableDictionary dictionary];
  
  // save the reactions
  xmlData = [NSPropertyListSerialization dataFromPropertyList:proposalReactions
                                                       format:NSPropertyListXMLFormat_v1_0
                                             errorDescription:&error];
  if (!xmlData) {
    printf("ERROR: could not serialize reactions: %s\n", [error UTF8String]);
    [error release];
    return NO;
  }

  NSPropertyListFormat format;
  id plist = [NSPropertyListSerialization propertyListFromData:xmlData
                                           mutabilityOption:NSPropertyListImmutable
                                                     format:&format
                                           errorDescription:&error];
  if (!plist) {
    printf("ERROR: could not deserialize reactions: %s\n", [error UTF8String]);
    [error release];
    return NO;
  }
  [d setObject: plist forKey: @"reactions"];
  
  // save the parameter info
  xmlData = [NSPropertyListSerialization dataFromPropertyList:proposalReactionParameters
                                                       format:NSPropertyListXMLFormat_v1_0
                                             errorDescription:&error];
  if (!xmlData) {
    printf("ERROR: could not serialize parameter info: %s\n", [error UTF8String]);
    [error release];
    return NO;
  }
  
  plist = [NSPropertyListSerialization propertyListFromData:xmlData
                                              mutabilityOption:NSPropertyListImmutable
                                                        format:&format
                                              errorDescription:&error];
  if (!plist) {
    printf("ERROR: could not deserialize parameter info: %s\n", [error UTF8String]);
    [error release];
    return NO;
  }
  [d setObject: plist forKey: @"parameters"];
  
  // save the complete model specification
  xmlData = [NSPropertyListSerialization dataFromPropertyList:theModel
                                                       format:NSPropertyListXMLFormat_v1_0
                                             errorDescription:&error];
  if (!xmlData) {
    printf("ERROR: could not serialize model specification: %s\n", [error UTF8String]);
    [error release];
    return NO;
  }
  [d setObject: plist forKey: @"model"];

  // add to list of accepted models
  proposalIndex = [acceptedModels count];
  [acceptedModels addObject: d];
  
  // MCMC info for model
  NSMutableDictionary *settings = [NSMutableDictionary dictionary];
  [settings setObject: [NSNumber numberWithDouble: bestPosterior] forKey: @"posterior"];
  [settings setObject: bestParameters forKey: @"parameters"];
  [acceptedModelSettings addObject: settings];

  return YES;
}

- (BOOL)saveCurrentModel
{
  // release old copies
  if (prevProposalReactions) {
    [prevProposalReactions release];
    prevProposalReactions = nil;
  }
  if (prevProposalReactionParameters) {
    [prevProposalReactionParameters release];
    prevProposalReactionParameters = nil;
  }
  
  // save by making a copy through serialization
  NSString *error = nil;
  prevProposalIndex = proposalIndex;
  prevProposalTransitions = proposalTransitions;

  // save the reactions
  prevProposalReactions = [NSPropertyListSerialization dataFromPropertyList:proposalReactions
                                                       format:NSPropertyListXMLFormat_v1_0
                                             errorDescription:&error];
  if (!prevProposalReactions) {
    printf("ERROR: could not serialize reactions: %s\n", [[error description] UTF8String]);
    [error release];
    return NO;
  }
  [prevProposalReactions retain];
    
  // save the parameter info
  prevProposalReactionParameters = [NSPropertyListSerialization dataFromPropertyList:proposalReactionParameters
                                                       format:NSPropertyListXMLFormat_v1_0
                                             errorDescription:&error];
  if (!prevProposalReactionParameters) {
    printf("ERROR: could not serialize parameter info: %s\n", [[error description] UTF8String]);
    [error release];
    return NO;
  }
  [prevProposalReactionParameters retain];
  
  return YES;
}

- (BOOL)restorePreviousModel
{
  if (!prevProposalReactions) return NO;
  if (!prevProposalReactionParameters) return NO;
  
  // restore by deserialization from saved copy
  NSPropertyListFormat format;
  NSString *error = nil;
  proposalIndex = prevProposalIndex;
  proposalTransitions = prevProposalTransitions;

  proposalReactions =
  [NSPropertyListSerialization propertyListFromData:prevProposalReactions
                                   mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                             format:&format
                                   errorDescription:&error];
  if (!proposalReactions) {
    printf("ERROR: could not deserialize reactions: %s\n", [error UTF8String]);
    [error release];
    return NO;
  }
  [proposalReactions retain];

  proposalReactionParameters =
  [NSPropertyListSerialization propertyListFromData:prevProposalReactionParameters
                                   mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                             format:&format
                                   errorDescription:&error];
  if (!proposalReactionParameters) {
    printf("ERROR: could not deserialize parameters: %s\n", [error UTF8String]);
    [error release];
    return NO;
  }
  [proposalReactionParameters retain];
  
  return YES;
}

- (int)modelStructureChange: (int)aNum doModify: (BOOL)aFlag
{
  int i, j, k;
  int numTransitions = 0; // NOTE: transition #0 is not a valid transition, i.e. model stays the same
  
  // removing one of the existing rules
  for (i = 0; i < [geneList count]; ++i) {
    NSString *gene = [geneList objectAtIndex: i];
    NSUInteger geneSpeciesIdx = [speciesList indexOfObject: gene];
    NSArray *ruleList = [proposalReactions objectForKey: gene];
    for (j = 0; j < [ruleList count]; ++j) {
      NSDictionary *rule = [ruleList objectAtIndex: j];
      numTransitions += [[rule objectForKey: @"reactants"] count];

      // if modify then remove rule
      if (aFlag && (numTransitions >= aNum)) {
        int idx = numTransitions - aNum;
        printf("change: %s: remove: %d reactant: %s\n", [gene UTF8String], idx, [[[rule objectForKey: @"reactants"] objectAtIndex: idx] UTF8String]);
        [[rule objectForKey: @"reactants"] removeObjectAtIndex: idx];

        NSString *ruleName = [self determineRuleName: ruleList forSpecies: geneSpeciesIdx];
        [self reassign: ruleList withRuleName: ruleName];
        return 0;
      }
    }

    // remove a binding rule
    NSString *protein = [gene uppercaseString];
    for (j = 0; j < [metaboliteList count]; ++j) {
      NSString *metab = [metaboliteList objectAtIndex: j];
      NSString *complex = [NSString stringWithFormat: @"%@_%@", protein, metab];
      NSString *dimer = [NSString stringWithFormat: @"%@_%@_dimer", protein, metab];
      NSMutableArray *complexRuleList = [proposalReactions objectForKey: complex];

      // cannot remove if dimer is reactant in any rules
      if ([self doRulesContainReactant: dimer]) continue;

      // remove bind and decay rules for complex and dimer
      if ((complexRuleList) && ([complexRuleList count] > 0)) {
        ++numTransitions;
        if (aFlag && (numTransitions == aNum)) {
          printf("change: %s: remove binding/dimer rules: %s %s\n", [protein UTF8String], [metab UTF8String], [complex UTF8String]);

          NSMutableArray *ruleList = [proposalReactions objectForKey: complex];
          for (k = 0; k < [ruleList count]; ++k) {
            NSDictionary *rule = [ruleList objectAtIndex: j];
            NSString *ruleName = [rule objectForKey: @"name"];
            [proposalReactionParameters removeObjectForKey: ruleName];
          }
          [proposalReactions removeObjectForKey: complex];

          ruleList = [proposalReactions objectForKey: dimer];
          for (k = 0; k < [ruleList count]; ++k) {
            NSDictionary *rule = [ruleList objectAtIndex: j];
            NSString *ruleName = [rule objectForKey: @"name"];
            [proposalReactionParameters removeObjectForKey: ruleName];
          }
          [proposalReactions removeObjectForKey: dimer];

          return 0;
        }
      }
    }
  }
  printf("remove: %d\n", numTransitions);
  
  // add a gene regulation rule
  for (i = 0; i < [geneList count]; ++i) {
    int numIncoming = 0;
    NSString *gene = [geneList objectAtIndex: i];
    NSUInteger geneSpeciesIdx = [speciesList indexOfObject: gene];
    
    // construct list of possible regulator species
    NSMutableArray *proteinList = [NSMutableArray array];
    for (j = 0; j < [geneList count]; ++j)
      [proteinList addObject: [[geneList objectAtIndex: j] uppercaseString]];

    // add any bind/dimer species
    for (j = 0; j < [proteinList count]; ++j) {
      NSString *protein = [proteinList objectAtIndex: j];
      for (k = 0; k < [metaboliteList count]; ++k) {
        NSString *metab = [metaboliteList objectAtIndex: k];
        NSString *dimer = [NSString stringWithFormat: @"%@_%@_dimer", protein, metab];
        NSMutableArray *dimerRuleList = [proposalReactions objectForKey: dimer];
      
        // add to list if rule exists to make the species
        if ((dimerRuleList) && ([dimerRuleList count] > 0)) {
          [proteinList addObject: dimer];
        }
      }
    }

    // remove existing reactants from possibilities
    NSMutableArray *ruleList = [proposalReactions objectForKey: gene];
    for (j = 0; j < [ruleList count]; ++j) {
      NSDictionary *rule = [ruleList objectAtIndex: j];
      NSArray *reactants = [rule objectForKey: @"reactants"];
      [proteinList removeObjectsInArray: reactants];
      numIncoming += [reactants count];
    }
    printf("%s: possible reactants: %s\n", [gene UTF8String], [[proteinList description] UTF8String]);

    // if at maximum then cannot add any more reactants
    if (numIncoming >= maxIncoming) continue;

    // Each possible reactant can:
    // a) new independent rule -or- add to existing rule
    // b) activation -or- inhibition
    for (j = 0; j < [proteinList count]; ++j) {
      NSString *protein = [proteinList objectAtIndex: j];

      // new independent rule, activation
      ++numTransitions;
      if (aFlag && (numTransitions == aNum)) {
        printf("change: add new independent rule, activation: %s %s\n", [gene UTF8String], [protein UTF8String]);
        // {type=signal; name=r10; products=(gene); reactants=(PROTEIN); rate=Hill;}
        NSMutableDictionary *newRule = [NSMutableDictionary dictionary];
        [newRule setObject: @"signal" forKey: @"type"];
        [newRule setObject: [NSMutableArray arrayWithObject: gene] forKey: @"products"];
        [newRule setObject: [NSMutableArray arrayWithObject: protein] forKey: @"reactants"];
        [newRule setObject: @"Hill" forKey: @"rate"];
        //NSString *ruleName = [NSString stringWithFormat: @"r%d_11", i];
        [ruleList addObject: newRule];
        NSString *ruleName = [self determineRuleName: ruleList forSpecies: geneSpeciesIdx];
        //[newRule setObject: ruleName forKey: @"name"];
        [self reassign: ruleList withRuleName: ruleName];

        // parameters for reaction rule
        NSMutableDictionary *parameterInfo = [proposalReactionParameters objectForKey: ruleName];
        NSMutableArray *paramList;
        NSMutableDictionary *paramAttributes;
        if (!parameterInfo) {
          parameterInfo = [NSMutableDictionary dictionary];
          paramList = [NSMutableArray array];
          paramAttributes = [NSMutableDictionary dictionary];
          [parameterInfo setObject: paramList forKey: @"parameters"];
          [parameterInfo setObject: paramAttributes forKey: @"attributes"];
          [proposalReactionParameters setObject: parameterInfo forKey: ruleName];
        } else {
          paramList = [parameterInfo objectForKey: @"parameters"];
          paramAttributes = [parameterInfo objectForKey: @"attributes"];
        }
        
        NSString *paramName = [NSString stringWithFormat: @"%@_%@_pmax_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        NSMutableDictionary *pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [paramAttributes setObject: pa forKey: paramName];
        
        paramName = [NSString stringWithFormat: @"%@_%@_c_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [pa setObject: [NSNumber numberWithFloat: 0.01] forKey: @"min"];
        [pa setObject: [NSNumber numberWithFloat: 20.0] forKey: @"max"];
        [paramAttributes setObject: pa forKey: paramName];
        
        paramName = [NSString stringWithFormat: @"%@_%@_h_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: -1.0] forKey: @"value"];
        [paramAttributes setObject: pa forKey: paramName];
        return 0;
      }

      // new independent rule, inhibition
      ++numTransitions;
      if (aFlag && (numTransitions == aNum)) {
        printf("change: add new independent rule, inhibition: %s %s\n", [gene UTF8String], [protein UTF8String]);
        // {type=signal; name=r10; products=(gene); reactants=(PROTEIN); rate=Hill;}
        NSMutableDictionary *newRule = [NSMutableDictionary dictionary];
        [newRule setObject: @"signal" forKey: @"type"];
        [newRule setObject: [NSMutableArray arrayWithObject: gene] forKey: @"products"];
        [newRule setObject: [NSMutableArray arrayWithObject: protein] forKey: @"reactants"];
        [newRule setObject: @"Hill" forKey: @"rate"];
        //NSString *ruleName = [NSString stringWithFormat: @"r%d_11", i];
        [ruleList addObject: newRule];
        NSString *ruleName = [self determineRuleName: ruleList forSpecies: geneSpeciesIdx];
        //[newRule setObject: ruleName forKey: @"name"];
        [self reassign: ruleList withRuleName: ruleName];

        // parameters for reaction rule
        NSMutableDictionary *parameterInfo = [proposalReactionParameters objectForKey: ruleName];
        NSMutableArray *paramList;
        NSMutableDictionary *paramAttributes;
        if (!parameterInfo) {
          parameterInfo = [NSMutableDictionary dictionary];
          paramList = [NSMutableArray array];
          paramAttributes = [NSMutableDictionary dictionary];
          [parameterInfo setObject: paramList forKey: @"parameters"];
          [parameterInfo setObject: paramAttributes forKey: @"attributes"];
          [proposalReactionParameters setObject: parameterInfo forKey: ruleName];
        } else {
          paramList = [parameterInfo objectForKey: @"parameters"];
          paramAttributes = [parameterInfo objectForKey: @"attributes"];
        }

        NSString *paramName = [NSString stringWithFormat: @"%@_%@_pmax_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        NSMutableDictionary *pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [paramAttributes setObject: pa forKey: paramName];
        
        paramName = [NSString stringWithFormat: @"%@_%@_c_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [pa setObject: [NSNumber numberWithFloat: 0.01] forKey: @"min"];
        [pa setObject: [NSNumber numberWithFloat: 20.0] forKey: @"max"];
        [paramAttributes setObject: pa forKey: paramName];
        
        paramName = [NSString stringWithFormat: @"%@_%@_h_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [paramAttributes setObject: pa forKey: paramName];
        return 0;
      }

      // add as reactant to existing rule, activation
      numTransitions += [ruleList count];
      if (aFlag && (numTransitions >= aNum)) {
        printf("change: add existing rule, activation: %s %s\n", [gene UTF8String], [protein UTF8String]);
        int idx = numTransitions - aNum;
        NSDictionary *rule = [ruleList objectAtIndex: idx];
        //NSString *ruleName = [rule objectForKey: @"name"];
        NSMutableArray *reactants = [rule objectForKey: @"reactants"];
        [reactants addObject: protein];
        NSString *ruleName = [self determineRuleName: ruleList forSpecies: geneSpeciesIdx];
        [self reassign: ruleList withRuleName: ruleName];

        // parameters for reaction rule
        NSMutableDictionary *parameterInfo = [proposalReactionParameters objectForKey: ruleName];
        NSMutableArray *paramList = [parameterInfo objectForKey: @"parameters"];
        NSMutableDictionary *paramAttributes = [parameterInfo objectForKey: @"attributes"];
        [parameterInfo setObject: paramList forKey: @"parameters"];
        [parameterInfo setObject: paramAttributes forKey: @"attributes"];
        [proposalReactionParameters setObject: parameterInfo forKey: ruleName];
        
        NSString *paramName = [NSString stringWithFormat: @"%@_%@_pmax_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        NSMutableDictionary *pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [paramAttributes setObject: pa forKey: paramName];
        
        paramName = [NSString stringWithFormat: @"%@_%@_c_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [pa setObject: [NSNumber numberWithFloat: 0.01] forKey: @"min"];
        [pa setObject: [NSNumber numberWithFloat: 20.0] forKey: @"max"];
        [paramAttributes setObject: pa forKey: paramName];
        
        paramName = [NSString stringWithFormat: @"%@_%@_h_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: -1.0] forKey: @"value"];
        [paramAttributes setObject: pa forKey: paramName];
        return 0;
      }

      // add as reactant to existing rule, inhibition
      numTransitions += [ruleList count];
      if (aFlag && (numTransitions >= aNum)) {
        printf("change: add existing rule, inhibition: %s %s\n", [gene UTF8String], [protein UTF8String]);
        int idx = numTransitions - aNum;
        NSDictionary *rule = [ruleList objectAtIndex: idx];
        //NSString *ruleName = [rule objectForKey: @"name"];
        NSMutableArray *reactants = [rule objectForKey: @"reactants"];
        [reactants addObject: protein];
        NSString *ruleName = [self determineRuleName: ruleList forSpecies: geneSpeciesIdx];
        [self reassign: ruleList withRuleName: ruleName];
        
        // parameters for reaction rule
        NSMutableDictionary *parameterInfo = [proposalReactionParameters objectForKey: ruleName];
        NSMutableArray *paramList = [parameterInfo objectForKey: @"parameters"];
        NSMutableDictionary *paramAttributes = [parameterInfo objectForKey: @"attributes"];
        [parameterInfo setObject: paramList forKey: @"parameters"];
        [parameterInfo setObject: paramAttributes forKey: @"attributes"];
        [proposalReactionParameters setObject: parameterInfo forKey: ruleName];
        
        NSString *paramName = [NSString stringWithFormat: @"%@_%@_pmax_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        NSMutableDictionary *pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [paramAttributes setObject: pa forKey: paramName];
        
        paramName = [NSString stringWithFormat: @"%@_%@_c_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"value"];
        [pa setObject: [NSNumber numberWithFloat: 0.01] forKey: @"min"];
        [pa setObject: [NSNumber numberWithFloat: 20.0] forKey: @"max"];
        [paramAttributes setObject: pa forKey: paramName];
        
        paramName = [NSString stringWithFormat: @"%@_%@_h_%@", ruleName, gene, protein];
        [paramList addObject: paramName];
        pa = [NSMutableDictionary dictionary];
        [pa setObject: [NSNumber numberWithFloat: -1.0] forKey: @"value"];
        [paramAttributes setObject: pa forKey: paramName];
        return 0;
      }
    }
  }
  printf("add: %d\n", numTransitions);
  
  // add a binding rule
  for (i = 0; i < [geneList count]; ++i) {
    NSString *gene = [geneList objectAtIndex: i];
    NSString *protein = [gene uppercaseString];
    for (j = 0; j < [metaboliteList count]; ++j) {
      NSString *metab = [metaboliteList objectAtIndex: j];
      NSString *complex = [NSString stringWithFormat: @"%@_%@", protein, metab];
      NSString *dimer = [NSString stringWithFormat: @"%@_%@_dimer", protein, metab];
      NSUInteger complexIdx = [speciesList indexOfObject: complex];
      NSUInteger dimerIdx = [speciesList indexOfObject: dimer];

      // rules are dependent upon each other, need the complex first before can make dimer
      NSMutableArray *complexRuleList = [proposalReactions objectForKey: complex];
      NSMutableArray *dimerRuleList = [proposalReactions objectForKey: dimer];

      // add rule to bind complex and dimer
      if ((!complexRuleList) || ([complexRuleList count] == 0)) {
        ++numTransitions;
        if (aFlag && (numTransitions == aNum)) {
          printf("change: add new binding rule, complex: %s\n", [complex UTF8String]);
          // {type=mass; name=ruleName; products=(protein_metabolite); reactants=(protein, metabolite); rate=MassAction;}
          NSMutableDictionary *newRule = [NSMutableDictionary dictionary];
          [newRule setObject: @"mass" forKey: @"type"];
          [newRule setObject: [NSMutableArray arrayWithObject: complex] forKey: @"products"];
          [newRule setObject: [NSMutableArray arrayWithObjects: protein, metab, nil] forKey: @"reactants"];
          [newRule setObject: @"MassAction" forKey: @"rate"];
          if (!complexRuleList) {
            complexRuleList = [NSMutableArray array];
            [proposalReactions setObject: complexRuleList forKey: complex];
          }
          [complexRuleList addObject: newRule];
          NSString *ruleName = [self determineRuleName: complexRuleList forSpecies: complexIdx];
          //[self reassign: complexRuleList withRuleName: ruleName];
          [newRule setObject: ruleName forKey: @"name"];
          
          // parameters for binding rule
          NSMutableDictionary *parameterInfo = [proposalReactionParameters objectForKey: ruleName];
          NSMutableArray *paramList;
          NSMutableDictionary *paramAttributes;
          if (!parameterInfo) {
            parameterInfo = [NSMutableDictionary dictionary];
            paramList = [NSMutableArray array];
            paramAttributes = [NSMutableDictionary dictionary];
            [parameterInfo setObject: paramList forKey: @"parameters"];
            [parameterInfo setObject: paramAttributes forKey: @"attributes"];
            [proposalReactionParameters setObject: parameterInfo forKey: ruleName];
          } else {
            paramList = [parameterInfo objectForKey: @"parameters"];
            paramAttributes = [parameterInfo objectForKey: @"attributes"];
          }
          
          NSString *paramName = [NSString stringWithFormat: @"%@_k", ruleName];
          [paramList addObject: paramName];
          NSMutableDictionary *pa = [NSMutableDictionary dictionary];
          [pa setObject: [NSNumber numberWithFloat: 0.1] forKey: @"value"];
          [pa setObject: [NSNumber numberWithFloat: 0.01] forKey: @"min"];
          [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"max"];
          [paramAttributes setObject: pa forKey: paramName];

          // complex binding rule also needs a decay rule
          // {type=signal; name=ruleName; products=(protein_metabolite); reactants=(protein_metabolite); rate=MassAction;}
          newRule = [NSMutableDictionary dictionary];
          [newRule setObject: @"mass" forKey: @"type"];
          [newRule setObject: [NSMutableArray arrayWithObject: complex] forKey: @"products"];
          [newRule setObject: [NSMutableArray arrayWithObject: complex] forKey: @"reactants"];
          [newRule setObject: @"MassAction" forKey: @"rate"];
          NSString *decayRuleName = [NSString stringWithFormat: @"d%@", ruleName];
          [newRule setObject: decayRuleName forKey: @"name"];
          [complexRuleList addObject: newRule];

          // parameters for decay rule
          parameterInfo = [proposalReactionParameters objectForKey: decayRuleName];
          if (!parameterInfo) {
            parameterInfo = [NSMutableDictionary dictionary];
            paramList = [NSMutableArray array];
            paramAttributes = [NSMutableDictionary dictionary];
            [parameterInfo setObject: paramList forKey: @"parameters"];
            [parameterInfo setObject: paramAttributes forKey: @"attributes"];
            [proposalReactionParameters setObject: parameterInfo forKey: decayRuleName];
          } else {
            paramList = [parameterInfo objectForKey: @"parameters"];
            paramAttributes = [parameterInfo objectForKey: @"attributes"];
          }
          
          paramName = [NSString stringWithFormat: @"%@_k", decayRuleName];
          [paramList addObject: paramName];
          pa = [NSMutableDictionary dictionary];
          [pa setObject: [NSNumber numberWithFloat: -0.1] forKey: @"value"];
          [paramAttributes setObject: pa forKey: paramName];

          // rule for complex to bind into a dimer
          printf("change: add new binding rule, dimer: %s\n", [dimer UTF8String]);
          // {type=mass; name=ruleName; products=(protein_metabolite_dimer); reactants=(complex, complex); rate=MassAction;}
          newRule = [NSMutableDictionary dictionary];
          [newRule setObject: @"mass" forKey: @"type"];
          [newRule setObject: [NSMutableArray arrayWithObject: dimer] forKey: @"products"];
          [newRule setObject: [NSMutableArray arrayWithObjects: complex, complex, nil] forKey: @"reactants"];
          [newRule setObject: @"MassAction" forKey: @"rate"];
          if (!dimerRuleList) {
            dimerRuleList = [NSMutableArray array];
            [proposalReactions setObject: dimerRuleList forKey: dimer];
          }
          [dimerRuleList addObject: newRule];
          ruleName = [self determineRuleName: dimerRuleList forSpecies: dimerIdx];
          //[self reassign: dimerRuleList withRuleName: ruleName];
          [newRule setObject: ruleName forKey: @"name"];

          // parameters for binding rule
          parameterInfo = [proposalReactionParameters objectForKey: ruleName];
          if (!parameterInfo) {
            parameterInfo = [NSMutableDictionary dictionary];
            paramList = [NSMutableArray array];
            paramAttributes = [NSMutableDictionary dictionary];
            [parameterInfo setObject: paramList forKey: @"parameters"];
            [parameterInfo setObject: paramAttributes forKey: @"attributes"];
            [proposalReactionParameters setObject: parameterInfo forKey: ruleName];
          } else {
            paramList = [parameterInfo objectForKey: @"parameters"];
            paramAttributes = [parameterInfo objectForKey: @"attributes"];
          }
          
          paramName = [NSString stringWithFormat: @"%@_k", ruleName];
          [paramList addObject: paramName];
          pa = [NSMutableDictionary dictionary];
          [pa setObject: [NSNumber numberWithFloat: 0.1] forKey: @"value"];
          [pa setObject: [NSNumber numberWithFloat: 0.01] forKey: @"min"];
          [pa setObject: [NSNumber numberWithFloat: 1.0] forKey: @"max"];
          [paramAttributes setObject: pa forKey: paramName];

          // dimer binding rule also needs a decay rule
          // {type=signal; name=ruleName; products=(dimer); reactants=(dimer); rate=MassAction;}
          newRule = [NSMutableDictionary dictionary];
          [newRule setObject: @"mass" forKey: @"type"];
          [newRule setObject: [NSMutableArray arrayWithObject: dimer] forKey: @"products"];
          [newRule setObject: [NSMutableArray arrayWithObject: dimer] forKey: @"reactants"];
          [newRule setObject: @"MassAction" forKey: @"rate"];
          decayRuleName = [NSString stringWithFormat: @"d%@", ruleName];
          [newRule setObject: decayRuleName forKey: @"name"];
          [dimerRuleList addObject: newRule];
          
          // parameters for decay rule
          parameterInfo = [proposalReactionParameters objectForKey: decayRuleName];
          if (!parameterInfo) {
            parameterInfo = [NSMutableDictionary dictionary];
            paramList = [NSMutableArray array];
            paramAttributes = [NSMutableDictionary dictionary];
            [parameterInfo setObject: paramList forKey: @"parameters"];
            [parameterInfo setObject: paramAttributes forKey: @"attributes"];
            [proposalReactionParameters setObject: parameterInfo forKey: decayRuleName];
          } else {
            paramList = [parameterInfo objectForKey: @"parameters"];
            paramAttributes = [parameterInfo objectForKey: @"attributes"];
          }
          
          paramName = [NSString stringWithFormat: @"%@_k", decayRuleName];
          [paramList addObject: paramName];
          pa = [NSMutableDictionary dictionary];
          [pa setObject: [NSNumber numberWithFloat: -0.1] forKey: @"value"];
          [paramAttributes setObject: pa forKey: paramName];

          return 0;
        }
      }
    }
  }
  printf("bind: %d\n", numTransitions);

  // add a enzymatic reaction rule

  return numTransitions;
}

- (NSArray *)mcmcParametersForRule: (NSDictionary *)rule
{
  NSString *param;
  int k;

  NSString *ruleName = [rule objectForKey: @"name"];

  // skip base and decay rules
  unichar c = [ruleName characterAtIndex: 0];
  if (c == 'd') return nil;
  if (c == 'b') return nil;

  NSArray *pList = [rule objectForKey: @"products"];
  NSString *pName = [pList objectAtIndex: 0];
  NSArray *rList = [rule objectForKey: @"reactants"];
  NSArray *eList = [rule objectForKey: @"enzymes"];
  NSString *rType = [rule objectForKey: @"type"];
  NSString *rateFunction = [rule objectForKey: @"rate"];
  
  NSMutableArray *mcmcParams = [NSMutableArray array];

  // construct parameter list based on rule type
  if ([rType isEqualToString: @"signal"]) {
    
    if ([rateFunction isEqualToString: @"Hill"]) {
      for (k = 0; k < [rList count]; ++k) {
        param = [NSString stringWithFormat: @"%@_%@_c_%@",
                 ruleName, pName, [rList objectAtIndex: k]];
        [mcmcParams addObject: param];
      }
      
    } else if ([rateFunction isEqualToString: @"Constant"]) {
      param = [NSString stringWithFormat: @"%@_%@_c", ruleName, pName];
      [mcmcParams addObject: param];
      
    } else if ([rateFunction isEqualToString: @"MassAction"]) {
      param = [NSString stringWithFormat: @"%@_%@_k", ruleName, pName];
      [mcmcParams addObject: param];
      
      if (eList) {
        for (k = 0; k < [eList count]; ++k) {
          param = [NSString stringWithFormat: @"%@_%@_c_%@",
                   ruleName, pName, [eList objectAtIndex: k]];
          [mcmcParams addObject: param];          
        }
      }
    } else {
      printf("ERROR: Unknown rate function: %s\n", [rateFunction UTF8String]);
      return nil;
    }
    
  } else if ([rType isEqualToString: @"mass"]) {
    
    if ([rateFunction isEqualToString: @"Hill"]) {
      // TODO: need enzyme
      printf("TODO: need enzyme\n");
    } else if ([rateFunction isEqualToString: @"MassAction"]) {
      param = [NSString stringWithFormat: @"%@_k", ruleName];
      [mcmcParams addObject: param];          

      if (eList) {
        for (k = 0; k < [eList count]; ++k) {
          param = [NSString stringWithFormat: @"%@_c_%@",
                   ruleName, [eList objectAtIndex: k]];
          [mcmcParams addObject: param];          
        }
      }
    } else {
      printf("ERROR: Unknown rate function: %s\n", [rateFunction UTF8String]);
      return nil;
    }
    
  } else {
    printf("ERROR: undefined type for reaction\n");
    return nil;
  }

  return mcmcParams;
}

- proposedModelSpecification
{
  int i, j, k;
  NSError *error = nil;

  NSDictionary *newModel =
  [NSPropertyListSerialization propertyListWithData: xmlModel
                                            options: NSPropertyListMutableContainersAndLeaves
                                             format: nil
                                              error: &error];
  if (error) return nil;
  //printf("newModel = %s\n", [[newModel description] UTF8String]);

  NSArray *modelList = [newModel objectForKey: @"models"];
  if ([modelList count] != 1) {
    printf("ERROR: ReactionStructureMCMC only supports single model.\n");
    return nil;
  }
  
  NSString *modelName = [modelList objectAtIndex: 0];
  NSMutableDictionary *theModel = [newModel objectForKey: modelName];
  if (![[theModel objectForKey: @"type"] isEqualToString: @"DynamicReactionRuleModel"]) {
    printf("ERROR: ReactionStructureMCMC only supports DynamicReactionRuleModel for model type.\n");
    return nil;
  }

  // species
  [theModel setObject: speciesList forKey: @"species"];

  // reactions and parameters
  NSMutableArray *reactions = [NSMutableArray array];
  NSMutableArray *mcmcParams = [NSMutableArray array];
  NSMutableDictionary *parameterRanges = [NSMutableDictionary dictionary];
  for (i = 0; i < [speciesList count]; ++i) {
    NSString *species = [speciesList objectAtIndex: i];

    // base reactions
    NSArray *ruleList = [baseReactions objectForKey: species];
    [reactions addObjectsFromArray: ruleList];
    
    // base reaction parameters
    for (j = 0; j < [ruleList count]; ++j) {
      NSString *ruleName = [[ruleList objectAtIndex: j] objectForKey: @"name"];
      //printf("ruleName = %s\n", [ruleName UTF8String]);
      NSDictionary *parameterInfo = [baseReactionParameters objectForKey: ruleName];
      //printf("parameterInfo = %s\n", [[parameterInfo description] UTF8String]);
      NSArray *paramList = [parameterInfo objectForKey: @"parameters"];
      //printf("paramList = %s\n", [[paramList description] UTF8String]);
      NSArray *mcmcP = [parameterInfo objectForKey: @"mcmcParameters"];
      [mcmcParams addObjectsFromArray: mcmcP];
      NSDictionary *paramAttributes = [parameterInfo objectForKey: @"attributes"];

      for (k = 0; k < [paramList count]; ++k) {
        NSString *paramName = [paramList objectAtIndex: k];
        //printf("paramName = %s\n", [paramName UTF8String]);
        NSDictionary *pa = [paramAttributes objectForKey: paramName];
        //printf("pa = %s\n", [[pa description] UTF8String]);
        NSNumber *n = [pa objectForKey: @"value"];
        //printf("value = %f\n", [n floatValue]);
        [theModel setObject: n forKey: paramName];
      }
      for (k = 0; k < [mcmcP count]; ++k) {
        NSString *paramName = [mcmcP objectAtIndex: k];
        //printf("paramName = %s\n", [paramName UTF8String]);
        NSDictionary *pa = [paramAttributes objectForKey: paramName];
        //printf("pa = %s\n", [[pa description] UTF8String]);
        NSNumber *n = [pa objectForKey: @"min"];
        //printf("min = %f\n", [n floatValue]);
        [parameterRanges setObject: n forKey: [NSString stringWithFormat: @"%@_min", paramName]];
        n = [pa objectForKey: @"max"];
        //printf("max = %f\n", [n floatValue]);
        [parameterRanges setObject: n forKey: [NSString stringWithFormat: @"%@_max", paramName]];
      }
    }
    
    // proposal reactions
    ruleList = [proposalReactions objectForKey: species];
    if (!ruleList) continue;
    [reactions addObjectsFromArray: ruleList];

    // proposal reaction parameters
    for (j = 0; j < [ruleList count]; ++j) {
      NSDictionary *rule = [ruleList objectAtIndex: j];
      NSString *ruleName = [rule objectForKey: @"name"];
      //printf("ruleName = %s\n", [ruleName UTF8String]);
      NSDictionary *parameterInfo = [proposalReactionParameters objectForKey: ruleName];
      //printf("parameterInfo = %s\n", [[parameterInfo description] UTF8String]);
      NSArray *paramList = [parameterInfo objectForKey: @"parameters"];
      //printf("paramList = %s\n", [[paramList description] UTF8String]);
      NSArray *mcmcP = [self mcmcParametersForRule: rule];
      [mcmcParams addObjectsFromArray: mcmcP];
      NSDictionary *paramAttributes = [parameterInfo objectForKey: @"attributes"];
      
      for (k = 0; k < [paramList count]; ++k) {
        NSString *paramName = [paramList objectAtIndex: k];
        //printf("paramName = %s\n", [paramName UTF8String]);
        NSDictionary *pa = [paramAttributes objectForKey: paramName];
        //printf("pa = %s\n", [[pa description] UTF8String]);
        NSNumber *n = [pa objectForKey: @"value"];
        //printf("value = %f\n", [n floatValue]);
        [theModel setObject: n forKey: paramName];
      }
      for (k = 0; k < [mcmcP count]; ++k) {
        NSString *paramName = [mcmcP objectAtIndex: k];
        //printf("paramName = %s\n", [paramName UTF8String]);
        NSDictionary *pa = [paramAttributes objectForKey: paramName];
        //printf("pa = %s\n", [[pa description] UTF8String]);
        NSNumber *n = [pa objectForKey: @"min"];
        //printf("min = %f\n", [n floatValue]);
        [parameterRanges setObject: n forKey: [NSString stringWithFormat: @"%@_min", paramName]];
        n = [pa objectForKey: @"max"];
        //printf("max = %f\n", [n floatValue]);
        [parameterRanges setObject: n forKey: [NSString stringWithFormat: @"%@_max", paramName]];
      }
    }
  }
  [theModel setObject: reactions forKey: @"reactions"];
  [theModel setObject: parameterRanges forKey: @"parameterRanges"];

  // MCMC parameters
  NSMutableDictionary *mcmcObject = [newModel objectForKey: @"MCMC"];
  [mcmcObject setObject: mcmcParams forKey: @"mcmcParameters"];

  return newModel;
}

- (BOOL)setupParameters
{
  return YES;
}

- (void)bestIndividual:(NSNotification *)notification
{
  int i;
  NSDictionary *userInfo = [notification userInfo];
  double posterior = [[userInfo objectForKey: @"posterior"] doubleValue];
  int modelIndex = [[userInfo objectForKey: @"particle"] intValue];
  NSArray *mInstances = [modelSampler modelInstances];
  //id modelObject = [mInstances objectAtIndex: modelIndex];
  printf("bestIndividual (%d): posterior = %f\n", modelIndex, posterior);

  int nParticles = [mInstances count];
  int totalParameters = [samplerParameterNames count];
  double (*acceptParameters)[nParticles][totalParameters] = samplerAcceptParametersValues;

  for (i = 0; i < [samplerParameterNames count]; ++i) {
    printf("parameter %d: %s = %f\n", i, [[samplerParameterNames objectAtIndex: i] UTF8String], (*acceptParameters)[modelIndex][i]);
  }

  // save best parameters
  if (!bestParameters) bestParameters = [NSMutableDictionary new];
  if (posterior > bestPosterior) {
    bestPosterior = posterior;
    for (i = 0; i < [samplerParameterNames count]; ++i) {
      [bestParameters setObject: [NSNumber numberWithDouble: (*acceptParameters)[modelIndex][i]]
                                                     forKey: [samplerParameterNames objectAtIndex: i]];
    }    
  }
}

- (void)samplerParameterInfo:(NSNotification *)notification
{
  int i;
  printf("samplerParameterInfo\n");
  
  NSDictionary *userInfo = [notification userInfo];
  samplerParameterNames = [userInfo objectForKey: @"names"];
  samplerParameterIndexes = [userInfo objectForKey: @"indexes"];
  samplerParameterModelNumbers = [userInfo objectForKey: @"modelNumbers"];
  samplerParameterModelKeys = [userInfo objectForKey: @"modelKeys"];
  samplerParameterMinRange = [[userInfo objectForKey: @"minRanges"] pointerValue];
  samplerParameterMaxRange = [[userInfo objectForKey: @"maxRanges"] pointerValue];
  samplerAcceptParametersValues = [[userInfo objectForKey: @"acceptValues"] pointerValue];
  samplerRejectParametersValues = [[userInfo objectForKey: @"rejectValues"] pointerValue];

  for (i = 0; i < [samplerParameterNames count]; ++i) {
    printf("parameter %d: %s\n", i, [[samplerParameterNames objectAtIndex: i] UTF8String]);
  }
}

- (BOOL)proposeMove
{
  // randomly pick a transition, equal probability for each
  proposalTransitions = [self modelStructureChange: 0 doModify: NO];
  if (proposalTransitions == 0) {
    printf("ERROR: Zero model structure transitions.\n");
    return NO;
  }
  int transitionNum = ([rnGen doubleValue] * (proposalTransitions - 1)) + 1; // shift to 1-N, zero not valid

  //transitionNum = 3;
  printf("propose move %d of %ld\n", transitionNum, (long)proposalTransitions);
  [self modelStructureChange: transitionNum doModify: YES];
  
  printf("proposalReactionParameters = \n%s\n", [[proposalReactionParameters description] UTF8String]);

  return YES;
}

- (void)performMCMCWithModel: (NSDictionary *)mcmcModel setNumber: (int)setNum
{
  printf("performMCMCWithModel:setNumber:\n");
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  double posteriorDensity, newPosteriorDensity;
  double posteriorTransitions, newPosteriorTransitions;

  // Get initial set of reaction rules from base model
  proposalTransitions = [self modelStructureChange: 0 doModify: NO];
  printf("transitions = %ld\n", (long)proposalTransitions);
  NSDictionary *theModel = [self proposedModelSpecification];
  if (!theModel) return;
  printf("model = %s\n", [[theModel description] UTF8String]);
  
  // register for notifications
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(bestIndividual:)
                                               name:BioSwarmBestMCMCIndividualNotification object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(samplerParameterInfo:)
                                               name:BioSwarmMCMCParameterInfoNotification object:nil];

  // Run initial SMCSampler, and get parameter set with best posterior probability
  bestPosterior = -INFINITY;
  modelSampler = [[SMCSampler alloc] initWithModel: theModel andManager: simManager];
  if (!modelSampler) {
    printf("ERROR: ReactionStructureMCMC could not create SMCSampler object.\n");
    return;
  }

  [modelSampler setDelegate: delegate];
  [modelSampler setGPUFunctions: gpuFuncs];
  [modelSampler setGPUDevice: gpuDevice];
  [modelSampler performMCMC];
  [modelSampler release];

  // save model
  [self saveAcceptedModel: theModel];
  [self saveCurrentModel];
  posteriorDensity = bestPosterior;
  posteriorTransitions = (double)proposalTransitions;

  // Monte Carlo iterations
  int mcIter, mcTot = networkEquilibration + networkSamples;
  for (mcIter = 1; mcIter <= mcTot; ++mcIter) {
    [pool release];
    pool = [NSAutoreleasePool new];
        
    // propose move
    if (![self proposeMove]) {
      printf("ERROR: Could not propose move.\n");
      return;
    }
    theModel = [self proposedModelSpecification];
    if (!theModel) return;
    printf("model = %s\n", [[theModel description] UTF8String]);

    // perform simulation
    bestPosterior = -INFINITY;
    modelSampler = [[SMCSampler alloc] initWithModel: theModel andManager: simManager];
    if (!modelSampler) {
      printf("ERROR: ReactionStructureMCMC could not create SMCSampler object.\n");
      return;
    }
    
    [modelSampler setDelegate: delegate];
    [modelSampler setGPUFunctions: gpuFuncs];
    [modelSampler setGPUDevice: gpuDevice];
    [modelSampler performMCMC];
    [modelSampler release];
    
    newPosteriorDensity = bestPosterior;
    posteriorTransitions = proposalTransitions;
    newPosteriorTransitions = (double)[self modelStructureChange: 0 doModify: NO];
    
    // accept or reject
    double acceptanceProb = exp(newPosteriorDensity - posteriorDensity) * posteriorTransitions / newPosteriorTransitions;
    printf("newPosteriorDensity / posteriorDensity = acceptanceProb = exp(%e - %e) * %f / %f = %f\n",
                               newPosteriorDensity, posteriorDensity, posteriorTransitions, newPosteriorTransitions, acceptanceProb);
    BOOL acceptFlag = NO;
    if ([rnGen doubleValue] <= acceptanceProb) {
      printf("accept\n");
      acceptFlag = YES;
    } else {
      printf("reject\n");
    }

    // take new model if accepted
    if (acceptFlag) {
      NSInteger idx = [self determineAcceptedModel];
      if (idx == NSNotFound) {
        if (![self saveAcceptedModel: theModel]) {
          printf("ERROR: Could not save accepted model.\n");
          return;
        }
      }
      // save new model
      proposalTransitions = newPosteriorTransitions;
      posteriorDensity = newPosteriorDensity;
      [self saveCurrentModel];
    } else {
      // restore the previous saved model
      [self restorePreviousModel];
      posteriorDensity = [[[acceptedModelSettings objectAtIndex: proposalIndex] objectForKey: @"posterior"] doubleValue];
    }
    
    // evalulate expectation function
    NSMutableDictionary *d = [acceptedModelSettings objectAtIndex: proposalIndex];
    NSNumber *n = [d objectForKey: @"expectation"];
    if (!n) n = [NSNumber numberWithInt: 1];
    else n = [NSNumber numberWithInt: [n intValue] + 1];
    [d setObject: n forKey: @"expectation"];

    printf("settings = %s\n", [[acceptedModelSettings description] UTF8String]);

    // save best model
    
  }

  [pool release];
}

@end
