/*
 StochasticReactionModel.m
 
 Stochastic chemical reaction model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Sept. 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "StochasticReactionModel.h"
#import <Swarm/simtools.h>
#import <Swarm/random.h>
#import <Swarm/MT19937gen.h>


@implementation StochasticReactionModel

- initWithModel: (NSDictionary *)aModel
{
  int i, j;
  
  [super init];
  
  // setup species
  species = [aModel objectForKey: @"species"];
  numOfSpecies = [species count];
  NSLog(@"%d species", numOfSpecies);
  
  X = malloc(sizeof(long) * numOfSpecies);
  for (i = 0; i < numOfSpecies; ++i) X[i] = 0;
  
  // setup reactions
  reactions = [aModel objectForKey: @"reactions"];
  numOfReactions = [reactions count];
  NSLog(@"%d reactions", numOfReactions);
  
  reactionPreConditions = malloc(sizeof(int *) * numOfReactions);
  numPreConditions = malloc(sizeof(int) * numOfReactions);
  reactionPostConditions = malloc(sizeof(int *) * numOfReactions);
  numPostConditions = malloc(sizeof(int) * numOfReactions);
  rate = malloc(sizeof(double) * numOfReactions);
  
  for (i = 0; i < numOfReactions; ++i) {
    NSDictionary *r = [reactions objectAtIndex: i];
    
    NSArray *a = [r objectForKey: @"pre"];
    numPreConditions[i] = [a count];
    if (numPreConditions[i]) reactionPreConditions[i] = malloc(sizeof(int) * numPreConditions[i]);
    else reactionPreConditions[i] = NULL;
    for (j = 0; j < numPreConditions[i]; ++j) {
      NSString *s = [a objectAtIndex: j];
      NSUInteger idx = [species indexOfObject: s];
      if (idx == NSNotFound) {
        NSLog(@"Species (%@) referenced in reaction, not in species list", s); abort();
      } else reactionPreConditions[i][j] = idx;
    }
    
    a = [r objectForKey: @"post"];
    numPostConditions[i] = [a count];
    if (numPostConditions[i]) reactionPostConditions[i] = malloc(sizeof(int) * numPostConditions[i]);
    else reactionPostConditions[i] = NULL;
    for (j = 0; j < numPostConditions[i]; ++j) {
      NSString *s = [a objectAtIndex: j];
      NSUInteger idx = [species indexOfObject: s];
      if (idx == NSNotFound) {
        NSLog(@"Species (%@) referenced in reaction, not in species list", s); abort();
      } else reactionPostConditions[i][j] = idx;
    }
    
    rate[i] = [[r objectForKey: @"rate"] doubleValue];
  }
  
  // initial conditions
  NSDictionary *d = [aModel objectForKey: @"initial"];
  NSArray *a = [d allKeys];
  for (i = 0; i < [a count]; ++i) {
    NSString *s = [a objectAtIndex: i];
    NSUInteger idx = [species indexOfObject: s];
    if (idx == NSNotFound) {
      NSLog(@"Species (%@) referenced in initial, not in species list", s); abort();
    } else X[idx] = [[d objectForKey: s] intValue];
  }
  
  return self;
}

- (void)runSimToTime: (double)stopTime
{
  int i, j;
  
  id RNG = [MT19937gen create: globalZone setStateFromSeed: DEFAULTSEED];
  id randomDouble = [UniformDoubleDist create: globalZone setGenerator: RNG];
  id randomExp = [ExponentialDist create: globalZone setGenerator: RNG];

  printf("0.0 ");
  for (i = 0; i < numOfSpecies; ++i) printf("%d ", X[i]);
  printf("\n");
  
  // do the simulation
  double t = 0.0;
  double rt = 1.0;
  double hazard[numOfReactions];
  while (t < stopTime) {
    
    // calculate hazards
    double totHazard = 0.0;
    for (i = 0; i < numOfReactions; ++i) {
      switch (numPreConditions[i]) {
        case 0:
          // zero order
          hazard[i] = rate[i];
          break;
        case 1:
          // first order
          hazard[i] = rate[i] * (double)X[reactionPreConditions[i][0]];
          break;
        case 2:
          // second order
          if (reactionPreConditions[i][0] == reactionPreConditions[i][1])
            hazard [i] = rate[i] * 0.5 * (double)X[reactionPreConditions[i][0]] * (double)(X[reactionPreConditions[i][1]] - 1);
          else hazard[i] = rate[i] * (double)X[reactionPreConditions[i][0]] * (double)X[reactionPreConditions[i][1]];
          break;
      }
      
      totHazard += hazard[i];
    }
    
    // no reactions possible
    if (totHazard == 0.0) {
      NSLog(@"No reactions possible");
      break;
    }
    
    // advance time
    double nt = [randomExp getSampleWithMean: (1.0 / totHazard)];
    t += nt;
    
    // perform the reaction
    double r = [randomDouble getDoubleWithMin: 0.0 withMax: totHazard];
    totHazard = 0.0;
    for (i = 0; i < numOfReactions; ++i) {
      if (hazard[i] == 0.0) continue;
      
      totHazard += hazard[i];
      if (r <= totHazard) {
        for (j = 0; j < numPreConditions[i]; ++j) --X[reactionPreConditions[i][j]];
        for (j = 0; j < numPostConditions[i]; ++j) ++X[reactionPostConditions[i][j]];
        //printf("%lf %d: ", t, i);
        break;
      }
    }
    
    if (t >= rt) {
      printf("%lf ", t);
      for (i = 0; i < numOfSpecies; ++i) printf("%d ", X[i]);
      printf("\n");
      rt += 1.0;
    }
  }
}

@end
