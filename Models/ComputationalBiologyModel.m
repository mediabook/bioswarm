/*
 ComputationalBiologyModel.m
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "ComputationalBiologyModel.h"

#import "SimulationController.h"
#import "internal.h"

@implementation ComputationalBiologyModel

- initWithModel: (NSDictionary *)aModel andController: anObj
{
  return [self initWithModel: aModel andController: anObj andParent: nil];
}

- initWithModel: (NSDictionary *)aModel andController: anObj andParent: aParent
{
  id s;
  
  [super init];
  
  delegate = nil;
  modelController = [anObj retain];
  model = [aModel retain];
  parentModel = [aParent retain];
  childModels = [NSMutableArray new];
  spatialContext = [aModel objectForKey: @"spatialContext"];
  //printf("parent child spatial: %p %p %p\n", parentModel, childModels, spatialContext);
  
  isDebug = NO;
  s = [aModel objectForKey: @"debug"];
  if (s) isDebug = [s boolValue];
  
  isBinaryFile = [modelController isBinaryFile];
  s = [aModel objectForKey: @"binaryFile"];
  if (s) isBinaryFile = [s boolValue];
  
  name = [model objectForKey: @"name"];
  if (!name) name = [modelController modelName];
  CHECK_PARAM(name, "name");

  runNumber = [modelController outputRun];
  
  paramMin = NULL;
  paramMax = NULL;
  
  return self;
}

- newFromModelIndividual
{
  return [[[self class] alloc] initWithModel: model];
}

- (void)dealloc
{
  if (paramMin) free(paramMin);
  if (paramMax) free(paramMax);
  [model release];
  [modelController release];
  [parentModel release];
  [childModels release];
  
  [super dealloc];
}

- (NSDictionary *)model { return model; }
- (NSString *)modelName { return name; }
- (id)modelController { return modelController; }
- (id)parentModel { return parentModel; }
- (NSMutableArray *)childModels { return childModels; }
- (void)addChildModel: aModel { [childModels addObject: aModel]; }

- (SpatialModel *)spatialModel
{
  // by default, ask our parent, if no parent then ask controller
  if (parentModel) return [parentModel spatialModel];
  else return [modelController spatialModel];
}
- (SpatialModel *)spatialModelForContext: aModel
{
  if (!spatialContext) return [self spatialModel];
  else {
    NSUInteger i = [childModels indexOfObject: aModel];
    if (i == NSNotFound) {
      printf("ERROR: attempt to get spatial model for invalid context.\n");
      return nil;
    }
    NSString *s = [spatialContext objectAtIndex: i];
    printf("spatial context: %d %s\n", i, [s UTF8String]);
    if ([s isEqualToString: @"parent"]) return [parentModel spatialModel];
    else if ([s isEqualToString: @"nil"]) return nil;
  }
  return nil;
}

- delegate { return delegate; }
- (void)setDelegate: anObj { delegate = anObj; }

- (int)numOfSpecies { return 0; }
- (NSString *)nameOfSpecies: (int)theSpecies { return nil; }
- (id)valueOfSpecies: (int)theSpecies { return nil; }

- (BOOL)isDebug
{
  // the controller may globally turn on debugging
  if (!isDebug) return [modelController isDebug];
  return isDebug;
}
- (void)setDebug: (BOOL)aFlag { isDebug = aFlag; }

- (BOOL)isBinaryFile { return isBinaryFile; }
- (void)setBinaryFile: (BOOL)aFlag { isBinaryFile = aFlag; }

- (void)setRunNumber: (int)aNum { runNumber = aNum; }
- (int)runNumber { return runNumber; }

#if 0
#pragma mark == MANAGE MODELS AND SIMULATIONS ==
#endif

//
// pass messages down the model hierarchy
//

- (int)numModelsInHierarchy
{
  int i, cnt = 1;
  for (i = 0; i < [childModels count]; ++i)
    cnt += [[childModels objectAtIndex: i] numModelsInHierarchy];
  return cnt;
}

- (void)allocateHierarchyWithEncode:(NSString *)anEncode
{
  int i;
  [self allocateSimulationWithEncode: anEncode];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] allocateHierarchyWithEncode: anEncode];
}

- (void)initializeHierarchy
{
  int i;
  [self initializeSimulation];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] initializeHierarchy];
}

- (BOOL)writeHierarchyDataWithCheck: (BOOL)aFlag
{
  int i;
  BOOL res = YES;
  res &= [self writeDataWithCheck: aFlag];
  for (i = 0; i < [childModels count]; ++i)
    res &= [[childModels objectAtIndex: i] writeHierarchyDataWithCheck: aFlag];
  return res;
}

- (BOOL)readHierarchyData
{
  int i;
  BOOL res = YES;
  res &= [self readData];
  for (i = 0; i < [childModels count]; ++i)
    res &= [[childModels objectAtIndex: i] readHierarchyData];
  return res;
}

- (void)rewindHierarchyFiles
{
  int i;
  [self rewindFiles];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] rewindHierarchyFiles];
}

- (void)runSimulation
{
}

- (void)cleanupHierarchy
{
  int i;
  [self cleanupSimulation];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] cleanupHierarchy];
}

//
// These should be implemented by subclasses
//

- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState
{
}

- (void)initializeSimulation
{
}

- (BOOL)writeDataWithCheck: (BOOL)aFlag
{
  return YES;
}

- (BOOL)readData
{
  return YES;
}

- (void)rewindFiles
{
}

- (void)cleanupSimulation
{
}

//
// Parameter operations
//
- (int)numOfParameters
{
  return 0;
}

- (NSString *)nameOfParameter: (int)theParam
{
  return nil;
}

- (BOOL)setupParameterRanges: (NSDictionary *)ranges doCheck: (BOOL)aFlag
{
  id paramRanges;
  BOOL good = YES;
  
  if (!ranges) {
    paramRanges = [model objectForKey: @"parameterRanges"];
    CHECK_PARAMF(paramRanges, "parameterRanges", good);
    if (!good) return good;
    
  } else paramRanges = ranges;
  
  // how many parameters
  int i, numParams = [self numOfParameters];
  
  // if no parameters then no ranges to setup
  if (numParams == 0) return NO;
  
  // Specify which parameters to randomize?                                                                                                                
  //NSArray *randomParameters = [modelController randomParameters];
  
  // Specify which parameters not to randomize?                                                                                                            
  //NSArray *notRandomParameters = [modelController notRandomParameters];
  
  if (paramMin) free(paramMin);
  if (paramMax) free(paramMax);
  paramMin = malloc(sizeof(double) * numParams);
  paramMax = malloc(sizeof(double) * numParams);
  BOOL flag = YES;
  for (i = 0; i < numParams; ++i) {
    NSString *s = [self nameOfParameter: i];
    
    //if (randomParameters && ([randomParameters indexOfObject: s] == NSNotFound)) continue;
    //if (notRandomParameters && ([notRandomParameters indexOfObject: s] != NSNotFound)) continue;
    
    NSString *paramName = [NSString stringWithFormat: @"%@_min", s];
    id param = [paramRanges objectForKey: paramName];
    if (aFlag) CHECK_PARAMF(param, [paramName UTF8String], flag);
    paramMin[i] = [param doubleValue];
    
    paramName = [NSString stringWithFormat: @"%@_max", s];
    param = [paramRanges objectForKey: paramName];
    if (aFlag) CHECK_PARAMF(param, [paramName UTF8String], flag);
    paramMax[i] = [param doubleValue];
  }
  
  return flag;
}

- (BOOL)setupParameterRanges: (NSDictionary *)ranges checkNames: (NSArray *)names
{
  id paramRanges;
  BOOL good = YES;
  
  if (!ranges) {
    paramRanges = [model objectForKey: @"parameterRanges"];
    CHECK_PARAMF(paramRanges, "parameterRanges", good);
    if (!good) return good;
    
  } else paramRanges = ranges;
  
  // how many parameters
  int i, numParams = [self numOfParameters];
  
  // if no parameters then no ranges to setup
  if (numParams == 0) return NO;
  
  if (paramMin) free(paramMin);
  if (paramMax) free(paramMax);
  paramMin = malloc(sizeof(double) * numParams);
  paramMax = malloc(sizeof(double) * numParams);
  BOOL flag = YES;
  for (i = 0; i < numParams; ++i) {
    NSString *s = [self nameOfParameter: i];
    
    NSString *paramName = [NSString stringWithFormat: @"%@_min", s];
    id param = [paramRanges objectForKey: paramName];
    if (names) {
      NSUInteger idx = [names indexOfObject: s];
      if (idx != NSNotFound) CHECK_PARAMF(param, [paramName UTF8String], flag);
    }
    paramMin[i] = [param doubleValue];
    
    paramName = [NSString stringWithFormat: @"%@_max", s];
    param = [paramRanges objectForKey: paramName];
    if (names) {
      NSUInteger idx = [names indexOfObject: s];
      if (idx != NSNotFound) CHECK_PARAMF(param, [paramName UTF8String], flag);
    }
    paramMax[i] = [param doubleValue];
  }
  
  return flag;
}

- (double *)parameterMinRange { return paramMin; }
- (double *)parameterMaxRange { return paramMax; }

static int determine_scale(double value)
{
  double v;
  int scale = 0;
  
  if (value < 0) v = -value;
  else v = value;
  
  if (v == 0.0) return scale;
  
  if (v >= 10) {
    while (v >= 10) {
      v = v / 10;
      ++scale;
    }
  } else {
    while (v < 1) {
      v = v * 10;
      --scale;
    }
  }
  
  //NSLog(@"%lf %d\n", value, scale);
  return scale;
}

- (double)randomValueforParameter: (int)aParam
{
  if ((aParam < 0) || (aParam >= [self numOfParameters])) {
    printf("ERROR: Invalid parameter index: %d\n", aParam);
    return 0.0;
  }
  
  // check if parameter ranges are setup
  if (!paramMin)
    if (![self setupParameterRanges: nil doCheck: NO]) {
      printf("ERROR: Could not setup parameter ranges.\n");
      return 0.0;
    }
  if (paramMin[aParam] == paramMax[aParam]) {
    printf("ERROR: Invalid parameter range defined for %s.\n", [[self nameOfParameter: aParam] UTF8String]);
    return 0.0;
  }
      
  // determine scales of range
  int minScale = determine_scale(paramMin[aParam]);
  int maxScale = determine_scale(paramMax[aParam]);
  
  int signCheck = 0;
  if ((paramMin[aParam] < 0) && (paramMax[aParam] < 0)) signCheck = 1;
  if (((paramMin[aParam] < 0) && (paramMax[aParam] >= 0))
      || ((paramMin[aParam] >= 0) && (paramMax[aParam] < 0))) signCheck = 2;
  
  double newValue = 0.0;
  if (minScale == maxScale) {
    double delta = paramMax[aParam] - paramMin[aParam];
    newValue = RAND_NUM * delta + paramMin[aParam];
  } else {
    do {
      double deltaScale = maxScale - minScale;
      double newScale = RAND_NUM * deltaScale + minScale;
      newScale = round(newScale);
      //printf("newScale: %lf %d %d %lf\n", deltaScale, minScale, maxScale, newScale);
      //double minVal = paramMin[aParam] / pow(10.0, minScale);
      //double maxVal = paramMax[aParam] / pow(10.0, maxScale);
      //double delta = maxVal - minVal;
      //newValue = RAND_NUM * delta + minVal;
      newValue = RAND_NUM;
      if (signCheck == 1) newValue = -newValue;
      if ((signCheck == 2) && (RAND_NUM < 0.5)) newValue = -newValue;
      //printf("newVal: %lf %lf %lf %lf\n", delta, minVal, maxVal, newValue);
      newValue = newValue * pow(10.0, newScale);
    } while ((newValue < paramMin[aParam]) || (newValue > paramMax[aParam]));
  }
  
  return newValue;
}

- (double)perturbValueForParameter: (int)aParam withinRange: (BOOL)aFlag standardDeviation: (double)SD
{
  if ((aParam < 0) || (aParam >= [self numOfParameters])) {
    printf("ERROR: Invalid parameter index: %d\n", aParam);
    return 0.0;
  }
  
  double origValue = [[model objectForKey: [self nameOfParameter: aParam]] doubleValue];
  //NSLog(@"origValue = %lf", origValue);
  double newValue = origValue;
  
  if (aFlag) {
    // check if parameter ranges are setup
    if (!paramMin)
      if (![self setupParameterRanges: nil doCheck: NO]) {
        printf("ERROR: Could not setup parameter ranges.\n");
        return 0.0;
      }
    
    do {
      newValue = origValue + normal_dist_rand(0, SD);
    } while ((newValue < paramMin[aParam]) || (newValue > paramMax[aParam]));
  } else {
    // don't worry about range, just keep same sign
    BOOL isPositive = YES;
    if (origValue < 0) isPositive = NO;
    
    do {
      newValue = origValue + normal_dist_rand(0, SD);
    } while (((newValue < 0) && isPositive) || ((newValue >= 0) && !isPositive));
  }
  
  return newValue;
}

- (int)modifyRandomParameter: (NSMutableDictionary *)params
{
  // check if parameter ranges are setup
  if (!paramMin)
    if (![self setupParameterRanges: nil doCheck: NO]) {
      printf("ERROR: Could not setup parameter ranges.\n");
      return -1;
    }
  
  // how many parameters
  int numParams = [self numOfParameters];
  
  int rp = (int)(RAND_NUM * numParams);
  NSString *ps = [self nameOfParameter: rp];
  
  if (!ps) printf("ERROR: Could not randomly pick parameter: %d %d\n", numParams, rp);
  else {
    double delta = paramMax[rp] - paramMin[rp];
    double pv = [[params objectForKey: ps] doubleValue];
    double newpv = RAND_NUM * delta + paramMin[rp];
    
#if 0
    double scale = determine_scale(pv);
    BOOL done = NO;
    while (!done) {
      newpv = normal_dist_rand(pv, scale);
      // maintain the same sign
      if ((pv < 0) && (newpv < 0)) done = YES;
      if ((pv >= 0) && (newpv >= 0)) done = YES;
    }
#endif
#if 0
    if (fabs(pv) < 0.001) {
      if (pv >= 0) newpv = RAND_NUM;
      else newpv = - RAND_NUM;
    } else {
      if (RAND_NUM < 0.5) newpv = pv + (RAND_NUM * pv);
      else newpv = pv - (RAND_NUM * pv);
    }
#endif
    
    [params setObject: [NSNumber numberWithDouble: newpv] forKey: ps];
    printf("change parameter %s(%d): %lf -> %lf\n", [ps UTF8String], rp, pv, newpv);
  }
  
  return rp;
}

// perturbation operations
- (BOOL)setValueForAllSpecies: (double)aValue { return NO; }
- (BOOL)setValue: (double)aValue forSpecies: (NSString *)speciesName { return NO; }
- (BOOL)adjustValue: (double)aValue forSpecies: (NSString *)speciesName { return NO; }
- (BOOL)knockoutSpecies: (NSString *)speciesName { return NO; }

@end

//
// GPU
//

@implementation ComputationalBiologyModel (GPU)
- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName { return nil; }
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName { return nil; }
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName { return nil; }
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName { return nil; }

@end

//
// Run GPU
//

@implementation ComputationalBiologyModel (GPURun)

- (void)allocateHierarchy: (int *)modelIndex GPUData: (ModelGPUData **)gpuData modelInstances: (int)numInstances
{
  // allocate GPU data for ourself
  getGPUFunctions *func = [modelController gpuFunctions];
  void *gpuFunctions = (func)(*modelIndex, 0);
  gpuData[*modelIndex] = [self allocGPUData: numInstances withGPUFunctions: gpuFunctions];
  [self allocGPUKernel: gpuData[*modelIndex]];
  ++(*modelIndex);

  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] allocateHierarchy: modelIndex GPUData: gpuData modelInstances: numInstances];
}

- (void)freeHierarchy: (int *)modelIndex GPUData: (ModelGPUData **)gpuData
{
  // free GPU data for ourself
  [self freeGPUData: gpuData[*modelIndex]];
  ++(*modelIndex);
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] freeHierarchy: modelIndex GPUData: gpuData];
}

- (void)assignHierarchyData: (int *)modelIndex GPUData: (ModelGPUData **)gpuData ofInstance: (int)aNum toGPU: (BOOL)aFlag
{
  // assign GPU data for ourself
  [self assignData: gpuData[*modelIndex] ofNumber: aNum toGPU: aFlag];
  ++(*modelIndex);

  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] assignHierarchyData: modelIndex GPUData: gpuData ofInstance: aNum toGPU: aFlag];
}

- (void)assignHierarchyParameters: (int *)modelIndex GPUData: (ModelGPUData **)gpuData ofInstance: (int)aNum toGPU: (BOOL)aFlag
{
  // assign GPU data for ourself
  [self assignParameters: gpuData[*modelIndex] ofNumber: aNum toGPU: aFlag];
  ++(*modelIndex);
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] assignHierarchyParameters: modelIndex GPUData: gpuData ofInstance: aNum toGPU: aFlag];
}

- (void)transferHierarchy: (int *)modelIndex GPUData: (ModelGPUData **)gpuData toGPU: (BOOL)aFlag
{
  // transfer GPU data for ourself
  [self transferGPUKernel: gpuData[*modelIndex] toGPU: aFlag];
  ++(*modelIndex);

  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] transferHierarchy: modelIndex GPUData: gpuData toGPU: aFlag];
}

- (void)releaseHierarchy: (int *)modelIndex GPUData: (ModelGPUData **)gpuData
{
  // call release kernel for ourself
  [self releaseGPUKernel: gpuData[*modelIndex]];
  ++(*modelIndex);
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] releaseHierarchy: modelIndex GPUData: gpuData];
}

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions { return NULL; }
- (void)freeGPUData: (void *)data { }
- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag { }
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag { return 0; }
- (void)allocGPUKernel: (void *)data { }
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag { }
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime { }
- (void)releaseGPUKernel: (void *)data { }

@end

//
// Visualization
//

@implementation ComputationalBiologyModel (Povray)

- (NSMutableString *)povrayCode
{
  return nil;
}

@end
