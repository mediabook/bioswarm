/*
 AggregatePopulationModel.m
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: July 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "AggregatePopulationModel.h"
#import "BioSwarmController.h"
#import "SimulationEvent.h"
#import "SpatialModel.h"

#import "internal.h"

@implementation AggregatePopulationModel

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  int i;
  NSString *param;
  
  [super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent];
  
  param = [model objectForKey: @"divisionTime"];
  CHECK_PARAM(param, "divisionTime");
  divisionTime = [param doubleValue];

  cellSpecies = [model objectForKey: @"cellSpecies"];
  CHECK_PARAM(cellSpecies, "cellSpecies");

  param = [model objectForKey: @"increment"];
  CHECK_PARAM(param, "increment");
  incrementValue = [param doubleValue];

  SpatialModel *spatialModel = [self spatialModel];
  int dims = [spatialModel dimensions];
  double *ds = [spatialModel domainSize];
  spatialVolume = 1.0;
  for (i = 0; i < dims; ++i) spatialVolume *= ds[i];
  cellValue = (double)numOfCells / spatialVolume;
  printf("cell Value: %f %f\n", spatialVolume, cellValue);

  // simulation event for cell division
  NSInvocation *anInv = [NSInvocation invocationWithMethodSignature: [self methodSignatureForSelector: @selector(performCellDivision)]];
  [anInv setTarget: self];
  [anInv setSelector: @selector(performCellDivision)];
  SimulationEvent *anEvent = [SimulationEvent eventWithInvocation: anInv atTime: 0 repeat: divisionTime];
  [anEvent setRequireTransfer: YES];
  [modelController addSimulationEvent: anEvent];
  
  return self;
}

- (void)performCellDivision
{
  int i;
  printf("performCellDivision: %f\n", [modelController currentTime]);
  if (! TIME_EQUAL([modelController currentTime], 0)) {
    numOfCells += incrementValue;
    cellValue = (double)numOfCells / spatialVolume;    
  }
  
  for (i = 0; i < [childModels count]; ++i) [[childModels objectAtIndex: i] setValue: cellValue forSpecies: cellSpecies];
}

@end
