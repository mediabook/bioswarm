/*
 DynamicReactionRuleModel.m
 
 General rule-based reaction model with dynamic rule set.
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: February 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "DynamicReactionRuleModel.h"
#import "BioSwarmController.h"
#import "internal.h"

@implementation DynamicReactionRuleModel

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  id param;
  int i, j, k, r;

  if (![super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent]) return nil;
  
  maxReactions = 32;
  param = [aModel objectForKey: @"maxReactions"];
  CHECK_PARAM(param, "maxReactions, using default of 32");
  if (param) maxReactions = [param intValue];

  maxReactants = 32;
  param = [aModel objectForKey: @"maxReactants"];
  CHECK_PARAM(param, "maxReactants, using default of 32");
  if (param) maxReactants = [param intValue];
  
  maxReactionParameters = 1 + 3*maxReactants; // 1 for MassAction K and 3 for each Hill
  
  speciesReactions = malloc(sizeof(int) * numOfSpecies * maxReactions);
  int (*SR)[numOfSpecies][maxReactions] = speciesReactions;
  speciesReactionSign = malloc(sizeof(int) * numOfSpecies * maxReactions);
  int (*SRS)[numOfSpecies][maxReactions] = speciesReactionSign;
  speciesParameterOffset = malloc(sizeof(int) * numOfSpecies * maxReactions);
  int (*SPO)[numOfSpecies][maxReactions] = speciesParameterOffset;

  reactionTable = malloc(sizeof(int) * numOfReactions * maxReactants);
  int (*RT)[numOfReactions][maxReactants] = reactionTable;
  reactionType = (int *)malloc(sizeof(int) * numOfReactions);

  reactionParameter = malloc(sizeof(int) * numOfReactions * maxReactionParameters);
  int (*RP)[numOfReactions][maxReactionParameters] = reactionParameter;

  //
  // species reaction tables
  //
  for (i = 0; i < numOfSpecies; ++i)
    for (j = 0; j < maxReactions; ++j) {
      (*SR)[i][j] = -1;
      (*SRS)[i][j] = 0;
      (*SPO)[i][j] = -1;
    }
  
  for (i = 0; i < numOfReactions; ++i)
    for (j = 0; j < maxReactionParameters; ++j) (*RP)[i][j] = -1;

  for (i = 0; i < numOfSpecies; ++i) {
    NSArray *sFunc = [speciesFunctions objectAtIndex: i];
    int rIndex = 0;
    for (j = 0; j < [sFunc count]; ++j) {
      NSDictionary *d = [sFunc objectAtIndex: j];
      NSString *rName = [d objectForKey: @"reaction"];

      if ([self isDebug]) printf("Species function: %s\n %s\n", [[species objectAtIndex: i] UTF8String], [[d description] UTF8String]);

      // find reaction index
      for (r = 0; r < numOfReactions; ++r) {
        if ([rName isEqualToString: [[reactions objectAtIndex: r] objectForKey: @"name"]]) break;
      }

      if (r == numOfReactions) {
        printf("ERROR: Could not find reaction: %s\n", [rName UTF8String]);
        return nil;
      } else (*SR)[i][rIndex] = r;

      // sign of the reaction
      if ([[d objectForKey: @"sign"] isEqualToString: @"negative"]) (*SRS)[i][rIndex] = 1;
      else (*SRS)[i][rIndex] = 0;

      ++rIndex;

      // parameters
      //
      // NOTE: we rely upon the specific ordering of parameters
      // first MassAction K (if applicable) then Hill PMAX, C and H
      //
      // TODO: multiple products not supported
      //
      NSArray *a = [d objectForKey: @"parameters"];
      for (k = 0; k < [a count]; ++k) {
        NSString *paramName = [a objectAtIndex: k];
        NSUInteger idx = [parameterNames indexOfObject: paramName];
        (*RP)[r][k] = idx;
      }
    }
  }
      
  if ([self isDebug]) {
    printf("Species reaction table\n");
    for (i = 0; i < numOfSpecies; ++i) {
      for (j = 0; j < maxReactions; ++j) {
        if (j != 0) printf(" ");
        printf("%d(%d)", (*SR)[i][j], (*SRS)[i][j]);
      }
      printf("\n");
    }

    printf("Reaction parameter table\n");
    for (i = 0; i < numOfReactions; ++i) {
      for (j = 0; j < maxReactionParameters; ++j) {
        if (j != 0) printf(" ");
        printf("%d", (*RP)[i][j]);
      }
      printf("\n");
    }
  }

  //
  // reaction tables
  //
  for (i = 0; i < numOfReactions; ++i) {
    reactionType[i] = -1;
    for (j = 0; j < maxReactants; ++j)
      (*RT)[i][j] = -1;
  }

  for (i = 0; i < numOfReactions; ++i) {
    NSDictionary *d = [reactions objectAtIndex: i];

    NSString *rateFunction = [d objectForKey: @"rate"];
    if ([rateFunction isEqualToString: @"Hill"]) reactionType[i] = 0;
    if ([rateFunction isEqualToString: @"MassAction"]) reactionType[i] = 1;
    if ([rateFunction isEqualToString: @"Constant"]) reactionType[i] = 1;

    NSArray *a = [d objectForKey: @"reactants"];
    int rIndex = 0;
    for (j = 0; j < [a count]; ++j) {
      NSString *rName = [a objectAtIndex: j];
      NSUInteger idx = [species indexOfObject: rName];
      (*RT)[i][rIndex] = idx;
      ++rIndex;
    }

    a = [d objectForKey: @"enzymes"];
    if (a) {
      reactionType[i] = 2;
      (*RT)[i][rIndex] = -1;
      ++rIndex;
      
      for (j = 0; j < [a count]; ++j) {
        NSString *rName = [a objectAtIndex: j];
        NSUInteger idx = [species indexOfObject: rName];
        (*RT)[i][rIndex] = idx;
        ++rIndex;
      }
    }
  }

  if ([self isDebug]) {
    printf("Reaction table\n");
    for (i = 0; i < numOfReactions; ++i) {
      printf("%s %d(%d):", [[[reactions objectAtIndex: i] objectForKey: @"name"] UTF8String], i, reactionType[i]);
      for (j = 0; j < maxReactants; ++j) {
        if (j != 0) printf(" ");
        printf("%d", (*RT)[i][j]);
      }
      printf("\n");
    }
  }
  
  // TODO: should check we don't exceed bounds
  
  return self;
}

- (void)dealloc
{
  //printf("DynamicReactionRuleModel -dealloc\n");
  if (speciesReactions) free(speciesReactions);
  if (speciesReactionSign) free(speciesReactionSign);
  if (speciesParameterOffset) free(speciesParameterOffset);
  if (reactionTable) free(reactionTable);
  if (reactionParameter) free(reactionParameter);
  if (reactionType) free(reactionType);

  [super dealloc];
}

@end


//
// GPU
//

@implementation DynamicReactionRuleModel (GPU)

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  int k;
  
  NSString *dType = @"float";
  if ([modelController isDoublePrecision]) dType = @"double";
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendString: @"\n// structure for managing host data\n"];
  [GPUCode appendFormat: @"typedef struct _%@_hostStruct {\n", prefixName];
  [GPUCode appendString: @"  double *currentTime;\n"];
  [GPUCode appendFormat: @"  %@ *hostCurrentTime;\n", dType];
  [GPUCode appendString: @"  int *hostTimeFlag;\n"];
  [GPUCode appendString: @"  double *currentDelta;\n"];
  [GPUCode appendFormat: @"  %@ *hostCurrentDelta;\n", dType];
  [GPUCode appendString: @"  int *hostDeltaFlag;\n"];
  [GPUCode appendFormat: @"} %@_hostStruct;\n", prefixName];
  
  [GPUCode appendString: @"\n// structure for GPU data\n"];
  [GPUCode appendFormat: @"typedef struct _%@_GPUgrids {\n", prefixName];
  [GPUCode appendString: @"  int numOfODEs;\n"];
  [GPUCode appendString: @"  int speciesCount;\n"];
  [GPUCode appendString: @"  int multiplicity;\n"];
  [GPUCode appendString: @"  int speciesTotal;\n"];
  [GPUCode appendString: @"  int numOfReactions;\n"];
  [GPUCode appendString: @"  int maxReactions;\n"];
  [GPUCode appendString: @"  int maxReactants;\n"];
  [GPUCode appendString: @"  int maxReactionParameters;\n"];
  [GPUCode appendString: @"  int numOfParams;\n"];
  [GPUCode appendFormat: @"  %@ *currentTime;\n", dType];
  [GPUCode appendString: @"  int *timeFlag;\n"];
  [GPUCode appendFormat: @"  %@ *currentDelta;\n", dType];
  [GPUCode appendFormat: @"  %@ *Delta;\n", dType];
  [GPUCode appendString: @"  int *deltaFlag;\n"];
  [GPUCode appendFormat: @"  %@ dt;\n", dType];
  [GPUCode appendFormat: @"  %@ EPS;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData_F1;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData_F2;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData_F3;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData_F4;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData_F5;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData_F6;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData_tmp;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesData_error;\n", dType];
  [GPUCode appendString: @"  int *speciesFlags;\n"];
  [GPUCode appendFormat: @"  %@ *parameters;\n", dType];
  [GPUCode appendFormat: @"  %@ *speciesInterface;\n", dType];
  [GPUCode appendString: @"  size_t pitch;\n"];
  [GPUCode appendString: @"  size_t idx_pitch;\n"];
  [GPUCode appendString: @"  size_t intPitch;\n"];
  [GPUCode appendString: @"  size_t idx_intPitch;\n"];
  [GPUCode appendString: @"  int *speciesReactions;\n"];
  [GPUCode appendString: @"  int *speciesReactionSign;\n"];
  [GPUCode appendString: @"  size_t srPitch;\n"];
  [GPUCode appendString: @"  size_t idx_srPitch;\n"];
  [GPUCode appendString: @"  int *reactionTable;\n"];
  [GPUCode appendString: @"  size_t rPitch;\n"];
  [GPUCode appendString: @"  size_t idx_rPitch;\n"];
  [GPUCode appendString: @"  int *reactionType;\n"];
  [GPUCode appendString: @"  int *reactionParameter;\n"];
  [GPUCode appendString: @"  size_t rpPitch;\n"];
  [GPUCode appendString: @"  size_t idx_rpPitch;\n"];
  [GPUCode appendFormat: @"  %@_hostStruct *hostStruct;\n", prefixName];
  [GPUCode appendFormat: @"} %@_GPUgrids;\n", prefixName];

  [GPUCode appendFormat: @"\n__constant__ %@_GPUgrids %@_struct[1];\n", prefixName, prefixName];
  
  [GPUCode appendString: @"\n// parameters\n"];
  [GPUCode appendFormat: @"#define PINDEX %@_struct->reactionParameter[rNum*%@_struct->idx_rpPitch+pIndex]\n", prefixName, prefixName];
  [GPUCode appendString: @"#define PARAM_K speciesParameters[PINDEX*pitch+ODEnum]\n"];
  [GPUCode appendString: @"#define PARAM_PMAX speciesParameters[PINDEX*pitch+ODEnum]\n"];
  [GPUCode appendString: @"#define PARAM_C speciesParameters[(PINDEX+1)*pitch+ODEnum]\n"];
  [GPUCode appendString: @"#define PARAM_H speciesParameters[(PINDEX+2)*pitch+ODEnum]\n"];
  
  // species index
  [GPUCode appendString: @"\n// Species index\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_species %d\n", s, k];
  }
  
  return GPUCode;
}

- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName
{
  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;
  
  NSString *dType = @"float";
  if ([modelController isDoublePrecision]) dType = @"double";

  //
  // Headers and defines
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];
  
  //
  // Allocation
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"*%@_allocGPUKernel(void *model, int numOfODEs, int numParams, int speciesCount, int multiplicity, int numOfReactions, int maxReactions, int maxReactants, int maxReactionParameters, %@ dt, %@ EPS)", prefixName, dType, dType];
  [GPUDictionary setObject: GPUCode forKey: @"allocation function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @"  %@_GPUgrids *ptrs = (%@_GPUgrids *)malloc(sizeof(%@_GPUgrids));\n", prefixName, prefixName, prefixName];
  [GPUCode appendString: @"  int i;\n"];

  [GPUCode appendString: @"\n // Save parameters\n"];
  [GPUCode appendString: @" ptrs->numOfODEs = numOfODEs;\n"];
  [GPUCode appendString: @" ptrs->speciesCount = speciesCount;\n"];
  [GPUCode appendString: @" ptrs->multiplicity = multiplicity;\n"];
  [GPUCode appendString: @" ptrs->speciesTotal = speciesCount * multiplicity;\n"];
  [GPUCode appendString: @" ptrs->numOfReactions = numOfReactions;\n"];
  [GPUCode appendString: @" ptrs->maxReactions = maxReactions;\n"];
  [GPUCode appendString: @" ptrs->maxReactants = maxReactants;\n"];
  [GPUCode appendString: @" ptrs->maxReactionParameters = maxReactionParameters;\n"];
  [GPUCode appendString: @" ptrs->numOfParams = numParams;\n"];
  [GPUCode appendString: @" ptrs->dt = dt;\n"];
  [GPUCode appendString: @" ptrs->EPS = EPS;\n"];
  //[GPUCode appendString: @" ptrs->localEpsilonCheck = (int *)malloc(numOfODEs * sizeof(int));\n"];
  
  [GPUCode appendString: @"\n // Allocate device memory\n"];
  //[GPUCode appendString: @" cudaMalloc((void**)&(ptrs->epsilonCheck), numOfODEs * sizeof(int));\n"];
  [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
  [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData_F1), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
  [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData_F2), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
  
  if ([numericalScheme isEqualToString: @"AdaptiveRungeKuttaFehlberg"]) {
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData_F3), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData_F4), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData_F5), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData_F6), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
  }
  
  [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData_tmp), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
  [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesData_error), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesFlags), &(ptrs->intPitch), numOfODEs * sizeof(int), ptrs->speciesTotal);\n"];
  [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->parameters), &(ptrs->pitch), numOfODEs * sizeof(%@), numParams);\n", dType];
  [GPUCode appendFormat: @" cudaMallocPitch((void**)&(ptrs->speciesInterface), &(ptrs->pitch), numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
  [GPUCode appendFormat: @" cudaMemset2D(ptrs->speciesInterface, ptrs->pitch, 0, ptrs->numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];

  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesReactions), &(ptrs->srPitch), maxReactions * sizeof(int), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesReactionSign), &(ptrs->srPitch), maxReactions * sizeof(int), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->reactionTable), &(ptrs->rPitch), maxReactants * sizeof(int), numOfReactions);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->reactionParameter), &(ptrs->rpPitch), maxReactionParameters * sizeof(int), numOfReactions);\n"];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->reactionType), numOfReactions * sizeof(int));\n"];
  
  [GPUCode appendFormat: @" cudaMalloc((void**)&(ptrs->currentTime), numOfODEs * sizeof(%@));\n", dType];
  [GPUCode appendFormat: @" cudaMemset(ptrs->currentTime, 0, numOfODEs * sizeof(%@));\n", dType];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->timeFlag), numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" cudaMemset(ptrs->timeFlag, 0, numOfODEs * sizeof(int));\n"];
  [GPUCode appendFormat: @" cudaMalloc((void**)&(ptrs->Delta), numOfODEs * sizeof(%@));\n", dType];
  [GPUCode appendFormat: @" cudaMemset(ptrs->Delta, 0, numOfODEs * sizeof(%@));\n", dType];
  [GPUCode appendFormat: @" cudaMalloc((void**)&(ptrs->currentDelta), numOfODEs * sizeof(%@));\n", dType];
  [GPUCode appendFormat: @" cudaMemset(ptrs->currentDelta, 0, numOfODEs * sizeof(%@));\n", dType];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->deltaFlag), numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" cudaMemset(ptrs->deltaFlag, 0, numOfODEs * sizeof(int));\n"];
  
  [GPUCode appendString: @"\n"];
  [GPUCode appendFormat: @" ptrs->idx_pitch = ptrs->pitch / sizeof(%@);\n", dType];
  [GPUCode appendString: @" ptrs->idx_intPitch = ptrs->intPitch / sizeof(int);\n"];
  [GPUCode appendString: @" ptrs->idx_srPitch = ptrs->srPitch / sizeof(int);\n"];
  [GPUCode appendString: @" ptrs->idx_rPitch = ptrs->rPitch / sizeof(int);\n"];
  [GPUCode appendString: @" ptrs->idx_rpPitch = ptrs->rpPitch / sizeof(int);\n"];
  
  [GPUCode appendString: @"\n // Allocate host memory\n"];
  [GPUCode appendFormat: @" ptrs->hostStruct = (%@_hostStruct *)malloc(sizeof(%@_hostStruct));\n", prefixName, prefixName];
  [GPUCode appendString: @" ptrs->hostStruct->currentTime = (double *)malloc(numOfODEs * sizeof(double));\n"];
  [GPUCode appendFormat: @" ptrs->hostStruct->hostCurrentTime = (%@ *)malloc(numOfODEs * sizeof(%@));\n", dType, dType];
  [GPUCode appendString: @" ptrs->hostStruct->hostTimeFlag = (int *)malloc(numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" ptrs->hostStruct->currentDelta = (double *)malloc(numOfODEs * sizeof(double));\n"];
  [GPUCode appendFormat: @" ptrs->hostStruct->hostCurrentDelta = (%@ *)malloc(numOfODEs * sizeof(%@));\n", dType, dType];
  [GPUCode appendString: @" ptrs->hostStruct->hostDeltaFlag = (int *)malloc(numOfODEs * sizeof(int));\n"];
  
  [GPUCode appendFormat: @" for (i = 0; i < ptrs->numOfODEs; ++i) {\n"];
  [GPUCode appendString: @"    ptrs->hostStruct->currentDelta[i] = ptrs->dt;\n"];
  [GPUCode appendString: @"    ptrs->hostStruct->hostCurrentDelta[i] = ptrs->dt;\n"];
  [GPUCode appendString: @" }\n"];
  [GPUCode appendFormat: @" cudaMemcpy(ptrs->Delta, ptrs->hostStruct->hostCurrentDelta, ptrs->numOfODEs * sizeof(%@), cudaMemcpyHostToDevice);\n", dType];
  [GPUCode appendFormat: @" cudaMemcpy(ptrs->currentDelta, ptrs->hostStruct->hostCurrentDelta, ptrs->numOfODEs * sizeof(%@), cudaMemcpyHostToDevice);\n", dType];
  
  [GPUCode appendString: @"\n"];
  [GPUCode appendString: @" return ptrs;\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"allocation code"];
  
  //
  // Data Transfer
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *p, int aFlag, %@ *hostParameters, %@ *hostData, int *hostSpeciesFlags, void *hostSpeciesReactions, void *hostSpeciesReactionSign, int *hostReactionTable, int *hostReactionType, void *hostReactionParameter)", prefixName, dType, dType];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *ptrs = (%@_GPUgrids *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @"\n if (aFlag) {\n"];
  [GPUCode appendString: @"    // Copy host memory to device memory\n"];
  [GPUCode appendFormat: @"    cudaMemcpyToSymbol(%@_struct, p, sizeof(%@_GPUgrids), 0, cudaMemcpyHostToDevice);\n", prefixName, prefixName];
  
  [GPUCode appendFormat: @"    cudaMemcpy2D(ptrs->speciesData, ptrs->pitch, (const cudaArray *)hostData, ptrs->numOfODEs * sizeof(%@), ptrs->numOfODEs * sizeof(%@), ptrs->speciesTotal, cudaMemcpyHostToDevice);\n", dType, dType];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->speciesFlags, ptrs->pitch, (const cudaArray *)hostSpeciesFlags, ptrs->numOfODEs * sizeof(int), ptrs->numOfODEs * sizeof(int), ptrs->speciesTotal, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendFormat: @"    cudaMemcpy2D(ptrs->parameters, ptrs->pitch, (const cudaArray *)hostParameters, ptrs->numOfODEs * sizeof(%@), ptrs->numOfODEs * sizeof(%@), ptrs->numOfParams, cudaMemcpyHostToDevice);\n", dType, dType];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->speciesReactions, ptrs->srPitch, (const cudaArray *)hostSpeciesReactions, ptrs->maxReactions * sizeof(int), ptrs->maxReactions * sizeof(int), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->speciesReactionSign, ptrs->srPitch, (const cudaArray *)hostSpeciesReactionSign, ptrs->maxReactions * sizeof(int), ptrs->maxReactions * sizeof(int), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->reactionTable, ptrs->rPitch, (const cudaArray *)hostReactionTable, ptrs->maxReactants * sizeof(int), ptrs->maxReactants * sizeof(int), ptrs->numOfReactions, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy(ptrs->reactionType, hostReactionType, ptrs->numOfReactions * sizeof(int), cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->reactionParameter, ptrs->rpPitch, (const cudaArray *)hostReactionParameter, ptrs->maxReactionParameters * sizeof(int), ptrs->maxReactionParameters * sizeof(int), ptrs->numOfReactions, cudaMemcpyHostToDevice);\n"];

  [GPUCode appendFormat: @"    cudaMemset2D(ptrs->speciesData_tmp, ptrs->pitch, 0, ptrs->numOfODEs * sizeof(%@), ptrs->speciesTotal);\n", dType];
  
  //[GPUCode appendString: @"    cudaMemset(ptrs->epsilonCheck, 0, ptrs->numOfODEs * sizeof(int));\n"];
  
  [GPUCode appendString: @" } else {\n"];
  
  [GPUCode appendString: @"    // Copy result to host memory\n"];
  //[GPUCode appendString: @"    cudaMemcpy(ptrs->localEpsilonCheck, ptrs->epsilonCheck, ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n"];
  [GPUCode appendFormat: @"    cudaMemcpy2D(hostData, ptrs->numOfODEs * sizeof(%@), ptrs->speciesData, ptrs->pitch, ptrs->numOfODEs * sizeof(%@), ptrs->speciesTotal, cudaMemcpyDeviceToHost);\n", dType, dType];
  
  [GPUCode appendString: @" }\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];
  
  //
  // Execution
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, double startTime, double nextTime)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" %@_GPUgrids *%@_ptrs = (%@_GPUgrids *)%@_data;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" int %@_threadsPerBlock1D;\n", prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerBlock(64, 8);\n", prefixName];
  [GPUCode appendFormat: @" if (%@_ptrs->numOfODEs < 64) {\n", prefixName];
  [GPUCode appendFormat: @"   %@_threadsPerBlock.x = %@_ptrs->numOfODEs;\n", prefixName, prefixName];
  [GPUCode appendFormat: @"   %@_threadsPerBlock1D = %@_ptrs->numOfODEs;\n", prefixName, prefixName];
  [GPUCode appendString: @" } else {\n"];
  [GPUCode appendFormat: @"   %@_threadsPerBlock = 64;\n", prefixName];
  [GPUCode appendFormat: @"   %@_threadsPerBlock1D = 64;\n", prefixName];
  [GPUCode appendString: @" }\n"];
  [GPUCode appendFormat: @" dim3 %@_blocksPerGrid((%@_ptrs->numOfODEs + %@_threadsPerBlock.x - 1) / %@_threadsPerBlock.x,", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" (%@_ptrs->speciesTotal + %@_threadsPerBlock.y - 1) / %@_threadsPerBlock.y);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" int %@_blocksPerGrid1D = (%@_ptrs->numOfODEs + %@_threadsPerBlock1D - 1) / %@_threadsPerBlock1D;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];
  
  GPUCode = [NSMutableString new];
  if ([numericalScheme isEqualToString: @"RungeKutta2ndOrder"]) {
    // 2nd-order Runge-Kutta
    [GPUCode appendFormat: @"      %@_kernel1_2rk<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel2_2rk<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];

  } else if ([numericalScheme isEqualToString: @"AdaptiveRungeKuttaFehlberg"]) {
    // 5th-order Runge-Kutta with error estimation for adaptive time step
#if 1
    [GPUCode appendString: @"   int i, doneFlag = 0;\n"];
    [GPUCode appendString: @"   printf(\"hello\\n\");\n"];
    [GPUCode appendFormat: @"   cudaMemset(%@_ptrs->timeFlag, 0, %@_ptrs->numOfODEs * sizeof(int));\n", prefixName, prefixName];
    [GPUCode appendFormat: @"   for (i = 0; i < %@_ptrs->numOfODEs; ++i) %@_ptrs->hostStruct->hostCurrentTime[i] = startTime;\n", prefixName, prefixName];
    //[GPUCode appendFormat: @"      %@_ptrs->hostStruct->currentTime[i] = startTime;\n", prefixName];
    //[GPUCode appendFormat: @"      %@_ptrs->hostStruct->hostCurrentTime[i] = startTime;\n", prefixName];
    //[GPUCode appendFormat: @"      %@_ptrs->hostStruct->hostCurrentDelta[i] = %@_ptrs->hostStruct->currentDelta[i];\n", prefixName, prefixName];
    //[GPUCode appendString: @"   }\n"];
    [GPUCode appendFormat: @"   cudaMemcpy(%@_ptrs->currentTime, %@_ptrs->hostStruct->hostCurrentTime, %@_ptrs->numOfODEs * sizeof(%@), cudaMemcpyHostToDevice);\n", prefixName, prefixName, prefixName, dType];
    //[GPUCode appendFormat: @"   cudaMemcpy(%@_ptrs->currentDelta, %@_ptrs->hostStruct->hostCurrentDelta, %@_ptrs->numOfODEs * sizeof(float), cudaMemcpyHostToDevice);\n", prefixName, prefixName, prefixName];
    
    [GPUCode appendString: @"\n"];
    [GPUCode appendString: @"   while (!doneFlag) {\n"];
    
    [GPUCode appendString: @"\n      // run the kernels\n"];
    [GPUCode appendFormat: @"      %@_kernelF1_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF2_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF3_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF4_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF5_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF6_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel_error_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    
    //[GPUCode appendFormat: @"      %@_kernel_adjust_rkf<<< %@_blocksPerGrid1D, %@_threadsPerBlock1D >>>(startTime, nextTime);\n", prefixName, prefixName, prefixName];
    //if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
    
    [GPUCode appendString: @"\n      // check error, update x(t + dt) and advance time\n"];
    [GPUCode appendFormat: @"      %@_kernel_check_rkf<<< %@_blocksPerGrid1D, %@_threadsPerBlock1D >>>(startTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel_update_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel_time_rkf<<< %@_blocksPerGrid1D, %@_threadsPerBlock1D >>>(startTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
    
    [GPUCode appendString: @"\n      // check if all threads have reached final time\n"];
    [GPUCode appendFormat: @"      cudaMemcpy(%@_ptrs->hostStruct->hostTimeFlag, %@_ptrs->timeFlag, %@_ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n", prefixName, prefixName, prefixName];
    [GPUCode appendString: @"      doneFlag = 1;\n"];
    [GPUCode appendFormat: @"      for (i = 0; i < %@_ptrs->numOfODEs; ++i) doneFlag &= %@_ptrs->hostStruct->hostTimeFlag[i];\n", prefixName, prefixName];
    [GPUCode appendString: @"   }\n"];
#else
    [GPUCode appendFormat: @"      %@_kernelF1_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF2_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF3_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF4_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF5_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF6_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel_update_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
#endif
  }
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];

  // for non-hybrid models, i.e. only 1 model
  // we have specialized kernel that can optimize the time step
  if ([numericalScheme isEqualToString: @"AdaptiveRungeKuttaFehlberg"]) {
    GPUCode = [NSMutableString new];

    [GPUCode appendString: @"\n   // Invoke kernel\n"];
    [GPUCode appendString: @"   double currentTime = startTime;\n"];
    [GPUCode appendString: @"   int i, doneFlag = 0;\n"];

    [GPUCode appendString: @"\n"];
    [GPUCode appendFormat: @"   cudaMemset(%@_ptrs->timeFlag, 0, %@_ptrs->numOfODEs * sizeof(int));\n", prefixName, prefixName];
    [GPUCode appendFormat: @"   for (i = 0; i < %@_ptrs->numOfODEs; ++i) %@_ptrs->hostStruct->hostCurrentTime[i] = startTime;\n", prefixName, prefixName];
    //[GPUCode appendFormat: @"      %@_ptrs->hostStruct->currentTime[i] = startTime;\n", prefixName];
    //[GPUCode appendFormat: @"      %@_ptrs->hostStruct->hostCurrentTime[i] = startTime;\n", prefixName];
    //[GPUCode appendFormat: @"      %@_ptrs->hostStruct->hostCurrentDelta[i] = %@_ptrs->hostStruct->currentDelta[i];\n", prefixName, prefixName];
    //[GPUCode appendString: @"   }\n"];
    [GPUCode appendFormat: @"   cudaMemcpy(%@_ptrs->currentTime, %@_ptrs->hostStruct->hostCurrentTime, %@_ptrs->numOfODEs * sizeof(%@), cudaMemcpyHostToDevice);\n", prefixName, prefixName, prefixName, dType];
    //[GPUCode appendFormat: @"   cudaMemcpy(%@_ptrs->currentDelta, %@_ptrs->hostStruct->hostCurrentDelta, %@_ptrs->numOfODEs * sizeof(float), cudaMemcpyHostToDevice);\n", prefixName, prefixName, prefixName];

    [GPUCode appendString: @"\n"];
    [GPUCode appendString: @"   while (!doneFlag) {\n"];
    
    [GPUCode appendString: @"\n      // run the kernels\n"];
    [GPUCode appendFormat: @"      %@_kernelF1_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF2_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF3_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF4_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF5_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernelF6_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel_error_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];

    //[GPUCode appendFormat: @"      %@_kernel_adjust_rkf<<< %@_blocksPerGrid1D, %@_threadsPerBlock1D >>>(startTime, nextTime);\n", prefixName, prefixName, prefixName];
    //if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];

    [GPUCode appendString: @"\n      // check error, update x(t + dt) and advance time\n"];
    [GPUCode appendFormat: @"      %@_kernel_check_rkf<<< %@_blocksPerGrid1D, %@_threadsPerBlock1D >>>(startTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel_update_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel_time_rkf<<< %@_blocksPerGrid1D, %@_threadsPerBlock1D >>>(startTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
    
    [GPUCode appendString: @"\n      // check if all threads have reached final time\n"];
    [GPUCode appendFormat: @"      cudaMemcpy(%@_ptrs->hostStruct->hostTimeFlag, %@_ptrs->timeFlag, %@_ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n", prefixName, prefixName, prefixName];
    [GPUCode appendString: @"      doneFlag = 1;\n"];
    [GPUCode appendFormat: @"      for (i = 0; i < %@_ptrs->numOfODEs; ++i) doneFlag &= %@_ptrs->hostStruct->hostTimeFlag[i];\n", prefixName, prefixName];

#if 0
    [GPUCode appendString: @"\n      // adjust dt\n"];
    [GPUCode appendString: @"      doneFlag = 1;\n"];
    [GPUCode appendFormat: @"      %@_kernel_adjust_rkf<<< %@_blocksPerGrid1D, %@_threadsPerBlock1D >>>(startTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      cudaMemcpy(%@_ptrs->hostStruct->hostDeltaFlag, %@_ptrs->deltaFlag, %@_ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n", prefixName, prefixName, prefixName];

    [GPUCode appendFormat: @"      for (i = 0; i < %@_ptrs->numOfODEs; ++i) {\n", prefixName, prefixName];
    [GPUCode appendFormat: @"         if (%@_ptrs->hostStruct->hostDeltaFlag[i] == 1) {\n", prefixName];
    [GPUCode appendString: @"            // dt adjust smaller, need to recompute\n"];
    [GPUCode appendFormat: @"            %@_ptrs->hostStruct->currentDelta[i] /= 2.0;\n", prefixName, prefixName];

    [GPUCode appendString: @"         } else {\n"];
    [GPUCode appendString: @"            // update current time\n"];
    [GPUCode appendFormat: @"            %@_ptrs->hostStruct->currentTime[i] += %@_ptrs->hostStruct->hostCurrentDelta[i];\n", prefixName, prefixName];

    [GPUCode appendString: @"\n            // dt adjust bigger\n"];
    [GPUCode appendFormat: @"            if (%@_ptrs->hostStruct->hostDeltaFlag[i] == 2) {\n", prefixName];
    [GPUCode appendString: @"\n               // do not adjust if last run was for a small end of time delta\n"];
    [GPUCode appendFormat: @"               if (%@_ptrs->hostStruct->hostCurrentDelta[i] == %@_ptrs->hostStruct->currentDelta[i]) {\n", prefixName, prefixName];
    [GPUCode appendFormat: @"                  %@_ptrs->hostStruct->currentDelta[i] *= 2.0;\n", prefixName];

    [GPUCode appendString: @"\n               // do not get bigger than the total time\n"];
    [GPUCode appendFormat: @"               if (%@_ptrs->hostStruct->currentDelta[i] > (nextTime - startTime))\n", prefixName];
    [GPUCode appendFormat: @"                  %@_ptrs->hostStruct->currentDelta[i] = nextTime - startTime;\n", prefixName];
    [GPUCode appendString: @"               }\n"];
    [GPUCode appendString: @"            }\n"];
    [GPUCode appendString: @"         }\n"];

    [GPUCode appendString: @"\n         // have reached final time?\n"];
    [GPUCode appendFormat: @"         if (%@_ptrs->hostStruct->currentTime[i] >= nextTime) doneFlag &= 1;\n", prefixName, prefixName];
    [GPUCode appendString: @"         else doneFlag = 0;\n"];
    
    [GPUCode appendString: @"\n         // final values for transfer to GPU\n"];
    [GPUCode appendFormat: @"         %@_ptrs->hostStruct->hostCurrentTime[i] = %@_ptrs->hostStruct->currentTime[i];\n", prefixName, prefixName];
    [GPUCode appendFormat: @"         if ((%@_ptrs->hostStruct->currentTime[i] + %@_ptrs->hostStruct->currentDelta[i]) > nextTime)\n", prefixName];
    [GPUCode appendFormat: @"            %@_ptrs->hostStruct->hostCurrentDelta[i] = nextTime - %@_ptrs->hostStruct->currentTime[i];\n", prefixName, prefixName];
    [GPUCode appendString: @"         else\n"];
    [GPUCode appendFormat: @"            %@_ptrs->hostStruct->hostCurrentDelta[i] = %@_ptrs->hostStruct->currentDelta[i];\n", prefixName, prefixName];

    [GPUCode appendString: @"      }\n"];
    
    [GPUCode appendString: @"\n      // transfer to GPU\n"];
    [GPUCode appendFormat: @"      //cudaMemcpy(%@_ptrs->currentDelta, %@_ptrs->hostStruct->hostCurrentDelta, %@_ptrs->numOfODEs * sizeof(%@), cudaMemcpyHostToDevice);\n", prefixName, prefixName, prefixName, dType];
    [GPUCode appendFormat: @"      //cudaMemcpy(%@_ptrs->currentTime, %@_ptrs->hostStruct->hostCurrentTime, %@_ptrs->numOfODEs * sizeof(%@), cudaMemcpyHostToDevice);\n", prefixName, prefixName, prefixName, dType];
    
    [GPUCode appendString: @"\n      // update x(t + dt)\n"];
    [GPUCode appendFormat: @"      %@_kernel_update_rkf<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
#endif

    [GPUCode appendString: @"   }\n"];

    [GPUDictionary setObject: GPUCode forKey: @"solo execution code"];
  }

  //
  // Release
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_releaseGPUKernel(void *model, void *p)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"release function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *ptrs = (%@_GPUgrids *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData_F1);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData_F2);\n"];
  if ([numericalScheme isEqualToString: @"AdaptiveRungeKuttaFehlberg"]) {
    [GPUCode appendString: @" cudaFree(ptrs->speciesData_F3);\n"];
    [GPUCode appendString: @" cudaFree(ptrs->speciesData_F4);\n"];
    [GPUCode appendString: @" cudaFree(ptrs->speciesData_F5);\n"];
    [GPUCode appendString: @" cudaFree(ptrs->speciesData_F6);\n"];
  }
  [GPUCode appendString: @" cudaFree(ptrs->speciesData_tmp);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData_error);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesFlags);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->parameters);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesReactions);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesReactionSign);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->reactionTable);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->reactionType);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->reactionParameter);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->timeFlag);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->currentDelta);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->deltaFlag);\n"];
  
  [GPUCode appendString: @"\n"];
  [GPUCode appendString: @"  free(ptrs->hostStruct->currentTime);\n"];
  [GPUCode appendString: @"  free(ptrs->hostStruct->hostCurrentTime);\n"];
  [GPUCode appendString: @"  free(ptrs->hostStruct->hostTimeFlag);\n"];
  [GPUCode appendString: @"  free(ptrs->hostStruct->currentDelta);\n"];
  [GPUCode appendString: @"  free(ptrs->hostStruct->hostCurrentDelta);\n"];
  [GPUCode appendString: @"  free(ptrs->hostStruct->hostDeltaFlag);\n"];
  [GPUCode appendString: @"  free(ptrs->hostStruct);\n"];
  
  [GPUCode appendString: @"\n"]; 
  [GPUCode appendString: @" free(ptrs);\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"release code"];
  
  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"RModelDynamicGPUFunctions_%@ %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", dType, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];
  
  return GPUDictionary;    
}

- (NSMutableString *)functionKernelGPUCode: (NSString *)prefixName
{
  NSString *dType = @"float";
  if ([modelController isDoublePrecision]) dType = @"double";

  NSMutableString *GPUCode = [NSMutableString new];
  
  id spatialModel = [parentModel spatialModelForContext: self];
  int dimensions = [spatialModel dimensions];
  if (dimensions == 0) dimensions = 1;
  printf("spatial: %p %p %d\n", spatialModel, parentModel, dimensions);

  //
  // Different kernel code required for different dimensional model
  // as the memory layout is different for each.
  //
  
  [GPUCode appendString: @"\n// Reaction function used by Runge-Kutta solvers\n"];
  [GPUCode appendString: @"__device__ void\n"];
  
  switch (dimensions) {
    case 1:
      // 1D spatial model, standard ReactionRuleModel with multiple ODEs
      [GPUCode appendFormat: @"%@_kernel_function(int ODEnum, int speciesIdx, int speciesNum, int multiplicity, int pitch, %@ *speciesData, %@ *speciesData_in, %@ *speciesData_out, %@ *speciesParameters, %@ mh, %@ mf)\n", prefixName, dType, dType, dType, dType, dType, dType];
      break;
      
    case 2:
      // 2D spatial model, as used by ReactionDiffusionModel
      [GPUCode appendFormat: @"%@_kernel_function(int i, int j, %@ mh, %@ mf)\n", prefixName, dType, dType];
      break;
  }
  [GPUCode appendString: @"{\n"];
  [GPUCode appendFormat: @"   %@ interactionValue, termValue, F_val;\n", dType];
  [GPUCode appendString: @"   int rIndex, rNum, rType;\n"];
  [GPUCode appendString: @"   int rsIndex, rSpecies, rSpeciesIdx;\n"];
  [GPUCode appendString: @"   int pIndex;\n"];

  [GPUCode appendString: @"\n   // check knockout\n"];
  [GPUCode appendFormat: @"   if (%@_struct->speciesFlags[speciesIdx*%@_struct->idx_intPitch+ODEnum]) {\n", prefixName, prefixName];
  [GPUCode appendString: @"      speciesData_out[speciesIdx*pitch+ODEnum] = 0;\n"];
  [GPUCode appendString: @"      return;\n"];
  [GPUCode appendString: @"   }\n"];

  [GPUCode appendString: @"\n   // loop through each reaction for species\n"];
  [GPUCode appendString: @"   interactionValue = 0.0;\n"];
  [GPUCode appendString: @"   rIndex = 0;\n"];
  [GPUCode appendFormat: @"   rNum = %@_struct->speciesReactions[speciesNum*%@_struct->idx_srPitch+rIndex];\n", prefixName, prefixName];
  [GPUCode appendString: @"   while (rNum >= 0) {\n"];
  [GPUCode appendFormat: @"      rType = %@_struct->reactionType[rNum];\n", prefixName];
  [GPUCode appendString: @"      pIndex = 0;\n"];
  [GPUCode appendString: @"      switch (rType) {\n"];

  [GPUCode appendString: @"\n        case 0:\n"];
  [GPUCode appendString: @"          // Hill function\n"];
  [GPUCode appendString: @"          rsIndex = 0;\n"];
  [GPUCode appendFormat: @"          rSpecies = %@_struct->reactionTable[rNum*%@_struct->idx_rPitch+rsIndex];\n", prefixName, prefixName];
      
  [GPUCode appendString: @"\n          termValue = 1.0;\n"];
  [GPUCode appendString: @"          while (rSpecies >= 0) {\n"];
  [GPUCode appendFormat: @"            rSpeciesIdx = rSpecies + multiplicity * %@_struct->speciesCount;\n", prefixName];

  [GPUCode appendFormat: @"\n            F_val = speciesData[rSpeciesIdx*pitch+ODEnum] + mf * speciesData_in[rSpeciesIdx*pitch+ODEnum];\n"];
  [GPUCode appendString: @"            termValue *= SHILL(F_val, PARAM_PMAX, PARAM_C, PARAM_H);\n"];
        
  [GPUCode appendString: @"\n            // next reactant\n"];
  [GPUCode appendString: @"            ++rsIndex;\n"];
  [GPUCode appendString: @"            pIndex += 3;\n"];
  [GPUCode appendFormat: @"            if (rsIndex > %@_struct->maxReactants) rSpecies = -1;\n", prefixName];
  [GPUCode appendFormat: @"            else rSpecies = %@_struct->reactionTable[rNum*%@_struct->idx_rPitch+rsIndex];\n", prefixName, prefixName];
  [GPUCode appendString: @"          }\n"];
      
  [GPUCode appendString: @"\n          interactionValue += termValue;\n"];
  [GPUCode appendString: @"          break;\n"];
      
  [GPUCode appendString: @"\n        case 1:\n"];
  [GPUCode appendString: @"          // Mass action\n"];
  [GPUCode appendString: @"          rsIndex = 0;\n"];
  [GPUCode appendFormat: @"          rSpecies = %@_struct->reactionTable[rNum*%@_struct->idx_rPitch+rsIndex];\n", prefixName, prefixName];
  
  [GPUCode appendFormat: @"\n          if (%@_struct->speciesReactionSign[speciesNum*%@_struct->idx_srPitch+rIndex]) termValue = -PARAM_K;\n", prefixName, prefixName];
  [GPUCode appendString: @"          else termValue = PARAM_K;\n"];
  [GPUCode appendString: @"          while (rSpecies >= 0) {\n"];
  [GPUCode appendFormat: @"            rSpeciesIdx = rSpecies + multiplicity * %@_struct->speciesCount;\n", prefixName];

  [GPUCode appendFormat: @"\n            F_val = speciesData[rSpeciesIdx*pitch+ODEnum] + mf * speciesData_in[rSpeciesIdx*pitch+ODEnum];\n"];
  [GPUCode appendString: @"            termValue *= F_val;\n"];
  
  [GPUCode appendString: @"\n            // next reactant\n"];
  [GPUCode appendString: @"            ++rsIndex;\n"];
  [GPUCode appendString: @"            ++pIndex;\n"];
  [GPUCode appendFormat: @"            if (rsIndex > %@_struct->maxReactants) rSpecies = -1;\n", prefixName];
  [GPUCode appendFormat: @"            else rSpecies = %@_struct->reactionTable[rNum*%@_struct->idx_rPitch+rsIndex];\n", prefixName, prefixName];
  [GPUCode appendString: @"          }\n"];
  
  [GPUCode appendString: @"\n          interactionValue += termValue;\n"];
  [GPUCode appendString: @"          break;\n"];
  
  [GPUCode appendString: @"\n        case 2:\n"];
  [GPUCode appendString: @"          // Mass action with Hill function enzyme\n"];
  [GPUCode appendString: @"          rsIndex = 0;\n"];
  [GPUCode appendFormat: @"          rSpecies = %@_struct->reactionTable[rNum*%@_struct->idx_rPitch+rsIndex];\n", prefixName, prefixName];
  
  [GPUCode appendString: @"\n          // some number of mass action reactants\n"];
  [GPUCode appendFormat: @"          if (%@_struct->speciesReactionSign[speciesNum*%@_struct->idx_srPitch+rIndex]) termValue = -PARAM_K;\n", prefixName, prefixName];
  [GPUCode appendString: @"          else termValue = PARAM_K;\n"];
  [GPUCode appendString: @"          while (rSpecies >= 0) {\n"];
  [GPUCode appendFormat: @"            rSpeciesIdx = rSpecies + multiplicity * %@_struct->speciesCount;\n", prefixName];
  
  [GPUCode appendFormat: @"\n            F_val = speciesData[rSpeciesIdx*pitch+ODEnum] + mf * speciesData_in[rSpeciesIdx*pitch+ODEnum];\n"];
  [GPUCode appendString: @"            termValue *= F_val;\n"];
  
  [GPUCode appendString: @"\n            // next reactant\n"];
  [GPUCode appendString: @"            ++rsIndex;\n"];
  [GPUCode appendString: @"            ++pIndex;\n"];
  [GPUCode appendFormat: @"            if (rsIndex > %@_struct->maxReactants) rSpecies = -1;\n", prefixName];
  [GPUCode appendFormat: @"            else rSpecies = %@_struct->reactionTable[rNum*%@_struct->idx_rPitch+rsIndex];\n", prefixName, prefixName];
  [GPUCode appendString: @"          }\n"];
  
  [GPUCode appendString: @"\n          // some number of hill function enzymes\n"];
  [GPUCode appendString: @"          ++rsIndex;\n"];
  [GPUCode appendFormat: @"          if (rsIndex > %@_struct->maxReactants) rSpecies = -1;\n", prefixName];
  [GPUCode appendFormat: @"          else rSpecies = %@_struct->reactionTable[rNum*%@_struct->idx_rPitch+rsIndex];\n", prefixName, prefixName];

  [GPUCode appendString: @"\n          while (rSpecies >= 0) {\n"];
  [GPUCode appendFormat: @"            rSpeciesIdx = rSpecies + multiplicity * %@_struct->speciesCount;\n", prefixName];
  
  [GPUCode appendFormat: @"\n            F_val = speciesData[rSpeciesIdx*pitch+ODEnum] + mf * speciesData_in[rSpeciesIdx*pitch+ODEnum];\n"];
  [GPUCode appendString: @"            termValue *= SHILL(F_val, PARAM_PMAX, PARAM_C, PARAM_H);\n"];
  
  [GPUCode appendString: @"\n            // next reactant\n"];
  [GPUCode appendString: @"            ++rsIndex;\n"];
  [GPUCode appendString: @"            pIndex += 3;\n"];
  [GPUCode appendFormat: @"            if (rsIndex > %@_struct->maxReactants) rSpecies = -1;\n", prefixName];
  [GPUCode appendFormat: @"            else rSpecies = %@_struct->reactionTable[rNum*%@_struct->idx_rPitch+rsIndex];\n", prefixName, prefixName];
  [GPUCode appendString: @"          }\n"];
  
  [GPUCode appendString: @"\n          interactionValue += termValue;\n"];
  [GPUCode appendString: @"          break;\n"];
  
  [GPUCode appendString: @"      }\n"];

  [GPUCode appendString: @"\n      // next reaction\n"];
  [GPUCode appendString: @"      ++rIndex;\n"];
  [GPUCode appendFormat: @"      if (rIndex > %@_struct->maxReactions) rNum = -1;\n", prefixName];
  [GPUCode appendFormat: @"      else rNum = %@_struct->speciesReactions[speciesNum*%@_struct->idx_srPitch+rIndex];\n", prefixName, prefixName];
  [GPUCode appendString: @"   }\n"];

  [GPUCode appendString: @"\n   // save result\n"];
  [GPUCode appendString: @"   F_val = mh * interactionValue;\n"];
  [GPUCode appendString: @"   speciesData_out[speciesIdx*pitch+ODEnum] = F_val;\n"];

  [GPUCode appendString: @"}\n"];
  
  return GPUCode;    
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  int k;

  NSString *dType = @"float";
  if ([modelController isDoublePrecision]) dType = @"double";

  NSMutableString *GPUCode = [NSMutableString new];
  
  // kernel to calculate reaction function
  [GPUCode appendString: [self functionKernelGPUCode: prefixName]];
  
  // kernel for numerical method
  if ([numericalScheme isEqualToString: @"RungeKutta2ndOrder"]) {
    
    [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel1_2rk(%@ finalTime)\n", prefixName, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesIdx = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesIdx >= %@_struct->speciesTotal) return;\n", prefixName];
    [GPUCode appendFormat: @"\n   int multiplicity = speciesIdx / %@_struct->speciesCount;\n", prefixName];
    [GPUCode appendFormat: @"   int speciesNum = speciesIdx - multiplicity * %@_struct->speciesCount;\n", prefixName];

    [GPUCode appendFormat: @"\n   if (%@_struct->currentDelta[ODEnum] == 0.0f) %@_struct->currentDelta[ODEnum] = %@_struct->dt;\n", prefixName, prefixName, prefixName];
    
    // Calculate F1
    [GPUCode appendString: @"\n   // calculate F1\n"];
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, speciesIdx, speciesNum, multiplicity, %@_struct->idx_pitch, %@_struct->speciesData,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_tmp, %@_struct->speciesData_F1, %@_struct->parameters,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->currentDelta[ODEnum], 0.0f);\n", prefixName];
    
    [GPUCode appendString: @"}\n"];
    
    [GPUCode appendString: @"\n// F2 Kernel for 2nd order Runge-Kutta\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel2_2rk(%@ finalTime)\n", prefixName, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesIdx = blockIdx.y * blockDim.y + threadIdx.y;\n"];

    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesIdx >= %@_struct->speciesTotal) return;\n", prefixName];
    [GPUCode appendFormat: @"\n   int multiplicity = speciesIdx / %@_struct->speciesCount;\n", prefixName];
    [GPUCode appendFormat: @"   int speciesNum = speciesIdx - multiplicity * %@_struct->speciesCount;\n", prefixName];

    // Calculate F2
    [GPUCode appendString: @"\n   // calculate F2\n"];
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, speciesIdx, speciesNum, multiplicity, %@_struct->idx_pitch, %@_struct->speciesData,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_F1, %@_struct->speciesData_F2, %@_struct->parameters,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->currentDelta[ODEnum], 0.5f);\n", prefixName];
    
    // calculate x(t + dt)                                                                                                                             
    [GPUCode appendString: @"\n   // calculate x(t + dt)\n"];
    [GPUCode appendFormat: @"   %@_struct->speciesData[speciesIdx*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      += %@_struct->speciesData_F2[speciesIdx*%@_struct->idx_pitch+ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendString: @"}\n"];
  
  } else if ([numericalScheme isEqualToString: @"AdaptiveRungeKuttaFehlberg"]) {

    // F1 kernel
    [GPUCode appendString: @"\n// F1 Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernelF1_rkf(%@ currentTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesNum >= %@_struct->speciesCount) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];

    [GPUCode appendString: @"\n   // calculate F1\n"];
    [GPUCode appendFormat: @"   %@_struct->speciesData_tmp[speciesNum*%@_struct->idx_pitch+ODEnum] = 0;\n", prefixName, prefixName];
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, speciesNum, %@_struct->idx_pitch, %@_struct->speciesData,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_tmp, %@_struct->speciesData_F1, %@_struct->parameters,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->currentDelta[ODEnum], 0.0f);\n", prefixName];
    
    [GPUCode appendString: @"}\n"];

    // F2 kernel
    [GPUCode appendString: @"\n// F2 Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernelF2_rkf(%@ currentTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesNum >= %@_struct->speciesCount) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // calculate F2\n"];
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, speciesNum, %@_struct->idx_pitch, %@_struct->speciesData,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_F1, %@_struct->speciesData_F2, %@_struct->parameters,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->currentDelta[ODEnum], 0.25f);\n", prefixName];
    
    [GPUCode appendString: @"}\n"];

    // F3 kernel
    [GPUCode appendString: @"\n// F3 Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernelF3_rkf(%@ currentTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesNum >= %@_struct->speciesCount) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // calculate F3\n"];
    [GPUCode appendFormat: @"   %@_struct->speciesData_tmp[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      = (3.0f / 32.0f) * %@_struct->speciesData_F1[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (9.0f / 32.0f) * %@_struct->speciesData_F2[speciesNum*%@_struct->idx_pitch+ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, speciesNum, %@_struct->idx_pitch, %@_struct->speciesData,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_tmp, %@_struct->speciesData_F3, %@_struct->parameters,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->currentDelta[ODEnum], 1.0f);\n", prefixName];
    
    [GPUCode appendString: @"}\n"];

    // F4 kernel
    [GPUCode appendString: @"\n// F4 Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernelF4_rkf(%@ currentTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesNum >= %@_struct->speciesCount) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // calculate F4\n"];
    [GPUCode appendFormat: @"   %@_struct->speciesData_tmp[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      = (1932.0f / 2197.0f) * %@_struct->speciesData_F1[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      - (7200.0f / 2197.0f) * %@_struct->speciesData_F2[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (7296.0f / 2197.0f) * %@_struct->speciesData_F3[speciesNum*%@_struct->idx_pitch+ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, speciesNum, %@_struct->idx_pitch, %@_struct->speciesData,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_tmp, %@_struct->speciesData_F4, %@_struct->parameters,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->currentDelta[ODEnum], 1.0f);\n", prefixName];
    
    [GPUCode appendString: @"}\n"];
    
    // F5 kernel
    [GPUCode appendString: @"\n// F5 Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernelF5_rkf(%@ currentTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesNum >= %@_struct->speciesCount) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // calculate F5\n"];
    [GPUCode appendFormat: @"   %@_struct->speciesData_tmp[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      = (439.0f / 216.0f) * %@_struct->speciesData_F1[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      - (8.0f) * %@_struct->speciesData_F2[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (3680.0f / 513.0f) * %@_struct->speciesData_F3[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      - (845.0f / 4104.0f) * %@_struct->speciesData_F4[speciesNum*%@_struct->idx_pitch+ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, speciesNum, %@_struct->idx_pitch, %@_struct->speciesData,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_tmp, %@_struct->speciesData_F5, %@_struct->parameters,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->currentDelta[ODEnum], 1.0f);\n", prefixName];
    
    [GPUCode appendString: @"}\n"];

    // F6 kernel
    [GPUCode appendString: @"\n// F6 Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernelF6_rkf(%@ currentTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesNum >= %@_struct->speciesCount) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // calculate F6\n"];
    [GPUCode appendFormat: @"   %@_struct->speciesData_tmp[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      = (-8.0f / 27.0f) * %@_struct->speciesData_F1[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (2.0f) * %@_struct->speciesData_F2[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      - (3544.0f / 2565.0f) * %@_struct->speciesData_F3[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (1859.0f / 4104.0f) * %@_struct->speciesData_F4[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      - (11.0f / 40.0f) * %@_struct->speciesData_F5[speciesNum*%@_struct->idx_pitch+ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, speciesNum, %@_struct->idx_pitch, %@_struct->speciesData,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_tmp, %@_struct->speciesData_F6, %@_struct->parameters,\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->currentDelta[ODEnum], 1.0f);\n", prefixName];

    [GPUCode appendString: @"}\n"];

    // Update kernel
    [GPUCode appendString: @"\n// Update Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel_update_rkf(%@ currentTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"   if (speciesNum >= %@_struct->speciesCount) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // do not update if exceeded error\n"];
    [GPUCode appendFormat: @"   if (%@_struct->deltaFlag[ODEnum] == 1) return;\n", prefixName];

    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];

    [GPUCode appendString: @"\n   // calculate x(t + dt)\n"];
    [GPUCode appendFormat: @"   %@_struct->speciesData[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      += (16.0f / 135.0f) * %@_struct->speciesData_F1[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (6656.0f / 12825.0f) * %@_struct->speciesData_F3[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (28561.0f / 56430.0f) * %@_struct->speciesData_F4[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      - (9.0f / 50.0f) * %@_struct->speciesData_F5[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (2.0f / 55.0f) * %@_struct->speciesData_F6[speciesNum*%@_struct->idx_pitch+ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendString: @"}\n"];
    
    // Error estimation kernel
    [GPUCode appendString: @"\n// Error estimation Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel_error_rkf(%@ currentTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    [GPUCode appendString: @"   int speciesNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    [GPUCode appendFormat: @"\n   if (speciesNum >= %@_struct->speciesCount) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // calculate error estimate\n"];
    [GPUCode appendFormat: @"   %@_struct->speciesData_error[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      = (1.0f / 360.0f) * %@_struct->speciesData_F1[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      - (128.0f / 4275.0f) * %@_struct->speciesData_F3[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      - (2197.0f / 75240.0f) * %@_struct->speciesData_F4[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (1.0f / 50.0f) * %@_struct->speciesData_F5[speciesNum*%@_struct->idx_pitch+ODEnum]\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      + (2.0f / 55.0f) * %@_struct->speciesData_F6[speciesNum*%@_struct->idx_pitch+ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendString: @"}\n"];

#if 0
    // Adaptive dt adjust kernel
    [GPUCode appendString: @"\n// Adaptive dt Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel_adjust_rkf(float startTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // initialization\n"];
    [GPUCode appendFormat: @"   if (%@_struct->currentDelta[ODEnum] == 0.0f) {\n", prefixName];
    [GPUCode appendFormat: @"      %@_struct->currentDelta[ODEnum] = %@_struct->dt;\n", prefixName, prefixName];
    [GPUCode appendString: @"      return;\n"];
    [GPUCode appendString: @"   }\n"];

    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // check if error is exceeded\n"];
    [GPUCode appendString: @"   int speciesNum, flag = 1;\n"];
    [GPUCode appendFormat: @"   %@_struct->deltaFlag[ODEnum] = 0;\n", prefixName];
    [GPUCode appendFormat: @"   for (speciesNum = 0; speciesNum < %@_struct->speciesCount; ++speciesNum) {\n", prefixName];
    [GPUCode appendFormat: @"      if (%@_struct->speciesData_error[speciesNum*%@_struct->idx_pitch+ODEnum] > GPU_FLOAT_EPS) {\n", prefixName, prefixName];
    [GPUCode appendString: @"\n         // if one species is bad then decrease dt\n"];
    [GPUCode appendFormat: @"         %@_struct->deltaFlag[ODEnum] = 1;\n", prefixName];
    [GPUCode appendString: @"         flag = 0;\n"];
    [GPUCode appendString: @"         break;\n"];
    [GPUCode appendString: @"      } else {\n"];
    
    [GPUCode appendString: @"\n         // check if dt can be increased\n"];
    [GPUCode appendFormat: @"         if (%@_struct->speciesData_error[speciesNum*%@_struct->idx_pitch+ODEnum] < (GPU_FLOAT_EPS / 128.0f)) flag &= 1;\n", prefixName, prefixName];
    [GPUCode appendString: @"         else flag = 0;\n"];
    [GPUCode appendString: @"      }\n"];
    [GPUCode appendString: @"   }\n"];

    [GPUCode appendString: @"\n   // if all species are good then increase dt\n"];
    [GPUCode appendFormat: @"   if (flag) %@_struct->deltaFlag[ODEnum] = 2;\n", prefixName];
        
    [GPUCode appendString: @"\n   // dt adjust smaller, need to recompute\n"];
    [GPUCode appendFormat: @"   if (%@_struct->deltaFlag[ODEnum] == 1) {\n", prefixName];
    [GPUCode appendFormat: @"      %@_struct->Delta[ODEnum] /= 2.0f;\n", prefixName];
    [GPUCode appendString: @"   } else {\n"];

    [GPUCode appendString: @"\n      // update current time\n"];
    [GPUCode appendFormat: @"      %@_struct->currentTime[ODEnum] += %@_struct->currentDelta[ODEnum];\n", prefixName];

    [GPUCode appendString: @"\n      // dt adjust bigger\n"];
    [GPUCode appendFormat: @"      if (%@_struct->deltaFlag[ODEnum] == 2) {\n", prefixName];

    [GPUCode appendString: @"\n         // do not adjust if last run was for a small end of time delta\n"];
    [GPUCode appendFormat: @"         if (%@_struct->currentDelta[ODEnum] == %@_struct->Delta[ODEnum]) {\n", prefixName];
    [GPUCode appendFormat: @"            %@_struct->Delta[ODEnum] *= 2.0f;\n", prefixName];

    [GPUCode appendString: @"\n            // do not get bigger than the total time\n"];
    [GPUCode appendFormat: @"            if (%@_struct->Delta[ODEnum] > (finalTime - startTime)) {\n", prefixName];
    [GPUCode appendFormat: @"               %@_struct->currentDelta[ODEnum] = finalTime - startTime;\n", prefixName];
    [GPUCode appendString: @"            }\n"];
    [GPUCode appendString: @"         }\n"];
    [GPUCode appendString: @"      }\n"];
    [GPUCode appendString: @"   }\n"];

    [GPUCode appendString: @"\n   // have reached final time?\n"];
    [GPUCode appendFormat: @"   %@_struct->timeFlag[ODEnum] = 1;\n", prefixName];
    [GPUCode appendFormat: @"   if (TIME_EQUAL(%@_struct->currentTime[ODEnum], finalTime) || (%@_struct->currentTime[ODEnum] >= finalTime)) %@_struct->timeFlag[ODEnum] &= 1;\n", prefixName, prefixName];
    [GPUCode appendFormat: @"   else %@_struct->timeFlag[ODEnum] = 0;\n", prefixName];
    
    [GPUCode appendString: @"\n   // final delta value\n"];
    [GPUCode appendFormat: @"   if ((%@_struct->currentTime[ODEnum] + %@_struct->Delta[ODEnum]) > finalTime)\n", prefixName];
    [GPUCode appendFormat: @"      %@_struct->currentDelta[ODEnum] = finalTime - %@_struct->currentTime[ODEnum];\n", prefixName, prefixName];
    [GPUCode appendString: @"   else\n"];
    [GPUCode appendFormat: @"      %@_struct->currentDelta[ODEnum] = %@_struct->Delta[ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendString: @"}\n"];
#endif
  
    // Error check kernel
    [GPUCode appendString: @"\n// Error check Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel_check_rkf(%@ startTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // initialization\n"];
    [GPUCode appendFormat: @"   if (%@_struct->currentDelta[ODEnum] == 0.0f) {\n", prefixName];
    [GPUCode appendFormat: @"      %@_struct->currentDelta[ODEnum] = %@_struct->dt;\n", prefixName, prefixName];
    [GPUCode appendString: @"      return;\n"];
    [GPUCode appendString: @"   }\n"];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // check if error is exceeded\n"];
    [GPUCode appendString: @"   int speciesNum, flag = 1;\n"];
    [GPUCode appendFormat: @"   %@_struct->deltaFlag[ODEnum] = 0;\n", prefixName];
    [GPUCode appendFormat: @"   for (speciesNum = 0; speciesNum < %@_struct->speciesCount; ++speciesNum) {\n", prefixName];
    [GPUCode appendFormat: @"      if (%@_struct->speciesData_error[speciesNum*%@_struct->idx_pitch+ODEnum] > GPU_FLOAT_EPS) {\n", prefixName, prefixName];
    [GPUCode appendString: @"\n         // if one species is bad then decrease dt\n"];
    [GPUCode appendFormat: @"         %@_struct->deltaFlag[ODEnum] = 1;\n", prefixName];
    [GPUCode appendString: @"         flag = 0;\n"];
    [GPUCode appendString: @"         break;\n"];
    [GPUCode appendString: @"      } else {\n"];
    
    [GPUCode appendString: @"\n         // check if dt can be increased\n"];
    [GPUCode appendFormat: @"         if (%@_struct->speciesData_error[speciesNum*%@_struct->idx_pitch+ODEnum] < (GPU_FLOAT_EPS / 128.0f)) flag &= 1;\n", prefixName, prefixName];
    [GPUCode appendString: @"         else flag = 0;\n"];
    [GPUCode appendString: @"      }\n"];
    [GPUCode appendString: @"   }\n"];
    
    [GPUCode appendString: @"\n   // if all species are good then increase dt\n"];
    [GPUCode appendFormat: @"   if (flag) %@_struct->deltaFlag[ODEnum] = 2;\n", prefixName];
    
    [GPUCode appendString: @"}\n"];

    // Advance time and adaptive dt kernel
    [GPUCode appendString: @"\n// Advance time and adaptive dt Kernel for Adaptive Runge-Kutta-Fehlberg\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel_time_rkf(%@ startTime, %@ finalTime)\n", prefixName, dType, dType];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    
    [GPUCode appendString: @"\n   // initialization\n"];
    [GPUCode appendFormat: @"   if (%@_struct->currentDelta[ODEnum] == 0.0f) {\n", prefixName];
    [GPUCode appendFormat: @"      %@_struct->currentDelta[ODEnum] = %@_struct->dt;\n", prefixName, prefixName];
    [GPUCode appendString: @"      return;\n"];
    [GPUCode appendString: @"   }\n"];
    
    [GPUCode appendString: @"\n   // do not calculate if at end of time\n"];
    [GPUCode appendFormat: @"   if (%@_struct->timeFlag[ODEnum]) return;\n", prefixName];

    [GPUCode appendString: @"\n   // do not adjust dt smaller than minimum\n"];
    [GPUCode appendFormat: @"   if ((%@_struct->deltaFlag[ODEnum] == 1) && (%@_struct->Delta[ODEnum] < %@_struct->dt)) {\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"      %@_struct->deltaFlag[ODEnum] = 0;\n", prefixName];
    [GPUCode appendString: @"   }\n"];

    [GPUCode appendString: @"\n   // dt adjust smaller, need to recompute\n"];
    [GPUCode appendFormat: @"   if (%@_struct->deltaFlag[ODEnum] == 1) {\n", prefixName];
    [GPUCode appendFormat: @"      %@_struct->Delta[ODEnum] /= 2.0f;\n", prefixName];
    [GPUCode appendString: @"   } else {\n"];
    
    [GPUCode appendString: @"\n      // update current time\n"];
    [GPUCode appendFormat: @"      %@_struct->currentTime[ODEnum] += %@_struct->currentDelta[ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendString: @"\n      // dt adjust bigger\n"];
    [GPUCode appendFormat: @"      if (%@_struct->deltaFlag[ODEnum] == 2) {\n", prefixName];
    
    [GPUCode appendString: @"\n         // do not adjust if last run was for a small end of time delta\n"];
    [GPUCode appendFormat: @"         if (%@_struct->currentDelta[ODEnum] == %@_struct->Delta[ODEnum]) {\n", prefixName, prefixName];
    [GPUCode appendFormat: @"            %@_struct->Delta[ODEnum] *= 2.0f;\n", prefixName];
    
    [GPUCode appendString: @"\n            // do not get bigger than the total time\n"];
    [GPUCode appendFormat: @"            if (%@_struct->Delta[ODEnum] > (finalTime - startTime)) {\n", prefixName];
    [GPUCode appendFormat: @"               %@_struct->currentDelta[ODEnum] = finalTime - startTime;\n", prefixName];
    [GPUCode appendString: @"            }\n"];
    [GPUCode appendString: @"         }\n"];
    [GPUCode appendString: @"      }\n"];
    [GPUCode appendString: @"   }\n"];
    
    [GPUCode appendString: @"\n   // have reached final time?\n"];
    [GPUCode appendFormat: @"   %@_struct->timeFlag[ODEnum] = 1;\n", prefixName];
    [GPUCode appendFormat: @"   if (TIME_EQUAL(%@_struct->currentTime[ODEnum], finalTime) || (%@_struct->currentTime[ODEnum] >= finalTime)) %@_struct->timeFlag[ODEnum] &= 1;\n", prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"   else %@_struct->timeFlag[ODEnum] = 0;\n", prefixName];
    
    [GPUCode appendString: @"\n   // final delta value\n"];
    [GPUCode appendFormat: @"   if ((%@_struct->currentTime[ODEnum] + %@_struct->Delta[ODEnum]) > finalTime)\n", prefixName, prefixName];
    [GPUCode appendFormat: @"      %@_struct->currentDelta[ODEnum] = finalTime - %@_struct->currentTime[ODEnum];\n", prefixName, prefixName];
    [GPUCode appendString: @"   else\n"];
    [GPUCode appendFormat: @"      %@_struct->currentDelta[ODEnum] = %@_struct->Delta[ODEnum];\n", prefixName, prefixName];
    
    [GPUCode appendString: @"}\n"];
    
  } else {
    printf("ERROR: unknown numerical scheme: %s\n", [numericalScheme UTF8String]);
    return nil;
  }
  
  return GPUCode;
}


- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName { return nil; }

@end

//
// Run GPU
//

@implementation DynamicReactionRuleModel (GPURun)

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions
{
  ModelGPUData *gpuData = (ModelGPUData *)malloc(sizeof(ModelGPUData));
  
  //printf("DynamicReactionRuleModel allocGPUData: (%d) withGPUFunctions:\n", numModels);

  gpuData->numModels = numModels;
  gpuData->numParams = [self numOfParameters];
  gpuData->gpuFunctions = gpuFunctions;
  if ([modelController isDoublePrecision]) {
    gpuData->data = malloc(sizeof(double) * numModels * numOfSpecies * multiplicity);
    gpuData->parameters = (double *)malloc(sizeof(double) * numModels * gpuData->numParams);
  } else {
    gpuData->data = malloc(sizeof(float) * numModels * numOfSpecies * multiplicity);
    gpuData->parameters = (float *)malloc(sizeof(float) * numModels * gpuData->numParams);
  }
  gpuData->flags = malloc(sizeof(int) * numModels * numOfSpecies * multiplicity);
  
  return gpuData;
}

- (void)freeGPUData: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if (gpuData) {
    if (gpuData->data) free(gpuData->data);
    if (gpuData->flags) free(gpuData->flags);
    if (gpuData->parameters) free(gpuData->parameters);
    free(gpuData);
  }
}

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int j;
  
  if ([modelController isDoublePrecision]) {
    double *indData = dataResults;
    double (*hostData)[numOfSpecies * multiplicity][gpuData->numModels] = (void *)gpuData->data;
    int (*hostFlags)[numOfSpecies * multiplicity][gpuData->numModels] = (void *)gpuData->flags;
    
    if (aFlag)
      for (j = 0; j < (numOfSpecies * multiplicity); ++j) {
        (*hostData)[j][aNum] = indData[j];
        (*hostFlags)[j][aNum] = dataFlags[j];
        //printf("toGPU (%d, %d): %lf %lf, %d %d\n", aNum, j, (*hostData)[j][aNum], indData[j], (*hostFlags)[j][aNum], dataFlags[j]);
      }
    else
      for (j = 0; j < (numOfSpecies * multiplicity); ++j) {
        indData[j] = (*hostData)[j][aNum];
        //printf("fromGPU (%d, %d): %lf %lf\n", aNum, j, (*hostData)[j][aNum], indData[j]);
      }
  } else {
    float *indData = dataResults;
    float (*hostData)[numOfSpecies * multiplicity][gpuData->numModels] = (void *)gpuData->data;
    int (*hostFlags)[numOfSpecies * multiplicity][gpuData->numModels] = (void *)gpuData->flags;
    
    if (aFlag)
      for (j = 0; j < (numOfSpecies * multiplicity); ++j) {
        (*hostData)[j][aNum] = indData[j];
        (*hostFlags)[j][aNum] = dataFlags[j];
        //printf("toGPU (%d, %d): %f %f\n", aNum, j, (*hostData)[j][aNum], indData[j]);
      }
    else
      for (j = 0; j < (numOfSpecies * multiplicity); ++j) {
        indData[j] = (*hostData)[j][aNum];
        //printf("fromGPU (%d, %d): %f %f\n", aNum, j, (*hostData)[j][aNum], indData[j]);
      }
  }
}

- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int p = 0;
  int k;
  
  if ([modelController isDoublePrecision]) {
    double (*hostParameters)[gpuData->numParams][gpuData->numModels] = (void *)gpuData->parameters;
    
    if (aFlag) {
      for (k = 0; k < numOfParameters; ++k) {
        (*hostParameters)[p][aNum] = parameterValues[k];
        if ([self isDebug]) printf("%p %d %d: %f\n", self, p, aNum, parameterValues[k]);
        ++p;
      }
    } else {
      // Transfer parameters from GPU??
    }
  } else {
    float (*hostParameters)[gpuData->numParams][gpuData->numModels] = (void *)gpuData->parameters;
  
    if (aFlag) {
      for (k = 0; k < numOfParameters; ++k) {
        (*hostParameters)[p][aNum] = parameterValues[k];
        if ([self isDebug]) printf("%p %d %d: %f\n", self, p, aNum, parameterValues[k]);
        ++p;
      }
    } else {
      // Transfer parameters from GPU??
    }
  }
  
  return p;
}

- (void)allocGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if ([modelController isDoublePrecision]) {
    RModelDynamicGPUFunctions_double *gpuFunctions = (RModelDynamicGPUFunctions_double *)gpuData->gpuFunctions;
    gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, gpuData->numModels, gpuData->numParams, numOfSpecies, multiplicity, numOfReactions, maxReactions, maxReactants, maxReactionParameters, dt, 0);
  } else {
    RModelDynamicGPUFunctions_float *gpuFunctions = (RModelDynamicGPUFunctions_float *)gpuData->gpuFunctions;
    gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, gpuData->numModels, gpuData->numParams, numOfSpecies, multiplicity, numOfReactions, maxReactions, maxReactants, maxReactionParameters, dt, 0);
  }
}

- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if ([modelController isDoublePrecision]) {
    RModelDynamicGPUFunctions_double *gpuFunctions = (RModelDynamicGPUFunctions_double *)gpuData->gpuFunctions;
    (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, aFlag, gpuData->parameters, gpuData->data, gpuData->flags, speciesReactions, speciesReactionSign, reactionTable, reactionType, reactionParameter);
  } else {
    RModelDynamicGPUFunctions_float *gpuFunctions = (RModelDynamicGPUFunctions_float *)gpuData->gpuFunctions;
    (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, aFlag, gpuData->parameters, gpuData->data, gpuData->flags, speciesReactions, speciesReactionSign, reactionTable, reactionType, reactionParameter);
  }
}

- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if ([modelController isDoublePrecision]) {
    RModelDynamicGPUFunctions_double *gpuFunctions = (RModelDynamicGPUFunctions_double *)gpuData->gpuFunctions;
    (gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, currentTime, endTime);
  } else {
    RModelDynamicGPUFunctions_float *gpuFunctions = (RModelDynamicGPUFunctions_float *)gpuData->gpuFunctions;
    (gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, currentTime, endTime);    
  }
}

- (void)releaseGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if ([modelController isDoublePrecision]) {
    RModelDynamicGPUFunctions_double *gpuFunctions = (RModelDynamicGPUFunctions_double *)gpuData->gpuFunctions;
    (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
  } else {
    RModelDynamicGPUFunctions_float *gpuFunctions = (RModelDynamicGPUFunctions_float *)gpuData->gpuFunctions;
    (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
  }
}


@end
