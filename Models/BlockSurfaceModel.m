/*
 BlockSurfaceModel.m
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BlockSurfaceModel.h"

#import "internal.h"

@implementation BlockSurfaceModel

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent;
{
  int i;
  
  [super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent];
  
  SpatialModel *spatialModel = [modelController spatialModel];
  dimensions = [spatialModel dimensions];
  double *domainSize = [spatialModel domainSize];
  
  NSArray *a = [model objectForKey: @"start"];
  CHECK_PARAM(a, "start");
  if ([a count] != dimensions) {
    printf("Size of start coordinates does not match number of dimensions: %ld != %d\n", (unsigned long)[a count], dimensions);
    return nil;
  }
  
  startCoordinates = (double *)malloc(sizeof(double) * dimensions);
  for (i = 0; i < dimensions; ++i) {
    id s = [a objectAtIndex: i];
    if ([s isEqualToString: @"domain"]) startCoordinates[i] = 0.0;
    else startCoordinates[i] = [s doubleValue];
  }

  a = [model objectForKey: @"end"];
  CHECK_PARAM(a, "end");
  if ([a count] != dimensions) {
    printf("Size of end coordinates does not match number of dimensions: %ld != %d\n", (unsigned long)[a count], dimensions);
    return nil;
  }
  
  endCoordinates = (double *)malloc(sizeof(double) * dimensions);
  for (i = 0; i < dimensions; ++i) {
    id s = [a objectAtIndex: i];
    if ([s isEqualToString: @"domain"]) endCoordinates[i] = domainSize[i];
    else endCoordinates[i] = [s doubleValue];
  }
  
  return self;
}

@end

//
// Visualization
//

@implementation BlockSurfaceModel (Povray)

- (NSMutableString *)povrayCode
{
  NSMutableString *povray = [NSMutableString new];

  [povray appendString: @"box {\n"];
  [povray appendFormat: @"  <%lf, %lf, %lf>, <%lf, %lf, %lf>\n", startCoordinates[0], startCoordinates[1], startCoordinates[2],
   endCoordinates[0], endCoordinates[1], endCoordinates[2]];
  [povray appendString: @"  texture {\n"];
  [povray appendString: @"    pigment { rgb <1.0, 0.5, 0.5> }\n"];
  [povray appendString: @"  }\n"];
  [povray appendString: @"}\n"];

  return povray;
}

@end
