/*
 CellColonyModel.h
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "BioSwarmModel.h"

@interface CellColonyModel : BioSwarmModel
{
  // cells
  int numOfCells;
  NSMutableArray *cells;
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent;

- (int)numOfCells;
- (void)setMultiplicityForSubmodels;

#if 0
- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState;
- (BOOL)writeDataWithCheck: (BOOL)aFlag;
- (BOOL)readData;
- (void)rewindFiles;
- (void)initializeSimulation;
- (void)runSimulation;
- (void)cleanupSimulation;
#endif
@end

//
// GPU
//
#if 0
@interface CellColonyModel (GPU)
- (NSDictionary *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName;
@end

//
// Run GPU
//

@interface CellColonyModel (GPURun)
- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions;
- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (void)allocGPUKernel: (void *)data;
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUKernel: (void *)data;
@end

//
// Visualization
//

@interface CellColonyModel (Povray)
- (NSMutableString *)povrayCode;
@end
#endif
