/*
 SubcellularElementModel.h
 
 subcellular element method for cell spatial representation.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "MultiScale2DGrid.h"
#import "BioSwarmModel.h"

@interface SubcellularElementModel : BioSwarmModel
{
  // model
  int numOfCells;
  int maxCells;
  int initialElements;
  int numElements;
  int maxElements;
  int totalElements;
  NSMutableArray *cells;
  //NSString *dataEncode;
  //int *numOfElements;
  int *cellNumber;
  MultiScale2DGrid *elementData;
  BOOL useMesh;
  int *meshNeighbors[19];
  //int numOfParameters;

  // element neighbors
  int *elementNeighborTable;
  
  // sectors
  int maxElementsPerSector;
  int maxSectors;
  double sectorSize;
  int sectorsInDomain;
  BOOL readOnlySectorBorder;
  int numSectors;
  int *sectorNeighborTable;
  int *sectorCoordinates;
  int *sectorElementTable;
  int *sectorReadOnlyFlag;
  
  FILE *dataFile;
  int dataFileState;
  FILE *meshFile;

  // cell parameters
  double cellRadius;

  // physical parameters
  double viscousTimescale;
  double elasticModulus;

  // force parameters
  NSMutableArray *forces;
  BOOL calcCellCenters;
  BOOL calcElementCenters;
  //double *rho;
  //double *factor;
  //double *eqSquared;
  double interactionMaxSquared;

  // element forces
  NSMutableDictionary *forceMap;
  NSMutableDictionary *forceTypeMap;
  int maxElementTypes;
  int maxForces;
  void *elementForceTable;
  void *elementForceTypeTable;
  void *elementTypeTable;
  void *elementForceFlagTable;

  // noise/diffusion parameters
  BOOL useDiffusion;
  double diffusionCoefficient;

  // initialization
  double *startRange;
  double *range;
  double *offset;
  double randMult;
  double randOffset;
  int sectorBorder;

  // MPI
  id *elementMapping;
  id *markedElements;
  NSMutableArray *availableElements;
  void *incomingSectors;
  void *outgoingSectors;
  void *incomingElements;
  void *outgoingElements;
  void *receiveRequests;
  void *sendRequests;
  void *saveMPIData;
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent;

- (int)numOfCells;
//- (int)maxCells;
//- (int *)numOfElements;
- (int)maxElements;
- (int)totalElements;
- (void *)elementX;
- (void *)elementY;
- (void *)elementZ;
- (void *)elementType;
- (void *)elementCellNumber;
- (double)maxInteractionRange;

- (int)maxElementsPerSector;
- (int)maxSectors;
- (double)sectorSize;
- (int)numSectors;
- (int *)sectorNeighborTable;
- (int *)sectorCoordinates;

- (void)addElementForCell: (int)cellNum;
- (void)divideCell: (int)cellNum;

- (void)setupDataFilesWithState: (int)aState;
- (void)allocateSimulationWithEncode: (NSString *)anEncode;
- (void)initializeSimulation;
- (BOOL)writeDataWithCheck: (BOOL)aFlag;
- (BOOL)readData;
- (void)rewindFiles;
- (void)cleanupSimulation;

@end

//
// GPU
//
#import "GPUDefines.h"

//
// Produce GPU code
//
@interface SubcellularElementModel (GPU)
- (NSDictionary *)controllerGPUCode: (NSString *)prefixName;
//- (NSMutableString *)fullControllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSDictionary *)splitKernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName;
@end

//
// Run GPU code
//
@interface SubcellularElementModel (GPURun)
- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions;
- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (void)allocGPUKernel: (void *)data;
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUKernel: (void *)data;
@end

//
// Visualization
//

@interface SubcellularElementModel (Povray)
- (NSMutableString *)povrayCode;
@end

//
// MPI
//
@interface SubcellularElementModel (MPI)
- (void)allocateMPISimulationWithEncode: (NSString *)anEncode;
- (void)initializeMPISimulation;
- (void)initializeMPIFileCompleted;
@end
