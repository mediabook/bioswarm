/*
 BioSwarmModel.h
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: April 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "BiologicalModel.h"

@interface BioSwarmModel : NSObject <BiologicalModel>
{
  id delegate;
  
  // model
  NSString *name;
  NSString *keyName;
  id modelController;
  NSDictionary *model;
  NSArray *spatialContext;

  NSString *dataEncode;
  int encodeTag;

  NSArray *species;
  int numOfSpecies;
  void *speciesData;

  int multiplicity;

  NSString *numericalScheme;
  double dt;
  int timeScale;
  
  // model hierarchy
  id parentModel;
  NSMutableArray *childModels;
  int modelNumber;
  id schedule;
  
  BOOL isDebug;
  BOOL isBinaryFile;
  int runNumber;
  NSString *outputSuffix;
  
  int numOfParameters;
  NSMutableArray *parameterNames;
  double *parameterValues;
  
  // for random search
  double *paramMin;
  double *paramMax;
}
- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj;
- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent;

- newFromModelIndividual;

- (NSDictionary *)model;
- (NSString *)modelName;
- (id)modelController;
- (id)parentModel;
- (NSMutableArray *)childModels;
- (void)addChildModel: aModel;
- (int)modelNumber;
- (void)setModelNumber: (int)aNum;
- modelWithNumber: (int)aNum;
- (NSString *)keyName;
- modelWithKey: (NSString *)aKey;
- (int)numOfModelObjects;

- (SpatialModel *)spatialModel;
- (SpatialModel *)spatialModelForContext: aModel;

- (id)modelOfTypeClass: (Class)aClass;

- (void)setMultiplicity: (int)aNum;
- (int)multiplicity;

- (double)dt;
- (int)actionTimeScale;

- (id)delegate;
- (void)setDelegate: anObj;

- (BOOL)isDebug;
- (void)setDebug: (BOOL)aFlag;

- (BOOL)isBinaryFile;
- (void)setBinaryFile: (BOOL)aFlag;

- (void)setRunNumber: (int)aNum;
- (int)runNumber;

// species operations
- (int)numOfSpecies;
- (NSString *)nameOfSpecies: (int)theSpecies;
- (id)valueOfSpecies: (int)theSpecies withMultiplicity: (int)aNum;
- (BOOL)hierarchySpeciesList:(NSMutableArray *)sNames indexes:(NSMutableArray *)sIndexes
                modelNumbers:(NSMutableArray *)sModelNumbers
                   modelKeys:(NSMutableArray *)sModelKeys;

// manage simulation
// pass messages down the model hierarchy
- (int)numModelsInHierarchy;
- (double)smallestDTInHierarchy;
- (int)largestTimeScaleInHierarchy;

- (void)performHierarchyInvocation: (NSInvocation *)anInvocation;
- (void)setupHierarchyDataFilesWithState: (int)aState;
- (void)allocateHierarchyWithEncode:(NSString *)anEncode;
- (void)initializeHierarchy;
- (void)initializeFileCompleted;
- (BOOL)writeHierarchyDataWithCheck: (BOOL)aFlag;
- (BOOL)readHierarchyData;
- (void)rewindHierarchyFiles;
- (void)stepSimulation;
- (void)cleanupHierarchy;

// these are implemented by subclasses
- (void)setupDataFilesWithState: (int)aState;
- (void)allocateSimulationWithEncode: (NSString *)anEncode;
- (void)initializeSimulation;
- (BOOL)writeDataWithCheck: (BOOL)aFlag;
- (BOOL)readData;
- (void)rewindFiles;
- (void)cleanupSimulation;

// parameter operations
- (int)numOfParameters;
- (NSString *)nameOfParameter: (int)theParam;

- (BOOL)hierarchyParameterList:(NSArray *)includeList exclude:(NSArray *)excludeList
                         names:(NSMutableArray *)pNames indexes:(NSMutableArray *)pIndexes
                  modelNumbers:(NSMutableArray *)pModelNumbers
                     modelKeys:(NSMutableArray *)pModelKeys;

- (BOOL)setupParameterRanges: (NSDictionary *)ranges doCheck: (BOOL)aFlag;
- (BOOL)setupParameterRanges: (NSDictionary *)ranges checkNames: (NSArray *)names;
- (double)randomValueforParameter: (int)aParam;
- (double)perturbValueForParameter: (int)aParam withinRange: (BOOL)aFlag standardDeviation: (double)SD;
- (int)modifyRandomParameter: (NSMutableDictionary *)params;
- (double *)parameterMinRange;
- (double *)parameterMaxRange;

// perturbation operations
// These should be implemented by subclasses
- (BOOL)setValueForAllSpecies: (double)aValue;
- (BOOL)setValue: (double)aValue forSpecies: (NSString *)speciesName;
- (BOOL)adjustValue: (double)aValue forSpecies: (NSString *)speciesName;
- (BOOL)knockoutSpecies: (NSString *)speciesName;
- (BOOL)setValue: (double)aValue forParameter: (NSString *)pName;

// encoding strings
+ (NSString *)idEncode;
+ (NSString *)intEncode;
+ (NSString *)longEncode;
+ (NSString *)floatEncode;
+ (NSString *)doubleEncode;
+ (NSString *)boolEncode;

@end

//
// Definitions
//

// Encode tags
#define BCENCODE_INT 1
#define BCENCODE_LONG 2
#define BCENCODE_FLOAT 3
#define BCENCODE_DOUBLE 4
#define BCENCODE_ID 5
#define BCENCODE_BOOL 6

//
// Generate GPU code
//

@interface BioSwarmModel (GPU) <BiologicalModelGPU>
- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName;
@end

//
// Run GPU
//

@interface BioSwarmModel (GPURun) <BiologicalModelGPURun>

// pass messages down the model hierarchy
- (void)allocateHierarchyGPUData: (ModelGPUData **)gpuData modelInstances: (int)numInstances;
- (void)freeHierarchyGPUData: (ModelGPUData **)gpuData;
- (void)assignHierarchyDataGPUData: (ModelGPUData **)gpuData ofInstance: (int)aNum toGPU: (BOOL)aFlag;
- (void)assignHierarchyParametersGPUData: (ModelGPUData **)gpuData ofInstance: (int)aNum toGPU: (BOOL)aFlag;
- (void)transferHierarchyGPUData: (ModelGPUData **)gpuData toGPU: (BOOL)aFlag;
- (void)invokeHierarchyGPUData: (ModelGPUData **)gpuData currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseHierarchyGPUData: (ModelGPUData **)gpuData;

// these are implemented by subclasses
- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions;
- (void)freeGPUData: (void *)data;

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;

- (void)allocGPUKernel: (void *)data;
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUKernel: (void *)data;
@end

//
// Visualization
//

@interface BioSwarmModel (Povray) <BiologicalModelPovray>
- (void)povrayHierarchyCode: (NSMutableString *)povrayCode;
- (NSMutableString *)povrayCode;
@end

//
// MPI and GPU
//
@interface BioSwarmModel (GPUMPI) <BiologicalModelGPU>
- (NSMutableDictionary *)controllerGPUMPICode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUMPICode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUMPICode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUMPICode: (NSString *)prefixName;
@end

@interface BioSwarmModel (GPUMPIRun)
// pass messages down the model hierarchy
- (void)allocateHierarchyGPUMPIData: (ModelGPUData **)gpuData modelInstances: (int)numInstances;
- (void)freeHierarchyGPUMPIData: (ModelGPUData **)gpuData;
- (void)transferHierarchyGPUMPIData: (ModelGPUData **)gpuData toGPU: (BOOL)aFlag;
- (void)releaseHierarchyGPUMPIData: (ModelGPUData **)gpuData;

// these are implemented by subclasses
- (void)allocGPUMPIKernel: (void *)data;
- (void)transferGPUMPIKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUMPIKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUMPIKernel: (void *)data;

- (void)initiateGPUMPIData: (void *)data mode: (int)aMode;
- (void)finalizeGPUMPIData: (void *)data mode: (int)aMode;
@end
