/*
 InterfaceModel.m
 
 Rules for interactions between models.
 
 Copyright (C) 2014 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2014
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "InterfaceModel.h"
#import "BioSwarmController.h"
#import "CodeGenerationManager.h"

#import "internal.h"

@implementation InterfaceModel

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  int i, j, k;
  NSString *param;
  
  [super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent];

  interactionRules = [aModel objectForKey: @"interactions"];
  numOfRules = [interactionRules count];

  // go through each rule and setup species functions
  for (i = 0; i < numOfRules; ++i) {
    NSDictionary *r = [interactionRules objectAtIndex: i];
    NSString *ruleName = [r objectForKey: @"name"];
    NSArray *pList = [r objectForKey: @"products"];
    NSArray *pmList = [r objectForKey: @"productModels"];
    NSArray *rList = [r objectForKey: @"reactants"];
    NSArray *rmList = [r objectForKey: @"reactantModels"];
    NSString *rType = [r objectForKey: @"type"];
    NSString *rateFunction = [r objectForKey: @"rate"];
    NSString *mapFunction = [r objectForKey: @"mapping"];

    if (![pList isKindOfClass: [NSArray class]]) printf("ERROR: products for rule (%s) is not an array.\n", [ruleName UTF8String]);
    if (![pmList isKindOfClass: [NSArray class]]) printf("ERROR: productModels for rule (%s) is not an array.\n", [ruleName UTF8String]);
    if (![rList isKindOfClass: [NSArray class]]) printf("ERROR: reactants for rule (%s) is not an array.\n", [ruleName UTF8String]);
    if (![rmList isKindOfClass: [NSArray class]]) printf("ERROR: reactantModels for rule (%s) is not an array.\n", [ruleName UTF8String]);
  }

  // register for notifications
  // need to wait for the full model hierarchy to be loaded, so we can connect models
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(connectInteractions)
                                               name:BioSwarmModelIsLoadedNotification object:nil];

  return self;
}

- (void)dealloc
{
  [parameterNames release];
  if (parameterValues) free(parameterValues);
  
  [super dealloc];
}

// once the full model hierarchy is loaded, we can check the rules
// and form the model connections
- (void)connectInteractions
{
  int i, j, k;

  printf("InterfaceModel -connectInteractions\n");

  // verify that referenced models and species exist
  modelSet = [NSMutableSet new];
  for (i = 0; i < numOfRules; ++i) {
    NSDictionary *r = [interactionRules objectAtIndex: i];
    NSString *ruleName = [r objectForKey: @"name"];
    NSArray *pList = [r objectForKey: @"products"];
    NSArray *pmList = [r objectForKey: @"productModels"];
    NSArray *rList = [r objectForKey: @"reactants"];
    NSArray *rmList = [r objectForKey: @"reactantModels"];

    for (j = 0; j < [pmList count]; ++j) {
      NSString *pmName = [pmList objectAtIndex: j];
      id m = [modelController modelWithKey: pmName];
      if (!m) printf("ERROR: Cannot find productModel: %s\n", [pmName UTF8String]);
      [modelSet addObject: pmName];

      NSString *pName = [pList objectAtIndex: j];
      int ns = [m numOfSpecies];
      BOOL found = NO;
      for (k = 0; k < ns; ++k) {
        if ([pName isEqualToString: [m nameOfSpecies:k]]) { found = YES; break; }
      }
      if (!found) printf("ERROR: Cannot find product species (%s) in productModel (%s).\n", [pName UTF8String], [pmName UTF8String]);
    }

    for (j = 0; j < [rmList count]; ++j) {
      NSString *rmName = [rmList objectAtIndex: j];
      id m = [modelController modelWithKey: rmName];
      if (!m) printf("ERROR: Cannot find productModel: %s\n", [rmName UTF8String]);
      [modelSet addObject: rmName];
      
      NSString *rName = [rList objectAtIndex: j];
      int ns = [m numOfSpecies];
      BOOL found = NO;
      for (k = 0; k < ns; ++k) {
        if ([rName isEqualToString: [m nameOfSpecies:k]]) { found = YES; break; }
      }
      if (!found) printf("ERROR: Cannot find reactant species (%s) in reactantModel (%s).\n", [rName UTF8String], [rmName UTF8String]);
    }
  }
  printf("modelSet = %s\n", [[modelSet description] UTF8String]);
}
@end

//
// GPU
//

@implementation InterfaceModel (GPU)

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendString: @"\n// structure for GPU data\n"];
  [GPUCode appendFormat: @"typedef struct _%@_GPUgrids {\n", prefixName];
  [GPUCode appendFormat: @"  int numOfModels;\n"];
  [GPUCode appendFormat: @"  float dt;\n"];
  [GPUCode appendFormat: @"  void *gpuData;\n"];
  [GPUCode appendFormat: @"} %@_GPUgrids;\n", prefixName];
  
  [GPUCode appendFormat: @"\n__constant__ %@_GPUgrids %@_struct[1];\n", prefixName, prefixName];
  [GPUCode appendString: @"\n"];
  
  return GPUCode;
}

- (NSDictionary *)controllerGPUCode: (NSString *)prefixName
{
  int dims = [[super spatialModel] dimensions];
  NSEnumerator *enumerator;
  id value;

  // collect referenced models and their parents
  NSMutableSet *refModels = [NSMutableSet setWithSet: modelSet];
  enumerator = [modelSet objectEnumerator];
  while ((value = [enumerator nextObject])) {
    id m = [modelController modelWithKey: value];
    while (m) {
      if ([m keyName]) [refModels addObject: [m keyName]];
      m = [m parentModel];
    }
  }
  printf("refModels = %s\n", [[refModels description] UTF8String]);

  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;
  
  //
  // Headers and defines
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <stdio.h>\n"];
  [GPUCode appendString: @"#include <unistd.h>\n\n"];
  [GPUCode appendString: @"extern \"C\" {\n"];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];
  
  //
  // Allocation
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"*%@_allocGPUKernel(void *model, int numOfModels, void *gpuData, float dt)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"allocation function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @"  %@_GPUgrids *ptrs = (%@_GPUgrids *)malloc(sizeof(%@_GPUgrids));\n", prefixName, prefixName, prefixName];

  [GPUCode appendString: @" ptrs->numOfModels = numOfModels;\n"];
  [GPUCode appendString: @" ptrs->dt = dt;\n"];
  [GPUCode appendString: @" ptrs->gpuData = gpuData;\n"];
  [GPUCode appendString: @"\n return ptrs;\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"allocation code"];

  //
  // Data Transfer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *g, int aFlag)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  //[GPUCode appendString: @"\n BC_SEM_transferGPUKernel(model, g, aFlag, hostParameters, cellNumber, numOfCells, totalElements, hostX, hostY, hostZ, hostType, hostSector, hostElementNeighborTable, EFT, EFTT, ETT, EFFT, hostData, meshElements, hostSectorCoordinates, hostNumSectors, hostSectorSize, hostSectorElementTable, hostSectorNeighborTable);\n"];
  //[GPUCode appendString: @"\n if (aFlag) {\n"];
  //[GPUCode appendString: @"   // Copy data structure\n"];
  //[GPUCode appendFormat: @"   cudaMemcpyToSymbol(%@_sem_grids, g, sizeof(BC_SEM_GPUgrids), 0, cudaMemcpyHostToDevice);\n", prefixName];
  
  //[GPUCode appendString: @" }\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];
  
  //
  // Execution
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, double startTime, double nextTime)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" %@_GPUgrids *%@_ptrs = (%@_GPUgrids *)%@_data;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" ModelGPUData** %@_gpuData = (ModelGPUData **)%@_ptrs->gpuData;\n", prefixName, prefixName];

  enumerator = [refModels objectEnumerator];
  while ((value = [enumerator nextObject])) {
    id m = [modelController modelWithKey: value];
    NSString *s = [[modelController manager] prefixForModelIndex: [m modelNumber]];
    NSDictionary *d = [m controllerGPUCode: s];
    NSString *gc = [d objectForKey: @"structure"];
    if (gc) {
      [GPUCode appendFormat: @" void* %@_data = %@_gpuData[%d]->gpuPtrs;\n", s, prefixName, [m modelNumber]];
      [GPUCode appendString: gc];
    }
  }
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" int %@_x = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_y = 1;\n", prefixName];
  //[GPUCode appendFormat: @" if (%@_x > %@_ptrs->totalElements) %@_x = %@_ptrs->totalElements;\n", prefixName, prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @" if (%@_y > %@_ptrs->numOfModels) %@_y = %@_ptrs->numOfModels;\n", prefixName, prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @" dim3 %@_threadsPerBlock(%@_x, %@_y);\n", prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @" dim3 %@_threadsPerGrid((%@_ptrs->totalElements + %@_threadsPerBlock.x - 1) / %@_threadsPerBlock.x, (%@_ptrs->numOfModels + %@_threadsPerBlock.y - 1) / %@_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  enumerator = [refModels objectEnumerator];
  while ((value = [enumerator nextObject])) {
    id m = [modelController modelWithKey: value];
    NSString *s = [[modelController manager] prefixForModelIndex: [m modelNumber]];
    NSDictionary *d = [m controllerGPUCode: s];
    NSString *gc = [d objectForKey: @"execution variables"];
    if (gc) [GPUCode appendString: gc];
  }

  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];
  
  GPUCode = [NSMutableString new];
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];
  
  //
  // Release
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_releaseGPUKernel(void *model, void *g)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"release function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"release code"];

  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"InterfaceGPUFunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];
  
  return GPUDictionary;
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  int k;
  //int numParams = [self numOfParameters];
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  return GPUCode;
}

- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName
{
  return nil;
}

@end

//
// Run GPU
//

@implementation InterfaceModel (GPURun)

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions
{
  ModelGPUData *gpuData = (ModelGPUData *)malloc(sizeof(ModelGPUData));
  
  gpuData->numModels = numModels;
  gpuData->numParams = [self numOfParameters];
  gpuData->gpuFunctions = gpuFunctions;
  //gpuData->data = malloc(sizeof(float) * numModels * numOfSpecies * multiplicity);
  //gpuData->flags = malloc(sizeof(int) * numModels * numOfSpecies * multiplicity);
  //gpuData->parameters = (float *)malloc(sizeof(float) * numModels * gpuData->numParams);
  
  //gpuData->extraData = (void *)[NSMutableDictionary new];
  
  return gpuData;
}

- (void)freeGPUData: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if (gpuData) {
    //if (gpuData->data) free(gpuData->data);
    //if (gpuData->flags) free(gpuData->flags);
    //if (gpuData->parameters) free(gpuData->parameters);
    //if (gpuData->extraData) [((NSMutableDictionary *)gpuData->extraData) release];
    free(gpuData);
  }
}

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  //ModelGPUData *gpuData = (ModelGPUData *)data;
  //int j;
}

- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  //ModelGPUData *gpuData = (ModelGPUData *)data;
  int p = 0;
  
  return p;
}

- (void)allocGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  ModelGPUData **allGPUData = [modelController gpuData];
  
  InterfaceGPUFunctions *gpuFunctions = (InterfaceGPUFunctions *)gpuData->gpuFunctions;
  gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, gpuData->numModels, allGPUData, (float)dt);
}

- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  InterfaceGPUFunctions *gpuFunctions = (InterfaceGPUFunctions *)gpuData->gpuFunctions;
  (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, aFlag);
}

- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  InterfaceGPUFunctions *gpuFunctions = (InterfaceGPUFunctions *)gpuData->gpuFunctions;
  (gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, currentTime, endTime);
}

- (void)releaseGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  InterfaceGPUFunctions *gpuFunctions = (InterfaceGPUFunctions *)gpuData->gpuFunctions;
  (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
}


@end
