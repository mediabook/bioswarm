/*
 SpatialModel.m
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "SpatialModel.h"
#import "MultiScale2DGrid.h"
#import "MultiScale3DGrid.h"
#import "internal.h"


@implementation SpatialModel

- newFromModelIndividual
{
  return [[[self class] alloc] initWithModel: model];
}

- initWithModel: (NSDictionary *)aModel
{
  int i;
  
  [self init];

  model = [aModel retain];
    
  NSString *s = [model objectForKey: @"dimensions"];
  CHECK_PARAM(s, "dimensions");
  dimensions = [s intValue];
  if ((dimensions < 1) || (dimensions > 3)) {
    printf("Unsupported number of dimensions: %d\n", dimensions);
    return nil;
  }

  NSArray *a = [model objectForKey: @"domainSize"];
  CHECK_PARAM(a, "domainSize");
  if ([a count] != dimensions) {
    printf("Size of domainSize does not match number of dimensions: %ld != %d\n", (unsigned long)[a count], dimensions);
    return nil;
  }
  domainSize = malloc(dimensions * sizeof(double));
  for (i = 0; i < [a count]; ++i)
    domainSize[i] = [[a objectAtIndex: i] doubleValue];

  a = [model objectForKey: @"gridSize"];
  CHECK_PARAM(a, "gridSize");
  if ([a count] != dimensions) {
    printf("Size of gridSize does not match number of dimensions: %ld != %d\n", (unsigned long)[a count], dimensions);
    return nil;
  }
  gridSize = malloc(dimensions * sizeof(int));
  for (i = 0; i < [a count]; ++i)
    gridSize[i] = [[a objectAtIndex: i] intValue];
  
  grid = nil;
  if (dimensions == 2) grid = [[MultiScale2DGrid alloc] initWithWidth: gridSize[0] andHeight: gridSize[1]];
  else if (dimensions == 3) grid = [[MultiScale3DGrid alloc] initWithGridWidth: gridSize[0] height: gridSize[1] depth: gridSize[2]];

  isSubspace = NO;

  return self;
}

- (NSDictionary *)model { return model; }
- (int)dimensions { return dimensions; }
- (double *)domainSize { return domainSize; }
- (int *)gridSize { return gridSize; }
- (id <MultiScaleGrid>)grid { return grid; }

- (BOOL)isSubspace { return isSubspace; }
- (void)setSubspace: (BOOL)aFlag { isSubspace = aFlag; }
- (void)setSubspaceTranslator: anObj { subspaceTranslator = anObj; }

- (void)translateIntegerCoordinatesToWorld: (int *)coordinates forModel: aModel
{
  if (!isSubspace) return;
  if (!subspaceTranslator) return;
  [subspaceTranslator translateIntegerCoordinatesToWorld: coordinates forModel: aModel];
}

- (void)translateFloatCoordinatesToWorld: (float *)coordinates forModel: aModel
{
  if (!isSubspace) return;
  if (!subspaceTranslator) return;
  [subspaceTranslator translateFloatCoordinatesToWorld: coordinates forModel: aModel];
}

- (void)translateFloatCoordinatesToWorld: (float *)coordinates forModel: aModel direction: (int)direction
{
  if (!isSubspace) return;
  if (!subspaceTranslator) return;
  [subspaceTranslator translateFloatCoordinatesToWorld: coordinates forModel: aModel direction: direction];
}

- (void)translateDoubleCoordinatesToWorld: (double *)coordinates forModel: aModel
{
  if (!isSubspace) return;
  if (!subspaceTranslator) return;
  [subspaceTranslator translateDoubleCoordinatesToWorld: coordinates forModel: aModel];
}

- (void)translateIntegerCoordinatesToSubspace: (int *)coordinates forModel: aModel
{
  if (!isSubspace) return;
  if (!subspaceTranslator) return;
  [subspaceTranslator translateIntegerCoordinatesToSubspace: coordinates forModel: aModel];
}

- (void)translateFloatCoordinatesToSubspace: (float *)coordinates forModel: aModel
{
  if (!isSubspace) return;
  if (!subspaceTranslator) return;
  [subspaceTranslator translateFloatCoordinatesToSubspace: coordinates forModel: aModel];
}

- (void)translateDoubleCoordinatesToSubspace: (double *)coordinates forModel: aModel
{
  if (!isSubspace) return;
  if (!subspaceTranslator) return;
  [subspaceTranslator translateDoubleCoordinatesToSubspace: coordinates forModel: aModel];
}

@end

//
// Produce GPU code
//

@implementation SpatialModel (GPU)

- (NSDictionary *)controllerGPUCode: (NSString *)prefixName
{
  return nil;
}

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  NSMutableString *GPUCode = [NSMutableString string];

  [GPUCode appendFormat: @"typedef struct _%@_spatial_GPUparams {\n", prefixName];
  [GPUCode appendString: @"    int dimensions;\n"];
  [GPUCode appendString: @"    float domainX;\n"];
  [GPUCode appendString: @"    float domainY;\n"];
  [GPUCode appendString: @"    float domainZ;\n"];
  [GPUCode appendString: @"    int gridX;\n"];
  [GPUCode appendString: @"    int gridY;\n"];
  [GPUCode appendString: @"    int gridZ;\n"];
  [GPUCode appendFormat: @"} %@_spatial_GPUparams;\n", prefixName];
  
  [GPUCode appendFormat: @"\n__constant__ %@_spatial_GPUparams %@_spatial_parameters[1];\n", prefixName, prefixName];
  
  return GPUCode;
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  return nil;
}

- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName
{
  return nil;
}

@end

//
// Visualization
//

@implementation SpatialModel (Povray)

- (NSMutableString *)povrayCode
{
  NSMutableString *povray = [NSMutableString new];
  
  NSDictionary *d = [model objectForKey: @"povray"];
  NSDictionary *camera = nil;
  if (d) camera = [d objectForKey: @"camera"];
  NSArray *a;
  printf("%s\n", [[d description] UTF8String]);
  
  [povray appendString: @"global_settings {\n"];
  [povray appendString: @"  max_trace_level 10\n"];
  [povray appendString: @"  assumed_gamma 1\n"];
  [povray appendString: @"}\n\n"];
  
  [povray appendString: @"camera {\n"];
  if ((a = [camera objectForKey: @"location"])) {
    [povray appendFormat: @"  location    <%lf, %lf, %lf>\n", [[a objectAtIndex: 0] doubleValue], [[a objectAtIndex: 1] doubleValue], [[a objectAtIndex: 2] doubleValue]];
  } else {
    [povray appendFormat: @"  location    <%lf, %lf, %lf>\n", domainSize[0]*2.0, domainSize[1]*2.0, domainSize[2]*2.0];
  }
  //[povray appendString: @"  direction   y\n"];
  //[povray appendString: @"  sky         z\n"];
  //[povray appendString: @"  up          z\n"];
  //[povray appendString: @"  right       (4/3)*x\n"];
  if ((a = [camera objectForKey: @"lookAt"])) {
    [povray appendFormat: @"  look_at     <%lf, %lf, %lf>\n", [[a objectAtIndex: 0] doubleValue], [[a objectAtIndex: 1] doubleValue], [[a objectAtIndex: 2] doubleValue]];
  } else {
    [povray appendFormat: @"  look_at     <%lf, %lf, 0>\n", domainSize[0]/2.0, domainSize[1]/2.0];
  }
  //[povray appendString: @"  angle       50\n"];
  [povray appendString: @"}\n\n"];

  NSArray *lights = [d objectForKey: @"lightSource"];
  if (lights) {
    int i;
    for (i = 0; i < [lights count]; ++i) {
      NSDictionary *l = [lights objectAtIndex: i];
      [povray appendString: @"light_source {\n"];
      a = [l objectForKey: @"location"];
      if (a) {
        [povray appendFormat: @"  <%lf, %lf, %lf>*500\n", [[a objectAtIndex: 0] doubleValue], [[a objectAtIndex: 1] doubleValue], [[a objectAtIndex: 2] doubleValue]];
      } else {
        [povray appendString: @"  <5, 0.5, 2>*500\n"];
      }
      [povray appendString: @"  color rgb <2.0, 1.5, 1.0>*1.8\n"];
      [povray appendString: @"  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient\n"];
      [povray appendString: @"}\n\n"];
    }
  } else {
    [povray appendString: @"light_source {\n"];
    [povray appendString: @"  <5, 0.5, 2>*500\n"];
    [povray appendString: @"  color rgb <2.0, 1.5, 1.0>*1.8\n"];
    [povray appendString: @"  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient\n"];
    [povray appendString: @"}\n\n"];
  }

  return povray;
}

@end

//
// Sector functions
//

int opposite_sector(int aSector)
{
  switch (aSector) {
    case S_LEFT: return S_RIGHT;
    case S_RIGHT: return S_LEFT;
    case S_UP: return S_DOWN;
    case S_DOWN: return S_UP;
    case S_LEFT_UP: return S_RIGHT_DOWN;
    case S_RIGHT_UP: return S_LEFT_DOWN;
    case S_LEFT_DOWN: return S_RIGHT_UP;
    case S_RIGHT_DOWN: return S_LEFT_UP;
    case S_FRONT: return S_BACK;
    case S_BACK: return S_FRONT;
    case S_LEFT_FRONT: return S_RIGHT_BACK;
    case S_RIGHT_FRONT: return S_LEFT_BACK;
    case S_LEFT_BACK: return S_RIGHT_FRONT;
    case S_RIGHT_BACK: return S_LEFT_FRONT;
    case S_UP_FRONT: return S_DOWN_BACK;
    case S_DOWN_FRONT: return S_UP_BACK;
    case S_UP_BACK: return S_DOWN_FRONT;
    case S_DOWN_BACK: return S_UP_FRONT;
    case S_LEFT_UP_FRONT: return S_RIGHT_DOWN_BACK;
    case S_RIGHT_UP_FRONT: return S_LEFT_DOWN_BACK;
    case S_LEFT_DOWN_FRONT: return S_RIGHT_UP_BACK;
    case S_RIGHT_DOWN_FRONT: return S_LEFT_UP_BACK;
    case S_LEFT_UP_BACK: return S_RIGHT_DOWN_FRONT;
    case S_RIGHT_UP_BACK: return S_LEFT_DOWN_FRONT;
    case S_LEFT_DOWN_BACK: return S_RIGHT_UP_FRONT;
    case S_RIGHT_DOWN_BACK: return S_LEFT_UP_FRONT;
  }
  return aSector;
}

void translate_coordinate(float X, float Y, float Z, int direction, float size,
                          float *newX, float *newY, float *newZ)
{
  // current as default
  *newX = X;
  *newY = Y;
  *newZ = Z;
  
  // translate
  switch (direction) {
    case S_LEFT:
      *newX = X - size; break;
    case S_RIGHT:
      *newX = X + size; break;
    case S_UP:
      *newY = Y + size; break;
    case S_DOWN:
      *newY = Y - size; break;
    case S_LEFT_UP:
      *newX = X - size; *newY = Y + size; break;
    case S_RIGHT_UP:
      *newX = X + size; *newY = Y + size; break;
    case S_LEFT_DOWN:
      *newX = X - size; *newY = Y - size; break;
    case S_RIGHT_DOWN:
      *newX = X + size; *newY = Y - size; break;
    case S_FRONT:
      *newZ = Z - size; break;
    case S_BACK:
      *newZ = Z + size; break;
    case S_LEFT_FRONT:
      *newX = X - size; *newZ = Z - size; break;
    case S_RIGHT_FRONT:
      *newX = X + size; *newZ = Z - size; break;
    case S_LEFT_BACK:
      *newX = X - size; *newZ = Z + size; break;
    case S_RIGHT_BACK:
      *newX = X + size; *newZ = Z + size; break;
    case S_UP_FRONT:
      *newY = Y + size; *newZ = Z - size; break;
    case S_DOWN_FRONT:
      *newY = Y - size; *newZ = Z - size; break;
    case S_UP_BACK:
      *newY = Y + size; *newZ = Z + size; break;
    case S_DOWN_BACK:
      *newY = Y - size; *newZ = Z + size; break;
    case S_LEFT_UP_FRONT:
      *newX = X - size; *newY = Y + size; *newZ = Z - size; break;
    case S_RIGHT_UP_FRONT:
      *newX = X + size; *newY = Y + size; *newZ = Z - size; break;
    case S_LEFT_DOWN_FRONT:
      *newX = X - size; *newY = Y - size; *newZ = Z - size; break;
    case S_RIGHT_DOWN_FRONT:
      *newX = X + size; *newY = Y - size; *newZ = Z - size; break;
    case S_LEFT_UP_BACK:
      *newX = X - size; *newY = Y + size; *newZ = Z + size; break;
    case S_RIGHT_UP_BACK:
      *newX = X + size; *newY = Y + size; *newZ = Z + size; break;
    case S_LEFT_DOWN_BACK:
      *newX = X - size; *newY = Y - size; *newZ = Z + size; break;
    case S_RIGHT_DOWN_BACK:
      *newX = X + size; *newY = Y - size; *newZ = Z + size; break;
  }
}

void fill_sector_from_current(int aSector, int direction, void *sectorGraph, int maxSectors)
{
  int (*graph)[S_XYZ_TOT][maxSectors] = sectorGraph;
  int newSector = (*graph)[direction][aSector];

  switch (direction) {
    case S_LEFT: 
      (*graph)[S_RIGHT][newSector] = aSector;
      (*graph)[S_UP][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_LEFT_UP_FRONT][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_LEFT_DOWN_FRONT][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_LEFT_UP_BACK][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_LEFT_DOWN_BACK][aSector];
      (*graph)[S_RIGHT_UP_FRONT][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_RIGHT_DOWN_FRONT][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_RIGHT_UP_BACK][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_RIGHT_DOWN_BACK][newSector] = (*graph)[S_DOWN_BACK][aSector];
      break;
    case S_RIGHT: 
      (*graph)[S_LEFT][newSector] = aSector;
      (*graph)[S_UP][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_RIGHT_UP_FRONT][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_RIGHT_DOWN_FRONT][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_RIGHT_UP_BACK][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_RIGHT_DOWN_BACK][aSector];
      (*graph)[S_LEFT_UP_FRONT][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_LEFT_DOWN_FRONT][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_LEFT_UP_BACK][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_LEFT_DOWN_BACK][newSector] = (*graph)[S_DOWN_BACK][aSector];
      break;
    case S_UP: 
      (*graph)[S_DOWN][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_RIGHT][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_RIGHT][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_LEFT_UP_FRONT][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_RIGHT_UP_FRONT][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_LEFT_UP_BACK][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_RIGHT_UP_BACK][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_LEFT_DOWN_FRONT][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_RIGHT_DOWN_FRONT][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_LEFT_DOWN_BACK][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_DOWN_BACK][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      break;
    case S_DOWN: 
      (*graph)[S_UP][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_RIGHT][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_DOWN_BACK][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_LEFT_DOWN_FRONT][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_RIGHT_DOWN_FRONT][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_LEFT_DOWN_BACK][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_RIGHT_DOWN_BACK][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_LEFT_UP_FRONT][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_RIGHT_UP_FRONT][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_LEFT_UP_BACK][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_UP_BACK][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      break;
    case S_LEFT_UP: 
      (*graph)[S_RIGHT_DOWN][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_LEFT_UP_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_LEFT_UP_BACK][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_DOWN_FRONT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_RIGHT_DOWN_BACK][newSector] = (*graph)[S_BACK][aSector];
      break;
    case S_RIGHT_UP: 
      (*graph)[S_LEFT_DOWN][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_RIGHT][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_RIGHT_UP_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_RIGHT_UP_BACK][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      (*graph)[S_LEFT_DOWN_FRONT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_LEFT_DOWN_BACK][newSector] = (*graph)[S_BACK][aSector];
      break;
    case S_LEFT_DOWN: 
      (*graph)[S_RIGHT_UP][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_LEFT_DOWN_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_LEFT_DOWN_BACK][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_DOWN_BACK][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_UP_FRONT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_RIGHT_UP_BACK][newSector] = (*graph)[S_BACK][aSector];
      break;
    case S_RIGHT_DOWN: 
      (*graph)[S_LEFT_UP][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_RIGHT][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_RIGHT_DOWN_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_RIGHT_DOWN_BACK][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_DOWN_BACK][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      (*graph)[S_LEFT_UP_FRONT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_LEFT_UP_BACK][newSector] = (*graph)[S_BACK][aSector];
      break;
    case S_FRONT: 
      (*graph)[S_BACK][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_RIGHT][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_LEFT_UP_FRONT][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_RIGHT_UP_FRONT][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_LEFT_DOWN_FRONT][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_RIGHT_DOWN_FRONT][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_RIGHT][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_LEFT_UP_BACK][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_RIGHT_UP_BACK][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_LEFT_DOWN_BACK][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_DOWN_BACK][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      break;
    case S_BACK: 
      (*graph)[S_FRONT][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_DOWN_BACK][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_LEFT_UP_BACK][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_RIGHT_UP_BACK][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_LEFT_DOWN_BACK][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_RIGHT_DOWN_BACK][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_RIGHT][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_LEFT_UP_FRONT][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_RIGHT_UP_FRONT][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_LEFT_DOWN_FRONT][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_DOWN_FRONT][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      break;
    case S_LEFT_FRONT: 
      (*graph)[S_RIGHT_BACK][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_LEFT_UP_FRONT][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_LEFT_DOWN_FRONT][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_UP_BACK][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_RIGHT_DOWN_BACK][newSector] = (*graph)[S_DOWN][aSector];
      break;
    case S_RIGHT_FRONT: 
      (*graph)[S_LEFT_BACK][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_RIGHT_UP_FRONT][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_RIGHT_DOWN_FRONT][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_RIGHT][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      (*graph)[S_LEFT_UP_BACK][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_LEFT_DOWN_BACK][newSector] = (*graph)[S_DOWN][aSector];
      break;
    case S_LEFT_BACK: 
      (*graph)[S_RIGHT_FRONT][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_LEFT_UP_BACK][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_LEFT_DOWN_BACK][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_DOWN_BACK][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_UP_FRONT][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_RIGHT_DOWN_FRONT][newSector] = (*graph)[S_DOWN][aSector];
      break;
    case S_RIGHT_BACK: 
      (*graph)[S_LEFT_FRONT][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_RIGHT_UP_BACK][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_RIGHT_DOWN_BACK][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_DOWN_BACK][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_RIGHT][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      (*graph)[S_LEFT_UP_FRONT][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_LEFT_DOWN_FRONT][newSector] = (*graph)[S_DOWN][aSector];
      break;
    case S_UP_FRONT: 
      (*graph)[S_DOWN_BACK][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_LEFT_UP_FRONT][aSector];
      (*graph)[S_RIGHT][newSector] = (*graph)[S_RIGHT_UP_FRONT][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_LEFT_DOWN_BACK][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_RIGHT_DOWN_BACK][newSector] = (*graph)[S_RIGHT][aSector];
      break;
    case S_DOWN_FRONT: 
      (*graph)[S_UP_BACK][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_LEFT_DOWN_FRONT][aSector];
      (*graph)[S_RIGHT][newSector] = (*graph)[S_RIGHT_DOWN_FRONT][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      (*graph)[S_LEFT_UP_BACK][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_RIGHT_UP_BACK][newSector] = (*graph)[S_RIGHT][aSector];
      break;
    case S_UP_BACK: 
      (*graph)[S_DOWN_FRONT][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_LEFT_UP_BACK][aSector];
      (*graph)[S_RIGHT][newSector] = (*graph)[S_RIGHT_UP_BACK][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_LEFT_DOWN_FRONT][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_RIGHT_DOWN_FRONT][newSector] = (*graph)[S_RIGHT][aSector];
      break;
    case S_DOWN_BACK: 
      (*graph)[S_UP_FRONT][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_LEFT_DOWN_BACK][aSector];
      (*graph)[S_RIGHT][newSector] = (*graph)[S_RIGHT_DOWN_BACK][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      (*graph)[S_LEFT_UP_FRONT][newSector] = (*graph)[S_LEFT][aSector];
      (*graph)[S_RIGHT_UP_FRONT][newSector] = (*graph)[S_RIGHT][aSector];
      break;
    case S_LEFT_UP_FRONT: 
      (*graph)[S_RIGHT_DOWN_BACK][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_LEFT][aSector];
      break;
    case S_RIGHT_UP_FRONT: 
      (*graph)[S_LEFT_DOWN_BACK][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_RIGHT_FRONT][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_LEFT_BACK][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_RIGHT][aSector];
      break;
    case S_LEFT_DOWN_FRONT: 
      (*graph)[S_RIGHT_UP_BACK][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_DOWN_FRONT][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_UP_BACK][newSector] = (*graph)[S_LEFT][aSector];
      break;
    case S_RIGHT_DOWN_FRONT: 
      (*graph)[S_RIGHT_DOWN_BACK][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_UP_FRONT][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_LEFT_FRONT][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_FRONT][aSector];
      (*graph)[S_BACK][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_RIGHT_BACK][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN_BACK][newSector] = (*graph)[S_LEFT][aSector];
      break;
    case S_LEFT_UP_BACK: 
      (*graph)[S_RIGHT_DOWN_FRONT][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_DOWN][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_LEFT_UP][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_LEFT][aSector];
      break;
    case S_RIGHT_UP_BACK: 
      (*graph)[S_LEFT_DOWN_FRONT][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_UP_BACK][aSector];
      (*graph)[S_DOWN][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      (*graph)[S_LEFT_DOWN][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_RIGHT_UP][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_UP][aSector];
      (*graph)[S_DOWN_FRONT][newSector] = (*graph)[S_RIGHT][aSector];
      break;
    case S_LEFT_DOWN_BACK: 
      (*graph)[S_RIGHT_UP_FRONT][newSector] = aSector;
      (*graph)[S_RIGHT][newSector] = (*graph)[S_DOWN_BACK][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_LEFT_BACK][aSector];
      (*graph)[S_RIGHT_UP][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_LEFT_DOWN][aSector];
      (*graph)[S_RIGHT_FRONT][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_LEFT][aSector];
      break;
    case S_RIGHT_DOWN_BACK: 
      (*graph)[S_LEFT_UP_FRONT][newSector] = aSector;
      (*graph)[S_LEFT][newSector] = (*graph)[S_DOWN_BACK][aSector];
      (*graph)[S_UP][newSector] = (*graph)[S_RIGHT_BACK][aSector];
      (*graph)[S_LEFT_UP][newSector] = (*graph)[S_BACK][aSector];
      (*graph)[S_FRONT][newSector] = (*graph)[S_RIGHT_DOWN][aSector];
      (*graph)[S_LEFT_FRONT][newSector] = (*graph)[S_DOWN][aSector];
      (*graph)[S_UP_FRONT][newSector] = (*graph)[S_RIGHT][aSector];
      break;      
  }

}
