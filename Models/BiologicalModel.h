/*
  BiologicalModel.h

  Protocol for biological model classes

  Copyright (C) 2009-2011 Scott Christley

  Author: Scott Christley <schristley@mac.com>
  Date: 2009
   
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "SpatialModel.h"

#define BCMODEL_NOFILE_STATE 0
#define BCMODEL_WRITE_STATE 1
#define BCMODEL_READ_STATE 2
#define BCMODEL_APPEND_AND_CLOSE_STATE 3

@protocol BiologicalModel

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj;
- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent;

- (NSDictionary *)model;
- (NSString *)modelName;
- (id)modelController;
- (id)parentModel;
- (NSMutableArray *)childModels;
- (void)addChildModel: aModel;

- (SpatialModel *)spatialModel;
- (SpatialModel *)spatialModelForContext: aModel;

- (void)setMultiplicity: (int)aNum;
- (int)multiplicity;

- (int)numOfSpecies;
- (NSString *)nameOfSpecies: (int)theSpecies;
- (id)valueOfSpecies: (int)theSpecies withMultiplicity: (int)aNum;

- (id)delegate;
- (void)setDelegate: anObj;

- (BOOL)isDebug;
- (void)setDebug: (BOOL)aFlag;

- (void)setRunNumber: (int)aNum;
- (int)runNumber;

- (void)setupDataFilesWithState: (int)aState;
- (void)allocateSimulationWithEncode: (NSString *)anEncode;
//- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState;
//- (BOOL)writeData;
- (BOOL)writeDataWithCheck: (BOOL)aFlag;
- (BOOL)readData;
- (void)rewindFiles;
- (void)initializeSimulation;
//- (void)runSimulation;
- (void)cleanupSimulation;

- (int)numOfParameters;
- (NSString *)nameOfParameter: (int)theParam;

- (BOOL)setupParameterRanges: (NSDictionary *)ranges doCheck: (BOOL)aFlag;
- (double)randomValueforParameter: (int)aParam;
- (double)perturbValueForParameter: (int)aParam withinRange: (BOOL)aFlag standardDeviation: (double)SD;
- (int)modifyRandomParameter: (NSMutableDictionary *)params;
- (double *)parameterMinRange;
- (double *)parameterMaxRange;

// perturbation operations
- (BOOL)setValueForAllSpecies: (double)aValue;
- (BOOL)setValue: (double)aValue forSpecies: (NSString *)speciesName;
- (BOOL)adjustValue: (double)aValue forSpecies: (NSString *)speciesName;
- (BOOL)knockoutSpecies: (NSString *)speciesName;

@end

// GPU code generation
@protocol BiologicalModelGPU
- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName;
@end

// run GPU
@protocol BiologicalModelGPURun
- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions;
- (void)freeGPUData: (void *)data;

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;

- (void)allocGPUKernel: (void *)data;
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUKernel: (void *)data;
@end

@protocol BiologicalModelGPUDelegate
- (void *)allocGPUDataForModel: aModel;
- (int)assignGPUParameters: (float *)paramValues forModel: aModel;
- (void)initializeSimulationForModel: aModel;
- (void)didInitializeSimulation: (id)anObject;
- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)anObject;
- (void)willRunSimulation: (id)anObject;
- (void)cleanupSimulation;
- (void)didFinishSimulation: (id)anObject;
@end

@protocol BiologicalModelPovray
- (NSMutableString *)povrayCode;
@end

@protocol BioSwarmDelegate
- (void)speciesInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes modelNumbers: (NSArray *)modelNumbers
          modelKeys: (NSArray *)modelKeys;
@end

@protocol EvolutionaryStrategyDelegate
- (double)calculateFitness: theController forIndividual: (int)modelIndex;
- (void)bestFitness: (double)aFitness forGeneration: (int)aGen controller: theController individual: (int)modelIndex;
@end

@protocol MCMCDelegate
- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes modelNumbers: (NSArray *)modelNumbers modelKeys: (NSArray *)modelKeys
            minRanges: (double *)minRange maxRange: (double *)maxRange acceptValues: (void *)acceptValues
         rejectValues: (void *)rejectValues;
- (void)willPerformMCMC: theController setNumber: (int)setNum;
- (void)didPerformMCMC: theController setNumber: (int)setNum;
- (void)willCalculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (double)calculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (void)initExpectation: theController;
- (void)calculateExpectation: theController forIndividual: (int)modelIndex accept: (BOOL)aFlag;
- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags;
- (void)finishExpectation: theController;
@end

