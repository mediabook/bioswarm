/*
 ReactionDiffusionModel.m
 
 Chemical reaction-diffusion model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "ReactionDiffusionModel.h"
#include <math.h>
#import "MultiScale2DGrid.h"

#import "internal.h"

int countClusters(int n, void *dGrid);

#define OUTPUT 0

#define BLOWUP_THRESHOLD 1000000

@implementation ReactionDiffusionModel

- (void)collectParameters
{
  int i;
  
  // get all the parameters from the reaction rules
  [super collectParameters];
  
  // add diffusion parameter for each species
  for (i = 0; i < numOfSpecies; ++i) {
    NSString *s = [species objectAtIndex: i];
    [parameterNames addObject: [NSString stringWithFormat: @"%@_diffusion", s]];
  }

  numOfParameters = [parameterNames count];
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  [super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent];

  id spatialModel = [modelController spatialModel];
  grid = [[spatialModel grid] retain];

  // TODO: don't store locally
  double *ds = [spatialModel domainSize];
  domainSize = ds[0];
  int *gs = [spatialModel gridSize];
  gridSize = gs[0];
  dx = domainSize / (double)gridSize;
  if (OUTPUT) printf("dx = %lf\n", dx);  
  
  return self;
}


- (void)dealloc
{
  if (speciesGrid) free(speciesGrid);
  if (speciesF1) free(speciesF1);
  if (speciesF2) free(speciesF2);
  if (speciesGradient) free(speciesGradient);
  if (speciesDiff) free(speciesDiff);
  
  [grid release];
  
  [super dealloc];
}

- (void)setupData
{
  int k;
  
  // create spatial grids
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [grid newGridWithName: s atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ F1", s]
                  atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ F2", s]
                  atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ gradient", s]
                  atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ diff", s]
                  atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ state", s]
                  atScale: 1 withEncode: dataEncode];
  }
  
  // cache access to the spatial matrices
  speciesGrid = malloc(sizeof(void *) * numOfSpecies);
  speciesF1 = malloc(sizeof(void *) * numOfSpecies);
  speciesF2 = malloc(sizeof(void *) * numOfSpecies);
  speciesGradient = malloc(sizeof(void *) * numOfSpecies);
  speciesDiff = malloc(sizeof(void *) * numOfSpecies);
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    speciesGrid[k] = [[[grid gridWithName: s] objectForKey: @"matrix"] getMatrix];
    speciesF1[k] = [[[grid gridWithName: [NSString stringWithFormat: @"%@ F1", s]]
                     objectForKey: @"matrix"] getMatrix];
    speciesF2[k] = [[[grid gridWithName: [NSString stringWithFormat: @"%@ F2", s]]
                     objectForKey: @"matrix"] getMatrix];
    speciesGradient[k] = [[[grid gridWithName: [NSString stringWithFormat: @"%@ gradient", s]]
                           objectForKey: @"matrix"] getMatrix];
    speciesDiff[k] = [[[grid gridWithName: [NSString stringWithFormat: @"%@ diff", s]]
                       objectForKey: @"matrix"] getMatrix];
  }
}

- (void)setupDataFilesWithState: (int)aState
{
  int k;
  
  dataFileState = aState;
  switch (dataFileState) {
    case BCMODEL_NOFILE_STATE:
      // no data files setup
      break;
    case BCMODEL_WRITE_STATE:
    {
      // data files for writing
      dataFiles = malloc(sizeof(FILE *) * numOfSpecies);
      for (k = 0; k < numOfSpecies; ++k) {
        id s = [species objectAtIndex: k];
        NSString *fileName = [NSString stringWithFormat: @"%@_%@_%@.dat", [self modelName], s, outputSuffix];
        dataFiles[k] = fopen([fileName UTF8String], "w");
        if (dataFiles[k] == NULL) {
          NSLog(@"Cannot open file: %@", fileName);
          exit(0);
        }
      }
      break;
    }
    case BCMODEL_READ_STATE:
      // data file for reading
      break;
    case BCMODEL_APPEND_AND_CLOSE_STATE:
    {
      // data file for append and close to prevent too many files open
      // open then close to truncate file
      dataFiles = malloc(sizeof(FILE *) * numOfSpecies);
      for (k = 0; k < numOfSpecies; ++k) {
        id s = [species objectAtIndex: k];
        NSString *fileName = [NSString stringWithFormat: @"%@_%@_%@.dat", [self modelName], s, outputSuffix];
        dataFiles[k] = fopen([fileName UTF8String], "w");
        if (dataFiles[k] == NULL) {
          NSLog(@"Cannot open file: %@", fileName);
          exit(0);
        }
        fclose(dataFiles[k]);
      }
      break;
    }
  }
}

- (void)initializeSimulation
{
  int i, j, k;
  
  switch (encodeTag) {
      case 1:
        for (k = 0; k < numOfSpecies; ++k) {
          int (*sGrid)[gridSize][gridSize];
          sGrid = speciesGrid[k];
          int (*sDiff)[gridSize][gridSize];
          sDiff = speciesDiff[k];
          
          for (i = 0; i < gridSize; ++i)
            for (j = 0; j < gridSize; ++j) {
              (*sGrid)[i][j] = 1.0;
              (*sDiff)[i][j] = (*sGrid)[i][j];
            }
        }
        break;
      case 2:
        for (k = 0; k < numOfSpecies; ++k) {
          long (*sGrid)[gridSize][gridSize];
          sGrid = speciesGrid[k];
          long (*sDiff)[gridSize][gridSize];
          sDiff = speciesDiff[k];
          
          for (i = 0; i < gridSize; ++i)
            for (j = 0; j < gridSize; ++j) {
              (*sGrid)[i][j] = 1.0;
              (*sDiff)[i][j] = (*sGrid)[i][j];
            }
        }
        break;
      case 3:
        for (k = 0; k < numOfSpecies; ++k) {
          float (*sGrid)[gridSize][gridSize];
          sGrid = speciesGrid[k];
          float (*sDiff)[gridSize][gridSize];
          sDiff = speciesDiff[k];
          
          for (i = 0; i < gridSize; ++i)
            for (j = 0; j < gridSize; ++j) {
              //(*sGrid)[i][j] = fabs(0.01 * normal_dist_rand(0,1));
              //(*sGrid)[i][j] = 1.0;
              (*sGrid)[i][j] = 0.0;
              (*sDiff)[i][j] = (*sGrid)[i][j];
            }
          //(*sGrid)[30][30] = 10.0;
        }
        break;
      case 4:
        for (k = 0; k < numOfSpecies; ++k) {
          double (*sGrid)[gridSize][gridSize];
          sGrid = speciesGrid[k];
          double (*sDiff)[gridSize][gridSize];
          sDiff = speciesDiff[k];
          
          for (i = 0; i < gridSize; ++i)
            for (j = 0; j < gridSize; ++j) {
              (*sGrid)[i][j] = fabs(0.01 * normal_dist_rand(0,1));
              //(*sGrid)[i][j] = 1.0;
              //(*sGrid)[i][j] = 0.0;
              (*sDiff)[i][j] = (*sGrid)[i][j];
            }
        }
        break;
  }
}

- (BOOL)writeDataWithCheck: (BOOL)aFlag
{
  int k;

  // write only if data files setup for write
  if ((dataFileState != BCMODEL_WRITE_STATE) && (dataFileState != BCMODEL_APPEND_AND_CLOSE_STATE)) return NO;
  
  if (dataFileState == BCMODEL_APPEND_AND_CLOSE_STATE) {
    for (k = 0; k < numOfSpecies; ++k) {
      id s = [species objectAtIndex: k];
      NSString *fileName = [NSString stringWithFormat: @"%@_%@_%@.dat", [self modelName], s, outputSuffix];
      dataFiles[k] = fopen([fileName UTF8String], "a");
      if (dataFiles[k] == NULL) {
        NSLog(@"Cannot open file: %@", fileName);
        exit(0);
      }
    }
  }

  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [grid dumpGrid: s toFile: dataFiles[k]];
    MultiScale2DGrid *gg = grid;
#if 1
    int x1, y1, x2, y2;
    printf("%s min = %lf, max = %lf", [s UTF8String],
             [gg minValueForDoubleGrid: s atX: &x1 atY: &y1],
             [gg maxValueForDoubleGrid: s atX: &x2 atY: &y2]);
    printf(" (%d, %d), (%d, %d)\n", x1, y1, x2, y2);
#endif
  }
  
  if (dataFileState == BCMODEL_APPEND_AND_CLOSE_STATE) {
    for (k = 0; k < numOfSpecies; ++k) fclose(dataFiles[k]);
  }
  
  return YES;
}

#if 0
- (BOOL)checkEpsilon
{
  double maxDiff = 0.0;
  BOOL done = YES;
  int i, j, k;
  
  switch (encodeTag) {
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        for (k = 0; k < numOfSpecies; ++k) {
          double (*sGrid)[gridSize][gridSize];
          sGrid = speciesGrid[k];
          double (*sDiff)[gridSize][gridSize];
          sDiff = speciesDiff[k];
          
          withinEpsilon[k] = YES;
          for (i = 0; i < gridSize; ++i)
            for (j = 0; j < gridSize; ++j) {
              if (fabs((*sGrid)[i][j] - (*sDiff)[i][j]) >= epsilon) withinEpsilon[k] = NO;
              if (fabs((*sGrid)[i][j] - (*sDiff)[i][j]) > maxDiff)
                maxDiff = fabs((*sGrid)[i][j] - (*sDiff)[i][j]);
              
              (*sDiff)[i][j] = (*sGrid)[i][j];
            }
          
          done &= withinEpsilon[k];
        }
        printf("maxDiff = %lf\n", maxDiff);
        break;
  }
  
  return done;
}
#endif

- (void)runSimulation
{
}

#if 0
- (BOOL)patternCheckForSpecies: (int)aSpecies
{
  id s = [species objectAtIndex: aSpecies];
  double minVal = [grid minValueForDoubleGrid: s atX: NULL atY: NULL];
  double maxVal = [grid maxValueForDoubleGrid: s atX: NULL atY: NULL];
  if (!isfinite(minVal)) return NO;
  if (!isfinite(maxVal)) return NO;  
  double p = maxVal * .9;
  if (maxVal < 0.1) return NO;
  if (p < minVal) return NO;
  return YES;
}

- (void)ARFFHeader
{
  int k;
  
  printf("@relation reaction-diffusion\n\n");
  
  // attributes
  int numParams = [self numOfParameters];
  for (k = 0; k < numParams; ++k) {
    NSString *ps = [self nameOfParameter: k];
    printf("@attribute %s real\n", [ps UTF8String]);
  }
  
  printf("\n@data\n");
}
#endif

#define DEFAULT_FORMAT 0
#define ARFF_FORMAT 1
#define LIBSVM_FORMAT 2

#define SPOT_PATTERN 1
#define STRIPE_PATTERN 2

#if 0
- (NSMutableString *)analyzeSimulationRunsWithEncode: (NSString *)anEncode andFormat: (int)aFormat
{
  int k;
  
  NSMutableString *resultString = [NSMutableString string];

  if ([anEncode isEqual: [Grid2DArray intEncode]]) {
    encodeTag = 1;
  } else if ([anEncode isEqual: [Grid2DArray longEncode]]) {
    encodeTag = 2;
  } else if ([anEncode isEqual: [Grid2DArray floatEncode]]) {
    encodeTag = 3;
  } else if ([anEncode isEqual: [Grid2DArray doubleEncode]]) {
    encodeTag = 4;
  } else {
    // throw exception or something
    NSLog(@"ERROR: ReactionModel unsupported encoding %@\n", anEncode);
    return nil;
  }
  dataEncode = [anEncode retain];
  
  // setup data
  [self setupData];
  
  // input data files
  dataFiles = malloc(sizeof(FILE *) * numOfSpecies);
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    NSString *fileName = [NSString stringWithFormat: @"%@_%@_%@.dat", name, s, outputSuffix];
    dataFiles[k] = fopen([fileName UTF8String], "rb+");
    if (dataFiles[k] == NULL) {
      NSLog(@"Cannot open file: %@", fileName);
      exit(0);
    }
  }
  
  // read to last time
  while (!feof(dataFiles[0])) {
    for (k = 0; k < speciesCount; ++k) {
      id s = [species objectAtIndex: k];
      [grid loadGrid: s fromFile: dataFiles[k]];
    }
  }
  
  // check for pattern
  BOOL pat = [self patternCheckForSpecies: 0];
  int patternType = 0;
  if (pat) {
    // determine what kind of pattern
    id s = [species objectAtIndex: 0];
    int i, j;
    float (*sGrid)[gridSize][gridSize];
    sGrid = speciesGrid[0];

#if 0
    NSString *fileName = [NSString stringWithFormat: @"finalValue_%@_%@.dat", s, outputSuffix];
    FILE *fvFile = fopen([fileName UTF8String], "w");
    for (i = 0; i < gridSize; ++i)
      for (j = 0; j < gridSize; ++j)
        fprintf(fvFile, "%lf\n", (*sGrid)[i][j]);
    fclose(fvFile);
#endif
    
    // spots or stripes
    // histogram
    Grid2DArray *g = [[grid gridWithName: s] objectForKey: @"matrix"];
    double minValue = [g minDoubleValueAtX: NULL atY: NULL];
    double maxValue = [g maxDoubleValueAtX: NULL atY: NULL];
    int bins[100];
    double delta = (maxValue - minValue) / 99;
    for (i = 0; i < 100; ++i) bins[i] = 0;
    for (i = 0; i < gridSize; ++i)
      for (j = 0; j < gridSize; ++j) {
        int b = ((*sGrid)[i][j] - minValue) / delta;
        if (b >= 100) NSLog(@"Error: %d", b);
        ++bins[b];
      }
    
    // unimodal or bimodal
    int maxTopBin = 0;
    double maxTopBinValue = 0;
    int maxBottomBin = 0;
    double maxBottomBinValue = 0;
    int minBin = 0;
    double minBinValue = INT_MAX;
    for (i = 0; i < 50; ++i) {
      if (bins[i] > maxBottomBinValue) {
        maxBottomBinValue = bins[i];
        maxBottomBin = i;
      }
    }
    for (i = 50; i < 100; ++i) {
      if (bins[i] > maxTopBinValue) {
        maxTopBinValue = bins[i];
        maxTopBin = i;
      }
    }
    for (i = maxBottomBin; i < maxTopBin; ++i) {
      if (bins[i] < minBinValue) {
        minBinValue = bins[i];
        minBin = i;
      }
    }
    
    double ratio;
    if (maxTopBinValue > maxBottomBinValue) ratio = maxTopBinValue / maxBottomBinValue;
    else ratio = maxBottomBinValue / maxTopBinValue;
    
    //printf("\n%lf %d %lf %d %lf %d %lf\n", ratio, maxTopBin, maxTopBinValue, maxBottomBin, maxBottomBinValue, minBin, minBinValue);
    if ((maxTopBin > minBin) && (maxBottomBin < minBin)) {
      if (((maxTopBinValue / minBinValue) > ratio) && ((maxBottomBinValue / minBinValue) > ratio)) {
        patternType = STRIPE_PATTERN;
      } else {
        patternType = SPOT_PATTERN;
      }
    } else {
      patternType = SPOT_PATTERN;
    }
  }
  
  int spotNumber = 0;
  int spotStabilize = 0;
  double maxValue = 0;
  double maxDiff = 0;
  if (patternType == SPOT_PATTERN) {
    // count number of spots
    id s = [species objectAtIndex: 0];
    Grid2DArray *g = [[grid gridWithName: s] objectForKey: @"matrix"];
    double minValue = [g minDoubleValueAtX: NULL atY: NULL];
    maxValue = [g maxDoubleValueAtX: NULL atY: NULL];

    double threshold = (maxValue - minValue) / 2;
    
    //printf("\n%lf\n", threshold);
    spotNumber = [g countClusters: YES threshold: threshold countGrid: NULL];

    // determine sharpness
    maxDiff = [g maxDifference: YES];
    if ((maxValue - minValue) > 0) maxDiff = maxDiff / (maxValue - minValue);

    // determine when number of spots stabilizes
    for (k = 0; k < speciesCount; ++k) rewind(dataFiles[k]);

    int prevSpotNumber = 0;
    int cnt = 1;
    while (!feof(dataFiles[0])) {
      for (k = 0; k < speciesCount; ++k) {
        id s = [species objectAtIndex: k];
        [grid loadGrid: s fromFile: dataFiles[k]];
      }

      int aNum = [g countClusters: YES threshold: threshold countGrid: NULL];
      //printf("%d\n", aNum);

      if (aNum == spotNumber) {
        if (aNum != prevSpotNumber) spotStabilize = cnt;
      } else {
        spotStabilize = cnt;
      }
      prevSpotNumber = aNum;
      ++cnt;
    }

  }
  
  if ((aFormat == ARFF_FORMAT) || (aFormat == DEFAULT_FORMAT)) {
    // parameters
    [resultString appendFormat: @"%d,", outputRun];
    //printf("%d,", outputRun);
    int numParams = [self numOfParameters];
    for (k = 0; k < numParams; ++k) {
      NSString *ps = [self nameOfParameter: k];
      double pv = [[model objectForKey: ps] doubleValue];
      [resultString appendFormat: @"%lf,", pv];
      //printf("%lf,", pv);
    }
    
    if (pat) {
      [resultString appendFormat: @"pattern,"];
      if (patternType == SPOT_PATTERN)
        [resultString appendFormat: @"spots,%d,%d,%f,%f", spotNumber, spotStabilize, maxDiff, maxValue];
      if (patternType == STRIPE_PATTERN) [resultString appendFormat: @"stripes,0,0,0.0,0.0"];
      //printf("pattern,");
      //if (patternType == SPOT_PATTERN) printf("spots,%d,%d,%f", spotNumber, spotStabilize, maxDiff);
      //if (patternType == STRIPE_PATTERN) printf("stripes,0,0,0.0");
    } else {
      [resultString appendFormat: @"no-pattern,none,0,0,0.0,0.0"];
      //printf("no-pattern,none,0,0,0.0");
    }

    //printf("\n");
  }
  
  if (aFormat == LIBSVM_FORMAT) {
    BOOL pat = [self patternCheckForSpecies: 0];
    if (pat)
      printf("1 ");
    else
      printf("0 ");
    
    // parameters
    int numParams = [self numOfParameters];
    for (k = 0; k < numParams; ++k) {
      NSString *ps = [self nameOfParameter: k];
      double pv = [[model objectForKey: ps] doubleValue];
      printf("%d:%lf ", k, pv);
    }
    printf("\n");    
  }

  return resultString;
}
#endif

@end

//
// Produce GPU code
//
@implementation ReactionDiffusionModel (GPU)

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  int k, l;
  int numParams = [self numOfParameters];
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendFormat: @"__constant__ float %@_parameters[%d];\n\n", prefixName, numParams];
  
  // structure for grids
  [GPUCode appendFormat: @"typedef struct _%@_GPUgrids {\n", prefixName];
  [GPUCode appendString: @"  float dt;\n"];
  [GPUCode appendString: @"  float dx;\n"];
  [GPUCode appendString: @"  int width;\n"];
  [GPUCode appendString: @"  int height;\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"  float *%@;\n", s];
    [GPUCode appendFormat: @"  float *%@_in;\n", s];
    [GPUCode appendFormat: @"  float *%@_out;\n", s];
    [GPUCode appendFormat: @"  float *%@_F1;\n", s];
    [GPUCode appendFormat: @"  float *%@_F2;\n", s];
  }
  [GPUCode appendString: @"  int* interfaceCount;\n"];
  [GPUCode appendString: @"  size_t pitch;\n"];
  [GPUCode appendString: @"  size_t idx_pitch;\n"];
  [GPUCode appendFormat: @"} %@_GPUgrids;\n\n", prefixName];
  
  [GPUCode appendFormat: @"__constant__ %@_GPUgrids %@_struct[1];\n\n", prefixName, prefixName];
  
  [GPUCode appendString: @"\n// parameters\n"];
  
  for (k = 0; k < numParams; ++k) {
    NSString *paramName = [self nameOfParameter: k];
    [GPUCode appendFormat: @"#define %@ %@_parameters[%d]\n", paramName, prefixName, k];
  }
  
  return GPUCode;
}

- (NSDictionary *)controllerGPUCode: (NSString *)prefixName
{
  int k;
  int numParams = [self numOfParameters];
  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;

  //
  // Headers and defines
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];
  
  //
  // Allocation
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"*%@_allocGPUKernel(void *model, float dt, float dx, float *hostParameters, int width, int height)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"allocation function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *grids = (%@_GPUgrids *)malloc(sizeof(%@_GPUgrids));\n", prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n // Save parameters\n"];
  [GPUCode appendString: @" grids->dt = dt;\n"];
  [GPUCode appendString: @" grids->dx = dx;\n"];
  [GPUCode appendString: @" grids->width = width;\n"];
  [GPUCode appendString: @" grids->height = height;\n"];
  [GPUCode appendString: @"\n // Allocate device memory\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@), &(grids->pitch), width * sizeof(float), height);\n", s];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@_in), &(grids->pitch), width * sizeof(float), height);\n", s];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@_out), &(grids->pitch), width * sizeof(float), height);\n", s];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@_F1), &(grids->pitch), width * sizeof(float), height);\n", s];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@_F2), &(grids->pitch), width * sizeof(float), height);\n", s];
  }
  [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->interfaceCount), &(grids->pitch), width * sizeof(float), height);\n"];

  [GPUCode appendString: @"\n grids->idx_pitch = grids->pitch / sizeof(float);\n"];
  [GPUCode appendString: @"\n return grids;\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"allocation code"];

  //
  // Data Transfer
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *p, int aFlag, float *hostParameters, RK2grids *rk2grids)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *grids = (%@_GPUgrids *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @"\n if (aFlag) {\n"];
  [GPUCode appendString: @"    // Copy host memory to device memory\n"];
  [GPUCode appendFormat: @"    cudaMemcpyToSymbol(%@_parameters, hostParameters, %d * sizeof(float), 0, cudaMemcpyHostToDevice);\n", prefixName, numParams];
  [GPUCode appendFormat: @"    cudaMemcpyToSymbol(%@_struct, p, sizeof(%@_GPUgrids), 0, cudaMemcpyHostToDevice);\n", prefixName, prefixName];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"    cudaMemcpy2D(grids->%@, grids->pitch, (const cudaArray *)rk2grids->speciesGrid[%d],\n", s, k];
    [GPUCode appendString: @"       grids->width * sizeof(float), grids->width * sizeof(float), grids->height, cudaMemcpyHostToDevice);\n"];
  }
  [GPUCode appendString: @" } else {\n"];
  // Copy result to host memory
  [GPUCode appendString: @"    // Copy result to host memory\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"    cudaMemcpy2D(rk2grids->speciesGrid[%d], grids->width * sizeof(float), grids->%@, grids->pitch,", k, s];
    [GPUCode appendString: @"       grids->width * sizeof(float), grids->height, cudaMemcpyDeviceToHost);\n"];
  }
  
  [GPUCode appendString: @"}\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];
  
  //
  // Execution
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, RK2grids *rk2grids, double startTime, double nextTime)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" %@_GPUgrids *%@_ptrs = (%@_GPUgrids *)%@_data;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" dim3 %@_threadsPerBlock(16, 16);\n", prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerGrid((%@_ptrs->width + %@_threadsPerBlock.x - 1) / %@_threadsPerBlock.x,", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" (%@_ptrs->height + %@_threadsPerBlock.y - 1) / %@_threadsPerBlock.y);\n", prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @" double %@_currentTime = startTime;\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];
  
  GPUCode = [NSMutableString new];
  //[GPUCode appendFormat: @" while (%@_currentTime < nextTime) {\n", prefixName];

  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   cudaMemset2D (%@_ptrs->%@_in, %@_ptrs->pitch, 0, %@_ptrs->width * sizeof(float), %@_ptrs->height);\n", prefixName, s, prefixName, prefixName, prefixName];
  }

  [GPUCode appendFormat: @"   %@_kernel_F1<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"   %@_kernel_F2<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @"   %@_currentTime += %@_grids->dt;\n", prefixName, prefixName];
  //[GPUCode appendFormat: @" };\n"];
  
//  for (k = 0; k < numOfSpecies; ++k) {
//    id s = [species objectAtIndex: k];
//    [GPUCode appendFormat: @"   cudaMemcpy2D(%@_grids->%@, %@_grids->pitch, %@_grids->%@_F2,", prefixName, s, prefixName, prefixName, s];
//    [GPUCode appendFormat: @" %@_grids->pitch, %@_grids->width * sizeof(float), %@_grids->height, cudaMemcpyDeviceToDevice);\n", prefixName, prefixName, prefixName];
//  }
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];

  //
  // Release
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_releaseGPUKernel(void *model, void *g)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"release function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *grids = (%@_GPUgrids *)g;\n", prefixName, prefixName];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @" cudaFree(grids->%@);\n", s];
    [GPUCode appendFormat: @" cudaFree(grids->%@_F1);\n", s];
    [GPUCode appendFormat: @" cudaFree(grids->%@_F2);\n", s];
  }
  [GPUCode appendFormat: @" free(grids);\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"release code"];
  
  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"RK2functions %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];
  
  return GPUDictionary;  
}

- (NSMutableString *)diffusionKernelGPUCode: (NSString *)prefixName
{
  int k;
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  id spatialModel = [modelController spatialModel];
  int dimensions = [spatialModel dimensions];
  if (dimensions == 0) dimensions = 1;

  [GPUCode appendString: @"__device__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_diffusion(int i, int j, float mh, float mf)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"  // periodic boundary conditions\n"];
  [GPUCode appendString: @"  int iplus = i + 1;\n"];
  [GPUCode appendFormat: @"  if (iplus == %@_struct->width) iplus = 0;\n", prefixName];
  [GPUCode appendString: @"  int iminus = i - 1;\n"];
  [GPUCode appendFormat: @"  if (iminus < 0) iminus = %@_struct->width-1;\n", prefixName];
  [GPUCode appendString: @"  int jplus = j + 1;\n"];
  [GPUCode appendFormat: @"  if (jplus == %@_struct->height) jplus = 0;\n", prefixName];
  [GPUCode appendString: @"  int jminus = j - 1;\n"];
  [GPUCode appendFormat: @"  if (jminus < 0) jminus = %@_struct->height-1;\n", prefixName];
  [GPUCode appendString: @"  float up, down, left, right, gradient;\n"];
  [GPUCode appendString: @"  float F_val;\n"];
    
  for (k = 0; k < numOfSpecies; ++k) {
    //if (interactionsCount[k] == 0) continue;
    id s = [species objectAtIndex: k];

    [GPUCode appendFormat: @"\n   // %@\n", s];
    [GPUCode appendString: @"  // Laplace gradients\n"];                                                                                                                             
    [GPUCode appendFormat: @"  up = %@_struct->%@[jplus*%@_struct->idx_pitch+i] + mf * %@_struct->%@_in[jplus*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"  down = %@_struct->%@[jminus*%@_struct->idx_pitch+i] + mf * %@_struct->%@_in[jminus*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"  left = %@_struct->%@[j*%@_struct->idx_pitch+iminus] + mf * %@_struct->%@_in[j*%@_struct->idx_pitch+iminus];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"  right = %@_struct->%@[j*%@_struct->idx_pitch+iplus] + mf * %@_struct->%@_in[j*%@_struct->idx_pitch+iplus];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendString: @"  gradient = up + down + left + right;\n"];
    [GPUCode appendFormat: @"  F_val = %@_struct->%@[j*%@_struct->idx_pitch+i] + mf * %@_struct->%@_in[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"  %@_struct->%@_out[j*%@_struct->idx_pitch+i] += mh * (%@_diffusion * gradient - 4.0f * %@_diffusion * F_val)/(%@_struct->dx*%@_struct->dx);\n", prefixName, s, prefixName, s, s, prefixName, prefixName];
  }
  [GPUCode appendString: @"}\n"];

  return GPUCode;
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  int k;
  //int numParams = [self numOfParameters];
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  // kernel to calculate reaction function
  [GPUCode appendString: [self functionKernelGPUCode: prefixName]];

  // kernel to calculate diffusion
  [GPUCode appendString: [self diffusionKernelGPUCode: prefixName]];

  // 2nd order Runge-Kutta
  
  // F1 kernel
  
  [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F1(float currentTime, float finalTime)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int i = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int j = blockIdx.y * blockDim.y + threadIdx.y;\n"];
  
  [GPUCode appendString: @"\n   // check thread in boundary\n"];
  [GPUCode appendFormat: @"   if (i >= %@_struct->width) return;\n", prefixName];
  [GPUCode appendFormat: @"   if (j >= %@_struct->height) return;\n", prefixName];
  
  [GPUCode appendString: @"\n   // calculate F1 for reactions\n"];
  [GPUCode appendFormat: @"   %@_kernel_function(i, j, %@_struct->dt, 0.0f);\n", prefixName, prefixName, prefixName];

  [GPUCode appendString: @"\n   // calculate F1 for diffusion\n"];
  [GPUCode appendFormat: @"   %@_kernel_diffusion(i, j, %@_struct->dt, 0.0f);\n", prefixName, prefixName, prefixName];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   %@_struct->%@_F1[j*%@_struct->idx_pitch+i] = %@_struct->%@_out[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"   %@_struct->%@_in[j*%@_struct->idx_pitch+i] = %@_struct->%@_F1[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
  }
  [GPUCode appendString: @"}\n"];
  
  // F2 kernel
  
  [GPUCode appendString: @"\n// F2 kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F2(float currentTime, float finalTime)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int i = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int j = blockIdx.y * blockDim.y + threadIdx.y;\n"];
  
  [GPUCode appendString: @"\n   // check thread in boundary\n"];
  [GPUCode appendFormat: @"   if (i >= %@_struct->width) return;\n", prefixName];
  [GPUCode appendFormat: @"   if (j >= %@_struct->height) return;\n", prefixName];
  
  [GPUCode appendString: @"\n   // calculate F2 for reactions\n"];
  [GPUCode appendFormat: @"   %@_kernel_function(i, j, %@_struct->dt, 0.5f);\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendString: @"\n   // calculate F2 for diffusion\n"];
  [GPUCode appendFormat: @"   %@_kernel_diffusion(i, j, %@_struct->dt, 0.5f);\n", prefixName, prefixName, prefixName];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   %@_struct->%@_F2[j*%@_struct->idx_pitch+i] = %@_struct->%@_out[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
  }
  
  [GPUCode appendString: @"\n   // calculate x(t + dt)\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   %@_struct->%@[j*%@_struct->idx_pitch+i] += %@_struct->%@_F2[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"   if (%@_struct->%@[j*%@_struct->idx_pitch+i] < 0.0f) %@_struct->%@[j*%@_struct->idx_pitch+i] = 0.0f;\n", prefixName, s, prefixName, prefixName, s, prefixName];
  }
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;
}

- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName
{
  return nil;
}

@end

//
// Run GPU code
//

@implementation ReactionDiffusionModel (GPURun)

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions
{
  int i, k;
  ModelGPUData *gpuData = (ModelGPUData *)malloc(sizeof(ModelGPUData));
  
  gpuData->data = (RK2grids *)malloc(sizeof(RK2grids) * numModels);
  for (i = 0; i < numModels; ++i) {
    RK2grids *g = (RK2grids *)gpuData->data;
    g[i].speciesCount = numOfSpecies;
    g[i].speciesGrid = malloc(sizeof(void *) * numOfSpecies);
    for (k = 0; k < numOfSpecies; ++k) g[i].speciesGrid[k] = NULL;
  }

  gpuData->numModels = numModels;
  gpuData->numParams = [self numOfParameters];
  gpuData->gpuFunctions = gpuFunctions;
  
  gpuData->parameters = (float *)malloc(sizeof(float) * numModels * gpuData->numParams);
  
  return gpuData;
}

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int j;
      
  RK2grids *g = (RK2grids *)gpuData->data;
  if (aFlag)
    for (j = 0; j < numOfSpecies; ++j) g[aNum].speciesGrid[j] = speciesGrid[j];

  /* data automatically transferred from GPU to individual */
}

- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  return [super assignParameters: data ofNumber: aNum toGPU: aFlag];
}

- (void)allocGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RK2functions *gpuFunctions = (RK2functions *)gpuData->gpuFunctions;
  gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, (float)dt, (float)dx, gpuData->parameters, gridSize, gridSize);
}

- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RK2functions *gpuFunctions = (RK2functions *)gpuData->gpuFunctions;
  RK2grids *g = (RK2grids *)gpuData->data; // g is a really an array of RK2grids
  (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, aFlag, gpuData->parameters, g);
}

- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RK2functions *gpuFunctions = (RK2functions *)gpuData->gpuFunctions;
  (gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, gpuData->data, currentTime, endTime);
}

- (void)releaseGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RK2functions *gpuFunctions = (RK2functions *)gpuData->gpuFunctions;
  (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
}

@end
