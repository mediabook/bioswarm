/*
 ReactionRuleModel.m
 
 General rule-based reaction model.
 
 Copyright (C) 2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: July 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "ReactionRuleModel.h"
#import "MultiScale2DGrid.h"
#import "BioSwarmController.h"

#import "internal.h"

@implementation ReactionRuleModel

- (void)collectParameters
{
  int i, j, k;
  
  // total up the parameters
  parameterNames = [NSMutableArray new];
  for (i = 0; i < [speciesFunctions count]; ++i) {
    NSArray *funcs = [speciesFunctions objectAtIndex: i];
    for (j = 0; j < [funcs count]; ++j) {
      NSDictionary *d = [funcs objectAtIndex: j];
      NSArray *a = [d objectForKey: @"parameters"];
      for (k = 0; k < [a count]; ++k) {
        NSString *paramName = [a objectAtIndex: k];
        if ([parameterNames indexOfObject: paramName] == NSNotFound) {
          [parameterNames addObject: paramName];
        }
      }
    }
  }
  numOfParameters = [parameterNames count];
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  int i, j, k;
  NSString *param;
  
  [super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent];
  
  // setup species functions
  //species = [aModel objectForKey: @"species"];
  //numOfSpecies = [species count];
  //printf("%d species\n", numOfSpecies);
  speciesFunctions = [NSMutableArray new];
  for (i = 0; i < numOfSpecies; ++i) [speciesFunctions addObject: [NSMutableArray array]];
  
  sharedParameters = [aModel objectForKey: @"sharedParameters"];
  reactions = [aModel objectForKey: @"reactions"];
  numOfReactions = [reactions count];
  
  // go through each reaction and setup species functions
  for (i = 0; i < numOfReactions; ++i) {
    NSDictionary *r = [reactions objectAtIndex: i];
    NSString *reactionName = [r objectForKey: @"name"];
    NSArray *pList = [r objectForKey: @"products"];
    NSArray *rList = [r objectForKey: @"reactants"];
    NSArray *eList = [r objectForKey: @"enzymes"];
    NSString *rType = [r objectForKey: @"type"];
    NSString *rateFunction = [r objectForKey: @"rate"];
    
    // for each product
    for (j = 0; j < [pList count]; ++j) {
      NSString *pName = [pList objectAtIndex: j];
      NSUInteger idx = [species indexOfObject: pName];
      if (idx == NSNotFound) {
        printf("ERROR: Species (%s) referenced in reaction, not in species list", [pName UTF8String]);
        return nil;
      }
      
      // build up function term for product species
      NSMutableArray *sf = [speciesFunctions objectAtIndex: idx];
      NSMutableDictionary *d = [NSMutableDictionary dictionary];
      [sf addObject: d];
      
      // construct parameter list
      if ([rType isEqualToString: @"signal"]) {
        NSMutableArray *params = [NSMutableArray array];
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          for (k = 0; k < [rList count]; ++k) {
            param = [NSString stringWithFormat: @"%@_%@_pmax_%@",
                     reactionName, pName, [rList objectAtIndex: k]];
            if (![sharedParameters objectForKey: param]) [params addObject: param];
            else [params addObject: [sharedParameters objectForKey: param]];
            
            param = [NSString stringWithFormat: @"%@_%@_c_%@",
                     reactionName, pName, [rList objectAtIndex: k]];
            if (![sharedParameters objectForKey: param]) [params addObject: param];
            else [params addObject: [sharedParameters objectForKey: param]];
            
            param = [NSString stringWithFormat: @"%@_%@_h_%@",
                     reactionName, pName, [rList objectAtIndex: k]];
            if (![sharedParameters objectForKey: param]) [params addObject: param];
            else [params addObject: [sharedParameters objectForKey: param]];
          }
          
        } else if ([rateFunction isEqualToString: @"Constant"]) {
          param = [NSString stringWithFormat: @"%@_%@_c", reactionName, pName];
          if (![sharedParameters objectForKey: param]) [params addObject: param];
          else [params addObject: [sharedParameters objectForKey: param]];
          
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_%@_k", reactionName, pName];
          if (![sharedParameters objectForKey: param]) [params addObject: param];
          else [params addObject: [sharedParameters objectForKey: param]];
          
          if (eList) {
            for (k = 0; k < [eList count]; ++k) {
              param = [NSString stringWithFormat: @"%@_%@_pmax_%@",
                       reactionName, pName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
              
              param = [NSString stringWithFormat: @"%@_%@_c_%@",
                       reactionName, pName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
              
              param = [NSString stringWithFormat: @"%@_%@_h_%@",
                       reactionName, pName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
            }
          }
        } else {
          printf("ERROR: Unknown rate function: %s\n", [rateFunction UTF8String]);
          return nil;
        }
        
        [d setObject: params forKey: @"parameters"];
        
      } else if ([rType isEqualToString: @"mass"]) {
        NSMutableArray *params = [NSMutableArray array];
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          // TODO: need enzyme
          printf("TODO: need enzyme\n");
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_k", reactionName];
          if (![sharedParameters objectForKey: param]) [params addObject: param];
          else [params addObject: [sharedParameters objectForKey: param]];
          
          if (eList) {
            for (k = 0; k < [eList count]; ++k) {
              param = [NSString stringWithFormat: @"%@_pmax_%@",
                       reactionName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
              
              param = [NSString stringWithFormat: @"%@_c_%@",
                       reactionName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
              
              param = [NSString stringWithFormat: @"%@_h_%@",
                       reactionName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
            }
          }
        } else {
          printf("ERROR: Unknown rate function: %s\n", [rateFunction UTF8String]);
          return nil;
        }
        
        [d setObject: @"positive" forKey: @"sign"];
        [d setObject: params forKey: @"parameters"];
        
      } else {
        printf("ERROR: undefined type for reaction\n");
        return nil;
      }
      
      [d setObject: reactionName forKey: @"reaction"];
      [d setObject: rList forKey: @"reactants"];
      if (eList) [d setObject: eList forKey: @"enzymes"];
      [d setObject: rType forKey: @"type"];
      [d setObject: rateFunction forKey: @"rate"];
      
      if ([self isDebug]) printf("Function term: %s\n %s\n", [pName UTF8String], [[d description] UTF8String]);
    }
    
    // if the reaction type is mass action
    // then need to add negation functions for each reactant
    if ([rType isEqualToString: @"mass"]) {
      for (j = 0; j < [rList count]; ++j) {
        NSString *pName = [rList objectAtIndex: j];
        NSUInteger idx = [species indexOfObject: pName];
        if (idx == NSNotFound) {
          printf("ERROR: Species (%s) referenced in reaction, not in species list", [pName UTF8String]);
          return nil;
        }
        
        // if a reactant appears multiple times, we only want to one negation function
        if ([rList indexOfObject: pName] != j) continue;
        
        // build up function term for reactant species
        NSMutableArray *sf = [speciesFunctions objectAtIndex: idx];
        NSMutableDictionary *d = [NSMutableDictionary dictionary];
        [sf addObject: d];
        [d setObject: reactionName forKey: @"reaction"];
        [d setObject: rList forKey: @"reactants"];
        if (eList) [d setObject: eList forKey: @"enzymes"];
        [d setObject: rType forKey: @"type"];
        [d setObject: rateFunction forKey: @"rate"];
        
        NSMutableArray *params = [NSMutableArray array];
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          // TODO: need enzyme
          printf("TODO: need enzyme\n");
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_k", reactionName];
          if (![sharedParameters objectForKey: param]) [params addObject: param];
          else [params addObject: [sharedParameters objectForKey: param]];
          
          if (eList) {
            for (k = 0; k < [eList count]; ++k) {
              param = [NSString stringWithFormat: @"%@_pmax_%@",
                       reactionName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
              
              param = [NSString stringWithFormat: @"%@_c_%@",
                       reactionName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
              
              param = [NSString stringWithFormat: @"%@_h_%@",
                       reactionName, [eList objectAtIndex: k]];
              if (![sharedParameters objectForKey: param]) [params addObject: param];
              else [params addObject: [sharedParameters objectForKey: param]];
            }
          }
        } else {
          printf("ERROR: Unknown rate function: %s\n", [rateFunction UTF8String]);
          return nil;
        }
        
        [d setObject: @"negative" forKey: @"sign"];
        [d setObject: params forKey: @"parameters"];
        
        if ([self isDebug]) printf("Function term: %s\n %s\n", [pName UTF8String], [[d description] UTF8String]);
      }
    }
  }
  
  // collect parameters
  [self collectParameters];
  
  if (!numOfParameters)
    printf("WARNING: No parameters in model.\n");
  else {
    parameterValues = (double *)malloc(sizeof(double) * numOfParameters);
    for (i = 0; i < numOfParameters; ++i) {
      NSString *paramName = [self nameOfParameter: i];
      param = [aModel objectForKey: paramName];
      CHECK_PARAM(param, [paramName UTF8String])
      parameterValues[i] = [param doubleValue];
    }
  }  
  
  // initial conditions
  param = [aModel objectForKey: @"initialValue"];
  if (!param) initialValue = [NSNumber numberWithInt: 0];
  else initialValue = param;
  
  initialValues = [aModel objectForKey: @"initialValues"];
  if (initialValues) {
    NSArray *a = [initialValues allKeys];
    for (i = 0; i < [a count]; ++i) {
      NSString *s = [a objectAtIndex: i];
      NSUInteger idx = [species indexOfObject: s];
      if (idx == NSNotFound)
        printf("Species (%s) referenced in initialValues, not in species list.\n", [s UTF8String]);
    }
  }

  return self;
}

- (void)dealloc
{
  [speciesFunctions release];
  [parameterNames release];
  if (parameterValues) free(parameterValues);
  
  if ((dataFile) && (dataFileState != BCMODEL_APPEND_AND_CLOSE_STATE)) fclose(dataFile);
  if (dataResults) free(dataResults);
  if (dataFlags) free(dataFlags);

  [super dealloc];
}

//
// Simulation actions
//
- (void)setupData
{
  if (dataResults) {
    free(dataResults);
    dataResults = NULL;
  }
  if (dataFlags) {
    free(dataFlags);
    dataFlags = NULL;
  }

  switch (encodeTag) {
    case 1:
      dataResults = malloc(sizeof(int) * numOfSpecies * multiplicity);
      break;
    case 2:
      dataResults = malloc(sizeof(long) * numOfSpecies * multiplicity);
      break;
    case 3:
      dataResults = malloc(sizeof(float) * numOfSpecies * multiplicity);
      break;
    case 4:
      dataResults = malloc(sizeof(double) * numOfSpecies * multiplicity);
      break;
  }
  
  int i;
  dataFlags = malloc(sizeof(int) * numOfSpecies * multiplicity);
  for (i = 0; i < (numOfSpecies * multiplicity); ++i) dataFlags[i] = 0;
}

- (void)setupDataFilesWithState: (int)aState
{
  dataFileState = aState;
  switch (dataFileState) {
    case BCMODEL_NOFILE_STATE:
      // no data files setup
      break;
    case BCMODEL_WRITE_STATE:
    {
      // data file for writing
      NSString *fileName = [NSString stringWithFormat: @"%@_%@.dat", [self modelName], outputSuffix];
      //printf("file = %s\n", [fileName UTF8String]);
      dataFile = fopen([fileName UTF8String], "w");
      if (dataFile == NULL) {
        NSLog(@"Cannot open file: %@", fileName);
        exit(0);
      }
      if (!isBinaryFile) {
        int i, k;
        fprintf(dataFile, "Time");
        for (i = 0; i < multiplicity; ++i)
          for (k = 0; k < numOfSpecies; ++k) {
            fprintf(dataFile, " %s", [[species objectAtIndex: k] UTF8String]);
          }
        fprintf(dataFile, "\n");
      }
      break;
    }
    case BCMODEL_READ_STATE:
      // data file for reading
      break;
    case BCMODEL_APPEND_AND_CLOSE_STATE:
    {
      // data file for append and close to prevent too many files open
      // open then close to truncate file
      NSString *fileName = [NSString stringWithFormat: @"%@_%@.dat", [self modelName], outputSuffix];
      dataFile = fopen([fileName UTF8String], "w");
      if (dataFile == NULL) {
        NSLog(@"Cannot open file: %@", fileName);
        exit(0);
      }
      fclose(dataFile);
      break;
    }
  }
}

- (void)allocateSimulationWithEncode: (NSString *)anEncode
{
  // setup data
  [self setupData];
}

- (void)initializeSimulation
{
  int i, k;
  switch (encodeTag) {
    case 1: {
      int *sData = dataResults;
      for (i = 0; i < multiplicity; ++i)
        for (k = 0; k < numOfSpecies; ++k) {
          sData[i*numOfSpecies+k] = [initialValue intValue];
          if (initialValues) {
            NSString *s = [species objectAtIndex: k];
            NSString *param = [initialValues objectForKey: s];
            if (param) sData[i*numOfSpecies+k] = [param intValue];
          }
        }
      break;
    }
    case 2: {
      long *sData = dataResults;
      for (i = 0; i < multiplicity; ++i)
        for (k = 0; k < numOfSpecies; ++k) {
          sData[i*numOfSpecies+k] = [initialValue intValue];
          if (initialValues) {
            NSString *s = [species objectAtIndex: k];
            NSString *param = [initialValues objectForKey: s];
            if (param) sData[i*numOfSpecies+k] = [param intValue];
          }
        }
      break;
    }
    case 3: {
      float *sData = dataResults;
      for (i = 0; i < multiplicity; ++i)
        for (k = 0; k < numOfSpecies; ++k) {
          sData[i*numOfSpecies+k] = [initialValue floatValue];
          if (initialValues) {
            NSString *s = [species objectAtIndex: k];
            NSString *param = [initialValues objectForKey: s];
            if (param) sData[i*numOfSpecies+k] = [param floatValue];
	    //printf("initial (%d) = %lf\n", k, sData[k]);
          }
        }
      break;
    }
    case 4: {
      double *sData = dataResults;
      for (i = 0; i < multiplicity; ++i)
        for (k = 0; k < numOfSpecies; ++k) {
          sData[i*numOfSpecies+k] = [initialValue doubleValue];
          if (initialValues) {
            NSString *s = [species objectAtIndex: k];
            NSString *param = [initialValues objectForKey: s];
            if (param) sData[i*numOfSpecies+k] = [param doubleValue];
          }
          //printf("initial (%d) = %lf\n", k, sData[k]);
        }
      break;
    }
  }
}

- (void)stepSimulation
{
  printf("ReactionRule step: %lf\n", [modelController currentTime]);
  ModelGPUData **gpuData = [modelController gpuData];
  if (gpuData) {
    ModelGPUData *data = gpuData[modelNumber];
    RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)data->gpuFunctions;
    printf("stepSimulation: %d %p %p\n", modelNumber, data, gpuFunctions);
    (gpuFunctions->invokeGPUKernel)(self, data->gpuPtrs, data->data, 0.0, dt);
  } else {
  }
}

- (BOOL)writeDataWithCheck: (BOOL)aFlag
{
  int k;
  
  // write only if data files setup for write
  if ((dataFileState != BCMODEL_WRITE_STATE) && (dataFileState != BCMODEL_APPEND_AND_CLOSE_STATE)) return NO;
  
  if (dataFileState == BCMODEL_APPEND_AND_CLOSE_STATE) {
    NSString *fileName = [NSString stringWithFormat: @"%@_%@.dat", [self modelName], outputSuffix];
    dataFile = fopen([fileName UTF8String], "a");
    if (dataFile == NULL) {
      NSLog(@"Cannot open file: %@", fileName);
      exit(0);
    }
  }
  
  double ct = [modelController currentTime];
  if (isBinaryFile) {
    // write data in binary file
    fwrite(&ct, sizeof(double), 1, dataFile);
    switch (encodeTag) {
      case 1: {
        int *data = dataResults;
        for (k = 0; k < (numOfSpecies * multiplicity); ++k) {
          fwrite(&(data[k]), sizeof(int), 1, dataFile);
        }
        break;
      }
      case 2: {
        long *data = dataResults;
        for (k = 0; k < (numOfSpecies * multiplicity); ++k) {
          fwrite(&(data[k]), sizeof(long), 1, dataFile);
        }
        break;
      }
      case 3: {
        float *data = dataResults;
        for (k = 0; k < (numOfSpecies * multiplicity); ++k) {
          fwrite(&(data[k]), sizeof(float), 1, dataFile);
        }
        break;
      }
      case 4: {
        double *data = dataResults;
        for (k = 0; k < (numOfSpecies * multiplicity); ++k) {
          fwrite(&(data[k]), sizeof(double), 1, dataFile);
        }
        break;
      }
    }
  } else {
    // write data in text file
    fprintf(dataFile, "%f", ct);
    switch (encodeTag) {
      case 1: {
        int *data = dataResults;
        for (k = 0; k < (numOfSpecies * multiplicity); ++k) {
          fprintf(dataFile, " %d", data[k]);
        }
        fprintf(dataFile, "\n");
        break;
      }
      case 2: {
        long *data = dataResults;
        for (k = 0; k < (numOfSpecies * multiplicity); ++k) {
          fprintf(dataFile, " %ld", data[k]);
        }
        fprintf(dataFile, "\n");
        break;
      }
      case 3: {
        float *data = dataResults;
        for (k = 0; k < (numOfSpecies * multiplicity); ++k) {
          fprintf(dataFile, " %f", data[k]);
        }
        fprintf(dataFile, "\n");
        break;
      }
      case 4: {
        double *data = dataResults;
        for (k = 0; k < (numOfSpecies * multiplicity); ++k) {
          fprintf(dataFile, " %f", data[k]);
        }
        fprintf(dataFile, "\n");
        break;
      }
    }
  }
  
  if (dataFileState == BCMODEL_APPEND_AND_CLOSE_STATE) {
    fclose(dataFile);
  }
  
  return YES;
}

- (BOOL)readData
{
  return YES;
}

- (void)rewindFiles
{
}

- (void)cleanupSimulation
{
}

//
// Accessors
//

- (NSArray *)reactions { return reactions; }
- (int)numOfSpecies { return numOfSpecies; }
- (NSString *)nameOfSpecies: (int)theSpecies { return [species objectAtIndex: theSpecies]; }
- (id)valueOfSpecies: (int)theSpecies withMultiplicity: (int)aNum
{
  switch (encodeTag) {
    case 1: {
      int *sData = dataResults;
      return [NSNumber numberWithInt: sData[(aNum-1)*numOfSpecies+theSpecies]];
      break;
    }
    case 2: {
      long *sData = dataResults;
      return [NSNumber numberWithLong: sData[(aNum-1)*numOfSpecies+theSpecies]];
      break;
    }
    case 3: {
      float *sData = dataResults;
      return [NSNumber numberWithFloat: sData[(aNum-1)*numOfSpecies+theSpecies]];
      break;
    }
    case 4: {
      double *sData = dataResults;
      return [NSNumber numberWithDouble: sData[(aNum-1)*numOfSpecies+theSpecies]];
      break;
    }
  }
  return nil;
}

//- (int)timeSteps { return timeSteps; }
//- (double)epsilon { return epsilon; }
- (NSString *)dataEncode { return dataEncode; }
- (void *)dataResults { return dataResults; }
- (FILE *)dataFile { return dataFile; }
- (int)numOfParameters { return numOfParameters; }

- (NSString *)nameOfParameter: (int)theParam
{
  return [parameterNames objectAtIndex: theParam];
}

#if 0
- (int)indexOfParameter: (NSString *)aName
{
  int i, p = 0;
  
  for (i = 0; i < [speciesFunctions count]; ++i) {
    NSMutableDictionary *d = [speciesFunctions objectAtIndex: i];
    NSArray *a = [d objectForKey: @"parameters"];
    NSUInteger idx = [a indexOfObject: aName];
    if (idx == NSNotFound) p += [a count];
    else return p + idx;
  }
  
  return NSNotFound;
}
#endif

#if 0
#pragma mark == PERTURBATIONS ==
#endif

//
// perturbation operations
//

- (BOOL)setValueForAllSpecies: (double)aValue
{
  int i, k;
  switch (encodeTag) {
    case 1: {
      int *sData = dataResults;
      for (i = 0; i < multiplicity; ++i)
        for (k = 0; k < numOfSpecies; ++k) {
          sData[i*numOfSpecies+k] = (int)aValue;
        }
      break;
    }
    case 2: {
      long *sData = dataResults;
      for (i = 0; i < multiplicity; ++i)
        for (k = 0; k < numOfSpecies; ++k) {
          sData[i*numOfSpecies+k] = (long)aValue;
        }
      break;
    }
    case 3: {
      float *sData = dataResults;
      for (i = 0; i < multiplicity; ++i)
        for (k = 0; k < numOfSpecies; ++k) {
          sData[i*numOfSpecies+k] = (float)aValue;
        }
      break;
    }
    case 4: {
      double *sData = dataResults;
      for (i = 0; i < multiplicity; ++i)
        for (k = 0; k < numOfSpecies; ++k) {
          sData[i*numOfSpecies+k] = aValue;
        }
      break;
    }
  }

  return YES;
}

- (BOOL)setValue: (double)aValue forSpecies: (NSString *)speciesName
{
  int i;
  NSUInteger idx = [species indexOfObject: speciesName];
  if (idx == NSNotFound) return NO;

  switch (encodeTag) {
    case 1: {
      int *sData = dataResults;
      for (i = 0; i < multiplicity; ++i) sData[i*numOfSpecies+idx] = (int)aValue;
      break;
    }
    case 2: {
      long *sData = dataResults;
      for (i = 0; i < multiplicity; ++i) sData[i*numOfSpecies+idx] = (long)aValue;
      break;
    }
    case 3: {
      float *sData = dataResults;
      for (i = 0; i < multiplicity; ++i) sData[i*numOfSpecies+idx] = (float)aValue;
      break;
    }
    case 4: {
      double *sData = dataResults;
      for (i = 0; i < multiplicity; ++i) sData[i*numOfSpecies+idx] = aValue;
      break;
    }
  }
  
  return YES;
}

- (BOOL)adjustValue: (double)aValue forSpecies: (NSString *)speciesName
{
  int i;
  NSUInteger idx = [species indexOfObject: speciesName];
  if (idx == NSNotFound) return NO;
  
  switch (encodeTag) {
    case 1: {
      int *sData = dataResults;
      for (i = 0; i < multiplicity; ++i) {
        sData[i*numOfSpecies+idx] += (int)aValue;
        if (sData[i*numOfSpecies+idx] < 0) sData[i*numOfSpecies+idx] = 0;
      }
      break;
    }
    case 2: {
      long *sData = dataResults;
      for (i = 0; i < multiplicity; ++i) {
        sData[i*numOfSpecies+idx] += (long)aValue;
        if (sData[i*numOfSpecies+idx] < 0) sData[i*numOfSpecies+idx] = 0;
      }
      break;
    }
    case 3: {
      float *sData = dataResults;
      for (i = 0; i < multiplicity; ++i) {
        sData[i*numOfSpecies+idx] += (float)aValue;
        if (sData[i*numOfSpecies+idx] < 0) sData[i*numOfSpecies+idx] = 0;
      }
      break;
    }
    case 4: {
      double *sData = dataResults;
      for (i = 0; i < multiplicity; ++i) {
        sData[i*numOfSpecies+idx] += aValue;
        if (sData[i*numOfSpecies+idx] < 0) sData[i*numOfSpecies+idx] = 0;
      }
      break;
    }
  }
  
  return YES;
}
- (BOOL)knockoutSpecies: (NSString *)speciesName
{
  int i;
  NSUInteger idx = [species indexOfObject: speciesName];
  if (idx == NSNotFound) return NO;

  for (i = 0; i < multiplicity; ++i) dataFlags[i*numOfSpecies+idx] = 1;

  return YES;
}

@end

//
// GPU
//

@implementation ReactionRuleModel (GPU)

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  int k;
  int numParams = [self numOfParameters];
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendFormat: @"typedef struct _%@_GPUgrids {\n", prefixName];
  [GPUCode appendString: @"  int numOfODEs;\n"];
  [GPUCode appendString: @"  int numParams;\n"];
  [GPUCode appendString: @"  int speciesCount;\n"];
  [GPUCode appendString: @"  int multiplicity;\n"];
  [GPUCode appendString: @"  float *currentTime;\n"];
  [GPUCode appendString: @"  float *currentDelta;\n"];
  [GPUCode appendString: @"  float dt;\n"];
  [GPUCode appendString: @"  float EPS;\n"];
  [GPUCode appendString: @"  float *speciesData;\n"];
  [GPUCode appendString: @"  float *speciesData_F1;\n"];
  [GPUCode appendString: @"  float *speciesData_F2;\n"];
  [GPUCode appendString: @"  float *speciesData_F3;\n"];
  [GPUCode appendString: @"  float *speciesData_F4;\n"];
  [GPUCode appendString: @"  float *speciesData_F5;\n"];
  [GPUCode appendString: @"  float *speciesData_F6;\n"];
  [GPUCode appendString: @"  float *speciesData_tmp;\n"];
  [GPUCode appendString: @"  float *speciesData_error;\n"];
  [GPUCode appendString: @"  float *parameters;\n"];
  [GPUCode appendString: @"  float *speciesInterface;\n"];
  [GPUCode appendString: @"  size_t pitch;\n"];
  [GPUCode appendString: @"  size_t idx_pitch;\n"];
  [GPUCode appendFormat: @"} %@_GPUgrids;\n", prefixName];
  
  [GPUCode appendFormat: @"\n__constant__ %@_GPUgrids %@_struct[1];\n", prefixName, prefixName];
  
  [GPUCode appendString: @"\n// parameters\n"];
  //[GPUCode appendFormat: @"__constant__ float %@_parameters[%d];\n\n", prefixName, numParams];
  
  for (k = 0; k < numParams; ++k) {
    NSString *paramName = [self nameOfParameter: k];
    [GPUCode appendFormat: @"#define %@ speciesParameters[%d*pitch+ODEnum]\n", paramName, k];
  }
  
  // species index
  [GPUCode appendString: @"\n// Species index\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_species %d\n", s, k];
  }
  
  // variable definitions (to avoid running out of registers for large models)
  [GPUCode appendString: @"\n// Variable definitions\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_val speciesData[%@_species*pitch+ODEnum]\n", s, s];
    //[GPUCode appendFormat: @"#define %@_F1val speciesData_F1[%@_species*pitch+ODEnum]\n", s, s, k];
  }
  
  return GPUCode;
}

- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName
{
  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;
  
  //
  // Headers and defines
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];
  
  //
  // Allocation
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"*%@_allocGPUKernel(void *model, int numOfODEs, int numParams, int speciesCount, int multiplicity, float dt, float EPS)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"allocation function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @"  %@_GPUgrids *ptrs = (%@_GPUgrids *)malloc(sizeof(%@_GPUgrids));\n", prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n // Save parameters\n"];
  [GPUCode appendString: @" ptrs->numOfODEs = numOfODEs;\n"];
  [GPUCode appendString: @" ptrs->numParams = numParams;\n"];
  [GPUCode appendString: @" ptrs->dt = dt;\n"];
  [GPUCode appendString: @" ptrs->EPS = EPS;\n"];
  //[GPUCode appendString: @" ptrs->localEpsilonCheck = (int *)malloc(numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" ptrs->speciesCount = speciesCount;\n"];
  [GPUCode appendString: @" ptrs->multiplicity = multiplicity;\n"];
  
  [GPUCode appendString: @"\n // Allocate device memory\n"];
  //[GPUCode appendString: @" cudaMalloc((void**)&(ptrs->epsilonCheck), numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData), &(ptrs->pitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData_F1), &(ptrs->pitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData_F2), &(ptrs->pitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData_tmp), &(ptrs->pitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData_error), &(ptrs->pitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->parameters), &(ptrs->pitch), numOfODEs * sizeof(float), numParams);\n"];
  
  [GPUCode appendString: @"\n"];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->currentTime), numOfODEs * sizeof(float));\n"];
  [GPUCode appendString: @" cudaMemset(ptrs->currentTime, 0, numOfODEs * sizeof(float));\n"];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->currentDelta), numOfODEs * sizeof(float));\n"];
  [GPUCode appendString: @" cudaMemset(ptrs->currentDelta, 0, numOfODEs * sizeof(float));\n"];
  
  [GPUCode appendString: @"\n"];
  [GPUCode appendString: @" ptrs->idx_pitch = ptrs->pitch / sizeof(float);\n"];
  
  [GPUCode appendString: @"\n"];
  [GPUCode appendString: @" return ptrs;\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"allocation code"];
  
  //
  // Data Transfer
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *p, int aFlag, float *hostParameters, float *hostData)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *ptrs = (%@_GPUgrids *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @"\n if (aFlag) {\n"];
  [GPUCode appendString: @"    // Copy host memory to device memory\n"];
  [GPUCode appendFormat: @"    cudaMemcpyToSymbol(%@_struct, p, sizeof(%@_GPUgrids), 0, cudaMemcpyHostToDevice);\n", prefixName, prefixName];
  
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->speciesData, ptrs->pitch, (const cudaArray *)hostData, ptrs->numOfODEs * sizeof(float), ptrs->numOfODEs * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->parameters, ptrs->pitch, (const cudaArray *)hostParameters, ptrs->numOfODEs * sizeof(float), ptrs->numOfODEs * sizeof(float), ptrs->numParams, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemset2D(ptrs->speciesData_tmp, ptrs->pitch, 0, ptrs->numOfODEs * sizeof(float), ptrs->speciesCount);\n"];
  
  //[GPUCode appendString: @"    cudaMemset(ptrs->epsilonCheck, 0, ptrs->numOfODEs * sizeof(int));\n"];
  
  [GPUCode appendString: @" } else {\n"];
  
  [GPUCode appendString: @"    // Copy result to host memory\n"];
  //[GPUCode appendString: @"    cudaMemcpy(ptrs->localEpsilonCheck, ptrs->epsilonCheck, ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(hostData, ptrs->numOfODEs * sizeof(float), ptrs->speciesData, ptrs->pitch, ptrs->numOfODEs * sizeof(float), ptrs->speciesCount, cudaMemcpyDeviceToHost);\n"];
  
  [GPUCode appendString: @" }\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];
  
  //
  // Execution
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, float *hostData, double startTime, double nextTime)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" %@_GPUgrids *%@_ptrs = (%@_GPUgrids *)%@_data;\n", prefixName, prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @" RModelRK2_GPUptrs *%@_ptrs = (RModelRK2_GPUptrs *)%@_data;\n", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" int %@_blocksPerGrid = 1;\n", prefixName];
  [GPUCode appendFormat: @" int %@_threadsPerBlock = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_ptrs->numOfODEs < 256) {\n", prefixName];
  [GPUCode appendFormat: @"   %@_threadsPerBlock = %@_ptrs->numOfODEs;\n", prefixName, prefixName];
  [GPUCode appendFormat: @"   %@_blocksPerGrid = 1;\n", prefixName];
  [GPUCode appendString: @" } else {\n"];
  [GPUCode appendFormat: @"   %@_threadsPerBlock = 256;\n", prefixName];
  [GPUCode appendFormat: @"   %@_blocksPerGrid = (%@_ptrs->numOfODEs + %@_threadsPerBlock - 1) / %@_threadsPerBlock;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @" }\n"];
  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];
  
  GPUCode = [NSMutableString new];
  if ([numericalScheme isEqualToString: @"RungeKutta2ndOrder"]) {
    [GPUCode appendFormat: @"      %@_kernel1_2rk<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
    [GPUCode appendFormat: @"      %@_kernel2_2rk<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(nextTime);\n", prefixName, prefixName, prefixName];
    if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
  }
  //[GPUCode appendFormat: @"   %@_kernel_F1<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(%@_ptrs->numOfODEs, %@_ptrs->speciesPitch/sizeof(float), %@_ptrs->speciesData, %@_ptrs->speciesF1, %@_ptrs->speciesF2, %@_ptrs->parameters, %@_ptrs->dt);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  //if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
  //[GPUCode appendFormat: @"   %@_kernel_F2<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(%@_ptrs->numOfODEs, %@_ptrs->speciesPitch/sizeof(float), %@_ptrs->speciesData, %@_ptrs->speciesF1, %@_ptrs->speciesF2, %@_ptrs->parameters, %@_ptrs->dt);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  //if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
  //[GPUCode appendFormat: @"   cudaMemcpy2D(%@_ptrs->speciesData, %@_ptrs->speciesPitch, %@_ptrs->speciesF2, %@_ptrs->speciesPitch, %@_ptrs->numOfODEs * sizeof(float), %@_ptrs->speciesCount, cudaMemcpyDeviceToDevice);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];
  
  //
  // Release
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_releaseGPUKernel(void *model, void *p)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"release function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *ptrs = (%@_GPUgrids *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData_F1);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData_F2);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData_tmp);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData_error);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->parameters);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->currentTime);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->currentDelta);\n"];
  //[GPUCode appendString: @" cudaFree(ptrs->epsilonCheck);\n"];
  //[GPUCode appendString: @" free(ptrs->localEpsilonCheck);\n"];
  [GPUCode appendString: @" free(ptrs);\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"release code"];
  
  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"RModelGPUFunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];
  
  return GPUDictionary;    
}

- (NSMutableString *)functionKernelGPUCode: (NSString *)prefixName
{
  int k, l, j;
  NSString *param;
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  id spatialModel = [modelController spatialModel];
  int dimensions = [spatialModel dimensions];
  if (dimensions == 0) dimensions = 1;

  //
  // Different kernel code required for different dimensional model
  // as the memory layout is different for each.
  //
  
  [GPUCode appendString: @"\n// Reaction function used by Runge-Kutta solvers\n"];
  [GPUCode appendString: @"__device__ void\n"];

  switch (dimensions) {
    case 1:
      // 1D spatial model, standard ReactionRuleModel with multiple ODEs
      [GPUCode appendFormat: @"%@_kernel_function(int ODEnum, int pitch, float *speciesData, float *speciesData_in, float *speciesData_out, float *speciesParameters, float mh, float mf)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendString: @"   float interactionValue, termValue, F_val;\n"];
      break;
      
    case 2:
      // 2D spatial model, as used by ReactionDiffusionModel
      [GPUCode appendFormat: @"%@_kernel_function(int i, int j, float mh, float mf)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendString: @"   float interactionValue, termValue, F_val;\n"];
      break;
  }

  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n   // %@\n", s];
    [GPUCode appendString: @"   interactionValue = 0.0;\n"];
    
    NSArray *funcs = [speciesFunctions objectAtIndex: k];
    for (l = 0; l < [funcs count]; ++l) {
      NSDictionary *d = [funcs objectAtIndex: l];
      NSString *reactionName = [d objectForKey: @"reaction"];
      NSString *rType = [d objectForKey: @"type"];
      NSString *rateFunction = [d objectForKey: @"rate"];
      NSArray *reactants = [d objectForKey: @"reactants"];
      NSArray *enzymes = [d objectForKey: @"enzymes"];
      
      [GPUCode appendFormat: @"\n   // reaction: %@ type: %@\n", reactionName, rType];
      
      if ([rType isEqualToString: @"signal"]) {
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          [GPUCode appendString: @"   termValue = 1.0;\n"];
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            switch (dimensions) {
              case 1:
                [GPUCode appendFormat: @"   F_val = %@_val + mf * speciesData_in[%@_species*pitch+ODEnum];\n", rName, rName];
                break;
              case 2:
                [GPUCode appendFormat: @"   F_val = %@_struct->%@[j*%@_struct->idx_pitch+i] + mf * %@_struct->%@_in[j*%@_struct->idx_pitch+i];\n", prefixName, rName, prefixName, prefixName, rName, prefixName];
                break;
            }                
            [GPUCode appendFormat: @"   termValue *= SHILL(F_val"];
            
            param = [NSString stringWithFormat: @"%@_%@_pmax_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@", param];
            
            param = [NSString stringWithFormat: @"%@_%@_c_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@", param];
            
            param = [NSString stringWithFormat: @"%@_%@_h_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@);\n", param];
          }
          
        } else if ([rateFunction isEqualToString: @"Constant"]) {
          param = [NSString stringWithFormat: @"%@_%@_c", reactionName, s];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          [GPUCode appendFormat: @"   termValue = %@;\n", param];
          
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_%@_k", reactionName, s];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          [GPUCode appendFormat: @"   termValue = %@;\n", param];
          
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            switch (dimensions) {
              case 1:
                [GPUCode appendFormat: @"   F_val = %@_val + mf * speciesData_in[%@_species*pitch+ODEnum];\n", rName, rName];
                break;
              case 2:
                [GPUCode appendFormat: @"   F_val = %@_struct->%@[j*%@_struct->idx_pitch+i] + mf * %@_struct->%@_in[j*%@_struct->idx_pitch+i];\n", prefixName, rName, prefixName, prefixName, rName, prefixName];
                break;
            }
            [GPUCode appendString: @"   termValue *= F_val;\n"];
          }
          
          if (enzymes) {
            for (j = 0; j < [enzymes count]; ++j) {
              NSString *eName = [enzymes objectAtIndex: j];
              switch (dimensions) {
                case 1:
                  [GPUCode appendFormat: @"   F_val = %@_val + mf * speciesData_in[%@_species*pitch+ODEnum];\n", eName, eName];
                  break;
                case 2:
                  [GPUCode appendFormat: @"   F_val = %@_struct->%@[j*%@_struct->idx_pitch+i] + mf * %@_struct->%@_in[j*%@_struct->idx_pitch+i];\n", prefixName, eName, prefixName, prefixName, eName, prefixName];
                  break;
              }
              [GPUCode appendFormat: @"   termValue *= SHILL(F_val"];
              
              param = [NSString stringWithFormat: @"%@_%@_pmax_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_%@_c_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_%@_h_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@);\n", param];
            }
          }
        }
        
      } else if ([rType isEqualToString: @"mass"]) {
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          // TODO: need enzyme
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_k", reactionName];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          
          if ([[d objectForKey: @"sign"] isEqualToString: @"positive"])
            [GPUCode appendFormat: @"   termValue = %@;\n", param];
          else
            [GPUCode appendFormat: @"   termValue = -%@;\n", param];
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            switch (dimensions) {
              case 1:
                [GPUCode appendFormat: @"   F_val = %@_val + mf * speciesData_in[%@_species*pitch+ODEnum];\n", rName, rName];
                break;
              case 2:
                [GPUCode appendFormat: @"   F_val = %@_struct->%@[j*%@_struct->idx_pitch+i] + mf * %@_struct->%@_in[j*%@_struct->idx_pitch+i];\n", prefixName, rName, prefixName, prefixName, rName, prefixName];
                break;
            }
            [GPUCode appendString: @"   termValue *= F_val;\n"];
          }
          
          if (enzymes) {
            for (j = 0; j < [enzymes count]; ++j) {
              NSString *eName = [enzymes objectAtIndex: j];
              switch (dimensions) {
                case 1:
                  [GPUCode appendFormat: @"   F_val = %@_val + mf * speciesData_in[%@_species*pitch+ODEnum];\n", eName, eName];
                  break;
                case 2:
                  [GPUCode appendFormat: @"   F_val = %@_struct->%@[j*%@_struct->idx_pitch+i] + mf * %@_struct->%@_in[j*%@_struct->idx_pitch+i];\n", prefixName, eName, prefixName, prefixName, eName, prefixName];
                  break;
              }
              [GPUCode appendString: @"   termValue *= SHILL(F_val"];
              
              param = [NSString stringWithFormat: @"%@_pmax_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_c_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_h_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@);\n", param];
            }
          }
        }
        
      }
      
      [GPUCode appendString: @"   interactionValue += termValue;\n"];
    }
    
    [GPUCode appendString: @"\n   F_val = mh * interactionValue;\n"];
    switch (dimensions) {
      case 1:
        [GPUCode appendFormat: @"   speciesData_out[%@_species*pitch+ODEnum] = F_val;\n", s];
        break;
      case 2:
        [GPUCode appendFormat: @"   %@_struct->%@_out[j*%@_struct->idx_pitch+i] = F_val;\n", prefixName, s, prefixName];
        break;
    }
  }
  
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;    
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  int k;
  NSMutableString *GPUCode = [NSMutableString new];
  
  // kernel to calculate reaction function
  [GPUCode appendString: [self functionKernelGPUCode: prefixName]];
  
  // kernel for numerical method
  if ([numericalScheme isEqualToString: @"RungeKutta2ndOrder"]) {
    
    [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel1_2rk(float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    
    [GPUCode appendFormat: @"\n   if (%@_struct->currentDelta[ODEnum] == 0.0f) %@_struct->currentDelta[ODEnum] = %@_struct->dt;\n", prefixName, prefixName, prefixName];
    
    // Calculate F1
    [GPUCode appendString: @"\n   // calculate F1\n"];
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, %@_struct->idx_pitch, %@_struct->speciesData, %@_struct->speciesData_tmp,\n", prefixName, prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_F1, %@_struct->parameters, %@_struct->currentDelta[ODEnum], 0.0f);\n", prefixName, prefixName, prefixName];
    
    [GPUCode appendString: @"}\n"];

    [GPUCode appendString: @"\n// F2 Kernel for 2nd order Runge-Kutta\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_kernel2_2rk(float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    
    [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
    
    [GPUCode appendFormat: @"\n   if (ODEnum >= %@_struct->numOfODEs) return;\n", prefixName];
    
    [GPUCode appendFormat: @"\n   if (%@_struct->currentDelta[ODEnum] == 0.0f) %@_struct->currentDelta[ODEnum] = %@_struct->dt;\n", prefixName, prefixName, prefixName];
    
    // Calculate F2
    [GPUCode appendString: @"\n   // calculate F2\n"];
    [GPUCode appendFormat: @"   %@_kernel_function(ODEnum, %@_struct->idx_pitch, %@_struct->speciesData, %@_struct->speciesData_F1,\n", prefixName, prefixName, prefixName, prefixName];
    [GPUCode appendFormat: @"                      %@_struct->speciesData_F2, %@_struct->parameters, %@_struct->currentDelta[ODEnum], 0.5f);\n", prefixName, prefixName, prefixName];
    
    // calculate x(t + dt)                                                                                                                             
    [GPUCode appendString: @"\n   // calculate x(t + dt)\n"];
    for (k = 0; k < numOfSpecies; ++k) {
      id s = [species objectAtIndex: k];
      [GPUCode appendFormat: @"   %@_struct->speciesData[%@_species*%@_struct->idx_pitch+ODEnum] += %@_struct->speciesData_F2[%@_species*%@_struct->idx_pitch+ODEnum];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    }
    
    [GPUCode appendString: @"}\n"];
    
  } else {
    printf("ERROR: unknown numerical scheme: %s\n", [numericalScheme UTF8String]);
    return nil;
  }
  
  return GPUCode;
}


#if 0
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  int k, l, j;
  NSString *param;
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  // 2nd order Runge-Kutta
  
  [GPUCode appendString: @"\n// Kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F1(int numOfODEs, int pitch, float *speciesData, float *speciesData_F1, float *speciesData_F2, float *speciesParameters, float dt)", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   float interactionValue, termValue, F1_val;\n"];
  
  [GPUCode appendString: @"\n   if (ODEnum >= numOfODEs) return;\n"];
  
  // Calculate F1
  [GPUCode appendString: @"\n   // calculate F1\n"];
  
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n   // %@\n", s];
    [GPUCode appendString: @"   interactionValue = 0.0;\n"];
    
    NSArray *funcs = [speciesFunctions objectAtIndex: k];
    for (l = 0; l < [funcs count]; ++l) {
      NSDictionary *d = [funcs objectAtIndex: l];
      NSString *reactionName = [d objectForKey: @"reaction"];
      NSString *rType = [d objectForKey: @"type"];
      NSString *rateFunction = [d objectForKey: @"rate"];
      NSArray *reactants = [d objectForKey: @"reactants"];
      NSArray *enzymes = [d objectForKey: @"enzymes"];
      
      [GPUCode appendFormat: @"\n   // reaction: %@ type: %@\n", reactionName, rType];
      
      if ([rType isEqualToString: @"signal"]) {
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          [GPUCode appendString: @"   termValue = 1.0;\n"];
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            [GPUCode appendFormat: @"   termValue *= SHILL(%@_val", rName];
            
            param = [NSString stringWithFormat: @"%@_%@_pmax_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@", param];
            
            param = [NSString stringWithFormat: @"%@_%@_c_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@", param];
            
            param = [NSString stringWithFormat: @"%@_%@_h_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@);\n", param];
          }
          
        } else if ([rateFunction isEqualToString: @"Constant"]) {
          param = [NSString stringWithFormat: @"%@_%@_c", reactionName, s];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          [GPUCode appendFormat: @"   termValue = %@;\n", param];
          
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_%@_k", reactionName, s];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          [GPUCode appendFormat: @"   termValue = %@;\n", param];
          
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            [GPUCode appendFormat: @"   termValue *= %@_val;\n", rName];
          }
          
          if (enzymes) {
            for (j = 0; j < [enzymes count]; ++j) {
              NSString *eName = [enzymes objectAtIndex: j];
              [GPUCode appendFormat: @"   termValue *= SHILL(%@_val", eName];
              
              param = [NSString stringWithFormat: @"%@_%@_pmax_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_%@_c_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_%@_h_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@);\n", param];
            }
          }
        }
        
      } else if ([rType isEqualToString: @"mass"]) {
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          // TODO: need enzyme
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_k", reactionName];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          
          if ([[d objectForKey: @"sign"] isEqualToString: @"positive"])
            [GPUCode appendFormat: @"   termValue = %@;\n", param];
          else
            [GPUCode appendFormat: @"   termValue = -%@;\n", param];
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            [GPUCode appendFormat: @"   termValue *= %@_val;\n", rName];
          }
          
          if (enzymes) {
            for (j = 0; j < [enzymes count]; ++j) {
              NSString *eName = [enzymes objectAtIndex: j];
              [GPUCode appendFormat: @"   termValue *= SHILL(%@_val", eName];
              
              param = [NSString stringWithFormat: @"%@_pmax_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_c_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_h_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@);\n", param];
            }
          }
        }
        
      }
      
      [GPUCode appendString: @"   interactionValue += termValue;\n"];
    }
    
    [GPUCode appendFormat: @"\n   F1_val = %@_val + 0.5f * dt * interactionValue;\n", s];
    [GPUCode appendString: @"   if (F1_val < 0) F1_val = 0;\n"];
    [GPUCode appendFormat: @"   speciesData_F1[%@_species*pitch+ODEnum] = F1_val;\n", s];
  }
  
  [GPUCode appendString: @"}\n"];
  
  [GPUCode appendString: @"\n// Kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F2(int numOfODEs, int pitch, float *speciesData, float *speciesData_F1, float *speciesData_F2, float *speciesParameters, float dt)", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   float interactionValue, termValue, F2_val;\n"];
  
  [GPUCode appendString: @"\n   if (ODEnum >= numOfODEs) return;\n"];
  
  // Calculate F2
  [GPUCode appendString: @"\n   // calculate F2\n"];
  
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n   // %@\n", s];
    [GPUCode appendString: @"   interactionValue = 0.0;\n"];
    
    NSArray *funcs = [speciesFunctions objectAtIndex: k];
    for (l = 0; l < [funcs count]; ++l) {
      NSDictionary *d = [funcs objectAtIndex: l];
      NSString *reactionName = [d objectForKey: @"reaction"];
      NSString *rType = [d objectForKey: @"type"];
      NSString *rateFunction = [d objectForKey: @"rate"];
      NSArray *reactants = [d objectForKey: @"reactants"];
      NSArray *enzymes = [d objectForKey: @"enzymes"];
      
      [GPUCode appendFormat: @"\n   // reaction: %@ type: %@\n", reactionName, rType];
      
      if ([rType isEqualToString: @"signal"]) {
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          [GPUCode appendString: @"   termValue = 1.0;\n"];
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            [GPUCode appendFormat: @"   termValue *= SHILL(%@_F1val", rName];
            
            param = [NSString stringWithFormat: @"%@_%@_pmax_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@", param];
            
            param = [NSString stringWithFormat: @"%@_%@_c_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@", param];
            
            param = [NSString stringWithFormat: @"%@_%@_h_%@", reactionName, s, rName];
            if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
            [GPUCode appendFormat: @", %@);\n", param];
          }
          
        } else if ([rateFunction isEqualToString: @"Constant"]) {
          param = [NSString stringWithFormat: @"%@_%@_c", reactionName, s];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          [GPUCode appendFormat: @"   termValue = %@;\n", param];
          
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_%@_k", reactionName, s];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          [GPUCode appendFormat: @"   termValue = %@;\n", param];
          
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            [GPUCode appendFormat: @"   termValue *= %@_F1val;\n", rName];
          }
          
          if (enzymes) {
            for (j = 0; j < [enzymes count]; ++j) {
              NSString *eName = [enzymes objectAtIndex: j];
              [GPUCode appendFormat: @"   termValue *= SHILL(%@_F1val", eName];
              
              param = [NSString stringWithFormat: @"%@_%@_pmax_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_%@_c_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_%@_h_%@", reactionName, s, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@);\n", param];
            }
          }
        }
        
      } else if ([rType isEqualToString: @"mass"]) {
        
        if ([rateFunction isEqualToString: @"Hill"]) {
          // TODO: need enzyme
        } else if ([rateFunction isEqualToString: @"MassAction"]) {
          param = [NSString stringWithFormat: @"%@_k", reactionName];
          if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
          
          if ([[d objectForKey: @"sign"] isEqualToString: @"positive"])
            [GPUCode appendFormat: @"   termValue = %@;\n", param];
          else
            [GPUCode appendFormat: @"   termValue = -%@;\n", param];
          
          for (j = 0; j < [reactants count]; ++j) {
            NSString *rName = [reactants objectAtIndex: j];
            [GPUCode appendFormat: @"   termValue *= %@_F1val;\n", rName];
          }
          
          if (enzymes) {
            for (j = 0; j < [enzymes count]; ++j) {
              NSString *eName = [enzymes objectAtIndex: j];
              [GPUCode appendFormat: @"   termValue *= SHILL(%@_F1val", eName];
              
              param = [NSString stringWithFormat: @"%@_pmax_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_c_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@", param];
              
              param = [NSString stringWithFormat: @"%@_h_%@", reactionName, eName];
              if ([sharedParameters objectForKey: param]) param = [sharedParameters objectForKey: param];
              [GPUCode appendFormat: @", %@);\n", param];
            }
          }
        }
        
      }
      
      [GPUCode appendString: @"   interactionValue += termValue;\n"];
    }
    
    [GPUCode appendFormat: @"\n   F2_val = %@_val + dt * interactionValue;\n", s];
    [GPUCode appendString: @"   if (F2_val < 0) F2_val = 0;\n"];
    [GPUCode appendFormat: @"   speciesData_F2[%@_species*pitch+ODEnum] = F2_val;\n", s];
  }
  
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;
}
#endif

- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName { return nil; }

@end

//
// Run GPU
//

@implementation ReactionRuleModel (GPURun)

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions
{
  ModelGPUData *gpuData = (ModelGPUData *)malloc(sizeof(ModelGPUData));
  
  gpuData->numModels = numModels;
  gpuData->numParams = [self numOfParameters];
  gpuData->gpuFunctions = gpuFunctions;
  gpuData->data = malloc(sizeof(float) * numModels * numOfSpecies * multiplicity);
  gpuData->flags = malloc(sizeof(int) * numModels * numOfSpecies * multiplicity);
  gpuData->parameters = (float *)malloc(sizeof(float) * numModels * gpuData->numParams);

  //gpuData->extraData = (void *)[NSMutableDictionary new];

  return gpuData;
}

- (void)freeGPUData: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if (gpuData) {
    if (gpuData->data) free(gpuData->data);
    if (gpuData->flags) free(gpuData->flags);
    if (gpuData->parameters) free(gpuData->parameters);
    if (gpuData->extraData) [((NSMutableDictionary *)gpuData->extraData) release];
    free(gpuData);
  }
}

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int j;
  
  float *indData = dataResults;
  float (*hostData)[numOfSpecies * multiplicity][gpuData->numModels] = (void *)gpuData->data;
  
  if (aFlag)
    for (j = 0; j < (numOfSpecies * multiplicity); ++j) (*hostData)[j][aNum] = indData[j];
  else
    for (j = 0; j < (numOfSpecies * multiplicity); ++j) indData[j] = (*hostData)[j][aNum];
}

- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int p = 0;
  int k;
  
  float (*hostParameters)[gpuData->numParams][gpuData->numModels] = (void *)gpuData->parameters;
  
  if (aFlag) {
    for (k = 0; k < numOfParameters; ++k) {
      (*hostParameters)[p][aNum] = parameterValues[k];
      //printf("assignParameters: %p %d %d: %f\n", self, p, aNum, parameterValues[k]);
      ++p;
    }    
  } else {
    // Transfer parameters from GPU??
  }
  
  return p;
}

- (void)allocGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
  gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, gpuData->numModels, gpuData->numParams, numOfSpecies, multiplicity, (float)dt, 0);
}

- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
  (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, aFlag, gpuData->parameters, gpuData->data);
}

- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
  (gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, gpuData->data, currentTime, endTime);
}

- (void)releaseGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
  (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
}


@end
