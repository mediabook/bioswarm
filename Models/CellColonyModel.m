/*
 CellColonyModel.m
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "CellColonyModel.h"
#import "BioSwarmController.h"

#import "internal.h"

@implementation CellColonyModel

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  NSString *param;

  [super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent];
  
  //NSDictionary *fullModel = [modelController model];
  
  param = [model objectForKey: @"numOfCells"];
  CHECK_PARAM(param, "numOfCells");
  numOfCells = [param intValue];
  
  // register for notifications
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(setMultiplicityForSubmodels)
                                               name:BioSwarmModelIsLoadedNotification object:nil];

  return self;
}

- (int)numOfCells { return numOfCells; }
- (int)numOfModelObjects { return numOfCells; }

- (void)setMultiplicityForSubmodels
{
  // model is loaded so set multiplicity for all submodels to be number of cells
  NSInvocation *anInv = [NSInvocation invocationWithMethodSignature: [self methodSignatureForSelector: @selector(setMultiplicity:)]];
  [anInv setSelector: @selector(setMultiplicity:)];
  [anInv setArgument: &numOfCells atIndex: 2];
  [self performHierarchyInvocation: anInv];
}

#if 0
- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState
{
  [cellSpatialModel allocateSimulationWithEncode: anEncode fileState: aState];
}

- (BOOL)writeDataWithCheck: (BOOL)aFlag
{
  return [cellSpatialModel writeDataWithCheck: aFlag];
}

- (BOOL)readData
{
  return [cellSpatialModel readData];
}

- (void)rewindFiles
{
  [cellSpatialModel rewindFiles];
}

- (void)initializeSimulation
{
  [cellSpatialModel initializeSimulation];
}

- (void)runSimulation
{
  [cellSpatialModel runSimulation];
}

- (void)cleanupSimulation
{
  [cellSpatialModel cleanupSimulation];
}
#endif
@end

//
// GPU
//
#if 0
@implementation CellColonyModel (GPU)

- (NSDictionary *)controllerGPUCode: (NSString *)prefixName
{
  return [cellSpatialModel controllerGPUCode: prefixName];
}

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  return [cellSpatialModel definitionsGPUCode: prefixName];
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
#if 0
  NSDictionary *cellSpatialKernel = [cellSpatialModel splitKernelGPUCode: prefixName];
  NSMutableString *GPUCode = [NSMutableString string];
  int i;
  
  NSArray *funcs = [cellSpatialKernel objectForKey: @"functions"];
  for (i = 0; i < [funcs count]; ++i) {
    NSDictionary *funcDict = [cellSpatialKernel objectForKey: [funcs objectAtIndex: i]];
    [GPUCode appendString: @"__global__ void\n"];
    NSString *s = [funcDict objectForKey: @"function"];
    [GPUCode appendString: s];
    [GPUCode appendString: @"{\n"];
    s = [funcDict objectForKey: @"common variables"];
    [GPUCode appendString: s];
    s = [funcDict objectForKey: @"forces"];
    //s = [funcDict objectForKey: @"intracellular"];
    //[GPUCode appendString: s];
    //s = [funcDict objectForKey: @"intercellular"];
    [GPUCode appendString: s];
    s = [funcDict objectForKey: @"update"];
    [GPUCode appendString: s];
    [GPUCode appendString: @"}\n\n"];
  }
#else
  NSMutableString *GPUCode = [cellSpatialModel kernelGPUCode: prefixName];
#endif
  
  return GPUCode;
}

- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName
{
  return [cellSpatialModel cleanupGPUCode: prefixName];
}

@end

//
// Run GPU
//

@implementation CellColonyModel (GPURun)

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions
{
  cellSpatialData = [cellSpatialModel allocGPUData: numModels withGPUFunctions: gpuFunctions];

  //cellInternalData = [cellInternalModel allocGPUData: numOfCells withGPUFunctions: 
  return cellSpatialData;
}

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  [cellSpatialModel assignData: data ofNumber: aNum toGPU: aFlag];
}

- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  return [cellSpatialModel assignParameters: data ofNumber: aNum toGPU: aFlag];
}

- (void)allocGPUKernel: (void *)data
{
  [cellSpatialModel allocGPUKernel: data];
}

- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag
{
  [cellSpatialModel transferGPUKernel: data toGPU: aFlag];
}

- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
{
  [cellSpatialModel invokeGPUKernel: data currentTime: currentTime endTime: endTime];
}

- (void)releaseGPUKernel: (void *)data
{
  [cellSpatialModel releaseGPUKernel: data];
}

@end

//
// Visualization
//

@implementation CellColonyModel (Povray)

- (NSMutableString *)povrayCode
{
  return [cellSpatialModel povrayCode];
}

@end
#endif