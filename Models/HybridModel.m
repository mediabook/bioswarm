/*
 HybridModel.m
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "HybridModel.h"
#import "internal.h"


@implementation HybridModel

- newFromModelIndividual
{
  return [[[self class] alloc] initWithModel: model];
}

- initWithModel: (NSDictionary *)aModel andController: anObj;
{
  [self init];
  
  controller = [anObj retain];
  model = [aModel retain];
  
  return self;
}

- (void)dealloc
{
  [model release];
  [controller release];
  
  [super dealloc];
}

- (NSDictionary *)model { return model; }

- (void)allocateSimulationWithEncode: (NSString *)anEncode
{
  [self allocateSimulationWithEncode: anEncode fileState: 1];
}

- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState
{
}

- (void)writeData
{
  [self writeDataWithCheck: YES];
}

- (void)writeDataWithCheck: (BOOL)aFlag
{
}

- (void)initializeSimulation
{
}

- (void)runSimulation
{
}

- (void)cleanupSimulation
{
}

@end

//
// GPU
//

@implementation HybridModel (GPU)

- (NSMutableString *)controllerGPUCode: (NSString *)prefixName
{
  return nil;
}

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  return nil;
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  return nil;
}

- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName
{
  return nil;
}

@end
