/*
 ReactionModel.m
 
 Chemical reaction model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "ReactionModel.h"
#import "SimulationController.h"

#include <math.h>

#define CHECK_PARAM(P,N) if (!P) { NSLog(@"Missing parameter: %@",N); }
#define CHECK_PARAMF(P,N,F) if (!P) { NSLog(@"Missing parameter: %@",N); F = NO; }

#define RAND_NUM ((double)rand() / (double)RAND_MAX )

#define OUTPUT 0

#define BLOWUP_THRESHOLD 1000000
#define EPS 0.0001

#if 0
// Generate normally distributed random number
double normal_dist_rand(double mean, double sd)
{
  double fac, radius, v1, v2;
  double rd1Value, rd2Value;
  
  do {
    rd1Value = RAND_NUM;
    rd2Value = RAND_NUM;
    v1 = (2.0 * rd1Value) - 1.0;
    v2 = (2.0 * rd2Value) - 1.0;
    radius = v1*v1 + v2*v2; 
  } while (radius >= 1.0);
  fac = sqrt (-2.0 * log (radius) / radius);
  return v2 * fac * sd + mean;    // use fixed params
}
#endif

@implementation ReactionModel

- newFromModelIndividual
{
  return [[[self class] alloc] initWithModel: model];
}

- initWithModel: (NSDictionary *)aModel
{
  return [self initWithModel: aModel andController: nil];
}

- initWithModel: (NSDictionary *)aModel andController: anObj
{
  int i, k, l;
  id s;
  NSString *param, *paramName;
  
  [super initWithModel: aModel andController: anObj];
  
  isGPUStatic = YES;
  s = [aModel objectForKey: @"static"];
  if (s) isGPUStatic = [s boolValue];

  // get model definition
  species = [aModel objectForKey: @"species"];
  CHECK_PARAM(species, @"species")
  speciesCount = [species count];

  // hill interactions
  interactions = malloc(sizeof(int *) * speciesCount);
  interactionsCount = malloc(sizeof(int) * speciesCount);
  interactions_pmin = malloc(sizeof(double *) * speciesCount);
  interactions_pmax = malloc(sizeof(double *) * speciesCount);
  interactions_c = malloc(sizeof(double *) * speciesCount);
  interactions_h = malloc(sizeof(double *) * speciesCount);
  for (k = 0; k < speciesCount; ++k) {
    s = [species objectAtIndex: k];
    s = [aModel objectForKey: [NSString stringWithFormat: @"%@_interactions", s]];
    if (!s) {
      interactions[k] = NULL;
      interactionsCount[k] = 0;
    } else {
      interactionsCount[k] = [s count];
      interactions[k] = malloc(sizeof(int) * interactionsCount[k]);
      interactions_pmin[k] = malloc(sizeof(double) * interactionsCount[k]);
      interactions_pmax[k] = malloc(sizeof(double) * interactionsCount[k]);
      interactions_c[k] = malloc(sizeof(double) * interactionsCount[k]);
      interactions_h[k] = malloc(sizeof(double) * interactionsCount[k]);
      for (l = 0; l < interactionsCount[k]; ++l) {
        interactions[k][l] = [species indexOfObject: [s objectAtIndex: l]];
        if (interactions[k][l] == NSNotFound){
          printf("ERROR: Interaction of %s with unknown species %s.\n", [[species objectAtIndex: k] UTF8String],
                 [[s objectAtIndex: l] UTF8String]);
          return nil;
        }
        if (OUTPUT) NSLog(@"%@ -> %@", [species objectAtIndex: interactions[k][l]], [species objectAtIndex: k]);
        
        paramName = [NSString stringWithFormat: @"%@_pmin_%@",
                     [species objectAtIndex: k],
                     [species objectAtIndex: interactions[k][l]]];
        param = [aModel objectForKey: paramName];
        CHECK_PARAM(param, paramName)
        interactions_pmin[k][l] = [param doubleValue];

        paramName = [NSString stringWithFormat: @"%@_pmax_%@",
                     [species objectAtIndex: k],
                     [species objectAtIndex: interactions[k][l]]];
        param = [aModel objectForKey: paramName];
        CHECK_PARAM(param, paramName)
        interactions_pmax[k][l] = [param doubleValue];
      
        paramName = [NSString stringWithFormat: @"%@_c_%@",
                     [species objectAtIndex: k],
                     [species objectAtIndex: interactions[k][l]]];
        param = [aModel objectForKey: paramName];
        CHECK_PARAM(param, paramName)
        interactions_c[k][l] = [param doubleValue];

        paramName = [NSString stringWithFormat: @"%@_h_%@",
                     [species objectAtIndex: k],
                     [species objectAtIndex: interactions[k][l]]];
        param = [aModel objectForKey: paramName];
        CHECK_PARAM(param, paramName)
        interactions_h[k][l] = [param doubleValue];

        if (OUTPUT) NSLog(@"%lf %lf %lf %lf", interactions_pmin[k][l], interactions_pmax[k][l],
                          interactions_c[k][l], interactions_h[k][l]);
      }
    }
  }
  
  // linear interactions
  linearPlus = malloc(sizeof(int *) * speciesCount);
  linearMinus = malloc(sizeof(int *) * speciesCount);
  linearPlusCount = malloc(sizeof(int) * speciesCount);
  linearMinusCount = malloc(sizeof(int) * speciesCount);
  for (k = 0; k < speciesCount; ++k) {
    linearPlusCount[k] = 0;
    linearMinusCount[k] = 0;
  }
  id li = [aModel objectForKey: @"linearInteractions"];
  if (li) {
    linearCount = [li count];
    linear_k = malloc(sizeof(double) * linearCount);
    for (k = 0; k < speciesCount; ++k) {
      s = [species objectAtIndex: k];
      for (l = 0; l < linearCount; ++l) {
        id a = [li objectAtIndex: l];
        if ([s isEqualToString: [a objectAtIndex: 0]]) ++linearMinusCount[k];
        if ([s isEqualToString: [a objectAtIndex: 1]]) ++linearPlusCount[k];
      }
    }
    for (k = 0; k < speciesCount; ++k) {
      if (linearPlusCount[k]) linearPlus[k] = malloc(sizeof(int) * linearPlusCount[k]);
      else linearPlus[k] = NULL;
      if (linearMinusCount[k]) linearMinus[k] = malloc(sizeof(int) * linearMinusCount[k]);
      else linearMinus[k] = NULL;
    }
    for (k = 0; k < speciesCount; ++k) {
      s = [species objectAtIndex: k];
      int mp = 0, pp = 0;
      for (l = 0; l < linearCount; ++l) {
        id a = [li objectAtIndex: l];
        if ([s isEqualToString: [a objectAtIndex: 0]]) linearMinus[k][mp++] = l;
        if ([s isEqualToString: [a objectAtIndex: 1]]) linearPlus[k][pp++] = l;
      }
    }
    for (l = 0; l < linearCount; ++l) {
      id a = [li objectAtIndex: l];
      id pName = [NSString stringWithFormat: @"%@_linear_%@", [a objectAtIndex: 0], [a objectAtIndex: 1]];
      NSString *param = [aModel objectForKey: pName];
      CHECK_PARAM(param, pName)
      linear_k[l] = [param doubleValue];
    }
  }
  
#if 0
  speciesDecay = malloc(sizeof(double) * speciesCount);
  for (k = 0; k < speciesCount; ++k) {
    s = [species objectAtIndex: k];
    NSString *param = [aModel objectForKey: [NSString stringWithFormat: @"%@_decay", s]];
    speciesDecay[k] = [param doubleValue];
  }
#endif
  
  // finite difference method
  param = [aModel objectForKey: @"dt"];
  CHECK_PARAM(param, @"dt")
  dt = [param doubleValue];
  if (OUTPUT) printf("dt = %lf\n", dt);
  
  // How long to simulate
  timeSteps = 0;
  runToEpsilon = NO;
  epsilon = 0;
  withinEpsilon = malloc(sizeof(BOOL) * speciesCount);
  epsilonCheck = 1 / dt;
  s = [aModel objectForKey: @"runToEpsilon"];
  if ([s boolValue]) {
    runToEpsilon = YES;
    s = [aModel objectForKey: @"epsilon"];
    epsilon = [s doubleValue];
    for (i = 0; i < speciesCount; ++i) withinEpsilon[i] = NO;
    s = [aModel objectForKey: @"epsilonCheck"];
    epsilonCheck = [s intValue] / dt;
    if (OUTPUT) printf("runToEpsilon = %lf check %d\n", epsilon, epsilonCheck);
  } else epsilon = EPS;
  s = [aModel objectForKey: @"simulateTime"];
  timeSteps = [s intValue];
  timeSteps = timeSteps / dt;
  if (timeSteps == 0) timeSteps = INT_MAX;
  if (OUTPUT) printf("timeSteps = %d\n", timeSteps);
  
  param = [aModel objectForKey: @"outputFrequency"];
  if (!param) outputFrequency = [modelController outputFrequency];
  else outputFrequency = [param intValue];
  outputFrequency = outputFrequency / dt;
  if (OUTPUT) printf("outputFreq = %d\n", outputFrequency);
  
  param = [aModel objectForKey: @"outputRun"];
  if (!param) outputRun = [modelController outputRun];
  else outputRun = [param intValue];    
  
  return self;
}

- (void)dealloc
{
  int k;
  
  if (dataResults) free(dataResults);
  if (dataDiff) free(dataDiff);
  if (dataCalc) free(dataCalc);
  if (dataF1) free(dataF1);
  if (dataF2) free(dataF2);
  [dataEncode release];
  
  if (dataFiles) {
    for (k = 0; k < speciesCount; ++k)
      if (dataFiles[k] != NULL) fclose(dataFiles[k]);
    free(dataFiles);
  }
  
  for (k = 0; k < speciesCount; ++k) {
    free(interactions[k]);
    free(interactions_pmin[k]);
    free(interactions_pmax[k]);
    free(interactions_c[k]);
    free(interactions_h[k]);
    if (linearPlus[k]) free(linearPlus[k]);
    if (linearMinus[k]) free(linearMinus[k]);
  }
  free(interactions);
  free(interactions_pmin);
  free(interactions_pmax);
  free(interactions_c);
  free(interactions_h);
  //free(speciesDecay);
  free(withinEpsilon);
  free(linear_k);
  free(linearPlus);
  free(linearPlusCount);
  free(linearMinus);
  free(linearMinusCount);
  
  if (paramMin) free(paramMin);
  if (paramMax) free(paramMax);

  //[model release];
  //[modelController release];
  
  [super dealloc];
}

- (void)setupData
{
  switch (encodeTag) {
      case 1:
        dataResults = malloc(sizeof(int) * speciesCount);
        dataDiff = malloc(sizeof(int) * speciesCount);
        break;
      case 2:
        dataResults = malloc(sizeof(long) * speciesCount);
        dataDiff = malloc(sizeof(long) * speciesCount);
        break;
      case 3:
        dataResults = malloc(sizeof(float) * speciesCount);
        dataDiff = malloc(sizeof(float) * speciesCount);
        break;
      case 4:
        dataResults = malloc(sizeof(double) * speciesCount);
        dataDiff = malloc(sizeof(double) * speciesCount);
        break;
  }
  dataCalc = malloc(sizeof(double) * speciesCount);
  dataF1 = malloc(sizeof(double) * speciesCount);
  dataF2 = malloc(sizeof(double) * speciesCount);
}

- (void)initializeSimulation
{
  int k;
  switch (encodeTag) {
      case 1: {
        int *sData = dataResults;
        int *sDiff = dataDiff;
        for (k = 0; k < speciesCount; ++k) {
          sData[k] = 1;
          sDiff[k] = 0;
        }
        break;
      }
      case 2: {
        long *sData = dataResults;
        long *sDiff = dataDiff;
        for (k = 0; k < speciesCount; ++k) {
          sData[k] = 1;
          sDiff[k] = 0;
        }
        break;
      }
      case 3: {
        float *sData = dataResults;
        float *sDiff = dataDiff;
        for (k = 0; k < speciesCount; ++k) {
          sData[k] = 0.1;
          sDiff[k] = 0;
        }
        break;
      }
      case 4: {
        double *sData = dataResults;
        double *sDiff = dataDiff;
        for (k = 0; k < speciesCount; ++k) {
          sData[k] = 0.1;
          sDiff[k] = 0;
        }
        break;
      }
  }
}

- (void)allocateSimulationWithEncode: (NSString *)anEncode
{
  [self allocateSimulationWithEncode: anEncode fileState: 1];
}

- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState
{
  int k;
  
  if ([anEncode isEqual: [Grid2DArray intEncode]]) {
    encodeTag = 1;
  } else if ([anEncode isEqual: [Grid2DArray longEncode]]) {
    encodeTag = 2;
  } else if ([anEncode isEqual: [Grid2DArray floatEncode]]) {
    encodeTag = 3;
  } else if ([anEncode isEqual: [Grid2DArray doubleEncode]]) {
    encodeTag = 4;
  } else {
    // throw exception or something
    NSLog(@"ERROR: ReactionModel unsupported encoding %@\n", anEncode);
    return;
  }
  dataEncode = [anEncode retain];
  
  // setup data
  [self setupData];
  
  // initial conditions
  //[self setupInitialConditions];
  
  // data files
  dataFileState = aState;
  switch (aState) {
    case 0:
      // no data files setup
      break;
    case 1:
      // data files for writing
      dataFiles = malloc(sizeof(FILE *) * speciesCount);
      for (k = 0; k < speciesCount; ++k) {
        id s = [species objectAtIndex: k];
        NSString *fileName = [NSString stringWithFormat: @"%@_%@_%@.dat", name, s, outputSuffix];
        dataFiles[k] = fopen([fileName UTF8String], "wb+");
        if (dataFiles[k] == NULL) {
          NSLog(@"Cannot open file: %@", fileName);
          exit(0);
        }
      }
      break;
    case 2:
      // data files for reading
      break;
  }
}

- (void)writeData
{
  [self writeDataWithCheck: YES];
}

- (void)writeDataWithCheck: (BOOL)aFlag
{
  int k;

  // write only if data files setup for write
  if (dataFileState != 1) return;

  switch (encodeTag) {
      case 1: {
        int *data = dataResults;
        for (k = 0; k < speciesCount; ++k) {
          fwrite(&(data[k]), sizeof(int), 1, dataFiles[k]);
        }
        break;
      }
      case 2: {
        long *data = dataResults;
        for (k = 0; k < speciesCount; ++k) {
          fwrite(&(data[k]), sizeof(long), 1, dataFiles[k]);
        }
        break;
      }
      case 3: {
        float *data = dataResults;
        for (k = 0; k < speciesCount; ++k) {
          fwrite(&(data[k]), sizeof(float), 1, dataFiles[k]);
        }
        break;
      }
      case 4: {
        double *data = dataResults;
        for (k = 0; k < speciesCount; ++k) {
          fwrite(&(data[k]), sizeof(double), 1, dataFiles[k]);
        }
        break;
      }
  }
}

- (BOOL)checkEpsilon
{
  //double maxDiff = 0.0;
  BOOL done = YES;
  
  int k;
  switch (encodeTag) {
      case 1: {
        int *sData = dataResults;
        int *sDiff = dataDiff;
        for (k = 0; k < speciesCount; ++k) {
          if (fabs(sData[k] - sDiff[k]) >= epsilon) done = NO;
          sDiff[k] = sData[k];
        }
        break;
      }
      case 2: {
        long *sData = dataResults;
        long *sDiff = dataDiff;
        for (k = 0; k < speciesCount; ++k) {
          if (fabs(sData[k] - sDiff[k]) >= epsilon) done = NO;
          sDiff[k] = sData[k];
        }
        break;
      }
      case 3: {
        float *sData = dataResults;
        float *sDiff = dataDiff;
        for (k = 0; k < speciesCount; ++k) {
          if (fabs(sData[k] - sDiff[k]) >= epsilon) done = NO;
          sDiff[k] = sData[k];
        }
        break;
      }
      case 4: {
        double *sData = dataResults;
        double *sDiff = dataDiff;
        for (k = 0; k < speciesCount; ++k) {
          if (fabs(sData[k] - sDiff[k]) >= epsilon) done = NO;
          sDiff[k] = sData[k];
        }
        break;
      }
  }
  
  return done;
}

- (void)runSimulation
{
  int k, l;
  
  printf("time = 1\n");
  [self writeData];
  
  switch (encodeTag) {
      case 1: {
        int *sData = dataResults;
        for (k = 0; k < speciesCount; ++k) {
          dataCalc[k] = sData[k];
        }
        break;
      }
      case 2: {
        long *sData = dataResults;
        for (k = 0; k < speciesCount; ++k) {
          dataCalc[k] = sData[k];
        }
        break;
      }
      case 3: {
        float *sData = dataResults;
        for (k = 0; k < speciesCount; ++k) {
          dataCalc[k] = sData[k];
        }
        break;
      }
      case 4: {
        double *sData = dataResults;
        for (k = 0; k < speciesCount; ++k) {
          dataCalc[k] = sData[k];
        }
        break;
      }
      default:
        return;
  }
  
  
  // run it
  BOOL done = NO;
  int t = 0;
  while (!done) {
    
    // are we done?
    ++t;
    if (runToEpsilon) {
      if ((t % epsilonCheck) == 0) {
        if ([self checkEpsilon]) break;
      }
    } else {
      if (t >= timeSteps) break;
    }
    
    // x(t + dt) = x(t) + F2
    // where
    // F1 = x + .5 * dt * f(t, x)
    // F2 = dt * f(t + .5*dt, F1)
    
    // calculate F1
    for (k = 0; k < speciesCount; ++k) {
      dataF1[k] = 0.0;
      
      // for each Hill interaction
      double interactionValue = 1.0;
      if (interactionsCount[k] == 0) interactionValue = 0.0;
      for (l = 0; l < interactionsCount[k]; ++l) {
        double iGrid = dataCalc[interactions[k][l]];
        double s_pmin = interactions_pmin[k][l];
        double s_pmax = interactions_pmax[k][l];
        double s_c = interactions_c[k][l];
        double s_h = interactions_h[k][l];
        
        interactionValue *= HILL(iGrid, s_pmin, s_pmax, s_c, s_h);
      }
      
      // linear interactions
      double linearValue = 0.0;
      for (l = 0; l < linearPlusCount[k]; ++l) {
        linearValue += linear_k[linearPlus[k][l]] * dataCalc[k];
      }
      for (l = 0; l < linearMinusCount[k]; ++l) {
        linearValue -= linear_k[linearMinus[k][l]] * dataCalc[k];
      }
      
      dataF1[k] = dataCalc[k] + 0.5 * dt * (interactionValue + linearValue);
      //dataF1[k] = dataCalc[k] + 0.5 * dt * (interactionValue - speciesDecay[k] * dataCalc[k]);
      //if (t < 50) printf("F1: %lf %lf %lf\n", dataCalc[k], dataF1[k], interactionValue);
      if (dataF1[k] < 0.0) dataF1[k] = 0;
    }
    //printf("%lf\n", tt);
    
    // calculate F2
    for (k = 0; k < speciesCount; ++k) {
      dataF2[k] = 0.0;
      
      // for each interaction
      double interactionValue = 1.0;
      if (interactionsCount[k] == 0) interactionValue = 0.0;
      for (l = 0; l < interactionsCount[k]; ++l) {
        double iF1 = dataF1[interactions[k][l]];
        double s_pmin = interactions_pmin[k][l];
        double s_pmax = interactions_pmax[k][l];
        double s_c = interactions_c[k][l];
        double s_h = interactions_h[k][l];
        
        interactionValue *= HILL(iF1, s_pmin, s_pmax, s_c, s_h);
      }
      
      // linear interactions
      double linearValue = 0.0;
      for (l = 0; l < linearPlusCount[k]; ++l) {
        linearValue += linear_k[linearPlus[k][l]] * dataF1[k];
      }
      for (l = 0; l < linearMinusCount[k]; ++l) {
        linearValue -= linear_k[linearMinus[k][l]] * dataF1[k];
      }
      
      dataF2[k] = dataCalc[k] + dt * (interactionValue + linearValue);
      //dataF2[k] = dataCalc[k] + dt * (interactionValue - speciesDecay[k] * dataF1[k]);
      //if (t < 50) printf("F2: %lf %lf %lf %lf\n", dataCalc[k], dataF2[k], dataF1[k], interactionValue);
      if (dataF2[k] < 0.0) dataF2[k] = 0;
    }
    
    // advance to next time
    switch (encodeTag) {
        case 1: {
          int *sData = dataResults;
          for (k = 0; k < speciesCount; ++k) {
            sData[k] = dataF2[k];
            dataCalc[k] = sData[k];
          }
          break;
        }
        case 2: {
          long *sData = dataResults;
          for (k = 0; k < speciesCount; ++k) {
            sData[k] = dataF2[k];
            dataCalc[k] = sData[k];
          }
          break;
        }
        case 3: {
          float *sData = dataResults;
          for (k = 0; k < speciesCount; ++k) {
            sData[k] = dataF2[k];
            dataCalc[k] = sData[k];
          }
          break;
        }
        case 4: {
          double *sData = dataResults;
          for (k = 0; k < speciesCount; ++k) {
            sData[k] = dataF2[k];
            dataCalc[k] = sData[k];
          }
          break;
        }
    }
    
    if ((t % outputFrequency) == 0) {
      printf("time = %d\n", t);
      [self writeData];
      double *sData = dataResults;
      for (k = 0; k < speciesCount; ++k) {
        id s = [species objectAtIndex: k];
        printf("%s value = %lf\n", [s UTF8String], sData[k]);
      }
    }
    
#if 0
    if (blowup) {
      printf("blowup\n");
      // if blowup then set all grids to threshold, no pattern
      for (k = 0; k < speciesCount; ++k) {
        double (*sGrid)[gridSize][gridSize];
        sGrid = speciesGrid[k];
        
        for (i = 0; i < gridSize; ++i)
          for (j = 0; j < gridSize; ++j) {
            (*sGrid)[i][j] = BLOWUP_THRESHOLD;
          }
      }
      break;
    }
#endif
  }
  
  // write out final data
  printf("time = %d\n", t);
  [self writeData];
  for (k = 0; k < speciesCount; ++k) {
    double *sData = dataResults;
    id s = [species objectAtIndex: k];
    fclose(dataFiles[k]);
    dataFiles[k] = NULL;
    printf("%s value = %lf\n", [s UTF8String], sData[k]);
  }
  
}

- (int)numOfSpecies { return [species count]; }
- (NSString *)nameOfSpecies: (int)theSpecies { return [species objectAtIndex: theSpecies]; }
- (double)dt { return dt; }
- (int)timeSteps { return timeSteps; }
- (double)epsilon { return epsilon; }
- (NSString *)dataEncode { return dataEncode; }
- (void *)dataResults { return dataResults; }
- (FILE **)dataFiles { return dataFiles; }

- (int)numOfParameters
{
  int numParams = 0;
  int k, l;
  
  for (k = 0; k < speciesCount; ++k) {
    // each hill function
    for (l = 0; l < interactionsCount[k]; ++l) numParams += 4;
  }
  // linear interactions
  numParams += linearCount;
  
  return numParams;
}

- (NSString *)nameOfParameter: (int)theParam
{
  NSString *ps = nil;
  
  // Hill interactions
  int k, l, p = 0;
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    
    for (l = 0; l < interactionsCount[k]; ++l) {
      id is = [species objectAtIndex: interactions[k][l]];
      
      if (theParam == p) ps = [NSString stringWithFormat: @"%@_pmin_%@", s, is];
      ++p;
      
      if (theParam == p) ps = [NSString stringWithFormat: @"%@_pmax_%@", s, is];
      ++p;
      
      if (theParam == p) ps = [NSString stringWithFormat: @"%@_c_%@", s, is];
      ++p;
      
      if (theParam == p) ps = [NSString stringWithFormat: @"%@_h_%@", s, is];
      ++p;
    }
  }
  
  // linear interactions
  id li = [model objectForKey: @"linearInteractions"];
  for (k = 0; k < linearCount; ++k) {
    id a = [li objectAtIndex: k];
    if (theParam == p) ps = [NSString stringWithFormat: @"%@_linear_%@", [a objectAtIndex: 0], [a objectAtIndex: 1]];
    ++p;
  }

  return ps;
}

- (BOOL)setupParameterRanges: (NSDictionary *)ranges
{
  id paramRanges;

  if (!ranges) paramRanges = model;
  else paramRanges = ranges;
    
  // how many parameters
  int i, numParams = [self numOfParameters];

  // Specify which parameters to randomize?                                                                                                                
  id randomParameters = [paramRanges objectForKey: @"randomParameters"];
  if ([randomParameters count] == 0) randomParameters = nil;
  
  // Specify which parameters not to randomize?                                                                                                            
  id notRandomParameters = [paramRanges objectForKey: @"notRandomParameters"];
  if ([notRandomParameters count] == 0) notRandomParameters = nil;

  if (paramMin) free(paramMin);
  if (paramMax) free(paramMax);
  paramMin = malloc(sizeof(double) * numParams);
  paramMax = malloc(sizeof(double) * numParams);
  BOOL flag = YES;
  for (i = 0; i < numParams; ++i) {
    NSString *s = [self nameOfParameter: i];

    if (randomParameters && ([randomParameters indexOfObject: s] == NSNotFound)) continue;
    if (notRandomParameters && ([notRandomParameters indexOfObject: s] != NSNotFound)) continue;

    NSString *paramName = [NSString stringWithFormat: @"%@_min", s];
    id param = [paramRanges objectForKey: paramName];
    CHECK_PARAMF(param, paramName, flag);
    paramMin[i] = [param doubleValue];

    paramName = [NSString stringWithFormat: @"%@_max", s];
    param = [paramRanges objectForKey: paramName];
    CHECK_PARAMF(param, paramName, flag);
    paramMax[i] = [param doubleValue];
  }

  return flag;
}

- (double *)parameterMinRange { return paramMin; }
- (double *)parameterMaxRange { return paramMax; }

static int determine_scale(double value)
{
  double v;
  int scale = 0;
  
  if (value < 0) v = -value;
  else v = value;
  
  if (v == 0.0) return scale;

  if (v >= 10) {
    while (v >= 10) {
      v = v / 10;
      ++scale;
    }
  } else {
    while (v < 1) {
      v = v * 10;
      --scale;
    }
  }
  
  //NSLog(@"%lf %d\n", value, scale);
  return scale;
}

- (double)randomValueforParameter: (int)aParam
{
  if ((aParam < 0) || (aParam >= [self numOfParameters])) {
    NSLog(@"ERROR: Invalid parameter index: %d", aParam);
    return 0.0;
  }
  
  // check if parameter ranges are setup
  if (!paramMin)
    if (![self setupParameterRanges: model]) {
      NSLog(@"ERROR: Could not setup parameter ranges");
      return 0.0;
    }

  // determine scales of range
  int minScale = determine_scale(paramMin[aParam]);
  int maxScale = determine_scale(paramMax[aParam]);

  int signCheck = 0;
  if ((paramMin[aParam] < 0) && (paramMax[aParam] < 0)) signCheck = 1;
  if (((paramMin[aParam] < 0) && (paramMax[aParam] >= 0))
      || ((paramMin[aParam] >= 0) && (paramMax[aParam] < 0))) signCheck = 2;
  
  double newValue = 0.0;
  if (minScale == maxScale) {
    double delta = paramMax[aParam] - paramMin[aParam];
    newValue = RAND_NUM * delta + paramMin[aParam];
  } else {
    do {
      double deltaScale = maxScale - minScale;
      double newScale = RAND_NUM * deltaScale + minScale;
      newScale = round(newScale);
      //printf("newScale: %lf %d %d %lf\n", deltaScale, minScale, maxScale, newScale);
      //double minVal = paramMin[aParam] / pow(10.0, minScale);
      //double maxVal = paramMax[aParam] / pow(10.0, maxScale);
      //double delta = maxVal - minVal;
      //newValue = RAND_NUM * delta + minVal;
      newValue = RAND_NUM;
      if (signCheck == 1) newValue = -newValue;
      if ((signCheck == 2) && (RAND_NUM < 0.5)) newValue = -newValue;
      //printf("newVal: %lf %lf %lf %lf\n", delta, minVal, maxVal, newValue);
      newValue = newValue * pow(10.0, newScale);
    } while ((newValue < paramMin[aParam]) || (newValue > paramMax[aParam]));
  }
  
  return newValue;
}

- (double)perturbValueForParameter: (int)aParam withinRange: (BOOL)aFlag standardDeviation: (double)SD
{
  if ((aParam < 0) || (aParam >= [self numOfParameters])) {
    NSLog(@"ERROR: Invalid parameter index: %d", aParam);
    return 0.0;
  }
  
  double origValue = [[model objectForKey: [self nameOfParameter: aParam]] doubleValue];
  //NSLog(@"origValue = %lf", origValue);
  double newValue = origValue;

  if (aFlag) {
    // check if parameter ranges are setup
    if (!paramMin)
      if (![self setupParameterRanges: model]) {
        NSLog(@"ERROR: Could not setup parameter ranges");
        return 0.0;
      }
  
    do {
      newValue = origValue + normal_dist_rand(0, SD);
    } while ((newValue < paramMin[aParam]) || (newValue > paramMax[aParam]));
  } else {
    // don't worry about range, just keep same sign
    BOOL isPositive = YES;
    if (origValue < 0) isPositive = NO;

    do {
      newValue = origValue + normal_dist_rand(0, SD);
    } while (((newValue < 0) && isPositive) || ((newValue >= 0) && !isPositive));
  }
  
  return newValue;
}

- (int)modifyRandomParameter: (NSMutableDictionary *)params
{
  // check if parameter ranges are setup
  if (!paramMin)
    if (![self setupParameterRanges: model]) {
      NSLog(@"ERROR: Could not setup parameter ranges");
      return -1;
    }
  
  // how many parameters
  int numParams = [self numOfParameters];
  
  int rp = (int)(RAND_NUM * numParams);
  NSString *ps = [self nameOfParameter: rp];
  
  if (!ps) NSLog(@"ERROR: Could not randomly pick parameter: %d %d", numParams, rp);
  else {
    double delta = paramMax[rp] - paramMin[rp];
    double pv = [[params objectForKey: ps] doubleValue];
    double newpv = RAND_NUM * delta + paramMin[rp];
    
#if 0
    double scale = determine_scale(pv);
    BOOL done = NO;
    while (!done) {
      newpv = normal_dist_rand(pv, scale);
      // maintain the same sign
      if ((pv < 0) && (newpv < 0)) done = YES;
      if ((pv >= 0) && (newpv >= 0)) done = YES;
    }
#endif
#if 0
    if (fabs(pv) < 0.001) {
      if (pv >= 0) newpv = RAND_NUM;
      else newpv = - RAND_NUM;
    } else {
      if (RAND_NUM < 0.5) newpv = pv + (RAND_NUM * pv);
      else newpv = pv - (RAND_NUM * pv);
    }
#endif
    
    [params setObject: [NSNumber numberWithDouble: newpv] forKey: ps];
    NSLog(@"change parameter %@(%d): %lf -> %lf", ps, rp, pv, newpv);
  }
  
  return rp;
}

- (void)cleanupSimulation
{
}

@end


//
// Produce GPU code
//
@implementation ReactionModel (GPU)

- (BOOL)isGPUStatic { return isGPUStatic; }
- (void)setGPUStatic: (BOOL)aFlag { isGPUStatic = aFlag; }

- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName
{
  if (isGPUStatic) return [self controllerStaticGPUCode: prefixName];
  else return [self controllerDynamicGPUCode: prefixName];
}

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  if (isGPUStatic) return [self definitionsStaticGPUCode: prefixName];
  else return [self definitionsDynamicGPUCode: prefixName];
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  if (isGPUStatic) return [self kernelStaticGPUCode: prefixName];
  else return [self kernelDynamicGPUCode: prefixName];
}

//
// The standard GPU code is for small-ish models where the complete calculation
// can be done in a single kernel.  The 2nd-order Runge-Kutta code is explicitly
// generated which provides maximumal performance. Threads are then used to run
// many kernels with different parameters settings.
//
- (NSMutableDictionary *)controllerStaticGPUCode: (NSString *)prefixName
{
  //int k;
  //int numParams = [self numOfParameters];
  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;
  
  //
  // Headers and defines
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];

  //
  // Allocation
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"*%@_allocGPUKernel(void *model, int numOfODEs, int numParams, int speciesCount, float dt, float EPS)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"allocation function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" RModelRK2_GPUptrs *ptrs = (RModelRK2_GPUptrs *)malloc(sizeof(RModelRK2_GPUptrs));\n", prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n // Save parameters\n"];
  [GPUCode appendString: @" ptrs->numOfODEs = numOfODEs;\n"];
  [GPUCode appendString: @" ptrs->numParams = numParams;\n"];
  [GPUCode appendString: @" ptrs->dt = dt;\n"];
  [GPUCode appendString: @" ptrs->EPS = EPS;\n"];
  [GPUCode appendString: @" ptrs->localEpsilonCheck = (int *)malloc(numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" ptrs->speciesCount = speciesCount;\n"];
  
  [GPUCode appendString: @"\n // Allocate device memory\n"];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->epsilonCheck), numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData), &(ptrs->speciesPitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesF1), &(ptrs->speciesPitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesF2), &(ptrs->speciesPitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->parameters), &(ptrs->paramPitch), numOfODEs * sizeof(float), numParams);\n"];
  
  [GPUCode appendString: @" return ptrs;\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"allocation code"];

  //
  // Data Transfer
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *p, int aFlag, float *hostParameters, float *hostData)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" RModelRK2_GPUptrs *ptrs = (RModelRK2_GPUptrs *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @"\n if (aFlag) {\n"];
  [GPUCode appendString: @"    // Copy host memory to device memory\n"];
  [GPUCode appendFormat: @"    cudaMemcpyToSymbol(%@_parameters, hostParameters, ptrs->numParams * sizeof(float), 0, cudaMemcpyHostToDevice);\n", prefixName];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->speciesData, ptrs->speciesPitch, (const cudaArray *)hostData, ptrs->numOfODEs * sizeof(float), ptrs->numOfODEs * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->parameters, ptrs->paramPitch, (const cudaArray *)hostParameters, ptrs->numOfODEs * sizeof(float), ptrs->numOfODEs * sizeof(float), ptrs->numParams, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemset(ptrs->epsilonCheck, 0, ptrs->numOfODEs * sizeof(int));\n"];
  
  [GPUCode appendString: @" } else {\n"];

  [GPUCode appendString: @"    // Copy result to host memory\n"];
  [GPUCode appendString: @"    cudaMemcpy(ptrs->localEpsilonCheck, ptrs->epsilonCheck, ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(hostData, ptrs->numOfODEs * sizeof(float), ptrs->speciesData, ptrs->speciesPitch, ptrs->numOfODEs * sizeof(float), ptrs->speciesCount, cudaMemcpyDeviceToHost);\n"];
  
  [GPUCode appendString: @" }\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];

  //
  // Execution
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, float *hostData, int timeSteps)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" RModelRK2_GPUptrs *%@_ptrs = (RModelRK2_GPUptrs *)%@_data;\n", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
 
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" int %@_blocksPerGrid = 1;\n", prefixName];
  [GPUCode appendFormat: @" int %@_threadsPerBlock = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_ptrs->numOfODEs < 256) {\n", prefixName];
  [GPUCode appendFormat: @"   %@_threadsPerBlock = %@_ptrs->numOfODEs;\n", prefixName, prefixName];
  [GPUCode appendFormat: @"   %@_blocksPerGrid = 1;\n", prefixName];
  [GPUCode appendString: @" } else {\n"];
  [GPUCode appendFormat: @"   %@_threadsPerBlock = 256;\n", prefixName];
  [GPUCode appendFormat: @"   %@_blocksPerGrid = (%@_ptrs->numOfODEs + %@_threadsPerBlock - 1) / %@_threadsPerBlock;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @" }\n"];
  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];

  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"   %@_kernel_F1<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(%@_ptrs->numOfODEs, %@_ptrs->speciesPitch/sizeof(float), %@_ptrs->speciesData, %@_ptrs->speciesF1, %@_ptrs->speciesF2, %@_ptrs->dt);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
  [GPUCode appendFormat: @"   %@_kernel_F2<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(%@_ptrs->numOfODEs, %@_ptrs->speciesPitch/sizeof(float), %@_ptrs->speciesData, %@_ptrs->speciesF1, %@_ptrs->speciesF2, %@_ptrs->dt);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @" cudaPrintfDisplay(stdout, true);\n"];
  [GPUCode appendFormat: @"   cudaMemcpy2D(%@_ptrs->speciesData, %@_ptrs->speciesPitch, %@_ptrs->speciesF2, %@_ptrs->speciesPitch, %@_ptrs->numOfODEs * sizeof(float), %@_ptrs->speciesCount, cudaMemcpyDeviceToDevice);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];

  //
  // Release
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_releaseGPUKernel(void *model, void *p)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"release function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendString: @" RModelRK2_GPUptrs *ptrs = (RModelRK2_GPUptrs *)p;\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesF1);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesF2);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->parameters);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->epsilonCheck);\n"];
  [GPUCode appendString: @" free(ptrs->localEpsilonCheck);\n"];
  [GPUCode appendString: @" free(ptrs);\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"release code"];

  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"RModelGPUFunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];
  
  return GPUDictionary;    
}

- (NSMutableString *)definitionsStaticGPUCode: (NSString *)prefixName
{
  int k, l;
  int numParams = [self numOfParameters];

  NSMutableString *GPUCode = [NSMutableString new];

  [GPUCode appendString: @"\n// parameters\n"];
  [GPUCode appendFormat: @"__constant__ float %@_parameters[%d];", prefixName, numParams];
  
  int p = 0;
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n// %@\n", s];        
    //[GPUCode appendFormat: @"#define %@_decay p[%d*speciesPitch+i]\n", s, p];
    //++p;
    
    for (l = 0; l < interactionsCount[k]; ++l) {
      id is = [species objectAtIndex: interactions[k][l]];
      
      [GPUCode appendFormat: @"// Hill parameters, %@\n", is];
      
      [GPUCode appendFormat: @"#define %@_pmin_%@ %@_parameters[%d]\n", s, is, prefixName, p];
      ++p;
      
      [GPUCode appendFormat: @"#define %@_pmax_%@ %@_parameters[%d]\n", s, is, prefixName, p];
      ++p;
      
      [GPUCode appendFormat: @"#define %@_c_%@ %@_parameters[%d]\n", s, is, prefixName, p];
      ++p;
      
      [GPUCode appendFormat: @"#define %@_h_%@ %@_parameters[%d]\n", s, is, prefixName, p];
      ++p;
    }
  }
  // linear interactions
  [GPUCode appendString: @"\n// Linear parameters\n"];
  id li = [model objectForKey: @"linearInteractions"];
  for (k = 0; k < linearCount; ++k) {
    id a = [li objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_linear_%@ %@_parameters[%d]\n", [a objectAtIndex: 0], [a objectAtIndex: 1], prefixName, p];
    ++p;
  }
  
  // species index
  [GPUCode appendString: @"\n// Species index\n"];
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_species %d\n", s, k];
  }

  // variable definitions (to avoid running out of registers for large models)
  [GPUCode appendString: @"\n// Variable definitions\n"];
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_val speciesData[%@_species*pitch+ODEnum]\n", s, s, k];
    [GPUCode appendFormat: @"#define %@_F1val speciesData_F1[%@_species*pitch+ODEnum]\n", s, s, k];
  }
    
  return GPUCode;
}

- (NSMutableString *)kernelStaticGPUCode: (NSString *)prefixName
{
  int k, l;
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  id li = [model objectForKey: @"linearInteractions"];

  // 2nd order Runge-Kutta
  
  [GPUCode appendString: @"\n// Kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F1(int numOfODEs, int pitch, float *speciesData, float *speciesData_F1, float *speciesData_F2, float dt)", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   float interactionValue, linearValue, F1_val;\n"];
  
  [GPUCode appendString: @"\n   if (ODEnum >= numOfODEs) return;\n"];
    
  // Calculate F1
  [GPUCode appendString: @"\n   // calculate F1\n"];
  
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n   // %@\n", s];
    
    if (interactionsCount[k] == 0) [GPUCode appendString: @"   interactionValue = 0.0;\n"];
    else [GPUCode appendString: @"   interactionValue = 1.0;\n"];
    for (l = 0; l < interactionsCount[k]; ++l) {
      id is = [species objectAtIndex: interactions[k][l]];
      [GPUCode appendFormat: @"   interactionValue *= HILL(%@_val, %@_pmin_%@, %@_pmax_%@, %@_c_%@, %@_h_%@);\n", is, s, is, s, is, s, is, s, is];
    }
    
    [GPUCode appendString: @"   linearValue = 0.0;\n"];
    if (li) {
      for (l = 0; l < linearPlusCount[k]; ++l) {
        id a = [li objectAtIndex: linearPlus[k][l]];
        [GPUCode appendFormat: @"   linearValue += %@_linear_%@ * %@_val;\n", [a objectAtIndex: 0], [a objectAtIndex: 1], s];
      }
      for (l = 0; l < linearMinusCount[k]; ++l) {
        id a = [li objectAtIndex: linearMinus[k][l]];
        [GPUCode appendFormat: @"   linearValue -= %@_linear_%@ * %@_val;\n", [a objectAtIndex: 0], [a objectAtIndex: 1], s];
      }
    }
    
    [GPUCode appendFormat: @"   F1_val = %@_val + 0.5f * dt * (interactionValue + linearValue);\n", s];
    [GPUCode appendString: @"   if (F1_val < 0) F1_val = 0;\n"];
    [GPUCode appendFormat: @"   speciesData_F1[%@_species*pitch+ODEnum] = F1_val;\n", s];
  }

  [GPUCode appendString: @"}\n"];

  [GPUCode appendString: @"\n// Kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F2(int numOfODEs, int pitch, float *speciesData, float *speciesData_F1, float *speciesData_F2, float dt)", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   float interactionValue, linearValue, F2_val;\n"];
  
  [GPUCode appendString: @"\n   if (ODEnum >= numOfODEs) return;\n"];
  
  // Calculate F2
  [GPUCode appendString: @"\n   // calculate F2\n"];
  
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n   // %@\n", s];
    
    if (interactionsCount[k] == 0) [GPUCode appendString: @"   interactionValue = 0.0;\n"];
    else [GPUCode appendString: @"   interactionValue = 1.0;\n"];
    for (l = 0; l < interactionsCount[k]; ++l) {
      id is = [species objectAtIndex: interactions[k][l]];
      [GPUCode appendFormat: @"   interactionValue *= HILL(%@_F1val, %@_pmin_%@, %@_pmax_%@, %@_c_%@, %@_h_%@);\n", is, s, is, s, is, s, is, s, is];
    }
    
    [GPUCode appendString: @"   linearValue = 0.0;\n"];
    if (li) {
      for (l = 0; l < linearPlusCount[k]; ++l) {
        id a = [li objectAtIndex: linearPlus[k][l]];
        [GPUCode appendFormat: @"   linearValue += %@_linear_%@ * %@_F1val;\n", [a objectAtIndex: 0], [a objectAtIndex: 1], s];
      }
      for (l = 0; l < linearMinusCount[k]; ++l) {
        id a = [li objectAtIndex: linearMinus[k][l]];
        [GPUCode appendFormat: @"   linearValue -= %@_linear_%@ * %@_F1val;\n", [a objectAtIndex: 0], [a objectAtIndex: 1], s];
      }
    }
    
    [GPUCode appendFormat: @"   F2_val = %@_val + dt * (interactionValue + linearValue);\n", s];
    [GPUCode appendString: @"   if (F2_val < 0) F2_val = 0;\n"];
    [GPUCode appendFormat: @"   speciesData_F2[%@_species*pitch+ODEnum] = F2_val;\n", s];
  }
  
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;
}

//
// Large-ish models cannot be calculated in a single kernel, versions (e.g. 2.1) of
// the CUDA compiler has difficulty optimizing such a large kernel and typically run
// out of registers.  Instead, we have a generic kernel which iterates through a
// connection matrix for calculation and each thread performs the calculation
// for an individual species.
//
// This also supports programs that wish to dynamically alter interaction structure
//

- (NSMutableDictionary *)controllerDynamicGPUCode: (NSString *)prefixName
{
  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;
  
  //
  // Headers and defines
  //

  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];
  
  //
  // Allocation
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"*%@_allocGPUKernel(void *model, int numOfODEs, int speciesCount, int linearCount, float dt, float EPS)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"allocation function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUptrs *ptrs = (%@_GPUptrs *)malloc(sizeof(%@_GPUptrs));\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendString: @"\n // Save parameters\n"];
  [GPUCode appendString: @" ptrs->numOfODEs = numOfODEs;\n"];
  [GPUCode appendString: @" ptrs->dt = dt;\n"];
  [GPUCode appendString: @" ptrs->EPS = EPS;\n"];
  [GPUCode appendString: @" ptrs->localEpsilonCheck = (int *)malloc(numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" ptrs->speciesCount = speciesCount;\n"];
  [GPUCode appendString: @" ptrs->linearCount = linearCount;\n"];
  
  // Allocate device memory
  [GPUCode appendString: @"\n // Allocate device memory\n"];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->epsilonCheck), numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData), &(ptrs->speciesPitch), speciesCount * sizeof(float), numOfODEs);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesF1), &(ptrs->speciesPitch), speciesCount * sizeof(float), numOfODEs);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesF2), &(ptrs->speciesPitch), speciesCount * sizeof(float), numOfODEs);\n"];

  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesConnection), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->pminParam), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->pmaxParam), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->cParam), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->hParam), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->linearParam), linearCount * sizeof(float));\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->linearPlus), &(ptrs->linearPitch), speciesCount * sizeof(int), linearCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->linearPlusConnection), &(ptrs->linearPitch), speciesCount * sizeof(int), linearCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->linearMinus), &(ptrs->linearPitch), speciesCount * sizeof(int), linearCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->linearMinusConnection), &(ptrs->linearPitch), speciesCount * sizeof(int), linearCount);\n"];
  
  [GPUCode appendString: @" return ptrs;\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"allocation code"];

  //
  // Data Transfer
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *p, int aFlag, RMDynamic_data *data)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUptrs *ptrs = (%@_GPUptrs *)p;\n", prefixName, prefixName];
  
  [GPUCode appendString: @"\n if (aFlag) {\n"];
  [GPUCode appendString: @"\n    // Copy host memory to device memory\n"];
  [GPUCode appendFormat: @"    cudaMemcpyToSymbol(%@_struct, p, sizeof(%@_GPUptrs), 0, cudaMemcpyHostToDevice);\n", prefixName, prefixName];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->speciesData, ptrs->speciesPitch, (const cudaArray *)data->speciesData, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->numOfODEs, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemset(ptrs->epsilonCheck, 0, ptrs->numOfODEs * sizeof(int));\n"];

  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->speciesConnection, ptrs->connectionPitch, (const cudaArray *)data->speciesConnection, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->pminParam, ptrs->connectionPitch, (const cudaArray *)data->pminParam, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->pmaxParam, ptrs->connectionPitch, (const cudaArray *)data->pmaxParam, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->cParam, ptrs->connectionPitch, (const cudaArray *)data->cParam, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->hParam, ptrs->connectionPitch, (const cudaArray *)data->hParam, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  
  [GPUCode appendString: @"    cudaMemcpy(ptrs->linearParam, data->linearParam, ptrs->linearCount * sizeof(float), cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->linearPlus, ptrs->linearPitch, data->linearPlusParam, ptrs->speciesCount * sizeof(int), ptrs->speciesCount * sizeof(int), ptrs->linearCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->linearPlusConnection, ptrs->linearPitch, data->linearPlusConn, ptrs->speciesCount * sizeof(int), ptrs->speciesCount * sizeof(int), ptrs->linearCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->linearMinus, ptrs->linearPitch, data->linearMinusParam, ptrs->speciesCount * sizeof(int), ptrs->speciesCount * sizeof(int), ptrs->linearCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(ptrs->linearMinusConnection, ptrs->linearPitch, data->linearMinusConn, ptrs->speciesCount * sizeof(int), ptrs->speciesCount * sizeof(int), ptrs->linearCount, cudaMemcpyHostToDevice);\n"];
  
  [GPUCode appendString: @" } else {\n"];

  [GPUCode appendString: @"\n    // Copy result to host memory\n"];
  [GPUCode appendString: @"    //cudaMemcpy(ptrs->localEpsilonCheck, ptrs->epsilonCheck, ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n"];
  [GPUCode appendString: @"    cudaMemcpy2D(data->speciesData, ptrs->speciesCount * sizeof(float), ptrs->speciesData, ptrs->speciesPitch, ptrs->speciesCount * sizeof(float), ptrs->numOfODEs, cudaMemcpyDeviceToHost);\n"];

  [GPUCode appendString: @" }\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];
  
  //
  // Execution
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, int timeSteps)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" %@_GPUptrs *ptrs = (%@_GPUptrs *)%@_data;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" %@_GPUptrs *%@_ptrs = (%@_GPUptrs *)%@_data;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" int %@_blocksPerGrid = 1;\n", prefixName];
  [GPUCode appendFormat: @" int %@_threadsPerBlock = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_ptrs->speciesCount < 256) {\n", prefixName];
  [GPUCode appendFormat: @"   %@_threadsPerBlock = %@_ptrs->speciesCount;\n", prefixName, prefixName];
  [GPUCode appendFormat: @"   %@_blocksPerGrid = 1;\n", prefixName];
  [GPUCode appendString: @" } else {\n"];
  [GPUCode appendFormat: @"   %@_threadsPerBlock = 256;\n", prefixName];
  [GPUCode appendFormat: @"   %@_blocksPerGrid = (%@_ptrs->speciesCount + %@_threadsPerBlock - 1) / %@_threadsPerBlock;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @" }\n"];
  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"   %@_kernel_F1<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(ptrs->speciesCount, ptrs->speciesPitch/sizeof(float), ptrs->speciesData, ptrs->speciesF1, ptrs->speciesF2, ptrs->speciesConnection, ptrs->pminParam, ptrs->pmaxParam, ptrs->cParam, ptrs->hParam, ptrs->linearPlus, ptrs->linearPlusConnection, ptrs->linearMinus, ptrs->linearMinusConnection, ptrs->linearParam, ptrs->linearCount, ptrs->dt);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"   %@_kernel_F2<<< %@_blocksPerGrid, %@_threadsPerBlock >>>(ptrs->speciesCount, ptrs->speciesPitch/sizeof(float), ptrs->speciesData, ptrs->speciesF1, ptrs->speciesF2, ptrs->speciesConnection, ptrs->pminParam, ptrs->pmaxParam, ptrs->cParam, ptrs->hParam, ptrs->linearPlus, ptrs->linearPlusConnection, ptrs->linearMinus, ptrs->linearMinusConnection, ptrs->linearParam, ptrs->linearCount, ptrs->dt);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"   cudaMemcpy2D(ptrs->speciesData, ptrs->speciesCount * sizeof(float), ptrs->speciesF2, ptrs->speciesPitch, ptrs->speciesCount * sizeof(float), ptrs->numOfODEs, cudaMemcpyDeviceToDevice);\n"];
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];
  
  //
  // Release
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_releaseGPUKernel(void *model, void *p)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"release function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUptrs *ptrs = (%@_GPUptrs *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesF1);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesF2);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->epsilonCheck);\n"];
  [GPUCode appendString: @" free(ptrs->localEpsilonCheck);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesConnection);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->pminParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->pmaxParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->cParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->hParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearPlus);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearPlusConnection);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearMinus);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearMinusConnection);\n"];
  [GPUCode appendString: @" free(ptrs);\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"release code"];
  
  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"RModelDynamicGPUFunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];
  
  return GPUDictionary;
}

#if 0
- (NSMutableString *)controllerDynamicGPUCode: (NSString *)prefixName
{
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendString: @"#include <stdio.h>\n"];
  [GPUCode appendString: @"#include <unistd.h>\n\n"];
  
  [GPUCode appendString: @"extern \"C\" {\n"];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n\n", prefixName];
  
  // Structure to hold data pointers
  
  [GPUCode appendFormat: @"typedef struct _%@_GPUptrs {\n", prefixName];
  [GPUCode appendString: @"  int numOfODEs;\n"];
  [GPUCode appendString: @"  int numParams;\n"];
  [GPUCode appendString: @"  float dt;\n"];
  [GPUCode appendString: @"  float EPS;\n"];
  [GPUCode appendString: @"  int *localEpsilonCheck;\n"];
  [GPUCode appendString: @"  int *epsilonCheck;\n"];
  [GPUCode appendString: @"  int speciesCount;\n"];
  [GPUCode appendString: @"  float *speciesData;\n"];
  [GPUCode appendString: @"  float *speciesF1;\n"];
  [GPUCode appendString: @"  float *speciesF2;\n"];
  [GPUCode appendString: @"  size_t speciesPitch;\n"];
  [GPUCode appendString: @"  float *speciesConnection;\n"];
  [GPUCode appendString: @"  float *pminParam;\n"];
  [GPUCode appendString: @"  float *pmaxParam;\n"];
  [GPUCode appendString: @"  float *cParam;\n"];
  [GPUCode appendString: @"  float *hParam;\n"];
  [GPUCode appendString: @"  size_t connectionPitch;\n"];
  [GPUCode appendString: @"  int linearCount;\n"];
  [GPUCode appendString: @"  float *linearParam;\n"];
  [GPUCode appendString: @"  int *linearPlus;\n"];
  [GPUCode appendString: @"  int *linearPlusConnection;\n"];
  [GPUCode appendString: @"  int *linearMinus;\n"];
  [GPUCode appendString: @"  int *linearMinusConnection;\n"];
  [GPUCode appendString: @"  size_t linearPitch;\n"];
  [GPUCode appendFormat: @"} %@_GPUptrs;\n", prefixName];
  
  //
  // Allocation
  //
  
  [GPUCode appendString: @"\n// Allocation\n"];
  [GPUCode appendFormat: @"void *%@_allocGPUKernel(void *model, int numOfODEs, int speciesCount, int linearCount, float dt, float EPS)\n{\n", prefixName];
  [GPUCode appendFormat: @" %@_GPUptrs *ptrs = (%@_GPUptrs *)malloc(sizeof(%@_GPUptrs));\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendString: @"\n // Save parameters\n"];
  [GPUCode appendString: @" ptrs->numOfODEs = numOfODEs;\n"];
  [GPUCode appendString: @" ptrs->dt = dt;\n"];
  [GPUCode appendString: @" ptrs->EPS = EPS;\n"];
  [GPUCode appendString: @" ptrs->localEpsilonCheck = (int *)malloc(numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" ptrs->speciesCount = speciesCount;\n"];
  [GPUCode appendString: @" ptrs->linearCount = linearCount;\n"];
  
  // Allocate device memory
  [GPUCode appendString: @"\n // Allocate device memory\n"];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->epsilonCheck), numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData), &(ptrs->speciesPitch), speciesCount * sizeof(float), numOfODEs);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesF1), &(ptrs->speciesPitch), speciesCount * sizeof(float), numOfODEs);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesF2), &(ptrs->speciesPitch), speciesCount * sizeof(float), numOfODEs);\n"];
  
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesConnection), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->pminParam), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->pmaxParam), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->cParam), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->hParam), &(ptrs->connectionPitch), speciesCount * sizeof(float), speciesCount);\n"];
  
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->linearParam), linearCount * sizeof(float));\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->linearPlus), &(ptrs->linearPitch), speciesCount * sizeof(int), linearCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->linearPlusConnection), &(ptrs->linearPitch), speciesCount * sizeof(int), linearCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->linearMinus), &(ptrs->linearPitch), speciesCount * sizeof(int), linearCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->linearMinusConnection), &(ptrs->linearPitch), speciesCount * sizeof(int), linearCount);\n"];
  
  [GPUCode appendString: @" return ptrs;\n"];
  [GPUCode appendString: @"}\n"];
  
  //
  // Initialization
  //
  [GPUCode appendString: @"\n// Initialization\n"];
  [GPUCode appendFormat: @"void %@_initGPUKernel(void *model, void *p, float *hostData, float *speciesConnection, float *pminParam, float *pmaxParam, float *cParam, float *hParam, float *linearParam, int *linearPlusParam, int *linearPlusConn, int *linearMinusParam, int *linearMinusConn)\n{\n", prefixName];
  [GPUCode appendFormat: @" %@_GPUptrs *ptrs = (%@_GPUptrs *)p;\n", prefixName, prefixName];
  
  // Copy host memory to device memory
  [GPUCode appendString: @"\n // Copy host memory to device memory\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->speciesData, ptrs->speciesPitch, (const cudaArray *)hostData, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->numOfODEs, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemset(ptrs->epsilonCheck, 0, ptrs->numOfODEs * sizeof(int));\n"];
  
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->speciesConnection, ptrs->connectionPitch, (const cudaArray *)speciesConnection, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->pminParam, ptrs->connectionPitch, (const cudaArray *)pminParam, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->pmaxParam, ptrs->connectionPitch, (const cudaArray *)pmaxParam, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->cParam, ptrs->connectionPitch, (const cudaArray *)cParam, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->hParam, ptrs->connectionPitch, (const cudaArray *)hParam, ptrs->speciesCount * sizeof(float), ptrs->speciesCount * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  
  [GPUCode appendString: @" cudaMemcpy(ptrs->linearParam, linearParam, ptrs->linearCount * sizeof(float), cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->linearPlus, ptrs->linearPitch, linearPlusParam, ptrs->speciesCount * sizeof(int), ptrs->speciesCount * sizeof(int), ptrs->linearCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->linearPlusConnection, ptrs->linearPitch, linearPlusConn, ptrs->speciesCount * sizeof(int), ptrs->speciesCount * sizeof(int), ptrs->linearCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->linearMinus, ptrs->linearPitch, linearMinusParam, ptrs->speciesCount * sizeof(int), ptrs->speciesCount * sizeof(int), ptrs->linearCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->linearMinusConnection, ptrs->linearPitch, linearMinusConn, ptrs->speciesCount * sizeof(int), ptrs->speciesCount * sizeof(int), ptrs->linearCount, cudaMemcpyHostToDevice);\n"];
  
  [GPUCode appendString: @"}\n"];
  
  //
  // Execution
  //
  [GPUCode appendString: @"\n// Execution\n"];
  [GPUCode appendFormat: @"void %@_invokeGPUKernel(void *model, void *p, float *hostData, int timeSteps)\n{\n", prefixName];
  [GPUCode appendFormat: @" %@_GPUptrs *ptrs = (%@_GPUptrs *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @" int t;\n"];
  [GPUCode appendString: @" int blocksPerGrid = 1;\n"];
  [GPUCode appendString: @" int threadsPerBlock = 1;\n"];
  [GPUCode appendString: @" if (ptrs->speciesCount < 256) {\n"];
  [GPUCode appendString: @"   threadsPerBlock = ptrs->speciesCount;\n"];
  [GPUCode appendString: @"   blocksPerGrid = 1;\n"];
  [GPUCode appendString: @" } else {\n"];
  [GPUCode appendString: @"   threadsPerBlock = 256;\n"];
  [GPUCode appendString: @"   blocksPerGrid = (ptrs->speciesCount + threadsPerBlock - 1) / threadsPerBlock;\n"];
  [GPUCode appendString: @" }\n"];
  
  [GPUCode appendString: @"\n // Invoke kernel\n"];
  [GPUCode appendFormat: @" for (t = 0; t < timeSteps; ++t) {\n"];
  [GPUCode appendFormat: @"   %@_kernel_F1<<< blocksPerGrid, threadsPerBlock >>>(ptrs->speciesCount, ptrs->speciesPitch/sizeof(float), ptrs->speciesData, ptrs->speciesF1, ptrs->speciesF2, ptrs->speciesConnection, ptrs->pminParam, ptrs->pmaxParam, ptrs->cParam, ptrs->hParam, ptrs->linearPlus, ptrs->linearPlusConnection, ptrs->linearMinus, ptrs->linearMinusConnection, ptrs->linearParam, ptrs->linearCount, ptrs->dt);\n", prefixName];
  [GPUCode appendFormat: @"   %@_kernel_F2<<< blocksPerGrid, threadsPerBlock >>>(ptrs->speciesCount, ptrs->speciesPitch/sizeof(float), ptrs->speciesData, ptrs->speciesF1, ptrs->speciesF2, ptrs->speciesConnection, ptrs->pminParam, ptrs->pmaxParam, ptrs->cParam, ptrs->hParam, ptrs->linearPlus, ptrs->linearPlusConnection, ptrs->linearMinus, ptrs->linearMinusConnection, ptrs->linearParam, ptrs->linearCount, ptrs->dt);\n", prefixName];
  [GPUCode appendFormat: @"   cudaMemcpy2D(ptrs->speciesData, ptrs->speciesCount * sizeof(float), ptrs->speciesF2, ptrs->speciesPitch, ptrs->speciesCount * sizeof(float), ptrs->numOfODEs, cudaMemcpyDeviceToDevice);\n"];
  [GPUCode appendFormat: @" }\n"];
  
  [GPUCode appendString: @"\n // Copy result to host memory\n"];
  [GPUCode appendString: @" //cudaMemcpy(ptrs->localEpsilonCheck, ptrs->epsilonCheck, ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(hostData, ptrs->speciesCount * sizeof(float), ptrs->speciesData, ptrs->speciesPitch, ptrs->speciesCount * sizeof(float), ptrs->numOfODEs, cudaMemcpyDeviceToHost);\n"];
  
  [GPUCode appendString: @"}\n"];
  
  //
  // Release
  //
  [GPUCode appendString: @"\n// Release\n"];
  
  [GPUCode appendFormat: @"void %@_releaseGPUKernel(void *model, void *p)\n{\n", prefixName];
  [GPUCode appendFormat: @" %@_GPUptrs *ptrs = (%@_GPUptrs *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesF1);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesF2);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->epsilonCheck);\n"];
  [GPUCode appendString: @" free(ptrs->localEpsilonCheck);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesConnection);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->pminParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->pmaxParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->cParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->hParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearParam);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearPlus);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearPlusConnection);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearMinus);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->linearMinusConnection);\n"];
  [GPUCode appendString: @" free(ptrs);\n"];
  [GPUCode appendString: @" cudaThreadExit();\n"];
  
  [GPUCode appendString: @"}\n"];
  
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"RModelDynamicGPUFunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_initGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;
}
#endif

- (NSMutableString *)definitionsDynamicGPUCode: (NSString *)prefixName
{
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendFormat: @"typedef struct _%@_GPUptrs {\n", prefixName];
  [GPUCode appendString: @"  int numOfODEs;\n"];
  [GPUCode appendString: @"  int numParams;\n"];
  [GPUCode appendString: @"  float dt;\n"];
  [GPUCode appendString: @"  float EPS;\n"];
  [GPUCode appendString: @"  int *localEpsilonCheck;\n"];
  [GPUCode appendString: @"  int *epsilonCheck;\n"];
  [GPUCode appendString: @"  int speciesCount;\n"];
  [GPUCode appendString: @"  float *speciesData;\n"];
  [GPUCode appendString: @"  float *speciesF1;\n"];
  [GPUCode appendString: @"  float *speciesF2;\n"];
  [GPUCode appendString: @"  size_t speciesPitch;\n"];
  [GPUCode appendString: @"  float *speciesConnection;\n"];
  [GPUCode appendString: @"  float *pminParam;\n"];
  [GPUCode appendString: @"  float *pmaxParam;\n"];
  [GPUCode appendString: @"  float *cParam;\n"];
  [GPUCode appendString: @"  float *hParam;\n"];
  [GPUCode appendString: @"  size_t connectionPitch;\n"];
  [GPUCode appendString: @"  int linearCount;\n"];
  [GPUCode appendString: @"  float *linearParam;\n"];
  [GPUCode appendString: @"  int *linearPlus;\n"];
  [GPUCode appendString: @"  int *linearPlusConnection;\n"];
  [GPUCode appendString: @"  int *linearMinus;\n"];
  [GPUCode appendString: @"  int *linearMinusConnection;\n"];
  [GPUCode appendString: @"  size_t linearPitch;\n"];
  [GPUCode appendFormat: @"} %@_GPUptrs;\n\n", prefixName];
    
  [GPUCode appendFormat: @"__constant__ %@_GPUptrs %@_struct[1];\n\n", prefixName, prefixName];

  return GPUCode;
}

- (NSMutableString *)kernelDynamicGPUCode: (NSString *)prefixName
{  
  NSMutableString *GPUCode = [NSMutableString new];

  // 2nd order Runge-Kutta
  
  // Calculate F1
  [GPUCode appendString: @"\n// F1 kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F1(int speciesCount, int speciesPitch, float *speciesData, float *speciesF1, float *speciesF2, float *speciesConnection, float *pminParam, float *pmaxParam, float *cParam, float *hParam, int *linearPlus, int *linearPlusConnection, int *linearMinus, int *linearMinusConnection, float *linear, int linearCount, float dt)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int i = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int k;\n"];
  [GPUCode appendString: @"   float interactionValue, linearValue, F1_val;\n"];
  
  [GPUCode appendString: @"\n   if (i >= speciesCount) return;\n"];
    
  [GPUCode appendString: @"\n   // calculate F1\n"];

  [GPUCode appendFormat: @"   //float species_val = speciesData[i];\n"];
  
    
  [GPUCode appendString: @"   interactionValue = 1.0;\n"];
  [GPUCode appendString: @"   char found = 0;\n"];
  [GPUCode appendString: @"   for (k = 0; k < speciesCount; ++k) {\n"];
  [GPUCode appendString: @"     if (speciesConnection[k*speciesPitch+i] != 0) {\n"];
  [GPUCode appendString: @"        found = 1;\n"];
  [GPUCode appendFormat: @"        interactionValue *= HILL(speciesData[k], pminParam[k*speciesPitch+i], pmaxParam[k*speciesPitch+i], cParam[k*speciesPitch+i], hParam[k*speciesPitch+i]);\n"];
  [GPUCode appendString: @"     }\n"];
  [GPUCode appendString: @"   }\n"];
  [GPUCode appendString: @"   if (!found) interactionValue = 0.0;\n"];
    
  [GPUCode appendString: @"   linearValue = 0.0;\n"];
  [GPUCode appendString: @"   for (k = 0; k < linearCount; ++k) {\n"];
  [GPUCode appendString: @"     if (linearPlusConnection[k*speciesPitch+i] == -1) break;\n"];
  [GPUCode appendString: @"     linearValue += linear[linearPlus[k*speciesPitch+i]] * speciesData[linearPlusConnection[k*speciesPitch+i]];\n"];
  [GPUCode appendString: @"   }\n"];
  [GPUCode appendString: @"   for (k = 0; k < linearCount; ++k) {\n"];
  [GPUCode appendString: @"     if (linearMinusConnection[k*speciesPitch+i] == -1) break;\n"];
  [GPUCode appendString: @"     linearValue -= linear[linearMinus[k*speciesPitch+i]] * speciesData[linearMinusConnection[k*speciesPitch+i]];\n"];
  [GPUCode appendString: @"   }\n"];
  
  [GPUCode appendFormat: @"   F1_val = speciesData[i] + 0.5 * dt * (interactionValue + linearValue);\n"];
  [GPUCode appendString: @"   if (F1_val < 0.0) F1_val = 0;\n"];
  [GPUCode appendString: @"   speciesF1[i] = F1_val;\n"];
  [GPUCode appendString: @"}\n"];

  // Calculate F2
  [GPUCode appendString: @"\n// F2 kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F2(int speciesCount, int speciesPitch, float *speciesData, float *speciesF1, float *speciesF2, float *speciesConnection, float *pminParam, float *pmaxParam, float *cParam, float *hParam, int *linearPlus, int *linearPlusConnection, int *linearMinus, int *linearMinusConnection, float *linear, int linearCount, float dt)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int i = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int k;\n"];
  [GPUCode appendString: @"   float interactionValue, linearValue, F2_val;\n"];
  
  [GPUCode appendString: @"\n   if (i >= speciesCount) return;\n"];
  
  [GPUCode appendString: @"\n   // calculate F2\n"];
  
  [GPUCode appendFormat: @"   //float species_val = speciesF1[i];\n"];
  [GPUCode appendFormat: @"   //float species_gval = speciesData[i];\n"];
  
  
  [GPUCode appendString: @"   interactionValue = 1.0;\n"];
  [GPUCode appendString: @"   char found = 0;\n"];
  [GPUCode appendString: @"   for (k = 0; k < speciesCount; ++k) {\n"];
  [GPUCode appendString: @"     if (speciesConnection[k*speciesPitch+i] != 0) {\n"];
  [GPUCode appendString: @"       found = 1;\n"];
  [GPUCode appendFormat: @"       interactionValue *= HILL(speciesF1[k], pminParam[k*speciesPitch+i], pmaxParam[k*speciesPitch+i], cParam[k*speciesPitch+i], hParam[k*speciesPitch+i]);\n"];
  [GPUCode appendString: @"     }\n"];
  [GPUCode appendString: @"   }\n"];
  [GPUCode appendString: @"   if (!found) interactionValue = 0.0;\n"];
  
  [GPUCode appendString: @"   linearValue = 0.0;\n"];
  [GPUCode appendString: @"   for (k = 0; k < linearCount; ++k) {\n"];
  [GPUCode appendString: @"     if (linearPlusConnection[k*speciesPitch+i] == -1) break;\n"];
  [GPUCode appendString: @"     linearValue += linear[linearPlus[k*speciesPitch+i]] * speciesF1[linearPlusConnection[k*speciesPitch+i]];\n"];
  [GPUCode appendString: @"   }\n"];
  [GPUCode appendString: @"   for (k = 0; k < linearCount; ++k) {\n"];
  [GPUCode appendString: @"     if (linearMinusConnection[k*speciesPitch+i] == -1) break;\n"];
  [GPUCode appendString: @"     linearValue -= linear[linearMinus[k*speciesPitch+i]] * speciesF1[linearMinusConnection[k*speciesPitch+i]];\n"];
  [GPUCode appendString: @"   }\n"];
  
  [GPUCode appendFormat: @"   F2_val = speciesData[i] + dt * (interactionValue + linearValue);\n"];
  [GPUCode appendString: @"   if (F2_val < 0.0) F2_val = 0;\n"];
  [GPUCode appendString: @"   speciesF2[i] = F2_val;\n"];
  [GPUCode appendString: @"}\n"];

#if 0
  // check epsilon
  [GPUCode appendString: @"\n   // check epsilon\n"];
  [GPUCode appendString: @"   epsilonCheck[i] = 1;\n"];
  for (k = 0; k < speciesCount; ++k) {
    [GPUCode appendFormat: @"   if (fabs(speciesF2[%d*speciesPitch+i] - speciesData[%d*speciesPitch+i]) >= EPS) epsilonCheck[i] = 0;\n", k, k];
  }
  
  // final values
  [GPUCode appendString: @"\n   // final values\n"];
  for (k = 0; k < speciesCount; ++k) {
    [GPUCode appendFormat: @"   speciesData[%d*speciesPitch+i] = speciesF2[%d*speciesPitch+i];\n", k, k];
  }
  [GPUCode appendString: @"   }\n"];
#endif    
  
  //[GPUCode appendString: @"}\n"];
  
  return GPUCode;
}

@end

//
// Run GPU code
//
@implementation ReactionModel (GPURun)

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions
{
  int i;
  ModelGPUData *gpuData = (ModelGPUData *)malloc(sizeof(ModelGPUData));
  
  gpuData->numModels = numModels;
  gpuData->numParams = [self numOfParameters];
  gpuData->gpuFunctions = gpuFunctions;
  
  if (isGPUStatic) {
    gpuData->data = malloc(sizeof(float) * numModels * speciesCount);
    gpuData->parameters = (float *)malloc(sizeof(float) * numModels * gpuData->numParams);
  } else {
    gpuData->data = (RMDynamic_data *)malloc(sizeof(RMDynamic_data) * numModels);
    for (i = 0; i < numModels; ++i) {
      RMDynamic_data *g = (RMDynamic_data *)gpuData->data;
      g[i].speciesData = malloc(sizeof(float) * speciesCount);
      g[i].speciesConnection = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
      g[i].pminParam = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
      g[i].pmaxParam = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
      g[i].cParam = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
      g[i].hParam = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
      g[i].linearPlusParam = (int *)malloc(sizeof(int) * speciesCount * linearCount);
      g[i].linearMinusParam = (int *)malloc(sizeof(int) * speciesCount * linearCount);
      g[i].linearPlusConn = (int *)malloc(sizeof(int) * speciesCount * linearCount);
      g[i].linearMinusConn = (int *)malloc(sizeof(int) * speciesCount * linearCount);
      g[i].linearParam = (float *)malloc(sizeof(float) * linearCount);
    }
  }
  
  return gpuData;
}

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int j;
  
  if (isGPUStatic) {
    // Static GPU layout
    float *indData = dataResults;
    float (*hostData)[speciesCount][gpuData->numModels] = (void *)gpuData->data;
    
    if (aFlag)
      for (j = 0; j < speciesCount; ++j) (*hostData)[j][aNum] = indData[j];
    else
      for (j = 0; j < speciesCount; ++j) indData[j] = (*hostData)[j][aNum];
    
  } else {
    // Dynamic GPU layout
    float *indData = dataResults;
    RMDynamic_data *g = (RMDynamic_data *)gpuData->data;

    if (aFlag)
      for (j = 0; j < speciesCount; ++j) g[aNum].speciesData[j] = indData[j];
    else
      for (j = 0; j < speciesCount; ++j) indData[j] = g[aNum].speciesData[j];
  }  
}

- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int p = 0;
  int k, l;
  
  if (isGPUStatic) {
    // Static GPU layout
    float (*hostParameters)[gpuData->numParams][gpuData->numModels] = (void *)gpuData->parameters;

    if (aFlag) {
      // Hill interactions
      for (k = 0; k < speciesCount; ++k) {
        for (l = 0; l < interactionsCount[k]; ++l) {
          (*hostParameters)[p][aNum] = interactions_pmin[k][l];
          ++p;
          
          (*hostParameters)[p][aNum] = interactions_pmax[k][l];
          ++p;
          
          (*hostParameters)[p][aNum] = interactions_c[k][l];
          ++p;
          
          (*hostParameters)[p][aNum] = interactions_h[k][l];
          ++p;
        }
      }
      
      // linear interactions
      for (k = 0; k < linearCount; ++k) {
        (*hostParameters)[p][aNum] = linear_k[k];
        ++p;
      }

    } else {
      // Transfer parameters from GPU??
    }

  } else {
    // Dynamic GPU layout
    RMDynamic_data *g = (RMDynamic_data *)gpuData->data;
    
    if (aFlag) {
      // Hill interactions
      for (k = 0; k < speciesCount; ++k) {
        for (l = 0; l < speciesCount; ++l) g[aNum].speciesConnection[l*speciesCount+k] = 0.0;
        for (l = 0; l < interactionsCount[k]; ++l) {
          g[aNum].speciesConnection[interactions[k][l]*speciesCount+k] = 1.0;
          g[aNum].pminParam[interactions[k][l]*speciesCount+k] = interactions_pmin[k][l];
          g[aNum].pmaxParam[interactions[k][l]*speciesCount+k] = interactions_pmax[k][l];
          g[aNum].cParam[interactions[k][l]*speciesCount+k] = interactions_c[k][l];
          g[aNum].hParam[interactions[k][l]*speciesCount+k] = interactions_h[k][l];
        }
      }

      id li = [model objectForKey: @"linearInteractions"];
      for (k = 0; k < speciesCount; ++k) {
        for (l = 0; l < linearCount; ++l) {
          g[aNum].linearPlusParam[l*speciesCount+k] = -1;
          g[aNum].linearMinusParam[l*speciesCount+k] = -1;
          g[aNum].linearPlusConn[l*speciesCount+k] = -1;
          g[aNum].linearMinusConn[l*speciesCount+k] = -1;
        }
      }

      for (k = 0; k < speciesCount; ++k) {
        if (li) {
          //printf("%d: %d\n", k, linearPlusCount[k]);
          for (l = 0; l < linearPlusCount[k]; ++l) {
            g[aNum].linearPlusParam[l*speciesCount+k] = linearPlus[k][l];
            id a = [li objectAtIndex: linearPlus[k][l]];
            NSUInteger idx = [species indexOfObject: [a objectAtIndex: 0]];
            if (idx != NSNotFound) g[aNum].linearPlusConn[l*speciesCount+k] = idx;
          }
          //printf("%d: %d\n", k, linearMinusCount[k]);
          for (l = 0; l < linearMinusCount[k]; ++l) {
            //printf("%d\n", linearMinus[k][l]);
            g[aNum].linearMinusParam[l*speciesCount+k] = linearMinus[k][l];
            id a = [li objectAtIndex: linearMinus[k][l]];
            NSUInteger idx = [species indexOfObject: [a objectAtIndex: 0]];
            //printf("idx = %d\n", idx);
            if (idx != NSNotFound) g[aNum].linearMinusConn[l*speciesCount+k] = idx;
          }
        }
      }
      for (l = 0; l < linearCount; ++l) g[aNum].linearParam[l] = (float)linear_k[l];
      
    } else {
      // Transfer parameters from GPU??
    }
    
  }
  
  return p;
}

- (int)assignParameters: (float *)paramValues toIndividual: (BOOL)aFlag
{
  int p = 0;
  int k, l;

  if (aFlag) {
    // Hill interactions
    for (k = 0; k < speciesCount; ++k) {
      for (l = 0; l < interactionsCount[k]; ++l) {
        interactions_pmin[k][l] = paramValues[p];
        ++p;
        
        interactions_pmax[k][l] = paramValues[p];
        ++p;
        
        interactions_c[k][l] = paramValues[p];
        ++p;
        
        interactions_h[k][l] = paramValues[p];
        ++p;
      }
    }
    
    // linear interactions
    for (k = 0; k < linearCount; ++k) {
      linear_k[k] = paramValues[p];
      ++p;
    }
  } else {
    // Hill interactions
    for (k = 0; k < speciesCount; ++k) {
      for (l = 0; l < interactionsCount[k]; ++l) {
        paramValues[p] = interactions_pmin[k][l];
        ++p;
        
        paramValues[p] = interactions_pmax[k][l];
        ++p;
        
        paramValues[p] = interactions_c[k][l];
        ++p;
        
        paramValues[p] = interactions_h[k][l];
        ++p;
      }
    }
    
    // linear interactions
    for (k = 0; k < linearCount; ++k) {
      paramValues[p] = linear_k[k];
      ++p;
    }
  }
  
  return p;
}

/*
- (int)assignGPUParameters: (float *)paramValues
{
  return [self assignGPUParameters: paramValues numberOfModels: 1];
}

//
// Parameters stored in column format for each model
// NxP matrix of N models and P parameters
// (best coalesced memory access for GPU)
//
- (int)assignGPUParameters: (float *)paramValues numberOfModels: (int)numModels
{
  int p = 0;
  int i, k, l;
  
  for (i = 0; i < numModels; ++i) {
    int pi = i;
    // Hill interactions
    for (k = 0; k < speciesCount; ++k) {
      for (l = 0; l < interactionsCount[k]; ++l) {
        paramValues[pi] = interactions_pmin[k][l];
        pi += numModels;
        if (i == 0) ++p;

        paramValues[pi] = interactions_pmax[k][l];
        pi += numModels;
        if (i == 0) ++p;

        paramValues[pi] = interactions_c[k][l];
        pi += numModels;
        if (i == 0) ++p;

        paramValues[pi] = interactions_h[k][l];
        pi += numModels;
        if (i == 0) ++p;
      }
    }

    // linear interactions
    for (k = 0; k < linearCount; ++k) {
      paramValues[pi] = linear_k[k];
      pi += numModels;
      if (i == 0) ++p;
    }
  }
  
  return p;
}
 */


- (void)allocGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;

  if (isGPUStatic) {
    RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
    gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, gpuData->numModels, gpuData->numParams, speciesCount, (float)dt, epsilon);
  } else {
    //RModelDynamicGPUFunctions *gpuFunctions = (RModelDynamicGPUFunctions *)gpuData->gpuFunctions;
    //gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, gpuData->numModels, speciesCount, linearCount, (float)dt, epsilon);
  }
}

- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if (isGPUStatic) {
    RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
    (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, aFlag, gpuData->parameters, gpuData->data);
  } else {
    //RModelDynamicGPUFunctions *gpuFunctions = (RModelDynamicGPUFunctions *)gpuData->gpuFunctions;
    //(gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, aFlag, gpuData->data);
  }
}

#if 0
- (void)initGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if (isGPUStatic) {
    RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
    (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, gpuData->parameters, gpuData->data);
  } else {
  }
}
#endif

- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if (isGPUStatic) {
    RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
    (gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, gpuData->data, currentTime, endTime);
  } else {
    //RModelDynamicGPUFunctions *gpuFunctions = (RModelDynamicGPUFunctions *)gpuData->gpuFunctions;
    //(gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, currentTime, endTime);
  }
}

- (void)releaseGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if (isGPUStatic) {
    RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)gpuData->gpuFunctions;
    (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
  } else {
    RModelDynamicGPUFunctions *gpuFunctions = (RModelDynamicGPUFunctions *)gpuData->gpuFunctions;
    (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
  }
}

- (void)runGPUSimulation: (void *)funcs
{
  int p, k;
  RModelGPUFunctions *gpuFunctions = (RModelGPUFunctions *)funcs;
  
  // initialize GPU                                                                                                                          
  
  // parameters                                                                                                                              
  p = [self numOfParameters];
  float *hostParameters = (float *)malloc(sizeof(float) * p);
  for (k = 0; k < p; ++k) {
    hostParameters[k] = 0.0;
  }
  p = [self assignGPUParameters: hostParameters];
  
  for (k = 0; k < p; ++k) {
    printf("(%d) %f\n", k, hostParameters[k]);
  }
  
  int numOfODEs = 1;
  void *gpuPtrs = (gpuFunctions->allocGPUKernel)(self, numOfODEs, p, speciesCount, dt, epsilon);
  
  printf("time = 1\n");
  for (k = 0; k < speciesCount; ++k) {
    printf("%f ", ((float *)dataResults)[k]);
  }
  printf("\n");
  
  // run it                                                                                                                                  
  BOOL done = NO;
  int t = 0;
  while (!done) {
    
    // init GPU data
    (gpuFunctions->initGPUKernel)(self, gpuPtrs, YES, hostParameters, dataResults);
    
    // invoke GPU
    //(gpuFunctions->invokeGPUKernel)(self, gpuPtrs, dataResults, outputFrequency);
    
    // copy GPU data to CPU
    (gpuFunctions->initGPUKernel)(self, gpuPtrs, NO, hostParameters, dataResults);

    // output data grids                                                                                                                     
    printf("time = %d\n", t);
    for (k = 0; k < speciesCount; ++k) {
      printf("%f ", ((float *)dataResults)[k]);
    }
    printf("\n");
    
    // are we done?                                                                                                                          
    t += outputFrequency;
    if (t >= timeSteps) done = YES;
  }
  
  // release GPU
  (gpuFunctions->releaseGPUKernel)(self, gpuPtrs);
  
  // write out final data                                                                                                                    
  printf("time = %d\n", t);
  for (k = 0; k < speciesCount; ++k) {
    printf("%f ", ((float *)dataResults)[k]);
  }
  printf("\n");
  
  //free(hostParameters);                                                                                                                    
  //free(rk2grids);                                                                                                                            
}

#if 0
- (void)runGPUDynamicSimulation: (RModelDynamicGPUFunctions *)gpuFunctions
{
  int p, k, l;
  
  // initialize GPU                                                                                                                          
  
  // parameters                                                                                                                              
  p = [self numOfParameters];
  float *hostParameters = (float *)malloc(sizeof(float) * p);
  for (k = 0; k < p; ++k) {
    hostParameters[k] = 0.0;
  }
  p = [self assignGPUParameters: hostParameters];

#if 0
  for (k = 0; k < p; ++k) {
    printf("(%d) %f\n", k, hostParameters[k]);
  }
#endif
  
  int numOfODEs = 1;
  void *gpuPtrs = (gpuFunctions->allocGPUKernel)(self, numOfODEs, speciesCount, linearCount, dt, epsilon);
  
  float *speciesConnection = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
  float *pminParam = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
  float *pmaxParam = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
  float *cParam = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
  float *hParam = (float *)malloc(sizeof(float) * speciesCount * speciesCount);
  for (k = 0; k < speciesCount; ++k) {
    for (l = 0; l < speciesCount; ++l) speciesConnection[l*speciesCount+k] = 0.0;
    for (l = 0; l < interactionsCount[k]; ++l) {
      speciesConnection[interactions[k][l]*speciesCount+k] = 1.0;
      pminParam[interactions[k][l]*speciesCount+k] = interactions_pmin[k][l];
      pmaxParam[interactions[k][l]*speciesCount+k] = interactions_pmax[k][l];
      cParam[interactions[k][l]*speciesCount+k] = interactions_c[k][l];
      hParam[interactions[k][l]*speciesCount+k] = interactions_h[k][l];
    }
  }
  
  id li = [model objectForKey: @"linearInteractions"];
  int *linearPlusParam = (int *)malloc(sizeof(int) * speciesCount * linearCount);
  int *linearMinusParam = (int *)malloc(sizeof(int) * speciesCount * linearCount);
  int *linearPlusConn = (int *)malloc(sizeof(int) * speciesCount * linearCount);
  int *linearMinusConn = (int *)malloc(sizeof(int) * speciesCount * linearCount);
  float *linearParam = (float *)malloc(sizeof(float) * linearCount);
  for (k = 0; k < speciesCount; ++k) {
    for (l = 0; l < linearCount; ++l) {
      linearPlusParam[l*speciesCount+k] = -1;
      linearMinusParam[l*speciesCount+k] = -1;
      linearPlusConn[l*speciesCount+k] = -1;
      linearMinusConn[l*speciesCount+k] = -1;
    }
  }

  for (k = 0; k < speciesCount; ++k) {
    if (li) {
      //printf("%d: %d\n", k, linearPlusCount[k]);
      for (l = 0; l < linearPlusCount[k]; ++l) {
        linearPlusParam[l*speciesCount+k] = linearPlus[k][l];
        id a = [li objectAtIndex: linearPlus[k][l]];
        NSUInteger idx = [species indexOfObject: [a objectAtIndex: 0]];
        if (idx != NSNotFound) linearPlusConn[l*speciesCount+k] = idx;
      }
      //printf("%d: %d\n", k, linearMinusCount[k]);
      for (l = 0; l < linearMinusCount[k]; ++l) {
        //printf("%d\n", linearMinus[k][l]);
        linearMinusParam[l*speciesCount+k] = linearMinus[k][l];
        id a = [li objectAtIndex: linearMinus[k][l]];
        NSUInteger idx = [species indexOfObject: [a objectAtIndex: 0]];
        //printf("idx = %d\n", idx);
        if (idx != NSNotFound) linearMinusConn[l*speciesCount+k] = idx;
      }
    }
  }
  for (l = 0; l < linearCount; ++l) linearParam[l] = (float)linear_k[l];

#if 0
  for (k = 0; k < speciesCount; ++k) {
    for (l = 0; l < speciesCount; ++l)
      printf("%f ", speciesConnection[l*speciesCount+k]);
    printf("\n");
  }
  for (k = 0; k < speciesCount; ++k) {
    for (l = 0; l < speciesCount; ++l)
      printf("%f ", hParam[l*speciesCount+k]);
    printf("\n");
  }
  for (k = 0; k < speciesCount; ++k) {
    for (l = 0; l < linearCount; ++l)
      printf("%d ", linearMinusParam[l*speciesCount+k]);
    printf("\n");
  }
#endif

  printf("time = 1\n");
  for (k = 0; k < speciesCount; ++k) {
    printf("%f ", ((float *)dataResults)[k]);
  }
  printf("\n");
  
  // run it                                                                                                                                  
  BOOL done = NO;
  int t = 0;
  while (!done) {
    
    // init GPU data
    (gpuFunctions->initGPUKernel)(self, gpuPtrs, YES, dataResults, speciesConnection, pminParam, pmaxParam, cParam, hParam,
                       linearParam, linearPlusParam, linearPlusConn, linearMinusParam, linearMinusConn);
    
    // invoke GPU
    (gpuFunctions->invokeGPUKernel)(self, gpuPtrs, dataResults, outputFrequency);
    
    // output data grids                                                                                                                     
    printf("time = %d\n", t);
    for (k = 0; k < speciesCount; ++k) {
      printf("%f ", ((float *)dataResults)[k]);
    }
    printf("\n");
    
    // are we done?                                                                                                                          
    t += outputFrequency;
    if (t >= timeSteps) done = YES;
  }
  
  // release GPU
  (gpuFunctions->releaseGPUKernel)(self, gpuPtrs);
  
  // write out final data                                                                                                                    
  printf("time = %d\n", t);
  for (k = 0; k < speciesCount; ++k) {
    printf("%f ", ((float *)dataResults)[k]);
  }
  printf("\n");
  
  //free(hostParameters);                                                                                                                    
  //free(rk2grids);                                                                                                                            
}
#endif

@end

//
// OLD CODE
//

#if 0
- (NSMutableString *)controllerGPUCode: (NSString *)prefixName
{
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendString: @"#include <stdio.h>\n"];
  [GPUCode appendString: @"#include <unistd.h>\n\n"];
  
  [GPUCode appendString: @"extern \"C\" {\n"];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUCode appendFormat: @"#include \"%@_grn_defines.cu\"\n\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_grn_kernel.cu\"\n\n", prefixName];
  
  //
  // Allocation
  //
  
  [GPUCode appendString: @"\n// Allocation\n"];
  [GPUCode appendFormat: @"void *%@_allocGPUKernel(void *model, int numOfODEs, int numParams, int speciesCount, float dt, float EPS)\n{\n", prefixName];
  [GPUCode appendFormat: @" RModelRK2_GPUptrs *ptrs = (RModelRK2_GPUptrs *)malloc(sizeof(RModelRK2_GPUptrs));\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendString: @"\n // Save parameters\n"];
  [GPUCode appendString: @" ptrs->numOfODEs = numOfODEs;\n"];
  [GPUCode appendString: @" ptrs->numParams = numParams;\n"];
  [GPUCode appendString: @" ptrs->dt = dt;\n"];
  [GPUCode appendString: @" ptrs->EPS = EPS;\n"];
  [GPUCode appendString: @" ptrs->localEpsilonCheck = (int *)malloc(numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" ptrs->speciesCount = speciesCount;\n"];
  
  // Allocate device memory
  [GPUCode appendString: @"\n // Allocate device memory\n"];
  [GPUCode appendString: @" cudaMalloc((void**)&(ptrs->epsilonCheck), numOfODEs * sizeof(int));\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesData), &(ptrs->speciesPitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesF1), &(ptrs->speciesPitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->speciesF2), &(ptrs->speciesPitch), numOfODEs * sizeof(float), speciesCount);\n"];
  [GPUCode appendString: @" cudaMallocPitch((void**)&(ptrs->parameters), &(ptrs->paramPitch), numOfODEs * sizeof(float), numParams);\n"];
  
  [GPUCode appendString: @" return ptrs;\n"];
  [GPUCode appendString: @"}\n"];
  
  //
  // Initialization
  //
  [GPUCode appendString: @"\n// Initialization\n"];
  [GPUCode appendFormat: @"void %@_initGPUKernel(void *model, void *p, float *hostParameters, float *hostData)\n{\n", prefixName];
  [GPUCode appendFormat: @" RModelRK2_GPUptrs *ptrs = (RModelRK2_GPUptrs *)p;\n", prefixName, prefixName];
  
  // Copy host memory to device memory
  [GPUCode appendString: @"\n // Copy host memory to device memory\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->speciesData, ptrs->speciesPitch, (const cudaArray *)hostData, ptrs->numOfODEs * sizeof(float), ptrs->numOfODEs * sizeof(float), ptrs->speciesCount, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(ptrs->parameters, ptrs->paramPitch, (const cudaArray *)hostParameters, ptrs->numOfODEs * sizeof(float), ptrs->numOfODEs * sizeof(float), ptrs->numParams, cudaMemcpyHostToDevice);\n"];
  [GPUCode appendString: @" cudaMemset(ptrs->epsilonCheck, 0, ptrs->numOfODEs * sizeof(int));\n"];
  
  [GPUCode appendString: @"}\n"];
  
  //
  // Execution
  //
  [GPUCode appendString: @"\n// Execution\n"];
  [GPUCode appendFormat: @"void %@_invokeGPUKernel(void *model, void *p, float *hostData, int timeSteps)\n{\n", prefixName];
  [GPUCode appendFormat: @" RModelRK2_GPUptrs *ptrs = (RModelRK2_GPUptrs *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @" int t;\n"];
  [GPUCode appendString: @" int blocksPerGrid = 1;\n"];
  [GPUCode appendString: @" int threadsPerBlock = 1;\n"];
  [GPUCode appendString: @" if (ptrs->numOfODEs < 256) {\n"];
  [GPUCode appendString: @"   threadsPerBlock = ptrs->numOfODEs;\n"];
  [GPUCode appendString: @"   blocksPerGrid = 1;\n"];
  [GPUCode appendString: @" } else {\n"];
  [GPUCode appendString: @"   threadsPerBlock = 256;\n"];
  [GPUCode appendString: @"   blocksPerGrid = (ptrs->numOfODEs + threadsPerBlock - 1) / threadsPerBlock;\n"];
  [GPUCode appendString: @" }\n"];
  
  [GPUCode appendString: @"\n// Invoke kernel\n"];
  [GPUCode appendString: @" for (t = 0; t < timeSteps; ++t) {\n"];
  [GPUCode appendFormat: @"   %@_kernel_F1<<< blocksPerGrid, threadsPerBlock >>>(ptrs->numOfODEs, ptrs->speciesPitch/sizeof(float), ptrs->speciesData, ptrs->speciesF1, ptrs->speciesF2, ptrs->dt);\n", prefixName];
  [GPUCode appendFormat: @"   %@_kernel_F2<<< blocksPerGrid, threadsPerBlock >>>(ptrs->numOfODEs, ptrs->speciesPitch/sizeof(float), ptrs->speciesData, ptrs->speciesF1, ptrs->speciesF2, ptrs->dt);\n", prefixName];
  [GPUCode appendString: @"   cudaMemcpy2D(ptrs->speciesData, ptrs->speciesPitch, ptrs->speciesF2, ptrs->speciesPitch, ptrs->numOfODEs * sizeof(float), ptrs->speciesCount, cudaMemcpyDeviceToDevice);\n"];
  [GPUCode appendString: @" }\n"];
  
  [GPUCode appendString: @"\n// Copy result to host memory\n"];
  [GPUCode appendString: @" cudaMemcpy(ptrs->localEpsilonCheck, ptrs->epsilonCheck, ptrs->numOfODEs * sizeof(int), cudaMemcpyDeviceToHost);\n"];
  [GPUCode appendString: @" cudaMemcpy2D(hostData, ptrs->numOfODEs * sizeof(float), ptrs->speciesData, ptrs->speciesPitch, ptrs->numOfODEs * sizeof(float), ptrs->speciesCount, cudaMemcpyDeviceToHost);\n"];
  
  [GPUCode appendString: @"}\n"];
  
  //
  // Release
  //
  [GPUCode appendString: @"\n// Release\n"];
  
  [GPUCode appendFormat: @"void %@_releaseGPUKernel(void *model, void *p)\n{\n", prefixName];
  [GPUCode appendFormat: @" RModelRK2_GPUptrs *ptrs = (RModelRK2_GPUptrs *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @" cudaFree(ptrs->speciesData);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesF1);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->speciesF2);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->parameters);\n"];
  [GPUCode appendString: @" cudaFree(ptrs->epsilonCheck);\n"];
  [GPUCode appendString: @" free(ptrs->localEpsilonCheck);\n"];
  [GPUCode appendString: @" free(ptrs);\n"];
  [GPUCode appendString: @" cudaThreadExit();\n"];
  
  [GPUCode appendString: @"}\n"];
  
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"RModelGPUFunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_initGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;
}
#endif

#if 0
// old definitions
// support separate set of parameters for each ODE model
{
  [GPUCode appendString: @"\n// parameters\n"];
  
  int p = 0;
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"// %@\n", s];        
    //[GPUCode appendFormat: @"#define %@_decay p[%d*speciesPitch+i]\n", s, p];
    //++p;
    
    for (l = 0; l < interactionsCount[k]; ++l) {
      id is = [species objectAtIndex: interactions[k][l]];
      
      [GPUCode appendFormat: @"// Hill parameters, %@\n", is];
      
      [GPUCode appendFormat: @"#define %@_pmin_%@ p[%d*speciesPitch+i]\n", s, is, p];
      ++p;
      
      [GPUCode appendFormat: @"#define %@_pmax_%@ p[%d*speciesPitch+i]\n", s, is, p];
      ++p;
      
      [GPUCode appendFormat: @"#define %@_c_%@ p[%d*speciesPitch+i]\n", s, is, p];
      ++p;
      
      [GPUCode appendFormat: @"#define %@_h_%@ p[%d*speciesPitch+i]\n", s, is, p];
      ++p;
    }
  }
  // linear interactions
  [GPUCode appendString: @"// Linear parameters\n"];
  id li = [model objectForKey: @"linearInteractions"];
  for (k = 0; k < linearCount; ++k) {
    id a = [li objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_linear_%@ p[%d*speciesPitch+i]\n", [a objectAtIndex: 0], [a objectAtIndex: 1], p];
    ++p;
  }
  
  // species index
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_species %d\n", s, k];
  }
  
  // variable definitions (to avoid running out of registers for large models)
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"#define %@_val speciesData[%d*speciesPitch+i]\n", s, k];
    [GPUCode appendFormat: @"#define %@_F1val speciesF1[%d*speciesPitch+i]\n", s, k];
  }
}  
#endif

#if 0
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  int k, l;
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  id li = [model objectForKey: @"linearInteractions"];
  
  // 2nd order Runge-Kutta
  
  [GPUCode appendString: @"\n// Kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_RK2(int numOfODEs, int pitch, float *speciesData, float *speciesData_F1, float *speciesData_F2, float dt, float EPS, int *epsilonCheck, int timeSteps)", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int ODEnum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int t;\n"];
  [GPUCode appendString: @"   float interactionValue, linearValue, F1_val, F2_val;\n"];
  
  [GPUCode appendString: @"\n   if (ODEnum >= numOfODEs) return;\n"];
  [GPUCode appendString: @"\n   if (epsilonCheck[i]) return;\n"];
  
  [GPUCode appendString: @"\n   for (t = 0; t < timeSteps; ++t) {\n"];
  
  // Calculate F1
  [GPUCode appendString: @"\n   // calculate F1\n"];
  
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n   // %@\n", s];
    
    if (interactionsCount[k] == 0) [GPUCode appendString: @"   interactionValue = 0.0;\n"];
    else [GPUCode appendString: @"   interactionValue = 1.0;\n"];
    for (l = 0; l < interactionsCount[k]; ++l) {
      id is = [species objectAtIndex: interactions[k][l]];
      [GPUCode appendFormat: @"   interactionValue *= HILL(%@_val, %@_pmin_%@, %@_pmax_%@, %@_c_%@, %@_h_%@);\n", is, s, is, s, is, s, is, s, is];
    }
    
    [GPUCode appendString: @"   linearValue = 0.0;\n"];
    if (li) {
      for (l = 0; l < linearPlusCount[k]; ++l) {
        id a = [li objectAtIndex: linearPlus[k][l]];
        [GPUCode appendFormat: @"   linearValue += %@_linear_%@ * %@_val;\n", [a objectAtIndex: 0], [a objectAtIndex: 1], s];
      }
      for (l = 0; l < linearMinusCount[k]; ++l) {
        id a = [li objectAtIndex: linearMinus[k][l]];
        [GPUCode appendFormat: @"   linearValue -= %@_linear_%@ * %@_val;\n", [a objectAtIndex: 0], [a objectAtIndex: 1], s];
      }
    }
    
    [GPUCode appendFormat: @"   F1_val = %@_val + 0.5 * dt * (interactionValue + linearValue);\n", s];
    //[GPUCode appendFormat: @"   F1_val = %@_val + 0.5 * dt * (interactionValue - %@_decay * %@_val);\n", s, s, s];
    [GPUCode appendString: @"   if (F1_val < 0.0) F1_val = 0;\n"];
    [GPUCode appendFormat: @"   speciesData_F1[%@_species*pitch+ODEnum] = F1_val;\n", s];
  }
  
  // Calculate F2
  [GPUCode appendString: @"\n   // calculate F2\n"];
  
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n   // %@\n", s];
    
    if (interactionsCount[k] == 0) [GPUCode appendString: @"   interactionValue = 0.0;\n"];
    else [GPUCode appendString: @"   interactionValue = 1.0;\n"];
    for (l = 0; l < interactionsCount[k]; ++l) {
      id is = [species objectAtIndex: interactions[k][l]];
      [GPUCode appendFormat: @"   interactionValue *= HILL(%@_F1val, %@_pmin_%@, %@_pmax_%@, %@_c_%@, %@_h_%@);\n", is, s, is, s, is, s, is, s, is];
    }
    
    [GPUCode appendString: @"   linearValue = 0.0;\n"];
    if (li) {
      for (l = 0; l < linearPlusCount[k]; ++l) {
        id a = [li objectAtIndex: linearPlus[k][l]];
        [GPUCode appendFormat: @"   linearValue += %@_linear_%@ * %@_F1val;\n", [a objectAtIndex: 0], [a objectAtIndex: 1], s];
      }
      for (l = 0; l < linearMinusCount[k]; ++l) {
        id a = [li objectAtIndex: linearMinus[k][l]];
        [GPUCode appendFormat: @"   linearValue -= %@_linear_%@ * %@_F1val;\n", [a objectAtIndex: 0], [a objectAtIndex: 1], s];
      }
    }
    
    [GPUCode appendFormat: @"   F2_val = %@_val + dt * (interactionValue + linearValue);\n", s];
    //[GPUCode appendFormat: @"   F2_val = %@_gval + dt * (interactionValue - %@_decay * %@_val);\n", s, s, s];
    [GPUCode appendString: @"   if (F2_val < 0.0) F2_val = 0;\n"];
    [GPUCode appendFormat: @"   speciesData_F2[%@_species*speciesPitch+i] = F2_val;\n", s];
  }
  
  // check epsilon
  [GPUCode appendString: @"\n   // check epsilon\n"];
  [GPUCode appendString: @"   epsilonCheck[i] = 1;\n"];
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   if (fabs(speciesF2[%@_species*speciesPitch+i] - speciesData[%@_species*speciesPitch+i]) >= EPS) epsilonCheck[i] = 0;\n", s, s];
  }
  
  // final values
  [GPUCode appendString: @"\n   // final values\n"];
  for (k = 0; k < speciesCount; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   speciesData[%@_species*speciesPitch+i] = speciesF2[%@_species*speciesPitch+i];\n", s, s];
  }
  [GPUCode appendString: @"   }\n"];
  
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;
}
#endif


