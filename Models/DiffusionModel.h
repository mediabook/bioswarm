/*
 DiffusionModel.h
 
 Diffusion model for multiple dimensions.
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: December 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BioSwarmModel.h"
#import "MultiScaleGrid.h"

@interface DiffusionModel : BioSwarmModel
{
  id <MultiScaleGrid> grid;
  double domainSize;
  int gridSize;
  
  double dx;
  
  void **speciesGrid;
  void **speciesF1;
  void **speciesF2;
  void **speciesGradient;
  void **speciesDiff;
  
  FILE **dataFiles;
  int dataFileState;
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent;

@end

#import "GPUDefines.h"

//
// Produce GPU code
//
@interface DiffusionModel (GPU)
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSDictionary *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName;
@end

//
// Run GPU code
//
@interface DiffusionModel (GPURun)
- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions;
- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (void)allocGPUKernel: (void *)data;
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUKernel: (void *)data;
@end
