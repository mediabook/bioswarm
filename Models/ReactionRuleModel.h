/*
 ReactionRuleModel.h
 
 General rule-based reaction model.
 
 Copyright (C) 2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: July 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import <BioSwarm/BioSwarmModel.h>

@interface ReactionRuleModel : BioSwarmModel
{
  NSArray *reactions;
  int numOfReactions;
  NSMutableArray *speciesFunctions;
  id initialValue;
  NSMutableDictionary *initialValues;

  NSDictionary *sharedParameters;

  void *dataResults;
  int *dataFlags;
  
  FILE *dataFile;
  int dataFileState;
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent;

- (void)collectParameters;

- (void)setupDataFilesWithState: (int)aState;
- (void)allocateSimulationWithEncode: (NSString *)anEncode;
- (BOOL)writeDataWithCheck: (BOOL)aFlag;
- (BOOL)readData;
- (void)rewindFiles;
- (void)initializeSimulation;
- (void)cleanupSimulation;

- (NSArray *)reactions;
- (int)numOfSpecies;
- (NSString *)nameOfSpecies: (int)theSpecies;
//- (int)timeSteps;
//- (double)epsilon;
- (NSString *)dataEncode;
- (void *)dataResults;
- (FILE *)dataFile;

- (int)numOfParameters;
- (NSString *)nameOfParameter: (int)theParam;

@end

//
// GPU
//
#import "GPUDefines.h"

//
// Produce GPU code
//
@interface ReactionRuleModel (GPU)
- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
@end

//
// Run GPU code
//
@interface ReactionRuleModel (GPURun)
- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions;
- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (void)allocGPUKernel: (void *)data;
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUKernel: (void *)data;
@end
