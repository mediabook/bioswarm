/*
 SpatialModel.h
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#ifndef _BC_SPATIALMODEL_H_
#define _BC_SPATIALMODEL_H_

#import <Foundation/Foundation.h>
#import <BioSwarm/MultiScaleGrid.h>

@interface SpatialModel : NSObject
{
  // model
  NSDictionary *model;
  
  id <MultiScaleGrid> grid;

  // model attributes
  int dimensions;
  double *domainSize;
  double *domainCoordinates;
  int *gridSize;

  BOOL isSubspace;
  id subspaceTranslator;
}

- newFromModelIndividual;
- initWithModel: (NSDictionary *)aModel;
- (NSDictionary *)model;

- (int)dimensions;
- (double *)domainSize;
- (int *)gridSize;
- (id <MultiScaleGrid>)grid;

- (BOOL)isSubspace;
- (void)setSubspace: (BOOL)aFlag;
- (void)setSubspaceTranslator: anObj;

- (void)translateIntegerCoordinatesToWorld: (int *)coordinates forModel: aModel;
- (void)translateFloatCoordinatesToWorld: (float *)coordinates forModel: aModel;
- (void)translateFloatCoordinatesToWorld: (float *)coordinates forModel: aModel direction: (int)direction;
- (void)translateDoubleCoordinatesToWorld: (double *)coordinates forModel: aModel;
- (void)translateIntegerCoordinatesToSubspace: (int *)coordinates forModel: aModel;
- (void)translateFloatCoordinatesToSubspace: (float *)coordinates forModel: aModel;
- (void)translateDoubleCoordinatesToSubspace: (double *)coordinates forModel: aModel;

@end

//
// GPU
//
#import "GPUDefines.h"

//
// Produce GPU code
//

@interface SpatialModel (GPU)
- (NSDictionary *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName;
@end

//
// Visualization
//

@interface SpatialModel (Povray)
- (NSMutableString *)povrayCode;
@end

//
// Sector utility functions
//

extern int opposite_sector(int aSector);
extern void translate_coordinate(float X, float Y, float Z, int direction, float size,
                                 float *newX, float *newY, float *newZ);
extern void fill_sector_from_current(int aSector, int direction, void *sectorGraph, int maxSectors);

#endif // _BC_SPATIALMODEL_H_
