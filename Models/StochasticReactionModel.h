/*
 StochasticReactionModel.h
 
 Stochastic chemical reaction model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Sept. 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Cocoa/Cocoa.h>


@interface StochasticReactionModel : NSObject {
  NSArray *species;
  int numOfSpecies;
  long *X;
  
  NSArray *reactions;
  int numOfReactions;
  int **reactionPreConditions;
  int *numPreConditions;
  int **reactionPostConditions;
  int *numPostConditions;
  double *rate;
  
  NSArray *interactions;
  int numOfInteractions;  
}

- initWithModel: (NSDictionary *)aModel;
- (void)runSimToTime: (double)stopTime;

@end
