/*
 ReactionModel.h
 
 Chemical reaction model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */


#include <Foundation/Foundation.h>

#import "MultiScale2DGrid.h"
#import "ComputationalBiologyModel.h"

@interface ReactionModel : ComputationalBiologyModel
{
  // reaction model
  NSArray *species;
  int speciesCount;
  int **interactions;
  int *interactionsCount;
  double **interactions_pmin;
  double **interactions_pmax;
  double **interactions_c;
  double **interactions_h;
  //double *speciesDecay;
  int **linearPlus;
  int **linearMinus;
  int *linearPlusCount;
  int *linearMinusCount;
  int linearCount;
  double *linear_k;

  double dt;

  int timeSteps;
  BOOL runToEpsilon;
  double epsilon;
  BOOL *withinEpsilon;
  int epsilonCheck;

  int outputFrequency;
  int outputRun;
  FILE **dataFiles;
  int dataFileState;

  NSString *dataEncode;
  int encodeTag;
  void *dataResults;
  double *dataCalc;
  double *dataF1;
  double *dataF2;
  void *dataDiff;

  BOOL isGPUStatic;
}

- newFromModelIndividual;
- initWithModel: (NSDictionary *)aModel;
- initWithModel: (NSDictionary *)aModel andController: anObj;

- (void)allocateSimulationWithEncode: (NSString *)anEncode;
- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState;
- (void)initializeSimulation;
- (void)writeData;
- (void)writeDataWithCheck: (BOOL)aFlag;
- (void)runSimulation;
- (void)cleanupSimulation;

- (int)numOfSpecies;
- (NSString *)nameOfSpecies: (int)theSpecies;
- (double)dt;
- (int)timeSteps;
- (double)epsilon;
- (NSString *)dataEncode;
- (void *)dataResults;
- (FILE **)dataFiles;

- (int)numOfParameters;
- (NSString *)nameOfParameter: (int)theParam;

- (BOOL)setupParameterRanges: (NSDictionary *)ranges;
- (double)randomValueforParameter: (int)aParam;
- (double)perturbValueForParameter: (int)aParam withinRange: (BOOL)aFlag standardDeviation: (double)SD;
- (int)modifyRandomParameter: (NSMutableDictionary *)params;
- (double *)parameterMinRange;
- (double *)parameterMaxRange;

@end

//
// Delegate methods
//
@protocol ReactionDelegate
- (void)preDataOutput: (int)aTime;
@end

//
// GPU
//
#import "GPUDefines.h"

//
// Produce GPU code
//
@interface ReactionModel (GPU)
- (BOOL)isGPUStatic;
- (void)setGPUStatic: (BOOL)aFlag;

- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;

- (NSMutableDictionary *)controllerStaticGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsStaticGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelStaticGPUCode: (NSString *)prefixName;

- (NSMutableDictionary *)controllerDynamicGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsDynamicGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelDynamicGPUCode: (NSString *)prefixName;
@end

//
// Run GPU code
//
@interface ReactionModel (GPURun)
- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions;
- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (float *)paramValues toIndividual: (BOOL)aFlag;
- (int)assignGPUParameters: (float *)paramValues;
- (int)assignGPUParameters: (float *)paramValues numberOfModels: (int)numModels;

- (void)allocGPUKernel: (void *)data;
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUKernel: (void *)data;

- (void)runGPUSimulation: (void *)gpuFunctions;
- (void)runGPUDynamicSimulation: (RModelDynamicGPUFunctions *)gpuFunctions;
@end

extern double normal_dist_rand(double mean, double sd);
