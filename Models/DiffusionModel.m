//
//  DiffusionModel.m
//  BioSwarm
//
//  Created by Scott Christley on 12/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DiffusionModel.h"
#import "Grid2DArray.h"
#import "Grid3DArray.h"

#import "internal.h"

@implementation DiffusionModel

- (void)collectParameters
{
  int i;
  
  // total up the parameters
  parameterNames = [NSMutableArray new];
  // add diffusion parameter for each species
  for (i = 0; i < numOfSpecies; ++i) {
    NSString *s = [species objectAtIndex: i];
    [parameterNames addObject: [NSString stringWithFormat: @"%@_diffusion", s]];
  }
  
  numOfParameters = [parameterNames count];
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  int i;
  NSString *param;

  [super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent];
  
  id spatialModel = [modelController spatialModel];
  grid = [[spatialModel grid] retain];
  
  // TODO: don't store locally
  double *ds = [spatialModel domainSize];
  domainSize = ds[0];
  int *gs = [spatialModel gridSize];
  gridSize = gs[0];
  dx = domainSize / (double)gridSize;
  if ([self isDebug]) printf("dx = %lf\n", dx);  

  // collect parameters
  [self collectParameters];

  if (!numOfParameters)
    printf("WARNING: No parameters in model.\n");
  else {
    parameterValues = (double *)malloc(sizeof(double) * numOfParameters);
    for (i = 0; i < numOfParameters; ++i) {
      NSString *paramName = [self nameOfParameter: i];
      param = [aModel objectForKey: paramName];
      CHECK_PARAM(param, [paramName UTF8String]);
      parameterValues[i] = [param doubleValue];
    }
  }

  return self;
}

- (void)dealloc
{
  if (speciesGrid) free(speciesGrid);
  if (speciesF1) free(speciesF1);
  if (speciesF2) free(speciesF2);
  if (speciesGradient) free(speciesGradient);
  if (speciesDiff) free(speciesDiff);
  
  [grid release];
  
  [super dealloc];
}

- (void)setupData
{
  int k;
  
  // create spatial grids
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [grid newGridWithName: s atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ F1", s]
                  atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ F2", s]
                  atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ gradient", s]
                  atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ diff", s]
                  atScale: 1 withEncode: dataEncode];
    [grid newGridWithName: [NSString stringWithFormat: @"%@ state", s]
                  atScale: 1 withEncode: dataEncode];
  }
  
  // cache access to the spatial matrices
  speciesGrid = malloc(sizeof(void *) * numOfSpecies);
  speciesF1 = malloc(sizeof(void *) * numOfSpecies);
  speciesF2 = malloc(sizeof(void *) * numOfSpecies);
  speciesGradient = malloc(sizeof(void *) * numOfSpecies);
  speciesDiff = malloc(sizeof(void *) * numOfSpecies);
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    speciesGrid[k] = [[[grid gridWithName: s] objectForKey: @"matrix"] getMatrix];
    speciesF1[k] = [[[grid gridWithName: [NSString stringWithFormat: @"%@ F1", s]]
                     objectForKey: @"matrix"] getMatrix];
    speciesF2[k] = [[[grid gridWithName: [NSString stringWithFormat: @"%@ F2", s]]
                     objectForKey: @"matrix"] getMatrix];
    speciesGradient[k] = [[[grid gridWithName: [NSString stringWithFormat: @"%@ gradient", s]]
                           objectForKey: @"matrix"] getMatrix];
    speciesDiff[k] = [[[grid gridWithName: [NSString stringWithFormat: @"%@ diff", s]]
                       objectForKey: @"matrix"] getMatrix];
  }
}

- (void)setupDataFilesWithState: (int)aState
{
  int k;
  
  dataFileState = aState;
  switch (dataFileState) {
    case BCMODEL_NOFILE_STATE:
      // no data files setup
      break;
    case BCMODEL_WRITE_STATE:
    {
      // data files for writing
      dataFiles = malloc(sizeof(FILE *) * numOfSpecies);
      for (k = 0; k < numOfSpecies; ++k) {
        id s = [species objectAtIndex: k];
        NSString *fileName = [NSString stringWithFormat: @"%@_%@_%@.dat", [self modelName], s, outputSuffix];
        dataFiles[k] = fopen([fileName UTF8String], "w");
        if (dataFiles[k] == NULL) {
          NSLog(@"Cannot open file: %@", fileName);
          exit(0);
        }
      }
      break;
    }
    case BCMODEL_READ_STATE:
      // data file for reading
      break;
    case BCMODEL_APPEND_AND_CLOSE_STATE:
    {
      // data file for append and close to prevent too many files open
      // open then close to truncate file
      dataFiles = malloc(sizeof(FILE *) * numOfSpecies);
      for (k = 0; k < numOfSpecies; ++k) {
        id s = [species objectAtIndex: k];
        NSString *fileName = [NSString stringWithFormat: @"%@_%@_%@.dat", [self modelName], s, outputSuffix];
        dataFiles[k] = fopen([fileName UTF8String], "w");
        if (dataFiles[k] == NULL) {
          NSLog(@"Cannot open file: %@", fileName);
          exit(0);
        }
        fclose(dataFiles[k]);
      }
      break;
    }
  }
}

- (void)initializeSimulation
{
  int i, j, k;
  
  switch (encodeTag) {
    case BCENCODE_INT:
      for (k = 0; k < numOfSpecies; ++k) {
        int (*sGrid)[gridSize][gridSize];
        sGrid = speciesGrid[k];
        int (*sDiff)[gridSize][gridSize];
        sDiff = speciesDiff[k];
        
        for (i = 0; i < gridSize; ++i)
          for (j = 0; j < gridSize; ++j) {
            (*sGrid)[i][j] = 1.0;
            (*sDiff)[i][j] = (*sGrid)[i][j];
          }
      }
      break;
    case BCENCODE_LONG:
      for (k = 0; k < numOfSpecies; ++k) {
        long (*sGrid)[gridSize][gridSize];
        sGrid = speciesGrid[k];
        long (*sDiff)[gridSize][gridSize];
        sDiff = speciesDiff[k];
        
        for (i = 0; i < gridSize; ++i)
          for (j = 0; j < gridSize; ++j) {
            (*sGrid)[i][j] = 1.0;
            (*sDiff)[i][j] = (*sGrid)[i][j];
          }
      }
      break;
    case BCENCODE_FLOAT:
      for (k = 0; k < numOfSpecies; ++k) {
        float (*sGrid)[gridSize][gridSize];
        sGrid = speciesGrid[k];
        float (*sDiff)[gridSize][gridSize];
        sDiff = speciesDiff[k];
        
        for (i = 0; i < gridSize; ++i)
          for (j = 0; j < gridSize; ++j) {
            (*sGrid)[i][j] = fabs(0.01 * normal_dist_rand(0,1));
            //(*sGrid)[i][j] = 1.0;
            (*sDiff)[i][j] = (*sGrid)[i][j];
          }
      }
      break;
    case BCENCODE_DOUBLE:
      for (k = 0; k < numOfSpecies; ++k) {
        double (*sGrid)[gridSize][gridSize];
        sGrid = speciesGrid[k];
        double (*sDiff)[gridSize][gridSize];
        sDiff = speciesDiff[k];
        
        for (i = 0; i < gridSize; ++i)
          for (j = 0; j < gridSize; ++j) {
            (*sGrid)[i][j] = fabs(0.01 * normal_dist_rand(0,1));
            //(*sGrid)[i][j] = 1.0;
            (*sDiff)[i][j] = (*sGrid)[i][j];
          }
      }
      break;
  }
}

- (BOOL)writeDataWithCheck: (BOOL)aFlag
{
  int k;
  
  // write only if data files setup for write
  if ((dataFileState != BCMODEL_WRITE_STATE) && (dataFileState != BCMODEL_APPEND_AND_CLOSE_STATE)) return NO;
  
  if (dataFileState == BCMODEL_APPEND_AND_CLOSE_STATE) {
    for (k = 0; k < numOfSpecies; ++k) {
      id s = [species objectAtIndex: k];
      NSString *fileName = [NSString stringWithFormat: @"%@_%@_%@.dat", [self modelName], s, outputSuffix];
      dataFiles[k] = fopen([fileName UTF8String], "a");
      if (dataFiles[k] == NULL) {
        NSLog(@"Cannot open file: %@", fileName);
        exit(0);
      }
    }
  }
  
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [grid dumpGrid: s toFile: dataFiles[k]];
  }
  
  if (dataFileState == BCMODEL_APPEND_AND_CLOSE_STATE) {
    for (k = 0; k < numOfSpecies; ++k) fclose(dataFiles[k]);
  }
  
  return YES;
}

@end

//
// Produce GPU code
//
@implementation DiffusionModel (GPU)

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  int k, l;
  int numParams = [self numOfParameters];
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendFormat: @"__constant__ float %@_parameters[%d];\n\n", prefixName, numParams];
  
  // structure for grids
  [GPUCode appendFormat: @"typedef struct _%@_GPUgrids {\n", prefixName];
  [GPUCode appendString: @"  float dt;\n"];
  [GPUCode appendString: @"  float dx;\n"];
  [GPUCode appendString: @"  int width;\n"];
  [GPUCode appendString: @"  int height;\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"  float *%@;\n", s];
    [GPUCode appendFormat: @"  float *%@_in;\n", s];
    [GPUCode appendFormat: @"  float *%@_out;\n", s];
    [GPUCode appendFormat: @"  float *%@_F1;\n", s];
    [GPUCode appendFormat: @"  float *%@_F2;\n", s];
  }
  [GPUCode appendString: @"  size_t pitch;\n"];
  [GPUCode appendString: @"  size_t idx_pitch;\n"];
  [GPUCode appendFormat: @"} %@_GPUgrids;\n\n", prefixName];
  
  [GPUCode appendFormat: @"__constant__ %@_GPUgrids %@_struct[1];\n\n", prefixName, prefixName];
  
  [GPUCode appendString: @"\n// parameters\n"];
  
  for (k = 0; k < numParams; ++k) {
    NSString *paramName = [self nameOfParameter: k];
    [GPUCode appendFormat: @"#define %@ %@_parameters[%d]\n", paramName, prefixName, k];
  }
  
  return GPUCode;
}

- (NSDictionary *)controllerGPUCode: (NSString *)prefixName
{
  int k;
  int numParams = [self numOfParameters];
  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;
  
  //
  // Headers and defines
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];
  
  //
  // Allocation
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"*%@_allocGPUKernel(void *model, float dt, float dx, float *hostParameters, int width, int height)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"allocation function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *grids = (%@_GPUgrids *)malloc(sizeof(%@_GPUgrids));\n", prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n // Save parameters\n"];
  [GPUCode appendString: @" grids->dt = dt;\n"];
  [GPUCode appendString: @" grids->dx = dx;\n"];
  [GPUCode appendString: @" grids->width = width;\n"];
  [GPUCode appendString: @" grids->height = height;\n"];
  [GPUCode appendString: @"\n // Allocate device memory\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@), &(grids->pitch), width * sizeof(float), height);\n", s];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@_in), &(grids->pitch), width * sizeof(float), height);\n", s];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@_out), &(grids->pitch), width * sizeof(float), height);\n", s];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@_F1), &(grids->pitch), width * sizeof(float), height);\n", s];
    [GPUCode appendFormat: @" cudaMallocPitch((void**)&(grids->%@_F2), &(grids->pitch), width * sizeof(float), height);\n", s];
  }
  [GPUCode appendString: @"\n grids->idx_pitch = grids->pitch / sizeof(float);\n"];
  [GPUCode appendString: @"\n return grids;\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"allocation code"];
  
  //
  // Data Transfer
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *p, int aFlag, float *hostParameters, RK2grids *rk2grids)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *grids = (%@_GPUgrids *)p;\n", prefixName, prefixName];
  [GPUCode appendString: @"\n if (aFlag) {\n"];
  [GPUCode appendString: @"    // Copy host memory to device memory\n"];
  [GPUCode appendFormat: @"    cudaMemcpyToSymbol(%@_parameters, hostParameters, %d * sizeof(float), 0, cudaMemcpyHostToDevice);\n", prefixName, numParams];
  [GPUCode appendFormat: @"    cudaMemcpyToSymbol(%@_struct, p, sizeof(%@_GPUgrids), 0, cudaMemcpyHostToDevice);\n", prefixName, prefixName];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"    cudaMemcpy2D(grids->%@, grids->pitch, (const cudaArray *)rk2grids->speciesGrid[%d],\n", s, k];
    [GPUCode appendString: @"       grids->width * sizeof(float), grids->width * sizeof(float), grids->height, cudaMemcpyHostToDevice);\n"];
  }
  [GPUCode appendString: @" } else {\n"];
  // Copy result to host memory
  [GPUCode appendString: @"    // Copy result to host memory\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"    cudaMemcpy2D(rk2grids->speciesGrid[%d], grids->width * sizeof(float), grids->%@, grids->pitch,", k, s];
    [GPUCode appendString: @"       grids->width * sizeof(float), grids->height, cudaMemcpyDeviceToHost);\n"];
  }
  
  [GPUCode appendString: @"}\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];
  
  //
  // Execution
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, RK2grids *rk2grids, double startTime, double nextTime)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" %@_GPUgrids *%@_ptrs = (%@_GPUgrids *)%@_data;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" dim3 %@_threadsPerBlock(16, 16);\n", prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerGrid((%@_ptrs->width + %@_threadsPerBlock.x - 1) / %@_threadsPerBlock.x,", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" (%@_ptrs->height + %@_threadsPerBlock.y - 1) / %@_threadsPerBlock.y);\n", prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @" double %@_currentTime = startTime;\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];
  
  GPUCode = [NSMutableString new];
  //[GPUCode appendFormat: @" while (%@_currentTime < nextTime) {\n", prefixName];
  [GPUCode appendFormat: @"   %@_kernel_F1<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"   %@_kernel_F2<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @"   %@_currentTime += %@_grids->dt;\n", prefixName, prefixName];
  //[GPUCode appendFormat: @" };\n"];
  
  //  for (k = 0; k < numOfSpecies; ++k) {
  //    id s = [species objectAtIndex: k];
  //    [GPUCode appendFormat: @"   cudaMemcpy2D(%@_grids->%@, %@_grids->pitch, %@_grids->%@_F2,", prefixName, s, prefixName, prefixName, s];
  //    [GPUCode appendFormat: @" %@_grids->pitch, %@_grids->width * sizeof(float), %@_grids->height, cudaMemcpyDeviceToDevice);\n", prefixName, prefixName, prefixName];
  //  }
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];
  
  //
  // Release
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_releaseGPUKernel(void *model, void *g)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"release function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" %@_GPUgrids *grids = (%@_GPUgrids *)g;\n", prefixName, prefixName];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @" cudaFree(grids->%@);\n", s];
    [GPUCode appendFormat: @" cudaFree(grids->%@_F1);\n", s];
    [GPUCode appendFormat: @" cudaFree(grids->%@_F2);\n", s];
  }
  [GPUCode appendFormat: @" free(grids);\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"release code"];
  
  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  [GPUCode appendFormat: @"BioSwarmGPUFunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];
  
  return GPUDictionary;  
}

- (NSMutableString *)diffusionKernelGPUCode: (NSString *)prefixName
{
  int k;
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  id spatialModel = [modelController spatialModel];
  int dimensions = [spatialModel dimensions];
  if (dimensions == 0) dimensions = 1;
  
  [GPUCode appendString: @"__device__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_diffusion(int i, int j, float mh, float mf)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"  // periodic boundary conditions\n"];
  [GPUCode appendString: @"  int iplus = i + 1;\n"];
  [GPUCode appendFormat: @"  if (iplus == %@_struct->width) iplus = 0;\n", prefixName];
  [GPUCode appendString: @"  int iminus = i - 1;\n"];
  [GPUCode appendFormat: @"  if (iminus < 0) iminus = %@_struct->width-1;\n", prefixName];
  [GPUCode appendString: @"  int jplus = j + 1;\n"];
  [GPUCode appendFormat: @"  if (jplus == %@_struct->height) jplus = 0;\n", prefixName];
  [GPUCode appendString: @"  int jminus = j - 1;\n"];
  [GPUCode appendFormat: @"  if (jminus < 0) jminus = %@_struct->height-1;\n", prefixName];
  [GPUCode appendString: @"  float up, down, left, right, gradient;\n"];
  [GPUCode appendString: @"  float F_val;\n"];
  
  for (k = 0; k < numOfSpecies; ++k) {
    //if (interactionsCount[k] == 0) continue;
    id s = [species objectAtIndex: k];
    
    [GPUCode appendFormat: @"\n   // %@\n", s];
    [GPUCode appendString: @"  // Laplace gradients\n"];                                                                                                                             
    [GPUCode appendFormat: @"  up = %@_struct->%@[jplus*%@_struct->idx_pitch+i] + mf * %@_struct->%@_F1[jplus*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"  down = %@_struct->%@[jminus*%@_struct->idx_pitch+i] + mf * %@_struct->%@_F1[jminus*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"  left = %@_struct->%@[j*%@_struct->idx_pitch+iminus] + mf * %@_struct->%@_F1[j*%@_struct->idx_pitch+iminus];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"  right = %@_struct->%@[j*%@_struct->idx_pitch+iplus] + mf * %@_struct->%@_F1[j*%@_struct->idx_pitch+iplus];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendString: @"  gradient = up + down + left + right;\n"];
    [GPUCode appendFormat: @"  F_val = %@_struct->%@[j*%@_struct->idx_pitch+i] + mf * %@_struct->%@_F1[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"  %@_struct->%@_out[j*%@_struct->idx_pitch+i] += mh * (%@_diffusion * gradient - 4.0f * %@_diffusion * F_val)/(%@_struct->dx*%@_struct->dx);\n", prefixName, s, prefixName, s, s, prefixName, prefixName];
  }
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  int k;
  //int numParams = [self numOfParameters];
  
  NSMutableString *GPUCode = [NSMutableString new];
  
  // kernel to calculate diffusion
  [GPUCode appendString: [self diffusionKernelGPUCode: prefixName]];
  
  // 2nd order Runge-Kutta
  
  // F1 kernel
  
  [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F1(float currentTime, float finalTime)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int i = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int j = blockIdx.y * blockDim.y + threadIdx.y;\n"];
  
  [GPUCode appendString: @"\n   // check thread in boundary\n"];
  [GPUCode appendFormat: @"   if (i >= %@_struct->width) return;\n", prefixName];
  [GPUCode appendFormat: @"   if (j >= %@_struct->height) return;\n", prefixName];
  
  [GPUCode appendString: @"\n   // calculate F1 for reactions\n"];
  [GPUCode appendFormat: @"   %@_kernel_function(i, j, %@_struct->dt, 0.0f);\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendString: @"\n   // calculate F1 for diffusion\n"];
  [GPUCode appendFormat: @"   %@_kernel_diffusion(i, j, %@_struct->dt, 0.0f);\n", prefixName, prefixName, prefixName];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   %@_struct->%@_F1[j*%@_struct->idx_pitch+i] = %@_struct->%@_out[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
  }
  [GPUCode appendString: @"}\n"];
  
  // F2 kernel
  
  [GPUCode appendString: @"\n// F2 kernel for 2nd order Runge-Kutta\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_kernel_F2(float currentTime, float finalTime)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendString: @"   int i = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int j = blockIdx.y * blockDim.y + threadIdx.y;\n"];
  
  [GPUCode appendString: @"\n   // check thread in boundary\n"];
  [GPUCode appendFormat: @"   if (i >= %@_struct->width) return;\n", prefixName];
  [GPUCode appendFormat: @"   if (j >= %@_struct->height) return;\n", prefixName];
  
  [GPUCode appendString: @"\n   // calculate F2 for reactions\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   %@_struct->%@_in[j*%@_struct->idx_pitch+i] = %@_struct->%@_F1[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
  }
  [GPUCode appendFormat: @"   %@_kernel_function(i, j, %@_struct->dt, 0.5f);\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendString: @"\n   // calculate F2 for diffusion\n"];
  [GPUCode appendFormat: @"   %@_kernel_diffusion(i, j, %@_struct->dt, 0.5f);\n", prefixName, prefixName, prefixName];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   %@_struct->%@_F2[j*%@_struct->idx_pitch+i] = %@_struct->%@_out[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
  }
  
  [GPUCode appendString: @"\n   // calculate x(t + dt)\n"];
  for (k = 0; k < numOfSpecies; ++k) {
    id s = [species objectAtIndex: k];
    [GPUCode appendFormat: @"   %@_struct->%@[j*%@_struct->idx_pitch+i] += %@_struct->%@_F2[j*%@_struct->idx_pitch+i];\n", prefixName, s, prefixName, prefixName, s, prefixName];
    [GPUCode appendFormat: @"   if (%@_struct->%@[j*%@_struct->idx_pitch+i] < 0.0f) %@_struct->%@[j*%@_struct->idx_pitch+i] = 0.0f;\n", prefixName, s, prefixName, prefixName, s, prefixName];
  }
  [GPUCode appendString: @"}\n"];
  
  return GPUCode;
}

- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName
{
  return nil;
}

@end

//
// Run GPU code
//
@implementation DiffusionModel (GPURun)

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions
{
  int i, k;
  ModelGPUData *gpuData = (ModelGPUData *)malloc(sizeof(ModelGPUData));
  
  gpuData->data = (RK2grids *)malloc(sizeof(RK2grids) * numModels);
  for (i = 0; i < numModels; ++i) {
    RK2grids *g = (RK2grids *)gpuData->data;
    g[i].speciesCount = numOfSpecies;
    g[i].speciesGrid = malloc(sizeof(void *) * numOfSpecies);
    for (k = 0; k < numOfSpecies; ++k) g[i].speciesGrid[k] = NULL;
  }
  
  gpuData->numModels = numModels;
  gpuData->numParams = [self numOfParameters];
  gpuData->gpuFunctions = gpuFunctions;
  
  gpuData->parameters = (float *)malloc(sizeof(float) * numModels * gpuData->numParams);
  
  return gpuData;
}

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int j;
  
  RK2grids *g = (RK2grids *)gpuData->data;
  if (aFlag)
    for (j = 0; j < numOfSpecies; ++j) g[aNum].speciesGrid[j] = speciesGrid[j];
  
  /* data automatically transferred from GPU to individual */
}

- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  return [super assignParameters: data ofNumber: aNum toGPU: aFlag];
}

- (void)allocGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RK2functions *gpuFunctions = (RK2functions *)gpuData->gpuFunctions;
  gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, (float)dt, (float)dx, gpuData->parameters, gridSize, gridSize);
}

- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RK2functions *gpuFunctions = (RK2functions *)gpuData->gpuFunctions;
  RK2grids *g = (RK2grids *)gpuData->data; // g is a really an array of RK2grids
  (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, aFlag, gpuData->parameters, g);
}

- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RK2functions *gpuFunctions = (RK2functions *)gpuData->gpuFunctions;
  (gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, gpuData->data, currentTime, endTime);
}

- (void)releaseGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  RK2functions *gpuFunctions = (RK2functions *)gpuData->gpuFunctions;
  (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
}

@end
