/*
 ComputationalBiologyModel.h
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "BiologicalModel.h"
#import "SimulationController.h"

@interface ComputationalBiologyModel : NSObject <BiologicalModel>
{
  id delegate;
  
  // model
  NSString *name;
  id modelController;
  NSDictionary *model;

  // model hierarchy
  id parentModel;
  NSMutableArray *childModels;
  NSArray *spatialContext;

  BOOL isDebug;
  BOOL isBinaryFile;
  int runNumber;
  
  // for random search
  double *paramMin;
  double *paramMax;  
}

- initWithModel: (NSDictionary *)aModel andController: anObj;
- initWithModel: (NSDictionary *)aModel andController: anObj andParent: aParent;

- newFromModelIndividual;

- (NSDictionary *)model;
- (NSString *)modelName;
- (id)modelController;
- (id)parentModel;
- (NSMutableArray *)childModels;
- (void)addChildModel: aModel;

- (SpatialModel *)spatialModel;
- (SpatialModel *)spatialModelForContext: aModel;

- (int)numOfSpecies;
- (NSString *)nameOfSpecies: (int)theSpecies;
- (id)valueOfSpecies: (int)theSpecies;

- (id)delegate;
- (void)setDelegate: anObj;

- (BOOL)isDebug;
- (void)setDebug: (BOOL)aFlag;

- (BOOL)isBinaryFile;
- (void)setBinaryFile: (BOOL)aFlag;

- (void)setRunNumber: (int)aNum;
- (int)runNumber;

// manage simulation
// pass messages down the model hierarchy
- (int)numModelsInHierarchy;
- (void)setupDataFilesWithState: (int)aState;
- (void)allocateHierarchyWithEncode:(NSString *)anEncode;
- (void)initializeHierarchy;
- (BOOL)writeHierarchyDataWithCheck: (BOOL)aFlag;
- (BOOL)readHierarchyData;
- (void)rewindHierarchyFiles;
- (void)runSimulation;
- (void)cleanupHierarchy;

// these are implemented by subclasses
- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState;
- (void)initializeSimulation;
- (BOOL)writeDataWithCheck: (BOOL)aFlag;
- (BOOL)readData;
- (void)rewindFiles;
- (void)cleanupSimulation;

// parameter operations
- (int)numOfParameters;
- (NSString *)nameOfParameter: (int)theParam;

- (BOOL)setupParameterRanges: (NSDictionary *)ranges doCheck: (BOOL)aFlag;
- (BOOL)setupParameterRanges: (NSDictionary *)ranges checkNames: (NSArray *)names;
- (double)randomValueforParameter: (int)aParam;
- (double)perturbValueForParameter: (int)aParam withinRange: (BOOL)aFlag standardDeviation: (double)SD;
- (int)modifyRandomParameter: (NSMutableDictionary *)params;
- (double *)parameterMinRange;
- (double *)parameterMaxRange;

// perturbation operations
// These should be implemented by subclasses
- (BOOL)setValueForAllSpecies: (double)aValue;
- (BOOL)setValue: (double)aValue forSpecies: (NSString *)speciesName;
- (BOOL)adjustValue: (double)aValue forSpecies: (NSString *)speciesName;
- (BOOL)knockoutSpecies: (NSString *)speciesName;

@end

//
// Generate GPU code
//

@interface ComputationalBiologyModel (GPU) <BiologicalModelGPU>
- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName;
@end

//
// Run GPU
//

@interface ComputationalBiologyModel (GPURun) <BiologicalModelGPURun>

// pass messages down the model hierarchy
- (void)allocateHierarchy: (int *)modelIndex GPUData: (ModelGPUData **)gpuData modelInstances: (int)numInstances;
- (void)freeHierarchy: (int *)modelIndex GPUData: (ModelGPUData **)gpuData;
- (void)assignHierarchyData: (int *)modelIndex GPUData: (ModelGPUData **)gpuData ofInstance: (int)aNum toGPU: (BOOL)aFlag;
- (void)assignHierarchyParameters: (int *)modelIndex GPUData: (ModelGPUData **)gpuData ofInstance: (int)aNum toGPU: (BOOL)aFlag;
- (void)transferHierarchy: (int *)modelIndex GPUData: (ModelGPUData **)gpuData toGPU: (BOOL)aFlag;
- (void)releaseHierarchy: (int *)modelIndex GPUData: (ModelGPUData **)gpuData;

// these are implemented by subclasses
- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions;
- (void)freeGPUData: (void *)data;

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag;

- (void)allocGPUKernel: (void *)data;
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag;
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime;
- (void)releaseGPUKernel: (void *)data;
@end

//
// Visualization
//

@interface ComputationalBiologyModel (Povray) <BiologicalModelPovray>
- (NSMutableString *)povrayCode;
@end
