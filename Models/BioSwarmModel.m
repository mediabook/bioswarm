/*
 BioSwarmModel.m
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: April 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BioSwarmModel.h"
#import "BioSwarmController.h"
#import "Grid2DArray.h"

#import "internal.h"

// encode strings
static NSString *idEncode = nil;
static NSString *intEncode = nil;
static NSString *floatEncode = nil;
static NSString *doubleEncode = nil;
static NSString *longEncode = nil;
static NSString *boolEncode = nil;

@implementation BioSwarmModel

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj
{
  return [self initWithModel: aModel andKey: aKey andController: anObj andParent: nil];
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  id s;
  
  [super init];
  
  delegate = nil;
  modelController = [anObj retain];
  model = [aModel retain];
  keyName = [aKey retain];
  parentModel = aParent;
  childModels = [NSMutableArray new];
  spatialContext = [aModel objectForKey: @"spatialContext"];
  //printf("parent child spatial: %p %p %p\n", parentModel, childModels, spatialContext);
  
  // setup species
  species = [aModel objectForKey: @"species"];
  numOfSpecies = [species count];
  //printf("%d species\n", numOfSpecies);
  speciesData = NULL;

  isDebug = NO;
  s = [aModel objectForKey: @"debug"];
  if (s) isDebug = [s boolValue];
  
  isBinaryFile = [modelController isBinaryFile];
  s = [aModel objectForKey: @"binaryFile"];
  if (s) isBinaryFile = [s boolValue];
  
  name = [model objectForKey: @"name"];
  if (!name) name = [modelController modelName];
  CHECK_PARAM(name, "name");

  // numerical scheme
  s = [aModel objectForKey: @"numericalScheme"];
  //CHECK_PARAM(s, "numericalScheme")
  numericalScheme = s;

  // Multiplicity is number of systems to manage.
  // Separate from multiple models which is managed externally.
  // Useful for models which require multiple copies of submodels.
  multiplicity = 1;

  dt = 1.0;
  s = [aModel objectForKey: @"dt"];
  //CHECK_PARAM(s, "dt")
  if (s) dt = [s doubleValue];

  timeScale = 1;
  s = [aModel objectForKey: @"timeScale"];
  if (s) timeScale = [s doubleValue];
  
  runNumber = [modelController outputRun];
  outputSuffix = [modelController outputSuffix];
  
  paramMin = NULL;
  paramMax = NULL;
  
  return self;
}

- newFromModelIndividual
{
  return [[[self class] alloc] initWithModel: model];
}

- (void)dealloc
{
  //printf("BioSwarmModel dealloc\n");
  if (paramMin) free(paramMin);
  if (paramMax) free(paramMax);
  [model release];
  [modelController release];
  //[parentModel release];
  [childModels release];
  [keyName release];
  if (speciesData) free(speciesData);
  
  [super dealloc];
}

- (NSDictionary *)model { return model; }
- (NSString *)modelName { return name; }
- (id)modelController { return modelController; }
- (id)parentModel { return parentModel; }
- (NSMutableArray *)childModels { return childModels; }
- (void)addChildModel: aModel { [childModels addObject: aModel]; }
- (int)modelNumber { return modelNumber; }
- (void)setModelNumber: (int)aNum { modelNumber = aNum; }

- modelWithNumber: (int)aNum
{
  if (modelNumber == aNum) return self;

  int i;
  for (i = 0; i < [childModels count]; ++i) {
    id aModel = [[childModels objectAtIndex: i] modelWithNumber: aNum];
    if (aModel) return aModel;
  }
  
  return nil;
}

- (NSString *)keyName { return keyName; }

- modelWithKey: (NSString *)aKey
{
  if ([keyName isEqualToString: aKey]) return self;
  
  int i;
  for (i = 0; i < [childModels count]; ++i) {
    id aModel = [[childModels objectAtIndex: i] modelWithKey: aKey];
    if (aModel) return aModel;
  }
  
  return nil;
}

- (int)numOfModelObjects { return 1; }

- (SpatialModel *)spatialModel
{
  // by default, ask our parent, if no parent then ask controller
  if (parentModel) return [parentModel spatialModel];
  else return [modelController spatialModel];
}
- (SpatialModel *)spatialModelForContext: aModel
{
  if (!spatialContext) return [self spatialModel];
  else {
    NSUInteger i = [childModels indexOfObject: aModel];
    if (i == NSNotFound) {
      printf("ERROR: attempt to get spatial model for invalid context.\n");
      return nil;
    }
    NSString *s = [spatialContext objectAtIndex: i];
    printf("spatial context: %ld %s\n", (unsigned long)i, [s UTF8String]);
    if ([s isEqualToString: @"parent"]) return [parentModel spatialModel];
    else if ([s isEqualToString: @"nil"]) return nil;
  }
  return nil;
}

- (id)modelOfTypeClass: (Class)aClass
{
  int i;

  if ([self isKindOfClass: aClass]) return self;

  for (i = 0; i < [childModels count]; ++i) {
    id m = [[childModels objectAtIndex: i] modelOfTypeClass: aClass];
    if (m) return m;
  }
  
  return nil;
}

- (void)setMultiplicity: (int)aNum { multiplicity = aNum; }
- (int)multiplicity { return multiplicity; }

- delegate { return delegate; }
- (void)setDelegate: anObj { delegate = anObj; }

- (int)numOfSpecies { return numOfSpecies; }
- (NSString *)nameOfSpecies: (int)theSpecies { return [species objectAtIndex: theSpecies]; }
- (id)valueOfSpecies: (int)theSpecies withMultiplicity: (int)aNum
{
  if (!numOfSpecies) return nil;

  switch (encodeTag) {
    case 1: {
      int *sData = speciesData;
      return [NSNumber numberWithInt: sData[theSpecies]];
      break;
    }
    case 2: {
      long *sData = speciesData;
      return [NSNumber numberWithLong: sData[theSpecies]];
      break;
    }
    case 3: {
      float *sData = speciesData;
      return [NSNumber numberWithFloat: sData[theSpecies]];
      break;
    }
    case 4: {
      double *sData = speciesData;
      return [NSNumber numberWithDouble: sData[theSpecies]];
      break;
    }
  }
  return nil;
}

- (BOOL)isDebug
{
  // the controller may globally turn on debugging
  //if (!isDebug) return [modelController isDebug];
  return isDebug;
}
- (void)setDebug: (BOOL)aFlag { isDebug = aFlag; }

- (BOOL)isBinaryFile { return isBinaryFile; }
- (void)setBinaryFile: (BOOL)aFlag { isBinaryFile = aFlag; }

- (void)setRunNumber: (int)aNum { runNumber = aNum; }
- (int)runNumber { return runNumber; }

- (double)dt { return dt; }

#if 0
#pragma mark == MANAGE MODELS AND SIMULATIONS ==
#endif

//
// pass messages down the model hierarchy
//

- (int)numModelsInHierarchy
{
  int i, cnt = 1;
  for (i = 0; i < [childModels count]; ++i)
    cnt += [[childModels objectAtIndex: i] numModelsInHierarchy];
    return cnt;
}

static int gcd(int a, int b)
{
  if (b == 0) return a;
  if (a == b) return a;
  return gcd(b, a % b);
}

- (double)smallestDTInHierarchy
{
  int i;
  
  double sdt = dt;
  for (i = 0; i < [childModels count]; ++i) {
    double cdt = [[childModels objectAtIndex: i] smallestDTInHierarchy];
    if (cdt < sdt) sdt = cdt;
  }
  
  return sdt;
}

- (int)largestTimeScaleInHierarchy
{
  int i;

  int lts = 0;
  for (i = 0; i < [childModels count]; ++i) {
    int ts = [[childModels objectAtIndex: i] largestTimeScaleInHierarchy];
    if (ts > lts) lts = ts;
  }
  if (timeScale > lts) lts = timeScale;
  
  return lts;
}

- (int)actionTimeScale
{
  int lts = [modelController largestTimeScale];
  int ats = gcd(lts, timeScale);
  printf("gcd(%d,%d) = %d, %d\n", lts, timeScale, ats, lts / ats);
  return lts / ats;
}

- (void)performHierarchyInvocation: (NSInvocation *)anInvocation
{
  int i;
  if ([self respondsToSelector: [anInvocation selector]]) [anInvocation invokeWithTarget: self];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] performHierarchyInvocation: anInvocation];
}

- (void)setupHierarchyDataFilesWithState: (int)aState
{
  int i;
  [self setupDataFilesWithState: aState];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] setupHierarchyDataFilesWithState: aState];
}

- (void)allocateHierarchyWithEncode:(NSString *)anEncode
{
  int i;

  if ([anEncode isEqual: [BioSwarmModel intEncode]]) {
    encodeTag = BCENCODE_INT;
  } else if ([anEncode isEqual: [BioSwarmModel longEncode]]) {
    encodeTag = BCENCODE_LONG;
  } else if ([anEncode isEqual: [BioSwarmModel floatEncode]]) {
    encodeTag = BCENCODE_FLOAT;
  } else if ([anEncode isEqual: [BioSwarmModel doubleEncode]]) {
    encodeTag = BCENCODE_DOUBLE;
  } else {
    // throw exception or something
    NSLog(@"ERROR: BioSwarmModel unsupported encoding %@\n", anEncode);
    return;
  }
  dataEncode = [anEncode retain];

  if (numOfSpecies) {
    switch (encodeTag) {
      case BCENCODE_INT:
        speciesData = malloc(sizeof(int) * numOfSpecies);
        for (i = 0; i < numOfSpecies; ++i) ((int *)speciesData)[i] = 0;
        break;
      case BCENCODE_LONG:
        speciesData = malloc(sizeof(long) * numOfSpecies);
        for (i = 0; i < numOfSpecies; ++i) ((long *)speciesData)[i] = 0;
        break;
      case BCENCODE_FLOAT:
        speciesData = malloc(sizeof(float) * numOfSpecies);
        for (i = 0; i < numOfSpecies; ++i) ((float *)speciesData)[i] = 0;
        break;
      case BCENCODE_DOUBLE:
        speciesData = malloc(sizeof(double) * numOfSpecies);
        for (i = 0; i < numOfSpecies; ++i) ((double *)speciesData)[i] = 0;
        break;
    }
  }
  
  [self allocateSimulationWithEncode: anEncode];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] allocateHierarchyWithEncode: anEncode];
}

- (void)initializeHierarchy
{
  int i;
  [self initializeSimulation];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] initializeHierarchy];
}

- (BOOL)writeHierarchyDataWithCheck: (BOOL)aFlag
{
  int i;
  BOOL res = YES;
  res &= [self writeDataWithCheck: aFlag];
  for (i = 0; i < [childModels count]; ++i)
    res &= [[childModels objectAtIndex: i] writeHierarchyDataWithCheck: aFlag];
    return res;
}

- (BOOL)readHierarchyData
{
  int i;
  BOOL res = YES;
  res &= [self readData];
  for (i = 0; i < [childModels count]; ++i)
    res &= [[childModels objectAtIndex: i] readHierarchyData];
    return res;
}

- (void)rewindHierarchyFiles
{
  int i;
  [self rewindFiles];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] rewindHierarchyFiles];
}

- (void)cleanupHierarchy
{
  int i;
  [self cleanupSimulation];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] cleanupHierarchy];
}

//
// These should be implemented by subclasses
//

- (void)setupDataFilesWithState: (int)aState
{
}

- (void)allocateSimulationWithEncode: (NSString *)anEncode
{
}

- (void)initializeSimulation
{
}

- (void)initializeFileCompleted
{
}

- (BOOL)writeDataWithCheck: (BOOL)aFlag
{
  return YES;
}

- (BOOL)readData
{
  return YES;
}

- (void)rewindFiles
{
}

- (void)stepSimulation
{
}

- (void)cleanupSimulation
{
}

//
// Parameter operations
//
- (int)numOfParameters { return numOfParameters; }
- (NSString *)nameOfParameter: (int)theParam { return [parameterNames objectAtIndex: theParam]; }

- (BOOL)hierarchyParameterList:(NSArray *)includeList exclude:(NSArray *)excludeList
                         names:(NSMutableArray *)pNames indexes:(NSMutableArray *)pIndexes
                  modelNumbers:(NSMutableArray *)pModelNumbers
                    modelKeys:(NSMutableArray *)pModelKeys
{
  int i, k;
  int numParams = [self numOfParameters];
  BOOL flag = YES;
  
  if (numParams) {
    for (k = 0; k < numParams; ++k) {
      NSString *pn = [self nameOfParameter: k];
      if (includeList && ([includeList indexOfObject: pn] == NSNotFound)) continue;
      if (excludeList && ([excludeList indexOfObject: pn] != NSNotFound)) continue;
    
      // save parameter info
      if (pNames) [pNames addObject: pn];
      if (pIndexes) [pIndexes addObject: [NSNumber numberWithInt: k]];
      if (pModelNumbers) [pModelNumbers addObject: [NSNumber numberWithInt: modelNumber]];
      if (pModelKeys) [pModelKeys addObject: keyName];
    }

    // make sure parameter ranges are setup
    //if (![self setupParameterRanges: nil checkNames: parameterNames]) flag = NO;
  }
  
  for (i = 0; i < [childModels count]; ++i)
    flag &= [[childModels objectAtIndex: i] hierarchyParameterList: includeList
                                                           exclude: excludeList
                                                             names: pNames
                                                           indexes: pIndexes
                                                      modelNumbers: pModelNumbers
                                                         modelKeys: pModelKeys];

  return flag;
}

- (BOOL)setupParameterRanges: (NSDictionary *)ranges doCheck: (BOOL)aFlag
{
  id paramRanges;
  BOOL good = YES;
  int i, numParams = [self numOfParameters];

  if (numParams) {
    if (!ranges) {
      paramRanges = [model objectForKey: @"parameterRanges"];
      CHECK_PARAMF(paramRanges, "parameterRanges", good);
    } else paramRanges = ranges;
    
    if (paramMin) free(paramMin);
    if (paramMax) free(paramMax);
    paramMin = malloc(sizeof(double) * numParams);
    paramMax = malloc(sizeof(double) * numParams);
    BOOL flag = YES;
    for (i = 0; i < numParams; ++i) {
      NSString *s = [self nameOfParameter: i];
        
      NSString *paramName = [NSString stringWithFormat: @"%@_min", s];
      id param = [paramRanges objectForKey: paramName];
      if (aFlag) CHECK_PARAMF(param, [paramName UTF8String], flag);
      paramMin[i] = [param doubleValue];
        
      paramName = [NSString stringWithFormat: @"%@_max", s];
      param = [paramRanges objectForKey: paramName];
      if (aFlag) CHECK_PARAMF(param, [paramName UTF8String], flag);
      paramMax[i] = [param doubleValue];
    }
    good &= flag;
  }

  return good;
}

- (BOOL)setupParameterRanges: (NSDictionary *)ranges checkNames: (NSArray *)names
{
  id paramRanges;
  BOOL good = YES;
  int i, numParams = [self numOfParameters];

  if (numParams) {
    if (!ranges) {
      paramRanges = [model objectForKey: @"parameterRanges"];
      CHECK_PARAMF(paramRanges, "parameterRanges", good);
    } else paramRanges = ranges;
  
    if (paramMin) free(paramMin);
    if (paramMax) free(paramMax);
    paramMin = malloc(sizeof(double) * numParams);
    paramMax = malloc(sizeof(double) * numParams);
    BOOL flag = YES;
    for (i = 0; i < numParams; ++i) {
      NSString *s = [self nameOfParameter: i];
      
      NSString *paramName = [NSString stringWithFormat: @"%@_min", s];
      id param = [paramRanges objectForKey: paramName];
      if (names) {
        NSUInteger idx = [names indexOfObject: s];
        if (idx != NSNotFound) CHECK_PARAMF(param, [paramName UTF8String], flag);
      }
      paramMin[i] = [param doubleValue];
      
      paramName = [NSString stringWithFormat: @"%@_max", s];
      param = [paramRanges objectForKey: paramName];
      if (names) {
        NSUInteger idx = [names indexOfObject: s];
        if (idx != NSNotFound) CHECK_PARAMF(param, [paramName UTF8String], flag);
      }
      paramMax[i] = [param doubleValue];
    }
    good &= flag;
  }

  for (i = 0; i < [childModels count]; ++i)
    good &= [[childModels objectAtIndex: i] setupParameterRanges: ranges checkNames: names];

  return good;
}

- (double *)parameterMinRange { return paramMin; }
- (double *)parameterMaxRange { return paramMax; }

- (double)randomValueforParameter: (int)aParam
{
  if ((aParam < 0) || (aParam >= [self numOfParameters])) {
    printf("ERROR: Invalid parameter index: %d\n", aParam);
    return 0.0;
  }
  
  // check if parameter ranges are setup
  if (!paramMin)
    if (![self setupParameterRanges: nil doCheck: NO]) {
      printf("ERROR: Could not setup parameter ranges.\n");
      return 0.0;
    }
  if (paramMin[aParam] == paramMax[aParam]) {
    printf("ERROR: Invalid parameter range defined for %s.\n", [[self nameOfParameter: aParam] UTF8String]);
    return 0.0;
  }
  
  // determine scales of range
  int minScale = determine_scale_double(paramMin[aParam]);
  int maxScale = determine_scale_double(paramMax[aParam]);
  
  int signCheck = 0;
  if ((paramMin[aParam] < 0) && (paramMax[aParam] < 0)) signCheck = 1;
    if (((paramMin[aParam] < 0) && (paramMax[aParam] >= 0))
        || ((paramMin[aParam] >= 0) && (paramMax[aParam] < 0))) signCheck = 2;
      
      double newValue = 0.0;
      if (minScale == maxScale) {
        double delta = paramMax[aParam] - paramMin[aParam];
        newValue = RAND_NUM * delta + paramMin[aParam];
      } else {
        do {
          double deltaScale = maxScale - minScale;
          double newScale = RAND_NUM * deltaScale + minScale;
          newScale = round(newScale);
          //printf("newScale: %lf %d %d %lf\n", deltaScale, minScale, maxScale, newScale);
          //double minVal = paramMin[aParam] / pow(10.0, minScale);
          //double maxVal = paramMax[aParam] / pow(10.0, maxScale);
          //double delta = maxVal - minVal;
          //newValue = RAND_NUM * delta + minVal;
          newValue = RAND_NUM;
          if (signCheck == 1) newValue = -newValue;
          if ((signCheck == 2) && (RAND_NUM < 0.5)) newValue = -newValue;
          //printf("newVal: %lf %lf %lf %lf\n", delta, minVal, maxVal, newValue);
          newValue = newValue * pow(10.0, newScale);
        } while ((newValue < paramMin[aParam]) || (newValue > paramMax[aParam]));
      }
  
  return newValue;
}

- (double)perturbValueForParameter: (int)aParam withinRange: (BOOL)aFlag standardDeviation: (double)SD
{
  if ((aParam < 0) || (aParam >= [self numOfParameters])) {
    printf("ERROR: Invalid parameter index: %d\n", aParam);
    return 0.0;
  }
  
  double origValue = [[model objectForKey: [self nameOfParameter: aParam]] doubleValue];
  //NSLog(@"origValue = %lf", origValue);
  double newValue = origValue;
  
  if (aFlag) {
    // check if parameter ranges are setup
    if (!paramMin)
      if (![self setupParameterRanges: nil doCheck: NO]) {
        printf("ERROR: Could not setup parameter ranges.\n");
        return 0.0;
      }
    
    do {
      newValue = origValue + normal_dist_rand(0, SD);
    } while ((newValue < paramMin[aParam]) || (newValue > paramMax[aParam]));
  } else {
    // don't worry about range, just keep same sign
    BOOL isPositive = YES;
    if (origValue < 0) isPositive = NO;
    
    do {
      newValue = origValue + normal_dist_rand(0, SD);
    } while (((newValue < 0) && isPositive) || ((newValue >= 0) && !isPositive));
  }
  
  return newValue;
}

- (int)modifyRandomParameter: (NSMutableDictionary *)params
{
  // check if parameter ranges are setup
  if (!paramMin)
    if (![self setupParameterRanges: nil doCheck: NO]) {
      printf("ERROR: Could not setup parameter ranges.\n");
      return -1;
    }
  
  // how many parameters
  int numParams = [self numOfParameters];
  
  int rp = (int)(RAND_NUM * numParams);
  NSString *ps = [self nameOfParameter: rp];
  
  if (!ps) printf("ERROR: Could not randomly pick parameter: %d %d\n", numParams, rp);
    else {
      double delta = paramMax[rp] - paramMin[rp];
      double pv = [[params objectForKey: ps] doubleValue];
      double newpv = RAND_NUM * delta + paramMin[rp];
      
#if 0
      double scale = determine_scale(pv);
      BOOL done = NO;
      while (!done) {
        newpv = normal_dist_rand(pv, scale);
        // maintain the same sign
        if ((pv < 0) && (newpv < 0)) done = YES;
        if ((pv >= 0) && (newpv >= 0)) done = YES;
      }
#endif
#if 0
      if (fabs(pv) < 0.001) {
        if (pv >= 0) newpv = RAND_NUM;
        else newpv = - RAND_NUM;
      } else {
        if (RAND_NUM < 0.5) newpv = pv + (RAND_NUM * pv);
        else newpv = pv - (RAND_NUM * pv);
      }
#endif
      
      [params setObject: [NSNumber numberWithDouble: newpv] forKey: ps];
      printf("change parameter %s(%d): %lf -> %lf\n", [ps UTF8String], rp, pv, newpv);
    }
  
  return rp;
}

// species operations
- (BOOL)hierarchySpeciesList:(NSMutableArray *)sNames indexes:(NSMutableArray *)sIndexes
                modelNumbers:(NSMutableArray *)sModelNumbers
                   modelKeys:(NSMutableArray *)sModelKeys;
{
  int i, k;
  int numSpecies = [self numOfSpecies];
  BOOL flag = YES;
  
  if (numSpecies) {
    for (k = 0; k < numSpecies; ++k) {
      NSString *pn = [self nameOfSpecies: k];

      // save species info
      if (sNames) [sNames addObject: pn];
      if (sIndexes) [sIndexes addObject: [NSNumber numberWithInt: k]];
      if (sModelNumbers) [sModelNumbers addObject: [NSNumber numberWithInt: modelNumber]];
      if (sModelKeys) [sModelKeys addObject: keyName];
    }
  }
  
  for (i = 0; i < [childModels count]; ++i)
    flag &= [[childModels objectAtIndex: i] hierarchySpeciesList: sNames
                                                         indexes: sIndexes
                                                    modelNumbers: sModelNumbers
                                                       modelKeys: sModelKeys];
  
  return flag;
}

// perturbation operations
- (BOOL)setValueForAllSpecies: (double)aValue { return NO; }
- (BOOL)setValue: (double)aValue forSpecies: (NSString *)speciesName
{
  if (!numOfSpecies) return NO;

  NSUInteger idx = [species indexOfObject: speciesName];
  if (idx == NSNotFound) return NO;
  
  switch (encodeTag) {
    case BCENCODE_INT: {
      int *sData = speciesData;
      sData[idx] = (int)aValue;
      break;
    }
    case BCENCODE_LONG: {
      long *sData = speciesData;
      sData[idx] = (long)aValue;
      break;
    }
    case BCENCODE_FLOAT: {
      float *sData = speciesData;
      sData[idx] = (float)aValue;
      break;
    }
    case BCENCODE_DOUBLE: {
      double *sData = speciesData;
      sData[idx] = aValue;
      break;
    }
  }
  
  return YES;
}

- (BOOL)adjustValue: (double)aValue forSpecies: (NSString *)speciesName { return NO; }
- (BOOL)knockoutSpecies: (NSString *)speciesName { return NO; }
- (BOOL)setValue: (double)aValue forParameter: (NSString *)pName { return NO; }

// Singleton encode strings
+ (NSString *)idEncode
{
	if (!idEncode) idEncode = [[NSString alloc] initWithUTF8String: @encode(id)];
	return idEncode;
}

+ (NSString *)intEncode
{
	if (!intEncode) intEncode = [[NSString alloc] initWithUTF8String: @encode(int)];
	return intEncode;
}

+ (NSString *)longEncode
{
	if (!longEncode) longEncode = [[NSString alloc] initWithUTF8String: @encode(long)];
	return longEncode;
}

+ (NSString *)floatEncode
{
	if (!floatEncode) floatEncode = [[NSString alloc] initWithUTF8String: @encode(float)];
	return floatEncode;
}

+ (NSString *)doubleEncode
{
	if (!doubleEncode) doubleEncode = [[NSString alloc] initWithUTF8String: @encode(double)];
	return doubleEncode;
}

+ (NSString *)boolEncode
{
	if (!boolEncode) boolEncode = [[NSString alloc] initWithUTF8String: @encode(BOOL)];
	return boolEncode;
}

@end

//
// GPU
//

@implementation BioSwarmModel (GPU)
- (NSMutableDictionary *)controllerGPUCode: (NSString *)prefixName { return nil; }
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName { return nil; }
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName { return nil; }
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName { return nil; }

@end

//
// Run GPU
//

@implementation BioSwarmModel (GPURun)

- (void)allocateHierarchyGPUData: (ModelGPUData **)gpuData modelInstances: (int)numInstances
{
  // allocate GPU data for ourself
  getGPUFunctions *func = [modelController gpuFunctions];
  void *gpuFunctions = (func)(modelNumber, 0);
  gpuData[modelNumber] = [self allocGPUData: numInstances withGPUFunctions: gpuFunctions];
  [self allocGPUKernel: gpuData[modelNumber]];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] allocateHierarchyGPUData: gpuData modelInstances: numInstances];
}

- (void)freeHierarchyGPUData: (ModelGPUData **)gpuData
{
  // free GPU data for ourself
  [self freeGPUData: gpuData[modelNumber]];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] freeHierarchyGPUData: gpuData];
}

- (void)assignHierarchyDataGPUData: (ModelGPUData **)gpuData ofInstance: (int)aNum toGPU: (BOOL)aFlag
{
  // assign GPU data for ourself
  [self assignData: gpuData[modelNumber] ofNumber: aNum toGPU: aFlag];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] assignHierarchyDataGPUData: gpuData ofInstance: aNum toGPU: aFlag];
}

- (void)assignHierarchyParametersGPUData: (ModelGPUData **)gpuData ofInstance: (int)aNum toGPU: (BOOL)aFlag
{
  // assign GPU data for ourself
  [self assignParameters: gpuData[modelNumber] ofNumber: aNum toGPU: aFlag];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] assignHierarchyParametersGPUData: gpuData ofInstance: aNum toGPU: aFlag];
}

- (void)transferHierarchyGPUData: (ModelGPUData **)gpuData toGPU: (BOOL)aFlag
{
  // transfer GPU data for ourself
  [self transferGPUKernel: gpuData[modelNumber] toGPU: aFlag];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] transferHierarchyGPUData: gpuData toGPU: aFlag];
}

- (void)invokeHierarchyGPUData: (ModelGPUData **)gpuData currentTime: (double)currentTime endTime: (double)endTime timeScale: (int)currentTimeScale
{
  if (currentTimeScale == timeScale) {
    // invoke GPU kernel for ourself
    [self invokeGPUKernel: gpuData[modelNumber] currentTime: currentTime endTime: endTime];
  }

  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] invokeHierarchyGPUData: gpuData currentTime: currentTime endTime: endTime timeScale: currentTimeScale];
}

- (void)invokeHierarchyGPUData: (ModelGPUData **)gpuData currentTime: (double)currentTime endTime: (double)endTime
{
  // Need to break up the total simulation time into smaller time chunks
  // appropriate for the submodels
  double deltaTime = [modelController smallestDT];
  double newTime = currentTime;
  double newEndTime = newTime + deltaTime;

  BOOL done = NO;
  while (!done) {
    // TODO: need to handle the different time scales for each model
    // right now assume each is on same time scale
    [self invokeHierarchyGPUData: gpuData currentTime: newTime endTime: newEndTime timeScale: timeScale];

    newTime += deltaTime;
    newEndTime = newTime + deltaTime;
    if (TIME_EQUAL(newTime, endTime) || (newTime >= endTime)) done = YES;
  }
}

- (void)releaseHierarchyGPUData: (ModelGPUData **)gpuData
{
  // call release kernel for ourself
  [self releaseGPUKernel: gpuData[modelNumber]];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] releaseHierarchyGPUData: gpuData];
}

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions { return NULL; }
- (void)freeGPUData: (void *)data { }
- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag { }
- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag { return 0; }
- (void)allocGPUKernel: (void *)data { }
- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag { }
- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime { }
- (void)releaseGPUKernel: (void *)data { }

@end

//
// Visualization
//

@implementation BioSwarmModel (Povray)

- (void)povrayHierarchyCode: (NSMutableString *)povrayCode
{
  int i;
  NSString *povray = [self povrayCode];
  if (povray) [povrayCode appendString: povray];

  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] povrayHierarchyCode: povrayCode];
}

- (NSMutableString *)povrayCode
{
  return nil;
}

@end

//
// GPU and MPI
//

@implementation BioSwarmModel (GPUMPI)
- (NSMutableDictionary *)controllerGPUMPICode: (NSString *)prefixName { return [self controllerGPUCode: prefixName]; }
- (NSMutableString *)definitionsGPUMPICode: (NSString *)prefixName { return [self definitionsGPUCode: prefixName]; }
- (NSMutableString *)kernelGPUMPICode: (NSString *)prefixName { return [self kernelGPUCode: prefixName]; }
- (NSMutableString *)cleanupGPUMPICode: (NSString *)prefixName { return [self cleanupGPUCode: prefixName]; }
@end

@implementation BioSwarmModel (GPUMPIRun)

- (void)allocateHierarchyGPUMPIData: (ModelGPUData **)gpuData modelInstances: (int)numInstances
{
  // we assume GPU data already allocated
  [self allocGPUMPIKernel: gpuData[modelNumber]];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] allocateHierarchyGPUMPIData: gpuData modelInstances: numInstances];
}

- (void)freeHierarchyGPUMPIData: (ModelGPUData **)gpuData
{
  // free GPU data for ourself
  [self freeGPUMPIData: gpuData[modelNumber]];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] freeHierarchyGPUMPIData: gpuData];
}

- (void)transferHierarchyGPUMPIData: (ModelGPUData **)gpuData toGPU: (BOOL)aFlag
{
  // transfer GPU data for ourself
  [self transferGPUMPIKernel: gpuData[modelNumber] toGPU: aFlag];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] transferHierarchyGPUMPIData: gpuData toGPU: aFlag];
}

- (void)releaseHierarchyGPUMPIData: (ModelGPUData **)gpuData
{
  // call release kernel for ourself
  [self releaseGPUMPIKernel: gpuData[modelNumber]];
  
  // propogate message down model hierarchy
  int i;
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] releaseHierarchyGPUMPIData: gpuData];
}

- (void)allocGPUMPIKernel: (void *)data { return; }
- (void)transferGPUMPIKernel: (void *)data toGPU: (BOOL)aFlag { return; }
- (void)invokeGPUMPIKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime { return; }
- (void)releaseGPUMPIKernel: (void *)data { return; }

- (void)initiateGPUMPIData: (void *)data mode: (int)aMode { return; }
- (void)finalizeGPUMPIData: (void *)data mode: (int)aMode { return; }
@end

//
// Common functions
//

// Generate normally distributed random number
double normal_dist_rand(double mean, double sd)
{
  double fac, radius, v1, v2;
  double rd1Value, rd2Value;
  
  do {
    rd1Value = RAND_NUM;
    rd2Value = RAND_NUM;
    v1 = (2.0 * rd1Value) - 1.0;
    v2 = (2.0 * rd2Value) - 1.0;
    radius = v1*v1 + v2*v2; 
  } while (radius >= 1.0);
  fac = sqrt (-2.0 * log (radius) / radius);
  return v2 * fac * sd + mean;    // use fixed params
}

int determine_scale_double(double value)
{
  double v;
  int scale = 0;
  
  if (value < 0) v = -value;
  else v = value;
  
  if (v == 0.0) return scale;
  
  if (v >= 10) {
    while (v >= 10) {
      v = v / 10;
      ++scale;
    }
  } else {
    while (v < 1) {
      v = v * 10;
      --scale;
    }
  }
  
  //NSLog(@"%lf %d\n", value, scale);
  return scale;
}

int determine_scale_float(float value)
{
  float v;
  int scale = 0;
  
  if (value < 0) v = -value;
  else v = value;
  
  if (v == 0.0) return scale;
  
  if (v >= 10) {
    while (v >= 10) {
      v = v / 10;
      ++scale;
    }
  } else {
    while (v < 1) {
      v = v * 10;
      --scale;
    }
  }
  
  return scale;
}

// which way to round error correction based upon direction
// and the coordinate dimension
BOOL direction_round(int direction, int dim)
{
  BOOL roundUp = NO; // round down for default

  switch (direction) {
  case S_LEFT:
    roundUp = NO;
    break;
  case S_RIGHT:
    if (dim == 0) roundUp = YES;
    break;
  case S_UP:
    if (dim == 1) roundUp = YES;
    break;
  case S_DOWN:
    roundUp = NO;
    break;
  case S_LEFT_UP:
    if (dim == 1) roundUp = YES;
    break;
  case S_RIGHT_UP:
    if (dim == 0) roundUp = YES;
    if (dim == 1) roundUp = YES;
    break;
  case S_LEFT_DOWN:
    roundUp = NO;
    break;
  case S_RIGHT_DOWN:
    if (dim == 0) roundUp = YES;
    break;
  case S_FRONT:
    roundUp = NO;
    break;
  case S_BACK:
    if (dim == 2) roundUp = YES;
    break;
  case S_LEFT_FRONT:
    roundUp = NO;
    break;
  case S_RIGHT_FRONT:
    if (dim == 0) roundUp = YES;
    break;
  case S_LEFT_BACK:
    if (dim == 2) roundUp = YES;
    break;
  case S_RIGHT_BACK:
    if (dim == 0) roundUp = YES;
    if (dim == 2) roundUp = YES;
    break;
  case S_UP_FRONT:
    if (dim == 1) roundUp = YES;
    break;
  case S_DOWN_FRONT:
    roundUp = NO;
    break;
  case S_UP_BACK:
    if (dim == 1) roundUp = YES;
    if (dim == 2) roundUp = YES;
    break;
  case S_DOWN_BACK:
    if (dim == 2) roundUp = YES;
    break;
  case S_LEFT_UP_FRONT:
    if (dim == 1) roundUp = YES;
    break;
  case S_RIGHT_UP_FRONT:
    if (dim == 0) roundUp = YES;
    if (dim == 1) roundUp = YES;
    break;
  case S_LEFT_DOWN_FRONT:
    roundUp = NO;
    break;
  case S_RIGHT_DOWN_FRONT:
    if (dim == 0) roundUp = YES;
    break;
  case S_LEFT_UP_BACK:
    if (dim == 1) roundUp = YES;
    if (dim == 2) roundUp = YES;
    break;
  case S_RIGHT_UP_BACK:
    if (dim == 0) roundUp = YES;
    if (dim == 1) roundUp = YES;
    if (dim == 2) roundUp = YES;
    break;
  case S_LEFT_DOWN_BACK:
    if (dim == 2) roundUp = YES;
    break;
  case S_RIGHT_DOWN_BACK:
    if (dim == 0) roundUp = YES;
    if (dim == 2) roundUp = YES;
    break;
  }

  return roundUp;
}

//
// Simple profiling function
//

static time_t bc_profile_time = 0;

void bc_record_time(const char *msg)
{
  time_t delta = 0;
  time_t current = time(NULL);

  if (!bc_profile_time) {
    bc_profile_time = time(NULL);
    current = bc_profile_time;
  } else {
    delta = current - bc_profile_time;
    bc_profile_time = current;
  }

  printf("TIME: (%d secs elapsed) %s, %s", delta, msg, asctime(localtime(&bc_profile_time)));
}
