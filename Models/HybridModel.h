/*
 HybridModel.h
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "SpatialModel.h"
#import "SimulationController.h"

@interface HybridModel : NSObject
{
  // models
  SimulationController *controller;
  NSDictionary *model;
}

- newFromModelIndividual;
- initWithModel: (NSDictionary *)aModel andController: anObj;
- (NSDictionary *)model;

- (void)allocateSimulationWithEncode: (NSString *)anEncode;
- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState;
- (void)writeData;
- (void)writeDataWithCheck: (BOOL)aFlag;
- (void)initializeSimulation;
- (void)runSimulation;
- (void)cleanupSimulation;

@end

//
// GPU
//
#import "GPUDefines.h"

//
// Produce GPU code
//
@interface HybridModel (GPU)
- (NSMutableString *)controllerGPUCode: (NSString *)prefixName;
- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName;
- (NSMutableString *)kernelGPUCode: (NSString *)prefixName;
- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName;
@end
