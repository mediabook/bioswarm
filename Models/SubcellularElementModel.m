/*
 SubcellularElementModel.m
 
 subcellular element method for cell spatial representation.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "SubcellularElementModel.h"
#import "Grid2DArray.h"
#import "HybridModel.h"
#import "BiologicalModel.h"
#import "BioSwarmController.h"

#import "internal.h"

@implementation SubcellularElementModel

- (void)collectParameters
{
  int i, j, k;
  
  // total up the parameters
  parameterNames = [NSMutableArray new];
  [parameterNames addObject: @"MAX_RANGE_SQ"];
  
  for (i = 0; i < [forces count]; ++i) {
    NSMutableDictionary *d = [forces objectAtIndex: i];
    
    if ([[d objectForKey: @"potential"] isEqualToString: @"D_MORSE"]
        || [[d objectForKey: @"potential"] isEqualToString: @"PD_MORSE"]) {
      [parameterNames addObject: [NSString stringWithFormat: @"%@_epsilon", [d objectForKey: @"name"]]];
      [parameterNames addObject: [NSString stringWithFormat: @"%@_equilibrium", [d objectForKey: @"name"]]];
      [parameterNames addObject: [NSString stringWithFormat: @"%@_interactionMax", [d objectForKey: @"name"]]];
    }
    
    if ([[d objectForKey: @"potential"] isEqualToString: @"MORSE"]
        || [[d objectForKey: @"potential"] isEqualToString: @"PMORSE"]) {
      [parameterNames addObject: [NSString stringWithFormat: @"%@_U0", [d objectForKey: @"name"]]];
      [parameterNames addObject: [NSString stringWithFormat: @"%@_U1", [d objectForKey: @"name"]]];
      [parameterNames addObject: [NSString stringWithFormat: @"%@_ETA0", [d objectForKey: @"name"]]];
      [parameterNames addObject: [NSString stringWithFormat: @"%@_ETA1", [d objectForKey: @"name"]]];
    }
  }
  numOfParameters = [parameterNames count];
}

- initWithModel: (NSDictionary *)aModel andKey: (NSString *)aKey andController: anObj andParent: aParent
{
  NSString *param;
  NSArray *a;
  id pm;
  int i, j;

  [super initWithModel: aModel andKey: aKey andController: anObj andParent: aParent];

  param = [model objectForKey: @"numOfCells"];
  if (param) numOfCells = [param intValue];
  else numOfCells = [aParent numOfModelObjects];
  //CHECK_PARAM(param, "numOfCells");
  //numOfCells = [param intValue];

  param = [model objectForKey: @"maxCells"];
  if (param) maxCells = [param intValue];
  else {
    maxCells = numOfCells;
    printf("WARNING: maxCells not defined, using maxCells = %d\n", maxCells);
  }

  param = [model objectForKey: @"initialElements"];
  CHECK_PARAM(param, "initialElements");
  initialElements = [param intValue];

  param = [model objectForKey: @"numOfElements"];
  CHECK_PARAM(param, "numOfElements");
  numElements = [param intValue];
  
  param = [model objectForKey: @"maxElements"];
  if (!param) maxElements = initialElements;
  else maxElements = [param intValue];
  if (maxElements < (initialElements * maxCells)) {
    printf("WARNING: maxElements less than (initialElements * maxCells), increasing maxElements = %d\n", initialElements * maxCells);
    maxElements = initialElements * maxCells;
  }

  param = [model objectForKey: @"maxElementTypes"];
  if (!param) {
    maxElementTypes = 32;
    printf("WARNING: maxElementTypes not defined, using maxElementTypes = %d\n", maxElementTypes);
  } else maxElementTypes = [param intValue];

  param = [model objectForKey: @"maxForces"];
  if (!param) {
    maxForces = 32;
    printf("WARNING: maxForces not defined, using maxForces = %d\n", maxForces);
  } else maxForces = [param intValue];

  param = [model objectForKey: @"maxSectors"];
  if (!param) {
    maxSectors = 4096;
    printf("WARNING: maxSectors not defined, using maxSectors = %d\n", maxSectors);
  } else maxSectors = [param intValue];

  param = [model objectForKey: @"maxElementsPerSector"];
  if (!param) {
    maxElementsPerSector = 1024;
    printf("WARNING: maxElementsPerSector not defined, using maxElementsPerSector = %d\n", maxSectors);
  } else maxElementsPerSector = [param intValue];

  readOnlySectorBorder = NO;
  param = [model objectForKey: @"readOnlySectorBorder"];
  if (param) readOnlySectorBorder = [param boolValue];
  if ([self isDebug]) printf("readOnlySectorBorder = %d\n", readOnlySectorBorder);

  sectorSize = 0;

  // cell parameters
  param = [model objectForKey: @"cellRadius"];
  CHECK_PARAM(param, "cellRadius");
  cellRadius = [param doubleValue];

  // physical parameters
  param = [model objectForKey: @"viscousTimescale"];
  CHECK_PARAM(param, "viscousTimescale");
  viscousTimescale = [param doubleValue];

  param = [model objectForKey: @"elasticModulus"];
  CHECK_PARAM(param, "elasticModulus");
  elasticModulus = [param doubleValue];

  useMesh = NO;
  param = [model objectForKey: @"mesh"];
  CHECK_PARAM(param, "mesh");
  useMesh = [param boolValue];

  // force parameters
  a = [model objectForKey: @"forces"];
  CHECK_PARAM(a, "forces");
  forces = [NSMutableArray new];
  for (i = 0; i < [a count]; ++i)
    [forces addObject: [NSMutableDictionary dictionaryWithDictionary: [a objectAtIndex: i]]];

  int dims = [[super spatialModel] dimensions];
  if ((dims != 2) && (dims != 3)) {
    printf("ERROR: Only 2d and 3d supported for SubcellularElementModel\n");
    return nil;
  }

  // numerical constants
  double pi=4.0*atan(1.0);
  double ot=1.0/3.0;
  double p3=pi/(3.0*sqrt(2.0));
  double p2=pi/(2.0*sqrt(3.0));

  // derived quantities                                                                                                                                        
  double kappa_cell=elasticModulus*cellRadius;
  double kappa_element=kappa_cell/pow(numElements,ot);
  double damping_cell=viscousTimescale*kappa_cell;
  double damping_element=damping_cell/numElements;

  interactionMaxSquared = 0.0;
  numOfParameters = 1;
  forceMap = [NSMutableDictionary new];
  forceTypeMap = [NSMutableDictionary new];
  for (i = 0; i < [forces count]; ++i) {
    NSMutableDictionary *d = [forces objectAtIndex: i];
    NSString *forceName = [d objectForKey: @"name"];
    if (!forceName) {
      printf("ERROR: force is missing 'name' attribute.\n");
      return nil;
    }
    if ([forceMap objectForKey: forceName]) {
      printf("ERROR: Two forces with same name: %s\n", [forceName UTF8String]);
      return nil;
    }
    
    if ([[d objectForKey: @"potential"] isEqualToString: @"D_MORSE"]
        || [[d objectForKey: @"potential"] isEqualToString: @"PD_MORSE"]) {
      double epsilon = [[d objectForKey: @"epsilon"] doubleValue];
      double equilibrium = [[d objectForKey: @"equilibrium"] doubleValue];
      double interactionMax = [[d objectForKey: @"interactionMax"] doubleValue];

      // look for parameter overrides
      NSString *s = [NSString stringWithFormat: @"%@_epsilon", forceName];
      param = [model objectForKey: s];
      if (param) epsilon = [param doubleValue];
      
      s = [NSString stringWithFormat: @"%@_equilibrium", forceName];
      param = [model objectForKey: s];
      if (param) equilibrium = [param doubleValue];

      s = [NSString stringWithFormat: @"%@_interactionMax", forceName];
      param = [model objectForKey: s];
      if (param) interactionMax = [param doubleValue];
      
      double r_equil, eqSquared, rho, pot_min, factor;
      if (dims == 2) {
        r_equil = equilibrium*cellRadius*sqrt(12*p2/(double)numElements);
        eqSquared = r_equil*r_equil;
        rho = -(1.0/(interactionMax*interactionMax-1.0))*log(1.0-sqrt(1.0-epsilon));
        pot_min=(kappa_element/8)*(r_equil/rho)*(r_equil/rho);
        factor = 4.0*pot_min*rho/(r_equil*r_equil)/damping_element;
        
        printf("For 2D\n");
        printf("%s rho = %lf\n", [forceName UTF8String], rho);
        printf("%s factor = %lf\n", [forceName UTF8String], factor);
        printf("%s equilibrium point sq = %lf\n", [forceName UTF8String], eqSquared);
      } else {
        r_equil = equilibrium*cellRadius*pow(p3/(double)numElements,ot);
        eqSquared = r_equil*r_equil;
        rho = -(1.0/(interactionMax*interactionMax-1.0))*log(1.0-sqrt(1.0-epsilon));
        pot_min=(kappa_element/8)*(r_equil/rho)*(r_equil/rho);
        factor = 4.0*pot_min*rho/(r_equil*r_equil)/damping_element;

        printf("For 3D\n");
        printf("%s rho = %lf\n", [forceName UTF8String], rho);
        printf("%s factor = %lf\n", [forceName UTF8String], factor);
        printf("%s equilibrium point sq = %lf\n", [forceName UTF8String], eqSquared);
      }

      // the smallest sector size based upon forces
      if (i == 0) sectorSize = 1.1 * interactionMax * r_equil;
      else if (sectorSize > (1.1 * interactionMax * r_equil))
        sectorSize = 1.1 * interactionMax * r_equil;

      double imax = interactionMax*interactionMax*r_equil*r_equil;
      if (imax > interactionMaxSquared) interactionMaxSquared = imax;

      [d setObject: [NSNumber numberWithDouble: rho] forKey: @"rho"];
      [d setObject: [NSNumber numberWithDouble: factor] forKey: @"factor"];
      [d setObject: [NSNumber numberWithDouble: eqSquared] forKey: @"eqSquared"];

      [forceMap setObject: [NSNumber numberWithInt: numOfParameters] forKey: forceName];
      if ([[d objectForKey: @"potential"] isEqualToString: @"D_MORSE"])
        [forceTypeMap setObject: [NSNumber numberWithInt: 0] forKey: forceName];
      else
        [forceTypeMap setObject: [NSNumber numberWithInt: 1] forKey: forceName];
      numOfParameters += 3;
    }

    if ([[d objectForKey: @"potential"] isEqualToString: @"MORSE"]
        || [[d objectForKey: @"potential"] isEqualToString: @"PMORSE"]) {

      NSString *s = [NSString stringWithFormat: @"%@_U0", forceName];
      param = [model objectForKey: s];
      CHECK_PARAM(param, [s UTF8String]);
      if (param) [d setObject: param forKey: s];

      s = [NSString stringWithFormat: @"%@_U1", forceName];
      param = [model objectForKey: s];
      CHECK_PARAM(param, [s UTF8String]);
      if (param) [d setObject: param forKey: s];

      s = [NSString stringWithFormat: @"%@_ETA0", forceName];
      param = [model objectForKey: s];
      CHECK_PARAM(param, [s UTF8String]);
      if (param) [d setObject: param forKey: s];

      s = [NSString stringWithFormat: @"%@_ETA1", forceName];
      param = [model objectForKey: s];
      CHECK_PARAM(param, [s UTF8String]);
      if (param) [d setObject: param forKey: s];
      
      [forceMap setObject: [NSNumber numberWithInt: numOfParameters] forKey: forceName];
      if ([[d objectForKey: @"potential"] isEqualToString: @"MORSE"])
        [forceTypeMap setObject: [NSNumber numberWithInt: 2] forKey: forceName];
      else
        [forceTypeMap setObject: [NSNumber numberWithInt: 3] forKey: forceName];
      numOfParameters += 4;
    }
  }
  printf("max range sq = %lf\n", interactionMaxSquared);

  if (sectorSize == 0) {
    printf("ERROR: sectorSize is zero.\n");
    return nil;
  }
  printf("initial sectorSize = %lf\n", sectorSize);
  double *domainSize = [[super spatialModel] domainSize];
  sectorsInDomain = floor(domainSize[0] / sectorSize);
  sectorSize = domainSize[0] / sectorsInDomain;
  printf("adjusted sectorSize = %lf, %d\n", sectorSize, sectorsInDomain);
  
  elementForceTable = malloc(sizeof(int) * maxElementTypes * maxForces);
  int (*EFT)[maxForces][maxElementTypes] = elementForceTable;
  elementForceTypeTable = malloc(sizeof(int) * maxElementTypes * maxForces);
  int (*EFTT)[maxForces][maxElementTypes] = elementForceTypeTable;
  elementTypeTable = malloc(sizeof(int) * maxElementTypes * maxForces);
  int (*ETT)[maxForces][maxElementTypes] = elementTypeTable;
  elementForceFlagTable = malloc(sizeof(int) * maxElementTypes * maxForces);
  int (*EFFT)[maxForces][maxElementTypes] = elementForceFlagTable;

  int lastEntry[maxElementTypes];
  for (i = 0; i < maxForces; ++i)
    for (j = 0; j < maxElementTypes; ++j) {
      (*EFT)[i][j] = -1;
      (*EFTT)[i][j] = -1;
      (*ETT)[i][j] = -1;
      (*EFFT)[i][j] = -1;
      lastEntry[j] = 0;
    }
  
  NSArray *forceSet = [model objectForKey: @"forceSet"];
  for (i = 0; i < [forceSet count]; ++i) {
    NSArray *forceEntry = [forceSet objectAtIndex: i];
    int t1 = [[forceEntry objectAtIndex: 0] intValue];
    int t2 = [[forceEntry objectAtIndex: 1] intValue];
    NSString *fName = [forceEntry objectAtIndex: 2];
    NSString *fType = [forceEntry objectAtIndex: 3];
    if (![forceMap objectForKey: fName]) {
      printf("ERROR: forceSet references unknown force: %s\n", [fName UTF8String]);
      return nil;
    }
    int fNum = [[forceMap objectForKey: fName] intValue];
    int ftNum = [[forceTypeMap objectForKey: fName] intValue];
    
    (*EFT)[lastEntry[t1]][t1] = fNum;
    (*EFTT)[lastEntry[t1]][t1] = ftNum;
    (*ETT)[lastEntry[t1]][t1] = t2;
    if ([fType isEqualToString: @"INTRA"]) (*EFFT)[lastEntry[t1]][t1] = FORCE_TYPE_INTRA;
    else if ([fType isEqualToString: @"INTER"]) (*EFFT)[lastEntry[t1]][t1] = FORCE_TYPE_INTER;
    else if ([fType isEqualToString: @"COLONY"]) (*EFFT)[lastEntry[t1]][t1] = FORCE_TYPE_COLONY;
    else if ([fType isEqualToString: @"CELL_CENTER"]) {
      (*EFFT)[lastEntry[t1]][t1] = FORCE_TYPE_CELL_CENTER;
      calcCellCenters = YES;
    } else if ([fType isEqualToString: @"ELEMENT_CENTER"]) {
      (*EFFT)[lastEntry[t1]][t1] = FORCE_TYPE_ELEMENT_CENTER;
      calcElementCenters = YES;
    }

    ++lastEntry[t1];
  }

  if ([self isDebug]) {
    printf("elementForceTable =\n");
    for (i = 0; i < maxForces; ++i) {
      for (j = 0; j < maxElementTypes; ++j) {
        if (j) printf(" ");
        printf("%d", (*EFT)[i][j]);
      }
      printf("\n");
    }
    printf("elementForceTypeTable =\n");
    for (i = 0; i < maxForces; ++i) {
      for (j = 0; j < maxElementTypes; ++j) {
        if (j) printf(" ");
        printf("%d", (*EFTT)[i][j]);
      }
      printf("\n");
    }
    printf("elementTypeTable =\n");
    for (i = 0; i < maxForces; ++i) {
      for (j = 0; j < maxElementTypes; ++j) {
        if (j) printf(" ");
        printf("%d", (*ETT)[i][j]);
      }
      printf("\n");
    }
    printf("elementForceFlagTable =\n");
    for (i = 0; i < maxForces; ++i) {
      for (j = 0; j < maxElementTypes; ++j) {
        if (j) printf(" ");
        printf("%d", (*EFFT)[i][j]);
      }
      printf("\n");
    }
  }
    
  // noise/diffusion parameters
  param = [model objectForKey: @"useDiffusion"];
  CHECK_PARAM(param, "useDiffusion");
  useDiffusion = [param boolValue];

  param = [model objectForKey: @"diffusionCoefficient"];
  if (useDiffusion) CHECK_PARAM(param, "diffusionCoefficient");
  diffusionCoefficient = [param doubleValue];

  [self collectParameters];
  
#if 0
  rho = (double *)malloc(sizeof(double) * [forces count]);
  factor = (double *)malloc(sizeof(double) * [forces count]);
  eqSquared = (double *)malloc(sizeof(double) * [forces count]);
  interactionMaxSquared = 0.0;
  for (i = 0; i < [forces count]; ++i) {
    NSDictionary *d = [forces objectAtIndex: i];
    double epsilon = [[d objectForKey: @"epsilon"] doubleValue];
    double equilibrium = [[d objectForKey: @"equilibrium"] doubleValue];
    double interactionMax = [[d objectForKey: @"interactionMax"] doubleValue];

    double r_equil = equilibrium*cellRadius*pow(p3/(double)numElements,ot);
    eqSquared[i] = r_equil*r_equil;
    rho[i] = -(1.0/(interactionMax*interactionMax-1.0))*log(1.0-sqrt(1.0-epsilon));
    double pot_min=(kappa_element/8)*(r_equil/rho[i])*(r_equil/rho[i]);
    factor[i] = 4.0*pot_min*rho[i]/(r_equil*r_equil)/damping_element;

    double imax = interactionMax*interactionMax*r_equil*r_equil;
    if (imax > interactionMaxSquared) interactionMaxSquared = imax;
    
    printf("%s rho = %lf\n", [[d objectForKey: @"type"] UTF8String], rho[i]);
    printf("%s factor = %lf\n", [[d objectForKey: @"type"] UTF8String], factor[i]);
    printf("%s equilibrium point sq = %lf\n", [[d objectForKey: @"type"] UTF8String], eqSquared[i]);
  }
  printf("max range sq = %lf\n", interactionMaxSquared);
#endif
  
  // emperical
  double optdt = 0.01*viscousTimescale/pow(numElements,2*ot);
  if (dt > optdt) printf("WARNING: dt is larger than optimal empirical dt (%lf > %lf), system might not be stable.\n", dt, optdt);

  // initialization
  startRange = (double *)malloc(sizeof(double) * dims);
  range = (double *)malloc(sizeof(double) * dims);
  offset = (double *)malloc(sizeof(double) * dims);
  startRange[0] = 0.0;
  range[0] = 1.5;
  offset[0] = 3.0;
  startRange[1] = 0.0;
  range[1] = 1.5;
  offset[1] = 3.0;
  if (dims == 3) {
    startRange[2] = 0.05;
    range[2] = 0.05;
    offset[2] = 0.1;
  }
  randMult = 0.1;
  randOffset = 0.05;
  
  pm = [model objectForKey: @"initialize"];
  if (pm) {
    a = [pm objectForKey: @"startRange"];
    CHECK_PARAM(a, "initialize startRange");
    if (a) for (i = 0; i < [a count]; ++i) startRange[i] = [[a objectAtIndex: i] doubleValue];
    a = [pm objectForKey: @"range"];
    CHECK_PARAM(a, "initialize range");
    if (a) for (i = 0; i < [a count]; ++i) range[i] = [[a objectAtIndex: i] doubleValue];
    a = [pm objectForKey: @"offset"];
    CHECK_PARAM(a, "initialize offset");
    if (a) for (i = 0; i < [a count]; ++i) offset[i] = [[a objectAtIndex: i] doubleValue];
    param = [pm objectForKey: @"randMult"];
    CHECK_PARAM(param, "initialize randMult");
    if (param) randMult = [param doubleValue];
    param = [pm objectForKey: @"randOffset"];
    CHECK_PARAM(param, "initialize randOffset");
    if (param) randOffset = [param doubleValue];
    sectorBorder = 0;
    param = [pm objectForKey: @"sectorBorder"];
    if (param) sectorBorder = [param intValue];
  }
  
  return self;
}

- (void)dealloc
{
  if (elementData) [elementData release];
  //if (numOfElements) free(numOfElements);
  if (dataFile) fclose(dataFile);
  //if (rho) free(rho);
  //if (factor) free(factor);
  //if (eqSquared) free(eqSquared);
  if (range) free(range);
  if (offset) free(offset);
  if (forces) [forces release];
  int i;
  for (i = 0; i < 19; ++i) if (meshNeighbors[i]) free(meshNeighbors[i]);

  if (incomingSectors) free(incomingSectors);
  if (outgoingSectors) free(outgoingSectors);
  if (incomingElements) free(incomingElements);
  if (outgoingElements) free(outgoingElements);
  if (availableElements) [availableElements release];

  [super dealloc];
}

- (int)numOfCells { return numOfCells; }
//- (int)maxCells { return maxCells; }
//- (int *)numOfElements { return numOfElements; }
- (int)maxElements { return maxElements; }
- (int)totalElements { return totalElements; }
- (void *)elementX { return [[[elementData gridWithName: @"X"] objectForKey: @"matrix"] getMatrix]; }
- (void *)elementY { return [[[elementData gridWithName: @"Y"] objectForKey: @"matrix"] getMatrix]; }
- (void *)elementZ { return [[[elementData gridWithName: @"Z"] objectForKey: @"matrix"] getMatrix]; }
- (void *)elementType { return [[[elementData gridWithName: @"type"] objectForKey: @"matrix"] getMatrix]; }
- (void *)elementCellNumber { return [[[elementData gridWithName: @"cell"] objectForKey: @"matrix"] getMatrix]; }
- (double)maxInteractionRange { return interactionMaxSquared; }

- (int)maxElementsPerSector { return maxElementsPerSector; }
- (int)maxSectors { return maxSectors; }
- (double)sectorSize { return sectorSize; }
- (int)numSectors { return numSectors; }
- (int *)sectorNeighborTable { return sectorNeighborTable; }
- (int *)sectorCoordinates { return sectorCoordinates; }

- (int)numOfParameters
{
  // just the derived quantities
  return numOfParameters;
}

- (NSString *)nameOfParameter: (int)theParam
{
  return [parameterNames objectAtIndex: theParam];
}

- (BOOL)setValue: (double)aValue forParameter: (NSString *)pName
{
  printf("perturb parameter: %s = %f\n", [pName UTF8String], aValue);
  if ([pName isEqualToString: @"MAX_RANGE_SQ"]) {
    interactionMaxSquared = aValue;
    return YES;
  }
  
  int i;
  for (i = 0; i < [forces count]; ++i) {
    NSMutableDictionary *d = [forces objectAtIndex: i];
    NSString *forceName = [d objectForKey: @"name"];

    if ([[d objectForKey: @"potential"] isEqualToString: @"D_MORSE"]
        || [[d objectForKey: @"potential"] isEqualToString: @"PD_MORSE"]) {
      // TODO: have to recalculate
      return NO;
    }
    
    if ([[d objectForKey: @"potential"] isEqualToString: @"MORSE"]
        || [[d objectForKey: @"potential"] isEqualToString: @"PMORSE"]) {
      NSString *s = [NSString stringWithFormat: @"%@_U0", forceName];
      if ([pName isEqualToString: s]) {
        [d setObject: [NSNumber numberWithDouble: aValue] forKey: s];
        //printf("found: %s\n", [[d description] UTF8String]);
        return YES;
      }

      s = [NSString stringWithFormat: @"%@_U1", forceName];
      if ([pName isEqualToString: s]) {
        [d setObject: [NSNumber numberWithDouble: aValue] forKey: s];
        return YES;
      }

      s = [NSString stringWithFormat: @"%@_ETA0", forceName];
      if ([pName isEqualToString: s]) {
        [d setObject: [NSNumber numberWithDouble: aValue] forKey: s];
        return YES;
      }

      s = [NSString stringWithFormat: @"%@_ETA1", forceName];
      if ([pName isEqualToString: s]) {
        [d setObject: [NSNumber numberWithDouble: aValue] forKey: s];
        return YES;
      }
    }
  }

  return NO;
}

//
//
//

// TODO
#if 0
#define X_RANGE 1.5
#define X_OFFSET 3.0
#define Y_RANGE 1.5
#define Y_OFFSET 3.0
#define Z_RANGE 0.05
#define Z_OFFSET 0.1
//#define RAND_MULT 0.1
//#define RAND_OFFSET 0.05
#define RAND_MULT 0.1
#define RAND_OFFSET 0.05
#endif

- (void)addElementForCell: (int)cellNum
{
  int i;

  if (cellNum >= numOfCells) return;
  
  Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *T = [[elementData gridWithName: @"type"] objectForKey: @"matrix"];
  Grid2DArray *C = [[elementData gridWithName: @"cell"] objectForKey: @"matrix"];
  int (*etGrid)[1][maxElements] = [T getMatrix];
  int (*cGrid)[1][maxElements] = [C getMatrix];
  
  SpatialModel *spatialModel = [super spatialModel];
  int dims = [spatialModel dimensions];
  double *domainSize = [spatialModel domainSize];
  
  // put new element at random offset of first element
  if ([dataEncode isEqual: [BioSwarmModel floatEncode]]) {
    float (*xGrid)[1][maxElements] = [X getMatrix];
    float (*yGrid)[1][maxElements] = [Y getMatrix];
    float (*zGrid)[1][maxElements] = [Z getMatrix];

    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] == cellNum) {
        (*xGrid)[0][totalElements] = (*xGrid)[0][i] + RAND_NUM * randMult - randOffset;
        if ((*xGrid)[0][totalElements] < 0.0) (*xGrid)[0][totalElements] = 0.0;
        if ((*xGrid)[0][totalElements] > domainSize[0]) (*xGrid)[0][totalElements] = domainSize[0];
        (*yGrid)[0][totalElements] = (*yGrid)[0][i] + RAND_NUM * randMult - randOffset;
        if ((*yGrid)[0][totalElements] < 0.0) (*yGrid)[0][totalElements] = 0.0;
        if ((*yGrid)[0][totalElements] > domainSize[1]) (*yGrid)[0][totalElements] = domainSize[1];
        if (dims == 2) (*zGrid)[0][totalElements] = 0.0;
        else {
          (*zGrid)[0][totalElements] = (*zGrid)[0][i] + RAND_NUM * randMult - randOffset;
          if ((*zGrid)[0][totalElements] < 0.0) (*zGrid)[0][totalElements] = 0.0;
          if ((*zGrid)[0][totalElements] > domainSize[2]) (*zGrid)[0][totalElements] = domainSize[2];
        }
        (*etGrid)[0][totalElements] = 0;
        (*cGrid)[0][totalElements] = cellNum;

        printf("add element (%d) at (%f, %f, %f)\n", cellNum,
               (*xGrid)[0][totalElements], (*yGrid)[0][totalElements],
               (*zGrid)[0][totalElements]);

        ++totalElements;
        break;
      }
    }
    
  } else if ([dataEncode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*xGrid)[1][maxElements] = [X getMatrix];
    double (*yGrid)[1][maxElements] = [Y getMatrix];
    double (*zGrid)[1][maxElements] = [Z getMatrix];

    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] == cellNum) {
        (*xGrid)[0][totalElements] = (*xGrid)[0][i] + RAND_NUM * randMult - randOffset;
        if ((*xGrid)[0][totalElements] < 0.0) (*xGrid)[0][totalElements] = 0.0;
        if ((*xGrid)[0][totalElements] > domainSize[0]) (*xGrid)[0][totalElements] = domainSize[0];
        (*yGrid)[0][totalElements] = (*yGrid)[0][i] + RAND_NUM * randMult - randOffset;
        if ((*yGrid)[0][totalElements] < 0.0) (*yGrid)[0][totalElements] = 0.0;
        if ((*yGrid)[0][totalElements] > domainSize[1]) (*yGrid)[0][totalElements] = domainSize[1];
        if (dims == 2) (*zGrid)[0][totalElements] = 0.0;
        else {
          (*zGrid)[0][totalElements] = (*zGrid)[0][i] + RAND_NUM * randMult - randOffset;
          if ((*zGrid)[0][totalElements] < 0.0) (*zGrid)[0][totalElements] = 0.0;
          if ((*zGrid)[0][totalElements] > domainSize[2]) (*zGrid)[0][totalElements] = domainSize[2];
        }
        (*etGrid)[0][totalElements] = 0;
        (*cGrid)[0][totalElements] = cellNum;
        
        printf("add element (%d) at (%f, %f, %f)\n", cellNum,
               (*xGrid)[0][totalElements], (*yGrid)[0][totalElements],
               (*zGrid)[0][totalElements]);
        
        ++totalElements;
        break;
      }
    }
  }
}

- (void)divideCell: (int)cellNum
{
  int i;
  
  if (cellNum >= numOfCells) return;
  
  Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *C = [[elementData gridWithName: @"cell"] objectForKey: @"matrix"];
  int (*cGrid)[1][maxElements] = [C getMatrix];
  
  if ([dataEncode isEqual: [BioSwarmModel floatEncode]]) {
    float (*xGrid)[1][maxElements] = [X getMatrix];
    float (*yGrid)[1][maxElements] = [Y getMatrix];
    float (*zGrid)[1][maxElements] = [Z getMatrix];

    // count the elements for the cell
    int n = 0;
    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] == cellNum) ++n;
    }
    int newn = n / 2;
    if (newn == 0) return;

    // cleave cell in random angle of XY plane
    // sort elements by translation of cleavage plane
    sort_helper *sortHelper = (sort_helper *)malloc(sizeof(sort_helper) * n);
    double theta = RAND_NUM * M_PI;
    double costheta = cos(theta);
    double sintheta = sin(theta);
    int sidx = 0;
    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] == cellNum) {
        sortHelper[sidx].fValue = costheta * (*xGrid)[0][i] + sintheta * (*yGrid)[0][i];
        sortHelper[sidx].idx = i;
        ++sidx;
      }
    }
    qsort(sortHelper, n, sizeof(sort_helper), &float_cmp_sort_helper);
    for (i = 0; i < n; ++i) {
      printf("%d: %f\n", sortHelper[i].idx, sortHelper[i].fValue);
    }
    printf("n = %d\n", n);

    // assign elements to new cell
    for (i = 0; i < newn; ++i) {
      (*cGrid)[0][sortHelper[i].idx] = numOfCells;
    }

    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] == cellNum) {
        printf("element (%d, %d) at (%f, %f, %f)\n", (*cGrid)[0][i], i,
               (*xGrid)[0][i], (*yGrid)[0][i],
               (*zGrid)[0][i]);
      }
    }

    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] == numOfCells) {
        printf("element (%d, %d) at (%f, %f, %f)\n", (*cGrid)[0][i], i,
               (*xGrid)[0][i], (*yGrid)[0][i],
               (*zGrid)[0][i]);
      }
    }

    ++numOfCells;
    free(sortHelper);

  } else if ([dataEncode isEqual: [BioSwarmModel doubleEncode]]) {
    printf("ERROR: double encoding not supported.\n");
  }
}

//
// Simulation routines
//

- (void)setupDataFilesWithState: (int)aState
{
  // data files
  dataFileState = aState;

  if (dataFile) {
    fclose(dataFile);
    dataFile = NULL;
  }

  if (meshFile) {
    fclose(meshFile);
    meshFile = NULL;
  }

  NSString *fileName, *meshFileName = nil;
  if ((dataFileState == BCMODEL_READ_STATE) && ([modelController useInitializeFile])) {
    fileName = [NSString stringWithFormat: @"%@_%@_cells.dat", [modelController initializeFile], name];
    meshFileName = [NSString stringWithFormat: @"%@_%@_mesh.dat", [modelController initializeFile], name];
  } else {
    NSString *md = [modelController modelDirectory];
    if (md) fileName = [NSString stringWithFormat: @"%@/%@_cells_%@.dat", md, name, outputSuffix];
    else fileName = [NSString stringWithFormat: @"%@_cells_%@.dat", name, outputSuffix];
  }
  //printf("fileName = %s\n", [fileName UTF8String]);
  
  switch (aState) {
    case BCMODEL_NOFILE_STATE:
      // no data files setup
      break;
    case BCMODEL_WRITE_STATE:
    {
      // data files for writing
      dataFile = fopen([fileName UTF8String], "wb+");
      if (dataFile == NULL) {
        NSLog(@"Cannot open file: %@", fileName);
        exit(0);
      }
      break;
    }
    case BCMODEL_READ_STATE:
    {
      // data files for reading
      dataFile = fopen([fileName UTF8String], "rb+");
      if (dataFile == NULL) {
        NSLog(@"Cannot open file: %@", fileName);
        exit(0);
      }
      if ((useMesh) & (meshFileName != nil)) {
        meshFile = fopen([meshFileName UTF8String], "rb+");
        if (meshFile == NULL) {
          NSLog(@"Cannot open file: %@", meshFileName);
          exit(0);
        }
      }
      break;
    }
  }
}

- (void)allocateSimulationWithEncode: (NSString *)anEncode
{
  //dataEncode = anEncode;

  printf("SEM: allocateSimulationWithEncode:\n");
  int i;
  int dims = [[super spatialModel] dimensions];

  if (elementData) {
    [elementData release];
    elementData = nil;
  }

  printf("childModels = %ld\n", [childModels count]);
  printf("multiplicity = %d\n", multiplicity);

  // use grid to store element data
  elementData = [[MultiScale2DGrid alloc] initWithWidth: maxElements andHeight: 1];
  [elementData newGridWithName: @"X" atScale: 1 withEncode: dataEncode];
  [elementData newGridWithName: @"Y" atScale: 1 withEncode: dataEncode];
  [elementData newGridWithName: @"Z" atScale: 1 withEncode: dataEncode];
  [elementData newGridWithName: @"type" atScale: 1 withEncode: [BioSwarmModel intEncode]];
  [elementData newGridWithName: @"cell" atScale: 1 withEncode: [BioSwarmModel intEncode]];
  [elementData newGridWithName: @"sector" atScale: 1 withEncode: [BioSwarmModel intEncode]];

  if (useMesh) {
    for (i = 0; i < 19; ++i) meshNeighbors[i] = malloc(sizeof(int) * maxElements);
    //printf("meshNeighbors[%d] = %p\n", i, meshNeighbors[i]);
  }
  //numOfElements = malloc(maxCells * sizeof(int));

  // sector tables
  if (sectorCoordinates) free(sectorCoordinates);
  sectorCoordinates = malloc(maxSectors * 3 * sizeof(int));
  for (i = 0; i < (maxSectors * 3); ++i) sectorCoordinates[i] = 0;

  if (sectorReadOnlyFlag) free(sectorReadOnlyFlag);
  sectorReadOnlyFlag = malloc(maxSectors * sizeof(int));
  for (i = 0; i < maxSectors; ++i) sectorReadOnlyFlag[i] = 0;

  if (sectorElementTable) free(sectorElementTable);
  sectorElementTable = malloc(maxElementsPerSector * maxSectors * sizeof(int));
  for (i = 0; i < (maxElementsPerSector * maxSectors); ++i) sectorElementTable[i] = -1;

  if (sectorNeighborTable) free(sectorNeighborTable);
  int nSize;
  if (dims == 2) nSize = S_XY_TOT;
  else nSize = S_XYZ_TOT;
  sectorNeighborTable = malloc(maxSectors * nSize * sizeof(int));
  for (i = 0; i < (maxSectors * nSize); ++i) sectorNeighborTable[i] = -1;
#if 0
  // element neighbor table
  if (elementNeighborTable) free(elementNeighborTable);
  elementNeighborTable = malloc(maxElements * nSize * maxElementsPerSector * sizeof(int));
  for (i = 0; i < (maxElements * nSize * maxElementsPerSector); ++i) elementNeighborTable[i] = -1;
#endif
}

- (BOOL)writeDataWithCheck: (BOOL)aFlag
{
  if (dataFileState != BCMODEL_WRITE_STATE) return NO;

  int i;
  Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *T = [[elementData gridWithName: @"type"] objectForKey: @"matrix"];
  Grid2DArray *C = [[elementData gridWithName: @"cell"] objectForKey: @"matrix"];
  //Grid2DArray *S = [[elementData gridWithName: @"sector"] objectForKey: @"matrix"];
  int (*etGrid)[1][maxElements] = [T getMatrix];
  int (*cGrid)[1][maxElements] = [C getMatrix];
  //int (*sGrid)[maxElements] = [S getMatrix];

  SpatialModel *spatialModel = [modelController spatialModel];
  int dims = [spatialModel dimensions];
  double *domainSize = [spatialModel domainSize];
  double ct = [modelController currentTime];
  if (isBinaryFile) {
    // write data in binary file
    if ([dataEncode isEqual: [BioSwarmModel floatEncode]]) {
      float (*xGrid)[1][maxElements] = [X getMatrix];
      float (*yGrid)[1][maxElements] = [Y getMatrix];
      float (*zGrid)[1][maxElements] = [Z getMatrix];
      float coords[3];

      // only write out elements within domain
      int countElements = 0;
      for (i = 0; i < totalElements; ++i) {
        coords[0] = (*xGrid)[0][i]; coords[1] = (*yGrid)[0][i]; coords[2] = (*zGrid)[0][i];
        if ((coords[0] < 0) || (coords[0] >= domainSize[0])) continue;
        if ((coords[1] < 0) || (coords[1] >= domainSize[1])) continue;
        if ((dims == 3) && ((coords[2] < 0) || (coords[2] >= domainSize[2]))) continue;
	++countElements;
      }

      fwrite(&(countElements), sizeof(int), 1, dataFile);
      for (i = 0; i < totalElements; ++i) {
        coords[0] = (*xGrid)[0][i]; coords[1] = (*yGrid)[0][i]; coords[2] = (*zGrid)[0][i];
        if ((coords[0] < 0) || (coords[0] >= domainSize[0])) continue;
        if ((coords[1] < 0) || (coords[1] >= domainSize[1])) continue;
        if ((dims == 3) && ((coords[2] < 0) || (coords[2] >= domainSize[2]))) continue;

	if ([spatialModel isSubspace]) [spatialModel translateFloatCoordinatesToWorld: coords forModel: self];

        fwrite(coords, sizeof(float), 3, dataFile);
        fwrite(&((*etGrid)[0][i]), sizeof(int), 1, dataFile);
        fwrite(&((*cGrid)[0][i]), sizeof(int), 1, dataFile);
        //fwrite(&((*sGrid)[i]), sizeof(int), 1, dataFile);
      }
    } else if ([dataEncode isEqual: [BioSwarmModel doubleEncode]]) {
      double (*xGrid)[1][maxElements] = [X getMatrix];
      double (*yGrid)[1][maxElements] = [Y getMatrix];
      double (*zGrid)[1][maxElements] = [Z getMatrix];
      double coords[3];

      // only write out elements within domain
      int countElements = 0;
      for (i = 0; i < totalElements; ++i) {
        coords[0] = (*xGrid)[0][i]; coords[1] = (*yGrid)[0][i]; coords[2] = (*zGrid)[0][i];
        if ((coords[0] < 0) || (coords[0] >= domainSize[0])) continue;
        if ((coords[1] < 0) || (coords[1] >= domainSize[1])) continue;
        if ((dims == 3) && ((coords[2] < 0) || (coords[2] >= domainSize[2]))) continue;
	++countElements;
      }

      fwrite(&(countElements), sizeof(int), 1, dataFile);
      for (i = 0; i < totalElements; ++i) {
        coords[0] = (*xGrid)[0][i]; coords[1] = (*yGrid)[0][i]; coords[2] = (*zGrid)[0][i];
        if ((coords[0] < 0) || (coords[0] >= domainSize[0])) continue;
        if ((coords[1] < 0) || (coords[1] >= domainSize[1])) continue;
        if ((dims == 3) && ((coords[2] < 0) || (coords[2] >= domainSize[2]))) continue;

        if ([spatialModel isSubspace]) [spatialModel translateDoubleCoordinatesToWorld: coords forModel: self];

        fwrite(coords, sizeof(double), 3, dataFile);
        //fwrite(&((*yGrid)[0][i]), sizeof(double), 1, dataFile);
        //fwrite(&((*zGrid)[0][i]), sizeof(double), 1, dataFile);
        fwrite(&((*etGrid)[0][i]), sizeof(int), 1, dataFile);
        fwrite(&((*cGrid)[0][i]), sizeof(int), 1, dataFile);
        //fwrite(&((*sGrid)[i]), sizeof(int), 1, dataFile);
      }
    }
    fflush(dataFile);

  } else {
    // write data in text file
    if ([dataEncode isEqual: [BioSwarmModel floatEncode]]) {
      float (*xGrid)[1][maxElements] = [X getMatrix];
      float (*yGrid)[1][maxElements] = [Y getMatrix];
      float (*zGrid)[1][maxElements] = [Z getMatrix];
      float coords[3];

      // only write out elements within domain
      int countElements = 0;
      for (i = 0; i < totalElements; ++i) {
        coords[0] = (*xGrid)[0][i]; coords[1] = (*yGrid)[0][i]; coords[2] = (*zGrid)[0][i];
        if ((coords[0] < 0) || (coords[0] >= domainSize[0])) continue;
        if ((coords[1] < 0) || (coords[1] >= domainSize[1])) continue;
        if ((dims == 3) && ((coords[2] < 0) || (coords[2] >= domainSize[2]))) continue;
	++countElements;
      }

      //fprintf(dataFile, "%f %d", ct, totalElements);
      fprintf(dataFile, "%f %d", ct, countElements);
      for (i = 0; i < totalElements; ++i) {
        coords[0] = (*xGrid)[0][i]; coords[1] = (*yGrid)[0][i]; coords[2] = (*zGrid)[0][i];
        if ((coords[0] < 0) || (coords[0] >= domainSize[0])) continue;
        if ((coords[1] < 0) || (coords[1] >= domainSize[1])) continue;
        if ((dims == 3) && ((coords[2] < 0) || (coords[2] >= domainSize[2]))) continue;

        if ([spatialModel isSubspace]) [spatialModel translateFloatCoordinatesToWorld: coords forModel: self];

        //fprintf(dataFile, " %f %f %f %d %d %d", coords[0], coords[1], coords[2], (*etGrid)[0][i], (*cGrid)[0][i], (*sGrid)[i]);
        fprintf(dataFile, " %f %f %f %d %d", coords[0], coords[1], coords[2], (*etGrid)[0][i], (*cGrid)[0][i]);
      }
      fprintf(dataFile, "\n");
    } else if ([dataEncode isEqual: [BioSwarmModel doubleEncode]]) {
      double (*xGrid)[1][maxElements] = [X getMatrix];
      double (*yGrid)[1][maxElements] = [Y getMatrix];
      double (*zGrid)[1][maxElements] = [Z getMatrix];
      double coords[3];

      // only write out elements within domain
      int countElements = 0;
      for (i = 0; i < totalElements; ++i) {
        coords[0] = (*xGrid)[0][i]; coords[1] = (*yGrid)[0][i]; coords[2] = (*zGrid)[0][i];
        if ((coords[0] < 0) || (coords[0] >= domainSize[0])) continue;
        if ((coords[1] < 0) || (coords[1] >= domainSize[1])) continue;
        if ((dims == 3) && ((coords[2] < 0) || (coords[2] >= domainSize[2]))) continue;
	++countElements;
      }

      fprintf(dataFile, "%f %d", ct, totalElements);
      for (i = 0; i < totalElements; ++i) {
        coords[0] = (*xGrid)[0][i]; coords[1] = (*yGrid)[0][i]; coords[2] = (*zGrid)[0][i];
        if ((coords[0] < 0) || (coords[0] >= domainSize[0])) continue;
        if ((coords[1] < 0) || (coords[1] >= domainSize[1])) continue;
        if ((dims == 3) && ((coords[2] < 0) || (coords[2] >= domainSize[2]))) continue;

        if ([spatialModel isSubspace]) [spatialModel translateDoubleCoordinatesToWorld: coords forModel: self];

        //fprintf(dataFile, " %f %f %f %d %d %d", coords[0], coords[1], coords[2], (*etGrid)[0][i], (*cGrid)[0][i], (*sGrid)[i]);
        fprintf(dataFile, " %f %f %f %d %d", coords[0], coords[1], coords[2], (*etGrid)[0][i], (*cGrid)[0][i]);
      }
      fprintf(dataFile, "\n");
    }
    fflush(dataFile);
  }
  
  return YES;
}

- (BOOL)readData
{
  if (dataFileState != BCMODEL_READ_STATE) return NO;
  
  int dims = [[super spatialModel] dimensions];

  int i, m;
  Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *T = [[elementData gridWithName: @"type"] objectForKey: @"matrix"];
  Grid2DArray *C = [[elementData gridWithName: @"cell"] objectForKey: @"matrix"];
  //Grid2DArray *S = [[elementData gridWithName: @"sector"] objectForKey: @"matrix"];
  int (*etGrid)[1][maxElements] = [T getMatrix];
  int (*cGrid)[1][maxElements] = [C getMatrix];
  //int (*sGrid)[maxElements] = [S getMatrix];
  SpatialModel *spatialModel = [modelController spatialModel];
  double *domainSize = [spatialModel domainSize];
  int currentElement = 0;

  double lowerBound, upperBound[3];
  lowerBound = 0 - (double)sectorBorder * sectorSize;
  upperBound[0] = domainSize[0] + (double)sectorBorder * sectorSize;
  upperBound[1] = domainSize[1] + (double)sectorBorder * sectorSize;
  upperBound[2] = domainSize[2] + (double)sectorBorder * sectorSize;

  if (isBinaryFile) {
    // read data from binary file
    if ([dataEncode isEqual: [BioSwarmModel floatEncode]]) {
      float (*xGrid)[1][maxElements] = [X getMatrix];
      float (*yGrid)[1][maxElements] = [Y getMatrix];
      float (*zGrid)[1][maxElements] = [Z getMatrix];
      float coords[3];
      currentElement = 0;
      fread(&(totalElements), sizeof(int), 1, dataFile);
      for (i = 0; i < totalElements; ++i) {
        fread(coords, sizeof(float), 3, dataFile);
        //fread(&((*yGrid)[0][i]), sizeof(float), 1, dataFile);
        //fread(&((*zGrid)[0][i]), sizeof(float), 1, dataFile);
        fread(&((*etGrid)[0][currentElement]), sizeof(int), 1, dataFile);
        fread(&((*cGrid)[0][currentElement]), sizeof(int), 1, dataFile);
        //fread(&((*sGrid)[currentElement]), sizeof(int), 1, dataFile);

        if ([spatialModel isSubspace]) {
          [spatialModel translateFloatCoordinatesToSubspace: coords forModel: self];
          if ((coords[0] < lowerBound) || (coords[0] >= upperBound[0])) continue;
          if ((coords[1] < lowerBound) || (coords[1] >= upperBound[1])) continue;
          if ((dims == 3) && ((coords[2] < lowerBound) || (coords[2] >= upperBound[2]))) continue;
        }
        (*xGrid)[0][currentElement] = coords[0]; (*yGrid)[0][currentElement] = coords[1]; (*zGrid)[0][currentElement] = coords[2];
        ++currentElement;
      }
    } else if ([dataEncode isEqual: [BioSwarmModel doubleEncode]]) {
      double (*xGrid)[1][maxElements] = [X getMatrix];
      double (*yGrid)[1][maxElements] = [Y getMatrix];
      double (*zGrid)[1][maxElements] = [Z getMatrix];
      double coords[3];
      currentElement = 0;
      fread(&(totalElements), sizeof(int), 1, dataFile);
      for (i = 0; i < totalElements; ++i) {
        fread(coords, sizeof(double), 3, dataFile);
        //fread(&((*yGrid)[0][i]), sizeof(double), 1, dataFile);
        //fread(&((*zGrid)[0][i]), sizeof(double), 1, dataFile);
        fread(&((*etGrid)[0][currentElement]), sizeof(int), 1, dataFile);
        fread(&((*cGrid)[0][currentElement]), sizeof(int), 1, dataFile);
        //fread(&((*sGrid)[currentElement]), sizeof(int), 1, dataFile);

        if ([spatialModel isSubspace]) {
          [spatialModel translateDoubleCoordinatesToSubspace: coords forModel: self];
          if ((coords[0] < lowerBound) || (coords[0] >= upperBound[0])) continue;
          if ((coords[1] < lowerBound) || (coords[1] >= upperBound[1])) continue;
          if ((dims == 3) && ((coords[2] < lowerBound) || (coords[2] >= upperBound[2]))) continue;
        }
        (*xGrid)[0][currentElement] = coords[0]; (*yGrid)[0][currentElement] = coords[1]; (*zGrid)[0][currentElement] = coords[2];
        ++currentElement;
      }
    }
    printf("totalElements = %d, currentElement = %d\n", totalElements, currentElement);
    totalElements = currentElement;

    // mesh neighbors
    if ((useMesh) & (meshFile != NULL)) {
      if (dims == 2) {
        for (i = 0; i < totalElements; ++i) {
          for (m = MESH_CURRENT; m < MESH_XY_TOT; ++m) {
            int *meshData = meshNeighbors[m];
            fread(&(meshData[i]), sizeof(int), 1, meshFile);
          }
        }
      } else {
        for (i = 0; i < totalElements; ++i) {
          for (m = MESH_CURRENT; m < MESH_XYZ_TOT; ++m) {
            int *meshData = meshNeighbors[m];
            fread(&(meshData[i]), sizeof(int), 1, meshFile);
          }
        }
      }
    }

    numOfCells = 0;
    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] > numOfCells) numOfCells = (*cGrid)[0][i];
    }
    ++numOfCells;

  } else {
    // read data in text file
    if ([dataEncode isEqual: [BioSwarmModel floatEncode]]) {
      float ct;
      float (*xGrid)[1][maxElements] = [X getMatrix];
      float (*yGrid)[1][maxElements] = [Y getMatrix];
      float (*zGrid)[1][maxElements] = [Z getMatrix];
      float coords[3];
      currentElement = 0;
      fscanf(dataFile, "%f %d", &ct, &totalElements);
      for (i = 0; i < totalElements; ++i) {
        fscanf(dataFile, "%f", &(coords[0]));
        fscanf(dataFile, "%f", &(coords[1]));
        fscanf(dataFile, "%f", &(coords[2]));
        fscanf(dataFile, "%d", &((*etGrid)[0][currentElement]));
        fscanf(dataFile, "%d", &((*cGrid)[0][currentElement]));
        //fscanf(dataFile, "%d", &((*sGrid)[currentElement]));

        if ([spatialModel isSubspace]) {
          [spatialModel translateFloatCoordinatesToSubspace: coords forModel: self];
          if ((coords[0] < lowerBound) || (coords[0] >= upperBound[0])) continue;
          if ((coords[1] < lowerBound) || (coords[1] >= upperBound[1])) continue;
          if ((dims == 3) && ((coords[2] < lowerBound) || (coords[2] >= upperBound[2]))) continue;
        }
        (*xGrid)[0][currentElement] = coords[0]; (*yGrid)[0][currentElement] = coords[1]; (*zGrid)[0][currentElement] = coords[2];
        //printf("(%d) %f %f %f\n", currentElement, coords[0], coords[1], coords[2]);
        ++currentElement;
      }
    } else if ([dataEncode isEqual: [BioSwarmModel doubleEncode]]) {
      double ct;
      double (*xGrid)[1][maxElements] = [X getMatrix];
      double (*yGrid)[1][maxElements] = [Y getMatrix];
      double (*zGrid)[1][maxElements] = [Z getMatrix];
      double coords[3];
      currentElement = 0;
      fscanf(dataFile, "%lf %d", &ct, &totalElements);
      for (i = 0; i < totalElements; ++i) {
        fscanf(dataFile, "%lf", &(coords[0]));
        fscanf(dataFile, "%lf", &(coords[1]));
        fscanf(dataFile, "%lf", &(coords[2]));
        fscanf(dataFile, "%d", &((*etGrid)[0][currentElement]));
        fscanf(dataFile, "%d", &((*cGrid)[0][currentElement]));
        //fscanf(dataFile, "%d", &((*sGrid)[currentElement]));

        if ([spatialModel isSubspace]) {
          [spatialModel translateDoubleCoordinatesToSubspace: coords forModel: self];
          if ((coords[0] < lowerBound) || (coords[0] >= upperBound[0])) continue;
          if ((coords[1] < lowerBound) || (coords[1] >= upperBound[1])) continue;
          if ((dims == 3) && ((coords[2] < lowerBound) || (coords[2] >= upperBound[2]))) continue;
        }
        (*xGrid)[0][currentElement] = coords[0]; (*yGrid)[0][currentElement] = coords[1]; (*zGrid)[0][currentElement] = coords[2];
        ++currentElement;
      }
    }
    printf("totalElements = %d, currentElement = %d\n", totalElements, currentElement);
    totalElements = currentElement;

    // mesh neighbors
    if ((useMesh) & (meshFile != NULL)) {
      if (dims == 2) {
        for (i = 0; i < totalElements; ++i) {
          for (m = MESH_CURRENT; m < MESH_XY_TOT; ++m) {
            int *meshData = meshNeighbors[m];
            fscanf(meshFile, "%d", &(meshData[i]));
          }
        }
      } else {
        for (i = 0; i < totalElements; ++i) {
          for (m = MESH_CURRENT; m < MESH_XYZ_TOT; ++m) {
            int *meshData = meshNeighbors[m];
            fscanf(meshFile, "%d", &(meshData[i]));
          }
        }
      }
    }

    numOfCells = 0;
    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] > numOfCells) numOfCells = (*cGrid)[0][i];
    }
    ++numOfCells;
  }

  if (totalElements > maxElements) {
    printf("ERROR: exceeded maximum number of elements: %d > %d\n", totalElements, maxElements);
    abort();
  }

  // check end of file
  if (feof(dataFile)) return NO;

  return YES;
}

- (void)rewindFiles
{
  if (dataFileState != BCMODEL_READ_STATE) return;
  rewind(dataFile);
}

- (void)generateSectors
{
  int dims = [[super spatialModel] dimensions];
  int i;

  printf("generateSectors\n");

  // sector coordinates
  numSectors = 0;
  int (*sCoordinates)[3][maxSectors] = (void *)sectorCoordinates;
  
  // create read only sector border if needed
  if (readOnlySectorBorder) {
      if (dims == 2) {
        // walk around the outside border
        int xc, yc;
        for (xc = -1; xc < sectorsInDomain; ++xc) {
          (*sCoordinates)[0][numSectors] = xc;
          (*sCoordinates)[1][numSectors] = -1;
          (*sCoordinates)[2][numSectors] = 0;
          sectorReadOnlyFlag[numSectors] = 1;
	  if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				     (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
          ++numSectors;
        }
        for (yc = -1; yc < sectorsInDomain; ++yc) {
          (*sCoordinates)[0][numSectors] = sectorsInDomain;
          (*sCoordinates)[1][numSectors] = yc;
          (*sCoordinates)[2][numSectors] = 0;
          sectorReadOnlyFlag[numSectors] = 1;
	  if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				     (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
          ++numSectors;
        }
        for (xc = sectorsInDomain; xc > -1; --xc) {
          (*sCoordinates)[0][numSectors] = xc;
          (*sCoordinates)[1][numSectors] = sectorsInDomain;
          (*sCoordinates)[2][numSectors] = 0;
          sectorReadOnlyFlag[numSectors] = 1;
	  if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				     (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
          ++numSectors;
        }
        for (yc = sectorsInDomain; yc > -1; --yc) {
          (*sCoordinates)[0][numSectors] = -1;
          (*sCoordinates)[1][numSectors] = yc;
          (*sCoordinates)[2][numSectors] = 0;
          sectorReadOnlyFlag[numSectors] = 1;
	  if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				     (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
          ++numSectors;
        }

	// walk around inside border
        for (xc = 0; xc < sectorsInDomain - 1; ++xc) {
          (*sCoordinates)[0][numSectors] = xc;
          (*sCoordinates)[1][numSectors] = 0;
          (*sCoordinates)[2][numSectors] = 0;
	  if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				     (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
          ++numSectors;
        }
        for (yc = 0; yc < sectorsInDomain - 1; ++yc) {
          (*sCoordinates)[0][numSectors] = sectorsInDomain - 1;
          (*sCoordinates)[1][numSectors] = yc;
          (*sCoordinates)[2][numSectors] = 0;
	  if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				     (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
          ++numSectors;
        }
        for (xc = sectorsInDomain - 1; xc > 0; --xc) {
          (*sCoordinates)[0][numSectors] = xc;
          (*sCoordinates)[1][numSectors] = sectorsInDomain - 1;
          (*sCoordinates)[2][numSectors] = 0;
	  if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				     (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
          ++numSectors;
        }
        for (yc = sectorsInDomain - 1; yc > 0; --yc) {
          (*sCoordinates)[0][numSectors] = 0;
          (*sCoordinates)[1][numSectors] = yc;
          (*sCoordinates)[2][numSectors] = 0;
	  if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				     (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
          ++numSectors;
        }

      } else {
	// 3D

	// walk around the outside border
        int xc, yc, zc;
        for (xc = -1; xc < sectorsInDomain; ++xc) {
	  for (yc = -1; yc < sectorsInDomain; ++yc) {
	    (*sCoordinates)[0][numSectors] = xc;
	    (*sCoordinates)[1][numSectors] = yc;
	    (*sCoordinates)[2][numSectors] = -1;
	    sectorReadOnlyFlag[numSectors] = 1;
	    if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
        for (yc = -1; yc < sectorsInDomain; ++yc) {
	  for (zc = -1; zc < sectorsInDomain; ++zc) {
	    (*sCoordinates)[0][numSectors] = sectorsInDomain;
	    (*sCoordinates)[1][numSectors] = yc;
	    (*sCoordinates)[2][numSectors] = zc;
	    sectorReadOnlyFlag[numSectors] = 1;
	    if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
        for (xc = sectorsInDomain; xc > -1; --xc) {
	  for (yc = -1; yc < sectorsInDomain; ++yc) {
	    (*sCoordinates)[0][numSectors] = xc;
	    (*sCoordinates)[1][numSectors] = yc;
	    (*sCoordinates)[2][numSectors] = sectorsInDomain;
	    sectorReadOnlyFlag[numSectors] = 1;
	    if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
	for (yc = -1; yc < sectorsInDomain; ++yc) {
	  for (zc = sectorsInDomain; zc > -1; --zc) {
	    (*sCoordinates)[0][numSectors] = -1;
	    (*sCoordinates)[1][numSectors] = yc;
	    (*sCoordinates)[2][numSectors] = zc;
	    sectorReadOnlyFlag[numSectors] = 1;
	    if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
	// fill top and bottom
        for (xc = -1; xc <= sectorsInDomain; ++xc) {
	  for (zc = -1; zc <= sectorsInDomain; ++zc) {
	    (*sCoordinates)[0][numSectors] = xc;
	    (*sCoordinates)[1][numSectors] = sectorsInDomain;
	    (*sCoordinates)[2][numSectors] = zc;
	    sectorReadOnlyFlag[numSectors] = 1;
	    if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
        for (xc = 0; xc < sectorsInDomain; ++xc) {
	  for (zc = 0; zc < sectorsInDomain; ++zc) {
	    (*sCoordinates)[0][numSectors] = xc;
	    (*sCoordinates)[1][numSectors] = -1;
	    (*sCoordinates)[2][numSectors] = zc;
	    sectorReadOnlyFlag[numSectors] = 1;
	    if ([self isDebug]) printf("read only border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}

	// walk around inside border
        for (xc = 0; xc < sectorsInDomain - 1; ++xc) {
	  for (yc = 0; yc < sectorsInDomain - 1; ++yc) {
	    (*sCoordinates)[0][numSectors] = xc;
	    (*sCoordinates)[1][numSectors] = yc;
	    (*sCoordinates)[2][numSectors] = 0;
	    if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
        for (yc = 0; yc < sectorsInDomain - 1; ++yc) {
	  for (zc = 0; zc < sectorsInDomain - 1; ++zc) {
	    (*sCoordinates)[0][numSectors] = sectorsInDomain - 1;
	    (*sCoordinates)[1][numSectors] = yc;
	    (*sCoordinates)[2][numSectors] = zc;
	    if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
        for (xc = sectorsInDomain - 1; xc > 0; --xc) {
	  for (yc = 0; yc < sectorsInDomain - 1; ++yc) {
	    (*sCoordinates)[0][numSectors] = xc;
	    (*sCoordinates)[1][numSectors] = yc;
	    (*sCoordinates)[2][numSectors] = sectorsInDomain - 1;
	    if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
	for (yc = 0; yc < sectorsInDomain - 1; ++yc) {
	  for (zc = sectorsInDomain - 1; zc > 0; --zc) {
	    (*sCoordinates)[0][numSectors] = 0;
	    (*sCoordinates)[1][numSectors] = yc;
	    (*sCoordinates)[2][numSectors] = zc;
	    if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
	// fill top and bottom
        for (xc = 0; xc < sectorsInDomain; ++xc) {
	  for (zc = 0; zc < sectorsInDomain; ++zc) {
	    (*sCoordinates)[0][numSectors] = xc;
	    (*sCoordinates)[1][numSectors] = sectorsInDomain - 1;
	    (*sCoordinates)[2][numSectors] = zc;
	    sectorReadOnlyFlag[numSectors] = 1;
	    if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}
        for (xc = 1; xc < sectorsInDomain - 1; ++xc) {
	  for (zc = 1; zc < sectorsInDomain - 1; ++zc) {
	    (*sCoordinates)[0][numSectors] = xc;
	    (*sCoordinates)[1][numSectors] = 0;
	    (*sCoordinates)[2][numSectors] = zc;
	    sectorReadOnlyFlag[numSectors] = 1;
	    if ([self isDebug]) printf("inside border sector %d (%d, %d, %d).\n", numSectors,
				       (*sCoordinates)[0][numSectors], (*sCoordinates)[1][numSectors], (*sCoordinates)[2][numSectors]);
	    ++numSectors;
	  }
	}

      }
  }
}

- (void)assignElementsToSectors
{  
  int dims = [[super spatialModel] dimensions];
  int i, j;
  Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *S = [[elementData gridWithName: @"sector"] objectForKey: @"matrix"];
  int (*sGrid)[maxElements] = [S getMatrix];
  int (*sCoordinates)[3][maxSectors] = (void *)sectorCoordinates;

  if ([dataEncode isEqual: [BioSwarmModel floatEncode]]) {
    float (*xGrid)[maxElements] = [X getMatrix];
    float (*yGrid)[maxElements] = [Y getMatrix];
    float (*zGrid)[maxElements] = [Z getMatrix];
    
    // assign each element to its sector, create new sectors if needed
    for (i = 0; i < totalElements; ++i) {
      int xc = floor((*xGrid)[i] / sectorSize);
      int yc = floor((*yGrid)[i] / sectorSize);
      int zc = floor((*zGrid)[i] / sectorSize);
      BOOL found = NO;
      for (j = 0; j < numSectors; ++j) {
        if (dims == 2) {
          if (((*sCoordinates)[0][j] == xc) && ((*sCoordinates)[1][j] == yc)) { found = YES; break; }
        } else {
          if (((*sCoordinates)[0][j] == xc) && ((*sCoordinates)[1][j] == yc) && ((*sCoordinates)[2][j] == zc)) { found = YES; break; }
        }
      }
      
      // sector numbers are sequential
      // so need to save sector coordinates
      if (found) (*sGrid)[i] = j;
      else {
        if (numSectors == maxSectors) { printf("ERROR: exceeded max number of sectors = %d\n", numSectors); abort(); }

        (*sCoordinates)[0][numSectors] = xc;
        (*sCoordinates)[1][numSectors] = yc;
        if (dims == 2) (*sCoordinates)[2][numSectors] = 0;
        else (*sCoordinates)[2][numSectors] = zc;
        (*sGrid)[i] = numSectors;
        
        ++numSectors;
      }
      if ([self isDebug]) printf("element %d (%f, %f %f) is in sector %d (%d, %d, %d), (%d, %d, %d).\n", i, (*xGrid)[i], (*yGrid)[i], (*zGrid)[i], (*sGrid)[i], (*sCoordinates)[0][numSectors-1], (*sCoordinates)[1][numSectors-1], (*sCoordinates)[2][numSectors-1], xc, yc, zc);
      //if ([modelController rank] == 2) printf("element %d (%f, %f %f) is in sector %d (%d, %d, %d).\n", i, (*xGrid)[i], (*yGrid)[i], (*zGrid)[i], (*sGrid)[i], xc, yc, zc);
    }
    
  } else if ([dataEncode isEqual: [BioSwarmModel doubleEncode]]) {
    printf("ERROR: double encoding not supported.\n");
  }
}

- (void)initializeNeighborTables
{
  int dims = [[super spatialModel] dimensions];
  int i, j;
  Grid2DArray *S = [[elementData gridWithName: @"sector"] objectForKey: @"matrix"];
  int (*sGrid)[maxElements] = [S getMatrix];
  int (*sCoordinates)[3][maxSectors] = (void *)sectorCoordinates;

  // sector neighbor table
  int nSize;
  if (dims == 2) nSize = S_XY_TOT;
  else nSize = S_XYZ_TOT;
  int (*snTable)[nSize][maxSectors] = (void *)sectorNeighborTable;
  for (i = 0; i < numSectors; ++i) {
    int nx, ny, nz;
    int direction;
    (*snTable)[S_CURRENT][i] = i;
    for (direction = S_START; direction < nSize; ++direction) {
      bc_sector_coordinate_shift((*sCoordinates)[0][i], (*sCoordinates)[1][i], (*sCoordinates)[2][i], direction, &nx, &ny, &nz);
      
      BOOL found = NO;
      for (j = 0; j < numSectors; ++j) {
        if (dims == 2) {
          if (((*sCoordinates)[0][j] == nx) && ((*sCoordinates)[1][j] == ny)) { found = YES; break; }
        } else {
          if (((*sCoordinates)[0][j] == nx) && ((*sCoordinates)[1][j] == ny) && ((*sCoordinates)[2][j] == nz)) { found = YES; break; }
        }
      }
      
      if (found) (*snTable)[direction][i] = j;
      else (*snTable)[direction][i] = -1;
      
      if ([self isDebug]) if ((*snTable)[direction][i] >= 0) printf("sector (%d) has neighbor (%d) in direction (%s).\n", i, (*snTable)[direction][i], bc_sector_direction_string(direction));
    }
  }
  
  // sector element table
  int (*seTable)[maxElementsPerSector][maxSectors] = (void *)sectorElementTable;
  for (i = 0; i < (maxElementsPerSector * maxSectors); ++i) sectorElementTable[i] = -1;
  for (i = 0; i < numSectors; ++i) {
    int sIdx = 0;
    for (j = 0; j < totalElements; ++j) {
      if ((*sGrid)[j] == i) {

        if (sIdx >= maxElementsPerSector) {
          printf("ERROR: maxElementsPerSector exceeded!\n");
          abort();
        }
        
        (*seTable)[sIdx][i] = j;
        ++sIdx;
      }
    }
  }
  
#if 0
  // element neighbor table
  int (*enTable)[nSize * maxElementsPerSector][maxElements] = (void *)elementNeighborTable;
  for (i = 0; i < (maxElements * nSize * maxElementsPerSector); ++i) elementNeighborTable[i] = -1;
  for (i = 0; i < totalElements; ++i) {
    int mySector = (*sGrid)[i];
    int direction;
    for (direction = S_CURRENT; direction < nSize; ++direction) {
      int currentSector = (*snTable)[direction][mySector];
      //printf("element (%d) in sector (%d) direction (%s, %d).\n", i, mySector, bc_sector_direction_string(direction), currentSector);
      if (currentSector == S_NONE) continue;
      
      int sIdx = 0;
      for (j = 0; j < totalElements; ++j) {
        if ((*sGrid)[j] == currentSector) {
          if (sIdx >= maxElementsPerSector) {
            printf("ERROR: maxElementsPerSector exceeded!\n");
            abort();
          }
          (*enTable)[(direction * maxElementsPerSector) + sIdx][i] = j;
          //if (i == 0) printf("init en = (%d, %d, %d)\n", i, sIdx, j);
          ++sIdx;
        }
      }
      if ([self isDebug]) printf("element (%d) in sector (%d) has %d elements in direction (%s, %d).\n", i, mySector, sIdx, bc_sector_direction_string(direction), currentSector);
    }
  }
#endif
}

- (void)initializeSimulation
{
  int dims = [[super spatialModel] dimensions];
  int i, j;
  Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *T = [[elementData gridWithName: @"type"] objectForKey: @"matrix"];
  Grid2DArray *C = [[elementData gridWithName: @"cell"] objectForKey: @"matrix"];
  Grid2DArray *S = [[elementData gridWithName: @"sector"] objectForKey: @"matrix"];
  int (*etGrid)[1][maxElements] = [T getMatrix];
  int (*cGrid)[1][maxElements] = [C getMatrix];
  int (*sGrid)[maxElements] = [S getMatrix];
  
  SpatialModel *spatialModel = [super spatialModel];
  double *domainSize = [spatialModel domainSize];

  // what type of initialization?
  int initType = 0;
  id pm = [model objectForKey: @"initialize"];
  if (pm) {
    NSString *s = [pm objectForKey: @"type"];
    if ([s isEqualToString: @"random"]) initType = 0;
    if ([s isEqualToString: @"grid"]) initType = 1;
    if ([s isEqualToString: @"hex"]) initType = 2;
  }

  if ([dataEncode isEqual: [BioSwarmModel floatEncode]]) {
    float (*xGrid)[1][maxElements] = [X getMatrix];
    float (*yGrid)[1][maxElements] = [Y getMatrix];
    float (*zGrid)[1][maxElements] = [Z getMatrix];

    switch (initType) {
      case 0: {
        // initial random positions for elements
        int elemNum = 0;
        for (j = 0; j < numOfCells; ++j) {
          //numOfElements[j] = initialElements;
          (*etGrid)[0][elemNum] = 0;
          (*cGrid)[0][elemNum] = j;
          
          (*xGrid)[0][elemNum] = range[0] * RAND_NUM + offset[0];
          if ((*xGrid)[0][elemNum] < 0.0) (*xGrid)[0][elemNum] = 0.0;
          if ((*xGrid)[0][elemNum] > domainSize[0]) (*xGrid)[0][elemNum] = domainSize[0];
          
          (*yGrid)[0][elemNum] = range[1] * RAND_NUM + offset[1];
          if ((*yGrid)[0][elemNum] < 0.0) (*yGrid)[0][elemNum] = 0.0;
          if ((*yGrid)[0][elemNum] > domainSize[1]) (*yGrid)[0][elemNum] = domainSize[1];
          
          if (dims == 2) (*zGrid)[0][elemNum] = 0.0;
          else {
            (*zGrid)[0][elemNum] = range[2] * RAND_NUM + offset[2];
            if ((*zGrid)[0][elemNum] < 0.0) (*zGrid)[0][elemNum] = 0.0;
            if ((*zGrid)[0][elemNum] > domainSize[2]) (*zGrid)[0][elemNum] = domainSize[2];
          }
          
          for (i = 1; i < initialElements; ++i) {
            (*xGrid)[0][elemNum+i] = (*xGrid)[0][elemNum] + RAND_NUM * randMult - randOffset;
            if ((*xGrid)[0][elemNum+i] < 0.0) (*xGrid)[0][elemNum+i] = 0.0;
            if ((*xGrid)[0][elemNum+i] > domainSize[0]) (*xGrid)[0][elemNum+i] = domainSize[0];
            (*yGrid)[0][elemNum+i] = (*yGrid)[0][elemNum] + RAND_NUM * randMult - randOffset;
            if ((*yGrid)[0][elemNum+i] < 0.0) (*yGrid)[0][elemNum+i] = 0.0;
            if ((*yGrid)[0][elemNum+i] > domainSize[1]) (*yGrid)[0][elemNum+i] = domainSize[1];
            if (dims == 2) (*zGrid)[0][elemNum+i] = 0.0;
            else {
              (*zGrid)[0][elemNum+i] = (*zGrid)[0][elemNum] + RAND_NUM * randMult - randOffset;
              if ((*zGrid)[0][elemNum+i] < 0.0) (*zGrid)[0][elemNum+i] = 0.0;
              if ((*zGrid)[0][elemNum+i] > domainSize[2]) (*zGrid)[0][elemNum+i] = domainSize[2];
            }
            (*etGrid)[0][elemNum+i] = 0;
            (*cGrid)[0][elemNum+i] = j;
          }
          elemNum += initialElements;
        }
        totalElements = elemNum;
        break;
      }

      case 1: {
        // uniform grid positions for cells/elements
        int elemNum = 0;
        float xpos = startRange[0], ypos = startRange[1], zpos;
        if (dims == 3) zpos = startRange[2];
  
        for (j = 0; j < numOfCells; ++j) {
          //numOfElements[j] = initialElements;
          (*etGrid)[0][elemNum] = 0;
          (*cGrid)[0][elemNum] = j;
          
          (*xGrid)[0][elemNum] = xpos + RAND_NUM * randMult - randOffset;
          if ((*xGrid)[0][elemNum] < 0.0) (*xGrid)[0][elemNum] = 0.0;
          if ((*xGrid)[0][elemNum] > domainSize[0]) (*xGrid)[0][elemNum] = domainSize[0];
          
          (*yGrid)[0][elemNum] = ypos + RAND_NUM * randMult - randOffset;
          if ((*yGrid)[0][elemNum] < 0.0) (*yGrid)[0][elemNum] = 0.0;
          if ((*yGrid)[0][elemNum] > domainSize[1]) (*yGrid)[0][elemNum] = domainSize[1];
          
          if (dims == 2) (*zGrid)[0][elemNum] = 0.0;
          else {
            (*zGrid)[0][elemNum] = zpos + RAND_NUM * randMult - randOffset;
            if ((*zGrid)[0][elemNum] < 0.0) (*zGrid)[0][elemNum] = 0.0;
            if ((*zGrid)[0][elemNum] > domainSize[2]) (*zGrid)[0][elemNum] = domainSize[2];
          }
          
          for (i = 1; i < initialElements; ++i) {
            (*xGrid)[0][elemNum+i] = xpos + RAND_NUM * randMult - randOffset;
            if ((*xGrid)[0][elemNum+i] < 0.0) (*xGrid)[0][elemNum+i] = 0.0;
            if ((*xGrid)[0][elemNum+i] > domainSize[0]) (*xGrid)[0][elemNum+i] = domainSize[0];
            (*yGrid)[0][elemNum+i] = ypos + RAND_NUM * randMult - randOffset;
            if ((*yGrid)[0][elemNum+i] < 0.0) (*yGrid)[0][elemNum+i] = 0.0;
            if ((*yGrid)[0][elemNum+i] > domainSize[1]) (*yGrid)[0][elemNum+i] = domainSize[1];
            if (dims == 2) (*zGrid)[0][elemNum+i] = 0.0;
            else {
              (*zGrid)[0][elemNum+i] = zpos + RAND_NUM * randMult - randOffset;
              if ((*zGrid)[0][elemNum+i] < 0.0) (*zGrid)[0][elemNum+i] = 0.0;
              if ((*zGrid)[0][elemNum+i] > domainSize[2]) (*zGrid)[0][elemNum+i] = domainSize[2];
            }
            (*etGrid)[0][elemNum+i] = 0;
            (*cGrid)[0][elemNum+i] = j;
          }
          elemNum += initialElements;

          xpos += offset[0];
          if (xpos > range[0]) {
            xpos = startRange[0];
            ypos += offset[1];
          }
        }
        totalElements = elemNum;
        break;
      }

      case 2: {
        // uniform hex positions for cells
        BOOL full = NO;
        BOOL flip = NO;
        int elemNum = 0;
        float xpos = startRange[0], ypos = startRange[1], zpos;
        if (dims == 3) zpos = startRange[2];
        
        for (j = 0; j < numOfCells; ++j) {

          // determine next good position
          BOOL good = NO;
          while (!good) {
            good = YES;

            if (sectorBorder > 0) {
              if (xpos < (sectorBorder * sectorSize + randMult)) good = NO;
              if (xpos > (domainSize[0] - sectorBorder * sectorSize - randMult)) good = NO;

              if (ypos < (sectorBorder * sectorSize + randMult)) good = NO;
              if (ypos > (domainSize[1] - sectorBorder * sectorSize - randMult)) good = NO;

              if (dims == 3) {
                if (zpos < (sectorBorder * sectorSize + randMult)) good = NO;
                if (zpos > (domainSize[2] - sectorBorder * sectorSize - randMult)) good = NO;
              }
            }

            if (!good) {
              xpos += offset[0];
              if (xpos > range[0]) {
                if (flip) xpos = startRange[0];
                else xpos = startRange[0] + offset[0] / 2.0;
                flip = !flip;
                ypos += offset[1];
              }
            }
            
            if ((xpos > range[0]) || (ypos > range[1])) { full = YES; break; }
          }

          if (full) {
            printf("WARNING: Not enough room for %d cells, placed %d.\n", numOfCells, j);
            numOfCells = j;
            break;
          }

          //numOfElements[j] = initialElements;
          (*etGrid)[0][elemNum] = 0;
          (*cGrid)[0][elemNum] = j;
          
          good = NO;
          while (!good) {
            good = YES;

            (*xGrid)[0][elemNum] = xpos + RAND_NUM * randMult - randOffset;
            if ((*xGrid)[0][elemNum] < 0.0) (*xGrid)[0][elemNum] = 0.0;
            if ((*xGrid)[0][elemNum] > domainSize[0]) (*xGrid)[0][elemNum] = domainSize[0];
            if (sectorBorder > 0) {
              if ((*xGrid)[0][elemNum] < (sectorBorder * sectorSize)) good = NO;
              if ((*xGrid)[0][elemNum] > (domainSize[0] - sectorBorder * sectorSize)) good = NO;
            }

            (*yGrid)[0][elemNum] = ypos + RAND_NUM * randMult - randOffset;
            if ((*yGrid)[0][elemNum] < 0.0) (*yGrid)[0][elemNum] = 0.0;
            if ((*yGrid)[0][elemNum] > domainSize[1]) (*yGrid)[0][elemNum] = domainSize[1];
            if (sectorBorder > 0) {
              if ((*yGrid)[0][elemNum] < (sectorBorder * sectorSize)) good = NO;
              if ((*yGrid)[0][elemNum] > (domainSize[1] - sectorBorder * sectorSize)) good = NO;
            }

            if (dims == 2) (*zGrid)[0][elemNum] = 0.0;
            else {
              (*zGrid)[0][elemNum] = zpos + RAND_NUM * randMult - randOffset;
              if ((*zGrid)[0][elemNum] < 0.0) (*zGrid)[0][elemNum] = 0.0;
              if ((*zGrid)[0][elemNum] > domainSize[2]) (*zGrid)[0][elemNum] = domainSize[2];
              if (sectorBorder > 0) {
                if ((*zGrid)[0][elemNum] < (sectorBorder * sectorSize)) good = NO;
                if ((*zGrid)[0][elemNum] > (domainSize[2] - sectorBorder * sectorSize)) good = NO;
              }
            }
          }
          
          for (i = 1; i < initialElements; ++i) {
            good = NO;
            while (!good) {
              good = YES;

              (*xGrid)[0][elemNum+i] = xpos + RAND_NUM * randMult - randOffset;
              if ((*xGrid)[0][elemNum+i] < 0.0) (*xGrid)[0][elemNum+i] = 0.0;
              if ((*xGrid)[0][elemNum+i] > domainSize[0]) (*xGrid)[0][elemNum+i] = domainSize[0];
              if (sectorBorder > 0) {
                if ((*xGrid)[0][elemNum+i] < (sectorBorder * sectorSize)) good = NO;
                if ((*xGrid)[0][elemNum+i] > (domainSize[0] - sectorBorder * sectorSize)) good = NO;
              }

              (*yGrid)[0][elemNum+i] = ypos + RAND_NUM * randMult - randOffset;
              if ((*yGrid)[0][elemNum+i] < 0.0) (*yGrid)[0][elemNum+i] = 0.0;
              if ((*yGrid)[0][elemNum+i] > domainSize[1]) (*yGrid)[0][elemNum+i] = domainSize[1];
              if (sectorBorder > 0) {
                if ((*yGrid)[0][elemNum+i] < (sectorBorder * sectorSize)) good = NO;
                if ((*yGrid)[0][elemNum+i] > (domainSize[1] - sectorBorder * sectorSize)) good = NO;
              }

              if (dims == 2) (*zGrid)[0][elemNum+i] = 0.0;
              else {
                (*zGrid)[0][elemNum+i] = zpos + RAND_NUM * randMult - randOffset;
                if ((*zGrid)[0][elemNum+i] < 0.0) (*zGrid)[0][elemNum+i] = 0.0;
                if ((*zGrid)[0][elemNum+i] > domainSize[2]) (*zGrid)[0][elemNum+i] = domainSize[2];
                if (sectorBorder > 0) {
                  if ((*zGrid)[0][elemNum+i] < (sectorBorder * sectorSize)) good = NO;
                  if ((*zGrid)[0][elemNum+i] > (domainSize[2] - sectorBorder * sectorSize)) good = NO;
                }
              }
            }

            (*etGrid)[0][elemNum+i] = 0;
            (*cGrid)[0][elemNum+i] = j;
          }
          elemNum += initialElements;

          xpos += offset[0];
          if (xpos > range[0]) {
            if (flip) xpos = startRange[0];
            else xpos = startRange[0] + offset[0] / 2.0;
            flip = !flip;
            ypos += offset[1];
          }
        }
        totalElements = elemNum;
        break;
      }
    }

  } else if ([dataEncode isEqual: [BioSwarmModel doubleEncode]]) {
    printf("ERROR: double encoding not supported.\n");
#if 0
    double (*xGrid)[maxElements][maxCells] = [X getMatrix];
    double (*yGrid)[maxElements][maxCells] = [Y getMatrix];
    double (*zGrid)[maxElements][maxCells] = [Z getMatrix];
    
    // initial random positions for elements                                                                                                                                                         
    for (j = 0; j < numOfCells; ++j) {
      numOfElements[j] = initialElements;
      
      //(*xGrid)[0][j] = BOUNDARY_X * RAND_NUM;                                                                                                                                                      
      (*xGrid)[0][j] = range[0] * RAND_NUM + offset[0];
      if ((*xGrid)[0][j] < 0.0) (*xGrid)[0][j] = 0.0;
      if ((*xGrid)[0][j] > domainSize[0]) (*xGrid)[0][j] = domainSize[0];
      
      //(*yGrid)[0][j] = BOUNDARY_Y * RAND_NUM;                                                                                                                                                      
      (*yGrid)[0][j] = range[1] * RAND_NUM + offset[1];
      if ((*yGrid)[0][j] < 0.0) (*yGrid)[0][j] = 0.0;
      if ((*yGrid)[0][j] > domainSize[1]) (*yGrid)[0][j] = domainSize[1];
      
      //(*zGrid)[0][j] = BOUNDARY_Z * RAND_NUM;                                                                                                                                                      
      if (dims == 2) (*zGrid)[0][j] = 0.0;
      else {
        (*zGrid)[0][j] = range[2] * RAND_NUM + offset[2];
        if ((*zGrid)[0][j] < 0.0) (*zGrid)[0][j] = 0.0;
        if ((*zGrid)[0][j] > domainSize[2]) (*zGrid)[0][j] = domainSize[2];
      }
      //(*zGrid)[0][j] = 2.0;                                                                                                                                                                        
      //(*zGrid)[0][j] = RAND_NUM * 100;                                                                                                                                                             
      
      for (i = 1; i < numOfElements[j]; ++i) {
        //if (i < 4) (*etGrid)[i][j] = 1;                                                                                                                                                            
        (*xGrid)[i][j] = (*xGrid)[0][j] + RAND_NUM * randMult - randOffset;
        if ((*xGrid)[i][j] < 0.0) (*xGrid)[i][j] = 0.0;
        if ((*xGrid)[i][j] > domainSize[0]) (*xGrid)[i][j] = domainSize[0];
        (*yGrid)[i][j] = (*yGrid)[0][j] + RAND_NUM * randMult - randOffset;
        if ((*yGrid)[i][j] < 0.0) (*yGrid)[i][j] = 0.0;
        if ((*yGrid)[i][j] > domainSize[1]) (*yGrid)[i][j] = domainSize[1];
        if (dims == 2) (*zGrid)[i][j] = 0.0;
        else {
          (*zGrid)[i][j] = (*zGrid)[0][j] + RAND_NUM * randMult - randOffset;
          if ((*zGrid)[i][j] < 0.0) (*zGrid)[i][j] = 0.0;
          if ((*zGrid)[i][j] > domainSize[2]) (*zGrid)[i][j] = domainSize[2];
        }
        //(*zGrid)[i][j] = 1.0;                                                                                                                                                                      
        //(*zGrid)[i][j] = (*zGrid)[0][j] + normal_dist_rand(0, 0.1);                                                                                                                                
        (*etGrid)[i][j] = 0;
      }
    }
#endif
  }

  if (totalElements > maxElements) {
    printf("ERROR: exceeded maximum number of elements %d > %d\n", totalElements, maxElements);
    abort();
  }

  // create sectors
  [self generateSectors];

  // place elements in sectors
  [self assignElementsToSectors];

  // neighbor tables are calculated from element and sector coordinates
  [self initializeNeighborTables];
}

- (void)initializeFileCompleted
{
  // create sectors
  [self generateSectors];
  
  // place elements in sectors
  [self assignElementsToSectors];

  // neighbor tables are calculated from element and sector coordinates
  [self initializeNeighborTables];
}

- (void)cleanupSimulation
{
}

@end

//
// GPU
//

@implementation SubcellularElementModel (GPU)

- (NSMutableString *)definitionsGPUCode: (NSString *)prefixName
{
  int dims = [[super spatialModel] dimensions];
  printf("definitionsGPUCode: %d\n", dims);
         
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendFormat: @"__constant__ float %@_sem_parameters[%d];\n\n", prefixName, [self numOfParameters]];
  
  [GPUCode appendFormat: @"\n__constant__ BC_SEM_GPUgrids %@_sem_grids[1];\n", prefixName];
  [GPUCode appendString: @"\n"];

  return GPUCode;
}

- (NSDictionary *)controllerGPUCode: (NSString *)prefixName
{
  int dims = [[super spatialModel] dimensions];

  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;

  //
  // Headers and defines
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <stdio.h>\n"];
  [GPUCode appendString: @"#include <unistd.h>\n\n"];
  [GPUCode appendString: @"extern \"C\" {\n"];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  //[GPUCode appendString: @"#include <BioSwarm/GPU_sem.h>\n"];
  //[GPUCode appendString: @"#include \"GPU_sem.h\"\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];

  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <BioSwarm/GPU_sem.h>\n"];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];

  //
  // Allocation: generic
  //
  
  //
  // Data Transfer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *g, int aFlag, float *hostParameters, int *cellNumber, int numOfCells, int totalElements, void *hostX, void *hostY, void *hostZ, void *hostType, void *hostSector, void *hostElementNeighborTable, void *EFT, void *EFTT, void *ETT, void *EFFT, void *hostData, void *meshElements, void *hostSectorCoordinates, void *hostNumSectors, void *hostSectorSize, void *hostSectorElementTable, void *hostSectorNeighborTable, void *hostReadOnlySector)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendString: @"\n BC_SEM_transferGPUKernel(model, g, aFlag, hostParameters, cellNumber, numOfCells, totalElements, hostX, hostY, hostZ, hostType, hostSector, hostElementNeighborTable, EFT, EFTT, ETT, EFFT, hostData, meshElements, hostSectorCoordinates, hostNumSectors, hostSectorSize, hostSectorElementTable, hostSectorNeighborTable, hostReadOnlySector);\n"];
  [GPUCode appendString: @"\n if (aFlag) {\n"];
  [GPUCode appendString: @"   // Copy data structure\n"];
  [GPUCode appendFormat: @"   cudaMemcpyToSymbol(%@_sem_grids, g, sizeof(BC_SEM_GPUgrids), 0, cudaMemcpyHostToDevice);\n", prefixName];
  
  [GPUCode appendString: @" }\n"];
  [GPUCode appendString: @"}\n"];
  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];

  //
  // Execution
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, double startTime, double nextTime)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];

  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" BC_SEM_GPUgrids *%@_ptrs = (BC_SEM_GPUgrids *)%@_data;\n", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
  
  GPUCode = [NSMutableString new];
#if 0
  [GPUCode appendFormat: @" int %@_x = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_y = 1;\n", prefixName];  
  //[GPUCode appendFormat: @" int %@_threadsPerBlock1D = %@_xx;\n", prefixName, prefixName];
  //[GPUCode appendFormat: @" int %@_blocksPerGrid1D = (%@_ptrs->numOfCells + %@_threadsPerBlock1D - 1) / %@_threadsPerBlock1D;\n", prefixName, prefixName, prefixName, prefixName];  
  [GPUCode appendFormat: @" if (%@_x > %@_ptrs->totalElements) %@_x = %@_ptrs->totalElements;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_y > %@_ptrs->numOfModels) %@_y = %@_ptrs->numOfModels;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerBlock(%@_x, %@_y);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerGrid((%@_ptrs->totalElements + %@_threadsPerBlock.x - 1) / %@_threadsPerBlock.x, (%@_ptrs->numOfModels + %@_threadsPerBlock.y - 1) / %@_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @" int %@_xx = 256;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_xx > %@_ptrs->numOfCells) %@_xx = %@_ptrs->numOfCells;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_cell_threadsPerBlock(%@_xx, %@_y);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_cell_blocksPerGrid((%@_ptrs->numOfCells + %@_cell_threadsPerBlock.x - 1) / %@_cell_threadsPerBlock.x, (%@_ptrs->numOfModels + %@_cell_threadsPerBlock.y - 1) / %@_cell_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];
#else
  [GPUCode appendFormat: @" int %@_x = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_y = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_x > %@_ptrs->totalElements) %@_x = %@_ptrs->totalElements;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_y > %@_ptrs->numOfModels) %@_y = %@_ptrs->numOfModels;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerBlock(%@_x, %@_y);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerGrid((%@_ptrs->totalElements + %@_threadsPerBlock.x - 1) / %@_threadsPerBlock.x, (%@_ptrs->numOfModels + %@_threadsPerBlock.y - 1) / %@_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendFormat: @" int %@_xx = 256;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_xx > %@_ptrs->numOfCells) %@_xx = %@_ptrs->numOfCells;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_cell_threadsPerBlock(%@_xx, %@_y);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_cell_blocksPerGrid((%@_ptrs->numOfCells + %@_cell_threadsPerBlock.x - 1) / %@_cell_threadsPerBlock.x, (%@_ptrs->numOfModels + %@_cell_threadsPerBlock.y - 1) / %@_cell_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @" int %@_nx = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_ny = 1;\n", prefixName];
  [GPUCode appendFormat: @" int %@_maxy = %@_ptrs->numOfModels * %@_ptrs->sectorNeighborSize * %@_ptrs->maxElementsPerSector;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_nx > %@_ptrs->totalElements) %@_nx = %@_ptrs->totalElements;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_ny > %@_maxy) %@_ny = %@_maxy;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@n_threadsPerBlock(%@_nx, %@_ny);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@n_threadsPerGrid((%@_ptrs->totalElements + %@n_threadsPerBlock.x - 1) / %@n_threadsPerBlock.x, (%@_maxy + %@n_threadsPerBlock.y - 1) / %@n_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @" int %@_sx = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_sy = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_sx > %@_ptrs->maxSectors) %@_sx = %@_ptrs->maxSectors;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_sy > %@_ptrs->numOfModels) %@_sy = %@_ptrs->numOfModels;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@s_threadsPerBlock(%@_sx, %@_sy);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@s_threadsPerGrid((%@_ptrs->maxSectors + %@s_threadsPerBlock.x - 1) / %@s_threadsPerBlock.x, (%@_ptrs->numOfModels + %@s_threadsPerBlock.y - 1) / %@s_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @" int %@_mx = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_my = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_mx > %@_ptrs->numOfModels) %@_mx = %@_ptrs->numOfModels;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@m_threadsPerBlock(%@_mx, %@_my);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@m_threadsPerGrid((%@_ptrs->numOfModels + %@m_threadsPerBlock.x - 1) / %@m_threadsPerBlock.x, 1);\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];
#endif
  
  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];

  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"      if (%@_ptrs->calcCellCenters) %@_SEM_center_kernel<<< %@_cell_blocksPerGrid, %@_cell_threadsPerBlock >>>();\n", prefixName, prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];

  [GPUCode appendFormat: @"      if (%@_ptrs->calcElementCenters) %@_SEM_element_center_kernel<<< %@_cell_blocksPerGrid, %@_cell_threadsPerBlock >>>();\n", prefixName, prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];

#if 0
  [GPUCode appendFormat: @"      %@_SEM_kernel1_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
  [GPUCode appendFormat: @"      %@_SEM_kernel2_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
#endif
#if 0
  [GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->X_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->Y_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->Z_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @"      %@_SEM_neighbor_kernel1_2rk<<< %@n_threadsPerGrid, %@n_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_neighbor_kernel1_2rk\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_neighbor_sum_kernel1_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_neighbor_sum_kernel1_2rk\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_neighbor_kernel2_2rk<<< %@n_threadsPerGrid, %@n_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_neighbor_kernel2_2rk\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_neighbor_update_kernel_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_neighbor_update_kernel_2rk\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_sector_count_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_sector_count_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_create_sectors_kernel<<< %@m_threadsPerGrid, %@m_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_create_sectors_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_build_sector_neighbors_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_build_sector_neighbors_kernel\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_move_elements_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_move_elements_kernel\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_update_element_sector_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_update_element_sector_kernel\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_update_element_neighbor_kernel<<< %@n_threadsPerGrid, %@n_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_update_element_neighbor_kernel\");\n\n", prefixName];
#endif
  //[GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->X_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->Y_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  //[GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->Z_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  //[GPUCode appendString: @"\n"];
  
  [GPUCode appendFormat: @"      %@_SEM_pairwise_kernel1_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_pairwise_kernel1_2rk\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_pairwise_kernel2_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_pairwise_kernel2_2rk\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_sector_count_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_sector_count_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_create_sectors_kernel<<< %@m_threadsPerGrid, %@m_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_create_sectors_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_build_sector_neighbors_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_build_sector_neighbors_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_move_elements_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_move_elements_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_update_element_sector_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_update_element_sector_kernel\");\n\n", prefixName];
  
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];
  
  //
  // Release: generic
  //
  
  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n// GPU function pointers\n"];
  //[GPUCode appendFormat: @"SEMfunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"SEMfunctions %@_gpuFunctions = {BC_SEM_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, BC_SEM_releaseGPUKernel, NULL, NULL, NULL, NULL};\n", prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];

  return GPUDictionary;
}

- (NSString *)centerKernelGPUCode: (NSString *)prefixName
{
  NSMutableString *GPUCode;
  int dims = [[super spatialModel] dimensions];
  
  GPUCode = [NSMutableString new];
  
  //
  // Center calculation for all elements in each cell
  //
  [GPUCode appendString: @"\n// Calculate cell center\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_SEM_center_kernel()\n", prefixName];
  [GPUCode appendString: @"{\n"];
  
  [GPUCode appendString: @"   int cellNum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int modelNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
  [GPUCode appendString: @"\n"];
  [GPUCode appendFormat: @"   if (cellNum >= %@_sem_grids->numOfCells) return;\n", prefixName];
  [GPUCode appendFormat: @"   if (modelNum >= %@_sem_grids->numOfModels) return;\n", prefixName];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendString: @"   int k;\n"];
  [GPUCode appendString: @"   float cX = 0.0;\n"];
  [GPUCode appendString: @"   float cY = 0.0;\n"];
  if (dims == 3) [GPUCode appendString: @"   float cZ = 0.0;\n"];
  [GPUCode appendFormat: @"   int cidx = modelNum*%@_sem_grids->idx_cPitch+cellNum;\n", prefixName];    
  //[GPUCode appendString: @"   int first = 1;\n"];
  [GPUCode appendString: @"   float numOfElements = 0.0;\n"];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->totalElements; ++k) {\n", prefixName];
  [GPUCode appendFormat: @"     int idx = modelNum*%@_sem_grids->idx_pitch+k;\n", prefixName];
  [GPUCode appendFormat: @"     if (%@_sem_grids->cellNumber[idx] != cellNum) continue;\n", prefixName];
  [GPUCode appendString: @"\n"];
      
  [GPUCode appendFormat: @"     cX += %@_sem_grids->X[idx];\n", prefixName];
  [GPUCode appendFormat: @"     cY += %@_sem_grids->Y[idx];\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"     cZ += %@_sem_grids->Z[idx];\n", prefixName];
  [GPUCode appendString: @"     numOfElements += 1.0f;\n"];
  [GPUCode appendString: @"   }\n"];
  [GPUCode appendString: @"\n"];
    
  [GPUCode appendString: @"   cX = cX / numOfElements;\n"];
  [GPUCode appendString: @"   cY = cY / numOfElements;\n"];
  if (dims == 3) [GPUCode appendString: @"   cZ = cZ / numOfElements;\n"];
    
  [GPUCode appendFormat: @"   %@_sem_grids->cellCenterX[cidx] = cX;\n", prefixName];
  [GPUCode appendFormat: @"   %@_sem_grids->cellCenterY[cidx] = cY;\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"   %@_sem_grids->cellCenterZ[cidx] = cZ;\n", prefixName];
  
  [GPUCode appendString: @"}\n"];
  
  //
  // Center calculation for each element type in each cell
  // To avoid using a 3D array, the element types and models are combined in the rows
  //
  [GPUCode appendString: @"\n// Calculate element centers\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_SEM_element_center_kernel()\n", prefixName];
  [GPUCode appendString: @"{\n"];
  
  [GPUCode appendString: @"   int cellNum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int modelNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
  [GPUCode appendString: @"\n"];
  [GPUCode appendFormat: @"   if (cellNum >= %@_sem_grids->numOfCells) return;\n", prefixName];
  [GPUCode appendFormat: @"   if (modelNum >= %@_sem_grids->numOfModels) return;\n", prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendString: @"   int k;\n"];

  [GPUCode appendString: @"\n   // zero out element centers\n"];
  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->maxElementTypes; ++k) {\n", prefixName];
  [GPUCode appendFormat: @"      int cidx = ((modelNum*%@_sem_grids->maxElementTypes)+k)*%@_sem_grids->idx_cPitch+cellNum;\n", prefixName, prefixName];    
  [GPUCode appendFormat: @"      %@_sem_grids->elementCenterX[cidx] = 0;\n", prefixName];
  [GPUCode appendFormat: @"      %@_sem_grids->elementCenterY[cidx] = 0;\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"      %@_sem_grids->elementCenterZ[cidx] = 0;\n", prefixName];
  [GPUCode appendFormat: @"      %@_sem_grids->elementCenterCount[cidx] = 0;\n", prefixName];
  [GPUCode appendString: @"   }\n"];

  [GPUCode appendString: @"\n   // calc element centers\n"];
  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->totalElements; ++k) {\n", prefixName];
  [GPUCode appendFormat: @"     int idx = modelNum*%@_sem_grids->idx_pitch+k;\n", prefixName];
  [GPUCode appendFormat: @"     if (%@_sem_grids->cellNumber[idx] != cellNum) continue;\n", prefixName];
  [GPUCode appendFormat: @"     int cidx = ((modelNum*%@_sem_grids->maxElementTypes)+%@_sem_grids->elementType[idx])*%@_sem_grids->idx_cPitch+cellNum;\n", prefixName, prefixName, prefixName];    
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendFormat: @"     %@_sem_grids->elementCenterX[cidx] += %@_sem_grids->X[idx];\n", prefixName, prefixName];
  [GPUCode appendFormat: @"     %@_sem_grids->elementCenterY[cidx] += %@_sem_grids->Y[idx];\n", prefixName, prefixName];
  if (dims == 3) [GPUCode appendFormat: @"     %@_sem_grids->elementCenterZ[cidx] += %@_sem_grids->Z[idx];\n", prefixName, prefixName];
  [GPUCode appendFormat: @"     %@_sem_grids->elementCenterCount[cidx] += 1.0f;\n", prefixName];
  
  [GPUCode appendString: @"   }\n"];
  
  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->maxElementTypes; ++k) {\n", prefixName];
  [GPUCode appendFormat: @"      int cidx = ((modelNum*%@_sem_grids->maxElementTypes)+k)*%@_sem_grids->idx_cPitch+cellNum;\n", prefixName, prefixName];    
  [GPUCode appendFormat: @"      if (%@_sem_grids->elementCenterCount[cidx] != 0) {\n", prefixName];
  [GPUCode appendFormat: @"         %@_sem_grids->elementCenterX[cidx] /= %@_sem_grids->elementCenterCount[cidx];\n", prefixName, prefixName];
  [GPUCode appendFormat: @"         %@_sem_grids->elementCenterY[cidx] /= %@_sem_grids->elementCenterCount[cidx];\n", prefixName, prefixName];
  if (dims == 3) [GPUCode appendFormat: @"         %@_sem_grids->elementCenterZ[cidx] /= %@_sem_grids->elementCenterCount[cidx];\n", prefixName, prefixName];
  [GPUCode appendString: @"      }\n"];
  [GPUCode appendString: @"   }\n"];

  [GPUCode appendString: @"}\n"];

  return GPUCode;
}

- (NSString *)functionKernelGPUCode: (NSString *)prefixName
{
  int i;
  NSMutableString *GPUCode;
  int dims = [[super spatialModel] dimensions];
  
  GPUCode = [NSMutableString new];

  [GPUCode appendString: @"\n// SEM movement function used by Runge-Kutta solvers\n"];
  [GPUCode appendString: @"__device__ void\n"];
  if (dims == 2) [GPUCode appendFormat: @"%@_SEM_kernel_function(int modelNum, int elemNum, float *X_in, float *Y_in, float *X_out, float *Y_out, float mh, float mf)\n", prefixName];
  else [GPUCode appendFormat: @"%@_SEM_kernel_function(int modelNum, int elemNum, float *X_in, float *Y_in, float *Z_in, float *X_out, float *Y_out, float *Z_out, float mh, float mf)\n", prefixName];
  [GPUCode appendString: @"{\n"];
  [GPUCode appendFormat: @"   int idx = modelNum*%@_sem_grids->idx_pitch+elemNum;\n", prefixName];
  [GPUCode appendString: @"   int j, k, fIndex, fNum;\n"];
  [GPUCode appendString: @"   float F_X1, F_X2;\n"];
  [GPUCode appendString: @"   float F_Y1, F_Y2;\n"];
  if (dims == 3) [GPUCode appendString: @"   float F_Z1, F_Z2;\n"];
  [GPUCode appendString: @"   float r, V;\n"];
  [GPUCode appendString: @"   float forceX = 0.0;\n"];
  [GPUCode appendString: @"   float forceY = 0.0;\n"];
  if (dims == 3) [GPUCode appendString: @"   float forceZ = 0.0;\n"];
  
  [GPUCode appendString: @"\n   // element position\n"];
  [GPUCode appendFormat: @"   F_X1 = %@_sem_grids->X[idx] + mf * X_in[idx];\n", prefixName];
  [GPUCode appendFormat: @"   F_Y1 = %@_sem_grids->Y[idx] + mf * Y_in[idx];\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"   F_Z1 = %@_sem_grids->Z[idx] + mf * Z_in[idx];\n", prefixName];
  [GPUCode appendFormat: @"   int eType = %@_sem_grids->elementType[idx];\n", prefixName];

  [GPUCode appendString: @"\n   // forces between elements\n"];
  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->totalElements; ++k) {\n", prefixName];
  [GPUCode appendString: @"      if (k == elemNum) continue;\n"];
  [GPUCode appendString: @"\n"];

  [GPUCode appendString: @"      fIndex = 0;\n"];
  [GPUCode appendFormat: @"      fNum = %@_sem_grids->elementForceTable[FINDEX];\n", prefixName];
  [GPUCode appendString: @"      while (fNum >= 0) {\n"];
  [GPUCode appendFormat: @"        switch (%@_sem_grids->elementForceFlagTable[FINDEX]) {\n", prefixName];
  [GPUCode appendString: @"          case FORCE_TYPE_INTRA: {\n"];
  [GPUCode appendString: @"            // elements of same cell\n"];
  [GPUCode appendFormat: @"            int oidx = modelNum*%@_sem_grids->idx_pitch+k;\n", prefixName];
  [GPUCode appendFormat: @"            int oeType = %@_sem_grids->elementType[oidx];\n", prefixName];
  [GPUCode appendString: @"\n"];
      
  [GPUCode appendFormat: @"            if (oeType == %@_sem_grids->elementTypeTable[FINDEX]) {\n", prefixName];
  [GPUCode appendFormat: @"              F_X2 = %@_sem_grids->X[oidx] + mf * X_in[oidx];\n", prefixName];
  [GPUCode appendFormat: @"              F_Y2 = %@_sem_grids->Y[oidx] + mf * Y_in[oidx];\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"              F_Z2 = %@_sem_grids->Z[oidx] + mf * Z_in[oidx];\n", prefixName];
  [GPUCode appendString: @"\n"];
        
  [GPUCode appendString: @"              V = 0;\n"];
  [GPUCode appendFormat: @"              switch (%@_sem_grids->elementForceTypeTable[FINDEX]) {\n", prefixName];
  [GPUCode appendString: @"                case 0:\n"];
  [GPUCode appendString: @"                  // D_MORSE\n"];
  if (dims == 3) [GPUCode appendString: @"                  r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];
  if (dims == 2) [GPUCode appendString: @"                  r = dist2(F_X1, F_Y1, 0.0, F_X2, F_Y2, 0.0);\n"];
  [GPUCode appendString: @"                  V = D_MORSE(r, PARAM_RHO, PARAM_EQUIL_SQ, PARAM_FACTOR);\n"];
  [GPUCode appendString: @"                  break;\n"];
  [GPUCode appendString: @"\n"];
            
  [GPUCode appendString: @"                case 1:\n"];
  [GPUCode appendString: @"                  // PD_MORSE\n"];
  if (dims == 3) [GPUCode appendString: @"                  r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];
  if (dims == 2) [GPUCode appendString: @"                  r = dist2(F_X1, F_Y1, 0.0, F_X2, F_Y2, 0.0);\n"];
  [GPUCode appendString: @"                  V = PD_MORSE(r, PARAM_RHO, PARAM_EQUIL_SQ, PARAM_FACTOR);\n"];
  [GPUCode appendString: @"                  break;\n"];
  [GPUCode appendString: @"\n"];
            
  [GPUCode appendString: @"                case 2:\n"];
  [GPUCode appendString: @"                  // MORSE\n"];
  if (dims == 3) [GPUCode appendString: @"                  r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];
  if (dims == 2) [GPUCode appendString: @"                  r = dist2(F_X1, F_Y1, 0.0, F_X2, F_Y2, 0.0);\n"];
  [GPUCode appendString: @"                  V = MORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);\n"];
  [GPUCode appendString: @"                  break;\n"];
  [GPUCode appendString: @"\n"];
            
  [GPUCode appendString: @"                case 3:\n"];
  [GPUCode appendString: @"                  // PMORSE\n"];
  if (dims == 3) [GPUCode appendString: @"                  r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];
  if (dims == 2) [GPUCode appendString: @"                  r = dist2(F_X1, F_Y1, 0.0, F_X2, F_Y2, 0.0);\n"];
  [GPUCode appendString: @"                  V = PMORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);\n"];
  [GPUCode appendString: @"                  break;\n"];
  [GPUCode appendString: @"              }\n"];
  [GPUCode appendString: @"              forceX += V * (F_X1 - F_X2);\n"];
  [GPUCode appendString: @"              forceY += V * (F_Y1 - F_Y2);\n"];
  if (dims == 3) [GPUCode appendString: @"              forceZ += V * (F_Z1 - F_Z2);\n"];
  
  [GPUCode appendString: @"            }\n"];
  [GPUCode appendString: @"            break;\n"];
  [GPUCode appendString: @"          }\n"];
  [GPUCode appendString: @"\n"];

  [GPUCode appendString: @"          case FORCE_TYPE_INTER: {\n"];
  [GPUCode appendString: @"            // elements of different cells\n"];
  [GPUCode appendString: @"            break;\n"];
  [GPUCode appendString: @"          }\n"];
  [GPUCode appendString: @"\n"];

  [GPUCode appendString: @"          case FORCE_TYPE_COLONY: {\n"];
  [GPUCode appendString: @"            // elements of different colonies\n"];
  [GPUCode appendString: @"            break;\n"];
  [GPUCode appendString: @"          }\n"];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendString: @"        }\n"];
  [GPUCode appendString: @"\n"];
        
  [GPUCode appendString: @"\n        // next force\n"];
  [GPUCode appendString: @"        ++fIndex;\n"];
  [GPUCode appendFormat: @"        if (fIndex > %@_sem_grids->maxForces) fNum = -1;\n", prefixName];
  [GPUCode appendFormat: @"        else fNum = %@_sem_grids->elementForceTable[FINDEX];\n", prefixName];

  [GPUCode appendString: @"      }\n"];
  [GPUCode appendString: @"   }\n"];
  
  [GPUCode appendString: @"\n   // check singular forces\n"];
  [GPUCode appendString: @"   fIndex = 0;\n"];
  [GPUCode appendFormat: @"   fNum = %@_sem_grids->elementForceTable[FINDEX];\n", prefixName];
  [GPUCode appendString: @"   while (fNum >= 0) {\n"];
  if (dims == 3) [GPUCode appendString: @"     F_X2 = 0; F_Y2 = 0; F_Z2 = 0;\n"];
  if (dims == 2) [GPUCode appendString: @"     F_X2 = 0; F_Y2 = 0;\n"];
  [GPUCode appendFormat: @"     switch (%@_sem_grids->elementForceFlagTable[FINDEX]) {\n", prefixName];
  [GPUCode appendString: @"       case FORCE_TYPE_CELL_CENTER: {\n"];
  [GPUCode appendString: @"         // cell center\n"];
  [GPUCode appendFormat: @"         int cidx = modelNum*%@_sem_grids->idx_cPitch+%@_sem_grids->cellNumber[idx];\n", prefixName, prefixName];
  [GPUCode appendFormat: @"         F_X2 = %@_sem_grids->cellCenterX[cidx];\n", prefixName];
  [GPUCode appendFormat: @"         F_Y2 = %@_sem_grids->cellCenterY[cidx];\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"         F_Z2 = %@_sem_grids->cellCenterZ[cidx];\n", prefixName];
  [GPUCode appendString: @"         break;\n"];
  [GPUCode appendString: @"       }\n"];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendString: @"       case FORCE_TYPE_ELEMENT_CENTER: {\n"];
  [GPUCode appendString: @"         // element center\n"];
  [GPUCode appendFormat: @"         int oeType = %@_sem_grids->elementTypeTable[FINDEX];\n", prefixName];
  [GPUCode appendFormat: @"         int cidx = ((modelNum*%@_sem_grids->maxElementTypes)+oeType)*%@_sem_grids->idx_cPitch+%@_sem_grids->cellNumber[idx];\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"         F_X2 = %@_sem_grids->elementCenterX[cidx];\n", prefixName];
  [GPUCode appendFormat: @"         F_Y2 = %@_sem_grids->elementCenterY[cidx];\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"         F_Z2 = %@_sem_grids->elementCenterZ[cidx];\n", prefixName];
  [GPUCode appendString: @"         break;\n"];
  [GPUCode appendString: @"       }\n"];
  [GPUCode appendString: @"\n"];

  [GPUCode appendString: @"     }\n"];

  [GPUCode appendString: @"     V = 0;\n"];
  [GPUCode appendFormat: @"     switch (%@_sem_grids->elementForceTypeTable[FINDEX]) {\n", prefixName];
  [GPUCode appendString: @"     case 0:\n"];
  [GPUCode appendString: @"       // D_MORSE\n"];
  if (dims == 3) [GPUCode appendString: @"       r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];
  if (dims == 2) [GPUCode appendString: @"       r = dist2(F_X1, F_Y1, 0.0, F_X2, F_Y2, 0.0);\n"];
  [GPUCode appendString: @"       V = D_MORSE(r, PARAM_RHO, PARAM_EQUIL_SQ, PARAM_FACTOR);\n"];
  [GPUCode appendString: @"       break;\n"];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendString: @"     case 1:\n"];
  [GPUCode appendString: @"       // PD_MORSE\n"];
  if (dims == 3) [GPUCode appendString: @"       r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];
  if (dims == 2) [GPUCode appendString: @"       r = dist2(F_X1, F_Y1, 0.0, F_X2, F_Y2, 0.0);\n"];
  [GPUCode appendString: @"       V = PD_MORSE(r, PARAM_RHO, PARAM_EQUIL_SQ, PARAM_FACTOR);\n"];
  [GPUCode appendString: @"       break;\n"];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendString: @"     case 2:\n"];
  [GPUCode appendString: @"       // MORSE\n"];
  if (dims == 3) [GPUCode appendString: @"       r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];
  if (dims == 2) [GPUCode appendString: @"       r = dist2(F_X1, F_Y1, 0.0, F_X2, F_Y2, 0.0);\n"];
  [GPUCode appendString: @"       V = MORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);\n"];
  [GPUCode appendString: @"       break;\n"];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendString: @"     case 3:\n"];
  [GPUCode appendString: @"       // PMORSE\n"];
  if (dims == 3) [GPUCode appendString: @"       r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];
  if (dims == 2) [GPUCode appendString: @"       r = dist2(F_X1, F_Y1, 0.0, F_X2, F_Y2, 0.0);\n"];
  [GPUCode appendString: @"       V = PMORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);\n"];
  [GPUCode appendString: @"       break;\n"];
  [GPUCode appendString: @"         }\n"];
  [GPUCode appendString: @"\n"];

  [GPUCode appendString: @"     forceX += V * (F_X1 - F_X2);\n"];
  [GPUCode appendString: @"     forceY += V * (F_Y1 - F_Y2);\n"];
  if (dims == 3) [GPUCode appendString: @"     forceZ += V * (F_Z1 - F_Z2);\n"];


  [GPUCode appendString: @"\n     // next force\n"];
  [GPUCode appendString: @"     ++fIndex;\n"];
  [GPUCode appendFormat: @"     if (fIndex > %@_sem_grids->maxForces) fNum = -1;\n", prefixName];
  [GPUCode appendFormat: @"     else fNum = %@_sem_grids->elementForceTable[FINDEX];\n", prefixName];
  [GPUCode appendString: @"   }\n"];

  
#if 0
  // loop through all the forces
  for (i = 0; i < [forces count]; ++i) {
    NSDictionary *d = [forces objectAtIndex: i];
    
    //
    // INTRACELLULAR force, between elements within cell
    //
    if ([[d objectForKey: @"type"] isEqualToString: @"INTRA"]) {
      [GPUCode appendString: @"\n   // intracellular\n"];
      [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->numOfElements[cellNum]; ++k) {\n", prefixName];
      [GPUCode appendString: @"      if (k == elemNum) continue;\n"];
      [GPUCode appendFormat: @"      int oidx = k*%@_sem_grids->idx_pitch+cellNum;\n", prefixName];
      [GPUCode appendFormat: @"      F_X2 = %@_sem_grids->X[oidx] + mf * X_in[oidx];\n", prefixName];
      [GPUCode appendFormat: @"      F_Y2 = %@_sem_grids->Y[oidx] + mf * Y_in[oidx];\n", prefixName];
      if (dims == 3) [GPUCode appendFormat: @"      F_Z2 = %@_sem_grids->Z[oidx] + mf * Z_in[oidx];\n", prefixName];
      [GPUCode appendString: @"\n"];

      if (dims == 2) [GPUCode appendString: @"      r = dist2d(F_X1, F_Y1, F_X2, F_Y2);\n"];
      else [GPUCode appendString: @"      r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];

      if ([[d objectForKey: @"potential"] isEqualToString: @"D_MORSE"]
          || [[d objectForKey: @"potential"] isEqualToString: @"PD_MORSE"]) {
        [GPUCode appendFormat: @"      V = %@(r, %@_%@_RHO, %@_%@_EQUIL_SQ, %@_%@_FACTOR);\n",
         [d objectForKey: @"potential"], prefixName, [d objectForKey: @"type"], prefixName,
         [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"]];
      }
      
      if ([[d objectForKey: @"potential"] isEqualToString: @"MORSE"]
          || [[d objectForKey: @"potential"] isEqualToString: @"PMORSE"]) {
        [GPUCode appendFormat: @"      V = %@(r, %@_%@_U0, %@_%@_ETA0, %@_%@_U1, %@_%@_ETA1);\n",
         [d objectForKey: @"potential"], prefixName, [d objectForKey: @"type"], prefixName,
         [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"]];
      }
      
      [GPUCode appendString: @"\n"];
      [GPUCode appendString: @"      forceX += V * (F_X1 - F_X2);\n"];
      [GPUCode appendString: @"      forceY += V * (F_Y1 - F_Y2);\n"];
      if (dims == 3) [GPUCode appendString: @"      forceZ += V * (F_Z1 - F_Z2);\n"];

      // TODO: periodic boundary conditions

      [GPUCode appendString: @"   }\n"];

      continue;
    }
    
    //
    // INTERCELLULAR force, between elements of another cell
    //
    if ([[d objectForKey: @"type"] isEqualToString: @"INTER"]) {
      [GPUCode appendString: @"\n   // intercellular\n"];
      [GPUCode appendFormat: @"   for (j = 0; j < %@_sem_grids->numOfCells; ++j) {\n", prefixName];
      [GPUCode appendString: @"      if (j == cellNum) continue;\n"];

      // TODO: optimize

      [GPUCode appendFormat: @"      for (k = 0; k < %@_sem_grids->numOfElements[j]; ++k) {\n", prefixName];
      [GPUCode appendFormat: @"         int oidx = k*%@_sem_grids->idx_pitch+j;\n", prefixName];
      [GPUCode appendFormat: @"         F_X2 = %@_sem_grids->X[oidx] + mf * X_in[oidx];\n", prefixName];
      [GPUCode appendFormat: @"         F_Y2 = %@_sem_grids->Y[oidx] + mf * Y_in[oidx];\n", prefixName];
      if (dims == 3) [GPUCode appendFormat: @"         F_Z2 = %@_sem_grids->Z[oidx] + mf * Z_in[oidx];\n", prefixName];
      [GPUCode appendString: @"\n"];
      
      if (dims == 2) [GPUCode appendString: @"         r = dist2d(F_X1, F_Y1, F_X2, F_Y2);\n"];
      else [GPUCode appendString: @"         r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);\n"];

      if ([[d objectForKey: @"potential"] isEqualToString: @"D_MORSE"]
          || [[d objectForKey: @"potential"] isEqualToString: @"PD_MORSE"]) {
        [GPUCode appendFormat: @"         V = %@(r, %@_%@_RHO, %@_%@_EQUIL_SQ, %@_%@_FACTOR);\n",
         [d objectForKey: @"potential"], prefixName, [d objectForKey: @"type"], prefixName,
         [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"]];
      }
      
      if ([[d objectForKey: @"potential"] isEqualToString: @"MORSE"]
          || [[d objectForKey: @"potential"] isEqualToString: @"PMORSE"]) {
        [GPUCode appendFormat: @"         V = %@(r, %@_%@_U0, %@_%@_ETA0, %@_%@_U1, %@_%@_ETA1);\n",
         [d objectForKey: @"potential"], prefixName, [d objectForKey: @"type"], prefixName,
         [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"]];
      }
      
      // TODO: repulse only flag
      
      [GPUCode appendString: @"\n"];
      [GPUCode appendString: @"         forceX += V * (F_X1 - F_X2);\n"];
      [GPUCode appendString: @"         forceY += V * (F_Y1 - F_Y2);\n"];
      if (dims == 3) [GPUCode appendString: @"         forceZ += V * (F_Z1 - F_Z2);\n"];

      // TODO: periodic boundary conditions

      [GPUCode appendString: @"      }\n"];
      [GPUCode appendString: @"   }\n"];

      continue;
    }
    
#if 0
    //
    // BOUNDARY force, between elements and some boundary
    //
    // TODO: generalize
    if ([[d objectForKey: @"type"] isEqualToString: @"BOUNDARY"]) {
      //NSString *bm = [d objectForKey: @"boundary"];
      [GPUCode appendString: @"\n// boundary\n"];
      id et = [d objectForKey: @"element"];
      if (et) [GPUCode appendFormat: @"if (%@_sem_grids->elementType[idx] == %d) {\n", prefixName, [et intValue]];
      
      if ([[d objectForKey: @"potential"] isEqualToString: @"D_MORSE"]
          || [[d objectForKey: @"potential"] isEqualToString: @"PD_MORSE"]) {
        [GPUCode appendFormat: @"  r = %@_sem_grids->Z[idx] * %@_sem_grids->Z[idx];\n", prefixName, prefixName];
        [GPUCode appendFormat: @"  V = %@(r, %@_%@_RHO, %@_%@_EQUIL_SQ, %@_%@_FACTOR);\n",
         [d objectForKey: @"potential"], prefixName, [d objectForKey: @"type"], prefixName,
         [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"]];
      }
      
      if ([[d objectForKey: @"potential"] isEqualToString: @"MORSE"]
          || [[d objectForKey: @"potential"] isEqualToString: @"PMORSE"]) {
        [GPUCode appendFormat: @"  r = %@_sem_grids->Z[idx];\n", prefixName];
        [GPUCode appendFormat: @"  V = %@(r, %@_%@_U0, %@_%@_ETA0, %@_%@_U1, %@_%@_ETA1);\n",
         [d objectForKey: @"potential"], prefixName, [d objectForKey: @"type"], prefixName,
         [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"], prefixName, [d objectForKey: @"type"]];
      }
      
      [GPUCode appendFormat: @"  forceZ += V * %@_sem_grids->Z[idx];\n", prefixName, prefixName];
      if (et) [GPUCode appendString: @"}\n"];
      continue;
    }
#endif

  }
#endif
  
  [GPUCode appendString: @"\n   // result\n"];
  [GPUCode appendString: @"   X_out[idx] = mh * forceX;\n"];
  [GPUCode appendString: @"   Y_out[idx] = mh * forceY;\n"];
  if (dims == 3) [GPUCode appendString: @"   Z_out[idx] = mh * forceZ;\n"];
  
  // TODO: boundary conditions
#if 0
#if PERIODIC_BOUNDARY
  if (X_F1[elemNum*pitch+cellNum] < 0.0) X_F1[elemNum*pitch+cellNum] = X_F1[elemNum*pitch+cellNum] + BOUNDARY_X;
  if (X_F1[elemNum*pitch+cellNum] > BOUNDARY_X) X_F1[elemNum*pitch+cellNum] = X_F1[elemNum*pitch+cellNum] - BOUNDARY_X;
  if (Y_F1[elemNum*pitch+cellNum] < 0.0) Y_F1[elemNum*pitch+cellNum] = Y_F1[elemNum*pitch+cellNum] + BOUNDARY_Y;
  if (Y_F1[elemNum*pitch+cellNum] > BOUNDARY_Y) Y_F1[elemNum*pitch+cellNum] = Y_F1[elemNum*pitch+cellNum] - BOUNDARY_Y;
  //if (Z_F1[elemNum*pitch+cellNum] < 0.0) Z_F1[elemNum*pitch+cellNum] = Z_F1[elemNum*pitch+cellNum] + BOUNDARY_Z;
  //if (Z_F1[elemNum*pitch+cellNum] > BOUNDARY_Z) Z_F1[elemNum*pitch+cellNum] = Z_F1[elemNum*pitch+cellNum] - BOUNDARY_Z;
#endif
  
#if HARD_Z
  if (Z_F1[elemNum*pitch+cellNum] < 0.0) Z_F1[elemNum*pitch+cellNum] = 0.0;
  if (Z_F1[elemNum*pitch+cellNum] > BOUNDARY_Z) Z_F1[elemNum*pitch+cellNum] = BOUNDARY_Z;
#endif
#endif
  
  [GPUCode appendString: @"}\n"];

  return GPUCode;
}

- (NSMutableString *)kernelGPUCode: (NSString *)prefixName
{
  int dims = [[super spatialModel] dimensions];

  NSMutableString *GPUCode = [NSMutableString new];

  // kernel to calculate centers
  [GPUCode appendString: [self centerKernelGPUCode: prefixName]];

  // kernel to calculate SEM movement function
  //[GPUCode appendString: [self functionKernelGPUCode: prefixName]];
  
  // kernel for numerical method
  if ([numericalScheme isEqualToString: @"RungeKutta2ndOrder"]) {
    [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_SEM_kernel1_2rk(float currentTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    if (useMesh)
      [GPUCode appendFormat: @"   BC_SEM_mesh_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    else
      [GPUCode appendFormat: @"   BC_SEM_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    [GPUCode appendString: @"}\n"];
    
    [GPUCode appendString: @"\n// F2 Kernel for 2nd order Runge-Kutta\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_SEM_kernel2_2rk(float currentTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    if (useMesh)
      [GPUCode appendFormat: @"   BC_SEM_mesh_kernel2_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    else
      [GPUCode appendFormat: @"   BC_SEM_kernel2_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    [GPUCode appendString: @"}\n"];

    [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta, pairwise algorithm\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_SEM_pairwise_kernel1_2rk(float currentTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    [GPUCode appendFormat: @"   BC_SEM_pairwise_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    [GPUCode appendString: @"}\n"];

    [GPUCode appendString: @"\n// F2 Kernel for 2nd order Runge-Kutta, pairwise algorithm\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_SEM_pairwise_kernel2_2rk(float currentTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    [GPUCode appendFormat: @"   BC_SEM_pairwise_kernel2_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    [GPUCode appendString: @"}\n"];

    if (!useMesh) {
      [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta, neighbor pair algorithm\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_neighbor_kernel1_2rk(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_neighbor_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Sum forces for F1\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_neighbor_sum_kernel1_2rk(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_neighbor_sum_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// F2 Kernel for 2nd order Runge-Kutta, neighbor pair algorithm\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_neighbor_kernel2_2rk(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_neighbor_kernel2_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Sum forces for F2 and update element positions\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_neighbor_update_kernel_2rk(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_neighbor_update_kernel_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Determine if need new sectors\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_sector_count_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_sector_count_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Create new sectors\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_create_sectors_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_create_sectors_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];
      
      [GPUCode appendString: @"\n// Update sector neighbor table\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_build_sector_neighbors_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_build_sector_neighbors_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Move elements to their sectors\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_move_elements_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_move_elements_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Update sector tables\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_update_element_sector_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_update_element_sector_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Update element neighbor table\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_update_element_neighbor_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_update_element_neighbor_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];
    }

  } else {
    printf("ERROR: unknown numerical scheme: %s\n", [numericalScheme UTF8String]);
    return nil;
  }
  
  return GPUCode;
}

- (NSMutableString *)cleanupGPUCode: (NSString *)prefixName
{
  return nil;
}

@end

//
// Run GPU code
//
@implementation SubcellularElementModel (GPURun)

- (void *)allocGPUData: (int)numModels withGPUFunctions: (void *)gpuFunctions
{
  ModelGPUData *gpuData = (ModelGPUData *)malloc(sizeof(ModelGPUData));
  int dims = [[super spatialModel] dimensions];
  
  id gpuElementData = [[MultiScale2DGrid alloc] initWithWidth: maxElements andHeight: numModels];
  [gpuElementData newGridWithName: @"X" atScale: 1 withEncode: dataEncode];
  [gpuElementData newGridWithName: @"Y" atScale: 1 withEncode: dataEncode];
  [gpuElementData newGridWithName: @"Z" atScale: 1 withEncode: dataEncode];
  [gpuElementData newGridWithName: @"type" atScale: 1 withEncode: [BioSwarmModel intEncode]];
  [gpuElementData newGridWithName: @"cell" atScale: 1 withEncode: [BioSwarmModel intEncode]];
  [gpuElementData newGridWithName: @"sector" atScale: 1 withEncode: [BioSwarmModel intEncode]];
  gpuData->data = gpuElementData;
  gpuData->numModels = numModels;
  gpuData->numParams = [self numOfParameters];
  gpuData->gpuFunctions = gpuFunctions;
  
  gpuData->parameters = (float *)malloc(sizeof(float) * numModels * gpuData->numParams);
  NSMutableDictionary *d = [NSMutableDictionary new];
  gpuData->data2 = (void *)d;

  printf("numOfSpecies = %d\n", numOfSpecies);
  NSValue *v;
  if (numOfSpecies) {
    float *sd = (float *)malloc(sizeof(float) * numModels * numOfSpecies);
    v = [NSValue value: &sd withObjCType: @encode(float *)];
    [d setObject: v forKey: @"speciesData"];
  }

  int *sc = (int *)malloc(sizeof(int) * 3 * maxSectors * numModels);
  v = [NSValue value: &sc withObjCType: @encode(int *)];
  [d setObject: v forKey: @"sectorCoordinates"];
  //printf("sectorCoordinates = %p\n", sc);

  sc = (int *)malloc(sizeof(int) * numModels);
  v = [NSValue value: &sc withObjCType: @encode(int *)];
  [d setObject: v forKey: @"numSectors"];

  float *sd = (float *)malloc(sizeof(float) * numModels);
  v = [NSValue value: &sd withObjCType: @encode(float *)];
  [d setObject: v forKey: @"sectorSize"];

  sc = (int *)malloc(sizeof(int) * maxElementsPerSector * maxSectors * numModels);
  v = [NSValue value: &sc withObjCType: @encode(int *)];
  [d setObject: v forKey: @"sectorElementTable"];

  int nSize;
  if (dims == 2) nSize = S_XY_TOT;
  else nSize = S_XYZ_TOT;
  
  sc = (int *)malloc(sizeof(int) * maxSectors * nSize * numModels);
  v = [NSValue value: &sc withObjCType: @encode(int *)];
  [d setObject: v forKey: @"sectorNeighborTable"];
  //printf("sectorNeighborTable = %p\n", sc);
#if 0
  sc = (int *)malloc(sizeof(int) * maxElements * nSize * maxElementsPerSector * numModels);
  v = [NSValue value: &sc withObjCType: @encode(int *)];
  [d setObject: v forKey: @"elementNeighborTable"];
  //printf("elementNeighborTable = %p\n", sc);
#endif
  sc = (int *)malloc(sizeof(int) * maxSectors * numModels);
  v = [NSValue value: &sc withObjCType: @encode(int *)];
  [d setObject: v forKey: @"readOnlySector"];

  return gpuData;
}

- (void)freeGPUData: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  
  if (gpuData) {
    id m = gpuData->data;
    if (m) [m release];
    if (gpuData->parameters) free(gpuData->parameters);
    if (gpuData->data2) {
      NSMutableDictionary *d = (NSMutableDictionary *)gpuData->data2;
      void *sd = NULL;
      NSValue *v = [d objectForKey: @"speciesData"];
      if (v) [v getValue: &sd];
      if (sd) free(sd);

      sd = NULL;
      v = [d objectForKey: @"sectorCoordinates"];
      if (v) [v getValue: &sd];
      if (sd) free(sd);

      sd = NULL;
      v = [d objectForKey: @"numSectors"];
      if (v) [v getValue: &sd];
      if (sd) free(sd);

      sd = NULL;
      v = [d objectForKey: @"sectorSize"];
      if (v) [v getValue: &sd];
      if (sd) free(sd);

      sd = NULL;
      v = [d objectForKey: @"sectorElementTable"];
      if (v) [v getValue: &sd];
      if (sd) free(sd);

      sd = NULL;
      v = [d objectForKey: @"sectorNeighborTable"];
      if (v) [v getValue: &sd];
      if (sd) free(sd);
#if 0
      sd = NULL;
      v = [d objectForKey: @"elementNeighborTable"];
      if (v) [v getValue: &sd];
      if (sd) free(sd);
#endif
      sd = NULL;
      v = [d objectForKey: @"readOnlySector"];
      if (v) [v getValue: &sd];
      if (sd) free(sd);

      [d release];
    }
    
    free(gpuData);
  }
}

- (void)assignData: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int dims = [[super spatialModel] dimensions];
  int i, j, k;
  
  Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *T = [[elementData gridWithName: @"type"] objectForKey: @"matrix"];
  Grid2DArray *C = [[elementData gridWithName: @"cell"] objectForKey: @"matrix"];
  Grid2DArray *S = [[elementData gridWithName: @"sector"] objectForKey: @"matrix"];
  float (*xGrid)[1][maxElements] = [X getMatrix];
  float (*yGrid)[1][maxElements] = [Y getMatrix];
  float (*zGrid)[1][maxElements] = [Z getMatrix];
  int (*etGrid)[1][maxElements] = [T getMatrix];
  int (*cGrid)[1][maxElements] = [C getMatrix];
  int (*sGrid)[1][maxElements] = [S getMatrix];
  
  id gpuElementData = gpuData->data;
  Grid2DArray *gX = [[gpuElementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *gY = [[gpuElementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *gZ = [[gpuElementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *gT = [[gpuElementData gridWithName: @"type"] objectForKey: @"matrix"];
  Grid2DArray *gC = [[gpuElementData gridWithName: @"cell"] objectForKey: @"matrix"];
  Grid2DArray *gS = [[gpuElementData gridWithName: @"sector"] objectForKey: @"matrix"];
  float (*gxGrid)[gpuData->numModels][maxElements] = [gX getMatrix];
  float (*gyGrid)[gpuData->numModels][maxElements] = [gY getMatrix];
  float (*gzGrid)[gpuData->numModels][maxElements] = [gZ getMatrix];
  int (*getGrid)[gpuData->numModels][maxElements] = [gT getMatrix];
  int (*gcGrid)[gpuData->numModels][maxElements] = [gC getMatrix];
  int (*gsGrid)[gpuData->numModels][maxElements] = [gS getMatrix];
  
  float *indData = speciesData;
  NSMutableDictionary *d = (NSMutableDictionary *)gpuData->data2;
  void *sd = NULL;
  NSValue *v = [d objectForKey: @"speciesData"];
  if (v) [v getValue: &sd];
  float (*hostData)[numOfSpecies][gpuData->numModels] = sd;

  sd = NULL;
  v = [d objectForKey: @"sectorCoordinates"];
  if (v) [v getValue: &sd];
  int (*hostSectorCoordinates)[3 * gpuData->numModels][maxSectors] = sd;
  int (*sCoordinates)[3][maxSectors] = (void *)sectorCoordinates;

  sd = NULL;
  v = [d objectForKey: @"numSectors"];
  if (v) [v getValue: &sd];
  int (*hostNumSectors)[gpuData->numModels] = sd;

  sd = NULL;
  v = [d objectForKey: @"sectorSize"];
  if (v) [v getValue: &sd];
  float (*hostSectorSize)[gpuData->numModels] = sd;

  sd = NULL;
  v = [d objectForKey: @"sectorElementTable"];
  if (v) [v getValue: &sd];
  int (*hostSectorElementTable)[maxElementsPerSector * gpuData->numModels][maxSectors] = sd;
  int (*seTable)[maxElementsPerSector][maxSectors] = (void *)sectorElementTable;

  int nSize;
  if (dims == 2) nSize = S_XY_TOT;
  else nSize = S_XYZ_TOT;

  sd = NULL;
  v = [d objectForKey: @"sectorNeighborTable"];
  if (v) [v getValue: &sd];
  //printf("sectorNeighborTable = %p\n", sd);
  int (*hostSectorNeighborTable)[nSize * gpuData->numModels][maxSectors] = sd;
  int (*snTable)[nSize][maxSectors] = (void *)sectorNeighborTable;

#if 0
  sd = NULL;
  v = [d objectForKey: @"elementNeighborTable"];
  if (v) [v getValue: &sd];
  //printf("elementNeighborTable = %p\n", sd);
  int (*hostElementNeighborTable)[nSize * maxElementsPerSector * gpuData->numModels][maxElements] = sd;
  int (*enTable)[nSize * maxElementsPerSector][maxElements] = (void *)elementNeighborTable;
#endif
  sd = NULL;
  v = [d objectForKey: @"readOnlySector"];
  if (v) [v getValue: &sd];
  int (*hostReadOnlySector)[gpuData->numModels][maxSectors] = sd;
  int (*roSector)[maxSectors] = (void *)sectorReadOnlyFlag;

  if (aFlag) {
    // transfer to GPU
    for (j = 0; j < totalElements; ++j) {
      (*gxGrid)[aNum][j] = (*xGrid)[0][j];
      (*gyGrid)[aNum][j] = (*yGrid)[0][j];
      (*gzGrid)[aNum][j] = (*zGrid)[0][j];
      (*getGrid)[aNum][j] = (*etGrid)[0][j];
      (*gcGrid)[aNum][j] = (*cGrid)[0][j];
      (*gsGrid)[aNum][j] = (*sGrid)[0][j];
#if 0
      for (i = 0; i < nSize; ++i) {
        for (k = 0; k < maxElementsPerSector; ++k) {
          (*hostElementNeighborTable)[(nSize * maxElementsPerSector * aNum) + (maxElementsPerSector * i) + k][j] = (*enTable)[(maxElementsPerSector * i) + k][j];
        }
      }
#endif
    }
    if (numOfSpecies) for (j = 0; j < numOfSpecies; ++j) (*hostData)[j][aNum] = indData[j];

    (*hostSectorSize)[aNum] = sectorSize;
    (*hostNumSectors)[aNum] = numSectors;
    for (i = 0; i < maxSectors; ++i) {
      (*hostSectorCoordinates)[0 + (3 * aNum)][i] = (*sCoordinates)[0][i];
      (*hostSectorCoordinates)[1 + (3 * aNum)][i] = (*sCoordinates)[1][i];
      (*hostSectorCoordinates)[2 + (3 * aNum)][i] = (*sCoordinates)[2][i];
      //printf("sector (%d) coordinates (%d, %d, %d)\n", i, (*hostSectorCoordinates)[0 + (3 * aNum)][i], (*hostSectorCoordinates)[1 + (3 * aNum)][i], (*hostSectorCoordinates)[2 + (3 * aNum)][i]);
      for (j = 0; j < maxElementsPerSector; ++j) (*hostSectorElementTable)[j + (maxElementsPerSector * aNum)][i] = (*seTable)[j][i];
      for (j = 0; j < nSize; ++j) (*hostSectorNeighborTable)[j + (nSize * aNum)][i] = (*snTable)[j][i];
      (*hostReadOnlySector)[aNum][i] = (*roSector)[i];
    }
  } else {
    // transfer from GPU
    for (j = 0; j < totalElements; ++j) {
      (*xGrid)[0][j] = (*gxGrid)[aNum][j];
      (*yGrid)[0][j] = (*gyGrid)[aNum][j];
      (*zGrid)[0][j] = (*gzGrid)[aNum][j];
      (*etGrid)[0][j] = (*getGrid)[aNum][j];
      (*cGrid)[0][j] = (*gcGrid)[aNum][j];
      (*sGrid)[0][j] = (*gsGrid)[aNum][j];
#if 0
      for (i = 0; i < nSize; ++i) {
        for (k = 0; k < maxElementsPerSector; ++k) {
          (*enTable)[(maxElementsPerSector * i) + k][j] = (*hostElementNeighborTable)[(nSize * maxElementsPerSector * aNum) + (maxElementsPerSector * i) + k][j];
        }
      }
#endif
    }
    if (numOfSpecies) for (j = 0; j < numOfSpecies; ++j) indData[j] = (*hostData)[j][aNum];
    
    sectorSize = (*hostSectorSize)[aNum];
    numSectors = (*hostNumSectors)[aNum];
    for (i = 0; i < maxSectors; ++i) {
      (*sCoordinates)[0][i] = (*hostSectorCoordinates)[0 + (3 * aNum)][i];
      (*sCoordinates)[1][i] = (*hostSectorCoordinates)[1 + (3 * aNum)][i];
      (*sCoordinates)[2][i] = (*hostSectorCoordinates)[2 + (3 * aNum)][i];
      for (j = 0; j < maxElementsPerSector; ++j) (*seTable)[j][i] = (*hostSectorElementTable)[j + (maxElementsPerSector * aNum)][i];
      for (j = 0; j < nSize; ++j) (*snTable)[j][i] = (*hostSectorNeighborTable)[j + (nSize * aNum)][i];
    }
  }
#if 0
  for (i = 0; i < gpuData->numModels; ++i) {
    for (j = 0; j < totalElements; ++j) {
      printf("%d %d: %f %f %f\n", i, j, (*gxGrid)[i][j], (*gyGrid)[i][j], (*gzGrid)[i][j]);
    }
  }
#endif
}

- (int)assignParameters: (void *)data ofNumber: (int)aNum toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  int i, p = 0;
  NSString *s;

  float (*hostParameters)[gpuData->numParams][gpuData->numModels] = (void *)gpuData->parameters;
    
  if (aFlag) {
    (*hostParameters)[p][aNum] = interactionMaxSquared;
    ++p;

    for (i = 0; i < [forces count]; ++i) {
      NSMutableDictionary *d = [forces objectAtIndex: i];
      NSString *forceName = [d objectForKey: @"name"];
      
      if ([[d objectForKey: @"potential"] isEqualToString: @"D_MORSE"]
          || [[d objectForKey: @"potential"] isEqualToString: @"PD_MORSE"]) {
        (*hostParameters)[p][aNum] = [[d objectForKey: @"rho"] floatValue];
        printf("assign parameter (%d, %d) rho = %f\n", p, aNum, (*hostParameters)[p][aNum]);
        ++p;

        (*hostParameters)[p][aNum] = [[d objectForKey: @"factor"] floatValue];
        printf("assign parameter (%d, %d) factor = %f\n", p, aNum, (*hostParameters)[p][aNum]);
        ++p;

        (*hostParameters)[p][aNum] = [[d objectForKey: @"eqSquared"] floatValue];
        printf("assign parameter (%d, %d) eqSquared = %f\n", p, aNum, (*hostParameters)[p][aNum]);
        ++p;
      }
      
      if ([[d objectForKey: @"potential"] isEqualToString: @"MORSE"]
          || [[d objectForKey: @"potential"] isEqualToString: @"PMORSE"]) {
        //printf("force: %s\n", [[d description] UTF8String]);
        s = [NSString stringWithFormat: @"%@_U0", forceName];
        (*hostParameters)[p][aNum] = [[d objectForKey: s] floatValue];
        printf("assign parameter (%d, %d) U0 = %f\n", p, aNum, (*hostParameters)[p][aNum]);
        ++p;

        s = [NSString stringWithFormat: @"%@_U1", forceName];
        (*hostParameters)[p][aNum] = [[d objectForKey: s] floatValue];
        printf("assign parameter (%d, %d) U1 = %f\n", p, aNum, (*hostParameters)[p][aNum]);
        ++p;

        s = [NSString stringWithFormat: @"%@_ETA0", forceName];
        (*hostParameters)[p][aNum] = [[d objectForKey: s] floatValue];
        printf("assign parameter (%d, %d) ETA0 = %f\n", p, aNum, (*hostParameters)[p][aNum]);
        ++p;

        s = [NSString stringWithFormat: @"%@_ETA1", forceName];
        (*hostParameters)[p][aNum] = [[d objectForKey: s] floatValue];
        printf("assign parameter (%d, %d) ETA1 = %f\n", p, aNum, (*hostParameters)[p][aNum]);
        ++p;
      }
    }
    
  } else {
  }
    
  return p;
}


- (void)allocGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  SEMfunctions *gpuFunctions = (SEMfunctions *)gpuData->gpuFunctions;
  gpuData->gpuPtrs = (gpuFunctions->allocGPUKernel)(self, [[super spatialModel] dimensions], gpuData->numModels, maxCells, maxElements, maxElementTypes, maxForces, maxSectors, maxElementsPerSector, gpuData->numParams, (int)calcCellCenters, (int)calcElementCenters, numOfSpecies, (int)useMesh, (float)dt);
}

- (void)transferGPUKernel: (void *)data toGPU: (BOOL)aFlag
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  SEMfunctions *gpuFunctions = (SEMfunctions *)gpuData->gpuFunctions;
  id gpuElementData = gpuData->data;
  Grid2DArray *gX = [[gpuElementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *gY = [[gpuElementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *gZ = [[gpuElementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *gT = [[gpuElementData gridWithName: @"type"] objectForKey: @"matrix"];
  Grid2DArray *gC = [[gpuElementData gridWithName: @"cell"] objectForKey: @"matrix"];
  Grid2DArray *gS = [[gpuElementData gridWithName: @"sector"] objectForKey: @"matrix"];
  NSMutableDictionary *d = (NSMutableDictionary *)gpuData->data2;

  void *hostSpeciesData = NULL;
  NSValue *v = [d objectForKey: @"speciesData"];
  if (v) [v getValue: &hostSpeciesData];

  void *hostSectorCoordinates = NULL;
  v = [d objectForKey: @"sectorCoordinates"];
  if (v) [v getValue: &hostSectorCoordinates];
  
  void *hostNumSectors = NULL;
  v = [d objectForKey: @"numSectors"];
  if (v) [v getValue: &hostNumSectors];
  
  void *hostSectorSize = NULL;
  v = [d objectForKey: @"sectorSize"];
  if (v) [v getValue: &hostSectorSize];
  
  void *hostSectorElementTable = NULL;
  v = [d objectForKey: @"sectorElementTable"];
  if (v) [v getValue: &hostSectorElementTable];

  void *hostSectorNeighborTable = NULL;
  v = [d objectForKey: @"sectorNeighborTable"];
  if (v) [v getValue: &hostSectorNeighborTable];
#if 0
  void *hostElementNeighborTable = NULL;
  v = [d objectForKey: @"elementNeighborTable"];
  if (v) [v getValue: &hostElementNeighborTable];
#endif
  void *hostReadOnlySector = NULL;
  v = [d objectForKey: @"readOnlySector"];
  if (v) [v getValue: &hostReadOnlySector];

#if 0
  int dims = [[super spatialModel] dimensions];
  int nSize, i;
  if (dims == 2) nSize = S_XY_TOT;
  else nSize = S_XYZ_TOT;
  int (*enTable)[nSize * maxElementsPerSector][maxElements] = (void *)hostElementNeighborTable;
  for (i = 0; i < (nSize * maxElementsPerSector); ++i) {
    if ( (*enTable)[i][0] != -1) printf("transfer en = (%d, %d)\n", i, (*enTable)[i][0]);
  }
#endif
  
  (gpuFunctions->initGPUKernel)(self, gpuData->gpuPtrs, (int)aFlag, gpuData->parameters, [gC getMatrix], numOfCells, totalElements,
                                [gX getMatrix], [gY getMatrix], [gZ getMatrix], [gT getMatrix], [gS getMatrix],
                                NULL, elementForceTable, elementForceTypeTable, elementTypeTable, elementForceFlagTable,
                                hostSpeciesData, meshNeighbors,
                                hostSectorCoordinates, hostNumSectors, hostSectorSize, hostSectorElementTable, hostSectorNeighborTable, hostReadOnlySector);
}

- (void)invokeGPUKernel: (void *)data currentTime: (double)currentTime endTime: (double)endTime
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  SEMfunctions *gpuFunctions = (SEMfunctions *)gpuData->gpuFunctions;

  (gpuFunctions->invokeGPUKernel)(self, gpuData->gpuPtrs, currentTime, endTime);
}

- (void)releaseGPUKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  SEMfunctions *gpuFunctions = (SEMfunctions *)gpuData->gpuFunctions;
  (gpuFunctions->releaseGPUKernel)(self, gpuData->gpuPtrs);
}

@end

//
// Visualization
//

@implementation SubcellularElementModel (Povray)

- (NSMutableString *)povrayCode
{
  int i, j;

  Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
  Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
  Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
  Grid2DArray *T = [[elementData gridWithName: @"type"] objectForKey: @"matrix"];
  Grid2DArray *C = [[elementData gridWithName: @"cell"] objectForKey: @"matrix"];

  float (*xGrid)[0][maxElements] = [X getMatrix];
  float (*yGrid)[0][maxElements] = [Y getMatrix];
  float (*zGrid)[0][maxElements] = [Z getMatrix];
  int (*etGrid)[0][maxElements] = [T getMatrix];
  int (*cGrid)[0][maxElements] = [C getMatrix];

  NSDictionary *povraySpec = [model objectForKey: @"povray"];
  BOOL translateOrigin = NO;
  id s = [povraySpec objectForKey: @"translateToOrigin"];
  if (s) translateOrigin = [s boolValue];

  BOOL blob = YES;
  s = [povraySpec objectForKey: @"blob"];
  if (s) blob = [s boolValue];

  NSString *shape = @"sphere";
  s = [povraySpec objectForKey: @"shape"];
  if (s) shape = s;

  double radius = 0.12;
  s = [povraySpec objectForKey: @"radius"];
  if (s) radius = [s doubleValue];

  NSMutableString *povray = [NSMutableString new];

  // get max cell number
  int maxNum = -1;
  for (i = 0; i < totalElements; ++i) {
    if ((*cGrid)[0][i] > maxNum) maxNum = (*cGrid)[0][i];
  }

  double transX = 0, transY = 0, transZ = 0;
  if (translateOrigin) {
    transX = 0 - (*xGrid)[0][0];
    transY = 0 - (*yGrid)[0][0];
    transZ = 0 - (*zGrid)[0][0];
  }

  for (j = 0; j <= maxNum; ++j) {
    BOOL first = YES;
    for (i = 0; i < totalElements; ++i) {
      if ((*cGrid)[0][i] == j) {
        if (first) {
          if (blob) {
            [povray appendString: @"blob {\n"];
            [povray appendString: @"  threshold 0.1\n"];
          }
          //[povray appendString: @"  threshold 0.5\n"];
          first = NO;
        }
        
        if ([shape isEqualToString: @"sphere"]) {
          [povray appendString: @"  sphere {\n"];
          [povray appendFormat: @"    <%f, %f, %f>, %f", (*xGrid)[0][i] + transX, (*yGrid)[0][i] + transY, (*zGrid)[0][i] + transZ, radius];
          if (blob) [povray appendString: @" strength 1"];
          [povray appendString: @"\n"];
          //[povray appendFormat: @"    <%f, %f, %f>, %f strength 1\n", (*xGrid)[0][i] + transX, (*yGrid)[0][i] + transY, (*zGrid)[0][i] + transZ, 0.1];
        //[povray appendFormat: @"    <%f, %f, %f>, %f strength 1\n", (*xGrid)[i][j], (*yGrid)[i][j], (*zGrid)[i][j], cellRadius/2.0];
          [povray appendString: @"    texture {\n"];
          if ((*etGrid)[0][i])
            [povray appendFormat: @"      pigment { rgb <%f, 0.5, 0.5> }\n", 1.0*(j+1)/(maxNum+1)];
          else
            [povray appendFormat: @"      pigment { rgb <%f, 0.5, 0.0> }\n", 1.0*(j+1)/(maxNum+1)];
          [povray appendString: @"    }\n"];
          [povray appendString: @"  }\n"];
        }
      }
    }

    if (!first) {
      if (blob) [povray appendString: @"}"];
      [povray appendString: @"\n\n"];
    }
  }

  return povray;
}

@end


//
// MPI
//
@implementation SubcellularElementModel (MPI)

- (void)allocateMPISimulationWithEncode: (NSString *)anEncode
{
  int dims = [[super spatialModel] dimensions];
  printf("ScEM allocateMPISimulationWithEncode\n");

  int neighborSize, neighborSectorSize;
  if (dims == 2) { neighborSize = S_XY_TOT; neighborSectorSize = sectorsInDomain; }
  else { neighborSize = S_XYZ_TOT; neighborSectorSize = sectorsInDomain*sectorsInDomain; }

  outgoingSectors = malloc(sizeof(int) * neighborSectorSize * neighborSize);
  outgoingElements = malloc(sizeof(float) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize);
  incomingSectors = malloc(sizeof(int) * neighborSectorSize * neighborSize);
  incomingElements = malloc(sizeof(float) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize);
  availableElements = [NSMutableArray new];

  int i, direction;
  float (*iElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = (void *)incomingElements;
  for (direction = S_START; direction < neighborSize; ++direction)
    for (i = 0; i < 4 * neighborSectorSize * maxElementsPerSector; ++i)
      (*iElements)[direction][i] = -1.0;
}

- (void)initializeMPISimulation
{
  int i, j;
  int dims = [[super spatialModel] dimensions];

  if (dims == 2) {
    int (*oSectors)[sectorsInDomain][S_XY_TOT] = outgoingSectors;
    int (*iSectors)[sectorsInDomain][S_XY_TOT] = incomingSectors;
    int (*sCoordinates)[3][maxSectors] = (void *)sectorCoordinates;

    for (i = 0; i < S_XY_TOT; ++i)
      for (j = 0; j < sectorsInDomain; ++j) {
	(*iSectors)[j][i] = -1;
	(*oSectors)[j][i] = -1;
      }

    // setup MPI neighbor incoming/outgoing sector tables

    // incoming sectors are the outside border sectors
    // they have coordinates of -1 or sectorsInDomain

    // outgoing sectors are the outside border sectors
    // they have coordinates of 0 or (sectorsInDomain - 1)

    int direction;
    for (direction = S_START; direction < S_XY_TOT; ++direction) {
      switch (direction) {

      case S_LEFT: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    //printf("S_LEFT outgoingSector: %d, %d\n", oNum, i);
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    //printf("S_RIGHT incomingSector: %d, %d\n", iNum, i);
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_UP: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[1][i] == sectorsInDomain) && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    //printf("S_UP incomingSector: %d, %d\n", iNum, i);
	    ++iNum;
	  }
	  if (((*sCoordinates)[1][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_DOWN: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[1][i] == -1) && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[1][i] == 0) && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    //printf("S_DOWN outgoingSector: %d, %d\n", oNum, i);
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_UP: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] == sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] == (sectorsInDomain - 1))) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_UP: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] == sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] == (sectorsInDomain - 1))) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_DOWN: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] == -1)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] == 0)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_DOWN: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] == -1)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] == 0)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }
      }
    }
  } else {
    // 3D

    int (*oSectors)[sectorsInDomain*sectorsInDomain][S_XYZ_TOT] = outgoingSectors;
    int (*iSectors)[sectorsInDomain*sectorsInDomain][S_XYZ_TOT] = incomingSectors;
    int (*sCoordinates)[3][maxSectors] = (void *)sectorCoordinates;

    for (i = 0; i < S_XYZ_TOT; ++i)
      for (j = 0; j < sectorsInDomain*sectorsInDomain; ++j) {
	(*iSectors)[j][i] = -1;
	(*oSectors)[j][i] = -1;
      }

    // setup MPI neighbor incoming/outgoing sector tables

    // incoming sectors are the outside border sectors
    // they have coordinates of -1 or sectorsInDomain

    // outgoing sectors are the outside border sectors
    // they have coordinates of 0 or (sectorsInDomain - 1)
 
    int direction;
    for (direction = S_START; direction < S_XYZ_TOT; ++direction) {
      switch (direction) {

      case S_LEFT: { // count = sectorsInDomain*sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    //printf("S_LEFT outgoingSector: %d, %d\n", oNum, i);
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT: { // count = sectorsInDomain*sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    //printf("S_RIGHT incomingSector: %d, %d\n", iNum, i);
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_UP: { // count = sectorsInDomain*sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[1][i] == sectorsInDomain)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[1][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    //printf("S_UP outgoingSector: %d, %d\n", oNum, i);
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_DOWN: { // count = sectorsInDomain*sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[1][i] == -1)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[1][i] == 0)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    //printf("S_DOWN outgoingSector: %d, %d\n", oNum, i);
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_UP: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] == sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_UP: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] == sectorsInDomain)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_DOWN: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] == -1)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] == 0)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_DOWN: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] == -1)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] == 0)
	      && ((*sCoordinates)[2][i] >= 0) && ((*sCoordinates)[2][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_FRONT: { // count = sectorsInDomain*sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[2][i] == -1)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[2][i] == 0)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    //printf("S_FRONT outgoingSector: %d, %d\n", oNum, i);
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_BACK: { // count = sectorsInDomain*sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[2][i] == sectorsInDomain)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[2][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    //printf("S_BACK outgoingSector: %d, %d\n", oNum, i);
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_FRONT: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[2][i] == -1)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[2][i] == 0)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_FRONT: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[2][i] == -1)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[2][i] == 0)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_BACK: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[2][i] == sectorsInDomain)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[2][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_BACK: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[2][i] == sectorsInDomain)
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[2][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[1][i] >= 0) && ((*sCoordinates)[1][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_UP_FRONT: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[1][i] == sectorsInDomain) && ((*sCoordinates)[2][i] == -1)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[1][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[2][i] == 0)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_DOWN_FRONT: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[1][i] == -1) && ((*sCoordinates)[2][i] == -1)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[1][i] == 0) && ((*sCoordinates)[2][i] == 0)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_UP_BACK: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[1][i] == sectorsInDomain) && ((*sCoordinates)[2][i] == sectorsInDomain)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[1][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[2][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_DOWN_BACK: { // count = sectorsInDomain
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[1][i] == -1) && ((*sCoordinates)[2][i] == sectorsInDomain)
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[1][i] == 0) && ((*sCoordinates)[2][i] == (sectorsInDomain - 1))
	      && ((*sCoordinates)[0][i] >= 0) && ((*sCoordinates)[0][i] < sectorsInDomain)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_UP_FRONT: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] == sectorsInDomain) && ((*sCoordinates)[2][i] == -1)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[2][i] == 0)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_UP_FRONT: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] == sectorsInDomain) && ((*sCoordinates)[2][i] == -1)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[2][i] == 0)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_DOWN_FRONT: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] == -1) && ((*sCoordinates)[2][i] == -1)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] == 0) && ((*sCoordinates)[2][i] == 0)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_DOWN_FRONT: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] == -1) && ((*sCoordinates)[2][i] == -1)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] == 0) && ((*sCoordinates)[2][i] == 0)) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_UP_BACK: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] == sectorsInDomain) && ((*sCoordinates)[2][i] == sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[2][i] == (sectorsInDomain - 1))) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_UP_BACK: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] == sectorsInDomain) && ((*sCoordinates)[2][i] == sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[2][i] == (sectorsInDomain - 1))) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_LEFT_DOWN_BACK: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == -1) && ((*sCoordinates)[1][i] == -1) && ((*sCoordinates)[2][i] == sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == 0) && ((*sCoordinates)[1][i] == 0) && ((*sCoordinates)[2][i] == (sectorsInDomain - 1))) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      case S_RIGHT_DOWN_BACK: { // count = 1
	int iNum = 0, oNum = 0;
	for (i = 0; i < numSectors; ++i) {
	  if (((*sCoordinates)[0][i] == sectorsInDomain) && ((*sCoordinates)[1][i] == -1) && ((*sCoordinates)[2][i] == sectorsInDomain)) {
	    (*iSectors)[iNum][direction] = i;
	    ++iNum;
	  }
	  if (((*sCoordinates)[0][i] == (sectorsInDomain - 1)) && ((*sCoordinates)[1][i] == 0) && ((*sCoordinates)[2][i] == (sectorsInDomain - 1))) {
	    (*oSectors)[oNum][direction] = i;
	    ++oNum;
	  }
	}
	if ([self isDebug]) printf("neighbor %s, incoming = %d, outgoing = %d\n", bc_sector_direction_string(direction), iNum, oNum);
	break;
      }

      }
    }
  }

  // perform initial MPI synchronization
  [self initiateGPUMPIData: NULL mode: BC_MPI_SEM_MODE_INIT];
}

- (void)initializeMPIFileCompleted
{
  printf("ScEM initializeMPIFileCompleted\n");

  // MPI initialization
  [self initializeMPISimulation];

  // place elements in sectors
  if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"start -assignElementsToSectors, rank = %d", [modelController rank]] UTF8String]);
  [self assignElementsToSectors];
  if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"end -assignElementsToSectors, rank = %d", [modelController rank]] UTF8String]);

  // neighbor tables are calculated from element and sector coordinates
  if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"start -initializeNeighborTables, rank = %d", [modelController rank]] UTF8String]);
  [self initializeNeighborTables];
  if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"end -initializeNeighborTables, rank = %d", [modelController rank]] UTF8String]);
}

@end


//
// GPU
//

@implementation SubcellularElementModel (GPUMPI)

- (NSMutableString *)definitionsGPUMPICode: (NSString *)prefixName
{
  int dims = [[super spatialModel] dimensions];
  printf("definitionsGPUCode: %d\n", dims);
         
  NSMutableString *GPUCode = [NSMutableString new];
  
  [GPUCode appendFormat: @"__constant__ float %@_sem_parameters[%d];\n\n", prefixName, [self numOfParameters]];
  
  [GPUCode appendFormat: @"\n__constant__ BC_SEM_GPUgrids %@_sem_grids[1];\n", prefixName];
  [GPUCode appendFormat: @"\n__constant__ BC_MPI_SEM_GPUdata %@_mpi_sem_data[1];\n", prefixName];
  [GPUCode appendString: @"\n"];

  return GPUCode;
}

- (NSDictionary *)controllerGPUMPICode: (NSString *)prefixName
{
  int dims = [[super spatialModel] dimensions];

  NSMutableDictionary *GPUDictionary = [NSMutableDictionary new];
  NSMutableString *GPUCode;

  //
  // Headers and defines
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <stdio.h>\n"];
  [GPUCode appendString: @"#include <unistd.h>\n\n"];
  [GPUCode appendString: @"extern \"C\" {\n"];
  [GPUCode appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  //[GPUCode appendString: @"#include <BioSwarm/GPU_sem.h>\n"];
  //[GPUCode appendString: @"#include \"GPU_sem.h\"\n"];
  [GPUDictionary setObject: GPUCode forKey: @"header"];

  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"#include <BioSwarm/GPU_sem.h>\n"];
  [GPUCode appendString: @"#include <BioSwarm/GPU_MPI.h>\n"];
  [GPUCode appendFormat: @"#include \"%@_defines.cu\"\n\n", prefixName];
  [GPUCode appendFormat: @"#include \"%@_kernel.cu\"\n\n", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"defines"];

  //
  // Allocation: generic
  //
  
  //
  // Data Transfer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_transferGPUKernel(void *model, void *g, int aFlag, float *hostParameters, int *cellNumber, int numOfCells, int totalElements, void *hostX, void *hostY, void *hostZ, void *hostType, void *hostSector, void *hostElementNeighborTable, void *EFT, void *EFTT, void *ETT, void *EFFT, void *hostData, void *meshElements, void *hostSectorCoordinates, void *hostNumSectors, void *hostSectorSize, void *hostSectorElementTable, void *hostSectorNeighborTable, void *hostReadOnlySector)", prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"transfer function"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendString: @"\n BC_SEM_transferGPUKernel(model, g, aFlag, hostParameters, cellNumber, numOfCells, totalElements, hostX, hostY, hostZ, hostType, hostSector, hostElementNeighborTable, EFT, EFTT, ETT, EFFT, hostData, meshElements, hostSectorCoordinates, hostNumSectors, hostSectorSize, hostSectorElementTable, hostSectorNeighborTable, hostReadOnlySector);\n"];
  [GPUCode appendString: @"\n if (aFlag) {\n"];
  [GPUCode appendString: @"   // Copy data structure\n"];
  [GPUCode appendFormat: @"   cudaMemcpyToSymbol(%@_sem_grids, g, sizeof(BC_SEM_GPUgrids), 0, cudaMemcpyHostToDevice);\n", prefixName];
  
  [GPUCode appendString: @" }\n"];
  [GPUCode appendString: @"}\n"];

  [GPUCode appendFormat: @"\nvoid %@_MPI_transferGPUKernel(void *model, void *g, int aFlag, void *sem, void *incomingElements, void *outgoingElements, int totalElements)", prefixName];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" BC_MPI_SEM_GPUdata *mpi_data = (BC_MPI_SEM_GPUdata *)g;\n"];
  [GPUCode appendFormat: @" BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)sem;\n"];
  [GPUCode appendString: @"\n BC_MPI_SEM_transferGPUKernel(model, g, aFlag, sem, incomingElements, outgoingElements, totalElements);\n"];
  [GPUCode appendString: @"\n if (sem_data->totalElements != totalElements) {\n"];
  [GPUCode appendString: @"   // Copy data structure\n"];
  [GPUCode appendFormat: @"   sem_data->totalElements = totalElements;\n"];
  [GPUCode appendFormat: @"   cudaMemcpyToSymbol(%@_sem_grids, sem, sizeof(BC_SEM_GPUgrids), 0, cudaMemcpyHostToDevice);\n", prefixName];
  
  [GPUCode appendString: @" }\n"];
  [GPUCode appendString: @"}\n"];

  [GPUCode appendFormat: @"\nvoid *%@_MPI_allocGPUKernel(void *model, void *g, int boundarySize, int sectorsInDomain, void *incomingSectors, void *outgoingSectors)", prefixName];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendString: @"  void *mpi_data = BC_MPI_SEM_allocGPUKernel(model, g, boundarySize, sectorsInDomain, incomingSectors, outgoingSectors);\n"];
  [GPUCode appendString: @"  // Copy data structure\n"];
  [GPUCode appendFormat: @"  cudaMemcpyToSymbol(%@_mpi_sem_data, mpi_data, sizeof(BC_MPI_SEM_GPUdata), 0, cudaMemcpyHostToDevice);\n", prefixName];
  [GPUCode appendString: @"  cudacheck(\"transfer MPI data structure\");\n"];
  [GPUCode appendString: @"  return mpi_data;\n"];
  [GPUCode appendString: @"}\n"];

  [GPUDictionary setObject: GPUCode forKey: @"transfer code"];

  //
  // Execution
  //
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"%@_invokeGPUKernel(void *model, void *%@_data, double startTime, double nextTime)", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"execution function"];

  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" BC_SEM_GPUgrids *%@_ptrs = (BC_SEM_GPUgrids *)%@_data;\n", prefixName, prefixName];
  [GPUCode appendFormat: @" BC_MPI_SEM_GPUdata *%@_mpi_ptrs = (BC_MPI_SEM_GPUdata *)%@_ptrs->mpiData;\n", prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"structure"];
  
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @" int %@_x = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_y = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_x > %@_ptrs->totalElements) %@_x = %@_ptrs->totalElements;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_y > %@_ptrs->numOfModels) %@_y = %@_ptrs->numOfModels;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerBlock(%@_x, %@_y);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_threadsPerGrid((%@_ptrs->totalElements + %@_threadsPerBlock.x - 1) / %@_threadsPerBlock.x, (%@_ptrs->numOfModels + %@_threadsPerBlock.y - 1) / %@_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendFormat: @" int %@_xx = 256;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_xx > %@_ptrs->numOfCells) %@_xx = %@_ptrs->numOfCells;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_cell_threadsPerBlock(%@_xx, %@_y);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_cell_blocksPerGrid((%@_ptrs->numOfCells + %@_cell_threadsPerBlock.x - 1) / %@_cell_threadsPerBlock.x, (%@_ptrs->numOfModels + %@_cell_threadsPerBlock.y - 1) / %@_cell_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @" int %@_nx = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_ny = 1;\n", prefixName];
  [GPUCode appendFormat: @" int %@_maxy = %@_ptrs->numOfModels * %@_ptrs->sectorNeighborSize * %@_ptrs->maxElementsPerSector;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_nx > %@_ptrs->totalElements) %@_nx = %@_ptrs->totalElements;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_ny > %@_maxy) %@_ny = %@_maxy;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@n_threadsPerBlock(%@_nx, %@_ny);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@n_threadsPerGrid((%@_ptrs->totalElements + %@n_threadsPerBlock.x - 1) / %@n_threadsPerBlock.x, (%@_maxy + %@n_threadsPerBlock.y - 1) / %@n_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @" int %@_sx = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_sy = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_sx > %@_ptrs->maxSectors) %@_sx = %@_ptrs->maxSectors;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_sy > %@_ptrs->numOfModels) %@_sy = %@_ptrs->numOfModels;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@s_threadsPerBlock(%@_sx, %@_sy);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@s_threadsPerGrid((%@_ptrs->maxSectors + %@s_threadsPerBlock.x - 1) / %@s_threadsPerBlock.x, (%@_ptrs->numOfModels + %@s_threadsPerBlock.y - 1) / %@s_threadsPerBlock.y);\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @" int %@_mx = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_my = 1;\n", prefixName];
  [GPUCode appendFormat: @" if (%@_mx > %@_ptrs->numOfModels) %@_mx = %@_ptrs->numOfModels;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@m_threadsPerBlock(%@_mx, %@_my);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@m_threadsPerGrid((%@_ptrs->numOfModels + %@m_threadsPerBlock.x - 1) / %@m_threadsPerBlock.x, 1);\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @" int %@_mpi_x = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@_mpi_maxx = %@_mpi_ptrs->boundarySize * %@_ptrs->maxElementsPerSector;\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@_mpi_x > %@_mpi_maxx) %@_mpi_x = %@_mpi_maxx;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_mpi_threadsPerBlock(%@_mpi_x, 1);\n", prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@_mpi_blocksPerGrid((%@_mpi_maxx + %@_mpi_threadsPerBlock.x - 1) / %@_mpi_threadsPerBlock.x, 1);\n", prefixName, prefixName, prefixName, prefixName];

  [GPUCode appendFormat: @" int %@s_mpi_x = 256;\n", prefixName];
  [GPUCode appendFormat: @" int %@s_mpi_maxx = %@_mpi_ptrs->boundarySize;\n", prefixName, prefixName];
  [GPUCode appendFormat: @" if (%@s_mpi_x > %@s_mpi_maxx) %@s_mpi_x = %@s_mpi_maxx;\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@s_mpi_threadsPerBlock(%@s_mpi_x, 1);\n", prefixName, prefixName];
  [GPUCode appendFormat: @" dim3 %@s_mpi_blocksPerGrid((%@s_mpi_maxx + %@s_mpi_threadsPerBlock.x - 1) / %@s_mpi_threadsPerBlock.x, 1);\n", prefixName, prefixName, prefixName, prefixName];
  
  [GPUDictionary setObject: GPUCode forKey: @"execution variables"];

  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"      if (%@_ptrs->calcCellCenters) %@_SEM_center_kernel<<< %@_cell_blocksPerGrid, %@_cell_threadsPerBlock >>>();\n", prefixName, prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];

  [GPUCode appendFormat: @"      if (%@_ptrs->calcElementCenters) %@_SEM_element_center_kernel<<< %@_cell_blocksPerGrid, %@_cell_threadsPerBlock >>>();\n", prefixName, prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];

#if 0
  [GPUCode appendFormat: @"      %@_SEM_kernel1_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
  [GPUCode appendFormat: @"      %@_SEM_kernel2_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  if ([self isDebug]) [GPUCode appendString: @"      cudaPrintfDisplay(stdout, true);\n"];
#endif
#if 0
  [GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->X_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->Y_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudaMemset2D (%@_ptrs->Z_tmp, %@_ptrs->pitch, 0, %@_ptrs->maxElements * sizeof(float), %@_ptrs->numOfModels);\n", prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @"      %@_SEM_neighbor_kernel1_2rk<<< %@n_threadsPerGrid, %@n_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_neighbor_kernel1_2rk\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_neighbor_sum_kernel1_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_neighbor_sum_kernel1_2rk\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_MPI_SEM_gather<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_F1);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_F1, currentTime, nextTime);\n"];
  [GPUCode appendFormat: @"      %@_MPI_SEM_scatter<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_F1);\n\n", prefixName, prefixName, prefixName];

  [GPUCode appendFormat: @"      %@_SEM_neighbor_kernel2_2rk<<< %@n_threadsPerGrid, %@n_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_neighbor_kernel2_2rk\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_neighbor_update_kernel_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_neighbor_update_kernel_2rk\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_sector_count_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_sector_count_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_create_sectors_kernel<<< %@m_threadsPerGrid, %@m_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_create_sectors_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_build_sector_neighbors_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_build_sector_neighbors_kernel\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_move_elements_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_move_elements_kernel\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_MPI_SEM_gather<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_MOVE_READ_ONLY, currentTime, nextTime);\n"];
  [GPUCode appendFormat: @"      %@_MPI_SEM_scatter<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      %@_MPI_SEM_sector_scatter<<< %@s_mpi_blocksPerGrid, %@s_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);\n\n", prefixName, prefixName, prefixName];

  [GPUCode appendFormat: @"      %@_MPI_SEM_gather<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_MOVE, currentTime, nextTime);\n"];
  [GPUCode appendFormat: @"      %@_MPI_SEM_scatter<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      %@_MPI_SEM_sector_scatter<<< %@s_mpi_blocksPerGrid, %@s_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);\n\n", prefixName, prefixName, prefixName];

  [GPUCode appendFormat: @"      %@_SEM_update_element_sector_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_update_element_sector_kernel\");\n\n", prefixName];

  [GPUCode appendFormat: @"      %@_SEM_update_element_neighbor_kernel<<< %@n_threadsPerGrid, %@n_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_update_element_neighbor_kernel\");\n\n", prefixName];
#endif

  [GPUCode appendFormat: @"      %@_SEM_pairwise_kernel1_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_pairwise_kernel1_2rk\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_MPI_SEM_gather<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_F1);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_F1, currentTime, nextTime);\n"];
  [GPUCode appendFormat: @"      %@_MPI_SEM_scatter<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_F1);\n\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_pairwise_kernel2_2rk<<< %@_threadsPerGrid, %@_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_pairwise_kernel2_2rk\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_sector_count_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_sector_count_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_create_sectors_kernel<<< %@m_threadsPerGrid, %@m_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_create_sectors_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_build_sector_neighbors_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_build_sector_neighbors_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_move_elements_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_move_elements_kernel\");\n\n", prefixName];
  
  [GPUCode appendFormat: @"      %@_MPI_SEM_gather<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_MOVE_READ_ONLY, currentTime, nextTime);\n"];
  [GPUCode appendFormat: @"      %@_MPI_SEM_scatter<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      %@_MPI_SEM_sector_scatter<<< %@s_mpi_blocksPerGrid, %@s_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);\n\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendFormat: @"      %@_MPI_SEM_gather<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_MOVE, currentTime, nextTime);\n"];
  [GPUCode appendFormat: @"      %@_MPI_SEM_scatter<<< %@_mpi_blocksPerGrid, %@_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      %@_MPI_SEM_sector_scatter<<< %@s_mpi_blocksPerGrid, %@s_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);\n\n", prefixName, prefixName, prefixName];
  
  [GPUCode appendFormat: @"      %@_SEM_update_element_sector_kernel<<< %@s_threadsPerGrid, %@s_threadsPerBlock >>>(currentTime, nextTime);\n", prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"      cudacheck(\"invoke %@_SEM_update_element_sector_kernel\");\n\n", prefixName];
  
  [GPUDictionary setObject: GPUCode forKey: @"execution code"];
  
  //
  // Release: generic
  //
  
  //
  // Footer
  //
  GPUCode = [NSMutableString new];
  [GPUCode appendFormat: @"\nvoid %@_MPI_invokeGPUKernel(void *model, void *%@_data, double startTime, double nextTime)", prefixName, prefixName];
  [GPUCode appendString: @"\n{\n"];
  [GPUCode appendFormat: @" BC_MPI_SEM_GPUdata *%@_ptrs = (BC_MPI_SEM_GPUdata *)%@_data;\n", prefixName, prefixName];
  [GPUCode appendString: @"\n}\n"];

  [GPUCode appendString: @"\n// GPU function pointers\n"];
  //[GPUCode appendFormat: @"SEMfunctions %@_gpuFunctions = {%@_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, %@_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUCode appendFormat: @"SEMfunctions %@_gpuFunctions = {BC_SEM_allocGPUKernel, %@_transferGPUKernel, %@_invokeGPUKernel, BC_SEM_releaseGPUKernel, %@_MPI_allocGPUKernel, %@_MPI_transferGPUKernel, %@_MPI_invokeGPUKernel, BC_MPI_SEM_releaseGPUKernel};\n", prefixName, prefixName, prefixName, prefixName, prefixName, prefixName];
  [GPUDictionary setObject: GPUCode forKey: @"footer"];

  return GPUDictionary;
}

- (NSString *)centerKernelGPUMPICode: (NSString *)prefixName
{
  NSMutableString *GPUCode;
  int dims = [[super spatialModel] dimensions];
  
  GPUCode = [NSMutableString new];
  
  //
  // Center calculation for all elements in each cell
  //
  [GPUCode appendString: @"\n// Calculate cell center\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_SEM_center_kernel()\n", prefixName];
  [GPUCode appendString: @"{\n"];
  
  [GPUCode appendString: @"   int cellNum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int modelNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
  [GPUCode appendString: @"\n"];
  [GPUCode appendFormat: @"   if (cellNum >= %@_sem_grids->numOfCells) return;\n", prefixName];
  [GPUCode appendFormat: @"   if (modelNum >= %@_sem_grids->numOfModels) return;\n", prefixName];
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendString: @"   int k;\n"];
  [GPUCode appendString: @"   float cX = 0.0;\n"];
  [GPUCode appendString: @"   float cY = 0.0;\n"];
  if (dims == 3) [GPUCode appendString: @"   float cZ = 0.0;\n"];
  [GPUCode appendFormat: @"   int cidx = modelNum*%@_sem_grids->idx_cPitch+cellNum;\n", prefixName];    
  //[GPUCode appendString: @"   int first = 1;\n"];
  [GPUCode appendString: @"   float numOfElements = 0.0;\n"];
  [GPUCode appendString: @"\n"];

  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->totalElements; ++k) {\n", prefixName];
  [GPUCode appendFormat: @"     int idx = modelNum*%@_sem_grids->idx_pitch+k;\n", prefixName];
  [GPUCode appendFormat: @"     if (%@_sem_grids->cellNumber[idx] != cellNum) continue;\n", prefixName];
  [GPUCode appendString: @"\n"];
      
  [GPUCode appendFormat: @"     cX += %@_sem_grids->X[idx];\n", prefixName];
  [GPUCode appendFormat: @"     cY += %@_sem_grids->Y[idx];\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"     cZ += %@_sem_grids->Z[idx];\n", prefixName];
  [GPUCode appendString: @"     numOfElements += 1.0f;\n"];
  [GPUCode appendString: @"   }\n"];
  [GPUCode appendString: @"\n"];
    
  [GPUCode appendString: @"   cX = cX / numOfElements;\n"];
  [GPUCode appendString: @"   cY = cY / numOfElements;\n"];
  if (dims == 3) [GPUCode appendString: @"   cZ = cZ / numOfElements;\n"];
    
  [GPUCode appendFormat: @"   %@_sem_grids->cellCenterX[cidx] = cX;\n", prefixName];
  [GPUCode appendFormat: @"   %@_sem_grids->cellCenterY[cidx] = cY;\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"   %@_sem_grids->cellCenterZ[cidx] = cZ;\n", prefixName];
  
  [GPUCode appendString: @"}\n"];
  
  //
  // Center calculation for each element type in each cell
  // To avoid using a 3D array, the element types and models are combined in the rows
  //
  [GPUCode appendString: @"\n// Calculate element centers\n"];
  [GPUCode appendString: @"__global__ void\n"];
  [GPUCode appendFormat: @"%@_SEM_element_center_kernel()\n", prefixName];
  [GPUCode appendString: @"{\n"];
  
  [GPUCode appendString: @"   int cellNum = blockIdx.x * blockDim.x + threadIdx.x;\n"];
  [GPUCode appendString: @"   int modelNum = blockIdx.y * blockDim.y + threadIdx.y;\n"];
  [GPUCode appendString: @"\n"];
  [GPUCode appendFormat: @"   if (cellNum >= %@_sem_grids->numOfCells) return;\n", prefixName];
  [GPUCode appendFormat: @"   if (modelNum >= %@_sem_grids->numOfModels) return;\n", prefixName];
  [GPUCode appendString: @"\n"];

  [GPUCode appendString: @"   int k;\n"];

  [GPUCode appendString: @"\n   // zero out element centers\n"];
  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->maxElementTypes; ++k) {\n", prefixName];
  [GPUCode appendFormat: @"      int cidx = ((modelNum*%@_sem_grids->maxElementTypes)+k)*%@_sem_grids->idx_cPitch+cellNum;\n", prefixName, prefixName];    
  [GPUCode appendFormat: @"      %@_sem_grids->elementCenterX[cidx] = 0;\n", prefixName];
  [GPUCode appendFormat: @"      %@_sem_grids->elementCenterY[cidx] = 0;\n", prefixName];
  if (dims == 3) [GPUCode appendFormat: @"      %@_sem_grids->elementCenterZ[cidx] = 0;\n", prefixName];
  [GPUCode appendFormat: @"      %@_sem_grids->elementCenterCount[cidx] = 0;\n", prefixName];
  [GPUCode appendString: @"   }\n"];

  [GPUCode appendString: @"\n   // calc element centers\n"];
  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->totalElements; ++k) {\n", prefixName];
  [GPUCode appendFormat: @"     int idx = modelNum*%@_sem_grids->idx_pitch+k;\n", prefixName];
  [GPUCode appendFormat: @"     if (%@_sem_grids->cellNumber[idx] != cellNum) continue;\n", prefixName];
  [GPUCode appendFormat: @"     int cidx = ((modelNum*%@_sem_grids->maxElementTypes)+%@_sem_grids->elementType[idx])*%@_sem_grids->idx_cPitch+cellNum;\n", prefixName, prefixName, prefixName];    
  [GPUCode appendString: @"\n"];
  
  [GPUCode appendFormat: @"     %@_sem_grids->elementCenterX[cidx] += %@_sem_grids->X[idx];\n", prefixName, prefixName];
  [GPUCode appendFormat: @"     %@_sem_grids->elementCenterY[cidx] += %@_sem_grids->Y[idx];\n", prefixName, prefixName];
  if (dims == 3) [GPUCode appendFormat: @"     %@_sem_grids->elementCenterZ[cidx] += %@_sem_grids->Z[idx];\n", prefixName, prefixName];
  [GPUCode appendFormat: @"     %@_sem_grids->elementCenterCount[cidx] += 1.0f;\n", prefixName];
  
  [GPUCode appendString: @"   }\n"];
  
  [GPUCode appendFormat: @"   for (k = 0; k < %@_sem_grids->maxElementTypes; ++k) {\n", prefixName];
  [GPUCode appendFormat: @"      int cidx = ((modelNum*%@_sem_grids->maxElementTypes)+k)*%@_sem_grids->idx_cPitch+cellNum;\n", prefixName, prefixName];    
  [GPUCode appendFormat: @"      if (%@_sem_grids->elementCenterCount[cidx] != 0) {\n", prefixName];
  [GPUCode appendFormat: @"         %@_sem_grids->elementCenterX[cidx] /= %@_sem_grids->elementCenterCount[cidx];\n", prefixName, prefixName];
  [GPUCode appendFormat: @"         %@_sem_grids->elementCenterY[cidx] /= %@_sem_grids->elementCenterCount[cidx];\n", prefixName, prefixName];
  if (dims == 3) [GPUCode appendFormat: @"         %@_sem_grids->elementCenterZ[cidx] /= %@_sem_grids->elementCenterCount[cidx];\n", prefixName, prefixName];
  [GPUCode appendString: @"      }\n"];
  [GPUCode appendString: @"   }\n"];

  [GPUCode appendString: @"}\n"];

  return GPUCode;
}

- (NSMutableString *)kernelGPUMPICode: (NSString *)prefixName
{
  int dims = [[super spatialModel] dimensions];

  NSMutableString *GPUCode = [NSMutableString new];

  // kernel to calculate centers
  [GPUCode appendString: [self centerKernelGPUCode: prefixName]];

  // kernel to calculate SEM movement function
  //[GPUCode appendString: [self functionKernelGPUCode: prefixName]];
  
  // kernel for numerical method
  if ([numericalScheme isEqualToString: @"RungeKutta2ndOrder"]) {
    [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_SEM_kernel1_2rk(float currentTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    if (useMesh)
      [GPUCode appendFormat: @"   BC_SEM_mesh_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    else
      [GPUCode appendFormat: @"   BC_SEM_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    [GPUCode appendString: @"}\n"];
    
    [GPUCode appendString: @"\n// F2 Kernel for 2nd order Runge-Kutta\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_SEM_kernel2_2rk(float currentTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    if (useMesh)
      [GPUCode appendFormat: @"   BC_SEM_mesh_kernel2_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    else
      [GPUCode appendFormat: @"   BC_SEM_kernel2_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    [GPUCode appendString: @"}\n"];

    [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta, pairwise algorithm\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_SEM_pairwise_kernel1_2rk(float currentTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    [GPUCode appendFormat: @"   BC_SEM_pairwise_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    [GPUCode appendString: @"}\n"];
    
    [GPUCode appendString: @"\n// F2 Kernel for 2nd order Runge-Kutta, pairwise algorithm\n"];
    [GPUCode appendString: @"__global__ void\n"];
    [GPUCode appendFormat: @"%@_SEM_pairwise_kernel2_2rk(float currentTime, float finalTime)\n", prefixName];
    [GPUCode appendString: @"{\n"];
    [GPUCode appendFormat: @"   BC_SEM_pairwise_kernel2_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
    [GPUCode appendString: @"}\n"];

    if (!useMesh) {
      [GPUCode appendString: @"\n// F1 Kernel for 2nd order Runge-Kutta, neighbor pair algorithm\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_neighbor_kernel1_2rk(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_neighbor_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Sum forces for F1\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_neighbor_sum_kernel1_2rk(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_neighbor_sum_kernel1_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// F2 Kernel for 2nd order Runge-Kutta, neighbor pair algorithm\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_neighbor_kernel2_2rk(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_neighbor_kernel2_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Sum forces for F2 and update element positions\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_neighbor_update_kernel_2rk(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_neighbor_update_kernel_2rk(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Determine if need new sectors\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_sector_count_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_sector_count_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Create new sectors\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_create_sectors_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_create_sectors_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];
      
      [GPUCode appendString: @"\n// Update sector neighbor table\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_build_sector_neighbors_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_build_sector_neighbors_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Move elements to their sectors\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_move_elements_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_move_elements_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Update sector tables\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_update_element_sector_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_update_element_sector_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// Update element neighbor table\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_SEM_update_element_neighbor_kernel(float currentTime, float finalTime)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_SEM_update_element_neighbor_kernel(%@_sem_grids, currentTime, finalTime);\n", prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// MPI gather\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_MPI_SEM_gather(float currentTime, float finalTime, int mode)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_MPI_SEM_gather(%@_mpi_sem_data, %@_sem_grids, mode);\n", prefixName, prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// MPI scatter\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_MPI_SEM_scatter(float currentTime, float finalTime, int mode)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_MPI_SEM_scatter(%@_mpi_sem_data, %@_sem_grids, mode);\n", prefixName, prefixName];
      [GPUCode appendString: @"}\n"];

      [GPUCode appendString: @"\n// MPI sector scatter\n"];
      [GPUCode appendString: @"__global__ void\n"];
      [GPUCode appendFormat: @"%@_MPI_SEM_sector_scatter(float currentTime, float finalTime, int mode)\n", prefixName];
      [GPUCode appendString: @"{\n"];
      [GPUCode appendFormat: @"   BC_MPI_SEM_sector_scatter(%@_mpi_sem_data, %@_sem_grids, mode);\n", prefixName, prefixName];
      [GPUCode appendString: @"}\n"];

    }

  } else {
    printf("ERROR: unknown numerical scheme: %s\n", [numericalScheme UTF8String]);
    return nil;
  }
  
  return GPUCode;
}

- (NSMutableString *)cleanupGPUMPICode: (NSString *)prefixName
{
  return nil;
}

@end


//
// Sector functions
//
void bc_sector_coordinate_shift(int xc, int yc, int zc, int direction, int *X, int *Y, int *Z)
{
  *X = xc;
  *Y = yc;
  *Z = zc;
  switch (direction) {
    case S_LEFT:
      *X = *X - 1; break;
    case S_RIGHT:
      *X = *X + 1; break;
    case S_UP:
      *Y = *Y + 1; break;
    case S_DOWN:
      *Y = *Y - 1; break;
    case S_LEFT_UP:
      *X = *X - 1; *Y = *Y + 1; break;
    case S_RIGHT_UP:
      *X = *X + 1; *Y = *Y + 1; break;
    case S_LEFT_DOWN:
      *X = *X - 1; *Y = *Y - 1; break;
    case S_RIGHT_DOWN:
      *X = *X + 1; *Y = *Y - 1; break;
    case S_FRONT:
      *Z = *Z - 1; break;
    case S_BACK:
      *Z = *Z + 1; break;
    case S_LEFT_FRONT:
      *X = *X - 1; *Z = *Z - 1; break;
    case S_RIGHT_FRONT:
      *X = *X + 1; *Z = *Z - 1; break;
    case S_LEFT_BACK:
      *X = *X - 1; *Z = *Z + 1; break;
    case S_RIGHT_BACK:
      *X = *X + 1; *Z = *Z + 1; break;
    case S_UP_FRONT:
      *Y = *Y + 1; *Z = *Z - 1; break;
    case S_DOWN_FRONT:
      *Y = *Y - 1; *Z = *Z - 1; break;
    case S_UP_BACK:
      *Y = *Y + 1; *Z = *Z + 1; break;
    case S_DOWN_BACK:
      *Y = *Y - 1; *Z = *Z + 1; break;
    case S_LEFT_UP_FRONT:
      *X = *X - 1; *Y = *Y + 1; *Z = *Z - 1; break;
    case S_RIGHT_UP_FRONT:
      *X = *X + 1; *Y = *Y + 1; *Z = *Z - 1; break;
    case S_LEFT_DOWN_FRONT:
      *X = *X - 1; *Y = *Y - 1; *Z = *Z - 1; break;
    case S_RIGHT_DOWN_FRONT:
      *X = *X + 1; *Y = *Y - 1; *Z = *Z - 1; break;
    case S_LEFT_UP_BACK:
      *X = *X - 1; *Y = *Y + 1; *Z = *Z + 1; break;
    case S_RIGHT_UP_BACK:
      *X = *X + 1; *Y = *Y + 1; *Z = *Z + 1; break;
    case S_LEFT_DOWN_BACK:
      *X = *X - 1; *Y = *Y - 1; *Z = *Z + 1; break;
    case S_RIGHT_DOWN_BACK:
      *X = *X + 1; *Y = *Y - 1; *Z = *Z + 1; break;
  }
}

char *bc_sector_direction_string(int direction)
{
  switch (direction) {
    case S_NONE:
      return "S_NONE"; break;
    case S_CURRENT:
      return "S_CURRENT"; break;
    case S_LEFT:
      return "S_LEFT"; break;
    case S_RIGHT:
      return "S_RIGHT"; break;
    case S_UP:
      return "S_UP"; break;
    case S_DOWN:
      return "S_DOWN"; break;
    case S_LEFT_UP:
      return "S_LEFT_UP"; break;
    case S_RIGHT_UP:
      return "S_RIGHT_UP"; break;
    case S_LEFT_DOWN:
      return "S_LEFT_DOWN"; break;
    case S_RIGHT_DOWN:
      return "S_RIGHT_DOWN"; break;
    case S_FRONT:
      return "S_FRONT"; break;
    case S_BACK:
      return "S_BACK"; break;
    case S_LEFT_FRONT:
      return "S_LEFT_FRONT"; break;
    case S_RIGHT_FRONT:
      return "S_RIGHT_FRONT"; break;
    case S_LEFT_BACK:
      return "S_LEFT_BACK"; break;
    case S_RIGHT_BACK:
      return "S_RIGHT_BACK"; break;
    case S_UP_FRONT:
      return "S_UP_FRONT"; break;
    case S_DOWN_FRONT:
      return "S_DOWN_FRONT"; break;
    case S_UP_BACK:
      return "S_UP_BACK"; break;
    case S_DOWN_BACK:
      return "S_DOWN_BACK"; break;
    case S_LEFT_UP_FRONT:
      return "S_LEFT_UP_FRONT"; break;
    case S_RIGHT_UP_FRONT:
      return "S_RIGHT_UP_FRONT"; break;
    case S_LEFT_DOWN_FRONT:
      return "S_LEFT_DOWN_FRONT"; break;
    case S_RIGHT_DOWN_FRONT:
      return "S_RIGHT_DOWN_FRONT"; break;
    case S_LEFT_UP_BACK:
      return "S_LEFT_UP_BACK"; break;
    case S_RIGHT_UP_BACK:
      return "S_RIGHT_UP_BACK"; break;
    case S_LEFT_DOWN_BACK:
      return "S_LEFT_DOWN_BACK"; break;
    case S_RIGHT_DOWN_BACK:
      return "S_RIGHT_DOWN_BACK"; break;
    default:
      return "ERROR: Unknown sector direction"; break;
  }
}
