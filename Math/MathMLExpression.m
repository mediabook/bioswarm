/*
 MathMLExpression.m
 
 MathML expressions.
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: Aug. 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "MathMLExpression.h"

static NSMutableDictionary *standard_functions = nil;

@implementation MathMLExpression

+ (NSDictionary *)standardFunctions
{
  int i, j;
  
  if (!standard_functions) {
    standard_functions = [NSMutableDictionary new];
    
    // load the standard functions
    NSURL *urlPath = [[NSBundle bundleForClass: [self class]]
                      URLForResource: @"StandardFunctions" withExtension: @"xml"];
    printf("%s\n", [[urlPath absoluteString] UTF8String]);
    NSError *err;
    NSXMLDocument *test = [[NSXMLDocument alloc] initWithContentsOfURL:urlPath options:NSXMLDocumentTidyXML error:&err];
    printf("%s\n", [[test description] UTF8String]);
    NSXMLElement *root = [test rootElement];
    printf("%s %ld\n", [[root name] UTF8String], (unsigned long)[root childCount]);
    NSXMLElement *f = (NSXMLElement *)[root childAtIndex: 0];
    printf("%s %ld\n", [[f name] UTF8String], (unsigned long)[[f attributes] count]);
    NSXMLNode *n = [[f attributes] objectAtIndex: 0];
    printf("%s = %s\n", [[n name] UTF8String], [[n stringValue] UTF8String]);

    for (i = 0; i < [root childCount]; ++i) {
      NSXMLElement *f = (NSXMLElement *)[root childAtIndex: i];
      NSArray *a = [f attributes];
      NSString *name = nil;
      for (j = 0; j < [a count]; ++j) {
        NSXMLNode *n = [[f attributes] objectAtIndex: j];
        if ([[n name] isEqualToString: @"name"]) {
          name = [n stringValue];
          break;
        }
      }
      if (!name) printf("ERROR: function has no name!\n");
      else [standard_functions setObject:f forKey: name];
    }
  }
  
  return standard_functions;
}

+ (MathMLExpression *)functionWithName: (NSString *)aName;
{
  NSXMLElement *f = nil;
  
  // search user defined functions first
  
  // search standard functions
  NSDictionary *sf = [self standardFunctions];
  f = [sf objectForKey: aName];
  if (f) return [[MathMLExpression alloc] initWithXML: f];
  
  return nil;
}

- (BOOL)validateExpression
{
  NSXMLElement *f = (NSXMLElement *)[xmlTree childAtIndex: 0];
  
  // child should be start of mathml
  if (![[f name] isEqualToString: @"math"]) {
    printf("ERROR: invalid mathml, expected <math>. got <%s>\n", [[f name] UTF8String]);
    return NO;
  }
  
  // next is semantics tag
  f = (NSXMLElement *)[f childAtIndex: 0];
  if (![[f name] isEqualToString: @"semantics"]) {
    printf("ERROR: invalid mathml, expected <semantics>. got <%s>\n", [[f name] UTF8String]);
    return NO;
  }
  
  // then lambda which is start of function
  f = (NSXMLElement *)[f childAtIndex: 0];
  if (![[f name] isEqualToString: @"lambda"]) {
    printf("ERROR: invalid mathml, expected <lambda>. got <%s>\n", [[f name] UTF8String]);
    return NO;
  }
  
  // set of variables for the function
  BOOL done = NO;
  int childCount = 0;
  variables = [NSMutableArray new];
  while (!done) {
    NSXMLElement *v = (NSXMLElement *)[f childAtIndex: childCount];
    if ([[v name] isEqualToString: @"bvar"]) {
      v = (NSXMLElement *)[v childAtIndex: 0];
      if (![[v name] isEqualToString: @"ci"]) {
        printf("ERROR: invalid mathml, expected <ci> within <bvar>, got <%s>\n", [[v name] UTF8String]);
        return NO;
      }
      printf("var = %s\n", [[v stringValue] UTF8String]);
      [variables addObject: [v stringValue]];
      ++childCount;
    } else done = YES;
  }

  // then the function application
  f = (NSXMLElement *)[f childAtIndex: childCount];
  if (![[f name] isEqualToString: @"apply"]) {
    printf("ERROR: invalid mathml, expected <apply>. got <%s>\n", [[f name] UTF8String]);
    return NO;
  }
  expression = f;
  
  return YES;
}
- initWithXML: (NSXMLElement *)mathml
{
  [super init];
  
  xmlTree = [mathml retain];
  if (![self validateExpression]) return nil;
  
  return self;
}

- (void)dealloc
{
  [xmlTree release];
  [variables release];

  [super dealloc];
}

- (NSXMLNode *)mathML { return [xmlTree childAtIndex: 0]; }
- (NSXMLElement *)annotationXML: (NSString *)aName { return nil; }

- (NSNumber *)calculateFunction: (NSXMLNode *)aFunction withArguments: (NSArray *)args
{
  int i;
  NSXMLNode *f = [aFunction childAtIndex: 0];
  
  if ([[aFunction name] isEqualToString: @"apply"]) {

    // function application
    if ([[f name] isEqualToString: @"plus"]) {
      double d = [[self calculateFunction: [aFunction childAtIndex: 1] withArguments: args] doubleValue];
      for (i = 2; i < [aFunction childCount]; ++i) {
        d += [[self calculateFunction: [aFunction childAtIndex: i] withArguments: args] doubleValue];
      }
      return [NSNumber numberWithDouble: d];

    } else if ([[f name] isEqualToString: @"minus"]) {
      double d = [[self calculateFunction: [aFunction childAtIndex: 1] withArguments: args] doubleValue];
      if ([aFunction childCount] == 2) return [NSNumber numberWithDouble: -d];
      else {
        d = d - [[self calculateFunction: [aFunction childAtIndex: 2] withArguments: args] doubleValue];
        return [NSNumber numberWithDouble: d];
      }
      
    } else if ([[f name] isEqualToString: @"divide"]) {
      double d = [[self calculateFunction: [aFunction childAtIndex: 1] withArguments: args] doubleValue];
      d = d / [[self calculateFunction: [aFunction childAtIndex: 2] withArguments: args] doubleValue];
      return [NSNumber numberWithDouble: d];

    } else if ([[f name] isEqualToString: @"times"]) {
      double d = [[self calculateFunction: [aFunction childAtIndex: 1] withArguments: args] doubleValue];
      for (i = 2; i < [aFunction childCount]; ++i) {
        d *= [[self calculateFunction: [aFunction childAtIndex: i] withArguments: args] doubleValue];
      }
      return [NSNumber numberWithDouble: d];

    } else if ([[f name] isEqualToString: @"power"]) {
      double d = [[self calculateFunction: [aFunction childAtIndex: 1] withArguments: args] doubleValue];
      d = pow(d, [[self calculateFunction: [aFunction childAtIndex: 2] withArguments: args] doubleValue]);
      return [NSNumber numberWithDouble: d];
      
    } else {
      // check if user-defined function
      MathMLExpression *m = [MathMLExpression functionWithName: [f name]];
      if (!m) {
        printf("ERROR: Unsupported function: %s\n", [[f name] UTF8String]);
        return nil;
      }
      NSMutableArray *funcArgs = [NSMutableArray array];
      if ([aFunction childCount] > 1) {
        // build up arguments
        for (i = 1; i < [aFunction childCount]; ++i) {
          [funcArgs addObject: [self calculateFunction: [aFunction childAtIndex: i] withArguments: args]];
        }
      }
      return [m calculateWithArguments: funcArgs];
    }
  
  } else if ([[aFunction name] isEqualToString: @"cn"]) {
    // numerical constant
    return [NSNumber numberWithDouble: [[aFunction stringValue] doubleValue]];

  } else if ([[aFunction name] isEqualToString: @"ci"]) {
    // symbolic value (variable)
    i = [variables indexOfObject: [aFunction stringValue]];
    NSNumber *n = [args objectAtIndex: i];
    if (!n) {
      printf("ERROR: No value for symbolic name: %s\n", [[aFunction stringValue] UTF8String]);
    } else return n;
  }

  return nil;
}

- (NSNumber *)calculateWithArguments: (NSArray *)args
{
  return [self calculateFunction: expression withArguments: args];
}

@end
