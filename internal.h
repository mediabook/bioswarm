/*
 internal.h
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#define CHECK_PARAM(P,N) if (!P) { printf("WARNING: Missing parameter: %s\n",N); }
#define CHECK_PARAMF(P,N,F) if (!P) { printf("ERROR: Missing parameter: %s\n",N); F = NO; }

#define RAND_NUM ((double)rand() / (double)RAND_MAX )

#define EPS 1e-7
#define DEPS 1e-12

extern double normal_dist_rand(double mean, double sd);
extern int determine_scale_double(double value);
extern int determine_scale_float(float value);
extern BOOL direction_round(int direction, int dim);

typedef struct _sort_helper
{
  float fValue;
  double dValue;
  int idx;
} sort_helper;

static int float_cmp_sort_helper(const void *a, const void *b)
{
  sort_helper *sa = (sort_helper *)a;
  sort_helper *sb = (sort_helper *)b;

  if (sa->fValue < sb->fValue) return -1;
  if (sa->fValue > sb->fValue) return 1;
  return 0;
}

static int double_cmp_sort_helper(const void *a, const void *b)
{
  sort_helper *sa = (sort_helper *)a;
  sort_helper *sb = (sort_helper *)b;
  
  if (sa->dValue < sb->dValue) return -1;
  if (sa->dValue > sb->dValue) return 1;
  return 0;
}

// simple time profiling
#define TIME_PROFILE 1
extern void bc_record_time(const char *msg);
