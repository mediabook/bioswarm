/*
 GPUDefines.h
 
 Definitions for GPU code.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#ifndef BC_GPU_DEFINES_H
#define BC_GPU_DEFINES_H

// ReactionModel dynamic CPU data
typedef struct _RMDynamic_data {
  float *speciesData;
  float *speciesConnection;
  float *pminParam;
  float *pmaxParam;
  float *cParam;
  float *hParam;
  int *linearPlusParam;
  int *linearMinusParam;
  int *linearPlusConn;
  int *linearMinusConn;
  float *linearParam;
} RMDynamic_data;

// ReactionDiffusionModel CPU data
typedef struct _RK2grids {
  int speciesCount;
  void **speciesGrid;
  void **speciesF1;
  void **speciesF2;
  void **speciesDiff;
} RK2grids;

// ReactionDiffusionModel GPU data
typedef struct _RModelRK2_GPUptrs {
  int numOfODEs;
  int numParams;
  float dt;
  float EPS;
  int *localEpsilonCheck;
  int *epsilonCheck;
  int speciesCount;
  float *speciesData;
  float *speciesF1;
  float *speciesF2;
  size_t speciesPitch;
  float *parameters;
  size_t paramPitch;
} RModelRK2_GPUptrs;

typedef struct _ModelGPUData {
  void *data;
  void *data2;
  int *flags;
  void *extraData;
  void *parameters;
  int numModels;
  int numParams;
  void *gpuPtrs;
  void *gpuFunctions;
  void *mpiData;
} ModelGPUData;

// typedef for GPU functions

// get GPU function pointers by model id
typedef void* (getGPUFunctions)(int, int);
typedef void (invokeGPUKernelFunction)(void*, ModelGPUData**, double, double);

// ReactionModel, static structure
typedef	void* (allocRModelGPUFunction)(void*, int, int, int, int, float, float);
typedef void (initRModelGPUFunction)(void*, void*, int, float*, float*);
typedef void (invokeRModelGPUFunction)(void*, void*, float*, double, double);
typedef void (releaseRModelGPUFunction)(void*, void*);

typedef struct _RModelGPUFunctions {
    allocRModelGPUFunction *allocGPUKernel;
    initRModelGPUFunction *initGPUKernel;
    invokeRModelGPUFunction *invokeGPUKernel;
    releaseRModelGPUFunction *releaseGPUKernel;
} RModelGPUFunctions;

// DynamicReactionRuleModel
typedef	void* (allocRModelDynamicGPUFunction_float)(void *, int, int, int, int, int, int, int, int, float, float);
typedef	void* (allocRModelDynamicGPUFunction_double)(void *, int, int, int, int, int, int, int, int, double, double);
typedef void (initRModelDynamicGPUFunction_float)(void *, void *, int, float *, float *, int *, void *, void *, int *, int *, void *);
typedef void (initRModelDynamicGPUFunction_double)(void *, void *, int, double *, double *, int *, void *, void *, int *, int *, void *);
typedef void (invokeRModelDynamicGPUFunction)(void*, void*, double, double);
typedef void (releaseRModelDynamicGPUFunction)(void*, void*);

typedef struct _RModelDynamicGPUFunctions_float {
  allocRModelDynamicGPUFunction_float *allocGPUKernel;
  initRModelDynamicGPUFunction_float *initGPUKernel;
  invokeRModelDynamicGPUFunction *invokeGPUKernel;
  releaseRModelDynamicGPUFunction *releaseGPUKernel;
} RModelDynamicGPUFunctions_float;

typedef struct _RModelDynamicGPUFunctions_double {
  allocRModelDynamicGPUFunction_double *allocGPUKernel;
  initRModelDynamicGPUFunction_double *initGPUKernel;
  invokeRModelDynamicGPUFunction *invokeGPUKernel;
  releaseRModelDynamicGPUFunction *releaseGPUKernel;
} RModelDynamicGPUFunctions_double;

// ReactionDiffusionModel
typedef	void* (allocRDModelGPUFunction)(void*, float, float, float*, int, int);
typedef void (initRDModelGPUFunction)(void*, void*, int, float*, RK2grids*);
typedef void (invokeRDModelGPUFunction)(void*, void*, RK2grids*, double, double);
typedef void (releaseRDModelGPUFunction)(void*, void*);

typedef struct _RK2functions {
    allocRDModelGPUFunction *allocGPUKernel;
    initRDModelGPUFunction *initGPUKernel;
    invokeRDModelGPUFunction *invokeGPUKernel;
    releaseRDModelGPUFunction *releaseGPUKernel;
} RK2functions;

// SubcellularElementModel (GPU)
typedef void* (allocSEMGPUFunction)(void*, int, int, int, int, int, int, int, int, int, int, int, int, int, float);
typedef void (initSEMGPUFunction)(void *, void *, int, float *, int *, int, int, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *);
typedef void (invokeSEMGPUFunction)(void *, void *, double, double);
typedef void (releaseSEMGPUFunction)(void *, void *);
// SubcellularElementModel (MPI)
typedef void* (allocSEMGPUMPIFunction)(void*, void*, int, int, void*, void*);
typedef void (initSEMGPUMPIFunction)(void *, void *, int, void*, void*, void*, int);
typedef void (invokeSEMGPUMPIFunction)(void *, void *, double, double);
typedef void (releaseSEMGPUMPIFunction)(void *, void *);

typedef struct _SEMfunctions {
  // standard GPU
  allocSEMGPUFunction *allocGPUKernel;
  initSEMGPUFunction *initGPUKernel;
  invokeSEMGPUFunction *invokeGPUKernel;
  releaseSEMGPUFunction *releaseGPUKernel;
  // GPU MPI
  allocSEMGPUMPIFunction *allocGPUMPIKernel;
  initSEMGPUMPIFunction *initGPUMPIKernel;
  invokeSEMGPUMPIFunction *invokeGPUMPIKernel;
  releaseSEMGPUMPIFunction *releaseGPUMPIKernel;
} SEMfunctions;

// InterfaceModel
typedef	void* (allocInterfaceModelGPUFunction)(void*, int, void *, float);
typedef void (transferInterfaceModelGPUFunction)(void*, void*, int);
typedef void (invokeInterfaceModelGPUFunction)(void*, void*, double, double);
typedef void (releaseInterfaceModelGPUFunction)(void*, void*);

typedef struct _InterfaceGPUfunctions {
  allocInterfaceModelGPUFunction *allocGPUKernel;
  transferInterfaceModelGPUFunction *initGPUKernel;
  invokeInterfaceModelGPUFunction *invokeGPUKernel;
  releaseInterfaceModelGPUFunction *releaseGPUKernel;
} InterfaceGPUFunctions;

// Generic GPU interface functions for BioSwarm Model
typedef	void* (allocBioSwarmModelGPUFunction)(void*, float, float, float*, int, int);
typedef void (initBioSwarmModelGPUFunction)(void*, void*, int, float*, void*);
typedef void (invokeBioSwarmModelGPUFunction)(void*, void*, void*, double, double);
typedef void (releaseBioSwarmModelGPUFunction)(void*, void*);

typedef struct _BioSwarmGPUfunctions {
  allocBioSwarmModelGPUFunction *allocGPUKernel;
  initBioSwarmModelGPUFunction *initGPUKernel;
  invokeBioSwarmModelGPUFunction *invokeGPUKernel;
  releaseBioSwarmModelGPUFunction *releaseGPUKernel;
} BioSwarmGPUFunctions;


// Hill function
// General form
// f(X) = a + (b - a) / (1 + (X / c)^h)
#ifndef HILL
#define HILL(X, A, B, C, H) ((A) + ((B) - (A)) / (1.0f + pow((X) / (C), H)))
#endif
// Simpler form
// f(X) = a / (1 + (X / c)^h)
#ifndef SHILL
#define SHILL(X, A, C, H) ((A) / (1.0 + pow((X) / (C), H)))
#endif

#define GPU_FLOAT_EPS 1e-3f
#define GPU_DOUBLE_EPS 1e-8f
#define TIME_EQUAL(T1, T2) (((T1) < ((T2) + 1e-6)) && ((T1) > ((T2) - 1e-6)))

#define FORCE_TYPE_INTRA 0
#define FORCE_TYPE_INTER 1
#define FORCE_TYPE_COLONY 2
#define FORCE_TYPE_CELL_CENTER 3
#define FORCE_TYPE_ELEMENT_CENTER 4

//
// Sector neighbor function:
// A given sector has 26 neighboring sectors in 3D space.
// The X dimension is considered left/right.
// The Y dimension is considered up/down.
// The Z dimension is considered front/back.
// We number them sequentially, so we can easily iterate through them
// First 2 are neighbors in 1D
#define S_X_TOT 3
// First 8 are neighbors in 2D
#define S_XY_TOT 9
// All 26 are neighbors in 3D
#define S_XYZ_TOT 27
//
#define S_NONE -1
#define S_CURRENT 0
#define S_START 1
#define S_LEFT 1
#define S_RIGHT 2
#define S_UP 3
#define S_DOWN 4
#define S_LEFT_UP 5
#define S_RIGHT_UP 6
#define S_LEFT_DOWN 7
#define S_RIGHT_DOWN 8
#define S_FRONT 9
#define S_BACK 10
#define S_LEFT_FRONT 11
#define S_RIGHT_FRONT 12
#define S_LEFT_BACK 13
#define S_RIGHT_BACK 14
#define S_UP_FRONT 15
#define S_DOWN_FRONT 16
#define S_UP_BACK 17
#define S_DOWN_BACK 18
#define S_LEFT_UP_FRONT 19
#define S_RIGHT_UP_FRONT 20
#define S_LEFT_DOWN_FRONT 21
#define S_RIGHT_DOWN_FRONT 22
#define S_LEFT_UP_BACK 23
#define S_RIGHT_UP_BACK 24
#define S_LEFT_DOWN_BACK 25
#define S_RIGHT_DOWN_BACK 26

extern void bc_sector_coordinate_shift(int xc, int yc, int zc, int direction, int *xo, int *yo, int *zo);
extern char *bc_sector_direction_string(int direction);

//
// Mesh neighbors
//
// A given grid point has 26 neighboring grid points in 3D space, 9 in 2D space.
// The X dimension is considered left/right.
// The Y dimension is considered up/down.
// The Z dimension is considered front/back.
//
// D3Q19 in 3D space
// D2Q5 in 2D space
// D1Q3 in 1D space
//
// We number them sequentially, so we can easily iterate through them as array indexes.
// The current mesh grid point is included.
//
// First 2 are neighbors in 1D
#define MESH_X_TOT 3
// First 4 are neighbors in 2D
#define MESH_XY_TOT 5
// All 26 are neighbors in 3D
#define MESH_XYZ_TOT 19
//
#define MESH_NONE -1
#define MESH_CURRENT 0
#define MESH_START 1
#define MESH_LEFT 1
#define MESH_RIGHT 2
#define MESH_UP 3
#define MESH_DOWN 4
#define MESH_LEFT_UP 5
#define MESH_RIGHT_UP 6
#define MESH_LEFT_DOWN 7
#define MESH_RIGHT_DOWN 8
#define MESH_FRONT 9
#define MESH_BACK 10
#define MESH_LEFT_FRONT 11
#define MESH_RIGHT_FRONT 12
#define MESH_LEFT_BACK 13
#define MESH_RIGHT_BACK 14
#define MESH_UP_FRONT 15
#define MESH_DOWN_FRONT 16
#define MESH_UP_BACK 17
#define MESH_DOWN_BACK 18

//
// modes for MPI transfers
//
#define BC_MPI_SEM_MODE_INIT 1
#define BC_MPI_SEM_MODE_F1 2
#define BC_MPI_SEM_MODE_MOVE_READ_ONLY 3
#define BC_MPI_SEM_MODE_MOVE 4

#endif /* BC_GPU_DEFINES_H */
