/*
 GPU_devfuncs.h
 
 GPU device functions and macros.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */


//
// distance macros
//

#define dist(X1, Y1, Z1, X2, Y2, Z2) sqrt( ((X1) - (X2))*((X1) - (X2)) + ((Y1) - (Y2))*((Y1) - (Y2)) + ((Z1) - (Z2))*((Z1) - (Z2)) )
#define dist2(X1, Y1, Z1, X2, Y2, Z2) ((X1) - (X2))*((X1) - (X2)) + ((Y1) - (Y2))*((Y1) - (Y2)) + ((Z1) - (Z2))*((Z1) - (Z2))

#define dist2d(X1, Y1, X2, Y2) (((X1) - (X2))*((X1) - (X2)) + ((Y1) - (Y2))*((Y1) - (Y2)))
#define dist3d(X1, Y1, Z1, X2, Y2, Z2) (((X1) - (X2))*((X1) - (X2)) + ((Y1) - (Y2))*((Y1) - (Y2)) + ((Z1) - (Z2))*((Z1) - (Z2)))


//
// Morse potentials
//

__device__ float D_MORSE(float d, float rho, float eqSq, float factor)
{
  float f = exp(rho*(1.0 - d / eqSq));
  return factor*f*(f-1.0);
}

// Positive (repulse only)
__device__ float PD_MORSE(float d, float rho, float eqSq, float factor)
{
  float f = D_MORSE(d, rho, eqSq, factor);
  if (f < 0) return 0.0;
  else return f;
}

#define MORSE(r, U, UE, V, VE) (U)*exp(-(r)/(UE)) - (V)*exp(-(r)/(VE))

// only repulsive force, zero out attractive force
__device__ float PMORSE(float r, float U, float UE, float V, float VE)
{
  float m = MORSE(r, U, UE, V, VE);
  if (m < 0) return 0.0;
  else return m;
}


// old potentials
#define INTRA_U0 0.3
#define INTRA_U1 0.1
#define INTRA_ETA0 0.12
#define INTRA_ETA1 0.36


//
// Sector neighbor function:
// A given sector has 26 neighboring sectors in 3D space.
// The X dimension is considered left/right.
// The Y dimension is considered up/down.
// The Z dimension is considered front/back.
// We number them sequentially, so we can easily iterate through them
// First 2 are neighbors in 1D
#define S_X_TOT 3
// First 8 are neighbors in 2D
#define S_XY_TOT 9
// All 26 are neighbors in 3D
#define S_XYZ_TOT 27
//
#define S_NONE -1
#define S_CURRENT 0
#define S_START 1
#define S_LEFT 1
#define S_RIGHT 2
#define S_UP 3
#define S_DOWN 4
#define S_LEFT_UP 5
#define S_RIGHT_UP 6
#define S_LEFT_DOWN 7
#define S_RIGHT_DOWN 8
#define S_FRONT 9
#define S_BACK 10
#define S_LEFT_FRONT 11
#define S_RIGHT_FRONT 12
#define S_LEFT_BACK 13
#define S_RIGHT_BACK 14
#define S_UP_FRONT 15
#define S_DOWN_FRONT 16
#define S_UP_BACK 17
#define S_DOWN_BACK 18
#define S_LEFT_UP_FRONT 19
#define S_RIGHT_UP_FRONT 20
#define S_LEFT_DOWN_FRONT 21
#define S_RIGHT_DOWN_FRONT 22
#define S_LEFT_UP_BACK 23
#define S_RIGHT_UP_BACK 24
#define S_LEFT_DOWN_BACK 25
#define S_RIGHT_DOWN_BACK 26

// translate coordinates to specified mirror
__device__ void mirror_translate(float *X, float *Y, float *Z, int direction)
{
#if 0
  switch (direction) {
    case S_LEFT:
      *X = *X - BOUNDARY_X; break;
    case S_RIGHT:
      *X = *X + BOUNDARY_X; break;
    case S_UP:
      *Y = *Y + BOUNDARY_Y; break;
    case S_DOWN:
      *Y = *Y - BOUNDARY_Y; break;
    case S_LEFT_UP:
      *X = *X - BOUNDARY_X; *Y = *Y + BOUNDARY_Y; break;
    case S_RIGHT_UP:
      *X = *X + BOUNDARY_X; *Y = *Y + BOUNDARY_Y; break;
    case S_LEFT_DOWN:
      *X = *X - BOUNDARY_X; *Y = *Y - BOUNDARY_Y; break;
    case S_RIGHT_DOWN:
      *X = *X + BOUNDARY_X; *Y = *Y - BOUNDARY_Y; break;
    case S_FRONT:
      *Z = *Z - BOUNDARY_Z; break;
    case S_BACK:
      *Z = *Z + BOUNDARY_Z; break;
    case S_LEFT_FRONT:
      *X = *X - BOUNDARY_X; *Z = *Z - BOUNDARY_Z; break;
    case S_RIGHT_FRONT:
      *X = *X + BOUNDARY_X; *Z = *Z - BOUNDARY_Z; break;
    case S_LEFT_BACK:
      *X = *X - BOUNDARY_X; *Z = *Z + BOUNDARY_Z; break;
    case S_RIGHT_BACK:
      *X = *X + BOUNDARY_X; *Z = *Z + BOUNDARY_Z; break;
    case S_UP_FRONT:
      *Y = *Y + BOUNDARY_Y; *Z = *Z - BOUNDARY_Z; break;
    case S_DOWN_FRONT:
      *Y = *Y - BOUNDARY_Y; *Z = *Z - BOUNDARY_Z; break;
    case S_UP_BACK:
      *Y = *Y + BOUNDARY_Y; *Z = *Z + BOUNDARY_Z; break;
    case S_DOWN_BACK:
      *Y = *Y - BOUNDARY_Y; *Z = *Z + BOUNDARY_Z; break;
    case S_LEFT_UP_FRONT:
      *X = *X - BOUNDARY_X; *Y = *Y + BOUNDARY_Y; *Z = *Z - BOUNDARY_Z; break;
    case S_RIGHT_UP_FRONT:
      *X = *X + BOUNDARY_X; *Y = *Y + BOUNDARY_Y; *Z = *Z - BOUNDARY_Z; break;
    case S_LEFT_DOWN_FRONT:
      *X = *X - BOUNDARY_X; *Y = *Y - BOUNDARY_Y; *Z = *Z - BOUNDARY_Z; break;
    case S_RIGHT_DOWN_FRONT:
      *X = *X + BOUNDARY_X; *Y = *Y - BOUNDARY_Y; *Z = *Z - BOUNDARY_Z; break;
    case S_LEFT_UP_BACK:
      *X = *X - BOUNDARY_X; *Y = *Y + BOUNDARY_Y; *Z = *Z + BOUNDARY_Z; break;
    case S_RIGHT_UP_BACK:
      *X = *X + BOUNDARY_X; *Y = *Y + BOUNDARY_Y; *Z = *Z + BOUNDARY_Z; break;
    case S_LEFT_DOWN_BACK:
      *X = *X - BOUNDARY_X; *Y = *Y - BOUNDARY_Y; *Z = *Z + BOUNDARY_Z; break;
    case S_RIGHT_DOWN_BACK:
      *X = *X + BOUNDARY_X; *Y = *Y - BOUNDARY_Y; *Z = *Z + BOUNDARY_Z; break;
  }
#endif
}

__device__ void sector_coordinate_shift(int xc, int yc, int zc, int direction, int *X, int *Y, int *Z)
{
  *X = xc;
  *Y = yc;
  *Z = zc;
  switch (direction) {
    case S_LEFT:
      *X = *X - 1; break;
    case S_RIGHT:
      *X = *X + 1; break;
    case S_UP:
      *Y = *Y + 1; break;
    case S_DOWN:
      *Y = *Y - 1; break;
    case S_LEFT_UP:
      *X = *X - 1; *Y = *Y + 1; break;
    case S_RIGHT_UP:
      *X = *X + 1; *Y = *Y + 1; break;
    case S_LEFT_DOWN:
      *X = *X - 1; *Y = *Y - 1; break;
    case S_RIGHT_DOWN:
      *X = *X + 1; *Y = *Y - 1; break;
    case S_FRONT:
      *Z = *Z - 1; break;
    case S_BACK:
      *Z = *Z + 1; break;
    case S_LEFT_FRONT:
      *X = *X - 1; *Z = *Z - 1; break;
    case S_RIGHT_FRONT:
      *X = *X + 1; *Z = *Z - 1; break;
    case S_LEFT_BACK:
      *X = *X - 1; *Z = *Z + 1; break;
    case S_RIGHT_BACK:
      *X = *X + 1; *Z = *Z + 1; break;
    case S_UP_FRONT:
      *Y = *Y + 1; *Z = *Z - 1; break;
    case S_DOWN_FRONT:
      *Y = *Y - 1; *Z = *Z - 1; break;
    case S_UP_BACK:
      *Y = *Y + 1; *Z = *Z + 1; break;
    case S_DOWN_BACK:
      *Y = *Y - 1; *Z = *Z + 1; break;
    case S_LEFT_UP_FRONT:
      *X = *X - 1; *Y = *Y + 1; *Z = *Z - 1; break;
    case S_RIGHT_UP_FRONT:
      *X = *X + 1; *Y = *Y + 1; *Z = *Z - 1; break;
    case S_LEFT_DOWN_FRONT:
      *X = *X - 1; *Y = *Y - 1; *Z = *Z - 1; break;
    case S_RIGHT_DOWN_FRONT:
      *X = *X + 1; *Y = *Y - 1; *Z = *Z - 1; break;
    case S_LEFT_UP_BACK:
      *X = *X - 1; *Y = *Y + 1; *Z = *Z + 1; break;
    case S_RIGHT_UP_BACK:
      *X = *X + 1; *Y = *Y + 1; *Z = *Z + 1; break;
    case S_LEFT_DOWN_BACK:
      *X = *X - 1; *Y = *Y - 1; *Z = *Z + 1; break;
    case S_RIGHT_DOWN_BACK:
      *X = *X + 1; *Y = *Y - 1; *Z = *Z + 1; break;
  }
}

__device__ int sector_determine_direction(int xc, int yc, int zc, int nxc, int nyc, int nzc)
{
  if (xc == nxc) {
    if (yc == nyc) {
      if (zc == nzc) return S_CURRENT;
      if (zc == (nzc + 1)) return S_FRONT;
      if (zc == (nzc - 1)) return S_BACK;
      return S_NONE;
    }

    if (yc == (nyc + 1)) {
      if (zc == nzc) return S_DOWN;
      if (zc == (nzc + 1)) return S_DOWN_FRONT;
      if (zc == (nzc - 1)) return S_DOWN_BACK;
      return S_NONE;
    }

    if (yc == (nyc - 1)) {
      if (zc == nzc) return S_UP;
      if (zc == (nzc + 1)) return S_UP_FRONT;
      if (zc == (nzc - 1)) return S_UP_BACK;
      return S_NONE;
    }
  }

  if (xc == (nxc + 1)) {
    //printf("sector_determine_direction(%d, %d, %d), (%d, %d, %d)\n", xc, yc, zc, nxc, nyc, nzc);
    if (yc == nyc) {
      if (zc == nzc) return S_LEFT;
      if (zc == (nzc + 1)) return S_LEFT_FRONT;
      if (zc == (nzc - 1)) return S_LEFT_BACK;
      return S_NONE;
    }
    
    if (yc == (nyc + 1)) {
      if (zc == nzc) return S_LEFT_DOWN;
      if (zc == (nzc + 1)) return S_LEFT_DOWN_FRONT;
      if (zc == (nzc - 1)) return S_LEFT_DOWN_BACK;
      return S_NONE;
    }
    
    if (yc == (nyc - 1)) {
      if (zc == nzc) return S_LEFT_UP;
      if (zc == (nzc + 1)) return S_LEFT_UP_FRONT;
      if (zc == (nzc - 1)) return S_LEFT_UP_BACK;
      return S_NONE;
    }
  }

  if (xc == (nxc - 1)) {
    if (yc == nyc) {
      if (zc == nzc) return S_RIGHT;
      if (zc == (nzc + 1)) return S_RIGHT_FRONT;
      if (zc == (nzc - 1)) return S_RIGHT_BACK;
      return S_NONE;
    }
    
    if (yc == (nyc + 1)) {
      if (zc == nzc) return S_RIGHT_DOWN;
      if (zc == (nzc + 1)) return S_RIGHT_DOWN_FRONT;
      if (zc == (nzc - 1)) return S_RIGHT_DOWN_BACK;
      return S_NONE;
    }
    
    if (yc == (nyc - 1)) {
      if (zc == nzc) return S_RIGHT_UP;
      if (zc == (nzc + 1)) return S_RIGHT_UP_FRONT;
      if (zc == (nzc - 1)) return S_RIGHT_UP_BACK;
      return S_NONE;
    }
  }

  return S_NONE;
}

// TODO: Disabled at moment
// Need to use new Morse potentials
// and optimize
#if 0
// intracellular mirror calculations for periodic boundary conditions
__device__ void
intracellular_mirror(int elemNum, int cellNum, int pitch, int k, float *intraX, float *intraY, float *intraZ, float *X, float *Y, float *Z)
{
  float oX, oY, r, V;
  
  // left mirror
  oX = X[k*pitch+cellNum] - BOUNDARY_X;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, Y[k*pitch+cellNum], Z[k*pitch+cellNum]);
  V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
  *intraX += V * (X[elemNum*pitch+cellNum] - oX);
  *intraY += V * (Y[elemNum*pitch+cellNum] - Y[k*pitch+cellNum]);
  *intraZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+cellNum]);
  
  // right mirror
  oX = X[k*pitch+cellNum] + BOUNDARY_X;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, Y[k*pitch+cellNum], Z[k*pitch+cellNum]);
  V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
  *intraX += V * (X[elemNum*pitch+cellNum] - oX);
  *intraY += V * (Y[elemNum*pitch+cellNum] - Y[k*pitch+cellNum]);
  *intraZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+cellNum]);
  
  // down mirror
  oY = Y[k*pitch+cellNum] - BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], X[k*pitch+cellNum], oY, Z[k*pitch+cellNum]);
  V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
  *intraX += V * (X[elemNum*pitch+cellNum] - X[k*pitch+cellNum]);
  *intraY += V * (Y[elemNum*pitch+cellNum] - oY);
  *intraZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+cellNum]);
  
  // up mirror
  oY = Y[k*pitch+cellNum] + BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], X[k*pitch+cellNum], oY, Z[k*pitch+cellNum]);
  V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
  *intraX += V * (X[elemNum*pitch+cellNum] - X[k*pitch+cellNum]);
  *intraY += V * (Y[elemNum*pitch+cellNum] - oY);
  *intraZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+cellNum]);
  
  // up-left mirror
  oX = X[k*pitch+cellNum] - BOUNDARY_X;
  oY = Y[k*pitch+cellNum] + BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, oY, Z[k*pitch+cellNum]);
  V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
  *intraX += V * (X[elemNum*pitch+cellNum] - oX);
  *intraY += V * (Y[elemNum*pitch+cellNum] - oY);
  *intraZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+cellNum]);
  
  // up-right mirror
  oX = X[k*pitch+cellNum] + BOUNDARY_X;
  oY = Y[k*pitch+cellNum] + BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, oY, Z[k*pitch+cellNum]);
  V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
  *intraX += V * (X[elemNum*pitch+cellNum] - oX);
  *intraY += V * (Y[elemNum*pitch+cellNum] - oY);
  *intraZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+cellNum]);
  
  // down-left mirror
  oX = X[k*pitch+cellNum] - BOUNDARY_X;
  oY = Y[k*pitch+cellNum] - BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, oY, Z[k*pitch+cellNum]);
  V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
  *intraX += V * (X[elemNum*pitch+cellNum] - oX);
  *intraY += V * (Y[elemNum*pitch+cellNum] - oY);
  *intraZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+cellNum]);
  
  // down-right mirror
  oX = X[k*pitch+cellNum] + BOUNDARY_X;
  oY = Y[k*pitch+cellNum] - BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, oY, Z[k*pitch+cellNum]);
  V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
  *intraX += V * (X[elemNum*pitch+cellNum] - oX);
  *intraY += V * (Y[elemNum*pitch+cellNum] - oY);
  *intraZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+cellNum]);
}

// intercellular mirror calculations for periodic boundary conditions
__device__ void
intercellular_mirror(int elemNum, int cellNum, int pitch, int k, int j, float *interX, float *interY, float *interZ, float *X, float *Y, float *Z)
{
  float oX, oY, r, V;
  
  // left mirror
  oX = X[k*pitch+j] - BOUNDARY_X;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, Y[k*pitch+j], Z[k*pitch+j]);
  if (r <= INTER_DIST) {
    V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    *interX += V * (X[elemNum*pitch+cellNum] - oX);
    *interY += V * (Y[elemNum*pitch+cellNum] - Y[k*pitch+j]);
    *interZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+j]);
  }
  
  // right mirror
  oX = X[k*pitch+j] + BOUNDARY_X;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, Y[k*pitch+j], Z[k*pitch+j]);
  if (r <= INTER_DIST) {
    V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    *interX += V * (X[elemNum*pitch+cellNum] - oX);
    *interY += V * (Y[elemNum*pitch+cellNum] - Y[k*pitch+j]);
    *interZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+j]);
  }
  
  // down mirror
  oY = Y[k*pitch+j] - BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], X[k*pitch+j], oY, Z[k*pitch+j]);
  if (r <= INTER_DIST) {
    V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    *interX += V * (X[elemNum*pitch+cellNum] - X[k*pitch+j]);
    *interY += V * (Y[elemNum*pitch+cellNum] - oY);
    *interZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+j]);
  }
  
  // up mirror
  oY = Y[k*pitch+j] + BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], X[k*pitch+j], oY, Z[k*pitch+j]);
  if (r <= INTER_DIST) {
    V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    *interX += V * (X[elemNum*pitch+cellNum] - X[k*pitch+j]);
    *interY += V * (Y[elemNum*pitch+cellNum] - oY);
    *interZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+j]);
  }
  
  // up-left mirror
  oX = X[k*pitch+j] - BOUNDARY_X;
  oY = Y[k*pitch+j] + BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, oY, Z[k*pitch+j]);
  if (r <= INTER_DIST) {
    V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    *interX += V * (X[elemNum*pitch+cellNum] - oX);
    *interY += V * (Y[elemNum*pitch+cellNum] - oY);
    *interZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+j]);
  }
  
  // up-right mirror
  oX = X[k*pitch+j] + BOUNDARY_X;
  oY = Y[k*pitch+j] + BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, oY, Z[k*pitch+j]);
  if (r <= INTER_DIST) {
    V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    *interX += V * (X[elemNum*pitch+cellNum] - oX);
    *interY += V * (Y[elemNum*pitch+cellNum] - oY);
    *interZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+j]);
  }
  
  // down-left mirror
  oX = X[k*pitch+j] - BOUNDARY_X;
  oY = Y[k*pitch+j] - BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, oY, Z[k*pitch+j]);
  if (r <= INTER_DIST) {
    V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    *interX += V * (X[elemNum*pitch+cellNum] - oX);
    *interY += V * (Y[elemNum*pitch+cellNum] - oY);
    *interZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+j]);
  }
  
  // down-right mirror
  oX = X[k*pitch+j] + BOUNDARY_X;
  oY = Y[k*pitch+j] - BOUNDARY_Y;
  r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], oX, oY, Z[k*pitch+j]);
  if (r <= INTER_DIST) {
    V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    *interX += V * (X[elemNum*pitch+cellNum] - oX);
    *interY += V * (Y[elemNum*pitch+cellNum] - oY);
    *interZ += V * (Z[elemNum*pitch+cellNum] - Z[k*pitch+j]);
  }
}
#endif

__device__ int
distance_check(float X, float Y, float Z, float oX, float oY, float oZ, int maxDirection, float distance)
{
  int direction;
  float nX, nY, nZ;
  
  for (direction = S_CURRENT; direction < maxDirection; ++direction) {
    nX = oX;
    nY = oY;
    nZ = oZ;
    mirror_translate(&nX, &nY, &nZ, direction);
    if (dist(X, Y, Z, nX, nY, nZ) <= distance)
      return direction;
  }
  
  return S_NONE;
}
