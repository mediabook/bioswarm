/*
 GPU_MPI.h
 
 GPU kernels for MPI.
 
 Copyright (C) 2015 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2015
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#ifndef _BC_GPU_MPI_H_
#define _BC_GPU_MPI_H_

// GPU-MPI data transfer functions
extern void BC_MPI_initiate_transfer(void *model, int mode, double currentTime, double nextTime);
extern void BC_MPI_finalize_transfer(void *model, int mode, double currentTime, double nextTime);

// Subcellular element method
typedef struct _BC_MPI_SEM_GPUdata {
  int boundarySize;
  int sectorsInDomain;
  int *incomingSectors;
  int *outgoingSectors;
  float *incomingElements;
  float *outgoingElements;
  size_t pitch;
  size_t idx_pitch;
  size_t ePitch;
  size_t idx_ePitch;
} BC_MPI_SEM_GPUdata;

//
// GPU-CPU interface functions
//

// Allocation
void *BC_MPI_SEM_allocGPUKernel(void *model, void *g, int boundarySize, int sectorsInDomain, void *incomingSectors, void *outgoingSectors)
{
  BC_MPI_SEM_GPUdata *mpi_data = (BC_MPI_SEM_GPUdata *)malloc(sizeof(BC_MPI_SEM_GPUdata));
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)g;
  int i;

  // Save parameters
  sem_data->mpiData = (void *)mpi_data;
  mpi_data->boundarySize = boundarySize;
  mpi_data->sectorsInDomain = sectorsInDomain;
  printf("BC_MPI_SEM_allocGPUKernel: %d\n", mpi_data->boundarySize);

  // Allocate device memory

  // sectors numbers
  //cudaMallocPitch((void**)&(mpi_data->incomingSectors), &(mpi_data->pitch), sem_data->maxElements * sizeof(int), sem_data->numOfModels);
  cudaMallocPitch((void**)&(mpi_data->incomingSectors), &(mpi_data->pitch), sem_data->sectorNeighborSize * sizeof(int), boundarySize);
  cudacheck("allocate incomingSectors");
  cudaMemcpy2D(mpi_data->incomingSectors, mpi_data->pitch, incomingSectors, sem_data->sectorNeighborSize * sizeof(int), sem_data->sectorNeighborSize * sizeof(int), boundarySize, cudaMemcpyHostToDevice);
  cudacheck("transfer incomingSectors");

  cudaMallocPitch((void**)&(mpi_data->outgoingSectors), &(mpi_data->pitch), sem_data->sectorNeighborSize * sizeof(int), boundarySize);
  cudacheck("allocate outgoingSectors");
  cudaMemcpy2D(mpi_data->outgoingSectors, mpi_data->pitch, outgoingSectors, sem_data->sectorNeighborSize * sizeof(int), sem_data->sectorNeighborSize * sizeof(int), boundarySize, cudaMemcpyHostToDevice);
  cudacheck("transfer outgoingSectors");

  // element data
  cudaMallocPitch((void**)&(mpi_data->incomingElements), &(mpi_data->ePitch), 4 * boundarySize * sem_data->maxElementsPerSector * sizeof(float), sem_data->sectorNeighborSize);
  cudacheck("allocate incomingElements");
  cudaMallocPitch((void**)&(mpi_data->outgoingElements), &(mpi_data->ePitch), 4 * boundarySize * sem_data->maxElementsPerSector * sizeof(float), sem_data->sectorNeighborSize);
  cudacheck("allocate outgoingElements");

  // index pitch
  mpi_data->idx_pitch = mpi_data->pitch / sizeof(int);
  mpi_data->idx_ePitch = mpi_data->ePitch / sizeof(float);
  
  return mpi_data;
}

// Data Transfer
void BC_MPI_SEM_transferGPUKernel(void *model, void *g, int aFlag, void *sem, void *incomingElements, void *outgoingElements, int totalElements)
{
  BC_MPI_SEM_GPUdata *mpi_data = (BC_MPI_SEM_GPUdata *)g;
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)sem;

  //printf("BC_MPI_SEM_transferGPUKernel: %p %p %d %p %p %p\n", model, g, aFlag, sem, incomingElements, outgoingElements);

  if (aFlag) {
    // Copy host memory to device memory
    // element data
    cudaMemcpy2D(mpi_data->incomingElements, mpi_data->ePitch, incomingElements,
		 4 * mpi_data->boundarySize * sem_data->maxElementsPerSector * sizeof(float),
		 4 * mpi_data->boundarySize * sem_data->maxElementsPerSector * sizeof(float),
		 sem_data->sectorNeighborSize,
		 cudaMemcpyHostToDevice);
    cudacheck("transfer incomingElements");

  } else {
    // Copy result to host memory
    cudaMemcpy2D(outgoingElements, 4 * mpi_data->boundarySize * sem_data->maxElementsPerSector * sizeof(float),
		 mpi_data->outgoingElements, mpi_data->ePitch,
		 4 * mpi_data->boundarySize * sem_data->maxElementsPerSector * sizeof(float),
		 sem_data->sectorNeighborSize,
		 cudaMemcpyDeviceToHost);
    cudacheck("transfer outgoingElements");
  }
}

// Release
void BC_MPI_SEM_releaseGPUKernel(void *model, void *g)
{
  BC_MPI_SEM_GPUdata *grids = (BC_MPI_SEM_GPUdata *)g;
  cudaFree(grids->incomingSectors);
  cudaFree(grids->outgoingSectors);
  cudaFree(grids->incomingElements);
  cudaFree(grids->outgoingElements);

  free(grids);
  cudaThreadExit();
}

//
//
//

// Gather outgoing element data
__device__ void
BC_MPI_SEM_gather(void *data, void *sem, int mode)
{
  BC_MPI_SEM_GPUdata *mpi_data = (BC_MPI_SEM_GPUdata *)data;
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)sem;
  int idxNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = 0;
  int direction;
  
  if (idxNum >= mpi_data->boundarySize * sem_data->maxElementsPerSector) return;
  int boundarySector = idxNum / sem_data->maxElementsPerSector;
  int offset = idxNum - boundarySector * sem_data->maxElementsPerSector;

  // the number of sectors is different for each neighbor, but we allocate
  // the array to be the same size for all, and rely upon the sector number
  // to know whether to copy data or not
  for (direction = S_START; direction < sem_data->sectorNeighborSize; ++direction) {
      switch (mode) {
      case BC_MPI_SEM_MODE_INIT: {
	int sectorNum = mpi_data->outgoingSectors[boundarySector*mpi_data->idx_pitch+direction];
	//if ((direction == S_LEFT) && (offset == 0)) printf("(%d) (%d, %d, %d) = %d, %d\n", direction, idxNum, boundarySector, offset, direction, sectorNum);
	if (sectorNum == -1) continue;

	int elemNum = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + offset)*sem_data->idx_sPitch + sectorNum];
	//if (sectorNum == 358) printf("(%d, %d, %d) = %d, %d, %d\n", idxNum, boundarySector, offset, direction, sectorNum, elemNum);
	mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+0)] = elemNum;
	if (elemNum != -1) {
	  // current data
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)] = sem_data->X[modelNum*sem_data->idx_pitch+elemNum];
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)] = sem_data->Y[modelNum*sem_data->idx_pitch+elemNum];
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)] = sem_data->Z[modelNum*sem_data->idx_pitch+elemNum];
	  //if (sectorNum == 358) printf("(%d) sector = %d, element = %d, %f, %f, %f\n", direction, sectorNum, elemNum, sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum], sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	}
	break;
      }

      case BC_MPI_SEM_MODE_F1: {
	int sectorNum = mpi_data->outgoingSectors[boundarySector*mpi_data->idx_pitch+direction];
	if (sectorNum == -1) continue;

	int elemNum = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + offset)*sem_data->idx_sPitch + sectorNum];
	mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+0)] = elemNum;
	if (elemNum != -1) {
	  // data after F1 kernel
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)] = sem_data->X_F1[modelNum*sem_data->idx_pitch+elemNum];
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)] = sem_data->Y_F1[modelNum*sem_data->idx_pitch+elemNum];
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)] = sem_data->Z_F1[modelNum*sem_data->idx_pitch+elemNum];
	  //printf("GPU: (%d) sector = %d, outgoing element = %d, %f, %f, %f\n", direction, sectorNum, elemNum, sem_data->X_F1[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y_F1[modelNum*sem_data->idx_pitch+elemNum], sem_data->Z_F1[modelNum*sem_data->idx_pitch+elemNum]);
	}
	break;
      }

      case BC_MPI_SEM_MODE_MOVE: {
	// gather elements for outgoing border
	// exclude elements that have moved out of sector, include elements that moved in
	int sectorNum = mpi_data->outgoingSectors[boundarySector*mpi_data->idx_pitch+direction];
	if (sectorNum == -1) continue;

	int elemNum = sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + offset)*sem_data->idx_sPitch + sectorNum];
	mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+0)] = elemNum;
	if (elemNum != -1) {
	  // elements that have moved out, have moveDirection set but have this sector number
	  int moveDirection = sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum];
	  int elemSector = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
	  if (moveDirection) printf("GPU (MOVE): element that is moving %d (%.15f, %.15f, %.15f), direction = %d, sector = %d (%d, %d, %d), %d (%d, %d, %d)\n",
				    elemNum, sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
				    sem_data->Z[modelNum*sem_data->idx_pitch+elemNum], moveDirection,
				    elemSector,
				    sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + elemSector],
				    sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + elemSector],
				    sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + elemSector],
				    sectorNum,
				    sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + sectorNum],
				    sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + sectorNum],
				    sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + sectorNum]);

	  if ((moveDirection) && (elemSector == sectorNum)) {
	    mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+0)] = elemNum;  // do not think we need this???
	  } else {
	    mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)] = sem_data->X[modelNum*sem_data->idx_pitch+elemNum];
	    mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)] = sem_data->Y[modelNum*sem_data->idx_pitch+elemNum];
	    mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)] = sem_data->Z[modelNum*sem_data->idx_pitch+elemNum];
	  }
	}
	break;
      }

      case BC_MPI_SEM_MODE_MOVE_READ_ONLY: {
	// determine which elements have moved into the border sectors
	// if element moved, has to be from a local sector because the border sectors are read-only
	int sectorNum = mpi_data->incomingSectors[boundarySector*mpi_data->idx_pitch+direction];
	if (sectorNum == -1) continue;

	mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+0)] = -1;
	int elemNum = sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + offset)*sem_data->idx_sPitch + sectorNum];
	if (elemNum == -1) continue;

	int moveDirection = sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum];
	if (moveDirection) {
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+0)] = elemNum;
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)] = sem_data->X[modelNum*sem_data->idx_pitch+elemNum];
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)] = sem_data->Y[modelNum*sem_data->idx_pitch+elemNum];
	  mpi_data->outgoingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)] = sem_data->Z[modelNum*sem_data->idx_pitch+elemNum];
	  printf("GPU (MOVE_READ_ONLY): moving element %d (%.15f, %.15f, %.15f), direction = %d, sector = %d\n", elemNum, sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum], sem_data->Z[modelNum*sem_data->idx_pitch+elemNum], moveDirection, sectorNum);
	}
	break;
      }

      }
  }
}

// Scatter incoming element data
__device__ void
BC_MPI_SEM_scatter(void *data, void *sem, int mode)
{
  BC_MPI_SEM_GPUdata *mpi_data = (BC_MPI_SEM_GPUdata *)data;
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)sem;
  int idxNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = 0;
  int direction;
  
  if (idxNum >= mpi_data->boundarySize * sem_data->maxElementsPerSector) return;

  for (direction = S_START; direction < sem_data->sectorNeighborSize; ++direction) {
    int elemNum = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+0)];

    if (elemNum != -1) {
      switch (mode) {

      case BC_MPI_SEM_MODE_F1:
	// data after F1 kernel
	// elements not in sector order, but only need element number to update
	sem_data->X_F1[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)];
	sem_data->Y_F1[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)];
	sem_data->Z_F1[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)];
	//if (elemNum == 308) printf("GPU: (%d) incoming element = %d, %f, %f, %f\n", direction, elemNum, mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)], mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)], mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)]);
	break;

      case BC_MPI_SEM_MODE_MOVE_READ_ONLY: {
	// elements that moved from a read-only sector into a local sector
	// elements not in sector order, we update element data here and update sector data elsewhere
	sem_data->X[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)];
	sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)];
	sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)];

	// Element keeps same local element number so its data still valid
	// calculate sector coordinates
	int sectorNum = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
	int xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	int yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	int zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	int sx = sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + sectorNum];
	int sy = sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + sectorNum];
	int sz = sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + sectorNum];

	printf("GPU (MOVE_READ_ONLY): new element %d (%.15f, %.15f, %.15f), old sector = %d, %.15f\n", elemNum, mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)], mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)], mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)], sectorNum, sem_data->sectorSize[modelNum]);

	// check if same or new sector
	// if this element does not move, we got a problem
	int direction = sector_determine_direction(sx, sy, sz, xc, yc, zc);
	if (direction == -1) printf("GPU_ERROR (MOVE_READ_ONLY): sector_determine_direction(%d, %d, %d, %f, %f, %f) failed, sector = %d (%d, %d, %d), element = %d.\n",
				    xc, yc, zc, sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
				    sem_data->Z[modelNum*sem_data->idx_pitch+elemNum], sectorNum, sx, sy, sz, elemNum);
	if (direction == S_CURRENT) {
	  // assume floating point precision error, tweak position to be in local sector
	  if (xc == mpi_data->sectorsInDomain) {
	    sem_data->X[modelNum*sem_data->idx_pitch+elemNum] = (mpi_data->sectorsInDomain - 1)*sem_data->sectorSize[modelNum] + (sem_data->sectorSize[modelNum] * 0.999);
	    xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	  }
	  if (xc == -1) {
	    sem_data->X[modelNum*sem_data->idx_pitch+elemNum] = 0.001;
	    xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	  }
	  if (yc == mpi_data->sectorsInDomain) {
	    sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] = (mpi_data->sectorsInDomain - 1)*sem_data->sectorSize[modelNum] + (sem_data->sectorSize[modelNum] * 0.999);
	    yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	  }
	  if (yc == -1) {
	    sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] = 0.001;
	    yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	  }
	  if (zc == mpi_data->sectorsInDomain) {
	    sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] = (mpi_data->sectorsInDomain - 1)*sem_data->sectorSize[modelNum] + (sem_data->sectorSize[modelNum] * 0.999);
	    zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	  }
	  if (zc == -1) {
	    sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] = 0.001;
	    zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	  }
	  int direction = sector_determine_direction(sx, sy, sz, xc, yc, zc);
	  if (direction == S_CURRENT) {
	    printf("GPU_ERROR (MOVE_READ_ONLY): element %d did not move (%d, %d, %d) ==> (%d, %d, %d) could not tweak position (%.15f, %.15f, %.15f)\n", elemNum, sx, sy, sz, xc, yc, zc,
		   sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		   sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  } else {
	    printf("GPU_WARNING (MOVE_READ_ONLY): element %d did not move (%d, %d, %d) ==> (%d, %d, %d) tweaked position (%.15f, %.15f, %.15f)\n", elemNum, sx, sy, sz, xc, yc, zc,
		   sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		   sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  }
	}
	sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum] = direction;

	break;
      }

      case BC_MPI_SEM_MODE_MOVE: {
	// elements in read-only border sector
	// elements not in sector order, we update element data here and update sector data elsewhere
	sem_data->X[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)];
	sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)];
	sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)];
	sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum] = S_CURRENT;
	if (!sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum]) printf("GPU (MOVE scatter): new element %d (%f, %f, %f), sector = %d\n", elemNum, mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+1)], mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+2)], mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*idxNum+3)], sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum]);

	// check for floating point precision error, tweak position to be in sector
	int xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	int yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	int zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	if (xc == (mpi_data->sectorsInDomain+1)) {
	  sem_data->X[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->sectorsInDomain*sem_data->sectorSize[modelNum] + (sem_data->sectorSize[modelNum] * 0.999);
	  printf("GPU_WARNING (MOVE): element %d position not in valid sector (%d, %d, %d), tweaked position (%.15f, %.15f, %.15f)\n", elemNum, xc, yc, zc,
		 sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		 sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	}
	if (xc == -2) {
	  sem_data->X[modelNum*sem_data->idx_pitch+elemNum] = -0.999 * sem_data->sectorSize[modelNum];
	  printf("GPU_WARNING (MOVE): element %d position not in valid sector (%d, %d, %d), tweaked position (%.15f, %.15f, %.15f)\n", elemNum, xc, yc, zc,
		 sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		 sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	}
	if (yc == (mpi_data->sectorsInDomain+1)) {
	  sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->sectorsInDomain*sem_data->sectorSize[modelNum] + (sem_data->sectorSize[modelNum] * 0.999);
	  printf("GPU_WARNING (MOVE): element %d position not in valid sector (%d, %d, %d), tweaked position (%.15f, %.15f, %.15f)\n", elemNum, xc, yc, zc,
		 sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		 sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	}
	if (yc == -2) {
	  sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] = -0.999 * sem_data->sectorSize[modelNum];
	  printf("GPU_WARNING (MOVE): element %d position not in valid sector (%d, %d, %d), tweaked position (%.15f, %.15f, %.15f)\n", elemNum, xc, yc, zc,
		 sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		 sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	}
	if (zc == (mpi_data->sectorsInDomain+1)) {
	  sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] = mpi_data->sectorsInDomain *sem_data->sectorSize[modelNum] + (sem_data->sectorSize[modelNum] * 0.999);
	  printf("GPU_WARNING (MOVE): element %d position not in valid sector (%d, %d, %d), tweaked position (%.15f, %.15f, %.15f)\n", elemNum, xc, yc, zc,
		 sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		 sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	}
	if (zc == -2) {
	  sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] = -0.999 * sem_data->sectorSize[modelNum];
	  printf("GPU_WARNING (MOVE): element %d position not in valid sector (%d, %d, %d), tweaked position (%.15f, %.15f, %.15f)\n", elemNum, xc, yc, zc,
		 sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		 sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	}

	if ((xc < -1) || (xc > (mpi_data->sectorsInDomain+1))
	    || (yc < -1) || (yc > (mpi_data->sectorsInDomain+1)) || (zc < -1) || (zc > (mpi_data->sectorsInDomain+1)))
	  {
	    printf("GPU_ERROR (MOVE): element %d position not in valid sector (%d, %d, %d) could not tweak position (%.15f, %.15f, %.15f)\n", elemNum, xc, yc, zc,
		   sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
		   sem_data->Z[modelNum*sem_data->idx_pitch+elemNum]);
	  }

	break;
      }

      }
    }
  }
}

// Update sector data for scatter elements
__device__ void
BC_MPI_SEM_sector_scatter(void *data, void *sem, int mode)
{
  BC_MPI_SEM_GPUdata *mpi_data = (BC_MPI_SEM_GPUdata *)data;
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)sem;
  int idxNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = 0;
  int i, direction;
  
  if (idxNum >= mpi_data->boundarySize) return;

  for (direction = S_START; direction < sem_data->sectorNeighborSize; ++direction) {
    switch (mode) {

    case BC_MPI_SEM_MODE_MOVE_READ_ONLY: {
      // elements that moved from a read-only sector into a local sector
      // Has similarities to BC_SEM_move_elements_kernel()
      int sectorNum = mpi_data->outgoingSectors[idxNum*mpi_data->idx_pitch+direction];
      if (sectorNum == -1) continue;

      // find next open spot
      int eIdx = 0;
      for (i = 0; i < sem_data->maxElementsPerSector; ++i) {
	int elemNum = sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + sectorNum];
	if (elemNum == -1) break;
	++eIdx;
      }

      // loop through element list
      for (i = 0; i < mpi_data->boundarySize * sem_data->maxElementsPerSector; ++i) {
	int elemNum = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*i)];
	if (elemNum == -1) break;
	
	// did it move into our sector?
	int moveDirection = sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum];
	int oldSectorNum = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
	if (moveDirection) {
	  int moveSectorNum = sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + moveDirection)*sem_data->idx_sPitch + oldSectorNum];
	  if (moveSectorNum == sectorNum) {
	    if (eIdx == sem_data->maxElementsPerSector) printf("GPU_ERROR: Could not find slot to put element, maxElementsPerSector is full (%d, %d)\n", sectorNum, eIdx);

	    // put element in table
	    sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + eIdx)*sem_data->idx_sPitch + sectorNum] = elemNum;
	    //printf("putting element (%d) in open spot (%d)\n", elemNum, eIdx);
	    ++eIdx;
	  }
	}
      }
      break;
    }

    case BC_MPI_SEM_MODE_MOVE: {
      // instead of trying to figure out what elements are new or old
      // just overwrite the whole table
      int sectorNum = mpi_data->incomingSectors[idxNum*mpi_data->idx_pitch+direction];
      if (sectorNum == -1) continue;

      for (i = 0; i < sem_data->maxElementsPerSector; ++i)
	sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + sectorNum] = -1;

      int sx = sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + sectorNum];
      int sy = sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + sectorNum];
      int sz = sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + sectorNum];
      int eIdx = 0;

      // loop through element list
      for (i = 0; i < mpi_data->boundarySize * sem_data->maxElementsPerSector; ++i) {
	int elemNum = mpi_data->incomingElements[direction*mpi_data->idx_ePitch+(4*i)];
	if (elemNum == -1) break;

	int xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	int yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
	int zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);

	int found = 0;
	if (sem_data->dims == 2) {
	  if ((sx == xc) && (sy == yc)) found = 1;
	} else {
	  if ((sx == xc) && (sy == yc) && (sz == zc)) found = 1;
	}

	if (found) {
	  // put element in this sector
	  if (eIdx == sem_data->maxElementsPerSector) printf("GPU_ERROR: Could not find slot to put element, maxElementsPerSector is full (%d, %d)\n", sectorNum, eIdx);
	  sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + eIdx)*sem_data->idx_sPitch + sectorNum] = elemNum;
	  if (sectorNum != sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum])
	    printf("GPU (MOVE sector_scatter): (%d, %d, %d, %d, %d, %d), sector = %d, old = %d, element = %d.\n", sx, sy, sz, xc, yc, zc, sectorNum, sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum], elemNum);
	  ++eIdx;
	}
      }
      break;
    }

    }
  }
}

#endif // _BC_GPU_MPI_H_
