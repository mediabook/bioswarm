/*
 GPU_sem.h
 
 GPU kernels for subcellular element method.
 
 Copyright (C) 2010-2014 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#ifndef _BC_GPU_SEM_H_
#define _BC_GPU_SEM_H_

#define BOUNDARY_X 5.9
#define BOUNDARY_Y 5.9
#define BOUNDARY_Z 8.0

void cudacheck(const char *message);

#include "GPU_devfuncs.h"

typedef struct _BC_SEM_GPUgrids {
  int dims;
  int numOfModels;
  
  int maxCells;
  int numOfCells;
  int maxElements;
  int maxElementsPerSector;
  int totalElements;
  int maxElementTypes;
  int maxForces;
  int numOfParams;
  int calcCellCenters;
  int calcElementCenters;
  float dt;
  
  // element attributes
  int *elementType;
  int *cellNumber;
  int *readOnly;
  int useMesh;
  int *meshElements[MESH_XYZ_TOT];
  float *X;
  float *X_F1;
  float *X_F2;
  float *X_tmp;
  float *Y;
  float *Y_F1;
  float *Y_F2;
  float *Y_tmp;
  float *Z;
  float *Z_F1;
  float *Z_F2;
  float *Z_tmp;
  int *elementSector;
  int *newSectorFlag;
  float *forceSum;
  int *elementNeighborTable;
  float *elementNeighborForce_X;
  float *elementNeighborForce_Y;
  float *elementNeighborForce_Z;

  float *speciesInterface;
  float *speciesInterface_F1;
  float *speciesInterface_F2;

  size_t pitch;
  size_t idx_pitch;

  // sector
  int maxSectors;
  int *numSectors;
  float *sectorSize;
  int sectorNeighborSize;
  int *sectorCoordinates;
  int *sectorNeighborTable;
  int *sectorElementTable;
  int *sectorElementUpdate;
  int *newSectorCount;
  int *readOnlySector;
  int *rebuildSectorNeighborTable;
  size_t sPitch;
  size_t idx_sPitch;
  
  // cell attributes
  float *cellCenterX;
  float *cellCenterY;
  float *cellCenterZ;
  float *elementCenterX;
  float *elementCenterY;
  float *elementCenterZ;
  float *elementCenterCount;
  size_t cPitch;
  size_t idx_cPitch;
  
  int numOfSpecies;
  float *speciesData;
  float *parameters;
  size_t pPitch;
  size_t idx_pPitch;
  
  // element forces
  int *elementForceTable;
  int *elementForceTypeTable;
  int *elementTypeTable;
  int *elementForceFlagTable;
  size_t tPitch;
  size_t idx_tPitch;

  // MPI
  void *mpiData;
} BC_SEM_GPUgrids;

// parameters
#define BC_SEM_FINDEX (fIndex*sem_data->idx_tPitch+eType)
#define BC_SEM_PARAM_RHO sem_data->parameters[fNum*sem_data->idx_pPitch+modelNum]
#define BC_SEM_PARAM_FACTOR sem_data->parameters[(fNum+1)*sem_data->idx_pPitch+modelNum]
#define BC_SEM_PARAM_EQUIL_SQ sem_data->parameters[(fNum+2)*sem_data->idx_pPitch+modelNum]
#define BC_SEM_PARAM_U0 sem_data->parameters[fNum*sem_data->idx_pPitch+modelNum]
#define BC_SEM_PARAM_U1 sem_data->parameters[(fNum+1)*sem_data->idx_pPitch+modelNum]
#define BC_SEM_PARAM_ETA0 sem_data->parameters[(fNum+2)*sem_data->idx_pPitch+modelNum]
#define BC_SEM_PARAM_ETA1 sem_data->parameters[(fNum+3)*sem_data->idx_pPitch+modelNum]

//
// SEM movement function used by Runge-Kutta solvers
//

__device__ void
BC_SEM_kernel_function(void *data, int modelNum, int elemNum, float *X_in, float *Y_in, float *Z_in, float *X_out, float *Y_out, float *Z_out, float mh, float mf)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int idx = modelNum*sem_data->idx_pitch+elemNum;
  int j, k, fIndex, fNum;
  float F_X1, F_X2;
  float F_Y1, F_Y2;
  float F_Z1, F_Z2;
  float r, V;
  float forceX = 0.0;
  float forceY = 0.0;
  float forceZ = 0.0;
  
  // TODO: element is read only
  
  // element position
  F_X1 = sem_data->X[idx] + mf * X_in[idx];
  F_Y1 = sem_data->Y[idx] + mf * Y_in[idx];
  F_Z1 = sem_data->Z[idx] + mf * Z_in[idx];
  int eType = sem_data->elementType[idx];
  
  // forces between elements
  for (k = 0; k < sem_data->totalElements; ++k) {
    if (k == elemNum) continue;
    
    fIndex = 0;
    fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
    while (fNum >= 0) {
      switch (sem_data->elementForceFlagTable[BC_SEM_FINDEX]) {
        case FORCE_TYPE_INTRA: {
          // elements of same cell
          int oidx = modelNum*sem_data->idx_pitch+k;
          int oeType = sem_data->elementType[oidx];
          
          if (oeType == sem_data->elementTypeTable[BC_SEM_FINDEX]) {
            F_X2 = sem_data->X[oidx] + mf * X_in[oidx];
            F_Y2 = sem_data->Y[oidx] + mf * Y_in[oidx];
            F_Z2 = sem_data->Z[oidx] + mf * Z_in[oidx];
            
            V = 0;
            switch (sem_data->elementForceTypeTable[BC_SEM_FINDEX]) {
              case 0:
                // D_MORSE
                r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                V = D_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
                break;
                
              case 1:
                // PD_MORSE
                r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                V = PD_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
                break;
                
              case 2:
                // MORSE
                r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                V = MORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
                break;
                
              case 3:
                // PMORSE
                r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                V = PMORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
                break;
            }
            forceX += V * (F_X1 - F_X2);
            forceY += V * (F_Y1 - F_Y2);
            forceZ += V * (F_Z1 - F_Z2);
            //if (elemNum == 0) printf("force (%d, %d): (%f, %f, %f)\n", elemNum, k, forceX, forceY, forceZ);
          }
          break;
        }
          
        case FORCE_TYPE_INTER: {
          // elements of different cells
          break;
        }
          
        case FORCE_TYPE_COLONY: {
          // elements of different colonies
          break;
        }
          
      }
      
      
      // next force
      ++fIndex;
      if (fIndex > sem_data->maxForces) fNum = -1;
      else fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
    }
  }
  
  // check singular forces
  fIndex = 0;
  fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  while (fNum >= 0) {
    F_X2 = 0; F_Y2 = 0; F_Z2 = 0;
    switch (sem_data->elementForceFlagTable[BC_SEM_FINDEX]) {
      case FORCE_TYPE_CELL_CENTER: {
        // cell center
        int cidx = modelNum*sem_data->idx_cPitch+sem_data->cellNumber[idx];
        F_X2 = sem_data->cellCenterX[cidx];
        F_Y2 = sem_data->cellCenterY[cidx];
        F_Z2 = sem_data->cellCenterZ[cidx];
        break;
      }
        
      case FORCE_TYPE_ELEMENT_CENTER: {
        // element center
        int oeType = sem_data->elementTypeTable[BC_SEM_FINDEX];
        int cidx = ((modelNum*sem_data->maxElementTypes)+oeType)*sem_data->idx_cPitch+sem_data->cellNumber[idx];
        F_X2 = sem_data->elementCenterX[cidx];
        F_Y2 = sem_data->elementCenterY[cidx];
        F_Z2 = sem_data->elementCenterZ[cidx];
        break;
      }
        
    }
    V = 0;
    switch (sem_data->elementForceTypeTable[BC_SEM_FINDEX]) {
      case 0:
        // D_MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = D_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
        break;
        
      case 1:
        // PD_MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = PD_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
        break;
        
      case 2:
        // MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = MORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
        break;
        
      case 3:
        // PMORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = PMORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
        break;
    }
    
    forceX += V * (F_X1 - F_X2);
    forceY += V * (F_Y1 - F_Y2);
    forceZ += V * (F_Z1 - F_Z2);
    
    // next force
    ++fIndex;
    if (fIndex > sem_data->maxForces) fNum = -1;
    else fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  }
  
  // result
  X_out[idx] = mh * forceX;
  Y_out[idx] = mh * forceY;
  Z_out[idx] = mh * forceZ;
}

// F1 Kernel for 2nd order Runge-Kutta
__device__ void
BC_SEM_kernel1_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (modelNum >= sem_data->numOfModels) return;
  
  // calculate F1
  sem_data->X_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Y_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Z_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  BC_SEM_kernel_function(data, modelNum, elemNum, sem_data->X_tmp, sem_data->Y_tmp, sem_data->Z_tmp,
                             sem_data->X_F1, sem_data->Y_F1, sem_data->Z_F1, sem_data->dt, 0.0f);
  //printf("final (%d, %d) = (%f, %f, %f)\n", elemNum, modelNum, sem_data->X_F1[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y_F1[modelNum*sem_data->idx_pitch+elemNum], sem_data->Z_F1[modelNum*sem_data->idx_pitch+elemNum]);
}

// F2 Kernel for 2nd order Runge-Kutta
__device__ void
BC_SEM_kernel2_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (modelNum >= sem_data->numOfModels) return;
  
  // calculate F2
  BC_SEM_kernel_function(data, modelNum, elemNum, sem_data->X_F1, sem_data->Y_F1, sem_data->Z_F1,
                             sem_data->X_F2, sem_data->Y_F2, sem_data->Z_F2, sem_data->dt, 0.5f);
  
  // calculate x(t + dt)
  sem_data->X[modelNum*sem_data->idx_pitch+elemNum] += sem_data->X_F2[modelNum*sem_data->idx_pitch+elemNum];
  sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] += sem_data->Y_F2[modelNum*sem_data->idx_pitch+elemNum];
  sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] += sem_data->Z_F2[modelNum*sem_data->idx_pitch+elemNum];
}

//
// Mesh SEM movement function used by Runge-Kutta solvers
//

__device__ void
BC_SEM_mesh_kernel_function(void *data, int modelNum, int elemNum, float *X_in, float *Y_in, float *Z_in, float *X_out, float *Y_out, float *Z_out, float mh, float mf)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int idx = modelNum*sem_data->idx_pitch+elemNum;
  int j, k, m, fIndex, fNum;
  float F_X1, F_X2;
  float F_Y1, F_Y2;
  float F_Z1, F_Z2;
  float r, V;
  float forceX = 0.0;
  float forceY = 0.0;
  float forceZ = 0.0;
  
  // element position
  F_X1 = sem_data->X[idx] + mf * X_in[idx];
  F_Y1 = sem_data->Y[idx] + mf * Y_in[idx];
  F_Z1 = sem_data->Z[idx] + mf * Z_in[idx];
  int eType = sem_data->elementType[idx];
  
  // mesh neighbors
  int meshTot = 0;
  if (sem_data->dims == 2) meshTot = MESH_XY_TOT;
  if (sem_data->dims == 3) meshTot = MESH_XYZ_TOT;

  // forces between mesh elements
  for (m = MESH_START; m < meshTot; ++m) {
    int *meshData = sem_data->meshElements[m];
    k = meshData[elemNum];
    
    //if (elemNum == 0) printf("mesh: %d %d\n", m, k);
    if (k == -1) continue;
    if (k == elemNum) continue;
    
    fIndex = 0;
    fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
    while (fNum >= 0) {
      switch (sem_data->elementForceFlagTable[BC_SEM_FINDEX]) {
        case FORCE_TYPE_INTRA: {
          // elements of same cell
          int oidx = modelNum*sem_data->idx_pitch+k;
          int oeType = sem_data->elementType[oidx];
          
          if (oeType == sem_data->elementTypeTable[BC_SEM_FINDEX]) {
            F_X2 = sem_data->X[oidx] + mf * X_in[oidx];
            F_Y2 = sem_data->Y[oidx] + mf * Y_in[oidx];
            F_Z2 = sem_data->Z[oidx] + mf * Z_in[oidx];
            
            V = 0;
            switch (sem_data->elementForceTypeTable[BC_SEM_FINDEX]) {
              case 0:
                // D_MORSE
                r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                V = D_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
                break;
                
              case 1:
                // PD_MORSE
                r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                V = PD_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
                break;
                
              case 2:
                // MORSE
                r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                V = MORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
                break;
                
              case 3:
                // PMORSE
                r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                V = PMORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
                break;
            }
            forceX += V * (F_X1 - F_X2);
            forceY += V * (F_Y1 - F_Y2);
            forceZ += V * (F_Z1 - F_Z2);
          }
          break;
        }
          
        case FORCE_TYPE_INTER: {
          // elements of different cells
          break;
        }
          
        case FORCE_TYPE_COLONY: {
          // elements of different colonies
          break;
        }
          
      }
      
      
      // next force
      ++fIndex;
      if (fIndex > sem_data->maxForces) fNum = -1;
      else fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
    }
  }
  
  // check singular forces
  fIndex = 0;
  fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  while (fNum >= 0) {
    F_X2 = 0; F_Y2 = 0; F_Z2 = 0;
    switch (sem_data->elementForceFlagTable[BC_SEM_FINDEX]) {
      case FORCE_TYPE_CELL_CENTER: {
        // cell center
        int cidx = modelNum*sem_data->idx_cPitch+sem_data->cellNumber[idx];
        F_X2 = sem_data->cellCenterX[cidx];
        F_Y2 = sem_data->cellCenterY[cidx];
        F_Z2 = sem_data->cellCenterZ[cidx];
        break;
      }
        
      case FORCE_TYPE_ELEMENT_CENTER: {
        // element center
        int oeType = sem_data->elementTypeTable[BC_SEM_FINDEX];
        int cidx = ((modelNum*sem_data->maxElementTypes)+oeType)*sem_data->idx_cPitch+sem_data->cellNumber[idx];
        F_X2 = sem_data->elementCenterX[cidx];
        F_Y2 = sem_data->elementCenterY[cidx];
        F_Z2 = sem_data->elementCenterZ[cidx];
        break;
      }
        
    }
    V = 0;
    switch (sem_data->elementForceTypeTable[BC_SEM_FINDEX]) {
      case 0:
        // D_MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = D_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
        break;
        
      case 1:
        // PD_MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = PD_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
        break;
        
      case 2:
        // MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = MORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
        break;
        
      case 3:
        // PMORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = PMORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
        break;
    }
    
    forceX += V * (F_X1 - F_X2);
    forceY += V * (F_Y1 - F_Y2);
    forceZ += V * (F_Z1 - F_Z2);
    
    // next force
    ++fIndex;
    if (fIndex > sem_data->maxForces) fNum = -1;
    else fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  }
  
  // result
  X_out[idx] = mh * forceX;
  Y_out[idx] = mh * forceY;
  Z_out[idx] = mh * forceZ;
}

// F1 Kernel for 2nd order Runge-Kutta
__device__ void
BC_SEM_mesh_kernel1_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (modelNum >= sem_data->numOfModels) return;
  
  // calculate F1
  sem_data->X_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Y_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Z_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  BC_SEM_mesh_kernel_function(data, modelNum, elemNum, sem_data->X_tmp, sem_data->Y_tmp, sem_data->Z_tmp,
                         sem_data->X_F1, sem_data->Y_F1, sem_data->Z_F1, sem_data->dt, 0.0f);
}

// F2 Kernel for 2nd order Runge-Kutta
__device__ void
BC_SEM_mesh_kernel2_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (modelNum >= sem_data->numOfModels) return;
  
  // calculate F2
  BC_SEM_mesh_kernel_function(data, modelNum, elemNum, sem_data->X_F1, sem_data->Y_F1, sem_data->Z_F1,
                         sem_data->X_F2, sem_data->Y_F2, sem_data->Z_F2, sem_data->dt, 0.5f);
  
  // calculate x(t + dt)
  sem_data->X[modelNum*sem_data->idx_pitch+elemNum] += sem_data->X_F2[modelNum*sem_data->idx_pitch+elemNum];
  sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] += sem_data->Y_F2[modelNum*sem_data->idx_pitch+elemNum];
  sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] += sem_data->Z_F2[modelNum*sem_data->idx_pitch+elemNum];
}


//
// Neighbor pairs algorithm
// SEM movement function used by Runge-Kutta solvers
//
// These kernels utilize an elementNeighborTable which
// contains all of the pairwise interactions. This has a
// high level of parallelism but a large memory requirement.
//

__device__ void
BC_SEM_neighbor_kernel_function(void *data, int modelNum, int elemNum, int pairIdx, float *X_in, float *Y_in, float *Z_in, float mh, float mf)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int idx = modelNum*sem_data->idx_pitch+elemNum;
  int j, k, fIndex, fNum;
  float F_X1, F_X2;
  float F_Y1, F_Y2;
  float F_Z1, F_Z2;
  float r, V;
  float forceX = 0.0;
  float forceY = 0.0;
  float forceZ = 0.0;

  // TODO: element is read only

  // element is in read only sector
  int sectorNum = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
  int roFlag = sem_data->readOnlySector[modelNum*sem_data->idx_sPitch + sectorNum];
  if (roFlag) return;

  // element position
  F_X1 = sem_data->X[idx] + mf * X_in[idx];
  F_Y1 = sem_data->Y[idx] + mf * Y_in[idx];
  F_Z1 = sem_data->Z[idx] + mf * Z_in[idx];
  int eType = sem_data->elementType[idx];

  // get pair element
  int pairNum = sem_data->elementNeighborTable[pairIdx*sem_data->idx_pitch+elemNum];
  if (pairNum == elemNum) return;
  if (pairNum == -1) return;

  fIndex = 0;
  fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  while (fNum >= 0) {
    switch (sem_data->elementForceFlagTable[BC_SEM_FINDEX]) {
      case FORCE_TYPE_INTRA: {
        // elements of same cell
        int oidx = modelNum*sem_data->idx_pitch+pairNum;
        int oeType = sem_data->elementType[oidx];
        
        if (oeType == sem_data->elementTypeTable[BC_SEM_FINDEX]) {
          F_X2 = sem_data->X[oidx] + mf * X_in[oidx];
          F_Y2 = sem_data->Y[oidx] + mf * Y_in[oidx];
          F_Z2 = sem_data->Z[oidx] + mf * Z_in[oidx];
          
          V = 0;
          switch (sem_data->elementForceTypeTable[BC_SEM_FINDEX]) {
            case 0:
              // D_MORSE
              r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
              V = D_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
              break;
              
            case 1:
              // PD_MORSE
              r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
              V = PD_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
              break;
              
            case 2:
              // MORSE
              r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
              V = MORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
              break;
              
            case 3:
              // PMORSE
              r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
              V = PMORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
              break;
          }
          forceX += V * (F_X1 - F_X2);
          forceY += V * (F_Y1 - F_Y2);
          forceZ += V * (F_Z1 - F_Z2);
        }
        break;
      }
        
      case FORCE_TYPE_INTER: {
        // elements of different cells
        break;
      }
        
      case FORCE_TYPE_COLONY: {
        // elements of different colonies
        break;
      }
        
    }
    
    
    // next force
    ++fIndex;
    if (fIndex > sem_data->maxForces) fNum = -1;
    else fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  }

  // check singular forces
  fIndex = 0;
  fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  while (fNum >= 0) {
    F_X2 = 0; F_Y2 = 0; F_Z2 = 0;
    switch (sem_data->elementForceFlagTable[BC_SEM_FINDEX]) {
      case FORCE_TYPE_CELL_CENTER: {
        // cell center
        int cidx = modelNum*sem_data->idx_cPitch+sem_data->cellNumber[idx];
        F_X2 = sem_data->cellCenterX[cidx];
        F_Y2 = sem_data->cellCenterY[cidx];
        F_Z2 = sem_data->cellCenterZ[cidx];
        break;
      }
        
      case FORCE_TYPE_ELEMENT_CENTER: {
        // element center
        int oeType = sem_data->elementTypeTable[BC_SEM_FINDEX];
        int cidx = ((modelNum*sem_data->maxElementTypes)+oeType)*sem_data->idx_cPitch+sem_data->cellNumber[idx];
        F_X2 = sem_data->elementCenterX[cidx];
        F_Y2 = sem_data->elementCenterY[cidx];
        F_Z2 = sem_data->elementCenterZ[cidx];
        break;
      }
        
    }
    V = 0;
    switch (sem_data->elementForceTypeTable[BC_SEM_FINDEX]) {
      case 0:
        // D_MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = D_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
        break;
        
      case 1:
        // PD_MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = PD_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
        break;
        
      case 2:
        // MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = MORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
        break;
        
      case 3:
        // PMORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = PMORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
        break;
    }
    
    forceX += V * (F_X1 - F_X2);
    forceY += V * (F_Y1 - F_Y2);
    forceZ += V * (F_Z1 - F_Z2);
    
    // next force
    ++fIndex;
    if (fIndex > sem_data->maxForces) fNum = -1;
    else fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  }
  
  // result
  //if (elemNum == 0) printf("element pair (%d, %d) idx %d, model %d force = (%f, %f, %f)\n", elemNum, pairNum, pairIdx, modelNum, forceX, forceY, forceZ);
  sem_data->elementNeighborForce_X[pairIdx*sem_data->idx_pitch+elemNum] = mh * forceX;
  sem_data->elementNeighborForce_Y[pairIdx*sem_data->idx_pitch+elemNum] = mh * forceY;
  sem_data->elementNeighborForce_Z[pairIdx*sem_data->idx_pitch+elemNum] = mh * forceZ;
}

__device__ void
BC_SEM_neighbor_sum_function(void *data, float *X_out, float *Y_out, float *Z_out)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (modelNum >= sem_data->numOfModels) return;
  
  X_out[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  Y_out[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  Z_out[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  
  // sum forces
  int startIdx = modelNum * sem_data->sectorNeighborSize * sem_data->maxElementsPerSector;
  int endIdx = startIdx + sem_data->sectorNeighborSize * sem_data->maxElementsPerSector;
  int i;
  for (i = startIdx; i < endIdx; ++i) {
    X_out[modelNum*sem_data->idx_pitch+elemNum] += sem_data->elementNeighborForce_X[i*sem_data->idx_pitch+elemNum];
    Y_out[modelNum*sem_data->idx_pitch+elemNum] += sem_data->elementNeighborForce_Y[i*sem_data->idx_pitch+elemNum];
    Z_out[modelNum*sem_data->idx_pitch+elemNum] += sem_data->elementNeighborForce_Z[i*sem_data->idx_pitch+elemNum];
  }
  //printf("(%d, %d, %d, %d) = (%f, %f, %f)\n", elemNum, modelNum, startIdx, endIdx, X_out[modelNum*sem_data->idx_pitch+elemNum], Y_out[modelNum*sem_data->idx_pitch+elemNum], Z_out[modelNum*sem_data->idx_pitch+elemNum]);
}


// F1 Kernel for 2nd order Runge-Kutta
__device__ void
BC_SEM_neighbor_kernel1_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int pairIdx = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (pairIdx >= (sem_data->numOfModels * sem_data->sectorNeighborSize * sem_data->maxElementsPerSector)) return;

  // determine model number
  int modelNum = pairIdx / (sem_data->sectorNeighborSize * sem_data->maxElementsPerSector);
  //printf("(%d, %d): %d\n", elemNum, pairIdx, modelNum);
  
  // calculate F1
  sem_data->elementNeighborForce_X[pairIdx*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->elementNeighborForce_Y[pairIdx*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->elementNeighborForce_Z[pairIdx*sem_data->idx_pitch+elemNum] = 0.0;
  BC_SEM_neighbor_kernel_function(data, modelNum, elemNum, pairIdx, sem_data->X_tmp, sem_data->Y_tmp, sem_data->Z_tmp, sem_data->dt, 0.0f);
}

__device__ void
BC_SEM_neighbor_sum_kernel1_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  
  // sum forces for F1
  BC_SEM_neighbor_sum_function(data, sem_data->X_F1, sem_data->Y_F1, sem_data->Z_F1);
}

// F2 Kernel for 2nd order Runge-Kutta
__device__ void
BC_SEM_neighbor_kernel2_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int pairIdx = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (pairIdx >= (sem_data->numOfModels * sem_data->sectorNeighborSize * sem_data->maxElementsPerSector)) return;

  // determine model number
  int modelNum = pairIdx / (sem_data->sectorNeighborSize * sem_data->maxElementsPerSector);

  // calculate F2
  sem_data->elementNeighborForce_X[pairIdx*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->elementNeighborForce_Y[pairIdx*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->elementNeighborForce_Z[pairIdx*sem_data->idx_pitch+elemNum] = 0.0;
  BC_SEM_neighbor_kernel_function(data, modelNum, elemNum, pairIdx, sem_data->X_F1, sem_data->Y_F1, sem_data->Z_F1, sem_data->dt, 0.5f);
}

__device__ void
BC_SEM_neighbor_update_kernel_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;

  if (elemNum >= sem_data->totalElements) return;
  if (modelNum >= sem_data->numOfModels) return;

  // sum forces for F2
  BC_SEM_neighbor_sum_function(data, sem_data->X_F2, sem_data->Y_F2, sem_data->Z_F2);
  
  // calculate x(t + dt)
  sem_data->X[modelNum*sem_data->idx_pitch+elemNum] += sem_data->X_F2[modelNum*sem_data->idx_pitch+elemNum];
  sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] += sem_data->Y_F2[modelNum*sem_data->idx_pitch+elemNum];
  sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] += sem_data->Z_F2[modelNum*sem_data->idx_pitch+elemNum];

  // calculate sector coordinates
  int sectorNum = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
  int xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
  int yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
  int zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
  int sx = sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + sectorNum];
  int sy = sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + sectorNum];
  int sz = sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + sectorNum];

  // check if same or new sector
  int direction = sector_determine_direction(sx, sy, sz, xc, yc, zc);
  if (direction == -1) printf("GPU_ERROR: sector_determine_direction(%d, %d, %d, %f, %f, %f) failed, sector = %d (%d, %d, %d), element = %d.\n",
			      xc, yc, zc, sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
			      sem_data->Z[modelNum*sem_data->idx_pitch+elemNum], sectorNum, sx, sy, sz, elemNum);
  sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum] = direction;
  //if (direction) printf("element (%d, %d), sector %d (%d, %d, %d) moved (%d) to new coordinates (%d, %d, %d)\n", elemNum, modelNum, sectorNum, sx, sy, sz, direction, xc, yc, zc);
}

//
// Neighbor pairwise algorithm
// SEM movement function used by Runge-Kutta solvers
//
// These kernels do not utilize an elementNeighborTable
// but instead walk through the sectorNeighborTable to find
// the pairwise elements. This has less parallelism but also
// uses much less memory.
//

__device__ void
BC_SEM_pairwise_kernel_function(void *data, int modelNum, int elemNum, int pairNum, float *X_in, float *Y_in, float *Z_in, float *X_out, float *Y_out, float *Z_out, float mh, float mf)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int idx = modelNum*sem_data->idx_pitch+elemNum;
  int j, k, fIndex, fNum;
  float F_X1, F_X2;
  float F_Y1, F_Y2;
  float F_Z1, F_Z2;
  float r, V;
  float forceX = 0.0;
  float forceY = 0.0;
  float forceZ = 0.0;
  
  // element is in read only sector
  int sectorNum = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
  int roFlag = sem_data->readOnlySector[modelNum*sem_data->idx_sPitch + sectorNum];
  if (roFlag) return;
  
  // element position
  F_X1 = sem_data->X[idx] + mf * X_in[idx];
  F_Y1 = sem_data->Y[idx] + mf * Y_in[idx];
  F_Z1 = sem_data->Z[idx] + mf * Z_in[idx];
  int eType = sem_data->elementType[idx];
  
  // calculate for interaction
  fIndex = 0;
  fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  while (fNum >= 0) {
    switch (sem_data->elementForceFlagTable[BC_SEM_FINDEX]) {
      case FORCE_TYPE_INTRA: {
        // elements of same cell
        int oidx = modelNum*sem_data->idx_pitch+pairNum;
        int oeType = sem_data->elementType[oidx];
        
        if (oeType == sem_data->elementTypeTable[BC_SEM_FINDEX]) {
          F_X2 = sem_data->X[oidx] + mf * X_in[oidx];
          F_Y2 = sem_data->Y[oidx] + mf * Y_in[oidx];
          F_Z2 = sem_data->Z[oidx] + mf * Z_in[oidx];
          
          V = 0;
          switch (sem_data->elementForceTypeTable[BC_SEM_FINDEX]) {
            case 0:
              // D_MORSE
              r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
              V = D_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
              break;
              
            case 1:
              // PD_MORSE
              r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
              V = PD_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
              break;
              
            case 2:
              // MORSE
              r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
              V = MORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
              break;
              
            case 3:
              // PMORSE
              r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
              V = PMORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
              break;
          }
          forceX += V * (F_X1 - F_X2);
          forceY += V * (F_Y1 - F_Y2);
          forceZ += V * (F_Z1 - F_Z2);
        }
        break;
      }
        
      case FORCE_TYPE_INTER: {
        // elements of different cells
        break;
      }
        
      case FORCE_TYPE_COLONY: {
        // elements of different colonies
        break;
      }
        
    }
    
    
    // next force
    ++fIndex;
    if (fIndex > sem_data->maxForces) fNum = -1;
    else fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  }
  
  // check singular forces
  fIndex = 0;
  fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  while (fNum >= 0) {
    F_X2 = 0; F_Y2 = 0; F_Z2 = 0;
    switch (sem_data->elementForceFlagTable[BC_SEM_FINDEX]) {
      case FORCE_TYPE_CELL_CENTER: {
        // cell center
        int cidx = modelNum*sem_data->idx_cPitch+sem_data->cellNumber[idx];
        F_X2 = sem_data->cellCenterX[cidx];
        F_Y2 = sem_data->cellCenterY[cidx];
        F_Z2 = sem_data->cellCenterZ[cidx];
        break;
      }
        
      case FORCE_TYPE_ELEMENT_CENTER: {
        // element center
        int oeType = sem_data->elementTypeTable[BC_SEM_FINDEX];
        int cidx = ((modelNum*sem_data->maxElementTypes)+oeType)*sem_data->idx_cPitch+sem_data->cellNumber[idx];
        F_X2 = sem_data->elementCenterX[cidx];
        F_Y2 = sem_data->elementCenterY[cidx];
        F_Z2 = sem_data->elementCenterZ[cidx];
        break;
      }
        
    }
    V = 0;
    switch (sem_data->elementForceTypeTable[BC_SEM_FINDEX]) {
      case 0:
        // D_MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = D_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
        break;
        
      case 1:
        // PD_MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = PD_MORSE(r, BC_SEM_PARAM_RHO, BC_SEM_PARAM_EQUIL_SQ, BC_SEM_PARAM_FACTOR);
        break;
        
      case 2:
        // MORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = MORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
        break;
        
      case 3:
        // PMORSE
        r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
        V = PMORSE(r, BC_SEM_PARAM_U0, BC_SEM_PARAM_ETA0, BC_SEM_PARAM_U1, BC_SEM_PARAM_ETA1);
        break;
    }
    
    forceX += V * (F_X1 - F_X2);
    forceY += V * (F_Y1 - F_Y2);
    forceZ += V * (F_Z1 - F_Z2);
    
    // next force
    ++fIndex;
    if (fIndex > sem_data->maxForces) fNum = -1;
    else fNum = sem_data->elementForceTable[BC_SEM_FINDEX];
  }
  
  // result
  //if (elemNum == 0) printf("element pair (%d, %d) idx %d, model %d force = (%f, %f, %f)\n", elemNum, pairNum, pairIdx, modelNum, forceX, forceY, forceZ);
  X_out[modelNum*sem_data->idx_pitch+elemNum] += mh * forceX;
  Y_out[modelNum*sem_data->idx_pitch+elemNum] += mh * forceY;
  Z_out[modelNum*sem_data->idx_pitch+elemNum] += mh * forceZ;
}

// F1 Kernel for 2nd order Runge-Kutta
__device__ void
BC_SEM_pairwise_kernel1_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (modelNum >= sem_data->numOfModels) return;
  
  // calculate F1
  sem_data->X_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->X_F1[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Y_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Y_F1[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Z_tmp[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Z_F1[modelNum*sem_data->idx_pitch+elemNum] = 0.0;

  // iterate through sector neighbors
  int direction, i;
  int sectorNum = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
  for (direction = S_CURRENT; direction < sem_data->sectorNeighborSize; ++direction) {
    int neighborSector = sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + direction)*sem_data->idx_sPitch + sectorNum];
    if (neighborSector == -1) continue;

    for (i = 0; i < sem_data->maxElementsPerSector; ++i) {
      int pairElem = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + neighborSector];
      if (pairElem == -1) continue;
      if (pairElem == elemNum) continue;
      BC_SEM_pairwise_kernel_function(data, modelNum, elemNum, pairElem, sem_data->X_tmp, sem_data->Y_tmp, sem_data->Z_tmp, sem_data->X_F1, sem_data->Y_F1, sem_data->Z_F1, sem_data->dt, 0.0f);
    }
  }
}

// F2 Kernel for 2nd order Runge-Kutta
__device__ void
BC_SEM_pairwise_kernel2_2rk(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (modelNum >= sem_data->numOfModels) return;
  
  // calculate F2
  sem_data->X_F2[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Y_F2[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  sem_data->Z_F2[modelNum*sem_data->idx_pitch+elemNum] = 0.0;
  
  // iterate through sector neighbors
  int direction, i;
  int sectorNum = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
  for (direction = S_CURRENT; direction < sem_data->sectorNeighborSize; ++direction) {
    int neighborSector = sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + direction)*sem_data->idx_sPitch + sectorNum];
    if (neighborSector == -1) continue;
    
    for (i = 0; i < sem_data->maxElementsPerSector; ++i) {
      int pairElem = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + neighborSector];
      if (pairElem == -1) continue;
      if (pairElem == elemNum) continue;
      BC_SEM_pairwise_kernel_function(data, modelNum, elemNum, pairElem, sem_data->X_F1, sem_data->Y_F1, sem_data->Z_F1, sem_data->X_F2, sem_data->Y_F2, sem_data->Z_F2, sem_data->dt, 0.5f);
    }
  }

  // calculate x(t + dt)
  sem_data->X[modelNum*sem_data->idx_pitch+elemNum] += sem_data->X_F2[modelNum*sem_data->idx_pitch+elemNum];
  sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] += sem_data->Y_F2[modelNum*sem_data->idx_pitch+elemNum];
  sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] += sem_data->Z_F2[modelNum*sem_data->idx_pitch+elemNum];
  
  // calculate sector coordinates
  int xc = floor(sem_data->X[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
  int yc = floor(sem_data->Y[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
  int zc = floor(sem_data->Z[modelNum*sem_data->idx_pitch+elemNum] / sem_data->sectorSize[modelNum]);
  int sx = sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + sectorNum];
  int sy = sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + sectorNum];
  int sz = sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + sectorNum];
  
  // check if same or new sector
  direction = sector_determine_direction(sx, sy, sz, xc, yc, zc);
  if (direction == -1) printf("GPU_ERROR: sector_determine_direction(%d, %d, %d, %f, %f, %f) failed, sector = %d (%d, %d, %d), element = %d, F2 = (%f, %f, %f).\n",
			      xc, yc, zc, sem_data->X[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y[modelNum*sem_data->idx_pitch+elemNum],
			      sem_data->Z[modelNum*sem_data->idx_pitch+elemNum], sectorNum, sx, sy, sz, elemNum,
			      sem_data->X_F2[modelNum*sem_data->idx_pitch+elemNum], sem_data->Y_F2[modelNum*sem_data->idx_pitch+elemNum],
			      sem_data->Z_F2[modelNum*sem_data->idx_pitch+elemNum]);
  sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum] = direction;
  //if (direction) printf("element (%d, %d), sector %d (%d, %d, %d) moved (%d) to new coordinates (%d, %d, %d)\n", elemNum, modelNum, sectorNum, sx, sy, sz, direction, xc, yc, zc);
}

//
// Sector kernels
// manage SEM movement
//

__device__ void
BC_SEM_sector_count_kernel(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int sectorNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  int i;
  
  if (modelNum >= sem_data->numOfModels) return;
  if (sectorNum >= sem_data->numSectors[modelNum]) return;
  //printf("sector (%d, %d), %d\n", sectorNum, modelNum, sem_data->numSectors[modelNum]);

  sem_data->newSectorCount[modelNum*sem_data->idx_sPitch+sectorNum] = 0;
  for (i = 0; i < sem_data->maxElementsPerSector; ++i) {
    int elemNum = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + sectorNum];
    if (elemNum == -1) continue;
    if (sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum]) {
      ++sem_data->newSectorCount[modelNum*sem_data->idx_sPitch+sectorNum];
      //printf("element (%d) moving out of sector (%d, %d)\n", elemNum, sectorNum, modelNum);
    }
  }
  //printf("sector (%d, %d): new = %d\n", sectorNum, modelNum, sem_data->newSectorCount[modelNum*sem_data->idx_sPitch+sectorNum]);
  
  for (i = 0; i < sem_data->sectorNeighborSize; ++i) {
    int ns = sem_data->sectorNeighborTable[(modelNum*sem_data->sectorNeighborSize + i)*sem_data->idx_sPitch+sectorNum];
    //if (ns >= 0) printf("sector (%d, %d) has neighbor (%d) in direction (%d).\n", sectorNum, modelNum, ns, i);
  }
}

__device__ void
BC_SEM_create_sectors_kernel(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int modelNum = blockIdx.x * blockDim.x + threadIdx.x;
  int sectorNum, i, j;
  
  if (modelNum >= sem_data->numOfModels) return;

  sem_data->rebuildSectorNeighborTable[modelNum] = 0;
  int newSectors = 0;
  for (sectorNum = 0; sectorNum < sem_data->numSectors[modelNum]; ++sectorNum) {
    if (sem_data->newSectorCount[modelNum*sem_data->idx_sPitch+sectorNum] > 0) {
      // elements moving out of this sector
      int sx = sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + sectorNum];
      int sy = sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + sectorNum];
      int sz = sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + sectorNum];

      for (i = 0; i < sem_data->maxElementsPerSector; ++i) {
        int elemNum = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + sectorNum];
        if (elemNum == -1) continue;

        int direction = sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum];
        if (direction) {
          int newSectorNum = sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + direction)*sem_data->idx_sPitch + sectorNum];
          if (newSectorNum == -1) {
            // coordinates for new sector
            int nsx, nsy, nsz;
            sector_coordinate_shift(sx, sy, sz, direction, &nsx, &nsy, &nsz);

            // check if already been created
            int found = 0;
            for (j = sem_data->numSectors[modelNum]; j < (sem_data->numSectors[modelNum] + newSectors); ++j) {
              if ((nsx == sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + j])
                  && (nsy == sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + j])
                  && (nsz == sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + j])) {
                found = 1;
                break;
              }
            }

            // new sector
            if (!found) {
              newSectorNum = sem_data->numSectors[modelNum] + newSectors;
              ++newSectors;
              if (newSectorNum >= sem_data->maxSectors) { printf("GPU_ERROR: Exceeded maxSectors (%d).\n", sem_data->maxSectors); return; }
            
              sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + newSectorNum] = nsx;
              sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + newSectorNum] = nsy;
              sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + newSectorNum] = nsz;

              // flag to indicate sector neighbor table needs to be rebuilt
              sem_data->rebuildSectorNeighborTable[modelNum] = 1;
            }
          }
        }
      }
    }
  }
  
  sem_data->numSectors[modelNum] += newSectors;
  //printf("Created %d new sectors: %d.\n", newSectors, sem_data->numSectors[modelNum]);
}

__device__ void
BC_SEM_build_sector_neighbors_kernel(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int sectorNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  int direction, i;
  
  if (modelNum >= sem_data->numOfModels) return;
  if (sectorNum >= sem_data->numSectors[modelNum]) return;
  if (!sem_data->rebuildSectorNeighborTable[modelNum]) return;
  //printf("build sector (%d, %d), %d\n", sectorNum, modelNum, sem_data->numSectors[modelNum]);

  sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + S_CURRENT)*sem_data->idx_sPitch + sectorNum] = sectorNum;
  for (direction = S_START; direction < sem_data->sectorNeighborSize; ++direction) {
    if (sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + direction)*sem_data->idx_sPitch + sectorNum] >= 0) continue;

    int nx, ny, nz;
    sector_coordinate_shift(sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + sectorNum],
                            sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + sectorNum],
                            sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + sectorNum],
                            direction, &nx, &ny, &nz);
    
    for (i = 0; i < sem_data->numSectors[modelNum]; ++i) {
      if ((nx == sem_data->sectorCoordinates[(modelNum*3 + 0)*sem_data->idx_sPitch + i])
          && (ny == sem_data->sectorCoordinates[(modelNum*3 + 1)*sem_data->idx_sPitch + i])
          && (nz == sem_data->sectorCoordinates[(modelNum*3 + 2)*sem_data->idx_sPitch + i])) {
        sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + direction)*sem_data->idx_sPitch + sectorNum] = i;
        //printf("Add new neighbor (%d, %d) for sector (%d, %d)\n", i, direction, sectorNum, modelNum);
        break;
      }
    }
  }
}

__device__ void
BC_SEM_move_elements_kernel(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int sectorNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  int i, direction;
  
  if (modelNum >= sem_data->numOfModels) return;
  if (sectorNum >= sem_data->numSectors[modelNum]) return;

  for (i = 0; i < sem_data->maxElementsPerSector; ++i)
    sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + sectorNum] = -1;

  // copy elements that stay in sector
  int eIdx = 0;
  for (i = 0; i < sem_data->maxElementsPerSector; ++i) {
    int elemNum = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + sectorNum];
    if (elemNum == -1) continue;
    
    int direction = sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum];
    if (!direction) {
      sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + eIdx)*sem_data->idx_sPitch + sectorNum] = elemNum;
      ++eIdx;
    } else {
      //printf("remove element (%d) from sector (%d, %d) and spot (%d)\n", elemNum, sectorNum, modelNum, i);
    }
  }

  // go through our neighbors, and see if any elements move into our sector
  for (direction = S_START; direction < sem_data->sectorNeighborSize; ++direction) {
    int newSectorNum = sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + direction)*sem_data->idx_sPitch + sectorNum];
    if (newSectorNum == -1) continue;
    
    for (i = 0; i < sem_data->maxElementsPerSector; ++i) {
      int elemNum = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + newSectorNum];
      if (elemNum == -1) continue;

      int moveDirection = sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum];
      if (moveDirection) {
        int moveSectorNum = sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + moveDirection)*sem_data->idx_sPitch + newSectorNum];
        if (moveSectorNum == sectorNum) {
          //printf("GPU: element (%d) moving (%d) out of sector (%d) into sector (%d, %d)\n", elemNum, moveDirection, newSectorNum, sectorNum, modelNum);

          if (eIdx == sem_data->maxElementsPerSector) printf("GPU_ERROR: Could not find slot to put element, maxElementsPerSector is full (%d)\n", eIdx);

          // put element in table
          sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + eIdx)*sem_data->idx_sPitch + sectorNum] = elemNum;
          //printf("putting element (%d) in open spot (%d)\n", elemNum, eIdx);
          ++eIdx;
        }
      }
    }
  }
}

__device__ void
BC_SEM_update_element_sector_kernel(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int sectorNum = blockIdx.x * blockDim.x + threadIdx.x;
  int modelNum = blockIdx.y * blockDim.y + threadIdx.y;
  int i, direction;
  
  if (modelNum >= sem_data->numOfModels) return;
  if (sectorNum >= sem_data->numSectors[modelNum]) return;
  
  // update sector table and assign sector to elements
  for (i = 0; i < sem_data->maxElementsPerSector; ++i) {
    int elemNum = sem_data->sectorElementUpdate[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + sectorNum];
    sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + i)*sem_data->idx_sPitch + sectorNum] = elemNum;
    if (elemNum != -1) {
      sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum] = sectorNum;
      sem_data->newSectorFlag[modelNum*sem_data->idx_pitch+elemNum] = S_CURRENT;
    }
  }
}

__device__ void
BC_SEM_update_element_neighbor_kernel(void *data, float currentTime, float finalTime)
{
  BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)data;
  int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
  int pairIdx = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (elemNum >= sem_data->totalElements) return;
  if (pairIdx >= (sem_data->numOfModels * sem_data->sectorNeighborSize * sem_data->maxElementsPerSector)) return;
  
  //if (elemNum != 0) return;
  
  // determine model number
  int modelNum = pairIdx / (sem_data->sectorNeighborSize * sem_data->maxElementsPerSector);

  // determine sector neighbor to copy element
  int sectorNum = sem_data->elementSector[modelNum*sem_data->idx_pitch+elemNum];
  int offset = pairIdx - modelNum * sem_data->sectorNeighborSize * sem_data->maxElementsPerSector;

  int direction = offset / sem_data->maxElementsPerSector;
  int eIdx = offset - direction * sem_data->maxElementsPerSector;
  int newSectorNum = sem_data->sectorNeighborTable[(modelNum * sem_data->sectorNeighborSize + direction)*sem_data->idx_sPitch + sectorNum];
  //if ((elemNum == 0) && (eIdx == 0)) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d)\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx);
  //if ((elemNum == 0) && (eIdx == 1)) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d)\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx);
  //if ((elemNum == 9) && (eIdx == 0)) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d): %d\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx, copyElemNum);
  //if ((elemNum == 9) && (eIdx == 1)) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d): %d\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx, copyElemNum);
  //printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d): -1\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx);

  if (newSectorNum == -1) {
    sem_data->elementNeighborTable[pairIdx*sem_data->idx_pitch+elemNum] = -1;
    //if (elemNum == 0) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d): -1\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx);
  } else {
    int copyElemNum = sem_data->sectorElementTable[(modelNum * sem_data->maxElementsPerSector + eIdx)*sem_data->idx_sPitch + newSectorNum];
    sem_data->elementNeighborTable[pairIdx*sem_data->idx_pitch+elemNum] = copyElemNum;
    //if ((elemNum == 0) && (eIdx == 0)) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d): %d\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx, copyElemNum);
    //if ((elemNum == 0) && (eIdx == 1)) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d): %d\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx, copyElemNum);
    //if ((elemNum == 9) && (eIdx == 0)) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d): %d\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx, copyElemNum);
    //if ((elemNum == 9) && (eIdx == 1)) printf("copy (%d, %d) in sector (%d, %d) from (%d, %d, %d, %d): %d\n", elemNum, pairIdx, sectorNum, modelNum, offset, direction, newSectorNum, eIdx, copyElemNum);
  }
}

//
// GPU-CPU interface functions
//

// Allocation
void *BC_SEM_allocGPUKernel(void *model, int dims, int numOfModels, int maxCells, int maxElements, int maxElementTypes, int maxForces, int maxSectors, int maxElementsPerSector, int numParams, int calcCellCenters, int calcElementCenters, int numOfSpecies, int useMesh, float dt)
{
  BC_SEM_GPUgrids *grids = (BC_SEM_GPUgrids *)malloc(sizeof(BC_SEM_GPUgrids));
  int i;

  // Save parameters
  grids->dims = dims;
  grids->numOfModels = numOfModels;
  grids->maxCells = maxCells;
  grids->maxElements = maxElements;
  grids->maxSectors = maxSectors;
  grids->maxElementsPerSector = maxElementsPerSector;
  if (dims == 2) grids->sectorNeighborSize = S_XY_TOT;
  else grids->sectorNeighborSize = S_XYZ_TOT;
  grids->maxElementTypes = maxElementTypes;
  grids->maxForces = maxForces;
  grids->numOfParams = numParams;
  grids->useMesh = useMesh;
  grids->calcCellCenters = calcCellCenters;
  grids->calcElementCenters = calcElementCenters;
  grids->numOfSpecies = numOfSpecies;
  grids->dt = dt;
  
  // Allocate device memory
  
  // cells and elements
  cudaMallocPitch((void**)&(grids->cellNumber), &(grids->pitch), maxElements * sizeof(int), numOfModels);
  cudacheck("allocate cellNumber");
  cudaMallocPitch((void**)&(grids->elementType), &(grids->pitch), maxElements * sizeof(int), numOfModels);
  cudacheck("allocate elementType");
  cudaMallocPitch((void**)&(grids->readOnly), &(grids->pitch), maxElements * sizeof(int), numOfModels);
  cudacheck("allocate readOnly");
  cudaMallocPitch((void**)&(grids->X), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element X");
  cudaMallocPitch((void**)&(grids->X_F1), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element X_F1");
  cudaMallocPitch((void**)&(grids->X_F2), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element X_F2");
  cudaMallocPitch((void**)&(grids->X_tmp), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element X_tmp");
  cudaMallocPitch((void**)&(grids->Y), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element Y");
  cudaMallocPitch((void**)&(grids->Y_F1), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element Y_F1");
  cudaMallocPitch((void**)&(grids->Y_F2), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element Y_F2");
  cudaMallocPitch((void**)&(grids->Y_tmp), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element Y_tmp");
  cudaMallocPitch((void**)&(grids->Z), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element Z");
  cudaMallocPitch((void**)&(grids->Z_F1), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element Z_F1");
  cudaMallocPitch((void**)&(grids->Z_F2), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element Z_F2");
  cudaMallocPitch((void**)&(grids->Z_tmp), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element Z_tmp");
  cudaMallocPitch((void**)&(grids->elementSector), &(grids->pitch), maxElements * sizeof(int), numOfModels);
  cudacheck("allocate element elementSector");
  cudaMallocPitch((void**)&(grids->newSectorFlag), &(grids->pitch), maxElements * sizeof(int), numOfModels);
  cudacheck("allocate element newSectorFlag");
  cudaMallocPitch((void**)&(grids->forceSum), &(grids->pitch), maxElements * sizeof(float), numOfModels);
  cudacheck("allocate element forceSum");
  
  if (numOfSpecies) {
    cudaMallocPitch((void**)&(grids->speciesInterface), &(grids->pitch), maxElements * sizeof(float), numOfModels * numOfSpecies);
    cudacheck("allocate element speciesInterface");
    cudaMallocPitch((void**)&(grids->speciesInterface_F1), &(grids->pitch), maxElements * sizeof(float), numOfModels * numOfSpecies);
    cudacheck("allocate element speciesInterface_F1");
    cudaMallocPitch((void**)&(grids->speciesInterface_F2), &(grids->pitch), maxElements * sizeof(float), numOfModels * numOfSpecies);
    cudacheck("allocate element speciesInterface_F2");
  }

#if 0
  // element neighbors
  cudaMallocPitch((void**)&(grids->elementNeighborTable), &(grids->pitch), maxElements * sizeof(int), numOfModels * grids->sectorNeighborSize * grids->maxElementsPerSector);
  cudacheck("allocate element elementNeighborTable");
  cudaMallocPitch((void**)&(grids->elementNeighborForce_X), &(grids->pitch), maxElements * sizeof(float), numOfModels * grids->sectorNeighborSize * grids->maxElementsPerSector);
  cudacheck("allocate element elementNeighborForce_X");
  cudaMallocPitch((void**)&(grids->elementNeighborForce_Y), &(grids->pitch), maxElements * sizeof(float), numOfModels * grids->sectorNeighborSize * grids->maxElementsPerSector);
  cudacheck("allocate element elementNeighborForce_Y");
  cudaMallocPitch((void**)&(grids->elementNeighborForce_Z), &(grids->pitch), maxElements * sizeof(float), numOfModels * grids->sectorNeighborSize * grids->maxElementsPerSector);
  cudacheck("allocate element elementNeighborForce_Z");
#endif
  
  // sectors
  cudaMalloc((void**)&(grids->numSectors), numOfModels * sizeof(int));
  cudacheck("allocate numSectors");
  cudaMalloc((void**)&(grids->sectorSize), numOfModels * sizeof(float));
  cudacheck("allocate sectorSize");
  cudaMallocPitch((void**)&(grids->sectorNeighborTable), &(grids->sPitch), maxSectors * sizeof(int), numOfModels * grids->sectorNeighborSize);
  cudacheck("allocate sectorNeighborTable");
  cudaMallocPitch((void**)&(grids->sectorCoordinates), &(grids->sPitch), maxSectors * sizeof(int), 3 * numOfModels);
  cudacheck("allocate sectorCoordinates");
  cudaMallocPitch((void**)&(grids->sectorElementTable), &(grids->sPitch), maxSectors * sizeof(int), numOfModels * grids->maxElementsPerSector);
  cudacheck("allocate sectorElementTable");
  cudaMallocPitch((void**)&(grids->sectorElementUpdate), &(grids->sPitch), maxSectors * sizeof(int), numOfModels * grids->maxElementsPerSector);
  cudacheck("allocate sectorElementUpdate");
  cudaMallocPitch((void**)&(grids->newSectorCount), &(grids->sPitch), maxSectors * sizeof(int), numOfModels);
  cudacheck("allocate newSectorCount");
  cudaMallocPitch((void**)&(grids->readOnlySector), &(grids->sPitch), maxSectors * sizeof(int), numOfModels);
  cudacheck("allocate readOnlySector");
  cudaMalloc((void**)&(grids->rebuildSectorNeighborTable), numOfModels * sizeof(int));
  cudacheck("allocate rebuildSectorNeighborTable");

  // mesh table
  for (i = MESH_CURRENT; i < MESH_XYZ_TOT; ++i) grids->meshElements[i] = NULL;
  if (useMesh) {
    if (dims == 2) {
      for (i = MESH_START; i < MESH_XY_TOT; ++i) {
        cudaMalloc((void**)&(grids->meshElements[i]), maxElements * sizeof(int));
        cudacheck("allocate 2D meshElements");
      }
    } else {
      for (i = MESH_START; i < MESH_XYZ_TOT; ++i) {
        cudaMalloc((void**)&(grids->meshElements[i]), maxElements * sizeof(int));
        cudacheck("allocate 3D meshElements");
      }
    }
  }

  // cells centers
  cudaMallocPitch((void**)&(grids->cellCenterX), &(grids->cPitch), maxCells * sizeof(float), numOfModels);
  cudacheck("allocate cellCenterX");
  cudaMallocPitch((void**)&(grids->cellCenterY), &(grids->cPitch), maxCells * sizeof(float), numOfModels);
  cudacheck("allocate cellCenterY");
  cudaMallocPitch((void**)&(grids->cellCenterZ), &(grids->cPitch), maxCells * sizeof(float), numOfModels);
  cudacheck("allocate cellCenterZ");
  
  // element centers
  cudaMallocPitch((void**)&(grids->elementCenterX), &(grids->cPitch), maxCells * sizeof(float), maxElementTypes * numOfModels);
  cudacheck("allocate elementCenterX");
  cudaMallocPitch((void**)&(grids->elementCenterY), &(grids->cPitch), maxCells * sizeof(float), maxElementTypes * numOfModels);
  cudacheck("allocate elementCenterY");
  cudaMallocPitch((void**)&(grids->elementCenterZ), &(grids->cPitch), maxCells * sizeof(float), maxElementTypes * numOfModels);
  cudacheck("allocate elementCenterZ");
  cudaMallocPitch((void**)&(grids->elementCenterCount), &(grids->cPitch), maxCells * sizeof(float), maxElementTypes * numOfModels);
  cudacheck("allocate elementCenterCount");
  
  // species
  if (numOfSpecies) {
    cudaMallocPitch((void**)&(grids->speciesData), &(grids->pPitch), numOfModels * sizeof(float), numOfSpecies);
    cudacheck("allocate speciesData");
  }
  else grids->speciesData = NULL;
  
  // parameters
  cudaMallocPitch((void**)&(grids->parameters), &(grids->pPitch), numOfModels * sizeof(float), numParams);
  cudacheck("allocate parameters");
  
  // force tables
  cudaMallocPitch((void**)&(grids->elementForceTable), &(grids->tPitch), maxElementTypes * sizeof(int), maxForces);
  cudacheck("allocate elementForceTable");
  cudaMallocPitch((void**)&(grids->elementForceTypeTable), &(grids->tPitch), maxElementTypes * sizeof(int), maxForces);
  cudacheck("allocate elementForceTypeTable");
  cudaMallocPitch((void**)&(grids->elementTypeTable), &(grids->tPitch), maxElementTypes * sizeof(int), maxForces);
  cudacheck("allocate elementTypeTable");
  cudaMallocPitch((void**)&(grids->elementForceFlagTable), &(grids->tPitch), maxElementTypes * sizeof(int), maxForces);
  cudacheck("allocate elementForceFlagTable");
  
  // index pitch
  grids->idx_pitch = grids->pitch / sizeof(float);
  grids->idx_cPitch = grids->cPitch / sizeof(float);
  grids->idx_pPitch = grids->pPitch / sizeof(float);
  grids->idx_tPitch = grids->tPitch / sizeof(int);
  grids->idx_sPitch = grids->sPitch / sizeof(int);
  
  return grids;
}

// Data Transfer
void BC_SEM_transferGPUKernel(void *model, void *g, int aFlag, float *hostParameters, int *cellNumber, int numOfCells, int totalElements,
                              void *hostX, void *hostY, void *hostZ, void *hostType, void *hostSector, void *hostElementNeighborTable,
                              void *EFT, void *EFTT, void *ETT, void *EFFT, void *hostData, void *meshNeighbors,
                              void *hostSectorCoordinates, void *hostNumSectors, void *hostSectorSize, void *hostSectorElementTable, void *hostSectorNeighborTable, void *hostReadOnlySector)
{
  BC_SEM_GPUgrids *grids = (BC_SEM_GPUgrids *)g;
  
  grids->numOfCells = numOfCells;
  grids->totalElements = totalElements;

  if (aFlag) {
    // Copy host memory to device memory
    // element data
    cudaMemcpy2D(grids->elementType, grids->pitch, hostType, grids->maxElements * sizeof(int), grids->maxElements * sizeof(int), grids->numOfModels, cudaMemcpyHostToDevice);
    cudacheck("transfer elementType");
    cudaMemcpy2D(grids->X, grids->pitch, hostX, grids->maxElements * sizeof(float), grids->maxElements * sizeof(float), grids->numOfModels, cudaMemcpyHostToDevice);
    cudacheck("transfer element X");
    cudaMemcpy2D(grids->Y, grids->pitch, hostY, grids->maxElements * sizeof(float), grids->maxElements * sizeof(float), grids->numOfModels, cudaMemcpyHostToDevice);
    cudacheck("transfer element Y");
    cudaMemcpy2D(grids->cellNumber, grids->pitch, cellNumber, grids->maxElements * sizeof(float), grids->maxElements * sizeof(float), grids->numOfModels, cudaMemcpyHostToDevice);
    cudacheck("transfer cellNumber");
    cudaMemcpy2D(grids->Z, grids->pitch, hostZ, grids->maxElements * sizeof(float), grids->maxElements * sizeof(float), grids->numOfModels, cudaMemcpyHostToDevice);
    cudacheck("transfer element Z");
    cudaMemcpy2D(grids->elementSector, grids->pitch, hostSector, grids->maxElements * sizeof(int), grids->maxElements * sizeof(int), grids->numOfModels, cudaMemcpyHostToDevice);
    cudacheck("transfer elementSector");
#if 0
    cudaMemcpy2D(grids->elementNeighborTable, grids->pitch, hostElementNeighborTable, grids->maxElements * sizeof(int), grids->maxElements * sizeof(int), grids->numOfModels * grids->sectorNeighborSize * grids->maxElementsPerSector, cudaMemcpyHostToDevice);
    cudacheck("transfer elementNeighborTable");
#endif
    
    // force tables
    cudaMemcpy2D(grids->elementForceTable, grids->tPitch, EFT, grids->maxElementTypes * sizeof(int), grids->maxElementTypes * sizeof(int), grids->maxForces, cudaMemcpyHostToDevice);
    cudacheck("transfer elementForceTable");
    cudaMemcpy2D(grids->elementForceTypeTable, grids->tPitch, EFTT, grids->maxElementTypes * sizeof(int), grids->maxElementTypes * sizeof(int), grids->maxForces, cudaMemcpyHostToDevice);
    cudacheck("transfer elementForceTypeTable");
    cudaMemcpy2D(grids->elementTypeTable, grids->tPitch, ETT, grids->maxElementTypes * sizeof(int), grids->maxElementTypes * sizeof(int), grids->maxForces, cudaMemcpyHostToDevice);
    cudacheck("transfer elementTypeTable");
    cudaMemcpy2D(grids->elementForceFlagTable, grids->tPitch, EFFT, grids->maxElementTypes * sizeof(int), grids->maxElementTypes * sizeof(int), grids->maxForces, cudaMemcpyHostToDevice);
    cudacheck("transfer elementForceFlagTable");

    // sectors
    cudaMemcpy(grids->numSectors, hostNumSectors, grids->numOfModels * sizeof(int), cudaMemcpyHostToDevice);
    cudacheck("transfer numSectors");
    cudaMemcpy(grids->sectorSize, hostSectorSize, grids->numOfModels * sizeof(int), cudaMemcpyHostToDevice);
    cudacheck("transfer sectorSize");
    cudaMemcpy2D(grids->sectorCoordinates, grids->sPitch, hostSectorCoordinates, grids->maxSectors * sizeof(int), grids->maxSectors * sizeof(int), grids->numOfModels * 3, cudaMemcpyHostToDevice);
    cudacheck("transfer sectorCoordinates");
    cudaMemcpy2D(grids->sectorElementTable, grids->sPitch, hostSectorElementTable, grids->maxSectors * sizeof(int), grids->maxSectors * sizeof(int), grids->numOfModels * grids->maxElementsPerSector, cudaMemcpyHostToDevice);
    cudacheck("transfer sectorElementTable");
    cudaMemcpy2D(grids->sectorNeighborTable, grids->sPitch, hostSectorNeighborTable, grids->maxSectors * sizeof(int), grids->maxSectors * sizeof(int), grids->numOfModels * grids->sectorNeighborSize, cudaMemcpyHostToDevice);
    cudacheck("transfer sectorNeighborTable");
    cudaMemcpy2D(grids->readOnlySector, grids->sPitch, hostReadOnlySector, grids->maxSectors * sizeof(int), grids->maxSectors * sizeof(int), grids->numOfModels, cudaMemcpyHostToDevice);
    cudacheck("transfer readOnlySector");

    // Copy species data
    if (grids->numOfSpecies) cudaMemcpy2D(grids->speciesData, grids->pPitch, hostData, grids->numOfModels * sizeof(float), grids->numOfModels * sizeof(float), grids->numOfSpecies, cudaMemcpyHostToDevice);
    cudacheck("transfer speciesData");
    
    // Mesh neighbors
    if (grids->useMesh) {
      int i;
      int **meshData = (int **)meshNeighbors;
      if (grids->dims == 2) {
        for (i = MESH_START; i < MESH_XY_TOT; ++i) {
          cudaMemcpy(grids->meshElements[i], meshData[i], grids->maxElements * sizeof(int), cudaMemcpyHostToDevice);
          cudacheck("transfer meshElements");
        }
      } else {
        for (i = MESH_START; i < MESH_XYZ_TOT; ++i) {
          cudaMemcpy(grids->meshElements[i], meshData[i], grids->maxElements * sizeof(int), cudaMemcpyHostToDevice);
          cudacheck("transfer meshElements");
          //printf("meshData[%d] = %p\n", i, meshData[i]);
        }
      }
    }
    
    // Copy parameters
    cudaMemcpy2D(grids->parameters, grids->pPitch, hostParameters, grids->numOfModels * sizeof(float), grids->numOfModels * sizeof(float), grids->numOfParams, cudaMemcpyHostToDevice);
    cudacheck("transfer parameters");
  } else {
    // Copy result to host memory
    cudaMemcpy2D(hostX, grids->maxElements * sizeof(float), grids->X, grids->pitch, grids->maxElements * sizeof(float), grids->numOfModels, cudaMemcpyDeviceToHost);
    cudacheck("transfer hostX");
    cudaMemcpy2D(hostY, grids->maxElements * sizeof(float), grids->Y, grids->pitch, grids->maxElements * sizeof(float), grids->numOfModels, cudaMemcpyDeviceToHost);
    cudacheck("transfer hostY");
    cudaMemcpy2D(hostZ, grids->maxElements * sizeof(float), grids->Z, grids->pitch, grids->maxElements * sizeof(float), grids->numOfModels, cudaMemcpyDeviceToHost);
    cudacheck("transfer hostZ");
    cudaMemcpy2D(hostSector, grids->maxElements * sizeof(float), grids->elementSector, grids->pitch, grids->maxElements * sizeof(float), grids->numOfModels, cudaMemcpyDeviceToHost);
    cudacheck("transfer hostSector");
#if 0
    cudaMemcpy2D(hostElementNeighborTable, grids->maxElements * sizeof(float), grids->elementNeighborTable, grids->pitch, grids->maxElements * sizeof(float), grids->numOfModels * grids->sectorNeighborSize * grids->maxElementsPerSector, cudaMemcpyDeviceToHost);
    cudacheck("transfer hostElementNeighborTable");
#endif
    
    // sectors
    cudaMemcpy(hostNumSectors, grids->numSectors, grids->numOfModels * sizeof(int), cudaMemcpyDeviceToHost);
    cudacheck("transfer hostNumSectors");
    cudaMemcpy2D(hostSectorCoordinates, grids->maxSectors * sizeof(int), grids->sectorCoordinates, grids->sPitch, grids->maxSectors * sizeof(int), grids->numOfModels * 3, cudaMemcpyDeviceToHost);
    cudacheck("transfer hostSectorCoordinates");
    cudaMemcpy2D(hostSectorElementTable, grids->maxSectors * sizeof(int), grids->sectorElementTable, grids->sPitch, grids->maxSectors * sizeof(int), grids->numOfModels * grids->maxElementsPerSector, cudaMemcpyDeviceToHost);
    cudacheck("transfer hostSectorElementTable");
    cudaMemcpy2D(hostSectorNeighborTable, grids->maxSectors * sizeof(int), grids->sectorNeighborTable, grids->sPitch, grids->maxSectors * sizeof(int), grids->numOfModels * grids->sectorNeighborSize, cudaMemcpyDeviceToHost);
    cudacheck("transfer hostSectorNeighborTable");
  }
}

// Release
void BC_SEM_releaseGPUKernel(void *model, void *g)
{
  BC_SEM_GPUgrids *grids = (BC_SEM_GPUgrids *)g;
  cudaFree(grids->elementType);
  cudaFree(grids->cellNumber);
  cudaFree(grids->X);
  cudaFree(grids->X_F1);
  cudaFree(grids->X_F2);
  cudaFree(grids->X_tmp);
  cudaFree(grids->Y);
  cudaFree(grids->Y_F1);
  cudaFree(grids->Y_F2);
  cudaFree(grids->Y_tmp);
  cudaFree(grids->Z);
  cudaFree(grids->Z_F1);
  cudaFree(grids->Z_F2);
  cudaFree(grids->Z_tmp);
  cudaFree(grids->elementSector);
  cudaFree(grids->newSectorFlag);
  cudaFree(grids->forceSum);
#if 0
  cudaFree(grids->elementNeighborTable);
  cudaFree(grids->elementNeighborForce_X);
  cudaFree(grids->elementNeighborForce_Y);
  cudaFree(grids->elementNeighborForce_Z);
#endif
  
  cudaFree(grids->numSectors);
  cudaFree(grids->sectorSize);
  cudaFree(grids->sectorCoordinates);
  cudaFree(grids->sectorNeighborTable);
  cudaFree(grids->sectorElementTable);
  cudaFree(grids->sectorElementUpdate);
  cudaFree(grids->newSectorCount);
  cudaFree(grids->readOnlySector);
  cudaFree(grids->rebuildSectorNeighborTable);

  if (grids->speciesInterface) cudaFree(grids->speciesInterface);
  if (grids->speciesInterface_F1) cudaFree(grids->speciesInterface_F1);
  if (grids->speciesInterface_F2) cudaFree(grids->speciesInterface_F2);

  cudaFree(grids->cellCenterX);
  cudaFree(grids->cellCenterY);
  cudaFree(grids->cellCenterZ);
  cudaFree(grids->elementCenterX);
  cudaFree(grids->elementCenterY);
  cudaFree(grids->elementCenterZ);
  cudaFree(grids->elementCenterCount);
  if (grids->speciesData) cudaFree(grids->speciesData);
  cudaFree(grids->parameters);
  cudaFree(grids->elementForceTable);
  cudaFree(grids->elementForceTypeTable);
  cudaFree(grids->elementTypeTable);
  cudaFree(grids->elementForceFlagTable);
  free(grids);
  cudaThreadExit();
}


//
// Cell movement
//

// Calculate cell center
__global__ void
SEM_center_kernel(int *numOfElements, float *X, float *Y, float *Z, int *elementType,
                   float *cellCenterX, float *cellCenterY, float *cellCenterZ, size_t pitch,
                   int numOfCells, int maxCells, int maxElements, float dt)
{
  int cellNum = blockIdx.x * blockDim.x + threadIdx.x;
  int elemNum;
  
  if (cellNum >= numOfCells) return;
  
  //
  float cX = 0.0;
  float cY = 0.0;
  float cZ = 0.0;
  float minX, maxX;
  float minY, maxY;
  
  minX = X[cellNum];
  maxX = X[cellNum];
  minY = Y[cellNum];
  maxY = Y[cellNum];
  //speciesData[BMA_species*pitch+cellNum] = 0.0;
  for (elemNum = 0; elemNum < numOfElements[cellNum]; ++elemNum) {
    // translate to avoid boundary conditions
    //cX += X[elemNum*pitch+cellNum] + BOUNDARY_X;
    //cY += Y[elemNum*pitch+cellNum] + BOUNDARY_Y;
    //cZ += Z[elemNum*pitch+cellNum] + BOUNDARY_Z;
    cX += X[elemNum*pitch+cellNum];
    cY += Y[elemNum*pitch+cellNum];
    cZ += Z[elemNum*pitch+cellNum];
    if (X[elemNum*pitch+cellNum] < minX) minX = X[elemNum*pitch+cellNum];
    if (X[elemNum*pitch+cellNum] > maxX) maxX = X[elemNum*pitch+cellNum];
    if (Y[elemNum*pitch+cellNum] < minY) minY = Y[elemNum*pitch+cellNum];
    if (Y[elemNum*pitch+cellNum] > maxY) maxY = Y[elemNum*pitch+cellNum];    
  }
  
  //cX = cX / (float)numOfElements[cellNum] - BOUNDARY_X;
  //cY = cY / (float)numOfElements[cellNum] - BOUNDARY_Y;
  //cZ = cZ / (float)numOfElements[cellNum] - BOUNDARY_Z;
  cX = cX / (float)numOfElements[cellNum];
  cY = cY / (float)numOfElements[cellNum];
  cZ = cZ / (float)numOfElements[cellNum];
  
  // handle special case when cell is split across periodic boundary
  if ((maxX - minX) > (BOUNDARY_X / 2)) {
    cX = 0;
    for (elemNum = 0; elemNum < numOfElements[cellNum]; ++elemNum) {
      if (X[elemNum*pitch+cellNum] > (BOUNDARY_X / 2))
        cX += X[elemNum*pitch+cellNum] - BOUNDARY_X;
      else
        cX += X[elemNum*pitch+cellNum];
    }
    cX = cX / (float)numOfElements[cellNum];
    if (cX < 0) cX += BOUNDARY_X;
  }
  if ((maxY - minY) > (BOUNDARY_Y / 2)) {
    cY = 0;
    for (elemNum = 0; elemNum < numOfElements[cellNum]; ++elemNum) {
      if (Y[elemNum*pitch+cellNum] > (BOUNDARY_Y / 2))
        cY += Y[elemNum*pitch+cellNum] - BOUNDARY_Y;
      else
        cY += Y[elemNum*pitch+cellNum];
    }
    cY = cY / (float)numOfElements[cellNum];
    if (cY < 0) cY += BOUNDARY_Y;
  }
  
  cellCenterX[cellNum] = cX;
  cellCenterY[cellNum] = cY;
  cellCenterZ[cellNum] = cZ;
  
  //if (speciesData[BMA_species*pitch+cellNum] != 0.0) speciesData[BMA_species*pitch+cellNum] = 1.0 / speciesData[BMA_species*pitch+cellNum];
  
  //cuPrintf("(%d) %f %f %f, %f %f %f\n", cellNum, X[cellNum], Y[cellNum], Z[cellNum], cX, cY, cZ);
}

// Calculate cell center for 2D
__global__ void
SEM_center2d_kernel(int *numOfElements, float *X, float *Y, int *elementType,
                  float *cellCenterX, float *cellCenterY, size_t pitch,
                  int numOfCells, int maxCells, int maxElements, float dt)
{
  int cellNum = blockIdx.x * blockDim.x + threadIdx.x;
  int elemNum;
  
  if (cellNum >= numOfCells) return;
  
  //
  float cX = 0.0;
  float cY = 0.0;
  float minX, maxX;
  float minY, maxY;
  
  minX = X[cellNum];
  maxX = X[cellNum];
  minY = Y[cellNum];
  maxY = Y[cellNum];
  for (elemNum = 0; elemNum < numOfElements[cellNum]; ++elemNum) {
    cX += X[elemNum*pitch+cellNum];
    cY += Y[elemNum*pitch+cellNum];
    // record min/max for handling boundary conditions
    if (X[elemNum*pitch+cellNum] < minX) minX = X[elemNum*pitch+cellNum];
    if (X[elemNum*pitch+cellNum] > maxX) maxX = X[elemNum*pitch+cellNum];
    if (Y[elemNum*pitch+cellNum] < minY) minY = Y[elemNum*pitch+cellNum];
    if (Y[elemNum*pitch+cellNum] > maxY) maxY = Y[elemNum*pitch+cellNum];    
  }
  
  cX = cX / (float)numOfElements[cellNum];
  cY = cY / (float)numOfElements[cellNum];
  
  // handle special case when cell is split across periodic boundary
  if ((maxX - minX) > (BOUNDARY_X / 2)) {
    cX = 0;
    for (elemNum = 0; elemNum < numOfElements[cellNum]; ++elemNum) {
      if (X[elemNum*pitch+cellNum] > (BOUNDARY_X / 2))
        cX += X[elemNum*pitch+cellNum] - BOUNDARY_X;
      else
        cX += X[elemNum*pitch+cellNum];
    }
    cX = cX / (float)numOfElements[cellNum];
    if (cX < 0) cX += BOUNDARY_X;
  }
  if ((maxY - minY) > (BOUNDARY_Y / 2)) {
    cY = 0;
    for (elemNum = 0; elemNum < numOfElements[cellNum]; ++elemNum) {
      if (Y[elemNum*pitch+cellNum] > (BOUNDARY_Y / 2))
        cY += Y[elemNum*pitch+cellNum] - BOUNDARY_Y;
      else
        cY += Y[elemNum*pitch+cellNum];
    }
    cY = cY / (float)numOfElements[cellNum];
    if (cY < 0) cY += BOUNDARY_Y;
  }
  
  cellCenterX[cellNum] = cX;
  cellCenterY[cellNum] = cY;
  
  //if (speciesData[BMA_species*pitch+cellNum] != 0.0) speciesData[BMA_species*pitch+cellNum] = 1.0 / speciesData[BMA_species*pitch+cellNum];
  
  //cuPrintf("(%d) %f %f %f, %f %f %f\n", cellNum, X[cellNum], Y[cellNum], Z[cellNum], cX, cY, cZ);
}


#if 0

// F1 kernel for 2nd order Runge-Kutta
__global__ void
SEM_moveKernel_F1()
{
  int cellNum = blockIdx.x * blockDim.x + threadIdx.x;
  int elemNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (cellNum >= scem_0_sem_grids->numOfCells) return;
  if (elemNum >= scem_0_sem_grids->numOfElements[cellNum]) return;
  int idx = elemNum*scem_0_sem_grids->idx_pitch+cellNum;
  
  //printf("%d %d %d %f\n", cellNum, elemNum, elemNum*pitch+cellNum, X[elemNum*pitch+cellNum]);
  
  // generate Gaussian noise
  
  // intracellular
  float intraX = 0.0;
  float intraY = 0.0;
  float intraZ = 0.0;
  
  int j, k;
  float r, V;
  for (k = 0; k < scem_0_sem_grids->numOfElements[cellNum]; ++k) {
    if (k == elemNum) continue;
    int oidx = k*scem_0_sem_grids->idx_pitch+cellNum;

#if 0
    r = dist(X[idx], Y[idx], Z[idx],
             X[k*pitch+cellNum], Y[k*pitch+cellNum], Z[k*pitch+cellNum]);
    if (elementType[idx] == 2)
      V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    else
      V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
#else
    r = dist2(scem_0_sem_grids->X[idx], scem_0_sem_grids->Y[idx], scem_0_sem_grids->Z[idx],
              scem_0_sem_grids->X[oidx], scem_0_sem_grids->Y[oidx], scem_0_sem_grids->Z[oidx]);
    V = D_MORSE(r, RHO, INTRA_EQUIL_SQ, FACTOR);
#endif
    
    intraX += V * (scem_0_sem_grids->X[idx] - scem_0_sem_grids->X[oidx]);
    intraY += V * (scem_0_sem_grids->Y[idx] - scem_0_sem_grids->Y[oidx]);
    intraZ += V * (scem_0_sem_grids->Z[idx] - scem_0_sem_grids->Z[oidx]);
    
#if PERIODIC_BOUNDARY
    if (elementType[elemNum*pitch+cellNum] == 2)
      intercellular_mirror(elemNum, cellNum, pitch, k, cellNum, &intraX, &intraY, &intraZ, X, Y, Z);
    else
      intracellular_mirror(elemNum, cellNum, pitch, k, &intraX, &intraY, &intraZ, X, Y, Z);
    
#endif
  }
  
  // intercellular
  float interX = 0.0;
  float interY = 0.0;
  float interZ = 0.0;
  
  for (j = 0; j < scem_0_sem_grids->numOfCells; ++j) {
    if (j == cellNum) continue;
    
#if 0
    if (elementType[j] != 2) {
      r = dist(cX[cellNum], cY[cellNum], cZ[cellNum], cX[j], cY[j], cZ[j]);
      if (r > INTER_DIST) continue;
    }
#endif
    
    for (k = 0; k < scem_0_sem_grids->numOfElements[j]; ++k) {
      int oidx = k*scem_0_sem_grids->idx_pitch+j;

      //r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum], X[k*pitch+j], Y[k*pitch+j], Z[k*pitch+j]);
      r = dist2(scem_0_sem_grids->X[idx], scem_0_sem_grids->Y[idx], scem_0_sem_grids->Z[idx],
                scem_0_sem_grids->X[oidx], scem_0_sem_grids->Y[oidx], scem_0_sem_grids->Z[oidx]);
      //if (r <= INTER_DIST) {
#if REPULSE_ONLY
      //V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
      V = PD_MORSE(r, RHO, INTER_EQUIL_SQ, FACTOR);
#else
      //V = MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
      V = D_MORSE(r, RHO, INTER_EQUIL_SQ, FACTOR);
#endif
      
      interX += V * (scem_0_sem_grids->X[idx] - scem_0_sem_grids->X[oidx]);
      interY += V * (scem_0_sem_grids->Y[idx] - scem_0_sem_grids->Y[oidx]);
      interZ += V * (scem_0_sem_grids->Z[idx] - scem_0_sem_grids->Z[oidx]);
      //}
      
#if PERIODIC_BOUNDARY
      intercellular_mirror(elemNum, cellNum, pitch, k, j, &interX, &interY, &interZ, X, Y, Z);
#endif
    }
  }
  
#if 0
  // basement membrane
  if (elementType[elemNum*pitch+cellNum] == 1) {
    // adhesion
    r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum],
             X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], 0);
    V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
    interZ += V * (Z[elemNum*pitch+cellNum] - 0);
  } else {
    // purely repulsion
    r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum],
             X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], 0);
    V = P_MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
    interZ += V * (Z[elemNum*pitch+cellNum] - 0);
  }
#endif
  
  // update
  scem_0_sem_grids->X_F1[idx] = scem_0_sem_grids->X[idx] + 0.5 * scem_0_sem_grids->dt * (intraX + interX);
  scem_0_sem_grids->Y_F1[idx] = scem_0_sem_grids->Y[idx] + 0.5 * scem_0_sem_grids->dt * (intraY + interY);
  scem_0_sem_grids->Z_F1[idx] = scem_0_sem_grids->Z[idx] + 0.5 * scem_0_sem_grids->dt * (intraZ + interZ);
  
#if PERIODIC_BOUNDARY
  if (X_F1[elemNum*pitch+cellNum] < 0.0) X_F1[elemNum*pitch+cellNum] = X_F1[elemNum*pitch+cellNum] + BOUNDARY_X;
  if (X_F1[elemNum*pitch+cellNum] > BOUNDARY_X) X_F1[elemNum*pitch+cellNum] = X_F1[elemNum*pitch+cellNum] - BOUNDARY_X;
  if (Y_F1[elemNum*pitch+cellNum] < 0.0) Y_F1[elemNum*pitch+cellNum] = Y_F1[elemNum*pitch+cellNum] + BOUNDARY_Y;
  if (Y_F1[elemNum*pitch+cellNum] > BOUNDARY_Y) Y_F1[elemNum*pitch+cellNum] = Y_F1[elemNum*pitch+cellNum] - BOUNDARY_Y;
  //if (Z_F1[elemNum*pitch+cellNum] < 0.0) Z_F1[elemNum*pitch+cellNum] = Z_F1[elemNum*pitch+cellNum] + BOUNDARY_Z;
  //if (Z_F1[elemNum*pitch+cellNum] > BOUNDARY_Z) Z_F1[elemNum*pitch+cellNum] = Z_F1[elemNum*pitch+cellNum] - BOUNDARY_Z;
#endif
  
#if HARD_Z
  if (Z_F1[elemNum*pitch+cellNum] < 0.0) Z_F1[elemNum*pitch+cellNum] = 0.0;
  if (Z_F1[elemNum*pitch+cellNum] > BOUNDARY_Z) Z_F1[elemNum*pitch+cellNum] = BOUNDARY_Z;
#endif
}


// F2 kernel for 2nd order Runge-Kutta
__global__ void
SEM_moveKernel_F2()
{
  int cellNum = blockIdx.x * blockDim.x + threadIdx.x;
  int elemNum = blockIdx.y * blockDim.y + threadIdx.y;
  
  if (cellNum >= scem_0_sem_grids->numOfCells) return;
  if (elemNum >= scem_0_sem_grids->numOfElements[cellNum]) return;
  int idx = elemNum*scem_0_sem_grids->idx_pitch+cellNum;

  //printf("%d %d %d %f\n", cellNum, elemNum, elemNum*pitch+cellNum, X[elemNum*pitch+cellNum]);
  
  // generate Gaussian noise
  
  // intracellular
  float intraX = 0.0;
  float intraY = 0.0;
  float intraZ = 0.0;
  
  int j, k;
  float r, V;
  for (k = 0; k < scem_0_sem_grids->numOfElements[cellNum]; ++k) {
    if (k == elemNum) continue;
    int oidx = k*scem_0_sem_grids->idx_pitch+cellNum;

#if 0
    r = dist(X_F1[elemNum*pitch+cellNum], Y_F1[elemNum*pitch+cellNum], Z_F1[elemNum*pitch+cellNum],
             X_F1[k*pitch+cellNum], Y_F1[k*pitch+cellNum], Z_F1[k*pitch+cellNum]);
    if (elementType[elemNum*pitch+cellNum] == 2)
      V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
    else
      V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
#else
    r = dist2(scem_0_sem_grids->X_F1[idx], scem_0_sem_grids->Y_F1[idx], scem_0_sem_grids->Z_F1[idx],
              scem_0_sem_grids->X_F1[oidx], scem_0_sem_grids->Y_F1[oidx], scem_0_sem_grids->Z_F1[oidx]);
    V = D_MORSE(r, RHO, INTRA_EQUIL_SQ, FACTOR);
#endif

    intraX += V * (scem_0_sem_grids->X_F1[idx] - scem_0_sem_grids->X_F1[oidx]);
    intraY += V * (scem_0_sem_grids->Y_F1[idx] - scem_0_sem_grids->Y_F1[oidx]);
    intraZ += V * (scem_0_sem_grids->Z_F1[idx] - scem_0_sem_grids->Z_F1[oidx]);
    
#if PERIODIC_BOUNDARY
    if (elementType[elemNum*pitch+cellNum] == 2)
      intercellular_mirror(elemNum, cellNum, pitch, k, cellNum, &intraX, &intraY, &intraZ, X, Y, Z);
    else
      intracellular_mirror(elemNum, cellNum, pitch, k, &intraX, &intraY, &intraZ, X_F1, Y_F1, Z_F1);
#endif
  }
  
  // intercellular
  float interX = 0.0;
  float interY = 0.0;
  float interZ = 0.0;
  
  for (j = 0; j < scem_0_sem_grids->numOfCells; ++j) {
    if (j == cellNum) continue;
    
#if 0
    if (elementType[j] != 2) {
      r = dist(cX[cellNum], cY[cellNum], cZ[cellNum], cX[j], cY[j], cZ[j]);
      if (r > INTER_DIST) continue;
    }
#endif
    
    for (k = 0; k < scem_0_sem_grids->numOfElements[j]; ++k) {
      int oidx = k*scem_0_sem_grids->idx_pitch+j;

      //r = dist(X_F1[elemNum*pitch+cellNum], Y_F1[elemNum*pitch+cellNum], Z_F1[elemNum*pitch+cellNum], X_F1[k*pitch+j], Y_F1[k*pitch+j], Z_F1[k*pitch+j]);
      r = dist2(scem_0_sem_grids->X_F1[idx], scem_0_sem_grids->Y_F1[idx], scem_0_sem_grids->Z_F1[idx],
                scem_0_sem_grids->X_F1[oidx], scem_0_sem_grids->Y_F1[oidx], scem_0_sem_grids->Z_F1[oidx]);
      //if (r <= INTER_DIST) {
#if REPULSE_ONLY
        //V = P_MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
        V = PD_MORSE(r, RHO, INTER_EQUIL_SQ, FACTOR);
#else
        //V = MORSE(r, INTER_U0, INTER_ETA0, INTER_U1, INTER_ETA1);
        V = D_MORSE(r, RHO, INTER_EQUIL_SQ, FACTOR);
#endif
      
        interX += V * (scem_0_sem_grids->X_F1[idx] - scem_0_sem_grids->X_F1[oidx]);
        interY += V * (scem_0_sem_grids->Y_F1[idx] - scem_0_sem_grids->Y_F1[oidx]);
        interZ += V * (scem_0_sem_grids->Z_F1[idx] - scem_0_sem_grids->Z_F1[oidx]);
      //}
      
#if PERIODIC_BOUNDARY
      intercellular_mirror(elemNum, cellNum, pitch, k, j, &interX, &interY, &interZ, X_F1, Y_F1, Z_F1);
#endif
    }
  }

#if 0
  // basement membrane
  if (elementType[elemNum*pitch+cellNum] == 1) {
    // adhesion
    r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum],
             X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], 0);
    V = MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
    interZ += V * (Z[elemNum*pitch+cellNum] - 0);
  } else {
    // purely repulsion
    r = dist(X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], Z[elemNum*pitch+cellNum],
             X[elemNum*pitch+cellNum], Y[elemNum*pitch+cellNum], 0);
    V = P_MORSE(r, INTRA_U0, INTRA_ETA0, INTRA_U1, INTRA_ETA1);
    interZ += V * (Z[elemNum*pitch+cellNum] - 0);
  }
#endif

  // update
  scem_0_sem_grids->X_F2[idx] = scem_0_sem_grids->X[idx] + scem_0_sem_grids->dt * (intraX + interX);
  scem_0_sem_grids->Y_F2[idx] = scem_0_sem_grids->Y[idx] + scem_0_sem_grids->dt * (intraY + interY);
  scem_0_sem_grids->Z_F2[idx] = scem_0_sem_grids->Z[idx] + scem_0_sem_grids->dt * (intraZ + interZ);
  
#if PERIODIC_BOUNDARY
  if (X_F2[elemNum*pitch+cellNum] < 0.0) X_F2[elemNum*pitch+cellNum] = X_F2[elemNum*pitch+cellNum] + BOUNDARY_X;
  if (X_F2[elemNum*pitch+cellNum] > BOUNDARY_X) X_F2[elemNum*pitch+cellNum] = X_F2[elemNum*pitch+cellNum] - BOUNDARY_X;
  if (Y_F2[elemNum*pitch+cellNum] < 0.0) Y_F2[elemNum*pitch+cellNum] = Y_F2[elemNum*pitch+cellNum] + BOUNDARY_Y;
  if (Y_F2[elemNum*pitch+cellNum] > BOUNDARY_Y) Y_F2[elemNum*pitch+cellNum] = Y_F2[elemNum*pitch+cellNum] - BOUNDARY_Y;
  //if (Z_F2[elemNum*pitch+cellNum] < 0.0) Z_F2[elemNum*pitch+cellNum] = Z_F2[elemNum*pitch+cellNum] + BOUNDARY_Z;
  //if (Z_F2[elemNum*pitch+cellNum] > BOUNDARY_Z) Z_F2[elemNum*pitch+cellNum] = Z_F2[elemNum*pitch+cellNum] - BOUNDARY_Z;
#endif
  
#if HARD_Z
  if (Z_F2[elemNum*pitch+cellNum] < 0.0) Z_F2[elemNum*pitch+cellNum] = 0.0;
  if (Z_F2[elemNum*pitch+cellNum] > BOUNDARY_Z) Z_F2[elemNum*pitch+cellNum] = BOUNDARY_Z;
#endif
}

#endif

#endif // _BC_GPU_SEM_H_

