/*
  MultiScale2DGrid.m

  Implementation of multiple 2D spatial grid overlays at different scales

  Copyright (C) 2005-2011 Scott Christley

  Author: Scott Christley <schristley@mac.com>
  Date: 2005
   
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "MultiScale2DGrid.h"
#import "Grid2DArray.h"
#import "BioSwarmModel.h"

@implementation MultiScale2DGrid

- initWithWidth: (unsigned int)aWidth andHeight: (unsigned int)aHeight
{
  [super init];

  width = aWidth;
  height = aHeight;
  grids = [NSMutableDictionary new];

  return self;
}

- (void)dealloc
{
  [grids release];
  [super dealloc];
}

- (NSDictionary *)newGridWithName: (NSString *)name atScale: (unsigned int)scale
		       withEncode: (NSString *)encode
{
  NSMutableDictionary *newGrid = [NSMutableDictionary new];

  int w = width * scale;
  int h = height * scale;

  [newGrid setObject: name forKey: @"name"];
  [newGrid setObject: [[NSNumber alloc] initWithInt: w] forKey: @"width"];
  [newGrid setObject: [[NSNumber alloc] initWithInt: h] forKey: @"height"];
  [newGrid setObject: [[NSNumber alloc] initWithInt: scale] forKey: @"scale"];
  [newGrid setObject: encode forKey: @"encode"];
  id matrix = [[Grid2DArray alloc] initWithWidth: w andHeight: h andEncode: encode];
  [matrix getMatrix];
  [newGrid setObject: matrix forKey: @"matrix"];
  [matrix release];
  [grids setObject: newGrid forKey: name];
  [newGrid release];

  return newGrid;
}

- (void)removeGridWithName: (NSString *)name
{
}

- (NSDictionary *)gridWithName: (NSString *)name
{
  return [grids objectForKey: name];
}

- (int)numOfDimensions { return 2; }

- (int)minValueForIntGrid: (NSString *)gridName atX: (int *)x atY: (int *)y
{
    NSDictionary *aGrid = [self gridWithName: gridName];
    
    if (!aGrid) return 0;
    Grid2DArray *a = [aGrid objectForKey: @"matrix"];
    if (!a) return 0;
    return [a minIntValueAtX: x atY: y];
}

- (int)maxValueForIntGrid: (NSString *)gridName atX: (int *)x atY: (int *)y
{
    NSDictionary *aGrid = [self gridWithName: gridName];
    
    if (!aGrid) return 0;
    Grid2DArray *a = [aGrid objectForKey: @"matrix"];
    if (!a) return 0;
    return [a maxIntValueAtX: x atY: y];
}

- (double)minValueForDoubleGrid: (NSString *)gridName atX: (int *)x atY: (int *)y
{
    NSDictionary *aGrid = [self gridWithName: gridName];
    
    if (!aGrid) return 0;
    Grid2DArray *a = [aGrid objectForKey: @"matrix"];
    if (!a) return 0;
    return [a minDoubleValueAtX: x atY: y];
}

- (double)maxValueForDoubleGrid: (NSString *)gridName atX: (int *)x atY: (int *)y
{
    NSDictionary *aGrid = [self gridWithName: gridName];
    
    if (!aGrid) return 0;
    Grid2DArray *a = [aGrid objectForKey: @"matrix"];
    if (!a) return 0;
    return [a maxDoubleValueAtX: x atY: y];
}

#if 0
- (int)maxValueForIntGrid: (NSString *)gridName
{
  NSDictionary *aGrid = [self gridWithName: gridName];

  if (!aGrid) return 0;

  NSNumber *n = [aGrid objectForKey: @"height"];
  int h = [n intValue];
  n = [aGrid objectForKey: @"width"];
  int w = [n intValue];
  Grid2DArray *m = [aGrid objectForKey: @"matrix"];
  int (*grid)[h][w] = [m getMatrix];

  int i, j;
  int max = INT_MIN;
  for (i = 0; i < h; ++i)
    for (j = 0; j < w; ++j)
      if ((*grid)[i][j] > max)
	max = (*grid)[i][j];

  return max;
}

- (double)maxValueForDoubleGrid: (NSString *)gridName
{
  NSDictionary *aGrid = [self gridWithName: gridName];

  if (!aGrid) return 0;

  NSNumber *n = [aGrid objectForKey: @"height"];
  int h = [n intValue];
  n = [aGrid objectForKey: @"width"];
  int w = [n intValue];
  Grid2DArray *m = [aGrid objectForKey: @"matrix"];
  double (*grid)[h][w] = [m getMatrix];

  int i, j;
  double max = -1;
  for (i = 0; i < h; ++i)
    for (j = 0; j < w; ++j)
      if ((*grid)[i][j] > max)
	max = (*grid)[i][j];

  return max;
}
#endif

- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile
{
  [self dumpGrid: gridName toFile: aFile withInvocation: nil];
}

- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation
{
  NSDictionary *aGrid = [grids objectForKey: gridName];
  NSNumber *n = [aGrid objectForKey: @"height"];
  int h = [n intValue];
  n = [aGrid objectForKey: @"width"];
  int w = [n intValue];
  Grid2DArray *m = [aGrid objectForKey: @"matrix"];
  NSString *encode = [aGrid objectForKey: @"encode"];

  int zero = 0;
  int one = 1;
  int i,j;
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[h][w] = [m getMatrix];
    for (i = 0;i < h; ++i)
      for (j = 0;j < w; ++j)
	if ((*grid)[i][j] != nil) {
	  if (anInvocation) {
	    int value;
	    [anInvocation invokeWithTarget: (*grid)[i][j]];
	    [anInvocation getReturnValue: &value];
	    //printf("value=%d\n", value);
	    fwrite(&value, sizeof(int), 1, aFile);
	  } else {
	    if ([(*grid)[i][j] respondsToSelector: @selector(intValue)]) {
	      int value = [(*grid)[i][j] intValue];
	      fwrite(&value, sizeof(int), 1, aFile);
	    } else
	      fwrite(&one, sizeof(int), 1, aFile);
	  }
	} else
	  fwrite(&zero, sizeof(int), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[h][w] = [m getMatrix];
    for (i = 0;i < h; ++i)
      for (j = 0;j < w; ++j)
	fwrite(&((*grid)[i][j]), sizeof(int), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[h][w] = [m getMatrix];
    for (i = 0;i < h; ++i)
      for (j = 0;j < w; ++j)
	fwrite(&((*grid)[i][j]), sizeof(long), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
      float (*grid)[h][w] = [m getMatrix];
      for (i = 0;i < h; ++i)
          for (j = 0;j < w; ++j)
              fwrite(&((*grid)[i][j]), sizeof(float), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[h][w] = [m getMatrix];
    for (i = 0;i < h; ++i)
      for (j = 0;j < w; ++j)
	fwrite(&((*grid)[i][j]), sizeof(double), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[h][w] = [m getMatrix];
    for (i = 0;i < h; ++i)
      for (j = 0;j < w; ++j)
	if ((*grid)[i][j] == YES)
	  fwrite(&one, sizeof(int), 1, aFile);
	else
	  fwrite(&zero, sizeof(int), 1, aFile);
  } else {
    // throw exception or something
  }

  fflush(aFile);
}

- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile
{
    [self loadGrid: gridName fromFile: aFile withInvocation: nil];
}

- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation
{
    NSDictionary *aGrid = [grids objectForKey: gridName];
    NSNumber *n = [aGrid objectForKey: @"height"];
    int h = [n intValue];
    n = [aGrid objectForKey: @"width"];
    int w = [n intValue];
    Grid2DArray *m = [aGrid objectForKey: @"matrix"];
    NSString *encode = [aGrid objectForKey: @"encode"];

    int value;
    int i,j;
    if ([encode isEqual: [BioSwarmModel idEncode]]) {
        id (*grid)[h][w] = [m getMatrix];
        for (i = 0;i < h; ++i)
            for (j = 0;j < w; ++j)
                // TODO: not sure what we want here
                if ((*grid)[i][j] != nil) {
                    if (anInvocation) {
                        int value;
                        [anInvocation invokeWithTarget: (*grid)[i][j]];
                        [anInvocation getReturnValue: &value];
                        //printf("value=%d\n", value);
                        fread(&value, sizeof(int), 1, aFile);
                    } else {
                        fread(&value, sizeof(int), 1, aFile);
                        if ([(*grid)[i][j] respondsToSelector: @selector(setIntValue:)]) {
                            [(*grid)[i][j] setIntValue: value];
                        }
                    }
                }
    } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
        int (*grid)[h][w] = [m getMatrix];
        for (i = 0;i < h; ++i)
            for (j = 0;j < w; ++j)
                fread(&((*grid)[i][j]), sizeof(int), 1, aFile);
    } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
        long (*grid)[h][w] = [m getMatrix];
        for (i = 0;i < h; ++i)
            for (j = 0;j < w; ++j)
                fread(&((*grid)[i][j]), sizeof(long), 1, aFile);
    } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
        float (*grid)[h][w] = [m getMatrix];
        for (i = 0;i < h; ++i)
            for (j = 0;j < w; ++j)
                fread(&((*grid)[i][j]), sizeof(float), 1, aFile);
    } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
        double (*grid)[h][w] = [m getMatrix];
        for (i = 0;i < h; ++i)
            for (j = 0;j < w; ++j)
                fread(&((*grid)[i][j]), sizeof(double), 1, aFile);
    } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
        BOOL (*grid)[h][w] = [m getMatrix];
        for (i = 0;i < h; ++i)
            for (j = 0;j < w; ++j) {
                fread(&value, sizeof(int), 1, aFile);
                if (value) (*grid)[i][j] = YES;
                else (*grid)[i][j] = NO;
            }
    } else {
        // throw exception or something
    }
    
}

@end
