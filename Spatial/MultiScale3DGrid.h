/*
 MultiScale3DGrid.h
 
 Implementation of multiple 3D spatial grid overlays at different scales
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: March 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "MultiScaleGrid.h"

@interface MultiScale3DGrid : NSObject <MultiScaleGrid>
{
  unsigned int width;
  unsigned int height;
  unsigned int depth;
  NSMutableDictionary *grids;
}

- initWithGridWidth: (unsigned int)aWidth height: (unsigned int)aHeight depth: (unsigned int)aDepth;

- (NSDictionary *)newGridWithName: (NSString *)name atScale: (unsigned int)scale
                       withEncode: (NSString *)encode;
- (void)removeGridWithName: (NSString *)name;
- (NSDictionary *)gridWithName: (NSString *)name;
- (int)numOfDimensions;

- (int)minValueForIntGrid: (NSString *)gridName atX: (int *)x atY: (int *)y atZ: (int *)z;
- (int)maxValueForIntGrid: (NSString *)gridName atX: (int *)x atY: (int *)y atZ: (int *)z;;
- (double)minValueForDoubleGrid: (NSString *)gridName atX: (int *)x atY: (int *)y atZ: (int *)z;;
- (double)maxValueForDoubleGrid: (NSString *)gridName atX: (int *)x atY: (int *)y atZ: (int *)z;;

- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile;
- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation;

- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile;
- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation;

@end


