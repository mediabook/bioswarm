/*
  Grid2DArray.h

  Wrapper around 2D grid array.

  Copyright (C) 2005-2011 Scott Christley

  Author: Scott Christley <schristley@mac.com>
  Date: 2005
   
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>

@interface Grid2DArray : NSObject
{
  unsigned int width;
  unsigned int height;
  NSString *encode;
  void *matrix;
}

- initWithWidth: (unsigned int)aWidth andHeight: (unsigned int)aHeight andEncode: (NSString *)anEncode;

- (unsigned int)getWidth;
- (unsigned int)getHeight;
- (void *)getMatrix;
- (NSString *)getEncode;

- (int)minIntValueAtX: (int *)x atY: (int *)y;
- (int)maxIntValueAtX: (int *)x atY: (int *)y;
- (double)minDoubleValueAtX: (int *)x atY: (int *)y;
- (double)maxDoubleValueAtX: (int *)x atY: (int *)y;

- (void)clearMatrix;

- (int)countClusters: (BOOL)periodic threshold: (double)threshold countGrid: (void *)cGrid;
- (double)maxDifference: (BOOL)periodic;

// GridData protocol
- (unsigned)getSizeX;
- (unsigned)getSizeY;
- getObjectAtX: (unsigned)x Y: (unsigned)y;
- (long)getValueAtX: (unsigned)x Y: (unsigned)y;

@end
