/*
 MultiScale3DGrid.m
 
 Implementation of multiple 3D spatial grid overlays at different scales
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: March 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "MultiScale3DGrid.h"
#import "Grid3DArray.h"
#import "Grid2DArray.h"
#import "BioSwarmModel.h"

@implementation MultiScale3DGrid

- initWithGridWidth: (unsigned int)aWidth height: (unsigned int)aHeight depth: (unsigned int)aDepth
{
  [super init];
  
  width = aWidth;
  height = aHeight;
  depth = aDepth;
  grids = [NSMutableDictionary new];
  
  return self;
}

- (void)dealloc
{
  [grids release];
  [super dealloc];
}

- (NSDictionary *)newGridWithName: (NSString *)name atScale: (unsigned int)scale
                       withEncode: (NSString *)encode
{
  NSMutableDictionary *newGrid = [NSMutableDictionary new];
  
  int w = width * scale;
  int h = height * scale;
  int d = depth * scale;
  
  [newGrid setObject: name forKey: @"name"];
  [newGrid setObject: [NSNumber numberWithInt: w] forKey: @"width"];
  [newGrid setObject: [NSNumber numberWithInt: h] forKey: @"height"];
  [newGrid setObject: [NSNumber numberWithInt: d] forKey: @"depth"];
  [newGrid setObject: [NSNumber numberWithInt: scale] forKey: @"scale"];
  [newGrid setObject: encode forKey: @"encode"];
  id matrix = [[Grid3DArray alloc] initWithWidth: w height: h depth: d encode: encode];
  [matrix getMatrix];
  [newGrid setObject: matrix forKey: @"matrix"];
  [matrix release];
  [grids setObject: newGrid forKey: name];
  [newGrid release];
  
  return newGrid;
}

- (void)removeGridWithName: (NSString *)name
{
}

- (NSDictionary *)gridWithName: (NSString *)name
{
  return [grids objectForKey: name];
}

- (int)numOfDimensions { return 3; }

- (int)minValueForIntGrid: (NSString *)gridName atX: (int *)x atY: (int *)y atZ: (int *)z
{
  NSDictionary *aGrid = [self gridWithName: gridName];
  
  if (!aGrid) return 0;
  Grid3DArray *a = [aGrid objectForKey: @"matrix"];
  if (!a) return 0;
  return [a minIntValueAtX: x atY: y atZ: z];
}

- (int)maxValueForIntGrid: (NSString *)gridName atX: (int *)x atY: (int *)y atZ: (int *)z
{
  NSDictionary *aGrid = [self gridWithName: gridName];
  
  if (!aGrid) return 0;
  Grid3DArray *a = [aGrid objectForKey: @"matrix"];
  if (!a) return 0;
  return [a maxIntValueAtX: x atY: y atZ: z];
}

- (double)minValueForDoubleGrid: (NSString *)gridName atX: (int *)x atY: (int *)y atZ: (int *)z
{
  NSDictionary *aGrid = [self gridWithName: gridName];
  
  if (!aGrid) return 0;
  Grid3DArray *a = [aGrid objectForKey: @"matrix"];
  if (!a) return 0;
  return [a minDoubleValueAtX: x atY: y atZ: z];
}

- (double)maxValueForDoubleGrid: (NSString *)gridName atX: (int *)x atY: (int *)y atZ: (int *)z
{
  NSDictionary *aGrid = [self gridWithName: gridName];
  
  if (!aGrid) return 0;
  Grid3DArray *a = [aGrid objectForKey: @"matrix"];
  if (!a) return 0;
  return [a maxDoubleValueAtX: x atY: y atZ: z];
}

- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile
{
  [self dumpGrid: gridName toFile: aFile withInvocation: nil];
}

- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation
{
  NSDictionary *aGrid = [grids objectForKey: gridName];
  int w = [[aGrid objectForKey: @"width"] intValue];
  int h = [[aGrid objectForKey: @"height"] intValue];
  int d = [[aGrid objectForKey: @"depth"] intValue];
  Grid3DArray *m = [aGrid objectForKey: @"matrix"];
  NSString *encode = [aGrid objectForKey: @"encode"];
  
  int zero = 0;
  int one = 1;
  int i,j,k;
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          if ((*grid)[i][j][k] != nil) {
            if (anInvocation) {
              int value;
              [anInvocation invokeWithTarget: (*grid)[i][j][k]];
              [anInvocation getReturnValue: &value];
              //printf("value=%d\n", value);
              fwrite(&value, sizeof(int), 1, aFile);
            } else {
              if ([(*grid)[i][j][k] respondsToSelector: @selector(intValue)]) {
                int value = [(*grid)[i][j][k] intValue];
                fwrite(&value, sizeof(int), 1, aFile);
              } else
                fwrite(&one, sizeof(int), 1, aFile);
            }
          } else
            fwrite(&zero, sizeof(int), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          fwrite(&((*grid)[i][j][k]), sizeof(int), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          fwrite(&((*grid)[i][j][k]), sizeof(long), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          fwrite(&((*grid)[i][j][k]), sizeof(float), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          fwrite(&((*grid)[i][j][k]), sizeof(double), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          if ((*grid)[i][j][k] == YES)
            fwrite(&one, sizeof(int), 1, aFile);
          else
            fwrite(&zero, sizeof(int), 1, aFile);
  } else {
    // throw exception or something
  }
  
  fflush(aFile);
}

- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile
{
  [self loadGrid: gridName fromFile: aFile withInvocation: nil];
}

- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation
{
  NSDictionary *aGrid = [grids objectForKey: gridName];
  int w = [[aGrid objectForKey: @"width"] intValue];
  int h = [[aGrid objectForKey: @"height"] intValue];
  int d = [[aGrid objectForKey: @"depth"] intValue];
  Grid3DArray *m = [aGrid objectForKey: @"matrix"];
  NSString *encode = [aGrid objectForKey: @"encode"];
  
  int value;
  int i,j,k;
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          // TODO: not sure what we want here
          if ((*grid)[i][j][k] != nil) {
            if (anInvocation) {
              int value;
              [anInvocation invokeWithTarget: (*grid)[i][j][k]];
              [anInvocation getReturnValue: &value];
              //printf("value=%d\n", value);
              fread(&value, sizeof(int), 1, aFile);
            } else {
              fread(&value, sizeof(int), 1, aFile);
              if ([(*grid)[i][j][k] respondsToSelector: @selector(setIntValue:)]) {
                [(*grid)[i][j][k] setIntValue: value];
              }
            }
          }
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          fread(&((*grid)[i][j][k]), sizeof(int), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          fread(&((*grid)[i][j][k]), sizeof(long), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          fread(&((*grid)[i][j][k]), sizeof(float), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k)
          fread(&((*grid)[i][j][k]), sizeof(double), 1, aFile);
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[d][h][w] = [m getMatrix];
    for (i = 0;i < d; ++i)
      for (j = 0;j < h; ++j)
        for (k = 0;k < w; ++k) {
          fread(&value, sizeof(int), 1, aFile);
          if (value) (*grid)[i][j][k] = YES;
          else (*grid)[i][j][k] = NO;
        }
  } else {
    // throw exception or something
  }
  
}

@end
