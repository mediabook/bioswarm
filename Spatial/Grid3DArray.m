/*
 Grid3DArray.h
 
 Wrapper around 3D grid array.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "Grid3DArray.h"
#import "Grid2DArray.h"
#import "BioSwarmModel.h"


@implementation Grid3DArray

- initWithWidth: (unsigned int)aWidth height: (unsigned int)aHeight depth: (unsigned int)aDepth encode: (NSString *)anEncode;
{
  [super init];

  width = aWidth;
  height = aHeight;
  depth = aDepth;
  encode = anEncode;
  
  int i, j, k;
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    int size = sizeof(id[depth][height][width]);
    matrix = malloc(size);
    //printf("id array [%d][%d] size: %d at: %p\n", height, width, size, matrix);
    id (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          (*grid)[i][j][k] = nil;
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int size = sizeof(int[depth][height][width]);
    matrix = malloc(size);
    //printf("int array [%d][%d] size: %d at: %p\n", height, width, size, matrix);
    int (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          (*grid)[i][j][k] = 0;
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    matrix = malloc(sizeof(long[depth][height][width]));
    long (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          (*grid)[i][j][k] = 0;
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    matrix = malloc(sizeof(float[depth][height][width]));
    float (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          (*grid)[i][j][k] = 0.0;
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    matrix = malloc(sizeof(double[depth][height][width]));
    double (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          (*grid)[i][j][k] = 0.0;
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    matrix = malloc(sizeof(BOOL[depth][height][width]));
    BOOL (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          (*grid)[i][j][k] = NO;
  } else {
    // throw exception or something
    NSLog(@"ERROR: Grid2DArray unknown encoding %@\n", anEncode);
    return nil;
  }
  
  return self;
}


- (unsigned int)getWidth { return width; }
- (unsigned int)getHeight { return height; }
- (unsigned int)getDepth { return depth; }
- (void *)getMatrix { return matrix; }
- (NSString *)getEncode { return encode; }

- (int)minIntValueAtX: (int *)x atY: (int *)y atZ: (int *)z
{
  int minValue = 0;
  int i, j, k;
  
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k) {
          id obj = (*grid)[i][j][k];
          if (!obj) continue;
          if (![obj respondsToSelector: @selector(intValue)]) continue;
          if ((minValue == 0) || ([obj intValue] < minValue)) {
            minValue = [obj intValue];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
        }
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[depth][height][width] = matrix;
    minValue = (*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[depth][height][width] = matrix;
    minValue = (int)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (int)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[depth][height][width] = matrix;
    minValue = (int)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (int)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[depth][height][width] = matrix;
    minValue = (int)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (int)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[depth][height][width] = matrix;
    minValue = (int)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (int)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  }
  
  return minValue;
}

- (int)maxIntValueAtX: (int *)x atY: (int *)y atZ: (int *)z
{
  int maxValue = 0;
  int i, j, k;
  
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k) {
          id obj = (*grid)[i][j][k];
          if (!obj) continue;
          if (![obj respondsToSelector: @selector(intValue)]) continue;
          if ((maxValue == 0) || ([obj intValue] > maxValue)) {
            maxValue = [obj intValue];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
        }
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[depth][height][width] = matrix;
    maxValue = (*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[depth][height][width] = matrix;
    maxValue = (int)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (int)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[depth][height][width] = matrix;
    maxValue = (int)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (int)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[depth][height][width] = matrix;
    maxValue = (int)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (int)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[depth][height][width] = matrix;
    maxValue = (int)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (int)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  }
  
  return maxValue;
}

- (double)minDoubleValueAtX: (int *)x atY: (int *)y atZ: (int *)z
{
  double minValue = 0;
  int i, j, k;
  
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k) {
          id obj = (*grid)[i][j][k];
          if (!obj) continue;
          if (![obj respondsToSelector: @selector(doubleValue)]) continue;
          if ((minValue == 0) || ([obj doubleValue] < minValue)) {
            minValue = [obj doubleValue];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
        }
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[depth][height][width] = matrix;
    minValue = (*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[depth][height][width] = matrix;
    minValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[depth][height][width] = matrix;
    minValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[depth][height][width] = matrix;
    minValue = (*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[depth][height][width] = matrix;
    minValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  }
  
  return minValue;
}

- (double)maxDoubleValueAtX: (int *)x atY: (int *)y atZ: (int *)z
{
  double maxValue = 0;
  int i, j, k;
  
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[depth][height][width] = matrix;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k) {
          id obj = (*grid)[i][j][k];
          if (!obj) continue;
          if (![obj respondsToSelector: @selector(doubleValue)]) continue;
          if ((maxValue == 0) || ([obj doubleValue] > maxValue)) {
            maxValue = [obj doubleValue];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
        }
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[depth][height][width] = matrix;
    maxValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[depth][height][width] = matrix;
    maxValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[depth][height][width] = matrix;
    maxValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[depth][height][width] = matrix;
    maxValue = (*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[depth][height][width] = matrix;
    maxValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
  }
  
  return maxValue;
}

- (NSNumber *)minValueAtX: (int *)x atY: (int *)y atZ: (int *)z
{
  NSNumber *retVal = nil;
  int i, j, k;
  
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[depth][height][width] = matrix;
    double minValue = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k) {
          id obj = (*grid)[i][j][k];
          if (!obj) continue;
          if (![obj respondsToSelector: @selector(doubleValue)]) continue;
          if ((minValue == 0) || ([obj doubleValue] < minValue)) {
            minValue = [obj doubleValue];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
        }
    retVal = [NSNumber numberWithDouble: minValue];
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[depth][height][width] = matrix;
    int minValue = (*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithInt: minValue];
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[depth][height][width] = matrix;
    long minValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithLong: minValue];
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[depth][height][width] = matrix;
    float minValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithFloat: minValue];
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[depth][height][width] = matrix;
    double minValue = (*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithDouble: minValue];
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[depth][height][width] = matrix;
    BOOL minValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] < minValue) {
            minValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithBool: minValue];
  }
  
  return retVal;
}

- (NSNumber *)maxValueAtX: (int *)x atY: (int *)y atZ: (int *)z
{
  NSNumber *retVal = nil;
  int i, j, k;
  
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[depth][height][width] = matrix;
    double maxValue = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k) {
          id obj = (*grid)[i][j][k];
          if (!obj) continue;
          if (![obj respondsToSelector: @selector(doubleValue)]) continue;
          if ((maxValue == 0) || ([obj doubleValue] > maxValue)) {
            maxValue = [obj doubleValue];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
        }
    retVal = [NSNumber numberWithDouble: maxValue];
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[depth][height][width] = matrix;
    int maxValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithInt: maxValue];
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[depth][height][width] = matrix;
    long maxValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithLong: maxValue];
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[depth][height][width] = matrix;
    float maxValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithFloat: maxValue];
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[depth][height][width] = matrix;
    double maxValue = (*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithDouble: maxValue];
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[depth][height][width] = matrix;
    BOOL maxValue = (double)(*grid)[0][0][0];
    if (x) *x = 0;
    if (y) *y = 0;
    if (z) *z = 0;
    for (i = 0;i < depth; ++i)
      for (j = 0;j < height; ++j)
        for (k = 0;k < width; ++k)
          if ((*grid)[i][j][k] > maxValue) {
            maxValue = (double)(*grid)[i][j][k];
            if (x) *x = k;
            if (y) *y = j;
            if (z) *z = i;
          }
    retVal = [NSNumber numberWithBool: maxValue];
  }
  
  return retVal;
}

@end
