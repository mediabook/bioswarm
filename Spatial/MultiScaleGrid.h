/*
 MultiScaleGrid.h
 
 Protocol for multi-scale spatial grids.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#ifndef _BC_MULTISCALEGRID_H_
#define _BC_MULTISCALEGRID_H_

#import <Foundation/Foundation.h>

@protocol MultiScaleGrid <NSObject>

// access methods
- (int)numOfDimensions;

// manage grids
- (NSDictionary *)newGridWithName: (NSString *)name atScale: (unsigned int)scale
                       withEncode: (NSString *)encode;
- (void)removeGridWithName: (NSString *)name;
- (NSDictionary *)gridWithName: (NSString *)name;

// save grid to file
- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile;
- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation;

// load grid from file
- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile;
- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation;

@end

#endif // _BC_MULTISCALEGRID_H_
