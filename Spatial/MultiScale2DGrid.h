/*
  MultiScale2DGrid.h

  Implementation of multiple 2D spatial grid overlays at different scales

  Copyright (C) 2005-2011 Scott Christley

  Author: Scott Christley <schristley@mac.com>
  Date: 2005
   
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#ifdef GNUSTEP
#include <Foundation/Foundation.h>
#else
#import <Cocoa/Cocoa.h>
#endif

#import "Grid2DArray.h"
#import "MultiScaleGrid.h"

// boundary conditions
#define PERIODIC_BOUNDARY 0
#define NOFLUX_BOUNDARY 1

@interface MultiScale2DGrid : NSObject <MultiScaleGrid>
{
  unsigned int width;
  unsigned int height;
  NSMutableDictionary *grids;
}

- initWithWidth: (unsigned int)aWidth andHeight: (unsigned int)aHeight;
- (NSDictionary *)newGridWithName: (NSString *)name atScale: (unsigned int)scale
		       withEncode: (NSString *)encode;
- (void)removeGridWithName: (NSString *)name;
- (NSDictionary *)gridWithName: (NSString *)name;
- (int)numOfDimensions;

- (int)minValueForIntGrid: (NSString *)gridName atX: (int *)x atY: (int *)y;
- (int)maxValueForIntGrid: (NSString *)gridName atX: (int *)x atY: (int *)y;
- (double)minValueForDoubleGrid: (NSString *)gridName atX: (int *)x atY: (int *)y;
- (double)maxValueForDoubleGrid: (NSString *)gridName atX: (int *)x atY: (int *)y;

- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile;
- (void)dumpGrid: (NSString *)gridName toFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation;

- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile;
- (void)loadGrid: (NSString *)gridName fromFile: (FILE *)aFile withInvocation: (NSInvocation *)anInvocation;

@end

//
// Square lattice directions
//
typedef enum _BioSwarmDirection {
    BioSwarmLeft = 1,
    BioSwarmRight = 2,
    BioSwarmUp = 3,
    BioSwarmDown = 4,
    BioSwarmFront = 5,
    BioSwarmBack = 6
} BioSwarmDirection;


// Convenience functions

// get coordinates
//
// x-direction (left = 1, right = -1)
// y-direction (up = 2, down = -2)
//
static inline BOOL
bioswarm_cell_2Dneighbor(int x, int y, BioSwarmDirection direction, int width, int height,
                         int boundary, int *nx, int *ny)
{
    switch(direction) {
        case BioSwarmLeft:
            // left
            *nx = x - 1;
            *ny = y;
            if (*nx < 0) {
                if (boundary == PERIODIC_BOUNDARY) *nx = width - 1;
                else return NO;
            }
            return YES;

        case BioSwarmRight:
            // right
            *nx = x + 1;
            *ny = y;
            if (*nx == width) {
                if (boundary == PERIODIC_BOUNDARY) *nx = 0;
                else return NO;
            }
            return YES;
            
        case BioSwarmUp:
            // up
            *nx = x;
            *ny = y + 1;
            if (*ny == height) {
                if (boundary == PERIODIC_BOUNDARY) *ny = 0;
                else return NO;
            }
            return YES;
            
        case BioSwarmDown:
            // down
            *nx = x;
            *ny = y - 1;
            if (*ny < 0) {
                if (boundary == PERIODIC_BOUNDARY) *ny = height - 1;
                else return NO;
            }
            return YES;

        case BioSwarmFront:
	  // front is not valid in 2D
	  break;
        case BioSwarmBack:
	  // front is not valid in 2D
	  break;
    }
    return NO;
}
