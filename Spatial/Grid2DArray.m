/*
  Grid2DArray.m

  Wrapper around 2D grid array.

  Copyright (C) 2005-2011 Scott Christley

  Author: Scott Christley <schristley@mac.com>
  Date: 2005
   
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#include <Foundation/Foundation.h>
#include <math.h>
#import "Grid2DArray.h"
#import "BioSwarmModel.h"

@implementation Grid2DArray

- initWithWidth: (unsigned int)aWidth andHeight: (unsigned int)aHeight andEncode: (NSString *)anEncode
{
  [super init];

  width = aWidth;
  height = aHeight;
  encode = anEncode;

  int i, j;
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    int size = sizeof(id[height][width]);
    matrix = malloc(size);
    //printf("id array [%d][%d] size: %d at: %p\n", height, width, size, matrix);
    id (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j)
          (*grid)[i][j] = nil;
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int size = sizeof(int[height][width]);
    matrix = malloc(size);
    //printf("int array [%d][%d] size: %d at: %p\n", height, width, size, matrix);
    int (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j)
          (*grid)[i][j] = 0;
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    matrix = malloc(sizeof(long[height][width]));
    long (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j)
          (*grid)[i][j] = 0;
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
      matrix = malloc(sizeof(float[height][width]));
      float (*grid)[height][width] = matrix;
      for (i = 0;i < height; ++i)
          for (j = 0;j < width; ++j)
              (*grid)[i][j] = 0.0;
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    matrix = malloc(sizeof(double[height][width]));
    double (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j)
          (*grid)[i][j] = 0.0;
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    matrix = malloc(sizeof(BOOL[height][width]));
    BOOL (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j)
          (*grid)[i][j] = NO;
  } else {
    // throw exception or something
    NSLog(@"ERROR: Grid2DArray unknown encoding %@\n", anEncode);
    return nil;
  }

  return self;
}

- (void)dealloc
{
  free(matrix);
  [super dealloc];
}

- (unsigned int)getWidth { return width; }
- (unsigned int)getHeight { return height; }
- (void *)getMatrix { return matrix; }
- (NSString *)getEncode { return encode; }

- (void)clearMatrix
{
}

- (int)minIntValueAtX: (int *)x atY: (int *)y
{
    int minValue = 0;
    int i, j;

    if ([encode isEqual: [BioSwarmModel idEncode]]) {
        id (*grid)[height][width] = matrix;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j) {
                id obj = (*grid)[i][j];
                if (!obj) continue;
                if (![obj respondsToSelector: @selector(intValue)]) continue;
                if ((minValue == 0) || ([obj intValue] < minValue)) {
                    minValue = [obj intValue];
                    if (x) *x = j;
                    if (y) *y = i;
                }
            }
    } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
        int (*grid)[height][width] = matrix;
        minValue = (*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
        long (*grid)[height][width] = matrix;
        minValue = (int)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (int)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
        float (*grid)[height][width] = matrix;
        minValue = (int)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (int)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
        double (*grid)[height][width] = matrix;
        minValue = (int)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (int)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
        BOOL (*grid)[height][width] = matrix;
        minValue = (int)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (int)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    }

    return minValue;
}

- (int)maxIntValueAtX: (int *)x atY: (int *)y
{
    int maxValue = 0;
    int i, j;
    
    if ([encode isEqual: [BioSwarmModel idEncode]]) {
        id (*grid)[height][width] = matrix;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j) {
                id obj = (*grid)[i][j];
                if (!obj) continue;
                if (![obj respondsToSelector: @selector(intValue)]) continue;
                if ((maxValue == 0) || ([obj intValue] > maxValue)) {
                    maxValue = [obj intValue];
                    if (x) *x = j;
                    if (y) *y = i;
                }
            }
    } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
        int (*grid)[height][width] = matrix;
        maxValue = (*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
        long (*grid)[height][width] = matrix;
        maxValue = (int)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (int)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
        float (*grid)[height][width] = matrix;
        maxValue = (int)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (int)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
        double (*grid)[height][width] = matrix;
        maxValue = (int)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (int)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
        BOOL (*grid)[height][width] = matrix;
        maxValue = (int)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (int)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    }
    
    return maxValue;
}

- (double)minDoubleValueAtX: (int *)x atY: (int *)y
{
    double minValue = 0;
    int i, j;
    
    if ([encode isEqual: [BioSwarmModel idEncode]]) {
        id (*grid)[height][width] = matrix;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j) {
                id obj = (*grid)[i][j];
                if (!obj) continue;
                if (![obj respondsToSelector: @selector(doubleValue)]) continue;
                if ((minValue == 0) || ([obj doubleValue] < minValue)) {
                    minValue = [obj doubleValue];
                    if (x) *x = j;
                    if (y) *y = i;
                }
            }
    } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
        int (*grid)[height][width] = matrix;
        minValue = (*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (double)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
        long (*grid)[height][width] = matrix;
        minValue = (double)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (double)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
        float (*grid)[height][width] = matrix;
        minValue = (double)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (double)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
        double (*grid)[height][width] = matrix;
        minValue = (*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
        BOOL (*grid)[height][width] = matrix;
        minValue = (double)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] < minValue) {
                    minValue = (double)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    }
    
    return minValue;
}

- (double)maxDoubleValueAtX: (int *)x atY: (int *)y
{
    double maxValue = 0;
    int i, j;
    
    if ([encode isEqual: [BioSwarmModel idEncode]]) {
        id (*grid)[height][width] = matrix;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j) {
                id obj = (*grid)[i][j];
                if (!obj) continue;
                if (![obj respondsToSelector: @selector(doubleValue)]) continue;
                if ((maxValue == 0) || ([obj doubleValue] > maxValue)) {
                    maxValue = [obj doubleValue];
                    if (x) *x = j;
                    if (y) *y = i;
                }
            }
    } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
        int (*grid)[height][width] = matrix;
        maxValue = (double)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (double)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
        long (*grid)[height][width] = matrix;
        maxValue = (double)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (double)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
        float (*grid)[height][width] = matrix;
        maxValue = (double)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (double)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
        double (*grid)[height][width] = matrix;
        maxValue = (*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
        BOOL (*grid)[height][width] = matrix;
        maxValue = (double)(*grid)[0][0];
        if (x) *x = 0;
        if (y) *y = 0;
        for (i = 0;i < height; ++i)
            for (j = 0;j < width; ++j)
                if ((*grid)[i][j] > maxValue) {
                    maxValue = (double)(*grid)[i][j];
                    if (x) *x = j;
                    if (y) *y = i;
                }
    }
    
    return maxValue;
}

// count the number of connected components
- (void)visitNeighbors: (int)i : (int)j : (BOOL)periodic visitGrid: (void *)vGrid threshold: (double)threshold
            clusterNum: (int)aNum countGrid: (void *)cGrid
{
	BOOL (*visitGrid)[height][width] = vGrid;
  int (*countGrid)[height][width] = cGrid;

	// already visited, just return
	if ((*visitGrid)[i][j]) return;

	// indicate we visited this grid pixel
	(*visitGrid)[i][j] = YES;
	
	// nothing at this grid pixel, just return
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[height][width] = matrix;
    id obj = (*grid)[i][j];
    if (!obj) return;
    if (![obj respondsToSelector: @selector(doubleValue)]) return;
    if ([obj doubleValue] < threshold) return;
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[height][width] = matrix;
    if ((*grid)[i][j] < threshold) return;
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[height][width] = matrix;
    if ((*grid)[i][j] < threshold) return;
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[height][width] = matrix;
    if ((*grid)[i][j] < threshold) return;
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[height][width] = matrix;
    if ((*grid)[i][j] < threshold) return;
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[height][width] = matrix;
    if (!(*grid)[i][j]) return;
  }
  
  // record the cluster number
  if (countGrid) (*countGrid)[i][j] = aNum;
    
	// check up
	if (i == 0) {
		if (periodic) [self visitNeighbors: height - 1 : j : periodic visitGrid: visitGrid threshold: threshold
                            clusterNum: aNum countGrid: cGrid];
	} else {
		[self visitNeighbors: i - 1 : j : periodic visitGrid: visitGrid threshold: threshold
              clusterNum: aNum countGrid: cGrid];
	}

	// check down
	if (i == (height - 1)) {
		if (periodic) [self visitNeighbors: 0 : j : periodic visitGrid: visitGrid threshold: threshold
                            clusterNum: aNum countGrid: cGrid];
	} else {
		[self visitNeighbors: i + 1 : j : periodic visitGrid: visitGrid threshold: threshold
              clusterNum: aNum countGrid: cGrid];
	}
	
	// check left
	if (j == 0) {
		if (periodic) [self visitNeighbors: i : width - 1 : periodic visitGrid: visitGrid threshold: threshold
                            clusterNum: aNum countGrid: cGrid];
	} else {
		[self visitNeighbors: i : j - 1 : periodic visitGrid: visitGrid threshold: threshold
              clusterNum: aNum countGrid: cGrid];
	}
	
	// check right
	if (j == (width - 1)) {
		if (periodic) [self visitNeighbors: i : 0 : periodic visitGrid: visitGrid threshold: threshold
                            clusterNum: aNum countGrid: cGrid];
	} else {
		[self visitNeighbors: i : j + 1 : periodic visitGrid: visitGrid threshold: threshold
              clusterNum: aNum countGrid: cGrid];
	}
}

- (int)countClusters: (BOOL)periodic threshold: (double)threshold countGrid: (void *)cGrid
{
	int numClusters = 0;
	int i, j;
  int (*countGrid)[height][width] = cGrid;

	// indicator grid for which points we have visited
	BOOL visitGrid[height][width];
	for (i = 0;i < height; ++i)
		for (j = 0;j < width; ++j) {
			visitGrid[i][j] = NO;
      if (countGrid) (*countGrid)[i][j] = 0;
    }

  // ccount connected components
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        id obj = (*grid)[i][j];
        if (!obj) continue;
        if (![obj respondsToSelector: @selector(doubleValue)]) continue;
        if ( (!visitGrid[i][j]) && ([obj doubleValue] >= threshold)) {
          ++numClusters;
          [self visitNeighbors: i : j : periodic visitGrid: visitGrid threshold: threshold
                    clusterNum: numClusters countGrid: cGrid];
        }
      }
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        if ( (!visitGrid[i][j]) && ((*grid)[i][j] >= threshold)) {
          ++numClusters;
          [self visitNeighbors: i : j : periodic visitGrid: visitGrid threshold: threshold
                    clusterNum: numClusters countGrid: cGrid];
        }
      }
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        if ( (!visitGrid[i][j]) && ((*grid)[i][j] >= threshold)) {
          ++numClusters;
          [self visitNeighbors: i : j : periodic visitGrid: visitGrid threshold: threshold
                    clusterNum: numClusters countGrid: cGrid];
        }
      }
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        if ( (!visitGrid[i][j]) && ((*grid)[i][j] >= threshold)) {
          ++numClusters;
          [self visitNeighbors: i : j : periodic visitGrid: visitGrid threshold: threshold
                    clusterNum: numClusters countGrid: cGrid];
        }
      }
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        if ( (!visitGrid[i][j]) && ((*grid)[i][j] >= threshold)) {
          ++numClusters;
          [self visitNeighbors: i : j : periodic visitGrid: visitGrid threshold: threshold
                    clusterNum: numClusters countGrid: cGrid];
        }
      }
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        if ( (!visitGrid[i][j]) && ((*grid)[i][j])) {
          ++numClusters;
          [self visitNeighbors: i : j : periodic visitGrid: visitGrid threshold: threshold
                    clusterNum: numClusters countGrid: cGrid];
        }
      }
  }
  
	return numClusters;
}

// Find maximum value difference between two neighbor grid points
- (double)maxDifference: (BOOL)periodic
{
  double maxDiff = 0;
  int i, j;

  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        id obj = (*grid)[i][j];
        if (!obj) continue;
        if (![obj respondsToSelector: @selector(doubleValue)]) continue;
        double diff, objValue = [obj doubleValue];

        int iplus = i + 1;
        int iminus = i - 1;
        int jplus = j + 1;
        int jminus = j - 1;
        
        if (periodic) {
          if (iplus == height) iplus = 0;
          if (iminus < 0) iminus = height-1;
          if (jplus == width) jplus = 0;
          if (jminus < 0) jminus = width-1;
        } else {
          // use negative to indicate no neighbor
          if (iplus == height) iplus = -1;
          if (jplus == width) jplus = -1;
        }
                
        // up
        if (iplus >= 0) {
          id obj2 = (*grid)[iplus][j];
          if ((obj2) && ([obj2 respondsToSelector: @selector(doubleValue)])) {
            diff = fabs(objValue - [obj2 doubleValue]);
            if (diff > maxDiff) maxDiff = diff;
          }
        }

        // down
        if (iminus >= 0) {
          id obj2 = (*grid)[iminus][j];
          if ((obj2) && ([obj2 respondsToSelector: @selector(doubleValue)])) {
            diff = fabs(objValue - [obj2 doubleValue]);
            if (diff > maxDiff) maxDiff = diff;
          }
        }
        
        // left
        if (jminus >= 0) {
          id obj2 = (*grid)[i][jminus];
          if ((obj2) && ([obj2 respondsToSelector: @selector(doubleValue)])) {
            diff = fabs(objValue - [obj2 doubleValue]);
            if (diff > maxDiff) maxDiff = diff;
          }
        }
        
        // right
        if (jplus >= 0) {
          id obj2 = (*grid)[i][jplus];
          if ((obj2) && ([obj2 respondsToSelector: @selector(doubleValue)])) {
            diff = fabs(objValue - [obj2 doubleValue]);
            if (diff > maxDiff) maxDiff = diff;
          }
        }
      }
  } else if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        double diff, gridValue = (double)(*grid)[i][j];
        
        int iplus = i + 1;
        int iminus = i - 1;
        int jplus = j + 1;
        int jminus = j - 1;
        
        if (periodic) {
          if (iplus == height) iplus = 0;
          if (iminus < 0) iminus = height-1;
          if (jplus == width) jplus = 0;
          if (jminus < 0) jminus = width-1;
        } else {
          // use negative to indicate no neighbor
          if (iplus == height) iplus = -1;
          if (jplus == width) jplus = -1;
        }
        
        // up
        if (iplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iplus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }

        // down
        if (iminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iminus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }

        // left
        if (jminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jminus]);
          if (diff > maxDiff) maxDiff = diff;
        }

        // right
        if (jplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jplus]);
          if (diff > maxDiff) maxDiff = diff;
        }
      }
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        double diff, gridValue = (double)(*grid)[i][j];
        
        int iplus = i + 1;
        int iminus = i - 1;
        int jplus = j + 1;
        int jminus = j - 1;
        
        if (periodic) {
          if (iplus == height) iplus = 0;
          if (iminus < 0) iminus = height-1;
          if (jplus == width) jplus = 0;
          if (jminus < 0) jminus = width-1;
        } else {
          // use negative to indicate no neighbor
          if (iplus == height) iplus = -1;
          if (jplus == width) jplus = -1;
        }
        
        // up
        if (iplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iplus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // down
        if (iminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iminus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // left
        if (jminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jminus]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // right
        if (jplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jplus]);
          if (diff > maxDiff) maxDiff = diff;
        }
      }
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
    float (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        double diff, gridValue = (double)(*grid)[i][j];
        
        int iplus = i + 1;
        int iminus = i - 1;
        int jplus = j + 1;
        int jminus = j - 1;
        
        if (periodic) {
          if (iplus == height) iplus = 0;
          if (iminus < 0) iminus = height-1;
          if (jplus == width) jplus = 0;
          if (jminus < 0) jminus = width-1;
        } else {
          // use negative to indicate no neighbor
          if (iplus == height) iplus = -1;
          if (jplus == width) jplus = -1;
        }
        
        // up
        if (iplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iplus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // down
        if (iminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iminus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // left
        if (jminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jminus]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // right
        if (jplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jplus]);
          if (diff > maxDiff) maxDiff = diff;
        }
      }
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        double diff, gridValue = (double)(*grid)[i][j];
        
        int iplus = i + 1;
        int iminus = i - 1;
        int jplus = j + 1;
        int jminus = j - 1;
        
        if (periodic) {
          if (iplus == height) iplus = 0;
          if (iminus < 0) iminus = height-1;
          if (jplus == width) jplus = 0;
          if (jminus < 0) jminus = width-1;
        } else {
          // use negative to indicate no neighbor
          if (iplus == height) iplus = -1;
          if (jplus == width) jplus = -1;
        }
        
        // up
        if (iplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iplus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // down
        if (iminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iminus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // left
        if (jminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jminus]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // right
        if (jplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jplus]);
          if (diff > maxDiff) maxDiff = diff;
        }
      }
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[height][width] = matrix;
    for (i = 0;i < height; ++i)
      for (j = 0;j < width; ++j) {
        double diff, gridValue = (double)(*grid)[i][j];
        
        int iplus = i + 1;
        int iminus = i - 1;
        int jplus = j + 1;
        int jminus = j - 1;
        
        if (periodic) {
          if (iplus == height) iplus = 0;
          if (iminus < 0) iminus = height-1;
          if (jplus == width) jplus = 0;
          if (jminus < 0) jminus = width-1;
        } else {
          // use negative to indicate no neighbor
          if (iplus == height) iplus = -1;
          if (jplus == width) jplus = -1;
        }
        
        // up
        if (iplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iplus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // down
        if (iminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[iminus][j]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // left
        if (jminus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jminus]);
          if (diff > maxDiff) maxDiff = diff;
        }
        
        // right
        if (jplus >= 0) {
          diff = fabs(gridValue - (double)(*grid)[i][jplus]);
          if (diff > maxDiff) maxDiff = diff;
        }
      }
  }
  
  return maxDiff;
}

//
// GridData protocol
//

- (unsigned)getSizeX { return width; }
- (unsigned)getSizeY { return height; }
- getObjectAtX: (unsigned)x Y: (unsigned)y
{
  if ([encode isEqual: [BioSwarmModel idEncode]]) {
    id (*grid)[height][width] = matrix;
    return (*grid)[y][x];
  }

  return nil;
}

- (long)getValueAtX: (unsigned)x Y: (unsigned)y
{
  if ([encode isEqual: [BioSwarmModel intEncode]]) {
    int (*grid)[height][width] = matrix;
    return (long)(*grid)[y][x];
  } else if ([encode isEqual: [BioSwarmModel longEncode]]) {
    long (*grid)[height][width] = matrix;
    return (*grid)[y][x];
  } else if ([encode isEqual: [BioSwarmModel floatEncode]]) {
      float (*grid)[height][width] = matrix;
      return (long)(*grid)[y][x];
  } else if ([encode isEqual: [BioSwarmModel doubleEncode]]) {
    double (*grid)[height][width] = matrix;
    return (long)(*grid)[y][x];
  } else if ([encode isEqual: [BioSwarmModel boolEncode]]) {
    BOOL (*grid)[height][width] = matrix;
    return (long)(*grid)[y][x];
  }

  return 0;
}

@end
