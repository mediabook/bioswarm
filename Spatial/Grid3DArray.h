/*
 Grid3DArray.h
 
 Wrapper around 3D grid array.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */


#import <Foundation/Foundation.h>


@interface Grid3DArray : NSObject {
  unsigned int width;
  unsigned int height;
  unsigned int depth;
  NSString *encode;
  void *matrix;  
}

- initWithWidth: (unsigned int)aWidth height: (unsigned int)aHeight depth: (unsigned int)aDepth encode: (NSString *)anEncode;

- (unsigned int)getWidth;
- (unsigned int)getHeight;
- (unsigned int)getDepth;
- (void *)getMatrix;
- (NSString *)getEncode;

- (int)minIntValueAtX: (int *)x atY: (int *)y atZ: (int *)z;
- (int)maxIntValueAtX: (int *)x atY: (int *)y atZ: (int *)z;
- (double)minDoubleValueAtX: (int *)x atY: (int *)y atZ: (int *)z;
- (double)maxDoubleValueAtX: (int *)x atY: (int *)y atZ: (int *)z;

- (NSNumber *)minValueAtX: (int *)x atY: (int *)y atZ: (int *)z;
- (NSNumber *)maxValueAtX: (int *)x atY: (int *)y atZ: (int *)z;

@end
