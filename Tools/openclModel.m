/*
 openclModel.m
 
 OpenCL code from a model specification.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3
 of the License, or (at your option) any later version.
 
 You should have received a copy of the GNU General Public
 License along with this library; see the file COPYING.
 If not, write to the Free Software Foundation,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <Foundation/Foundation.h>
#import "ReactionDiffusionModel.h"

void printUsage(NSArray *args);

int
main( int argc, char** argv) 
{
  NSMutableDictionary *modelSpec;
  NSAutoreleasePool *pool;
  NSString *paramFile;
  NSString *prefixName;
  
  pool = [NSAutoreleasePool new];
  
  // determine model file
  NSArray *args = [[NSProcessInfo processInfo] arguments];
  if ([args count] != 3) {
    printUsage(args);
    return 1;
  } else {
    paramFile = [args objectAtIndex: 1];
    prefixName = [args objectAtIndex: 2];
  }	
  
  // load model file
  modelSpec = [[NSMutableDictionary dictionaryWithContentsOfFile: paramFile] retain];
  if (!modelSpec) {
    printf("Could not open model specification file: %s\n", [paramFile UTF8String]);
    return 1;
  }
  
  // create models
  NSDictionary *models = [modelSpec objectForKey: @"model"];
  if (!models) {
    printf("No top-level model defined.\n");
    return 1;
  }

  //id rd = [[ReactionDiffusionModel alloc] initWithModel: runParameters spatialGrid: nil];
  
  // generate GPU code
  //NSMutableString *initCode = [rd controllerGPUCode: prefixName];
  //NSString *outFile = [NSString stringWithFormat: @"%@.cu", prefixName];
  //[initCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
  //initCode = [rd kernelGPUCode: prefixName];
  //outFile = [NSString stringWithFormat: @"%@_kernel.cu", prefixName];
  //[initCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
  
  [pool release];
  return 0;
}

void printUsage(NSArray *args)
{
  printf("Usage:\n");
  printf("%s modelFile outputName\n\n", [[[args objectAtIndex: 0] lastPathComponent] UTF8String]);
  printf("modelFile: file name of model specification.\n");
  printf("outputName: name for GPU code and files.\n");
  printf("\n");
}
