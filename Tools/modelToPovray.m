/*
 modelToPovray.m
 
 Produce povray visualation code from model output.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3
 of the License, or (at your option) any later version.
 
 You should have received a copy of the GNU General Public
 License along with this library; see the file COPYING.
 If not, write to the Free Software Foundation,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include <Foundation/Foundation.h>
#import "PovrayManager.h"

int
main( int argc, char** argv) 
{
  NSAutoreleasePool *pool;
  
  pool = [NSAutoreleasePool new];

  PovrayManager *aManager = [PovrayManager new];
  if (![aManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    printf("ERROR: An error occurred while generating the povray code.\n");
    return 1;
  }
  
  return 0;
}
