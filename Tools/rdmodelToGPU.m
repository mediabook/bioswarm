/*
 rdmodelToGPU.m
 
 Produce GPU code for a reaction-diffusion model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2009
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3
 of the License, or (at your option) any later version.
 
 You should have received a copy of the GNU General Public
 License along with this library; see the file COPYING.
 If not, write to the Free Software Foundation,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <Foundation/Foundation.h>
#import "ReactionDiffusionModel.h"

void printUsage(NSArray *args);

int
main( int argc, char** argv) 
{
    NSMutableDictionary *runParameters;
    NSAutoreleasePool *pool;
    NSString *paramFile;
    NSString *prefixName;
    
    pool = [NSAutoreleasePool new];
    
    // determine parameter file
    NSArray *args = [[NSProcessInfo processInfo] arguments];
    if ([args count] != 3) {
        printUsage(args);
        return 1;
    } else {
        paramFile = [args objectAtIndex: 1];
        prefixName = [args objectAtIndex: 2];
    }	
    
    // load parameter file
    runParameters = [[NSMutableDictionary dictionaryWithContentsOfFile: paramFile] retain];
    if (!runParameters) {
        NSLog(@"Could not open parameter file: %@", paramFile);
        return 1;
    }
    
    // create model
    id rd = [[ReactionDiffusionModel alloc] initWithModel: runParameters spatialGrid: nil];
    
    // generate GPU code
    NSMutableString *initCode = [rd controllerGPUCode: prefixName];
    NSString *outFile = [NSString stringWithFormat: @"%@.cu", prefixName];
    [initCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
    initCode = [rd kernelGPUCode: prefixName];
    outFile = [NSString stringWithFormat: @"%@_kernel.cu", prefixName];
    [initCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];

    [pool release];
    return 0;
}

void printUsage(NSArray *args)
{
    printf("Usage:\n");
    printf("%s modelFile outputName\n\n", [[[args objectAtIndex: 0] lastPathComponent] UTF8String]);
    printf("modelFile: file name of model specification.\n");
    printf("outputName: name for GPU code and files.\n");
    printf("\n");
}
