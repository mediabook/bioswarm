/*
 modelToGPU.m
 
 Produce GPU code from a model specification.
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3
 of the License, or (at your option) any later version.
 
 You should have received a copy of the GNU General Public
 License along with this library; see the file COPYING.
 If not, write to the Free Software Foundation,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <Foundation/Foundation.h>
#import <BioSwarm/CodeGenerationManager.h>

void printUsage(NSArray *args);

int
main( int argc, char** argv) 
{
  NSMutableDictionary *modelSpec;
  NSAutoreleasePool *pool;
  NSString *paramFile;
  NSString *prefixName;
  
  pool = [NSAutoreleasePool new];
  
  CodeGenerationManager *codeManager = [CodeGenerationManager new];
  if ([codeManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    if (![codeManager generateGPUCode]) {
      printf("ERROR: An error occurred while generating the GPU code.\n");
      return 1;
    }
  }
  
  [pool release];
  return 0;
}

void printUsage(NSArray *args)
{
  printf("Usage:\n");
  printf("%s modelFile outputName\n\n", [[[args objectAtIndex: 0] lastPathComponent] UTF8String]);
  printf("modelFile: file name of model specification.\n");
  printf("outputName: name for GPU code and files.\n");
  printf("\n");
}
