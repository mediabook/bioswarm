/*
 rmodelToGPU.m
 
 Produce GPU code for a reaction model.
 
 Copyright (C) 2009-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: 2009
 
 This file is part of the BioSwarm Framework.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3
 of the License, or (at your option) any later version.

 You should have received a copy of the GNU General Public
 License along with this library; see the file COPYING.
 If not, write to the Free Software Foundation,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <Foundation/Foundation.h>
#import <BioSwarm/ReactionModel.h>

void printUsage(NSArray *args);

int
main( int argc, char** argv) 
{
  int i;
  NSMutableDictionary *runParameters;
  NSAutoreleasePool *pool;
  NSString *paramFile = nil;
  NSString *prefixName = nil;
  BOOL dynamicCode = NO;
  
  pool = [NSAutoreleasePool new];
  
  // parse command line
  NSArray *args = [[NSProcessInfo processInfo] arguments];
  for (i = 1; i < [args count]; ++i) {
    // check options
    if ([[args objectAtIndex: i] isEqualToString: @"--dynamic"]) {
      dynamicCode = YES;
      continue;
    }

    if (!paramFile) {
      paramFile = [args objectAtIndex: i];
      continue;
    }
    if (!prefixName) {
      prefixName = [args objectAtIndex: i];
      continue;
    }
  }

  if ((!paramFile) || (!prefixName)) {
    printUsage(args);
    return 1;
  }	
  
  // load parameter file
  runParameters = [[NSMutableDictionary dictionaryWithContentsOfFile: paramFile] retain];
  if (!runParameters) {
    NSLog(@"Could not open parameter file: %@", paramFile);
    return 1;
  }
  
  // create model
  id rd = [[ReactionModel alloc] initWithModel: runParameters];
  
  // generate GPU code
  if (dynamicCode) {
    NSMutableString *initCode = [rd controllerDynamicGPUCode: prefixName];
    NSString *outFile = [NSString stringWithFormat: @"%@.cu", prefixName];
    [initCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
    initCode = [rd kernelDynamicGPUCode: prefixName];
    outFile = [NSString stringWithFormat: @"%@_kernel.cu", prefixName];
    [initCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
  } else {
    NSMutableString *initCode = [rd controllerGPUCode: prefixName];
    NSString *outFile = [NSString stringWithFormat: @"%@.cu", prefixName];
    [initCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
    initCode = [rd kernelGPUCode: prefixName];
    outFile = [NSString stringWithFormat: @"%@_kernel.cu", prefixName];
    [initCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
  }

  [pool release];
  return 0;
}

void printUsage(NSArray *args)
{
  printf("Usage:\n");
  printf("%s [--dynamic] modelFile outputName\n\n", [[[args objectAtIndex: 0] lastPathComponent] UTF8String]);
  printf("--dynamic option produces GPU code\n");
  printf("\n");
  printf("modelFile: file name of model specification.\n");
  printf("outputName: name for GPU code and files.\n");
  printf("\n");
}
