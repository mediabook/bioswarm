/*
 SimulationController.h
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "SpatialModel.h"

@interface SimulationController : NSObject
{
  id delegate;
  BOOL respondsToInit;
  BOOL respondsToDidInit;
  BOOL respondsToWillRun;
  BOOL respondsToDidTransfer;
  BOOL respondsToDidFinish;
  
  // arguments
  NSString *modelFilename;
  BOOL doSweep;
  BOOL doRandom;
  BOOL doEvolutionaryStrategy;
  BOOL doMCMC;

  BOOL doRSearch;
  int randomRuns;
  int startRun;
  BOOL doAnalyze;
  int format;
  BOOL doLearn;
  NSString *trainFile;
  BOOL doSample;
  int paramSamples;
  double paramSD;
  BOOL doAllSample;
  int allSamples;
  double allSD;
  BOOL doLatin;
  int latinSamples;
  double latinSD;
  BOOL doAutoRange;
  BOOL doPerturbation;
  NSString *perturbationName;
  
  // models
  NSString *name;
  NSDictionary *model;
  NSString *modelDirectory;
  SpatialModel *spatialModel;
  NSMutableArray *modelInstances;
  NSMutableArray *modelSpecifications;
  NSMutableArray *totalSpecies;
  int numTotalSpecies;
  
  BOOL isDebug;
  BOOL isBinaryFile;
  int fileState;
  
  int outputRun;
  double outputFrequency;
  double simulateTime;
  double currentTime;

  int numOfIterations;
  int currentIteration;
  int currentSpecies;
  int currentRange;
  
  // random search
  NSArray *randomParameters;
  NSArray *notRandomParameters;
  
  // perturbations
  NSDictionary *perturbations;
  
  // GPU
  int gpuDevice;
  void *gpuFuncs;
}

- initWithDefaultModelFile: (NSString *)fileName;

- (BOOL)processArguments:(NSArray *)args;

- newInstanceForModel:(NSDictionary *)modelSpec;
- loadModel: (NSDictionary *)aModel;
- (void)releaseModel;

- (BOOL)saveModelSpecification;
- (BOOL)saveSpecificationForModel:(int)modelIndex forRun:(int)aNum;
- (BOOL)saveModelSpecification:(NSDictionary *)aModel forRun:(int)aNum;

- (void)setDelegate: anObj;
- delegate;
- (NSDictionary *)model;
- (NSString *)modelName;
- (NSString *)modelDirectory;
- (void)setModelDirectory: (NSString *)aDir;
- (int)outputRun;
- (double)outputFrequency;
- (double)simulateTime;
- (double)currentTime;
- (SpatialModel *)spatialModel;
//- (NSArray *)subModels;
//- (NSArray *)species;
//- (id)getModelWithName: (NSString *)aName;
- (NSArray *)modelInstances;
- (int)numModelInstances;
- (id)getModel:(int)modelIndex withName: (NSString *)aName;
- (NSArray *)modelSpecifications;
- (NSDictionary *)getSpecificationForModel:(int)modelIndex;

- (BOOL)isDebug;
- (void)setDebug: (BOOL)aFlag;

- (BOOL)isBinaryFile;
- (void)setBinaryFile: (BOOL)aFlag;

- (int)fileState;
- (void)setFileState: (int)aState;

- (int)constructParameterList:(NSArray *)includeList exclude:(NSArray *)excludeList
                        names:(NSMutableArray **)parameterNames indexes:(NSMutableArray **)parameterIndexes
                       models:(NSMutableArray **)parameterModels;

- (int)constructSpeciesList:(NSMutableArray **)speciesNames indexes:(NSMutableArray **)speciesIndexes
                     models:(NSMutableArray **)speciesModels;

- (void)allocateSimulationWithEncode: (NSString *)anEncode;
- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState;
- (void)allocateSimulation: (int)modelIndex withEncode: (NSString *)anEncode fileState: (int)aState;

- (BOOL)writeData;
- (BOOL)writeDataWithCheck: (BOOL)aFlag;
- (BOOL)writeDataForModel:(int)modelIndex withCheck: (BOOL)aFlag;
- (BOOL)writeTextData;
- (BOOL)writeTextDataWithCheck: (BOOL)aFlag;
- (BOOL)writeTextDataForModel:(int)modelIndex withCheck: (BOOL)aFlag;

- (BOOL)readData;
- (BOOL)readDataForModel:(int)modelIndex;
- (BOOL)readTextData;
- (BOOL)readTextDataForModel:(int)modelIndex;

- (void)initializeSimulation;
- (void)initializeSimulationForModel:(int)modelIndex;
- (void)runCPUSimulation;
- (void)doSimulationRun;
- (void)startSimulation;
- (void)cleanupSimulation;

- (int)numOfIterations;
- (int)currentIteration;
- (void)setCurrentIteration: (int)aNum;
- (int)currentSpecies;
- (void)setCurrentSpecies: (int)aNum;
- (float)speciesMin;
- (float)speciesMax;
- (void)setCurrentRange: (int)aNum;

- (void)setupSimulationReplay;
- (void)rewindFiles;
- (void)rewindFilesForModel:(int)modelIndex;
- (void)nextIteration;

- (NSArray *)randomParameters;
- (NSArray *)notRandomParameters;

- (BOOL)isPerturbation;
- (void)setPerturbation: (NSString *)pName;
- (NSDictionary *)perturbations;
- (void)applyPerturbation: (NSString *)pName;
- (void)applyPerturbation: (NSString *)pName forModel: (int)modelIndex;

@end

//
// GPU
//
#import "GPUDefines.h"

//
// Produce GPU code
//
@interface SimulationController (GPU)
- (BOOL)generateGPUCode: (NSString *)prefixName;
@end

@interface SimulationController (GPURun)
- (void)setGPUFunctions: (getGPUFunctions *)func;
- (getGPUFunctions *)gpuFunctions;
- (void)runGPUSimulation: (getGPUFunctions *)func;
@end

//
// Visualization
//

@interface SimulationController (Povray)
- (NSMutableString *)povrayCode;
- (NSMutableString *)povrayCodeForModel:(int)modelIndex;
@end
