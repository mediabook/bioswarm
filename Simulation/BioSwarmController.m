/*
 BioSwarmController.m
 
 The top level Swarm for controlling a simulation run.
 
 Copyright (C) 2012 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: March 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BioSwarmController.h"
#import "BiologicalModel.h"
#import "BioSwarmModel.h"
#import "Grid2DArray.h"
#import "EvolutionaryStrategy.h"
#import "MCMC.h"
#import <objc/runtime.h>

#import "internal.h"

// Notifications
NSString * const BioSwarmDidTransferDataNotification = @"BioSwarmDidTransferDataNotification";
NSString * const BioSwarmModelIsLoadedNotification = @"BioSwarmModelIsLoadedNotification";

@implementation BioSwarmController

+ (BioSwarmController *)newController: (NSString *)controllerType forModel: (NSDictionary *)aModel andManager: aManager
{
  BOOL good = YES;
  
  // look for settings
  NSDictionary *controllerSpec;
  id aType;
  if (!controllerType) {
    // default controller
    controllerSpec = aModel;
    aType = @"BioSwarmController";
  } else {
    // specialized controller type
    controllerSpec = [aModel objectForKey: controllerType];
    CHECK_PARAMF(controllerSpec, [controllerType UTF8String], good);
    if (!good) {
      printf("ERROR: No '%s' settings for model.\n", [controllerType UTF8String]);
      return nil;
    }

    aType = [controllerSpec objectForKey: @"type"];
    if (!aType) {
      printf("ERROR: Controller 'type' not defined for model: %s\n", [controllerType UTF8String]);
      return nil;
    }    
  }

  // create appropriate Controller object
  id aClass = objc_getClass([aType UTF8String]);
  if (!aClass) {
    printf("ERROR: Controller type does not exist: %s\n", [aType UTF8String]);
    return nil;
  }
  
  id obj = [[aClass alloc] initWithModel: aModel andManager: aManager];
  if (!obj) {
    printf("ERROR: Could not create Controller object: %s\n", [aType UTF8String]);
    return nil;
  }
  
  return obj;
}

- initWithModel: (NSDictionary *)aModel andManager: anObj
{
  [super init];

  simManager = anObj;
  model = aModel;
  
  isDebug = NO;
  id param = [aModel objectForKey: @"debug"];
  if (param) isDebug = [param boolValue];
  
  respondsToInit = NO;
  respondsToDidInit = NO;
  respondsToWillRun = NO;
  respondsToDidTransfer = NO;
  respondsToDidFinish = NO;

  startRun = 0;
  deltaTime = 0.0;
  largestTimeScale = 0;
  
  gpuFuncs = NULL;

  doPerturbation = NO;

  fileState = BCMODEL_WRITE_STATE;

  // spatial model
  NSDictionary *d = [model objectForKey: @"spatialModel"];
  if (!d) {
    printf("No spatialModel defined.\n");
  } else {
    if ([self isDebug]) printf("Processing spatial model.\n");
    spatialModel = [[SpatialModel alloc] initWithModel: d];
  }
  
  return self;
}

#if 0
#pragma mark == MANAGE MODELS AND SIMULATIONS ==
#endif

- newInstanceForModel: (NSDictionary *)aModel ofKey: (NSString *)modelKey withParent: aParent modelNumber: (int *)modelIndex
{
  int i;

  // we search for models at the top level
  NSDictionary *modelSpec;
  if (modelKey) modelSpec = [aModel objectForKey: modelKey];
  else modelSpec = aModel;
  if (!modelSpec) {
    printf("ERROR: Cannot find model: %s\n", [modelKey UTF8String]);
    return nil;
  }
    
  // determine the model type
  NSString *aType;
  if (modelKey) aType = [modelSpec objectForKey: @"type"];
  else aType = @"BioSwarmModel";
  if ([self isDebug]) printf("model type = %s\n", [aType UTF8String]);
  if (!aType) {
    printf("ERROR: Type not defined for model: %s\n", [modelKey UTF8String]);
    return nil;
  }
  
  id aClass = objc_getClass([aType UTF8String]);
  if (!aClass) {
    printf("ERROR: Model type does not exist: %s\n", [aType UTF8String]);
    return nil;
  }
  
  // create the model
  id modelObject = [[aClass alloc] initWithModel: modelSpec andKey: modelKey andController: self andParent: aParent];
  if (!modelObject) {
    printf("ERROR: Could not create model: %s\n", [modelKey UTF8String]);
    return nil;
  }
  [modelObject setModelNumber: *modelIndex];
  ++(*modelIndex);
  
  // TODO: some init, can we move?
  [modelObject setRunNumber: outputRun];
  //NSDictionary *simControl = [model objectForKey: @"simulationControl"];
  //NSString *param = [simControl objectForKey: @"outputRun"];
  //if (param) [modelObject setRunNumber: [param intValue]];
  //printf("outputRun = %d %s %s\n", [param intValue], [[simControl description] UTF8String], [param UTF8String]);
  
  // have model initialize its sub-hierarchy
  NSArray *modelNames = [modelSpec objectForKey: @"models"];
  if (modelNames) {
    for (i = 0; i < [modelNames count]; ++i) {
      NSString *mName = [modelNames objectAtIndex: i];

      id childModel = [self newInstanceForModel: aModel ofKey: mName withParent: modelObject modelNumber: modelIndex];
      if (!childModel) return nil;

      [modelObject addChildModel: childModel];
    }
  }

  return modelObject;
}

- newInstanceForModel:(NSDictionary *)modelSpec
{
  if (!modelSpec) modelSpec = model;

  int modelIndex = 0;
  id modelObject = [self newInstanceForModel: modelSpec ofKey: nil withParent: nil modelNumber: &modelIndex];
  if (modelObject) {
    [modelInstances addObject: modelObject];
    [modelSpecifications addObject: modelSpec];
  }

  return modelObject;
}

- loadModel: (NSDictionary *)aModel
{
  int i;
  NSString *param;
  
  model = [aModel retain];
  modelInstances = [NSMutableArray new];
  modelSpecifications = [NSMutableArray new];
  
  eventSchedule = [NSMutableArray new];

  isBinaryFile = YES;
  param = [aModel objectForKey: @"binaryFile"];
  if (param) isBinaryFile = [param boolValue];
  
  name = [model objectForKey: @"name"];
  CHECK_PARAM(name, "name");
  
  NSArray *m = [model objectForKey: @"models"];
  CHECK_PARAM(m, "models");
  
  NSDictionary *simControl = [model objectForKey: @"simulationControl"];
  CHECK_PARAM(simControl, "simulationControl");
  
  param = [simControl objectForKey: @"outputRun"];
  CHECK_PARAM(param, "outputRun");
  outputRun = [param intValue];
  
  param = [simControl objectForKey: @"outputFrequency"];
  CHECK_PARAM(param, "outputFrequency");
  outputFrequency = [param doubleValue];

  param = [simControl objectForKey: @"simulateTime"];
  CHECK_PARAM(param, "simulateTime");
  simulateTime = [param doubleValue];
  
  param = [simControl objectForKey: @"randomSeed"];
  CHECK_PARAM(param, "randomSeed");
  randomSeed = [param intValue];
  srand(randomSeed);
  
  isDoublePrecision = NO;
  param = [simControl objectForKey: @"doublePrecision"];
  if (param) isDoublePrecision = [param boolValue];

  useInitializeFile = NO;
  initializeFile = [simControl objectForKey: @"initializeFile"];
  isInitializeBinaryFile = NO;
  param = [aModel objectForKey: @"initializeBinaryFile"];
  if (param) isInitializeBinaryFile = [param boolValue];
  
  // create an initial model instance
  if (![self newInstanceForModel:nil]) return nil;

  // gather list of all species
  [self constructSpeciesList: &speciesNames indexes: &speciesIndexes modelNumbers: &speciesModelNumbers modelKeys: &speciesModelKeys];

  // inform delegate of species information
  if ([delegate respondsToSelector: @selector(speciesInfo:names:indexes:modelNumbers:modelKeys:)])
    [delegate speciesInfo: self names: speciesNames indexes: speciesIndexes modelNumbers: speciesModelNumbers
                modelKeys: speciesModelKeys];

  // gather list of all parameters
  [self constructParameterList: nil exclude: nil
                         names: &allParameterNames
                       indexes: &allParameterIndexes
                  modelNumbers: &allParameterModelNumbers
                     modelKeys: &allParameterModelKeys];

#if 0
  // gather list of species
  totalSpecies = [NSMutableArray new];
  numTotalSpecies = 0;
  NSArray *subModels = [modelInstances objectAtIndex: 0];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    numTotalSpecies += [m numOfSpecies];
  }

  // look for random search setup
  randomParameters = nil;
  notRandomParameters = nil;
  NSDictionary *randomSearch = [model objectForKey: @"randomSearch"];
  if (randomSearch) {
    // Specify which parameters to randomize?
    randomParameters = [randomSearch objectForKey: @"randomParameters"];
    if ([randomParameters count] == 0) randomParameters = nil;
    
    // Specify which parameters not to randomize?
    notRandomParameters = [randomSearch objectForKey: @"notRandomParameters"];
    if ([notRandomParameters count] == 0) notRandomParameters = nil;
  }
#endif
  
  // perturbations is optional
  perturbations = [model objectForKey: @"perturbations"];
  
  return self;
}

- (void)releaseModelHierarchy: modelObject
{
  // release child models first
  int i;
  NSArray *childModels = [modelObject childModels];
  for (i = 0; i < [childModels count]; ++i) [self releaseModelHierarchy: [childModels objectAtIndex: i]];
  
  [modelObject release];
}

- (void)releaseModel
{
  int i;
  [speciesNames release]; speciesNames = nil;
  [speciesIndexes release]; speciesIndexes = nil;
  [speciesModelNumbers release]; speciesModelNumbers = nil;
  [speciesModelKeys release]; speciesModelKeys = nil;
  
  [allParameterNames release]; allParameterNames = nil;
  [allParameterIndexes release]; allParameterIndexes = nil;
  [allParameterModelNumbers release]; allParameterModelNumbers = nil;
  [allParameterModelKeys release]; allParameterModelKeys = nil;
  
  [eventSchedule release]; eventSchedule = nil;
  [totalSpecies release]; totalSpecies = nil;
  [modelSpecifications release]; modelSpecifications = nil;
  for (i = 0; i < [modelInstances count]; ++i) {
    id modelObject = [modelInstances objectAtIndex: i];
    [self releaseModelHierarchy: modelObject];
  }
  [modelInstances release]; modelInstances = nil;
  [spatialModel release]; spatialModel = nil;
  [modelDirectory release]; modelDirectory = nil;
  [model release]; model = nil;
}

- (void)dealloc
{
  [self releaseModel];
  [delegate release];
  [eventSchedule release];

  [allParameterNames release];
  [allParameterIndexes release];
  [allParameterModelNumbers release];
  [allParameterModelKeys release];
  
  [super dealloc];
}

- (void)setDelegate: anObj
{
  delegate = anObj;
  if (delegate) {
    [delegate retain];
    // cache whether the delegate responds to messages
    if ([delegate respondsToSelector: @selector(initializeSimulationForModel:)]) respondsToInit = YES;
    else respondsToInit = NO;
    if ([delegate respondsToSelector: @selector(didInitializeSimulation:)]) respondsToDidInit = YES;
    else respondsToDidInit = NO;
    if ([delegate respondsToSelector: @selector(willRunSimulation:)]) respondsToWillRun = YES;
    else respondsToWillRun = NO;
    if ([delegate respondsToSelector: @selector(didTransferDataToGPU:sender:)]) respondsToDidTransfer = YES;
    else respondsToDidTransfer = NO;
    if ([delegate respondsToSelector: @selector(didFinishSimulation:)]) respondsToDidFinish = YES;
    else respondsToDidFinish = NO;
  } else {
    respondsToInit = NO;
    respondsToDidInit = NO;
    respondsToWillRun = NO;
    respondsToDidTransfer = NO;
    respondsToDidFinish = NO;
  }
  
}

- delegate { return delegate; }

- (id)manager { return simManager; }
- (NSDictionary *)model { return model; }
- (NSString *)modelName { return name; }
- (NSString *)modelDirectory { return modelDirectory; }
- (void)setModelDirectory: (NSString *)aDir
{
  if (modelDirectory) [modelDirectory release];
  if (!aDir || ([aDir length] == 0)) modelDirectory = nil;
    else modelDirectory = [aDir retain];
      }
- (int)outputRun { return outputRun; }
- (NSString *)outputSuffix { return [NSString stringWithFormat: @"%d", outputRun]; }
- (int)outputFrequency { return outputFrequency; }
- (double)simulateTime { return simulateTime; }
- (double)currentTime { return currentTime; }
- (SpatialModel *)spatialModel { return spatialModel; }
//- (NSArray *)subModels { return subModels; }
//- (NSArray *)species { return species; }
- (NSArray *)modelInstances { return modelInstances; }
- (int)numModelInstances { return [modelInstances count]; }
- (id)getModel:(int)modelIndex withName: (NSString *)aName
{
  int i;
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    if ([aName isEqualToString: [m modelName]]) return m;
  }
  return nil;
}
- (NSArray *)modelSpecifications { return modelSpecifications; }
- (NSDictionary *)getSpecificationForModel:(int)modelIndex { return [modelSpecifications objectAtIndex: modelIndex]; }

- (id)modelOfTypeClass: (Class)aClass
{
  return [self modelOfTypeClass: aClass forModel: 0];
}

- (id)modelOfTypeClass: (Class)aClass forModel:(int)modelIndex
{
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  return [modelObject modelOfTypeClass: aClass];
}

- (id)modelWithKey: (NSString *)aModelKey { return [self modelWithKey: aModelKey forModel: 0]; }

- (id)modelWithKey: (NSString *)aModelKey forModel:(int)modelIndex
{
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  return [modelObject modelWithKey: aModelKey];
}

- (BOOL)isDebug { return isDebug; }
- (void)setDebug: (BOOL)aFlag { isDebug = aFlag; }

- (BOOL)isBinaryFile { return isBinaryFile; }
- (void)setBinaryFile: (BOOL)aFlag { isBinaryFile = aFlag; }

- (int)fileState { return fileState; }
- (void)setFileState: (int)aState { fileState = aState; }

- (BOOL)isDoublePrecision { return isDoublePrecision; }
- (void)setDoublePrecision: (BOOL)aFlag { isDoublePrecision = aFlag; }

- (BOOL)useInitializeFile { return useInitializeFile; }
- (BOOL)isInitializeBinaryFile { return isInitializeBinaryFile; }
- (NSString *)initializeFile { return initializeFile; }

- (void)addSimulationEvent: (SimulationEvent *)anEvent
{
  [eventSchedule addObject: anEvent];
}

- (void)removeSimulationEvent: (SimulationEvent *)anEvent
{
  [eventSchedule removeObject: anEvent];
}

- (void)clearEventSchedule
{
  [eventSchedule removeAllObjects];
}

- (double)nextEventTime
{
  int i;
  
  if ([eventSchedule count] == 0) return -1;

  SimulationEvent *anEvent = [eventSchedule objectAtIndex: 0];
  double nextTime = anEvent->nextTime;
  for (i = 1; i < [eventSchedule count]; ++i) {
    anEvent = [eventSchedule objectAtIndex: i];
    if (anEvent->nextTime < nextTime) nextTime = anEvent->nextTime;
  }
  if ([self isDebug]) printf("nextEventTime = %f\n", nextTime);
  return nextTime;
}

- (void)processEventSchedule
{
  int i;
  BOOL done = NO;
  BOOL didTransferBefore = NO;
  BOOL willTransferAfter = NO;

  while (!done) {
    done = YES;
    for (i = 0; i < [eventSchedule count]; ++i) {
      SimulationEvent *anEvent = [eventSchedule objectAtIndex: i];
      if ([self isDebug]) printf("anEvent = %f %f\n", anEvent->nextTime, currentTime);
      if (TIME_EQUAL(anEvent->nextTime, currentTime)) {
        if (anEvent->requireTransfer & !didTransferBefore) {
          didTransferBefore = YES;
          willTransferAfter = YES;
          [self transferDataToGPU: NO];
        }
  
        [anEvent->theInvocation invoke];

        if ([self isDebug]) printf("repeatInterval = %f\n", anEvent->repeatInterval);
        if (anEvent->repeatInterval > 0) {
          anEvent->nextTime += anEvent->repeatInterval;
        } else {
          // if we remove event from list, should re-iterate
          done = NO;
          [self removeSimulationEvent: anEvent];
          break;
        }
      }
    }
  }
  
  if (willTransferAfter) {
    [self transferDataToGPU: YES];
  }
}

- (BOOL)saveModelSpecification
{
  return [self saveModelSpecification: model forRun: outputRun];
}
- (BOOL)saveSpecificationForModel:(int)modelIndex forRun:(int)aNum
{
  return [self saveModelSpecification: [self getSpecificationForModel: modelIndex] forRun: aNum];
}
- (BOOL)saveModelSpecification:(NSDictionary *)aModel forRun:(int)aNum
{
  NSString *outputFile = [NSString stringWithFormat: @"%@_run_%d.bioswarm", [aModel objectForKey: @"name"], aNum];
#if GNUSTEP
  [[NSPropertyListSerialization dataFromPropertyList: aModel
                                              format: NSPropertyListXMLFormat_v1_0
                                    errorDescription: NULL]
   writeToFile:outputFile atomically: YES];
#else
  [aModel writeToFile: outputFile atomically: NO];
#endif
  
  return YES;
}

- (BOOL)constructParameterList:(NSArray *)includeList exclude:(NSArray *)excludeList
                         names:(NSMutableArray **)pNames indexes:(NSMutableArray **)pIndexes
                  modelNumbers:(NSMutableArray **)pModelNumbers
                    modelKeys:(NSMutableArray **)pModelKeys
{
  int i;
  BOOL flag = YES;
  
  // determine parameters
  if (pNames) *pNames = [NSMutableArray new];
  if (pIndexes) *pIndexes = [NSMutableArray new];
  if (pModelNumbers) *pModelNumbers = [NSMutableArray new];
  if (pModelKeys) *pModelKeys = [NSMutableArray new];
  
  // validate the include and exclude list
  for (i = 0; i < [includeList count]; ++i) {
    NSString *testName = [includeList objectAtIndex: i];
    NSUInteger idx = [allParameterNames indexOfObject: testName];
    if (idx == NSNotFound) {
      printf("WARNING: Parameter (%s) in include list is not part of model.\n", [testName UTF8String]);
      flag = NO;
    }
  }
  
  for (i = 0; i < [excludeList count]; ++i) {
    NSString *testName = [includeList objectAtIndex: i];
    NSUInteger idx = [allParameterNames indexOfObject: testName];
    if (idx == NSNotFound) {
      printf("WARNING: Parameter (%s) in exclude list is not part of model.\n", [testName UTF8String]);
      flag = NO;
    }
  }

  id modelObject = [modelInstances objectAtIndex: 0];
  flag &= [modelObject hierarchyParameterList: includeList
                                      exclude: excludeList
                                        names: *pNames
                                      indexes: *pIndexes
                                 modelNumbers: *pModelNumbers
                                    modelKeys: *pModelKeys];
  //totalParameters = [*pNames count];
  //if ([self isDebug]) printf("totalParameters = %d\n", totalParameters);
  //printf("totalParameters = %d\n", totalParameters);

  return flag;
}

- (BOOL)setupParameterRanges: (NSDictionary *)ranges checkNames: (NSArray *)names
{
  id modelObject = [modelInstances objectAtIndex: 0];
  return [modelObject setupParameterRanges: ranges checkNames: names];
}

- (int)constructSpeciesList:(NSMutableArray **)sNames indexes:(NSMutableArray **)sIndexes
               modelNumbers:(NSMutableArray **)sModelNumbers modelKeys:(NSMutableArray **)sModelKeys
{
  BOOL flag = YES;

  // determine species
  if (sNames) *sNames = [NSMutableArray new];
  if (sIndexes) *sIndexes = [NSMutableArray new];
  if (sModelNumbers) *sModelNumbers = [NSMutableArray new];
  if (sModelKeys) *sModelKeys = [NSMutableArray new];
  
  id modelObject = [modelInstances objectAtIndex: 0];
  flag &= [modelObject hierarchySpeciesList: *sNames
                                    indexes: *sIndexes
                               modelNumbers: *sModelNumbers
                                  modelKeys: *sModelKeys];
  
  return flag;
}

- (BOOL)setupDataSets: (NSDictionary *)modelSpec matrixCount: (int)numMatrices
{
  int i, j;

  // data handler
  dataSets = [modelSpec objectForKey: @"dataSets"];
  if (dataSets) {
    id dataSet = [dataSets objectForKey: @"data"];
    if (!dataSet) {
      printf("ERROR: dataSets is missing 'data' property.\n");
      return NO;
    }
    
    NSDictionary *dataDesc = [model objectForKey: dataSet];
    if (!dataDesc) {
      printf("ERROR: Could not find data set: %s\n", [dataSet UTF8String]);
      return NO;
    }
    
    // data handler to manage observation data
    dataHandler = [[DataHandler alloc] initWithController: self andDescriptor: dataDesc];
    if (!dataHandler) return NO;
    
    // process experiment list
    experiments = [dataSets objectForKey: @"experiments"];
    printf("experiments: %ld\n", (unsigned long)[experiments count]);
    if (!experiments) {
      printf("ERROR: dataSets is missing 'experiments' list.\n");
      return NO;
    }
    
    // create comparison matrix for each experiment
    dataIndexes = [NSMutableDictionary new];
    NSMutableDictionary *compareMatrices = [NSMutableDictionary new];
    compareMatrixList = [NSMutableArray new];
    [compareMatrixList addObject: compareMatrices];
    NSDictionary *dataMatrices = [dataHandler dataMatrices];
    for (i = 0; i < [experiments count]; ++i) {
      NSString *expName = [experiments objectAtIndex: i];
      NSDictionary *anExperiment = [dataSets objectForKey: expName];
      if (!anExperiment) {
        printf("ERROR: Missing experiment descriptor: %s\n", [expName UTF8String]);
        return NO;
      }
      
      NSString *dataName = [anExperiment objectForKey: @"name"];
      if (!dataName) {
        printf("ERROR: Experiment (%s) missing 'name' descriptor\n", [expName UTF8String]);
        return NO;
      }
      
      NSString *expType = [anExperiment objectForKey: @"type"];
      if (!expType) {
        printf("ERROR: Experiment (%s) missing 'type' descriptor\n", [expName UTF8String]);
        return NO;
      }
      
      // TODO: species map should probably be experiment specific
      id speciesMap = [dataSets objectForKey: @"speciesMap"];
      if (speciesMap) {
        [dataHandler assignSpeciesMap: speciesMap];
        NSArray *colNames = [speciesMap allKeys];

        int rowNum = [dataHandler numberForName: dataName andType: expType];
        if (rowNum == 0) {
          printf("ERROR: No data with name '%s' of type '%s' for experiment '%s'\n", [dataName UTF8String], [expType UTF8String], [expName UTF8String]);
          return NO;
        }
        BCDataMatrix *nm = [BCDataMatrix emptyDataMatrixWithRows: rowNum andColumns: [colNames count] andEncode: BCdoubleEncode];
        [nm setColumnNames: colNames];
        [compareMatrices setObject: nm forKey: expName];
      } else {
        
        // if no species map then matrix columns must match
        NSMutableArray *idx = [NSMutableArray array];
        BCDataMatrix *dm = [dataMatrices objectForKey: dataName];
        if (!dm) {
          printf("ERROR: No data with name '%s' of type '%s' for experiment '%s'\n", [dataName UTF8String], [expType UTF8String], [expName UTF8String]);
          return NO;
        }
        NSArray *colNames = [dm columnNames];
        //printf("colNames = %s\n", [[colNames description] UTF8String]);
        for (j = 0; j < [colNames count]; ++j) {
          NSString *cName = [colNames objectAtIndex: j];
          NSUInteger k = [speciesNames indexOfObject: cName];
          if (k == NSNotFound) printf("%s: %d -> Not in model\n", [cName UTF8String], j);
          else printf("%s: %d -> %ld(%ld)\n", [cName UTF8String], j, (unsigned long)k, (unsigned long)[[speciesIndexes objectAtIndex: k] unsignedIntegerValue]);
          [idx addObject: [NSNumber numberWithUnsignedInteger: k]];
        }
        [dataIndexes setObject: idx forKey: expName];
        
        // create compare matrix to hold simulation data
        int rowNum = [dataHandler numberForName: dataName andType: expType];
        if (rowNum == 0) {
          printf("ERROR: No data with name '%s' of type '%s' for experiment '%s'\n", [dataName UTF8String], [expType UTF8String], [expName UTF8String]);
          return NO;
        }
        BCDataMatrix *nm = [BCDataMatrix emptyDataMatrixWithRows: rowNum andColumns: [idx count] andEncode: BCdoubleEncode];
        [nm setColumnNames: colNames];
        [compareMatrices setObject: nm forKey: expName];
      }
    }

    // duplicate matrices upto desired count
    NSArray *keys = [compareMatrices allKeys];
    for (i = 1; i < numMatrices; ++i) {
      NSMutableDictionary *d = [NSMutableDictionary new];
      for (j = 0; j < [keys count]; ++j) {
        NSString *key = [keys objectAtIndex: j];
        BCDataMatrix *nm = [compareMatrices objectForKey: key];
        [d setObject: [BCDataMatrix dataMatrixWithDataMatrix: nm] forKey: key];
      }
      [compareMatrixList addObject: d];
    }
    
  }
  
  return YES;
}

- (DataHandler *)dataHandler { return dataHandler; }

#if 0
#pragma mark == PERFORM SIMULATIONS ==
#endif

- (void)performHierarchy: (int)modelIndex withInvocation: (NSInvocation *)anInvocation
{
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  [modelObject performHierarchyInvocation: anInvocation];
}

- (void)performHierarchyInvocation: (NSInvocation *)anInvocation
{
  int i;
  for (i = 0; i < [modelInstances count]; ++i) {
    [self performHierarchy: i withInvocation: anInvocation];
  }
}

- (void)performSimulationInitialization
{
  printf("performSimulationInitialization\n");
  currentTime = 0;
  [self initializeGPUSimulation];
  [self transferDataToGPU: YES];
  [self writeHierarchyData];
}

- (void)performOutputSimulationData
{
  //printf("performOutputSimulationData: %f\n", currentTime);
  //if (gpuFuncs) [self transferDataToGPU: NO];
  [self writeHierarchyData];
}

- (void)setupHierarchyDataFilesWithState: (int)aState
{
#if 0
  if (fileState == BCMODEL_NOFILE_STATE) [self allocateHierarchyWithEncode: anEncode fileState: fileState];
  else {
    // check if would open too many files
    if (numTotalSpecies * [modelInstances count] > 200) {
      fileState = BCMODEL_APPEND_AND_CLOSE_STATE;
      [self allocateHierarchyWithEncode: anEncode fileState: fileState];
    } else {
      fileState = BCMODEL_WRITE_STATE;
      [self allocateHierarchyWithEncode: anEncode fileState: fileState];
    }
  }
#endif
  // check if would open too many files
  if ((numTotalSpecies * [modelInstances count] > 200) && (aState == BCMODEL_WRITE_STATE)) aState = BCMODEL_APPEND_AND_CLOSE_STATE;
    
  int i;
  for (i = 0; i < [modelInstances count]; ++i)
    [self setupHierarchyDataFiles: i withState: aState];  
}

- (void)setupHierarchyDataFiles: (int)modelIndex withState: (int)aState
{
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  [modelObject setupHierarchyDataFilesWithState: aState];
}

#if 0
- (void)allocateHierarchyWithEncode: (NSString *)anEncode
{
  if (fileState == BCMODEL_NOFILE_STATE) [self allocateHierarchyWithEncode: anEncode fileState: fileState];
  else {
    // check if would open too many files
    if (numTotalSpecies * [modelInstances count] > 200) {
      fileState = BCMODEL_APPEND_AND_CLOSE_STATE;
      [self allocateHierarchyWithEncode: anEncode fileState: fileState];
    } else {
      fileState = BCMODEL_WRITE_STATE;
      [self allocateHierarchyWithEncode: anEncode fileState: fileState];
    }
  }
}
#endif

- (void)allocateHierarchyWithEncode: (NSString *)anEncode
{
  int i;
  for (i = 0; i < [modelInstances count]; ++i)
    [self allocateHierarchy: i withEncode: anEncode];
}

- (void)allocateHierarchy: (int)modelIndex withEncode: (NSString *)anEncode
{
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  [modelObject allocateHierarchyWithEncode: anEncode];
}

- (void)initializeHierarchy
{
  // if initialization file
  // then read file and skip normal initialization
  if (initializeFile) {
    useInitializeFile = YES;
    [self setupHierarchyDataFilesWithState: BCMODEL_READ_STATE];
    [self readHierarchyData];

    useInitializeFile = NO;
    [self setupHierarchyDataFilesWithState: fileState];

    // allow specific initialization after reading initialization file
    NSInvocation *anInv = [NSInvocation invocationWithMethodSignature: [BioSwarmModel instanceMethodSignatureForSelector: @selector(initializeFileCompleted)]];
    [anInv setSelector: @selector(initializeFileCompleted)];
    [self performHierarchyInvocation: anInv];
  } else {
    [self setupHierarchyDataFilesWithState: fileState];

    int i;
    for (i = 0; i < [modelInstances count]; ++i)
      [self initializeHierarchyForModel: i];
  }
}

- (void)initializeHierarchyForModel:(int)modelIndex
{
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  [modelObject initializeHierarchy];
}

- (BOOL)writeHierarchyData
{
  return [self writeHierarchyDataWithCheck: YES];
}

- (BOOL)writeHierarchyDataWithCheck: (BOOL)aFlag
{
  int i;
  BOOL res = YES;
  
  for (i = 0; i < [modelInstances count]; ++i)
    res &= [self writeHierarchyDataForModel: i withCheck: aFlag];
    
    return res;
}

- (BOOL)writeHierarchyDataForModel:(int)modelIndex withCheck: (BOOL)aFlag
{
  int i;
  BOOL res = YES;
  
#if 0
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    res &= [m writeHierarchyDataWithCheck: aFlag];
  }
#else
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  res = [modelObject writeHierarchyDataWithCheck: aFlag];
#endif
  
  return res;
}

- (BOOL)readHierarchyData
{
  int i;
  BOOL res = YES;
  
  for (i = 0; i < [modelInstances count]; ++i)
    res &= [self readHierarchyDataForModel: i];
    
    return res;
}

- (BOOL)readHierarchyDataForModel:(int)modelIndex
{
  int i;
  BOOL res = YES;
  
#if 0
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    res &= [m readHierarchyData];
  }
#else
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  res = [modelObject readHierarchyData];
#endif
  
  return res;
}

- (int)largestTimeScale
{
  if (largestTimeScale == 0) {
    NSArray *subModels = [modelInstances objectAtIndex: 0];
    id m = [subModels objectAtIndex: 0];
    largestTimeScale = [m largestTimeScaleInHierarchy];
  }
  return largestTimeScale;
}

- (double)smallestDT
{
  if (deltaTime == 0.0) {
    //NSArray *subModels = [modelInstances objectAtIndex: 0];
    //id m = [subModels objectAtIndex: 0];
    id m = [modelInstances objectAtIndex: 0];
    deltaTime = [m smallestDTInHierarchy];
  }
  return deltaTime;
}

- (BOOL)loadModelHierarchy
{
  if (![self loadModel: model]) return NO;
  [[NSNotificationCenter defaultCenter] postNotificationName: BioSwarmModelIsLoadedNotification object: nil];
  return YES;
}

- (void)recordDataAt: (int)aRow
{
  if (!currentExperiment) return;
  
  int modelInstance;
  for (modelInstance = 0; modelInstance < [modelInstances count]; ++modelInstance) {
    id modelObject = [modelInstances objectAtIndex: modelInstance];
    NSDictionary *compareMatrices = [compareMatrixList objectAtIndex: modelInstance];
    BCDataMatrix *compareMatrix = [compareMatrices objectForKey: currentExperiment];
    int numCols = [compareMatrix numberOfColumns];
    int numRows = [compareMatrix numberOfRows];
    double (*CM)[numRows][numCols] = [compareMatrix dataMatrix];
    
    if (aRow >= numRows) {
      printf("ERROR: Invalid row (%d), numOfRows = %d\n", aRow, numRows);
      return;
    }

    // collect values and put into matrix for comparison
    NSDictionary *speciesMap = [dataSets objectForKey: @"speciesMap"];
    if (speciesMap) {
      int j;
      NSArray *colNames = [compareMatrix columnNames];
      for (j = 0; j < numCols; ++j) {
        NSString *cName = [colNames objectAtIndex: j];
        NSUInteger idx = [speciesNames indexOfObject: cName];
        printf("%d %s %ld\n", j, [cName UTF8String], (unsigned long)idx);
        int mnum = [[speciesModelNumbers objectAtIndex: idx] intValue];
        id aModel = [modelObject modelWithNumber: mnum];
        id val = [aModel valueOfSpecies: [[speciesIndexes objectAtIndex: idx] intValue] withMultiplicity: 1];
        if ([self isDebug]) printf("recordDataAt: %d %d : %s(%d) -> %ld = %lf\n", aRow, mnum, [cName UTF8String], j, (unsigned long)idx, [val doubleValue]);
        (*CM)[aRow][j] = [val doubleValue];
      }
    } else {
      int j;
      NSArray *idxArray = [dataIndexes objectForKey: currentExperiment];
      for (j = 0; j < numCols; ++j) {
        NSUInteger idx = [[idxArray objectAtIndex: j] unsignedIntegerValue];
        if (idx == NSNotFound) {
          // data for something which is not in model, so skip
          (*CM)[aRow][j] = 0.0;
        } else {
          NSString *cName = [speciesNames objectAtIndex: idx];
          int mnum = [[speciesModelNumbers objectAtIndex: idx] intValue];
          id aModel = [modelObject modelWithNumber: mnum];
          //id aModel = [subModels objectAtIndex: [[speciesModels objectAtIndex: idx] intValue]];
          id val = [aModel valueOfSpecies: [[speciesIndexes objectAtIndex: idx] intValue] withMultiplicity: 1];
          if ([self isDebug]) printf("recordDataAt(%d): %d %d : %s(%d) -> %ld(%d) = %lf\n", modelInstance, aRow, mnum,
				     [cName UTF8String], j, (unsigned long)idx, [[speciesIndexes objectAtIndex: idx] intValue], [val doubleValue]);
          (*CM)[aRow][j] = [val doubleValue];
        }
      }
    }
    
#if 0
    int k;
    printf("CM = \n");
    for (j = 0; j < numRows; ++j) {
      for (k = 0; k < numCols; ++k) printf("%f ", (*CM)[j][k]);
      printf("\n");
    }
#endif
    
  }
}

- (void)handleSimulationData:(NSNotification *)notification
{
  int i;
  NSDictionary *userInfo = [notification userInfo];
  if ([self isDebug]) printf("handleSimulationData (%s): toGPU = %s\n", [currentExperiment UTF8String], [[userInfo objectForKey: @"toGPU"] UTF8String]);
  
  // check if recording time course data
  if (currentExperiment) {
    NSDictionary *d = [dataSets objectForKey: currentExperiment];
    NSArray *timeCourse = [dataHandler timeCourseForName: [d objectForKey: @"name"]];
    if (timeCourse) {
      for (i = 0; i < [timeCourse count]; ++i) {
        double t = [[timeCourse objectAtIndex: i] doubleValue];
        if (TIME_EQUAL(t, currentTime)) {
          if ([self isDebug]) printf("record data at %f\n", t);
          [self recordDataAt: i];
          //break;
        }
      }
    }
  }
}

- (void)runCPUSimulation
{
}

- (void)doSimulationRun
{
  //printf("doSimulationRun\n");

  // register output events
  if (outputFrequency) {
    NSInvocation *anInv = [NSInvocation invocationWithMethodSignature: [self methodSignatureForSelector: @selector(performOutputSimulationData)]];
    [anInv setTarget: self];
    [anInv setSelector: @selector(performOutputSimulationData)];
    SimulationEvent *anEvent = [SimulationEvent eventWithInvocation: anInv atTime: 0 repeat: outputFrequency];
    [anEvent setRequireTransfer: YES];
    [self addSimulationEvent: anEvent];
  }

  if (gpuFuncs) [self runGPUSimulation: (getGPUFunctions *)gpuFuncs];
  else [self runCPUSimulation];

  //[[self getActivity] run];
  //return [[self getActivity] getStatus];
}

- (void)doSimulationRunForExperiment: (NSString *)expName
{
  // setup simulation experiment
  currentExperiment = expName;
  
  // register for notifications
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(handleSimulationData:)
                                               name:BioSwarmDidTransferDataNotification object:nil];
  
  // clear event schedule
  [self clearEventSchedule];

  // clear compare matrix
  int modelInstance;
  for (modelInstance = 0; modelInstance < [modelInstances count]; ++modelInstance) {
    NSDictionary *compareMatrices = [compareMatrixList objectAtIndex: modelInstance];
    BCDataMatrix *compareMatrix = [compareMatrices objectForKey: currentExperiment];
    int numCols = [compareMatrix numberOfColumns];
    int numRows = [compareMatrix numberOfRows];
    double (*CM)[numRows][numCols] = [compareMatrix dataMatrix];
    int j, k;
    for (j = 0; j < numRows; ++j)
      for (k = 0; k < numCols; ++k) (*CM)[j][k] = 0;
  }
  
  // apply perturbation?
  NSDictionary *anExp = [dataSets objectForKey: expName];
  NSString *p = [anExp objectForKey: @"perturbation"];
  [self setPerturbation: p];
  
  // record time course?
  
  [self doSimulationRun];
  
  // record final data
  if (currentExperiment) {
    NSDictionary *d = [dataSets objectForKey: currentExperiment];
    NSArray *timeCourse = [dataHandler timeCourseForName: [d objectForKey: @"name"]];
    if (!timeCourse) {
      NSDictionary *compareMatrices = [compareMatrixList objectAtIndex: 0];
      BCDataMatrix *compareMatrix = [compareMatrices objectForKey: currentExperiment];
      int numRows = [compareMatrix numberOfRows];
      int j;
      for (j = 0; j < numRows; ++j) [self recordDataAt: j];
    }
  }

  // remove notification
  [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void)cleanupSimulation
{
}

#if 0
#pragma mark == SIMULATION REPLAY ==
#endif

//
// Load simulation data for replay and analysis
//

- (int)numOfIterations { return numOfIterations; }
- (int)currentIteration { return currentIteration; }
- (void)setCurrentIteration: (int)aNum
{
  if (currentIteration == aNum) return;
  
  if (aNum < 0) aNum = 0;
  if (aNum > numOfIterations) aNum = numOfIterations;

  if (aNum > currentIteration) {
    // move forward
    while (currentIteration != aNum) [self nextIteration];
  } else {
    // move back
    [self rewindFiles];
    while (currentIteration != aNum) [self nextIteration];
  }
}

- (int)currentSpecies { return currentSpecies; }
- (void)setCurrentSpecies: (int)aNum { currentSpecies = aNum; }
- (float)speciesMin
{
#if 0
  float val;
  if (currentRange) val = currentMin[currentSpecies][currentIteration];
    else val = minSpecies[currentSpecies];
      //printf("speciesMin: (%d %d): %f\n", currentSpecies, currentIteration, val);
      return val;
#endif
  return 0.0;
}
- (float)speciesMax
{
#if 0
  float val;
  if (currentRange) val = currentMax[currentSpecies][currentIteration];
    else val = maxSpecies[currentSpecies];
      //printf("speciesMax: (%d %d): %f\n", currentSpecies, currentIteration, val);
      return val;
#endif
  return 0.0;
}
- (void)setCurrentRange: (int)aNum { currentRange = aNum; }

- (void)setupSimulationReplay
{
  [self allocateHierarchyWithEncode: [BioSwarmModel floatEncode]];
  useInitializeFile = NO;
  [self setupHierarchyDataFilesWithState: BCMODEL_READ_STATE];
  
  // determine the number of iterations
  numOfIterations = 0;
  BOOL more = YES;
  do {
    more = [self readHierarchyData];
    if (more) ++numOfIterations;
  } while (more);
  printf("numOfIterations: %d\n", numOfIterations);
  
  // start at first iteration
  [self rewindFiles];
  [self readHierarchyData];
}

- (void)rewindFiles
{
  currentIteration = 0;
  [self rewindFilesForModel: 0];
}

- (void)rewindFilesForModel:(int)modelIndex
{
  int i;
  
#if 0
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    [m rewindHierarchyFiles];
  }
#else
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  [modelObject rewindHierarchyFiles];
#endif
}

- (void)nextIteration
{
  if (currentIteration == numOfIterations) return;
  ++currentIteration;
  [self readHierarchyData];
}

#if 0
#pragma mark == RANDOM SEARCH ==
#endif

//
// Random search
//
- (NSArray *)randomParameters { return randomParameters; }
- (NSArray *)notRandomParameters { return notRandomParameters; }

#if 0
#pragma mark == PERTURBATIONS ==
#endif

- (BOOL)isPerturbation { return doPerturbation; }
- (NSString *)perturbationName { return perturbationName; }
- (NSDictionary *)perturbations { return perturbations; }

- (void)setPerturbation: (NSString *)pName
{
  if (pName) {
    if (![perturbations objectForKey: pName]) {
      printf("ERROR: Unknown perturbation: %s\n", [pName UTF8String]);
    } else {
      doPerturbation = YES;
      perturbationName = pName;
    }
  } else {
    doPerturbation = NO;
    perturbationName = nil;
  }
}

- (void)applyPerturbation: (NSString *)pName
{
  int i;
  
  NSDictionary *perturb = [perturbations objectForKey: pName];
  if (!perturb) {
    printf("ERROR: Unknown perturbation: %s\n", [pName UTF8String]);
    return;
  }
  
  for (i = 0; i < [modelInstances count]; ++i)
    [self applyPerturbation: pName forModel: i];
}

- (void)applyPerturbation: (NSString *)pName forModel: (int)modelIndex
{
  NSDictionary *perturb = [perturbations objectForKey: pName];
  NSString *mkey = [perturb objectForKey: @"model"];
  id topModel = [modelInstances objectAtIndex: modelIndex];
  //NSDictionary *modelSpec = [modelSpecifications objectAtIndex: modelIndex];
  id modelObject = [topModel modelWithKey: mkey];
  //NSDictionary *modelSet = [modelSpec objectForKey: @"models"];
  //NSArray *modelKeys = [modelSet allKeys];
  
  //NSUInteger idx = [modelKeys indexOfObject: mName];
  //id <BiologicalModel> aModel = [subModels objectAtIndex: idx];
  
  id param, val;
  int i;
  NSString *desc = [perturb objectForKey: @"description"];  
  if ([self isDebug]) printf("Applying perturbation (%s): %s\n", [pName UTF8String], [desc UTF8String]);
    
    // change (absolute) value of all species in a model
    param = [perturb objectForKey: @"value"];
    if (param) {
      if (![modelObject setValueForAllSpecies: [param doubleValue]]) {
        printf("WARNING: Could not apply perturbation (%s): value = %s\n", [pName UTF8String], [param UTF8String]);
      }
    }
  
  // change (absolute) value for specific species in a model
  param = [perturb objectForKey: @"values"];
  if (param) {
    NSArray *speciesKeys = [param allKeys];
    for (i = 0; i < [speciesKeys count]; ++i) {
      NSString *key = [speciesKeys objectAtIndex: i];
      val = [param objectForKey: key];
      if (![modelObject setValue: [val doubleValue] forSpecies: key]) {
        printf("WARNING: Could not apply perturbation (%s): change %s = %s\n", [pName UTF8String], [key UTF8String], [val UTF8String]);
      }
    }
  }
  
  // change (relative) value for specific species in a model
  param = [perturb objectForKey: @"adjust"];
  if (param) {
    NSArray *speciesKeys = [param allKeys];
    for (i = 0; i < [speciesKeys count]; ++i) {
      NSString *key = [speciesKeys objectAtIndex: i];
      val = [param objectForKey: key];
      if (![modelObject adjustValue: [val doubleValue] forSpecies: key]) {
        printf("WARNING: Could not apply perturbation (%s): adjust %s = %s\n", [pName UTF8String], [key UTF8String], [val UTF8String]);
      }
    }
  }

  // change parameters in a model
  param = [perturb objectForKey: @"parameters"];
  if (param) {
    NSArray *speciesKeys = [param allKeys];
    for (i = 0; i < [speciesKeys count]; ++i) {
      NSString *key = [speciesKeys objectAtIndex: i];
      val = [param objectForKey: key];
      if (![modelObject setValue: [val doubleValue] forParameter: key]) {
        printf("WARNING: Could not apply perturbation (%s): adjust %s = %s\n", [pName UTF8String], [key UTF8String], [val UTF8String]);
      }
    }
  }

  // knockout species
  param = [perturb objectForKey: @"knockout"];
  if (param) {
    for (i = 0; i < [param count]; ++i) {
      NSString *key = [param objectAtIndex: i];
      if (![modelObject knockoutSpecies: key]) {
        printf("WARNING: Could not apply perturbation (%s): knockout %s\n", [pName UTF8String], [key UTF8String]);
      }
    }
  }
}

@end

#if 0
#pragma mark == GPU ==
#endif

//
// GPU
//

@implementation BioSwarmController (GPURun)

- (void)setGPUFunctions: (getGPUFunctions *)func
{
  gpuFuncs = (void *)func;
}

- (getGPUFunctions *)gpuFunctions { return gpuFuncs; }
- (ModelGPUData **)gpuData { return gpuData; }
- (void)setGPUDevice: (int)aDevice { gpuDevice = aDevice; }

- (void)allocateHierarchyGPUData: (int)numInstances
{
  id aModel = [modelInstances objectAtIndex: 0];
  int numSubModels = [aModel numModelsInHierarchy];
  gpuData = (ModelGPUData **)malloc(sizeof(ModelGPUData *) * numSubModels);  
  [aModel allocateHierarchyGPUData: gpuData modelInstances: numInstances];
}

- (void)transferDataToGPU: (BOOL)aFlag
{
  NSNotificationCenter *noticeCenter = [NSNotificationCenter defaultCenter];
  int modelInstance;
  int numInstances = [modelInstances count];
  id aModel = [modelInstances objectAtIndex: 0];

  if (aFlag) {
    // transfer CPU data to GPU
    for (modelInstance = 0; modelInstance < numInstances; ++modelInstance) {
      id modelObject = [modelInstances objectAtIndex: modelInstance];
      [modelObject assignHierarchyDataGPUData: gpuData ofInstance: modelInstance toGPU: YES];
      [modelObject assignHierarchyParametersGPUData: gpuData ofInstance: modelInstance toGPU: YES];
    }
    [aModel transferHierarchyGPUData: gpuData toGPU: YES];
    
    if (respondsToDidTransfer) [delegate didTransferDataToGPU: YES sender: self];
    [noticeCenter postNotificationName: BioSwarmDidTransferDataNotification object: self userInfo: 
     [NSDictionary dictionaryWithObjectsAndKeys: @"YES", @"toGPU", nil]];
  } else {
    // transfer GPU data to CPU
    [aModel transferHierarchyGPUData: gpuData toGPU: NO];
    for (modelInstance = 0; modelInstance < numInstances; ++modelInstance) {
      id modelObject = [modelInstances objectAtIndex: modelInstance];
      [modelObject assignHierarchyDataGPUData: gpuData ofInstance: modelInstance toGPU: NO];
    }
    
    //printf("time = %f\n", currentTime);
    if (respondsToDidTransfer) [delegate didTransferDataToGPU: NO sender: self];
    [noticeCenter postNotificationName: BioSwarmDidTransferDataNotification object: self userInfo: 
     [NSDictionary dictionaryWithObjectsAndKeys: @"NO", @"toGPU", nil]];
  }
}
- (void)runGPUSimulation: (getGPUFunctions *)func
{
  int i, modelInstance;
  NSNotificationCenter *noticeCenter = [NSNotificationCenter defaultCenter];

  // global initialization
  (func)(-2, 0);
  
  // allocate CPU data
  if (isDoublePrecision)
    [self allocateHierarchyWithEncode: [BioSwarmModel doubleEncode]];
  else
    [self allocateHierarchyWithEncode: [BioSwarmModel floatEncode]];
  
  // initialize CPU data
  [self initializeHierarchy];
  
  // set the GPU device
  if (gpuDevice) (func)(-4, gpuDevice);
  
  // allocate GPU data
  int numInstances = [modelInstances count];
  id aModel = [modelInstances objectAtIndex: 0];
  //NSArray *subModels = [modelInstances objectAtIndex: 0];
  //id aModel = [subModels objectAtIndex: 0];
  [self allocateHierarchyGPUData: numInstances];
  
  // inform delegate that simulation finished initialization
  if (respondsToDidInit) [delegate didInitializeSimulation: self];
  
  double perturbStart = 0.0;
  if (doPerturbation) {
    NSDictionary *perturb = [perturbations objectForKey: perturbationName];
    NSString *s = [perturb objectForKey: @"perturbStart"];
    if (s) perturbStart = [s doubleValue];
  }
  
  // run it                                                                                                                                                                                        
  BOOL done = NO;
  double nextTime = 0;
  double nextOutput = 0;
  currentTime = 0;
  [self smallestDT];
  
  if (doPerturbation && (perturbStart == currentTime)) [self applyPerturbation: perturbationName];
  
  printf("time = %f, dt = %f\n", currentTime, deltaTime);
  [self transferDataToGPU: YES];

  // process any events
  [self processEventSchedule];

  // inform delegate that simulation will start executing
  if (respondsToWillRun) [delegate willRunSimulation: self];
  
  while (!done) {
    // determine next time for simulation
    nextOutput += outputFrequency;
    nextTime = [self nextEventTime];
    if (nextTime < 0) nextTime = nextOutput;

    if (doPerturbation) {
      if (TIME_EQUAL(perturbStart, currentTime) && (perturbStart > 0)) {
        [self applyPerturbation: perturbationName];
        
        // transfer CPU data to GPU
        for (modelInstance = 0; modelInstance < numInstances; ++modelInstance) {
          id modelObject = [modelInstances objectAtIndex: modelInstance];
          [modelObject assignHierarchyDataGPUData: gpuData ofInstance: modelInstance toGPU: YES];
          [modelObject assignHierarchyParametersGPUData: gpuData ofInstance: modelInstance toGPU: YES];
        }
        [aModel transferHierarchyGPUData: gpuData toGPU: YES];
        
        if (respondsToDidTransfer) [delegate didTransferDataToGPU: YES sender: self];
        [noticeCenter postNotificationName: BioSwarmDidTransferDataNotification object: self userInfo: 
         [NSDictionary dictionaryWithObjectsAndKeys: @"YES", @"toGPU", nil]];
      }
      
      if ((perturbStart > currentTime) && (perturbStart < nextTime)) nextTime = perturbStart;
    }
    if (nextTime > simulateTime) nextTime = simulateTime;
    
    // invoke GPU
    [aModel invokeHierarchyGPUData: gpuData currentTime: currentTime endTime: nextTime];
    
    // are we done?
    currentTime = nextTime;
    //if (currentTime >= simulateTime) done = YES;
    if (TIME_EQUAL(currentTime, simulateTime) || (currentTime >= simulateTime)) done = YES;
    printf("time = %f\n", currentTime);
    
    // process any events
    [self processEventSchedule];
  }
  
  // inform delegate that simulation has finished executing
  if (respondsToDidFinish) [delegate didFinishSimulation: self];
  
  // cleanup GPU data
  [aModel releaseHierarchyGPUData: gpuData];
  [aModel freeHierarchyGPUData: gpuData];
  free(gpuData);
  
  // GPU device reset
  (func)(-3, 0);
}

@end

//
// Visualization
//

@implementation BioSwarmController (Povray)

- (NSMutableString *)povrayCode
{
  return [self povrayCodeForModel: 0];
}

- (NSMutableString *)povrayCodeForModel:(int)modelIndex
{
  int i;
  
  NSMutableString *povray = [NSMutableString new];
  
  if (spatialModel) [povray appendString: [spatialModel povrayCode]];
  
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  [modelObject povrayHierarchyCode: povray];
  
  return povray;
}

@end
