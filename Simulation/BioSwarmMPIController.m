/*
 BioSwarmMPIController.m
 
 The top level controller for an MPI simulation run.
 
 Copyright (C) 2014 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: December 2014
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BioSwarmMPIController.h"
#import "SubcellularElementModel.h"

#include <mpi.h>

#import "internal.h"

@implementation BioSwarmMPIController

- initWithModel: (NSDictionary *)aModel andManager: anObj
{
  id param;
  int i, j;
  BOOL good = YES;
  
  [super initWithModel: aModel andManager: anObj];

  // assume models are in subspace
  [spatialModel setSubspace: YES];
  [spatialModel setSubspaceTranslator: self];

  // look for MPI settings
  NSDictionary *mpi = [aModel objectForKey: @"MPI"];
  CHECK_PARAMF(mpi, "MPI", good);
  if (!good) return nil;

  [model setObject: @"YES" forKey: @"isMPI"];

  NSArray *a = [mpi objectForKey: @"topologySize"];
  CHECK_PARAM(a, "topologySize");
  if ([a count] < 1) {
    printf("ERROR: Size of topologySize is not valid: %d\n", [a count]);
    return nil;
  }

  int dims = [spatialModel dimensions];
  if ([a count] != dims) {
    printf("ERROR: Dimensions of toplogySize does not match spatial dimensions, %ld != %d\n", (unsigned long)[a count], dims);
    exit(1);
  }

  topologySize = malloc(dims * sizeof(int));
  periodic = malloc(dims * sizeof(int));
  coordinates = malloc(dims * sizeof(int));
  for (i = 0; i < dims; ++i) {
    topologySize[i] = [[a objectAtIndex: i] intValue];
    periodic[i] = 0;
  }

  // initialize MPI
  int res = MPI_Init(NULL, NULL);

  // Create topology, allow reorder of ranks
  MPI_Cart_create(MPI_COMM_WORLD, dims, topologySize, periodic, 1, &topologyMPIComm);

  // get MPI rank
  res = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("my rank = %d, res = %d\n", rank, res);

  // coordinates
  MPI_Cart_coords(topologyMPIComm, rank, dims, coordinates);
  if (dims == 2) printf("rank = %d, coordinates = %d %d\n", rank, coordinates[0], coordinates[1]);
  else printf("rank = %d, coordinates = %d %d %d\n", rank, coordinates[0], coordinates[1], coordinates[2]);

  int src, dest;
  MPI_Cart_shift(topologyMPIComm, 0, 1, &src, &dest);
  printf("rank = %d, down, src = %d, dest = %d\n", rank, src, dest);
  MPI_Cart_shift(topologyMPIComm, 0, -1, &src, &dest);
  printf("rank = %d, up, src = %d, dest = %d\n", rank, src, dest);
  MPI_Cart_shift(topologyMPIComm, 1, 1, &src, &dest);
  printf("rank = %d, right, src = %d, dest = %d\n", rank, src, dest);
  MPI_Cart_shift(topologyMPIComm, 1, -1, &src, &dest);
  printf("rank = %d, left, src = %d, dest = %d\n", rank, src, dest);

  // cache ranks for neighbor MPI nodes
  for (i = S_CURRENT; i < S_XYZ_TOT; ++i) neighborRanks[i] = -1;
  neighborRanks[S_CURRENT] = rank;

  int maxNeighbors;
  if (dims == 2) maxNeighbors = S_XY_TOT;
  else maxNeighbors = S_XYZ_TOT;
  for (i = S_START; i < maxNeighbors; ++i) {
    int c[3];
    int nRank = MPI_PROC_NULL;
    bc_mpi_sector_coordinate_shift(coordinates[0], coordinates[1], coordinates[2], i, &(c[0]), &(c[1]), &(c[2]));
    if (c[0] < 0) { neighborRanks[i] = MPI_PROC_NULL; continue; }
    if (c[1] < 0) { neighborRanks[i] = MPI_PROC_NULL; continue; }
    if (c[2] < 0) { neighborRanks[i] = MPI_PROC_NULL; continue; }
    if (c[0] >= topologySize[0]) { neighborRanks[i] = MPI_PROC_NULL; continue; }
    if (c[1] >= topologySize[1]) { neighborRanks[i] = MPI_PROC_NULL; continue; }
    if (c[2] >= topologySize[2]) { neighborRanks[i] = MPI_PROC_NULL; continue; }

    MPI_Cart_rank(topologyMPIComm, c, &nRank);
    neighborRanks[i] = nRank;
    printf("rank = %d (%d, %d, %d), %s neighbor = %d (%d, %d, %d)\n", rank, coordinates[0], coordinates[1], coordinates[2],
	   bc_sector_direction_string(i), nRank, c[0], c[1], c[2]);
  }

  if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"initialize MPI rank = %d", rank] UTF8String]);

  return self;
}

- (void)dealloc
{
  if (topologySize) free(topologySize);

  [super dealloc];
}

- (int)rank { return rank; }
- (int *)neighborRanks { return neighborRanks; }
- (MPI_Comm)topologyMPICommunicator { return topologyMPIComm; }

- (NSString *)outputSuffix { return [NSString stringWithFormat: @"rank%d_%d", rank, outputRun]; }

- (BOOL)saveModelSpecification:(NSDictionary *)aModel forRun:(int)aNum
{
  // only one MPI process needs to save
  if (rank == 0) return [super saveModelSpecification: aModel forRun: aNum];
  return YES;
}

//
// Override core BioSwarmController methods to add MPI
//

- (void)allocateHierarchy: (int)modelIndex withEncode: (NSString *)anEncode
{
  // default
  [super allocateHierarchy: modelIndex withEncode: anEncode];

  // MPI
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  [modelObject allocateMPIHierarchyWithEncode: anEncode];
}

- (void)initializeHierarchy
{
  printf("initializeHierarchy\n");
  if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"start MPI init simulation, rank = %d", rank] UTF8String]);
  [super initializeHierarchy];
  if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"end MPI init simulation, rank = %d", rank] UTF8String]);

  // if initialized from file, allow specific MPI initialization                                                                                                                                                
  if (initializeFile) {
    printf("initializeHierarchy saveIF\n");
    NSInvocation *anInv = [NSInvocation invocationWithMethodSignature: [BioSwarmModel instanceMethodSignatureForSelector: @selector(initializeMPIFileCompleted)]];
    [anInv setSelector: @selector(initializeMPIFileCompleted)];
    [self performHierarchyInvocation: anInv];
  }
}

- (void)initializeHierarchyForModel:(int)modelIndex
{
  // default
  [super initializeHierarchyForModel: modelIndex];

  // MPI
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  [modelObject initializeMPIHierarchy];
}

//
// Translate coordinates from/to world and subspace
//
// MPI uses a flipped coordinate system where (y, x) == (0, 0) is in the
// upper left corner for a 2D system. (y, x, z) == (0, 0, 0) is in the
// upper left front corner for a 3D system.
//
// BioSwarm has coordinate system where (x, y, z) == (0, 0, 0) is in the
// lower left corner.
//
// Translating back/forth requires using both the flipped coordinate system
// and the proper coordinate index.
//

// tweak float precision
// if precision is lost in addition, out != in + a
// adjust by scaled epsilon to maintain correct inequality
static float tweak_precision(float in, double a)
{
  float out = in + a;
  int i;

  // out should be less than a
  if ((in < 0) && !(out < a)) {
    int scale = determine_scale_float(out);
    float tweak = 0.0000001;
    for (i = 0; i < scale; ++i) tweak *= 10;
    printf("WARNING: tweak float precision, %e + %lf !< %f, tweak %f - %f = %f \n", in, a, out, out, tweak, out - tweak);
    out = out - tweak;
    return out;
  }

  // out should be greater than a
  if ((in > 0) && !(out > a)) {
    int scale = determine_scale_float(out);
    float tweak = 0.0000001;
    for (i = 0; i < scale; ++i) tweak *= 10;
    printf("WARNING: tweak float precision, %e + %lf !> %f, tweak %f + %f = %f \n", in, a, out, out, tweak, out + tweak);
    out = out + tweak;
    return out;
  }

  return out;
}

static float tweak_precision_direction(float in, double a, BOOL roundUp)
{
  float out = in + a;
  float temp = out - a;
  int i;

  if (roundUp) {
    if (temp < in) { // error rounded down, we want up
      int scale = determine_scale_float(out);
      float tweak = 0.0000001;
      for (i = 0; i < scale; ++i) tweak *= 10;
      //printf("WARNING: tweak float precision (roundUp = %d), %e + %e !> %e, tweak %e + %e = %e \n", roundUp, in, a, out, out, tweak, out + tweak);
      out = out + tweak;
      return out;
    }
  } else {
    if (temp > in) { // error rounded up, we want down
      int scale = determine_scale_float(out);
      float tweak = 0.0000001;
      for (i = 0; i < scale; ++i) tweak *= 10;
      //printf("WARNING: tweak float precision (roundUp = %d), %e + %e !< %e, tweak %e - %e = %e \n", roundUp, in, a, out, out, tweak, out - tweak);
      out = out - tweak;
      return out;
    }
  }

  return out;
}

- (void)translateIntegerCoordinatesToWorld: (int *)localCoordinates forModel: aModel
{
  int *gridSize = [spatialModel gridSize];
  int i, dims = [spatialModel dimensions];

  localCoordinates[0] = coordinates[1] * gridSize[0] + localCoordinates[0];
  localCoordinates[1] = (topologySize[1] - coordinates[0] - 1) * gridSize[0] + localCoordinates[1];
  if (dims == 3) localCoordinates[2] = coordinates[2] * gridSize[2] + localCoordinates[2];
}

- (void)translateFloatCoordinatesToWorld: (float *)localCoordinates forModel: aModel
{
  double *domainSize = [spatialModel domainSize];
  int i, dims = [spatialModel dimensions];

  // single precision can be problematic so tweak with EPS if necessary
  localCoordinates[0] = tweak_precision(localCoordinates[0], coordinates[1] * domainSize[0]);
  localCoordinates[1] = tweak_precision(localCoordinates[1], (topologySize[1] - coordinates[0] - 1) * domainSize[0]);
  if (dims == 3) localCoordinates[2] = tweak_precision(localCoordinates[2], coordinates[2] * domainSize[2]);
}

- (void)translateFloatCoordinatesToWorld: (float *)localCoordinates forModel: aModel direction: (int)direction
{
  double *domainSize = [spatialModel domainSize];
  int i, dims = [spatialModel dimensions];

  // single precision can be problematic so tweak with EPS if necessary
  localCoordinates[0] = tweak_precision_direction(localCoordinates[0], coordinates[1] * domainSize[0], direction_round(direction, 0));
  localCoordinates[1] = tweak_precision_direction(localCoordinates[1], (topologySize[1] - coordinates[0] - 1) * domainSize[0], direction_round(direction, 1));
  if (dims == 3) localCoordinates[2] = tweak_precision_direction(localCoordinates[2], coordinates[2] * domainSize[2], direction_round(direction, 2));
}

- (void)translateDoubleCoordinatesToWorld: (double *)localCoordinates forModel: aModel
{
  double *domainSize = [spatialModel domainSize];
  int i, dims = [spatialModel dimensions];

  localCoordinates[0] = coordinates[1] * domainSize[0] + localCoordinates[0];
  localCoordinates[1] = (topologySize[1] - coordinates[0] - 1) * domainSize[0] + localCoordinates[1];
  if (dims == 3) localCoordinates[2] = coordinates[2] * domainSize[2] + localCoordinates[2];
}

- (void)translateIntegerCoordinatesToSubspace: (int *)worldCoordinates forModel: aModel
{
  int *gridSize = [spatialModel gridSize];
  int i, dims = [spatialModel dimensions];

  worldCoordinates[0] = worldCoordinates[0] - coordinates[1] * gridSize[0];
  worldCoordinates[1] = worldCoordinates[1] - (topologySize[1] - coordinates[0] - 1) * gridSize[1];
  if (dims == 3) worldCoordinates[2] = worldCoordinates[2] - coordinates[2] * gridSize[2];
}

- (void)translateFloatCoordinatesToSubspace: (float *)worldCoordinates forModel: aModel
{
  double *domainSize = [spatialModel domainSize];
  int i, dims = [spatialModel dimensions];

  worldCoordinates[0] = worldCoordinates[0] - coordinates[1] * domainSize[0];
  worldCoordinates[1] = worldCoordinates[1] - (topologySize[1] - coordinates[0] - 1) * domainSize[1];
  if (dims == 3) worldCoordinates[2] = worldCoordinates[2] - coordinates[2] * domainSize[2];
}

- (void)translateDoubleCoordinatesToSubspace: (double *)worldCoordinates forModel: aModel
{
  double *domainSize = [spatialModel domainSize];
  int i, dims = [spatialModel dimensions];

  worldCoordinates[0] = worldCoordinates[0] - coordinates[1] * domainSize[0];
  worldCoordinates[1] = worldCoordinates[1] - (topologySize[1] - coordinates[0] - 1) * domainSize[1];
  if (dims == 3) worldCoordinates[2] = worldCoordinates[2] - coordinates[2] * domainSize[2];
}

@end

//
// GPU methods
//

@implementation BioSwarmMPIController (GPURun)

- (void)allocateHierarchyGPUData: (int)numInstances
{
  // allocate standard GPU memory
  [super allocateHierarchyGPUData: numInstances];

  // allocate GPU-MPI memory
  id aModel = [modelInstances objectAtIndex: 0];
  int numSubModels = [aModel numModelsInHierarchy];
  [aModel allocateHierarchyGPUMPIData: gpuData modelInstances: numInstances];
}

@end

//
// MPI coordinates system
// fortran matrix style, x coordinate is rows (up, down), y coordinate is columns (left, right)
// origin in upper, left, front corner
//
void bc_mpi_sector_coordinate_shift(int xc, int yc, int zc, int direction, int *X, int *Y, int *Z)
{
  *X = xc;
  *Y = yc;
  *Z = zc;
  switch (direction) {
  case S_LEFT:
    *Y = *Y - 1; break;
  case S_RIGHT:
    *Y = *Y + 1; break;
  case S_UP:
    *X = *X - 1; break;
  case S_DOWN:
    *X = *X + 1; break;
  case S_LEFT_UP:
    *X = *X - 1; *Y = *Y - 1; break;
  case S_RIGHT_UP:
    *X = *X - 1; *Y = *Y + 1; break;
  case S_LEFT_DOWN:
    *X = *X + 1; *Y = *Y - 1; break;
  case S_RIGHT_DOWN:
    *X = *X + 1; *Y = *Y + 1; break;
  case S_FRONT:
    *Z = *Z - 1; break;
  case S_BACK:
    *Z = *Z + 1; break;
  case S_LEFT_FRONT:
    *Y = *Y - 1; *Z = *Z - 1; break;
  case S_RIGHT_FRONT:
    *Y = *Y + 1; *Z = *Z - 1; break;
  case S_LEFT_BACK:
    *Y = *Y - 1; *Z = *Z + 1; break;
  case S_RIGHT_BACK:
    *Y = *Y + 1; *Z = *Z + 1; break;
  case S_UP_FRONT:
    *X = *X - 1; *Z = *Z - 1; break;
  case S_DOWN_FRONT:
    *X = *X + 1; *Z = *Z - 1; break;
  case S_UP_BACK:
    *X = *X - 1; *Z = *Z + 1; break;
  case S_DOWN_BACK:
    *X = *X + 1; *Z = *Z + 1; break;
  case S_LEFT_UP_FRONT:
    *X = *X - 1; *Y = *Y - 1; *Z = *Z - 1; break;
  case S_RIGHT_UP_FRONT:
    *X = *X - 1; *Y = *Y + 1; *Z = *Z - 1; break;
  case S_LEFT_DOWN_FRONT:
    *X = *X + 1; *Y = *Y - 1; *Z = *Z - 1; break;
  case S_RIGHT_DOWN_FRONT:
    *X = *X + 1; *Y = *Y + 1; *Z = *Z - 1; break;
  case S_LEFT_UP_BACK:
    *X = *X - 1; *Y = *Y - 1; *Z = *Z + 1; break;
  case S_RIGHT_UP_BACK:
    *X = *X - 1; *Y = *Y + 1; *Z = *Z + 1; break;
  case S_LEFT_DOWN_BACK:
    *X = *X + 1; *Y = *Y - 1; *Z = *Z + 1; break;
  case S_RIGHT_DOWN_BACK:
    *X = *X + 1; *Y = *Y + 1; *Z = *Z + 1; break;
  }
}

//
// GPU-MPI data transfer interface functions
//

void BC_MPI_initiate_transfer(void *model, int mode, double currentTime, double nextTime)
{
  id modelObject = (id)model;
  int modelNumber = [modelObject modelNumber];
  id modelController = [modelObject modelController];
  ModelGPUData **gpuData = [modelController gpuData];

  printf("initiate transfer MPI data: %lf\n", currentTime);
  [modelObject initiateGPUMPIData: gpuData[modelNumber] mode: mode];
}

void BC_MPI_finalize_transfer(void *model, int mode, double currentTime, double nextTime)
{
  id modelObject = (id)model;

  printf("finalize transfer MPI data: %lf\n", currentTime);
  [modelObject finalizeGPUMPIData: NULL mode: mode];
}

void BC_MPI_record_time(void *model, const char *msg)
{
  id modelObject = (id)model;
  id modelController = [modelObject modelController];
  
  if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"%s, rank = %d", msg, [modelController rank]] UTF8String]);
}

//
// MPI enhancements to standard BioSwarmModel
//

@implementation BioSwarmModel (MPI)

- (void)allocateMPIHierarchyWithEncode:(NSString *)anEncode
{
  int i;

  printf("allocateMPIHierarchyWithEncode:\n");

  if ([anEncode isEqual: [BioSwarmModel intEncode]]) {
    encodeTag = BCENCODE_INT;
  } else if ([anEncode isEqual: [BioSwarmModel longEncode]]) {
    encodeTag = BCENCODE_LONG;
  } else if ([anEncode isEqual: [BioSwarmModel floatEncode]]) {
    encodeTag = BCENCODE_FLOAT;
  } else if ([anEncode isEqual: [BioSwarmModel doubleEncode]]) {
    encodeTag = BCENCODE_DOUBLE;
  } else {
    // throw exception or something
    NSLog(@"ERROR: BioSwarmModel unsupported encoding %@\n", anEncode);
    return;
  }
  dataEncode = [anEncode retain];

  if (numOfSpecies) {
    switch (encodeTag) {
    case BCENCODE_INT:
      speciesData = malloc(sizeof(int) * numOfSpecies);
      for (i = 0; i < numOfSpecies; ++i) ((int *)speciesData)[i] = 0;
      break;
    case BCENCODE_LONG:
      speciesData = malloc(sizeof(long) * numOfSpecies);
      for (i = 0; i < numOfSpecies; ++i) ((long *)speciesData)[i] = 0;
      break;
    case BCENCODE_FLOAT:
      speciesData = malloc(sizeof(float) * numOfSpecies);
      for (i = 0; i < numOfSpecies; ++i) ((float *)speciesData)[i] = 0;
      break;
    case BCENCODE_DOUBLE:
      speciesData = malloc(sizeof(double) * numOfSpecies);
      for (i = 0; i < numOfSpecies; ++i) ((double *)speciesData)[i] = 0;
      break;
    }
  }
  
  [self allocateMPISimulationWithEncode: anEncode];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] allocateMPIHierarchyWithEncode: anEncode];
}

- (void)initializeMPIHierarchy
{
  int i;
  printf("initializeMPIHierarchy\n");

  [self initializeMPISimulation];
  for (i = 0; i < [childModels count]; ++i)
    [[childModels objectAtIndex: i] initializeMPIHierarchy];
}

// these are implemented by subclasses
- (void)allocateMPISimulationWithEncode: (NSString *)anEncode
{
}

- (void)initializeMPISimulation
{
}

- (void)initializeMPIFileCompleted
{
}

@end

//
// SubcellularElementMethod
//
@implementation SubcellularElementModel (GPUMPIRun)

- (void)allocGPUMPIKernel: (void *)data
{
  ModelGPUData *gpuData = (ModelGPUData *)data;
  SEMfunctions *gpuFunctions = (SEMfunctions *)gpuData->gpuFunctions;
  SpatialModel *spatialModel = [modelController spatialModel];
  int dims = [spatialModel dimensions];
  int neighborSectorSize;
  if (dims == 2) neighborSectorSize = sectorsInDomain;
  else neighborSectorSize = sectorsInDomain*sectorsInDomain;

  gpuData->mpiData = (gpuFunctions->allocGPUMPIKernel)(self, gpuData->gpuPtrs, neighborSectorSize, sectorsInDomain, incomingSectors, outgoingSectors);
  printf("allocGPUMPIKernel: %d %d %p %p %p\n", neighborSectorSize, sectorsInDomain, gpuData, gpuFunctions, gpuData->mpiData);
}

- (void)transferGPUMPIKernel: (void *)data toGPU: (BOOL)aFlag
{
  printf("transferGPUMPIKernel:\n");
  ModelGPUData *gpuData = (ModelGPUData *)data;
  SEMfunctions *gpuFunctions = (SEMfunctions *)gpuData->gpuFunctions;

  printf("transferGPUMPIKernel: %d %p %p %p\n", aFlag, gpuData, gpuFunctions, gpuData->mpiData);
  (gpuFunctions->initGPUMPIKernel)(self, gpuData->mpiData, aFlag, gpuData->gpuPtrs, incomingElements, outgoingElements, totalElements);
}

- (void)initiateGPUMPIData: (void *)data mode: (int)aMode
{
  SpatialModel *spatialModel = [modelController spatialModel];
  int dims = [spatialModel dimensions];
  int *neighborRanks = [modelController neighborRanks];
  MPI_Comm comm = [modelController topologyMPICommunicator];
  int direction;
  int i, j, k;

  int neighborSize, neighborSectorSize;
  if (dims == 2) { neighborSize = S_XY_TOT; neighborSectorSize = sectorsInDomain; }
  else { neighborSize = S_XYZ_TOT; neighborSectorSize = sectorsInDomain*sectorsInDomain; }

  float (*oElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = (void *)outgoingElements;
  int (*oSectors)[neighborSectorSize][neighborSize] = (void *)outgoingSectors;
  float (*iElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = (void *)incomingElements;
  int (*iSectors)[neighborSectorSize][neighborSize] = (void *)incomingSectors;

  switch (aMode) {

  case BC_MPI_SEM_MODE_INIT: {
    printf("BC_MPI_SEM_MODE_INIT\n");
    if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"start MPI_INIT rank = %d", [modelController rank]] UTF8String]);

    // element map for each MPI neighbor
    elementMapping = (id *)malloc(neighborSize * sizeof(id));
    markedElements = (id *)malloc(neighborSize * sizeof(id));
    for (direction = S_START; direction < neighborSize; ++direction) {
      elementMapping[direction] = [NSMutableDictionary new];
      markedElements[direction] = [NSMutableDictionary new];
    }

    if (receiveRequests) free(receiveRequests);
    receiveRequests = malloc(neighborSize * sizeof(MPI_Request));
    if (sendRequests) free(sendRequests);
    sendRequests = malloc(neighborSize * sizeof(MPI_Request));
    MPI_Request (*recvRequest)[neighborSize] = receiveRequests;
    MPI_Request (*sendRequest)[neighborSize] = sendRequests;

    // initiate the data receive
    // max size plus 1 int for end of data flag
    void *receiveElements = malloc(sizeof(int) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize + sizeof(int));
    float (*rElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = (void *)receiveElements;

    for (direction = S_START; direction < neighborSize; ++direction) {
      int dataSize = 0;
      switch (direction) {
      case S_LEFT:
      case S_RIGHT:
      case S_UP:
      case S_DOWN:
      case S_FRONT:
      case S_BACK:
	dataSize = 4 * neighborSectorSize * maxElementsPerSector;
	break;

      case S_LEFT_UP:
      case S_RIGHT_UP:
      case S_LEFT_DOWN:
      case S_RIGHT_DOWN:
      case S_LEFT_FRONT:
      case S_RIGHT_FRONT:
      case S_LEFT_BACK:
      case S_RIGHT_BACK:
      case S_UP_FRONT:
      case S_DOWN_FRONT:
      case S_UP_BACK:
      case S_DOWN_BACK:
	if (dims == 2) dataSize = 4 * maxElementsPerSector;
	else dataSize = 4 * sectorsInDomain * maxElementsPerSector;
	break;

      case S_LEFT_UP_FRONT:
      case S_RIGHT_UP_FRONT:
      case S_LEFT_DOWN_FRONT:
      case S_RIGHT_DOWN_FRONT:
      case S_LEFT_UP_BACK:
      case S_RIGHT_UP_BACK:
      case S_LEFT_DOWN_BACK:
      case S_RIGHT_DOWN_BACK:
	dataSize = 4 * maxElementsPerSector;
	break;
      }
      ++dataSize; // end flag
      //printf("receive: rank = %d, %s neighbor rank = %d, size = %d\n", [modelController rank], bc_sector_direction_string(direction), neighborRanks[direction], dataSize);
      MPI_Irecv(&((*rElements)[direction][0]), dataSize, MPI_FLOAT, neighborRanks[direction], 1, comm, &(*recvRequest)[direction]);
    }

    // pack the elements
    // No GPU data yet so we use CPU data
    void *packElements = malloc(sizeof(int) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize + sizeof(int));
    float (*pElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = packElements;
    int currentPos[neighborSize];
    int (*seTable)[maxElementsPerSector][maxSectors] = (void *)sectorElementTable;

    // TODO: need to handle float/double switch
    Grid2DArray *X = [[elementData gridWithName: @"X"] objectForKey: @"matrix"];
    Grid2DArray *Y = [[elementData gridWithName: @"Y"] objectForKey: @"matrix"];
    Grid2DArray *Z = [[elementData gridWithName: @"Z"] objectForKey: @"matrix"];
    float (*xGrid)[maxElements] = [X getMatrix];
    float (*yGrid)[maxElements] = [Y getMatrix];
    float (*zGrid)[maxElements] = [Z getMatrix];
    float coords[3];

    for (direction = S_START; direction < neighborSize; ++direction) {
      currentPos[direction] = 0;
      for (i = 0; i < neighborSectorSize; ++i) {
	int sectorNum = (*oSectors)[i][direction];
	if (sectorNum == -1) continue;

	for (j = 0; j < maxElementsPerSector; ++j) {
	  int elemNum = (*seTable)[j][sectorNum];
	  if (elemNum == -1) continue;

	  // convert coordinates to world
	  coords[0] = (*xGrid)[elemNum]; coords[1] = (*yGrid)[elemNum]; coords[2] = (*zGrid)[elemNum];
	  if ([spatialModel isSubspace]) [spatialModel translateFloatCoordinatesToWorld: coords forModel: self direction: direction];

	  (*pElements)[direction][currentPos[direction]] = (float)elemNum;
	  (*pElements)[direction][currentPos[direction]+1] = coords[0];
	  (*pElements)[direction][currentPos[direction]+2] = coords[1];
	  (*pElements)[direction][currentPos[direction]+3] = coords[2];
	  currentPos[direction] += 4;
	}
      }
      printf("rank = %d, direction = %d, pack = %d\n", [modelController rank], direction, currentPos[direction] / 4);
      // add end of data flag
      (*pElements)[direction][currentPos[direction]] = -1.0;
      ++currentPos[direction];
    }

    // send the data
    for (direction = S_START; direction < neighborSize; ++direction) {
      MPI_Isend(&((*pElements)[direction][0]), currentPos[direction], MPI_FLOAT, neighborRanks[direction], 1, comm, &(*sendRequest)[direction]);
    }

    printf("rank = %d, wait on send\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*sendRequest)[S_START], MPI_STATUSES_IGNORE);
    printf("rank = %d, wait on receive\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*recvRequest)[S_START], MPI_STATUSES_IGNORE);

    // We assume the elements and sectors do not exist

    for (direction = S_START; direction < neighborSize; ++direction) {
      for (i = 0; i < neighborSectorSize * maxElementsPerSector; ++i) {
	if (neighborRanks[direction] == MPI_PROC_NULL) break;

	int elemNum = (*rElements)[direction][4*i];
	if (elemNum == -1) break;

	// convert coordinates to local subspace
	coords[0] = (*rElements)[direction][4*i+1];
	coords[1] = (*rElements)[direction][4*i+2];
	coords[2] = (*rElements)[direction][4*i+3];
	if ([spatialModel isSubspace]) [spatialModel translateFloatCoordinatesToSubspace: coords forModel: self];
	//if ([modelController rank] == 2) printf("remote (%d) = (%f %f %f), (%f %f %f)\n", elemNum, (*rElements)[direction][4*i+1], (*rElements)[direction][4*i+2], (*rElements)[direction][4*i+3], coords[0], coords[1], coords[2]);

	if (totalElements >= maxElements) {
	  printf("ERROR: exceeded maximum number of elements: %d >= %d\n", totalElements, maxElements);
	  abort();
	}

	// add element
        (*xGrid)[totalElements] = coords[0]; (*yGrid)[totalElements] = coords[1]; (*zGrid)[totalElements] = coords[2];
	[elementMapping[direction] setObject: [NSNumber numberWithInt: totalElements] forKey: [NSNumber numberWithInt: elemNum]];
        ++totalElements;
      }
      printf("rank = %d, direction = %d, elements = %d %f\n", [modelController rank], direction, i, (*rElements)[direction][0]);
    }

    free(receiveElements);
    free(packElements);

    if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"end MPI_INIT rank = %d", [modelController rank]] UTF8String]);
    break;
  }

  case BC_MPI_SEM_MODE_F1: {
    printf("BC_MPI_SEM_MODE_F1\n");
    if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"start MPI_F1 rank = %d", [modelController rank]] UTF8String]);

    if (receiveRequests) free(receiveRequests);
    receiveRequests = malloc(neighborSize * sizeof(MPI_Request));
    if (sendRequests) free(sendRequests);
    sendRequests = malloc(neighborSize * sizeof(MPI_Request));
    MPI_Request (*recvRequest)[neighborSize] = receiveRequests;
    MPI_Request (*sendRequest)[neighborSize] = sendRequests;

    //printf("pointers: %p %p %p %p\n", outgoingElements, outgoingSectors, incomingElements, incomingSectors);

    // receive data from MPI neighbors
    // we use non-blocking communication

    // max size plus 1 int for end of data flag
    void *receiveElements = malloc(sizeof(int) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize + sizeof(int));
    float (*rElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = (void *)receiveElements;

    for (direction = S_START; direction < neighborSize; ++direction) {
      int dataSize = 0;
      switch (direction) {
      case S_LEFT:
      case S_RIGHT:
      case S_UP:
      case S_DOWN:
      case S_FRONT:
      case S_BACK:
	dataSize = 4 * neighborSectorSize * maxElementsPerSector;
	break;

      case S_LEFT_UP:
      case S_RIGHT_UP:
      case S_LEFT_DOWN:
      case S_RIGHT_DOWN:
      case S_LEFT_FRONT:
      case S_RIGHT_FRONT:
      case S_LEFT_BACK:
      case S_RIGHT_BACK:
      case S_UP_FRONT:
      case S_DOWN_FRONT:
      case S_UP_BACK:
      case S_DOWN_BACK:
	if (dims == 2) dataSize = 4 * maxElementsPerSector;
	else dataSize = 4 * sectorsInDomain * maxElementsPerSector;
	break;

      case S_LEFT_UP_FRONT:
      case S_RIGHT_UP_FRONT:
      case S_LEFT_DOWN_FRONT:
      case S_RIGHT_DOWN_FRONT:
      case S_LEFT_UP_BACK:
      case S_RIGHT_UP_BACK:
      case S_LEFT_DOWN_BACK:
      case S_RIGHT_DOWN_BACK:
	dataSize = 4 * maxElementsPerSector;
	break;
      }
      ++dataSize; // end flag
      printf("receive: rank = %d, %s neighbor rank = %d, size = %d\n", [modelController rank], bc_sector_direction_string(direction), neighborRanks[direction], dataSize);
      MPI_Irecv(&((*rElements)[direction][0]), dataSize, MPI_FLOAT, neighborRanks[direction], 1, comm, &(*recvRequest)[direction]);
    }

    // gather data from GPU
    [self transferGPUMPIKernel: data toGPU: NO];

    // max size plus one int for end marker
    int currentPos[neighborSize];
    void *packElements = malloc(sizeof(int) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize + sizeof(int));
    float (*pElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = packElements;
    NSMutableArray *openElements = [NSMutableArray array];

    for (direction = S_START; direction < neighborSize; ++direction) {
      currentPos[direction] = 0;
      for (i = 0; i < neighborSectorSize; ++i) {
	int sectorNum = (*oSectors)[i][direction];
	if (sectorNum == -1) continue;

	int idx = 4 * i * maxElementsPerSector;
	for (j = 0; j < maxElementsPerSector; ++j) {
	  int elemNum = (int)(*oElements)[direction][idx+4*j];
	  if (elemNum == -1) continue;

	  // found element
	  [openElements addObject: [NSNumber numberWithInt: elemNum]];
	  (*pElements)[direction][currentPos[direction]] = (float)elemNum;
	  (*pElements)[direction][currentPos[direction]+1] = (*oElements)[direction][idx+4*j+1];
	  (*pElements)[direction][currentPos[direction]+2] = (*oElements)[direction][idx+4*j+2];
	  (*pElements)[direction][currentPos[direction]+3] = (*oElements)[direction][idx+4*j+3];
	  currentPos[direction] += 4;
	}
      }
      printf("rank = %d, direction = %d, pack = %d\n", [modelController rank], direction, currentPos[direction] / 4);
      (*pElements)[direction][currentPos[direction]] = -1.0;
      ++currentPos[direction];
    }
    printf("rank = %d, total pack = %d\n", [modelController rank], [openElements count]);

    // send data to MPI neighbors
    for (direction = S_START; direction < neighborSize; ++direction) {
      MPI_Isend(&((*pElements)[direction][0]), currentPos[direction], MPI_FLOAT, neighborRanks[direction], 1, comm, &(*sendRequest)[direction]);
    }

    printf("rank = %d, wait on send\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*sendRequest)[S_START], MPI_STATUSES_IGNORE);
    printf("rank = %d, wait on receive\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*recvRequest)[S_START], MPI_STATUSES_IGNORE);

    //  for (direction = S_START; direction < neighborSize; ++direction) {
    // printf("rank = %d, receive (%d, %d) = %f\n", [modelController rank], direction, neighborRanks[direction], (*iElements)[direction][0]);
    //}

    // Extract elements and copy data for GPU transfer
    // This data is not in any sector order, but we do not care because
    // we just update data based upon the element number.
    // The sector numbers do not change for F1.
    for (direction = S_START; direction < neighborSize; ++direction) {
      currentPos[direction] = 0;
      for (i = 0; i < 4 * neighborSectorSize * maxElementsPerSector; ++i) (*iElements)[direction][i] = -1;

      for (i = 0; i < neighborSectorSize * maxElementsPerSector; ++i) {
	if (neighborRanks[direction] == MPI_PROC_NULL) break;

	int remoteElem = (*rElements)[direction][4*i];
	if (remoteElem == -1) break;

	// search for element in map
	NSNumber *n = [elementMapping[direction] objectForKey: [NSNumber numberWithInt: remoteElem]];
	if (!n) printf("MPI_ERROR: could not find matching element in map (%d, %f, %f, %f)\n", remoteElem, (*rElements)[direction][4*i+1], (*rElements)[direction][4*i+2], (*rElements)[direction][4*i+3]);
	else {
	  // copy data for GPU transfer
	  int elemNum = [n intValue];
	  (*iElements)[direction][currentPos[direction]] = (float)elemNum;
	  (*iElements)[direction][currentPos[direction]+1] = (*rElements)[direction][4*i+1];
	  (*iElements)[direction][currentPos[direction]+2] = (*rElements)[direction][4*i+2];
	  (*iElements)[direction][currentPos[direction]+3] = (*rElements)[direction][4*i+3];
	  currentPos[direction] += 4;

	  if (elemNum == 308) printf("rank = %d, incoming element map %d = %d, %f, %f, %f\n", [modelController rank], elemNum, remoteElem, (*rElements)[direction][4*i+1], (*rElements)[direction][4*i+2], (*rElements)[direction][4*i+3]);
	}
      }
      //printf("rank = %d, direction = %d\n", [modelController rank], direction);
    }

    // transfer data to GPU
    [self transferGPUMPIKernel: data toGPU: YES];

    free(receiveElements);
    free(packElements);

    if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"end MPI_F1 rank = %d", [modelController rank]] UTF8String]);
    break;
  }

  case BC_MPI_SEM_MODE_MOVE_READ_ONLY: {
    printf("BC_MPI_SEM_MODE_MOVE_READ_ONLY\n");
    if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"start MPI_MOVE_READ_ONLY rank = %d", [modelController rank]] UTF8String]);

    if (receiveRequests) free(receiveRequests);
    receiveRequests = malloc(neighborSize * sizeof(MPI_Request));
    if (sendRequests) free(sendRequests);
    sendRequests = malloc(neighborSize * sizeof(MPI_Request));
    MPI_Request (*recvRequest)[neighborSize] = receiveRequests;
    MPI_Request (*sendRequest)[neighborSize] = sendRequests;

    // max size plus 1 int for end of data flag
    void *receiveElements = malloc(sizeof(int) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize + sizeof(int));
    float (*rElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = (void *)receiveElements;

    for (direction = S_START; direction < neighborSize; ++direction) {
      //printf("receive: rank = %d, %s neighbor rank = %d, %p\n", [modelController rank], bc_sector_direction_string(direction), neighborRanks[direction], &((*iElements)[direction][0]));
      int dataSize = 0;
      switch (direction) {
      case S_LEFT:
      case S_RIGHT:
      case S_UP:
      case S_DOWN:
      case S_FRONT:
      case S_BACK:
	dataSize = 4 * neighborSectorSize * maxElementsPerSector;
	break;

      case S_LEFT_UP:
      case S_RIGHT_UP:
      case S_LEFT_DOWN:
      case S_RIGHT_DOWN:
      case S_LEFT_FRONT:
      case S_RIGHT_FRONT:
      case S_LEFT_BACK:
      case S_RIGHT_BACK:
      case S_UP_FRONT:
      case S_DOWN_FRONT:
      case S_UP_BACK:
      case S_DOWN_BACK:
	if (dims == 2) dataSize = 4 * maxElementsPerSector;
	else dataSize = 4 * sectorsInDomain * maxElementsPerSector;
	break;

      case S_LEFT_UP_FRONT:
      case S_RIGHT_UP_FRONT:
      case S_LEFT_DOWN_FRONT:
      case S_RIGHT_DOWN_FRONT:
      case S_LEFT_UP_BACK:
      case S_RIGHT_UP_BACK:
      case S_LEFT_DOWN_BACK:
      case S_RIGHT_DOWN_BACK:
	dataSize = 4 * maxElementsPerSector;
	break;
      }
      ++dataSize; // end flag
      MPI_Irecv(&((*rElements)[direction][0]), dataSize, MPI_FLOAT, neighborRanks[direction], 1, comm, &(*recvRequest)[direction]);
    }

    // gather data from GPU
    [self transferGPUMPIKernel: data toGPU: NO];

    // pack data
    // max size plus one int for end marker
    int currentPos[neighborSize];
    void *packElements = malloc(sizeof(int) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize + sizeof(int));
    float (*pElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = packElements;
    float coords[3];

    for (direction = S_START; direction < neighborSize; ++direction) {
      currentPos[direction] = 0;
      for (i = 0; i < neighborSectorSize; ++i) {
	int sectorNum = (*oSectors)[i][direction];
	if (sectorNum == -1) continue;

	int idx = 4 * i * maxElementsPerSector;
	for (j = 0; j < maxElementsPerSector; ++j) {
	  int elemNum = (int)(*oElements)[direction][idx+4*j];
	  if (elemNum == -1) continue;

	  // convert coordinates to world
	  coords[0] = (*oElements)[direction][idx+4*j+1]; coords[1] = (*oElements)[direction][idx+4*j+2]; coords[2] = (*oElements)[direction][idx+4*j+3];
	  if ([spatialModel isSubspace]) [spatialModel translateFloatCoordinatesToWorld: coords forModel: self direction: direction];

	  printf("MOVE_READ_ONLY: rank = %d, direction = %d, send element %d, %.15f, %.15f, %.15f (%.15f, %.15f, %.15f)\n", [modelController rank], direction, elemNum, coords[0], coords[1], coords[2], (*oElements)[direction][idx+4*j+1], (*oElements)[direction][idx+4*j+2], (*oElements)[direction][idx+4*j+3]);

	  // found element
	  (*pElements)[direction][currentPos[direction]] = (float)elemNum;
	  (*pElements)[direction][currentPos[direction]+1] = coords[0];
	  (*pElements)[direction][currentPos[direction]+2] = coords[1];
	  (*pElements)[direction][currentPos[direction]+3] = coords[2];
	  currentPos[direction] += 4;
	}
      }
      printf("MOVE_READ_ONLY: rank = %d, direction = %d, pack = %d\n", [modelController rank], direction, currentPos[direction] / 4);
      (*pElements)[direction][currentPos[direction]] = -1.0;
      ++currentPos[direction];
    }

    // send data to MPI neighbors

    for (direction = S_START; direction < neighborSize; ++direction) {
      //printf("send: rank = %d, %s neighbor rank = %d\n", [modelController rank], bc_sector_direction_string(direction), neighborRanks[direction]);
      MPI_Isend(&((*pElements)[direction][0]), currentPos[direction], MPI_FLOAT, neighborRanks[direction], 1, comm, &(*sendRequest)[direction]);
    }

    printf("MOVE_READ_ONLY: rank = %d, wait on send\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*sendRequest)[S_START], MPI_STATUSES_IGNORE);
    printf("MOVE_READ_ONLY: rank = %d, wait on receive\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*recvRequest)[S_START], MPI_STATUSES_IGNORE);

    //
    // Given a set of elements that have moved from a read-only border sector
    // into a local sector. Each element has essentially migrated from one
    // MPI node to another, it was under our neighbor's control before but
    // now we control it. As an element in a read-only border sector, it
    // has an entry in the element map and it has a local element number.
    // We remove it from the element map, let it keep its local element
    // number, and update the sector tables to include that element.
    //
    for (direction = S_START; direction < neighborSize; ++direction)
      [markedElements[direction] removeAllObjects];

    for (direction = S_START; direction < neighborSize; ++direction) {
      currentPos[direction] = 0;
      for (i = 0; i < 4 * neighborSectorSize * maxElementsPerSector; ++i) (*iElements)[direction][i] = -1;

      for (i = 0; i < neighborSectorSize * maxElementsPerSector; ++i) {
	if (neighborRanks[direction] == MPI_PROC_NULL) break;

	int remoteElem = (*rElements)[direction][4*i];
	if (remoteElem == -1) break;

	// search for element in map
	NSNumber *rn =  [NSNumber numberWithInt: remoteElem];
	NSNumber *n = [elementMapping[direction] objectForKey: rn];
	if (!n) printf("MPI_ERROR: could not find matching element in map (%d, %f, %f, %f)\n", remoteElem, (*rElements)[direction][4*i+1], (*rElements)[direction][4*i+2], (*rElements)[direction][4*i+3]);
	else {
	  // convert coordinates to local subspace
	  coords[0] = (*rElements)[direction][4*i+1];
	  coords[1] = (*rElements)[direction][4*i+2];
	  coords[2] = (*rElements)[direction][4*i+3];
	  if ([spatialModel isSubspace]) [spatialModel translateFloatCoordinatesToSubspace: coords forModel: self];

	  // copy data for GPU transfer
	  int elemNum = [n intValue];
	  (*iElements)[direction][currentPos[direction]] = (float)elemNum;
	  (*iElements)[direction][currentPos[direction]+1] = coords[0];
	  (*iElements)[direction][currentPos[direction]+2] = coords[1];
	  (*iElements)[direction][currentPos[direction]+3] = coords[2];
	  currentPos[direction] += 4;

	  [markedElements[direction] setObject: n forKey: rn];
	  [elementMapping[direction] removeObjectForKey: rn];
	  printf("MOVE_READ_ONLY: rank = %d, direction = %d, add incoming element %d = %d, %f, %f, %f\n", [modelController rank], direction, elemNum, remoteElem, (*rElements)[direction][4*i+1], (*rElements)[direction][4*i+2], (*rElements)[direction][4*i+3]);
	}
      }
      //printf("rank = %d, direction = %d\n", [modelController rank], direction);
    }

    //
    // On the sending side, the MPI node has lost control of the element
    // so that local element number is available, however that element is
    // not really deleted because we expect is to show up in our neighbor
    // read-only sector border list. But we do not know the new element
    // number that will be assigned in our MPI neighbor. We need to
    // let our neighbors know their new element numbers so they can add to their map.
    //

    for (direction = S_START; direction < neighborSize; ++direction) {
      //printf("receive: rank = %d, %s neighbor rank = %d, %p\n", [modelController rank], bc_sector_direction_string(direction), neighborRanks[direction], &((*iElements)[direction][0]));
      int dataSize = 0;
      switch (direction) {
      case S_LEFT:
      case S_RIGHT:
      case S_UP:
      case S_DOWN:
      case S_FRONT:
      case S_BACK:
	dataSize = 4 * neighborSectorSize * maxElementsPerSector;
	break;

      case S_LEFT_UP:
      case S_RIGHT_UP:
      case S_LEFT_DOWN:
      case S_RIGHT_DOWN:
      case S_LEFT_FRONT:
      case S_RIGHT_FRONT:
      case S_LEFT_BACK:
      case S_RIGHT_BACK:
      case S_UP_FRONT:
      case S_DOWN_FRONT:
      case S_UP_BACK:
      case S_DOWN_BACK:
	if (dims == 2) dataSize = 4 * maxElementsPerSector;
	else dataSize = 4 * sectorsInDomain * maxElementsPerSector;
	break;

      case S_LEFT_UP_FRONT:
      case S_RIGHT_UP_FRONT:
      case S_LEFT_DOWN_FRONT:
      case S_RIGHT_DOWN_FRONT:
      case S_LEFT_UP_BACK:
      case S_RIGHT_UP_BACK:
      case S_LEFT_DOWN_BACK:
      case S_RIGHT_DOWN_BACK:
	dataSize = 4 * maxElementsPerSector;
	break;
      }
      ++dataSize; // end flag
      MPI_Irecv(&((*rElements)[direction][0]), dataSize, MPI_FLOAT, neighborRanks[direction], 1, comm, &(*recvRequest)[direction]);
    }

    // pack data
    for (direction = S_START; direction < neighborSize; ++direction) {
      currentPos[direction] = 0;

      NSArray *allKeys = [markedElements[direction] allKeys];
      printf("MOVE_READ_ONLY: rank = %d, direction = %d, send elements for map = %d\n", [modelController rank], direction, [allKeys count]);
      for (i = 0; i < [allKeys count]; ++i) {
	NSNumber *rn = [allKeys objectAtIndex: i];
	(*pElements)[direction][currentPos[direction]] = [rn floatValue];
	(*pElements)[direction][currentPos[direction]+1] = [[markedElements[direction] objectForKey: rn] floatValue];
	currentPos[direction] += 2;
      }
      (*pElements)[direction][currentPos[direction]] = -1.0;
      ++currentPos[direction];
    }

    // send data to MPI neighbors

    for (direction = S_START; direction < neighborSize; ++direction) {
      MPI_Isend(&((*pElements)[direction][0]), currentPos[direction], MPI_FLOAT, neighborRanks[direction], 1, comm, &(*sendRequest)[direction]);
    }

    printf("rank = %d, wait on send\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*sendRequest)[S_START], MPI_STATUSES_IGNORE);
    printf("rank = %d, wait on receive\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*recvRequest)[S_START], MPI_STATUSES_IGNORE);

    // update element maps
    for (direction = S_START; direction < neighborSize; ++direction) {
      for (i = 0; i < neighborSectorSize * maxElementsPerSector; ++i) {
	if (neighborRanks[direction] == MPI_PROC_NULL) break;

	int elemNum = (*rElements)[direction][2*i];
	if (elemNum == -1) break;
	int remoteElem = (*rElements)[direction][2*i+1];
	[elementMapping[direction] setObject: [NSNumber numberWithInt: elemNum] forKey: [NSNumber numberWithInt: remoteElem]];
	printf("MOVE_READ_ONLY: rank = %d, direction = %d, add element map %d = %d\n", [modelController rank], direction, remoteElem, elemNum);
      }
    }

    // transfer data to GPU
    [self transferGPUMPIKernel: data toGPU: YES];

    free(receiveElements);
    free(packElements);

    if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"end MPI_MOVE_READ_ONLY rank = %d", [modelController rank]] UTF8String]);
    break;
  }

  case BC_MPI_SEM_MODE_MOVE: {
    printf("BC_MPI_SEM_MODE_MOVE\n");
    if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"start MPI_MOVE rank = %d", [modelController rank]] UTF8String]);

    if (receiveRequests) free(receiveRequests);
    receiveRequests = malloc(neighborSize * sizeof(MPI_Request));
    if (sendRequests) free(sendRequests);
    sendRequests = malloc(neighborSize * sizeof(MPI_Request));
    MPI_Request (*recvRequest)[neighborSize] = receiveRequests;
    MPI_Request (*sendRequest)[neighborSize] = sendRequests;

    // max size plus 1 int for end of data flag
    void *receiveElements = malloc(sizeof(int) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize + sizeof(int));
    float (*rElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = (void *)receiveElements;

    for (direction = S_START; direction < neighborSize; ++direction) {
      //printf("receive: rank = %d, %s neighbor rank = %d, %p\n", [modelController rank], bc_sector_direction_string(direction), neighborRanks[direction], &((*iElements)[direction][0]));
      int dataSize = 0;
      switch (direction) {
      case S_LEFT:
      case S_RIGHT:
      case S_UP:
      case S_DOWN:
      case S_FRONT:
      case S_BACK:
	dataSize = 4 * neighborSectorSize * maxElementsPerSector;
	break;

      case S_LEFT_UP:
      case S_RIGHT_UP:
      case S_LEFT_DOWN:
      case S_RIGHT_DOWN:
      case S_LEFT_FRONT:
      case S_RIGHT_FRONT:
      case S_LEFT_BACK:
      case S_RIGHT_BACK:
      case S_UP_FRONT:
      case S_DOWN_FRONT:
      case S_UP_BACK:
      case S_DOWN_BACK:
	if (dims == 2) dataSize = 4 * maxElementsPerSector;
	else dataSize = 4 * sectorsInDomain * maxElementsPerSector;
	break;

      case S_LEFT_UP_FRONT:
      case S_RIGHT_UP_FRONT:
      case S_LEFT_DOWN_FRONT:
      case S_RIGHT_DOWN_FRONT:
      case S_LEFT_UP_BACK:
      case S_RIGHT_UP_BACK:
      case S_LEFT_DOWN_BACK:
      case S_RIGHT_DOWN_BACK:
	dataSize = 4 * maxElementsPerSector;
	break;
      }
      ++dataSize; // end flag
      MPI_Irecv(&((*rElements)[direction][0]), dataSize, MPI_FLOAT, neighborRanks[direction], 1, comm, &(*recvRequest)[direction]);
    }

    // transfer data from GPU
    [self transferGPUMPIKernel: data toGPU: NO];

    // pack data
    // max size plus one int for end marker
    int currentPos[neighborSize];
    void *packElements = malloc(sizeof(int) * 4 * neighborSectorSize * maxElementsPerSector * neighborSize + sizeof(int));
    float (*pElements)[neighborSize][4 * neighborSectorSize * maxElementsPerSector] = packElements;
    float coords[3];

    for (direction = S_START; direction < neighborSize; ++direction) {
      currentPos[direction] = 0;
      for (i = 0; i < neighborSectorSize; ++i) {
	int sectorNum = (*oSectors)[i][direction];
	if (sectorNum == -1) continue;

	int idx = 4 * i * maxElementsPerSector;
	for (j = 0; j < maxElementsPerSector; ++j) {
	  int elemNum = (int)(*oElements)[direction][idx+4*j];
	  if (elemNum == -1) continue;

	  // convert coordinates to world
	  coords[0] = (*oElements)[direction][idx+4*j+1]; coords[1] = (*oElements)[direction][idx+4*j+2]; coords[2] = (*oElements)[direction][idx+4*j+3];
	  if ([spatialModel isSubspace]) [spatialModel translateFloatCoordinatesToWorld: coords forModel: self direction: direction];

	  // found element
	  (*pElements)[direction][currentPos[direction]] = (float)elemNum;
	  (*pElements)[direction][currentPos[direction]+1] = coords[0];
	  (*pElements)[direction][currentPos[direction]+2] = coords[1];
	  (*pElements)[direction][currentPos[direction]+3] = coords[2];
	  currentPos[direction] += 4;
	}
      }
      printf("BC_MPI_SEM_MODE_MOVE: rank = %d, direction = %d, pack = %d\n", [modelController rank], direction, currentPos[direction] / 4);
      (*pElements)[direction][currentPos[direction]] = -1.0;
      ++currentPos[direction];
    }

    // send data to MPI neighbors

    for (direction = S_START; direction < neighborSize; ++direction) {
      //printf("send: rank = %d, %s neighbor rank = %d, size = %d\n", [modelController rank], bc_sector_direction_string(direction), neighborRanks[direction], currentPos[direction]);
      MPI_Isend(&((*pElements)[direction][0]), currentPos[direction], MPI_FLOAT, neighborRanks[direction], 1, comm, &(*sendRequest)[direction]);
    }

    printf("rank = %d, wait on send\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*sendRequest)[S_START], MPI_STATUSES_IGNORE);
    printf("rank = %d, wait on receive\n", [modelController rank]);
    MPI_Waitall(neighborSize - 1, &(*recvRequest)[S_START], MPI_STATUSES_IGNORE);

    //
    // Given the set of elements for the read-only border sectors, need to
    // determine elements in four categories.
    // (1) elements that stayed in sector
    // (2) elements moved from local sector to read-only border
    // (3) elements moved out of read-only border (removed)
    // (4) new elements in read-only border (insert)
    //
    // to determine these categores
    // (1) exists in element map
    // (2) exists in element map, put in map by MOVE_READ_ONLY abve
    // (3) exists in element map, not in given element set
    // (4) does not exist in element map
    //

    NSMutableArray *insertElements[neighborSize];
    for (direction = S_START; direction < neighborSize; ++direction) {
      NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary: elementMapping[direction]];
      insertElements[direction] = [NSMutableArray array];
      
      currentPos[direction] = 0;
      for (i = 0; i < 4 * neighborSectorSize * maxElementsPerSector; ++i) (*iElements)[direction][i] = -1;

      for (i = 0; i < neighborSectorSize * maxElementsPerSector; ++i) {
	if (neighborRanks[direction] == MPI_PROC_NULL) break;

	int remoteElem = (*rElements)[direction][4*i];
	if (remoteElem == -1) break;

	// search for element in map
	NSNumber *rn = [NSNumber numberWithInt: remoteElem];
	NSNumber *n = [d objectForKey: rn];
	if (n) {
	  // existing element
	  [d removeObjectForKey: rn];
	  //printf("rank = %d, existing incoming element %d = %d\n", [modelController rank], [n intValue], [rn intValue]);
	} else {
	  // new element, save for later
	  [insertElements[direction] addObject: rn];
	}
      }

      // any remaining elements in map are category (3) and removed
      // these element numbers are available for reassignment
      NSArray *allKeys = [d allKeys];
      for (i = 0; i < [allKeys count]; ++i) {
	NSNumber *rn = [allKeys objectAtIndex: i];
	NSNumber *n = [d objectForKey: rn];
	[availableElements addObject: n];
	[elementMapping[direction] removeObjectForKey: rn];
	printf("MOVE: rank = %d, remove incoming element %d = %d\n", [modelController rank], [n intValue], [rn intValue]);
      }

      // assign new elements a local element number
      for (i = 0; i < [insertElements[direction] count]; ++i) {
	NSNumber *rn = [insertElements[direction] objectAtIndex: i];
	if ([availableElements count] > 0) {
	  // grab an available elements
	  NSNumber *n = [availableElements lastObject];
	  [availableElements removeLastObject];
	  [elementMapping[direction] setObject: n forKey: rn];
	} else {
	  // no available elements so add to end
	  [elementMapping[direction] setObject: [NSNumber numberWithInt: totalElements] forKey: rn];
	  ++totalElements;
	}
	printf("MOVE: rank = %d, direction = %d, add incoming element %d = %d\n", [modelController rank], direction, [[elementMapping[direction] objectForKey: rn] intValue], [rn intValue]);
      }

      // now that we have the element numbers resolved
      // copy data for transfer down to GPU
      for (i = 0; i < neighborSectorSize * maxElementsPerSector; ++i) {
	if (neighborRanks[direction] == MPI_PROC_NULL) break;

	int remoteElem = (*rElements)[direction][4*i];
	if (remoteElem == -1) break;

	// search for element in map
	NSNumber *rn = [NSNumber numberWithInt: remoteElem];
	NSNumber *n = [elementMapping[direction] objectForKey: rn];
	if (!n) printf("MPI_ERROR: could not find matching element in map (%d, %f, %f, %f)\n", remoteElem, (*rElements)[direction][4*i+1], (*rElements)[direction][4*i+2], (*rElements)[direction][4*i+3]);
	else {
	  // convert coordinates to local subspace
	  coords[0] = (*rElements)[direction][4*i+1];
	  coords[1] = (*rElements)[direction][4*i+2];
	  coords[2] = (*rElements)[direction][4*i+3];
	  if ([spatialModel isSubspace]) [spatialModel translateFloatCoordinatesToSubspace: coords forModel: self];

	  // copy data for GPU transfer
	  int elemNum = [n intValue];
	  (*iElements)[direction][currentPos[direction]] = (float)elemNum;
	  (*iElements)[direction][currentPos[direction]+1] = coords[0];
	  (*iElements)[direction][currentPos[direction]+2] = coords[1];
	  (*iElements)[direction][currentPos[direction]+3] = coords[2];
	  if ([insertElements[direction] containsObject: rn])
	    printf("MOVE: rank = %d, direction = %d, incoming element %d, %.15f %.15f %.15f (%.15f %.15f %.15f)\n", [modelController rank], direction, elemNum, coords[0], coords[1], coords[2],
		   (*rElements)[direction][4*i+1], (*rElements)[direction][4*i+2], (*rElements)[direction][4*i+3]);

	  currentPos[direction] += 4;
	}
      }
      //printf("rank = %d, direction = %d\n", [modelController rank], direction);
    }

    // transfer data to GPU
    [self transferGPUMPIKernel: data toGPU: YES];

    free(receiveElements);
    free(packElements);

    if (TIME_PROFILE) bc_record_time([[NSString stringWithFormat: @"end MPI_MOVE rank = %d", [modelController rank]] UTF8String]);
    break;
  }

  default:
    printf("ERROR: Unknown SubcellularElementModel MPI mode (%d)\n", aMode);
  }

}

@end
