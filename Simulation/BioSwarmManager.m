/*
 BioSwarmManager.m
 
 Copyright (C) 2012 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: March 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BioSwarmManager.h"
#import "BioSwarmController.h"
#import "BiologicalModel.h"
#import "EvolutionaryStrategy.h"
#import "ParameterSweep.h"
#import "MCMC.h"
#import "MathMLExpression.h"

#import "internal.h"

@implementation BioSwarmManager

- init
{
  return [self initWithDefaultModelFile: nil];
}

- initWithDefaultModelFile: (NSString *)fileName
{
  [super init];
  
  modelFilename = fileName;
  doSweep = NO;
  doRandom = NO;
  doEvolutionaryStrategy = NO;
  doMCMC = NO;
  doMPI = NO;
  
  doRSearch = NO;
  randomRuns = 0;
  startRun = -1;
  doAnalyze = NO;
  format = 0;
  doLearn = NO;
  trainFile = nil;
  doSample = NO;
  paramSamples = 0;
  paramSD = 0.0;
  doAllSample = NO;
  allSamples = 0;
  allSD = 0.0;
  doLatin = NO;
  latinSamples = 0;
  latinSD = 0.0;
  doAutoRange = NO;
  doPerturbation = NO;
  perturbationName = nil;
  doPredictError = NO;
  
  gpuDevice = 0;
  
  fileState = BCMODEL_WRITE_STATE;
  
  return self;
}

- (void)printUsage
{
  printf("Options:\n");
  printf("--help           Display usage information.\n");
  printf("--gpu N          Select GPU device N (default = 0).\n");
  printf("--sweep          Perform parameter sweep.\n");
  printf("--random N       Perform N simulation runs with random parameters.\n");
  printf("--evolve         Fit parameters using evolutionary strategy.\n");
  printf("--mcmc           Markov chain Monte Carlo for Bayesian analysis.\n");
  printf("--mpi            Perform simulation using MPI controller.\n");
  printf("--perturb N      Perform perturbation N.\n");
  printf("--predict-error  Perform error prediction for model.\n");
  //printf("--analyze      Output analysis of simulation runs.\n");
  //printf("--arff         Analysis data in ARFF format.\n");
  //printf("--libsmv       Analysis data in LIBSVM format.\n");
  //printf("--range        Perform automatic parameter sweep for range detection.\n");
  //printf("--rsearch N    Perform N search simulation runs, random modify one parameter at a time.\n");
  //printf("--sample N S   Perform N random samples of individual parameters with standard deviation S.\n");
  //printf("--asample N S  Perform N random samples of all parameters with standard deviation S.\n");
  printf("--run N          Start data output file number as N.\n");
  //printf("--learn F      Perform learning with data file F.\n");
  //printf("--latin N S    Perform Latin hypercube sampling with N samples and standard deviation S.\n");
  if (modelFilename) printf("[model]        Model file name, default is '%s'.\n", [modelFilename UTF8String]);
  else printf("model          Model file name\n");
}

- (BOOL)processArguments:(NSArray *)args
{
  BOOL error = NO;
  NS_DURING {
    
    // process parameters
    NSArray *args = [[NSProcessInfo processInfo] arguments];
    int i, argsCount = [args count];
    for (i = 1; i < argsCount; ++i) {
      if ([[args objectAtIndex: i] isEqualToString: @"--help"]) {
        [self printUsage];
        error = YES;
        goto done;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--gpu"]) {
        ++i;
        gpuDevice = [[args objectAtIndex: i] intValue];
        continue;
        
        if (gpuDevice < 0) {
          printf("ERROR: Invalid GPU device number.\n");
          [self printUsage];
          error = YES;
          break;
        }
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--sweep"]) {
        doSweep = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--random"]) {
        doRandom = YES;
        ++i;
        randomRuns = [[args objectAtIndex: i] intValue];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--evolve"]) {
        doEvolutionaryStrategy = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--mcmc"]) {
        doMCMC = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--mpi"]) {
        doMPI = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--run"]) {
        ++i;
        startRun = [[args objectAtIndex: i] intValue];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--perturb"]) {
        ++i;
        doPerturbation = YES;
        perturbationName = [args objectAtIndex: i];
        continue;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--predict-error"]) {
        doPredictError = YES;
        continue;
      }

#if 0
      if ([[args objectAtIndex: i] isEqualToString: @"--range"]) {
        doAutoRange = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--analyze"]) {
        doAnalyze = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--arff"]) {
        format = 1;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--libsvm"]) {
        format = 2;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--rsearch"]) {
        doRSearch = YES;
        ++i;
        randomRuns = [[args objectAtIndex: i] intValue];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--learn"]) {
        doLearn = YES;
        ++i;
        trainFile = [args objectAtIndex: i];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--sample"]) {
        doSample = YES;
        ++i;
        paramSamples = [[args objectAtIndex: i] intValue];
        ++i;
        paramSD = [[args objectAtIndex: i] doubleValue];
        
        if (paramSamples <= 0) {
          printf("ERROR: Number of samples for parameter sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        if (paramSD <= 0.0) {
          printf("ERROR: Standard deviation for parameter sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--asample"]) {
        doAllSample = YES;
        ++i;
        allSamples = [[args objectAtIndex: i] intValue];
        ++i;
        allSD = [[args objectAtIndex: i] doubleValue];
        
        if (allSamples <= 0) {
          printf("ERROR: Number of samples for parameter sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        if (allSD <= 0.0) {
          printf("ERROR: Standard deviation for parameter sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--latin"]) {
        doLatin = YES;
        ++i;
        latinSamples = [[args objectAtIndex: i] intValue];
        ++i;
        latinSD = [[args objectAtIndex: i] doubleValue];
        
        if (latinSamples <= 0) {
          printf("ERROR: Number of samples for Latin hypercube sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        if (latinSD <= 0.0) {
          printf("ERROR: Standard deviation for Latin hypercube sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        continue;
      }
#endif
      
      modelFilename = [args objectAtIndex: i];
    }
    
  } NS_HANDLER {
    
    printf("ERROR: Invalid usage.\n\n");
    [self printUsage];
    error = YES;
    
  } NS_ENDHANDLER
  
  if (error) goto done;
  
  // load model file
  theModel = [[NSMutableDictionary dictionaryWithContentsOfFile: modelFilename] retain];
  if (!theModel) {
    printf("ERROR: Could not open model file: %s\n", [modelFilename UTF8String]);
    error = YES;
    goto done;
  }
  
#if 0
  NSURL *urlPath = [[NSBundle bundleForClass: [self class]]
                        URLForResource: @"StandardFunctions" withExtension: @"xml"];
  printf("%s\n", [[urlPath absoluteString] UTF8String]);
  NSError *err;
  NSXMLDocument *test = [[NSXMLDocument alloc] initWithContentsOfURL:urlPath options:NSXMLDocumentTidyXML error:&err];
  printf("%s\n", [[test description] UTF8String]);
  NSXMLElement *root = [test rootElement];
  printf("%s %d\n", [[root name] UTF8String], [root childCount]);
  NSXMLElement *f = (NSXMLElement *)[root childAtIndex: 0];
  printf("%s %d\n", [[f name] UTF8String], [[f attributes] count]);
  NSXMLNode *n = [[f attributes] objectAtIndex: 0];
  printf("%s = %s\n", [[n name] UTF8String], [[n stringValue] UTF8String]);
#endif

#if 0
  MathMLExpression *f = [MathMLExpression functionWithName: @"hillFunction"];
  NSXMLElement *x = [f mathML];
  printf("%s %d\n", [[x name] UTF8String], [[x attributes] count]);
  NSMutableArray *funcArgs = [NSMutableArray array];
  [funcArgs addObject: [NSNumber numberWithDouble: 1.0]];
  [funcArgs addObject: [NSNumber numberWithDouble: 1.0]];
  [funcArgs addObject: [NSNumber numberWithDouble: 1.0]];
  [funcArgs addObject: [NSNumber numberWithDouble: 1.0]];
  NSNumber *n = [f calculateWithArguments: funcArgs];
  printf("hillFunction(1.0, 1.0, 1.0, 1.0) = %f\n", [n doubleValue]);
#endif

  if (doPredictError) {
    if (!doMCMC && !doEvolutionaryStrategy) {
      printf("ERROR: Need to specify a meta-analysis (mcmc, evolve, etc) for error prediction.\n");
      error = YES;
      goto done;
    }
  }

  // determine starting run number
  //if (startRun == -1) startRun = [[runParameters objectForKey: @"outputRun"] intValue];
  
  // load the model
  //[self loadModel: runParameters];
  
  //
  // post load checks
  //

done:
  return !error;
}

- (void)setDelegate: anObj
{
  delegate = anObj;
}

- delegate { return delegate; }

- (void)setGPUFunctions: (getGPUFunctions *)func
{
  gpuFuncs = (void *)func;
}

#if 0
#pragma mark == MANAGE SIMULATIONS ==
#endif

//                                                                                                                                                             
// Parameter sweep                                                                                                                                             
//                                                                                                                                                             
- (void)doSweepSimulations
{
  NSAutoreleasePool *pool;
  
  pool = [NSAutoreleasePool new];
  
  modelController = [BioSwarmController newController: @"sweepParameters" forModel: theModel andManager: self];
  if (modelController) {
    [modelController setDelegate: delegate];
    [modelController setGPUFunctions: gpuFuncs];
    [modelController setGPUDevice: gpuDevice];
    [modelController performSweep];
    [modelController release];
  }
  
  [pool drain];
}

//
// Simulations with random search in parameter space
//
- (void)doRandomParameterSimulations
{
#if 0
  NSAutoreleasePool *pool;
  
  // save the base model
  NSDictionary *baseModel = [model retain];
  
  pool = [NSAutoreleasePool new];
  
  outputRun = startRun;
  int i, j, k;
  for (i = 1; i < randomRuns; ++i) {
    NSMutableDictionary *simParameters = [NSMutableDictionary dictionaryWithDictionary: baseModel];
    NSMutableDictionary *simControl = [simParameters objectForKey: @"simulationControl"];
    NSDictionary *modelSet = [simParameters objectForKey: @"models"];
    NSArray *modelKeys = [modelSet allKeys];
    
    // go through each submodel
    // randomize each parameter
    NSArray *subModels = [modelInstances objectAtIndex: 0];
    for (j = 0; j < [subModels count]; ++j) {
      id aModel = [subModels objectAtIndex: j];
      int numParams = [aModel numOfParameters];
      NSString *key = [modelKeys objectAtIndex: j];
      NSString *val = [modelSet objectForKey: key];
      NSMutableDictionary *modelDict = [simParameters objectForKey: val];
      for (k = 0; k < numParams; ++k) {
        NSString *pn = [aModel nameOfParameter: k];
        if (randomParameters && ([randomParameters indexOfObject: pn] == NSNotFound)) continue;
        if (notRandomParameters && ([notRandomParameters indexOfObject: pn] != NSNotFound)) continue;
        double newValue = [aModel randomValueforParameter: k];
        [modelDict setObject: [NSNumber numberWithDouble: newValue] forKey: pn];
        if ([self isDebug]) printf("Randomize %s = %lf\n", [pn UTF8String], newValue);
      }
    }
    
    // new random seed
    [simControl setObject: [NSNumber numberWithInt: rand()] forKey: @"randomSeed"];
    [simControl setObject: [NSNumber numberWithInt: outputRun+i] forKey: @"outputRun"];
    
    // create random model and save parameter set
    subModels = [self newInstanceForModel: simParameters];
    [self saveModelSpecification: simParameters forRun: outputRun+i];
    //for (j = 0; j < [subModels count]; ++j) {
    //id aModel = [subModels objectAtIndex: j];
    //[aModel setRunNumber: outputRun+i];
    //}
  }
  outputRun = startRun;
  
  // run simulation
  [self doSimulationRun];
  
  [pool drain];
  
  // load the base model back
  [self releaseModel];
  [self loadModel: baseModel];
  [baseModel release];
#endif
}

//
// Fit parameters using evolutionary strategy
//
- (void)doEvolutionaryStrategy
{
  NSAutoreleasePool *pool;
  
  pool = [NSAutoreleasePool new];
  
  //EvolutionaryStrategy *es = [[EvolutionaryStrategy alloc] initWithModel: theModel andManager: self];
  modelController = [BioSwarmController newController: @"evolutionaryStrategy" forModel: theModel andManager: self];
  if (modelController) {
    [modelController setDelegate: delegate];
    [modelController setGPUFunctions: gpuFuncs];
    [modelController setGPUDevice: gpuDevice];
    if (doPredictError)
      [modelController performErrorPrediction];
    else
      [modelController performEvolution];
    [modelController release];
  }

  [pool drain];
}

//
// Markov chain Monte Carlo for Bayesian analysis
//
- (void)doMCMC
{
  NSAutoreleasePool *pool;
  
  pool = [NSAutoreleasePool new];
  
  //MCMC *mcmc = [MCMC newMCMCWithController: self];
  modelController = [BioSwarmController newController: @"MCMC" forModel: theModel andManager: self];
  if (modelController) {
    [modelController setDelegate: delegate];
    [modelController setGPUFunctions: gpuFuncs];
    [modelController setGPUDevice: gpuDevice];
    if (doPredictError)
      [modelController performErrorPrediction];
    else
      [modelController performMCMC];
    [modelController release];
  }
  
  [pool drain];
}

//
// MPI controller for HPC platform
//
- (void)doMPI
{
  NSAutoreleasePool *pool;
  
  pool = [NSAutoreleasePool new];
  
  modelController = [BioSwarmController newController: @"MPI" forModel: theModel andManager: self];
  if (modelController) {
    [modelController setDelegate: delegate];
    [modelController setGPUFunctions: gpuFuncs];
    [modelController setGPUDevice: gpuDevice];
    [modelController loadModelHierarchy];
    [modelController saveModelSpecification];
    if (doPerturbation) [modelController setPerturbation: perturbationName];
    [modelController doSimulationRun];
    [modelController cleanupSimulation];
    [modelController release];
  }
  
  [pool drain];
}

//
// Standard simulation run
//
- (void)doSimulationRun
{
  NSAutoreleasePool *pool;
  
  pool = [NSAutoreleasePool new];
  
  // Create the model controller and run the simulation
  //modelController = [[BioSwarmController alloc] initWithModel: theModel andManager: self];
  modelController = [BioSwarmController newController: nil forModel: theModel andManager: self];
  if (modelController) {
    [modelController setDelegate: delegate];
    [modelController setGPUFunctions: gpuFuncs];
    [modelController setGPUDevice: gpuDevice];
    [modelController loadModelHierarchy];
    [modelController saveModelSpecification];
    if (doPerturbation) [modelController setPerturbation: perturbationName];
    [modelController doSimulationRun];
    [modelController cleanupSimulation];
  }

  [pool release];
}

- (void)startSimulation
{
  if (doSweep) {
    [self doSweepSimulations];
  } else if (doAutoRange) {
    //res = range_tongue(runParameters, startRun);                                                                                                  
  } else if (doRandom) {
    [self doRandomParameterSimulations];
  } else if (doEvolutionaryStrategy) {
    [self doEvolutionaryStrategy];
  } else if (doMCMC) {
    [self doMCMC];
  } else if (doMPI) {
    [self doMPI];
  } else if (doRSearch) {
    //res = rsearch_tongue(runParameters, randomRuns, startRun);                                                                                    
  } else if (doAnalyze) {
    //res = analyze_tongue(runParameters, startRun, format);                                                                                        
  } else if (doLearn) {
    //res = learn_tongue(runParameters, trainFile);                                                                                                 
  } else if (doSample) {
    //res = sample_tongue(runParameters, paramSamples, paramSD, startRun);                                                                          
  } else if (doAllSample) {
    //res = asample_tongue(runParameters, allSamples, allSD, startRun);                                                                             
  } else if (doLatin) {
    //res = lhs_tongue(runParameters, latinSamples, latinSD, startRun);                                                                             
  } else {
    [self doSimulationRun];
  }
}

@end
