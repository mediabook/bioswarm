/*
 DataHandler.h
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: February 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import <BioCocoa/BCDataMatrix.h>

typedef double (DataHandlerCompareFunction)(double, double);
extern double data_handler_diff_function(double, double);
extern double data_handler_sqdiff_function(double, double);

@interface DataHandler : NSObject {
  id modelController;
  NSDictionary *dataDesc;

  NSMutableDictionary *dataMatrices;
  NSDictionary *speciesMap;
  NSMutableDictionary *timeCourse;
}

- initWithController: aController andDescriptor: (NSDictionary *)aDesc;

- (void)assignSpeciesMap: (NSDictionary *)aMap;

- (int)numberForName: (NSString *)aName andType: (NSString *)aType;
- (NSDictionary *)dataMatrices;
- (NSArray *)timeCourseForName: (NSString *)aName;

- (BCDataMatrix *)compareData: (BCDataMatrix *)dataMatrix forExperiment: (NSDictionary *)anExp;
- (BOOL)compareData: (BCDataMatrix *)dataMatrix forExperiment: (NSDictionary *)anExp withResult: (BCDataMatrix *)resultMatrix;
- (BOOL)compareData: (BCDataMatrix *)dataMatrix forExperiment: (NSDictionary *)anExp withResult: (BCDataMatrix *)resultMatrix
    compareFunction: (DataHandlerCompareFunction *)aFunction;

@end
