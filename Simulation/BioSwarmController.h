/*
 BioSwarmController.h
 
 The top level Swarm for controlling a simulation run.
 
 Copyright (C) 2012 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: March 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "SpatialModel.h"
#import "DataHandler.h"
#import "SimulationEvent.h"

@interface BioSwarmController : NSObject
{
  id simManager;
  
  id delegate;
  BOOL respondsToInit;
  BOOL respondsToDidInit;
  BOOL respondsToWillRun;
  BOOL respondsToDidTransfer;
  BOOL respondsToDidFinish;
  
  // models
  NSString *name;
  NSDictionary *model;
  NSString *modelDirectory;
  SpatialModel *spatialModel;
  NSMutableArray *modelInstances;
  NSMutableArray *modelSpecifications;
  NSMutableArray *totalSpecies;
  int numTotalSpecies;
  
  BOOL isDebug;
  BOOL isBinaryFile;
  int fileState;
  BOOL useInitializeFile;
  BOOL isInitializeBinaryFile;
  NSString *initializeFile;

  //id controlSchedule;
  //id simSchedule;
  //id outputSchedule;
  
  NSMutableArray *eventSchedule;
  
  int startRun;
  int outputRun;
  double outputFrequency;
  double simulateTime;
  double currentTime;
  double deltaTime;
  int largestTimeScale;
  int randomSeed;
  BOOL isDoublePrecision;
  
  int numOfIterations;
  int currentIteration;
  int currentSpecies;
  int currentRange;

  // species
  NSMutableArray *speciesNames;
  NSMutableArray *speciesIndexes;
  NSMutableArray *speciesModelNumbers;
  NSMutableArray *speciesModelKeys;
  
  // parameters
  NSMutableArray *allParameterNames;
  NSMutableArray *allParameterIndexes;
  NSMutableArray *allParameterModelNumbers;
  NSMutableArray *allParameterModelKeys;

  // parameter subset
  //int totalParameters;
  NSMutableArray *parameterNames;
  NSMutableArray *parameterIndexes;
  NSMutableArray *parameterModelNumbers;
  NSMutableArray *parameterModelKeys;

  // data sets and experiments
  NSDictionary *dataSets;
  DataHandler *dataHandler;
  NSMutableDictionary *dataIndexes;
  NSMutableArray *compareMatrixList;
  NSArray *experiments;
  NSString *currentExperiment;
  
  // random search
  NSArray *randomParameters;
  NSArray *notRandomParameters;
  
  // perturbations
  BOOL doPerturbation;
  NSString *perturbationName;
  NSDictionary *perturbations;
  
  // GPU
  int gpuDevice;
  void *gpuFuncs;
  ModelGPUData **gpuData;
}

+ (BioSwarmController *)newController: (NSString *)controllerType forModel: (NSDictionary *)aModel andManager: aManager;
- initWithModel: (NSDictionary *)aModel andManager: anObj;

- newInstanceForModel:(NSDictionary *)modelSpec;
- loadModel: (NSDictionary *)aModel;
- (void)releaseModel;

- (BOOL)saveModelSpecification;
- (BOOL)saveSpecificationForModel:(int)modelIndex forRun:(int)aNum;
- (BOOL)saveModelSpecification:(NSDictionary *)aModel forRun:(int)aNum;

- (void)setDelegate: anObj;
- delegate;
- (id)manager;
- (NSDictionary *)model;
- (NSString *)modelName;
- (NSString *)modelDirectory;
- (void)setModelDirectory: (NSString *)aDir;
- (int)outputRun;
- (NSString *)outputSuffix;
- (int)outputFrequency;
- (double)simulateTime;
- (double)currentTime;
- (int)largestTimeScale;
- (double)smallestDT;
- (SpatialModel *)spatialModel;
//- (NSArray *)subModels;
//- (NSArray *)species;
//- (id)getModelWithName: (NSString *)aName;
- (NSArray *)modelInstances;
- (int)numModelInstances;
- (id)getModel:(int)modelIndex withName: (NSString *)aName;
- (NSArray *)modelSpecifications;
- (NSDictionary *)getSpecificationForModel:(int)modelIndex;

- (id)modelOfTypeClass: (Class)aClass;
- (id)modelOfTypeClass: (Class)aClass forModel:(int)modelIndex;

- (id)modelWithKey: (NSString *)aModelKey;
- (id)modelWithKey: (NSString *)aModelKey forModel:(int)modelIndex;

- (BOOL)isDebug;
- (void)setDebug: (BOOL)aFlag;

- (BOOL)isBinaryFile;
- (void)setBinaryFile: (BOOL)aFlag;

- (int)fileState;
- (void)setFileState: (int)aState;

- (BOOL)isDoublePrecision;
- (void)setDoublePrecision: (BOOL)aFlag;

- (BOOL)useInitializeFile;
- (BOOL)isInitializeBinaryFile;
- (NSString *)initializeFile;

- (void)addSimulationEvent: (SimulationEvent *)anEvent;
- (void)removeSimulationEvent: (SimulationEvent *)anEvent;
- (void)clearEventSchedule;

- (BOOL)constructParameterList:(NSArray *)includeList exclude:(NSArray *)excludeList
                         names:(NSMutableArray **)pNames indexes:(NSMutableArray **)pIndexes
                  modelNumbers:(NSMutableArray **)pModelNumbers
                    modelKeys:(NSMutableArray **)pModelKeys;
- (BOOL)setupParameterRanges: (NSDictionary *)ranges checkNames: (NSArray *)names;

- (int)constructSpeciesList:(NSMutableArray **)speciesNames indexes:(NSMutableArray **)speciesIndexes
               modelNumbers:(NSMutableArray **)speciesModelNumbers modelKeys:(NSMutableArray **)speciesModelKeys;

- (BOOL)setupDataSets: (NSDictionary *)modelSpec matrixCount: (int)numMatrices;
- (DataHandler *)dataHandler;

- (void)performHierarchy: (int)modelIndex withInvocation: (NSInvocation *)anInvocation;
- (void)performHierarchyInvocation: (NSInvocation *)anInvocation;

- (void)setupHierarchyDataFilesWithState: (int)aState;
- (void)setupHierarchyDataFiles: (int)modelIndex withState: (int)aState;

- (void)allocateHierarchyWithEncode: (NSString *)anEncode;
//- (void)allocateHierarchyWithEncode: (NSString *)anEncode fileState: (int)aState;
//- (void)allocateHierarchy: (int)modelIndex withEncode: (NSString *)anEncode fileState: (int)aState;
- (void)allocateHierarchy: (int)modelIndex withEncode: (NSString *)anEncode;

- (void)initializeHierarchy;
- (void)initializeHierarchyForModel:(int)modelIndex;

- (BOOL)writeHierarchyData;
- (BOOL)writeHierarchyDataWithCheck: (BOOL)aFlag;
- (BOOL)writeHierarchyDataForModel:(int)modelIndex withCheck: (BOOL)aFlag;

- (BOOL)readHierarchyData;
- (BOOL)readHierarchyDataForModel:(int)modelIndex;

- (BOOL)loadModelHierarchy;
//- (void)runCPUSimulation;
- (void)doSimulationRun;
- (void)doSimulationRunForExperiment: (NSString *)expName;
- (void)cleanupSimulation;

- (int)numOfIterations;
- (int)currentIteration;
- (void)setCurrentIteration: (int)aNum;
- (int)currentSpecies;
- (void)setCurrentSpecies: (int)aNum;
- (float)speciesMin;
- (float)speciesMax;
- (void)setCurrentRange: (int)aNum;

- (void)setupSimulationReplay;
- (void)rewindFiles;
- (void)rewindFilesForModel:(int)modelIndex;
- (void)nextIteration;

- (NSArray *)randomParameters;
- (NSArray *)notRandomParameters;

- (BOOL)isPerturbation;
- (NSString *)perturbationName;
- (void)setPerturbation: (NSString *)pName;
- (NSDictionary *)perturbations;
- (void)applyPerturbation: (NSString *)pName;
- (void)applyPerturbation: (NSString *)pName forModel: (int)modelIndex;

@end

//
// GPU
//
#import "GPUDefines.h"

//
// Produce GPU code
//
@interface BioSwarmController (GPU)
- (BOOL)generateGPUCode: (NSString *)prefixName;
@end

@interface BioSwarmController (GPURun)
- (void)setGPUFunctions: (getGPUFunctions *)func;
- (getGPUFunctions *)gpuFunctions;
- (void)setGPUDevice: (int)aDevice;
- (ModelGPUData **)gpuData;
- (void)transferDataToGPU: (BOOL)aFlag;

- (void)runGPUSimulation: (getGPUFunctions *)func;
@end

//
// Visualization
//

@interface BioSwarmController (Povray)
- (NSMutableString *)povrayCode;
- (NSMutableString *)povrayCodeForModel:(int)modelIndex;
@end

//
// Notifications
//
extern NSString * const BioSwarmDidTransferDataNotification;

/*!
 @constant BCModelIsLoadedNotification
 @abstract Notification that model hierarchy is loaded
 */
extern NSString * const BioSwarmModelIsLoadedNotification;



