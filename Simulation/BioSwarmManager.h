/*
 BioSwarmManager.h
 
 Copyright (C) 2012 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: March 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>

#import "GPUDefines.h"

@interface BioSwarmManager : NSObject
{
  // model
  NSString *modelFilename;
  NSMutableDictionary *theModel;
  id modelController;
  
  id delegate;
  
  // arguments
  BOOL doSweep;
  BOOL doRandom;
  BOOL doEvolutionaryStrategy;
  BOOL doMCMC;
  BOOL doMPI;
  
  BOOL doRSearch;
  int randomRuns;
  int startRun;
  BOOL doAnalyze;
  int format;
  BOOL doLearn;
  NSString *trainFile;
  BOOL doSample;
  int paramSamples;
  double paramSD;
  BOOL doAllSample;
  int allSamples;
  double allSD;
  BOOL doLatin;
  int latinSamples;
  double latinSD;
  BOOL doAutoRange;
  BOOL doPerturbation;
  NSString *perturbationName;
  BOOL doPredictError;
  
  int fileState;

  // GPU
  int gpuDevice;
  void *gpuFuncs;

}

- initWithDefaultModelFile: (NSString *)fileName;

- (BOOL)processArguments:(NSArray *)args;

- (void)setDelegate: anObj;
- delegate;

- (void)setGPUFunctions: (getGPUFunctions *)func;

- (void)startSimulation;

@end
