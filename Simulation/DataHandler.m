/*
 DataHandler.m
 
 Copyright (C) 2012 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: February 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "DataHandler.h"
#import <BioCocoa/BCDataMatrix.h>

// Some standard compare functions
double data_handler_diff_function(double a, double b)
{
  return (a - b);
}

double data_handler_sqdiff_function(double a, double b)
{
  return (a - b)*(a - b);
}

@implementation DataHandler

- initWithController: aController andDescriptor: (NSDictionary *)aDesc
{
  [super init];
  
  modelController = [aController retain];
  dataDesc = [aDesc retain];
  speciesMap = nil;

  // load each data file
  NSArray *dataFiles = [dataDesc objectForKey: @"dataFiles"];
  if (!dataFiles) return nil;

  dataMatrices = [NSMutableDictionary new];
  timeCourse = [NSMutableDictionary new];
  int fileIdx;
  for (fileIdx = 0; fileIdx < [dataFiles count]; ++fileIdx) {
    NSString *dataName = [dataFiles objectAtIndex: fileIdx];
    NSDictionary *dataFile = [dataDesc objectForKey: dataName];

    // load file
    NSString *dataFileName = [dataFile objectForKey: @"dataFile"];
    if (!dataFileName) return nil;
    
    NSMutableDictionary *format = [dataFile objectForKey: @"format"];
    if (!format) return nil;
    
    BCDataMatrix *dm = [BCDataMatrix dataMatrixWithContentsOfFile: dataFileName
                                                        andEncode: BCdoubleEncode
                                                        andFormat: format];
    
    if (!dm) return nil;
    
    id s = [format objectForKey: @"transpose"];
    if (s && [s boolValue]) dm = [dm dataMatrixFromTranspose];
    
    [dataMatrices setObject: dm forKey: dataName];
    
    s = [dataFile objectForKey: @"timeCourse"];
    if (s) {
      [timeCourse setObject: s forKey: dataName];
      
      // check that observations match time course
      NSDictionary *observations = [dataFile objectForKey: @"observations"];
      NSArray *keys = [observations allKeys];
      int i;
      for (i = 0; i < [keys count]; ++i) {
        NSString *key = [keys objectAtIndex: i];
        NSArray *a = [observations objectForKey: key];
        if ([s count] != [a count]) {
          printf("ERROR: Number of observations for '%s' (%ld) != number of time course list (%ld) for data: %s\n",
                 [key UTF8String], (unsigned long)[a count], (unsigned long)[s count], [dataName UTF8String]);
          return nil;
        }
      }
    }
  }

  return self;
}

- (void)dealloc
{
  [modelController release];
  [dataDesc release];
  [dataMatrices release];
  [speciesMap release];
  [timeCourse release];
  
  [super dealloc];
}

- (void)assignSpeciesMap: (NSDictionary *)aMap
{
  if (speciesMap) [speciesMap release];
  speciesMap = [aMap retain];
}

- (int)numberForName: (NSString *) aName andType: (NSString *)aType
{
  NSDictionary *dataFile = [dataDesc objectForKey: aName];
  NSDictionary *observations = [dataFile objectForKey: @"observations"];
  NSArray *a = [observations objectForKey: aType];
  return [a count];
}

- (NSArray *)timeCourseForName: (NSString *) aName { return [timeCourse objectForKey: aName]; }

- (NSDictionary *)dataMatrices { return dataMatrices; }

- (BCDataMatrix *)compareData: (BCDataMatrix *)dataMatrix forExperiment: (NSDictionary *)anExp
{
  NSString *aName = [anExp objectForKey: @"name"];
  NSString *aType = [anExp objectForKey: @"type"];
  BCDataMatrix *dm = [dataMatrices objectForKey: aName];
  NSDictionary *dataFile = [dataDesc objectForKey: aName];
  NSDictionary *observations = [dataFile objectForKey: @"observations"];
  NSArray *a = [observations objectForKey: aType];
  
  int numRows = [a count];
  int numCols = [dm numberOfColumns];
  //printf("numr = %d, numc = %d\n", numRows, numCols);
  BCDataMatrix *rm = [BCDataMatrix emptyDataMatrixWithRows: numRows andColumns: numCols andEncode: BCdoubleEncode];
  
  if ([self compareData: dataMatrix forExperiment: anExp withResult: rm]) return rm;
  else return nil;
}

- (BOOL)compareData: (BCDataMatrix *)dataMatrix forExperiment: (NSDictionary *)anExp withResult: (BCDataMatrix *)resultMatrix
{
  //return [self compareData: dataMatrix forExperiment: anExp withResult: resultMatrix compareFunction: &data_handler_sqdiff_function];
  return [self compareData: dataMatrix forExperiment: anExp withResult: resultMatrix compareFunction: &data_handler_diff_function];
}

- (BOOL)compareData: (BCDataMatrix *)dataMatrix forExperiment: (NSDictionary *)anExp withResult: (BCDataMatrix *)resultMatrix
    compareFunction: (DataHandlerCompareFunction *)aFunction
{
  NSString *aName = [anExp objectForKey: @"name"];
  NSString *aType = [anExp objectForKey: @"type"];
  BCDataMatrix *dm = [dataMatrices objectForKey: aName];
  NSDictionary *dataFile = [dataDesc objectForKey: aName];
  NSDictionary *observations = [dataFile objectForKey: @"observations"];
  NSArray *a = [observations objectForKey: aType];
  NSString *relative = [anExp objectForKey: @"relative"];

  int numRows = [a count];
  int fullRows = [dm numberOfRows];
  int numCols = [dm numberOfColumns];
  int dataCols = [dataMatrix numberOfColumns];
  double (*DM)[fullRows][numCols] = [dm dataMatrix];
  double (*RM)[numRows][numCols] = [resultMatrix dataMatrix];
  double (*CM)[numRows][dataCols] = [dataMatrix dataMatrix];
  int i, j;

  NSArray *colNames = [dm columnNames];
  //printf("colNames = %s\n", [[colNames description] UTF8String]);
  NSArray *dataNames = [dataMatrix columnNames];
  //printf("dataNames = %s\n", [[dataNames description] UTF8String]);
  NSUInteger relativeCol = NSNotFound, rCol = NSNotFound;
  if (relative) {
    relativeCol = [colNames indexOfObject: relative];
    rCol = [dataNames indexOfObject: relative];
    //printf("relative (%d, %d) = %f\n", relativeCol, rCol, (*CM)[0][rCol]);
    if (relativeCol == NSNotFound) {
      printf("ERROR: Unknown relative column: %s\n", [relative UTF8String]);
      return NO;
    }
  }

  if (speciesMap) {
    for (j = 0; j < dataCols; ++j) {
      NSString *cName = [dataNames objectAtIndex: j];
      NSString *aName = [speciesMap objectForKey: cName];
      if (!aName) continue;
      NSUInteger aCol = [colNames indexOfObject: aName];
      if (aCol == NSNotFound) continue;
      
      for (i = 0; i < numRows; ++i) {
        int aRow = [[a objectAtIndex: i] intValue];
        if (relative) (*RM)[i][aCol] = (*aFunction)((*DM)[aRow][aCol] / (*DM)[aRow][relativeCol], (*CM)[i][j] / (*CM)[i][rCol]);
        else (*RM)[i][aCol] = (*aFunction)((*DM)[aRow][aCol], (*CM)[i][j]);
      }
    }
  } else {
    // with no species map, the matrices must match each other
    if (numCols != [dataMatrix numberOfColumns]) {
      printf("ERROR: matrices must match when no species map (cols: %d != %d)\n", numCols, [dataMatrix numberOfColumns]);
      return NO;
    }

    for (i = 0; i < numRows; ++i) {
      int aRow = [[a objectAtIndex: i] intValue];
      for (j = 0; j < numCols; ++j) {
        if (relative) (*RM)[i][j] = (*aFunction)((*DM)[aRow][j] / (*DM)[aRow][relativeCol], (*CM)[i][j] / (*CM)[i][rCol]);
        else (*RM)[i][j] = (*aFunction)((*DM)[aRow][j], (*CM)[i][j]);
      }
    }
  }
  
  return YES;
}

@end
