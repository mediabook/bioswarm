/*
 CodeGenerationManager.m
 
 Copyright (C) 2012 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: March 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "CodeGenerationManager.h"
#import "BioSwarmController.h"
#import "BiologicalModel.h"
#import "BioSwarmModel.h"

@implementation CodeGenerationManager

- init
{
  [super init];
  
  return self;
}

- (void)dealloc
{
  if (theModel) [theModel release];
  if (generatorMethods) [generatorMethods release];

  [super dealloc];
}

- (void)printUsage: (NSArray *)args
{
  printf("Usage:\n");
  printf("%s modelFile outputName\n\n", [[[args objectAtIndex: 0] lastPathComponent] UTF8String]);

  printf("Options:\n");
  printf("--help         Display usage information.\n");
  printf("--mpi          Produce MPI code.\n");
  printf("modelFile      File name of model specification.\n");
  printf("outputName     Name for GPU code and files.\n");
}

- (BOOL)processArguments:(NSArray *)args
{
  BOOL error = NO;
  NS_DURING {
    
    // process parameters
    NSArray *args = [[NSProcessInfo processInfo] arguments];
    int i, argsCount = [args count];
    for (i = 1; i < argsCount; ++i) {
      if ([[args objectAtIndex: i] isEqualToString: @"--help"]) {
        [self printUsage: args];
        error = YES;
        goto done;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--old"]) {
        useOld = YES;
        continue;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--mpi"]) {
        isMPI = YES;
        continue;
      }

      modelFilename = [args objectAtIndex: i++];
      outputName = [args objectAtIndex: i];
    }
    
  } NS_HANDLER {
    
    printf("ERROR: Invalid usage.\n\n");
    [self printUsage: args];
    error = YES;
    
  } NS_ENDHANDLER
  
  if (error) goto done;
  
  // load model file
  theModel = [[NSMutableDictionary dictionaryWithContentsOfFile: modelFilename] retain];
  if (!theModel) {
    printf("ERROR: Could not open parameter file: %s\n", [modelFilename UTF8String]);
    error = YES;
    goto done;
  }
  
done:
  return !error;
}

- (BOOL)defineGeneratorMethods
{
  generatorMethods = [NSMutableDictionary new];
  
  if (isMPI) {
    NSMethodSignature *ms = [BioSwarmModel instanceMethodSignatureForSelector: @selector(controllerGPUMPICode:)];
    if (!ms) {
      printf("ERROR: MPI capability not available.\n");
      return NO;
    }

    NSInvocation *anInv = [NSInvocation invocationWithMethodSignature: [BioSwarmModel instanceMethodSignatureForSelector: @selector(controllerGPUMPICode:)]];
    [anInv setSelector: @selector(controllerGPUMPICode:)];
    [generatorMethods setObject: anInv forKey: @"controller"];
    
    anInv = [NSInvocation invocationWithMethodSignature: [BioSwarmModel instanceMethodSignatureForSelector: @selector(definitionsGPUMPICode:)]];
    [anInv setSelector: @selector(definitionsGPUMPICode:)];
    [generatorMethods setObject: anInv forKey: @"definitions"];
    
    anInv = [NSInvocation invocationWithMethodSignature: [BioSwarmModel instanceMethodSignatureForSelector: @selector(kernelGPUMPICode:)]];
    [anInv setSelector: @selector(kernelGPUMPICode:)];
    [generatorMethods setObject: anInv forKey: @"kernel"];

  } else {
    // assume plain GPU
    NSInvocation *anInv = [NSInvocation invocationWithMethodSignature: [BioSwarmModel instanceMethodSignatureForSelector: @selector(controllerGPUCode:)]];
    [anInv setSelector: @selector(controllerGPUCode:)];
    [generatorMethods setObject: anInv forKey: @"controller"];

    anInv = [NSInvocation invocationWithMethodSignature: [BioSwarmModel instanceMethodSignatureForSelector: @selector(definitionsGPUCode:)]];
    [anInv setSelector: @selector(definitionsGPUCode:)];
    [generatorMethods setObject: anInv forKey: @"definitions"];

    anInv = [NSInvocation invocationWithMethodSignature: [BioSwarmModel instanceMethodSignatureForSelector: @selector(kernelGPUCode:)]];
    [anInv setSelector: @selector(kernelGPUCode:)];
    [generatorMethods setObject: anInv forKey: @"kernel"];
  }
  
  return YES;
}

- (NSString *)prefixForModelIndex: (int)modelIndex
{
  return [NSString stringWithFormat: @"%@_%d", outputName, modelIndex];
}

- (BOOL)generateGPUCode: modelObject withModelIndex: (int *)modelIndex
             codeString: (NSMutableString *)code invokeString: (NSMutableString *)invokeCode
             execString: (NSMutableString *)execCode functionString: (NSMutableString *)functions
{
  int i;
  NSMutableString *GPUCode;
  NSString *outFile;
  NSDictionary *GPUDictionary;

  if (!modelObject) return YES;
  printf("model index: %d\n", *modelIndex);

  NSString *newPrefix = [NSString stringWithFormat: @"%@_%d", outputName, *modelIndex];

  NSInvocation *anInv = [generatorMethods objectForKey: @"controller"];
  [anInv setTarget: modelObject];
  [anInv setArgument: &newPrefix atIndex: 2];
  [anInv invoke];
  [anInv getReturnValue: &GPUDictionary];

  //NSDictionary *GPUDictionary = [modelObject controllerGPUCode: newPrefix];
  if (GPUDictionary) {
    
    GPUCode = [NSMutableString new];
    //[GPUCode appendString: [GPUDictionary objectForKey: @"header"]];
    [GPUCode appendString: [GPUDictionary objectForKey: @"defines"]];
    if ([GPUDictionary objectForKey: @"allocation function"]) {
      [GPUCode appendString: @"\n// Allocation\n"];
      [GPUCode appendString: @"void "];
      [GPUCode appendString: [GPUDictionary objectForKey: @"allocation function"]];
      [GPUCode appendString: [GPUDictionary objectForKey: @"allocation code"]];
    }
    if ([GPUDictionary objectForKey: @"transfer function"]) {
      [GPUCode appendString: @"\n// Data Transfer\n"];
      [GPUCode appendString: @"void "];
      [GPUCode appendString: [GPUDictionary objectForKey: @"transfer function"]];
      [GPUCode appendString: [GPUDictionary objectForKey: @"transfer code"]];
    }
    [GPUCode appendString: @"\n// Execution\n"];
    [GPUCode appendString: @"void "];
    [GPUCode appendString: [GPUDictionary objectForKey: @"execution function"]];
    [GPUCode appendString: @"\n{\n"];
    [GPUCode appendString: [GPUDictionary objectForKey: @"structure"]];
    [GPUCode appendString: [GPUDictionary objectForKey: @"execution variables"]];
    
    if ([GPUDictionary objectForKey: @"solo execution code"]) {
      [GPUCode appendString: [GPUDictionary objectForKey: @"solo execution code"]];
    } else {
      [GPUCode appendString: @"\n   // Invoke kernel\n"];
      [GPUCode appendString: @"   double currentTime = startTime;\n"];
      [GPUCode appendString: @"   int done = 0;\n"];
      [GPUCode appendString: @"   while (!done) {\n"];
      [GPUCode appendString: [GPUDictionary objectForKey: @"execution code"]];
      [GPUCode appendFormat: @"\n      currentTime += %@_ptrs->dt;\n", newPrefix];
      [GPUCode appendString: @"      if (TIME_EQUAL(currentTime, nextTime) || currentTime > nextTime) done = 1;\n"];
      [GPUCode appendString: @"   }\n"];
    }
    [GPUCode appendString: @"}\n"];
    
    if ([GPUDictionary objectForKey: @"release function"]) {
      [GPUCode appendString: @"\n// Release\n"];
      [GPUCode appendString: @"void "];
      [GPUCode appendString: [GPUDictionary objectForKey: @"release function"]];
      [GPUCode appendString: [GPUDictionary objectForKey: @"release code"]];
    }
    [GPUCode appendString: [GPUDictionary objectForKey: @"footer"]];
    
    outFile = [NSString stringWithFormat: @"%@.cu", newPrefix];
    [GPUCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
    
    //GPUCode = [NSMutableString new];
    
    anInv = [generatorMethods objectForKey: @"definitions"];
    [anInv setTarget: modelObject];
    [anInv setArgument: &newPrefix atIndex: 2];
    [anInv invoke];
    [anInv getReturnValue: &GPUCode];

    //GPUCode = [modelObject definitionsGPUCode: newPrefix];
    outFile = [NSString stringWithFormat: @"%@_defines.cu", newPrefix];
    [GPUCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
    
    anInv = [generatorMethods objectForKey: @"kernel"];
    [anInv setTarget: modelObject];
    [anInv setArgument: &newPrefix atIndex: 2];
    [anInv invoke];
    [anInv getReturnValue: &GPUCode];

    //GPUCode = [modelObject kernelGPUCode: newPrefix];
    outFile = [NSString stringWithFormat: @"%@_kernel.cu", newPrefix];
    [GPUCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
    
    [code appendFormat: @"#include \"%@.cu\"\n", newPrefix];
    
    [invokeCode appendFormat: @"  void *%@_data = data[%d]->gpuPtrs;\n", newPrefix, *modelIndex];
    [invokeCode appendString: [GPUDictionary objectForKey: @"structure"]];
    [invokeCode appendString: [GPUDictionary objectForKey: @"execution variables"]];
    
    [execCode appendString: [GPUDictionary objectForKey: @"execution code"]];
    
    [functions appendFormat: @"  case %d:\n", *modelIndex];
    [functions appendFormat: @"    return (void *)&%@_gpuFunctions;\n", newPrefix];
  }
  ++(*modelIndex);
  
  // get code for submodels
  NSArray *childModels = [modelObject childModels];
  for (i = 0; i < [childModels count]; ++i) {
    id aModel = [childModels objectAtIndex: i];
    [self generateGPUCode: aModel withModelIndex: modelIndex
               codeString: code invokeString: invokeCode
               execString: execCode functionString: functions];
  }

  return YES;
}

- (BOOL)generateGPUCode
{
  int i;
  NSString *outFile;
  NSMutableString *functions;
  NSMutableString *code;
  NSMutableString *invokeCode;
  NSMutableString *execCode;

  // define the generator methods
  if (![self defineGeneratorMethods]) return NO;

  // Create the model objects
  BioSwarmController *modelController = [[BioSwarmController alloc] initWithModel: theModel andManager: self];
  if (!modelController) return NO;
  [modelController loadModelHierarchy];
  id modelObject = [[modelController modelInstances] objectAtIndex: 0];

  // Generate top-level GPU code
  code = [NSMutableString new];
  [code appendString: @"#include <stdio.h>\n"];
  [code appendString: @"#include <unistd.h>\n\n"];

  // check if any submodels have debugging on, and include cuPrintf
#if 0
  BOOL debugFlag = [modelController isDebug];
  NSArray *subModels = [[modelController modelInstances] objectAtIndex: 0];
  for (i = 0; i < [subModels count]; ++i) debugFlag |= [[subModels objectAtIndex: i] isDebug];
  if (debugFlag) [code appendString: @"#include \"cuPrintf.cu.m\"\n\n"];
#endif

  [code appendString: @"extern \"C\" {\n"];
  [code appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  //[code appendString: @"#include <BioSwarm/GPU_sem.h>\n"];
  
  [code appendString: @"\nvoid cudacheck(const char *message) {\n"];
  [code appendString: @"  cudaError_t error = cudaGetLastError();\n"];
  [code appendString: @"  if (error!=cudaSuccess) printf(\"cudaERROR: %s : %i (%s)\\n\", message, error, cudaGetErrorString(error));\n"];
  [code appendString: @"}\n\n"];
  
  invokeCode = [NSMutableString new];
  [invokeCode appendString: @"\n// main execution function\n"];
  [invokeCode appendFormat: @"void %@_invokeGPUKernel(void *controller, ModelGPUData** data, double startTime, double nextTime)\n", outputName];
  [invokeCode appendString: @"{\n"];
  //[invokeCode appendString: @"\n   // Invoke kernel\n"];
  //[invokeCode appendString: @"   double currentTime = startTime;\n"];
  //[invokeCode appendString: @"   while (currentTime < nextTime) {\n"];
  //[invokeCode appendString: execCode];
  //[invokeCode appendString: @"      currentTime += 0.01;\n"];
  //[invokeCode appendFormat: @"      currentTime += %@_0_ptrs->dt;\n", outputName];
  //[invokeCode appendString: @"   }\n"];
  [invokeCode appendString: @"}\n"];
  
  functions = [NSMutableString new];
  [functions appendString: @"\n// Pointers to gpu functions for models\n"];
  [functions appendString: @"void *getGPUFunctionForModel(int aModel, int value)\n"];
  [functions appendString: @"{\n"];
  [functions appendString: @"  switch (aModel) {\n"];
  [functions appendString: @"  case -4:\n"];
  [functions appendString: @"    cudaSetDevice(value);\n"];
  [functions appendString: @"    cudacheck(\"set GPU device\");\n"];
  [functions appendString: @"    return NULL;\n"];
  [functions appendString: @"  case -3:\n"];
  [functions appendString: @"    cudaDeviceReset();\n"];
  [functions appendString: @"    return NULL;\n"];
  [functions appendString: @"  case -2:\n"];
  //if (debugFlag) [functions appendString: @"    cudaPrintfInit();\n"];
  [functions appendString: @"    return NULL;\n"];
  [functions appendString: @"  case -1:\n"];
  [functions appendFormat: @"    return (void *)&%@_invokeGPUKernel;\n", outputName];
  
  NSMutableString *subInvokeCode = [NSMutableString new];
  execCode = [NSMutableString new];
  int modelIndex = 0;
  [self generateGPUCode: modelObject withModelIndex: &modelIndex
             codeString: code invokeString: subInvokeCode
             execString: execCode functionString: functions];
  
  [functions appendString: @"  default:\n"];
  [functions appendString: @"    return NULL;\n"];
  [functions appendString: @"  }\n"];
  [functions appendString: @"}\n"];
  
  [code appendString: invokeCode];
  [code appendString: @"\n"];
  [code appendString: functions];
  [code appendString: @"}\n"];
  
  outFile = [NSString stringWithFormat: @"%@.cu", outputName];
  [code writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
  
  return YES;
}

@end
