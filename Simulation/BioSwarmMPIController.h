/*
 BioSwarmMPIController.h
 
 The top level controller for an MPI simulation run.
 
 Copyright (C) 2014 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: December 2014
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import "BioSwarmController.h"
#import "BioSwarmModel.h"

#include <mpi.h>

@interface BioSwarmMPIController : BioSwarmController
{
  int rank;

  int *topologySize;
  int *periodic;
  MPI_Comm topologyMPIComm;
  int *coordinates;

  int neighborRanks[S_XYZ_TOT];
}

- (int)rank;
- (int *)neighborRanks;
- (MPI_Comm)topologyMPICommunicator;

@end

//
// MPI enhancements to standard BioSwarmModel
//

@interface BioSwarmModel (MPI)
- (void)allocateMPIHierarchyWithEncode:(NSString *)anEncode;
- (void)initializeMPIHierarchy;

// these are implemented by subclasses
- (void)allocateMPISimulationWithEncode: (NSString *)anEncode;
- (void)initializeMPISimulation;
- (void)initializeMPIFileCompleted;
@end

//
// MPI coordinates system is different from BioSwarm
// 
extern void bc_mpi_sector_coordinate_shift(int xc, int yc, int zc, int direction, int *xo, int *yo, int *zo);
