/*
 SimulationEvent.m
 
 Copyright (C) 2012 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: July 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "SimulationEvent.h"

@implementation SimulationEvent

+ eventWithInvocation: (NSInvocation *)anInv atTime: (double)aTime
{
  SimulationEvent *anEvent = [[SimulationEvent alloc] init];
  anEvent->theInvocation = anInv;
  anEvent->startTime = aTime;
  anEvent->repeatInterval = 0;
  anEvent->nextTime = aTime;
  anEvent->requireTransfer = NO;
  
  return anEvent;
}

+ eventWithInvocation: (NSInvocation *)anInv atTime: (double)aTime repeat: (double)aRepeat
{
  SimulationEvent *anEvent = [[SimulationEvent alloc] init];
  anEvent->theInvocation = anInv;
  anEvent->startTime = aTime;
  anEvent->repeatInterval = aRepeat;
  anEvent->nextTime = aTime;
  anEvent->requireTransfer = NO;
  
  return anEvent;
}

- (void)setRequireTransfer: (BOOL)aFlag { requireTransfer = aFlag; }

@end
