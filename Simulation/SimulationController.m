/*
 SimulationController.m
 
 Copyright (C) 2010-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: November 2010
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "SimulationController.h"
#import "BiologicalModel.h"
#import "Grid2DArray.h"
#import "EvolutionaryStrategy.h"
#import "MCMC.h"

#import "internal.h"

@implementation SimulationController

- init
{
  return [self initWithDefaultModelFile: nil];
}

- initWithDefaultModelFile: (NSString *)fileName
{
  [super init];
  
  respondsToInit = NO;
  respondsToDidInit = NO;
  respondsToWillRun = NO;
  respondsToDidTransfer = NO;
  respondsToDidFinish = NO;

  modelFilename = fileName;
  doSweep = NO;
  doRandom = NO;
  doEvolutionaryStrategy = NO;
  doMCMC = NO;

  doRSearch = NO;
  randomRuns = 0;
  startRun = -1;
  doAnalyze = NO;
  format = 0;
  doLearn = NO;
  trainFile = nil;
  doSample = NO;
  paramSamples = 0;
  paramSD = 0.0;
  doAllSample = NO;
  allSamples = 0;
  allSD = 0.0;
  doLatin = NO;
  latinSamples = 0;
  latinSD = 0.0;
  doAutoRange = NO;
  doPerturbation = NO;
  perturbationName = nil;
  perturbations = nil;

  gpuDevice = 0;
  gpuFuncs = NULL;
  
  fileState = BCMODEL_WRITE_STATE;
  
  return self;
}

- (void)printUsage
{
  printf("Options:\n");
  printf("--help         Display usage information.\n");
  printf("--gpu N        Select GPU device N (default = 0).\n");
  printf("--sweep        Perform parameter sweep.\n");
  printf("--random N     Perform N simulation runs with random parameters.\n");
  printf("--evolve       Fit parameters using evolutionary strategy.\n");
  printf("--mcmc         Markov chain Monte Carlo for Bayesian analysis.\n");
  printf("--perturb N    Perform perturbation N.\n");
  //printf("--analyze      Output analysis of simulation runs.\n");
  //printf("--arff         Analysis data in ARFF format.\n");
  //printf("--libsmv       Analysis data in LIBSVM format.\n");
  //printf("--range        Perform automatic parameter sweep for range detection.\n");
  //printf("--rsearch N    Perform N search simulation runs, random modify one parameter at a time.\n");
  //printf("--sample N S   Perform N random samples of individual parameters with standard deviation S.\n");
  //printf("--asample N S  Perform N random samples of all parameters with standard deviation S.\n");
  printf("--run N        Start data output file number as N.\n");
  //printf("--learn F      Perform learning with data file F.\n");
  //printf("--latin N S    Perform Latin hypercube sampling with N samples and standard deviation S.\n");
  if (modelFilename) printf("[model]        Model file name, default is '%s'.\n", [modelFilename UTF8String]);
  else printf("model          Model file name\n");
}

- (BOOL)processArguments:(NSArray *)args
{
  BOOL error = NO;
  NS_DURING {
    
    // process parameters
    NSArray *args = [[NSProcessInfo processInfo] arguments];
    int i, argsCount = [args count];
    for (i = 1; i < argsCount; ++i) {
      if ([[args objectAtIndex: i] isEqualToString: @"--help"]) {
        [self printUsage];
        error = YES;
        goto done;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--gpu"]) {
        ++i;
        gpuDevice = [[args objectAtIndex: i] intValue];
        continue;

        if (gpuDevice < 0) {
          printf("ERROR: Invalid GPU device number.\n");
          [self printUsage];
          error = YES;
          break;
        }
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--sweep"]) {
        doSweep = YES;
        continue;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--random"]) {
        doRandom = YES;
        ++i;
        randomRuns = [[args objectAtIndex: i] intValue];
        continue;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--evolve"]) {
        doEvolutionaryStrategy = YES;
        continue;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--mcmc"]) {
        doMCMC = YES;
        continue;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--run"]) {
        ++i;
        startRun = [[args objectAtIndex: i] intValue];
        continue;
      }
            
      if ([[args objectAtIndex: i] isEqualToString: @"--perturb"]) {
        ++i;
        doPerturbation = YES;
        perturbationName = [args objectAtIndex: i];
        continue;
      }

#if 0
      if ([[args objectAtIndex: i] isEqualToString: @"--range"]) {
        doAutoRange = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--analyze"]) {
        doAnalyze = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--arff"]) {
        format = 1;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--libsvm"]) {
        format = 2;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--rsearch"]) {
        doRSearch = YES;
        ++i;
        randomRuns = [[args objectAtIndex: i] intValue];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--learn"]) {
        doLearn = YES;
        ++i;
        trainFile = [args objectAtIndex: i];
        continue;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--sample"]) {
        doSample = YES;
        ++i;
        paramSamples = [[args objectAtIndex: i] intValue];
        ++i;
        paramSD = [[args objectAtIndex: i] doubleValue];
        
        if (paramSamples <= 0) {
          printf("ERROR: Number of samples for parameter sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        if (paramSD <= 0.0) {
          printf("ERROR: Standard deviation for parameter sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--asample"]) {
        doAllSample = YES;
        ++i;
        allSamples = [[args objectAtIndex: i] intValue];
        ++i;
        allSD = [[args objectAtIndex: i] doubleValue];
        
        if (allSamples <= 0) {
          printf("ERROR: Number of samples for parameter sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        if (allSD <= 0.0) {
          printf("ERROR: Standard deviation for parameter sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--latin"]) {
        doLatin = YES;
        ++i;
        latinSamples = [[args objectAtIndex: i] intValue];
        ++i;
        latinSD = [[args objectAtIndex: i] doubleValue];
        
        if (latinSamples <= 0) {
          printf("ERROR: Number of samples for Latin hypercube sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        if (latinSD <= 0.0) {
          printf("ERROR: Standard deviation for Latin hypercube sampling cannot be negative or zero.\n");
          [self printUsage];
          error = YES;
          break;
        }
        
        continue;
      }
#endif
      
      modelFilename = [args objectAtIndex: i];
    }
    
  } NS_HANDLER {
    
    printf("ERROR: Invalid usage.\n\n");
    [self printUsage];
    error = YES;
    
  } NS_ENDHANDLER
  
  if (error) goto done;
  
  // load parameter file
  NSMutableDictionary *runParameters = [[NSMutableDictionary dictionaryWithContentsOfFile: modelFilename] retain];
  if (!runParameters) {
    printf("ERROR: Could not open parameter file: %s\n", [modelFilename UTF8String]);
    error = YES;
    goto done;
  }
  
  // determine starting run number
  if (startRun == -1) startRun = [[runParameters objectForKey: @"outputRun"] intValue];
  
  // load the model
  [self loadModel: runParameters];
  
  //
  // post load checks
  //
  
  if (doPerturbation) {
    id s = [perturbations objectForKey: perturbationName];
    if (!s) {
      printf("ERROR: Invalid perturbation: %s\n\n", [perturbationName UTF8String]);
      [self printUsage];
      error = YES;
      goto done;
    }
  }

done:
  return !error;
}

#if 0
#pragma mark == MANAGE MODELS AND SIMULATIONS ==
#endif

- newInstanceForModel:(NSDictionary *)modelSpec
{
  int i;
  
  if (!modelSpec) modelSpec = model;
  NSDictionary *m = [modelSpec objectForKey: @"models"];

  // construct the sub models
  NSMutableArray *subModels = [NSMutableArray array];
  NSArray *modelKeys = [m allKeys];
  for (i = 0; i < [modelKeys count]; ++i) {
    NSString *key = [modelKeys objectAtIndex: i];
    if ([key isEqualToString: @"spatialModel"]) continue;
    NSString *s = [m objectForKey: key];
    id aModel = [modelSpec objectForKey: s];
    if (!aModel) {
      printf("Cannot find %s: %s\n", [key UTF8String], [s UTF8String]);
      return nil;
    }
    
    // construct the model
    id aType = [aModel objectForKey: @"type"];
    if ([self isDebug]) printf("model type = %s\n", [aType UTF8String]);
    if (!aType) {
      printf("Type not defined for model: %s\n", [s UTF8String]);
      return nil;
    }
    
    id aClass = objc_getClass([aType UTF8String]);
    if (!aClass) {
      printf("ERROR: Model type does not exist: %s\n", [aType UTF8String]);
      return nil;
    }
    
    id rd = [[aClass alloc] initWithModel: aModel andKey: key andController: self andParent: nil];
    if (!rd) {
      printf("ERROR: Could not create model: %s\n", [s UTF8String]);
      return nil;
    }
    
    NSDictionary *simControl = [model objectForKey: @"simulationControl"];
    NSString *param = [simControl objectForKey: @"outputRun"];
    if (param) [rd setRunNumber: [param intValue]];

    [subModels addObject: rd];
  }
  
  [modelInstances addObject: subModels];
  [modelSpecifications addObject: modelSpec];
  
  return subModels;
}

- loadModel: (NSDictionary *)aModel
{
  int i;
  NSString *param;
  
  model = [aModel retain];
  modelInstances = [NSMutableArray new];
  modelSpecifications = [NSMutableArray new];
  
  isDebug = NO;
  param = [aModel objectForKey: @"debug"];
  if (param) isDebug = [param boolValue];
  
  isBinaryFile = YES;
  param = [aModel objectForKey: @"binaryFile"];
  if (param) isBinaryFile = [param boolValue];
  
  name = [model objectForKey: @"name"];
  CHECK_PARAM(name, "name");
  
  NSDictionary *m = [model objectForKey: @"models"];
  CHECK_PARAM(m, "models");

  NSDictionary *simControl = [model objectForKey: @"simulationControl"];
  CHECK_PARAM(simControl, "simulationControl");

  param = [simControl objectForKey: @"outputRun"];
  CHECK_PARAM(param, "outputRun");
  outputRun = [param intValue];

  param = [simControl objectForKey: @"outputFrequency"];
  CHECK_PARAM(param, "outputFrequency");
  outputFrequency = [param doubleValue];

  param = [simControl objectForKey: @"simulateTime"];
  CHECK_PARAM(param, "simulateTime");
  simulateTime = [param doubleValue];

  param = [simControl objectForKey: @"randomSeed"];
  CHECK_PARAM(param, "randomSeed");
  int randomSeed = [param intValue];
  srand(randomSeed);
  
  // spatial model
  NSString *s = [m objectForKey: @"spatialModel"];
  if (s) {
    NSDictionary *d = [model objectForKey: s];
    if (!d) {
      printf("Cannot find spatialModel: %s\n", [s UTF8String]);
      return nil;
    }
    if ([self isDebug]) printf("Processing spatial model.\n");
    spatialModel = [[SpatialModel alloc] initWithModel: d];
  }

  // create an initial model instance
  [self newInstanceForModel:nil];
  
  // gather list of species
  totalSpecies = [NSMutableArray new];
  numTotalSpecies = 0;
  NSArray *subModels = [modelInstances objectAtIndex: 0];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    numTotalSpecies += [m numOfSpecies];
  }
  
  // look for random search setup
  randomParameters = nil;
  notRandomParameters = nil;
  NSDictionary *randomSearch = [model objectForKey: @"randomSearch"];
  if (randomSearch) {
    // Specify which parameters to randomize?
    randomParameters = [randomSearch objectForKey: @"randomParameters"];
    if ([randomParameters count] == 0) randomParameters = nil;
  
    // Specify which parameters not to randomize?
    notRandomParameters = [randomSearch objectForKey: @"notRandomParameters"];
    if ([notRandomParameters count] == 0) notRandomParameters = nil;
  }

  // perturbations is optional
  perturbations = [model objectForKey: @"perturbations"];

  return self;
}

- (void)releaseModel
{
  int i, j;
  [totalSpecies release]; totalSpecies = nil;
  [modelSpecifications release]; modelSpecifications = nil;
  for (i = 0; i < [modelInstances count]; ++i) {
    NSMutableArray *subModels = [modelInstances objectAtIndex: i];
    for (j = 0; j < [subModels count]; ++j) [[subModels objectAtIndex: j] release];
  }
  [modelInstances removeAllObjects];
  [modelInstances release]; modelInstances = nil;
  [spatialModel release]; spatialModel = nil;
  [modelDirectory release]; modelDirectory = nil;
  [model release]; model = nil;
}

- (void)dealloc
{
  [self releaseModel];
  [delegate release];
  
  [super dealloc];
}

- (void)setDelegate: anObj
{
  delegate = anObj;
  if (delegate) {
    [delegate retain];
    // cache whether the delegate responds to messages
    if ([delegate respondsToSelector: @selector(initializeSimulationForModel:)]) respondsToInit = YES;
    else respondsToInit = NO;
    if ([delegate respondsToSelector: @selector(didInitializeSimulation:)]) respondsToDidInit = YES;
    else respondsToDidInit = NO;
    if ([delegate respondsToSelector: @selector(willRunSimulation:)]) respondsToWillRun = YES;
    else respondsToWillRun = NO;
    if ([delegate respondsToSelector: @selector(didTransferDataToGPU:sender:)]) respondsToDidTransfer = YES;
    else respondsToDidTransfer = NO;
    if ([delegate respondsToSelector: @selector(didFinishSimulation:)]) respondsToDidFinish = YES;
    else respondsToDidFinish = NO;
  } else {
    respondsToInit = NO;
    respondsToDidInit = NO;
    respondsToWillRun = NO;
    respondsToDidTransfer = NO;
    respondsToDidFinish = NO;
  }

}

- delegate { return delegate; }

- (NSDictionary *)model { return model; }
- (NSString *)modelName { return name; }
- (NSString *)modelDirectory { return modelDirectory; }
- (void)setModelDirectory: (NSString *)aDir
{
  if (modelDirectory) [modelDirectory release];
  if (!aDir || ([aDir length] == 0)) modelDirectory = nil;
  else modelDirectory = [aDir retain];
}
- (int)outputRun { return outputRun; }
- (double)outputFrequency { return outputFrequency; }
- (double)simulateTime { return simulateTime; }
- (double)currentTime { return currentTime; }
- (SpatialModel *)spatialModel { return spatialModel; }
//- (NSArray *)subModels { return subModels; }
//- (NSArray *)species { return species; }
- (NSArray *)modelInstances { return modelInstances; }
- (int)numModelInstances { return [modelInstances count]; }
- (id)getModel:(int)modelIndex withName: (NSString *)aName
{
  int i;
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    if ([aName isEqualToString: [m modelName]]) return m;
  }
  return nil;
}
- (NSArray *)modelSpecifications { return modelSpecifications; }
- (NSDictionary *)getSpecificationForModel:(int)modelIndex { return [modelSpecifications objectAtIndex: modelIndex]; }

- (BOOL)isDebug { return isDebug; }
- (void)setDebug: (BOOL)aFlag { isDebug = aFlag; }

- (BOOL)isBinaryFile { return isBinaryFile; }
- (void)setBinaryFile: (BOOL)aFlag { isBinaryFile = aFlag; }

- (int)fileState { return fileState; }
- (void)setFileState: (int)aState { fileState = aState; }

- (BOOL)saveModelSpecification
{
  return [self saveModelSpecification: model forRun: outputRun];
}
- (BOOL)saveSpecificationForModel:(int)modelIndex forRun:(int)aNum
{
  return [self saveModelSpecification: [self getSpecificationForModel: modelIndex] forRun: aNum];
}
- (BOOL)saveModelSpecification:(NSDictionary *)aModel forRun:(int)aNum
{
  NSString *outputFile = [NSString stringWithFormat: @"%@_run_%d.bioswarm", [aModel objectForKey: @"name"], aNum];
#if GNUSTEP
  [[NSPropertyListSerialization dataFromPropertyList: aModel
                                              format: NSPropertyListXMLFormat_v1_0
                                    errorDescription: NULL]
   writeToFile:outputFile atomically: YES];
#else
  [aModel writeToFile: outputFile atomically: NO];
#endif
  
  return YES;
}

- (int)constructParameterList:(NSArray *)includeList exclude:(NSArray *)excludeList
                        names:(NSMutableArray **)parameterNames indexes:(NSMutableArray **)parameterIndexes
                       models:(NSMutableArray **)parameterModels
{
  int i, j, k;
  
  // determine parameters
  int totalParameters = 0;
  if (parameterNames) *parameterNames = [NSMutableArray new];
  if (parameterIndexes) *parameterIndexes = [NSMutableArray new];
  if (parameterModels) *parameterModels = [NSMutableArray new];
  NSArray *subModels = [modelInstances objectAtIndex: 0];

  // validate the include and exclude list
  for (i = 0; i < [includeList count]; ++i) {
    NSString *testName = [includeList objectAtIndex: i];
    BOOL flag = NO;
    for (j = 0; j < [subModels count]; ++j) {
      id aModel = [subModels objectAtIndex: j];
      int numParams = [aModel numOfParameters];

      for (k = 0; k < numParams; ++k) {
	NSString *pn = [aModel nameOfParameter: k];
	if ([pn isEqualToString: testName]) { flag = YES; break; }
      }
      if (flag) break;
    }
    if (!flag) {
      printf("WARNING: Parameter (%s) in include list is not part of model.\n", [testName UTF8String]);
    }
  }

  for (i = 0; i < [excludeList count]; ++i) {
    NSString *testName = [includeList objectAtIndex: i];
    BOOL flag = NO;
    for (j = 0; j < [subModels count]; ++j) {
      id aModel = [subModels objectAtIndex: j];
      int numParams = [aModel numOfParameters];

      for (k = 0; k < numParams; ++k) {
	NSString *pn = [aModel nameOfParameter: k];
	if ([pn isEqualToString: testName]) { flag = YES; break; }
      }
      if (flag) break;
    }
    if (!flag) {
      printf("WARNING: Parameter (%s) in exclude list is not part of model.\n", [testName UTF8String]);
    }
  }

  // go through each submodel
  for (j = 0; j < [subModels count]; ++j) {
    id aModel = [subModels objectAtIndex: j];
    int numParams = [aModel numOfParameters];

    for (k = 0; k < numParams; ++k) {
      NSString *pn = [aModel nameOfParameter: k];
      if (includeList && ([includeList indexOfObject: pn] == NSNotFound)) continue;
      if (excludeList && ([excludeList indexOfObject: pn] != NSNotFound)) continue;
      
      // save parameter info
      ++totalParameters;
      if (parameterNames) [*parameterNames addObject: pn];
      if (parameterIndexes) [*parameterIndexes addObject: [NSNumber numberWithInt: k]];
      if (parameterModels) [*parameterModels addObject: [NSNumber numberWithInt: j]];
    }
  }

  // make sure parameter ranges are setup
  for (j = 0; j < [subModels count]; ++j) {
    id aModel = [subModels objectAtIndex: j];
    if (![aModel setupParameterRanges: nil checkNames: *parameterNames]) return 0;
  }

  return totalParameters;
}

- (int)constructSpeciesList:(NSMutableArray **)speciesNames indexes:(NSMutableArray **)speciesIndexes
                     models:(NSMutableArray **)speciesModels
{
  int i, j, k;
  
  // determine species
  int totalSpecies = 0;
  if (speciesNames) *speciesNames = [NSMutableArray new];
  if (speciesIndexes) *speciesIndexes = [NSMutableArray new];
  if (speciesModels) *speciesModels = [NSMutableArray new];
  NSArray *subModels = [modelInstances objectAtIndex: 0];
  
  // go through each submodel
  for (j = 0; j < [subModels count]; ++j) {
    id aModel = [subModels objectAtIndex: j];
    int numSpecies = [aModel numOfSpecies];
    
    for (k = 0; k < numSpecies; ++k) {
      NSString *pn = [aModel nameOfSpecies: k];
      
      // save species info
      ++totalSpecies;
      if (speciesNames) [*speciesNames addObject: pn];
      if (speciesIndexes) [*speciesIndexes addObject: [NSNumber numberWithInt: k]];
      if (speciesModels) [*speciesModels addObject: [NSNumber numberWithInt: j]];
    }
  }
  
  return totalSpecies;
}

#if 0
#pragma mark == PERFORM SIMULATIONS ==
#endif

- (void)allocateSimulationWithEncode: (NSString *)anEncode
{
  if (fileState == BCMODEL_NOFILE_STATE) [self allocateSimulationWithEncode: anEncode fileState: fileState];
  else {
    // check if would open too many files
    if (numTotalSpecies * [modelInstances count] > 200) {
      fileState = BCMODEL_APPEND_AND_CLOSE_STATE;
      [self allocateSimulationWithEncode: anEncode fileState: fileState];
    } else {
      fileState = BCMODEL_WRITE_STATE;
      [self allocateSimulationWithEncode: anEncode fileState: fileState];
    }
  }
}

- (void)allocateSimulationWithEncode: (NSString *)anEncode fileState: (int)aState
{
  int i;
  for (i = 0; i < [modelInstances count]; ++i)
    [self allocateSimulation: i withEncode: anEncode fileState: aState];
}

- (void)allocateSimulation: (int)modelIndex withEncode: (NSString *)anEncode fileState: (int)aState
{
  int i;
  
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    [m allocateSimulationWithEncode: anEncode fileState: aState];
  }
}

- (BOOL)writeData
{
  return [self writeDataWithCheck: YES];
}

- (BOOL)writeDataWithCheck: (BOOL)aFlag
{
  int i;
  BOOL res = YES;
  
  for (i = 0; i < [modelInstances count]; ++i)
    res &= [self writeDataForModel: i withCheck: aFlag];

  return res;
}

- (BOOL)writeDataForModel:(int)modelIndex withCheck: (BOOL)aFlag
{
  int i;
  BOOL res = YES;
  
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    res &= [m writeDataWithCheck: aFlag];
  }
  
  return res;
}

- (BOOL)readData
{
  int i;
  BOOL res = YES;

  for (i = 0; i < [modelInstances count]; ++i)
    res &= [self readDataForModel: i];

  return res;
}

- (BOOL)readDataForModel:(int)modelIndex
{
  int i;
  BOOL res = YES;
  
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    res &= [m readData];
  }
  
  return res;
}

- (void)initializeSimulation
{
  int i;
  for (i = 0; i < [modelInstances count]; ++i)
    [self initializeSimulationForModel: i];
}

- (void)initializeSimulationForModel:(int)modelIndex
{
  int i;
  
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    [m initializeSimulation];
  }
}

- (void)runCPUSimulation
{
}

- (void)doSimulationRun
{
  if (gpuFuncs) [self runGPUSimulation: (getGPUFunctions *)gpuFuncs];
  else [self runCPUSimulation];
}

//                                                                                                                                                             
// Parameter sweep                                                                                                                                             
//                                                                                                                                                             
- (void)doSweepSimulations
{
  NSAutoreleasePool *pool, *simPool;
  id s;
  BOOL done = YES;
  
  // save the base model
  NSDictionary *baseModel = [model retain];
  
  pool = [NSAutoreleasePool new];
  
  NSDictionary *sweep = [baseModel objectForKey: @"sweepParameters"];
  CHECK_PARAMF(sweep, "sweepParameters", done);
  if (!done) return;
  
  NSArray *sweepParams = [sweep objectForKey: @"parameters"];
  CHECK_PARAMF(sweepParams, "parameters", done);
  if (!done) return;

  int i, j, numParams = [sweepParams count];
  NSString *paramModel[numParams];
  double paramValue[numParams];
  double paramStart[numParams];
  double paramEnd[numParams];
  double paramStep[numParams];
  
  for (i = 0; i < numParams; ++i) {
    NSString *p = [sweepParams objectAtIndex: i];

    // find model parameter is in
    paramModel[i] = nil;
    NSDictionary *m = [baseModel objectForKey: @"models"];
    NSArray *modelKeys = [m allKeys];
    for (j = 0; j < [modelKeys count]; ++j) {
      NSString *key = [modelKeys objectAtIndex: j];
      NSString *val = [m objectForKey: key];
      id aModel = [baseModel objectForKey: val];
      s = [aModel objectForKey: p];
      if (s) { paramModel[i] = val; break; }
    }
    if (!paramModel[i]) {
      printf("Could not find model with parameter: %s\n", [p UTF8String]);
      return;
    }
    
    NSString *pn = [NSString stringWithFormat: @"%@_start", p];
    s = [sweep objectForKey: pn];
    CHECK_PARAMF(s, [pn UTF8String], done);
    if (!done) return;
    paramStart[i] = [s doubleValue];
    paramValue[i] = paramStart[i];
    
    pn = [NSString stringWithFormat: @"%@_end", p];
    s = [sweep objectForKey: pn];
    CHECK_PARAMF(s, [pn UTF8String], done);
    if (!done) return;
    paramEnd[i] = [s doubleValue];
    
    pn = [NSString stringWithFormat: @"%@_step", p];
    s = [sweep objectForKey: pn];
    CHECK_PARAMF(s, [pn UTF8String], done);
    if (!done) return;
    paramStep[i] = [s doubleValue];
  }

  // release the initial model
  //[self releaseModel];

  int sweepRun = startRun + 1;
  done = NO;
  while (!done) {
    NSMutableDictionary *simParameters = [[NSMutableDictionary alloc] initWithDictionary: baseModel];
    for (i = 0; i < numParams; ++i) {
      NSString *p = [sweepParams objectAtIndex: i];
      NSMutableDictionary *m = [simParameters objectForKey: paramModel[i]];
      [m setObject: [NSNumber numberWithDouble: paramValue[i]] forKey: p];
      if ([self isDebug]) printf("%lf ", paramValue[i]);
    }
    if ([self isDebug]) printf("\n");
    
    //simPool = [NSAutoreleasePool new];
    
    // create model
    NSMutableDictionary *simControl = [simParameters objectForKey: @"simulationControl"];
    [simControl setObject: [NSNumber numberWithInt: rand()] forKey: @"randomSeed"];
    [simControl setObject: [NSNumber numberWithInt: sweepRun] forKey: @"outputRun"];
    //[self loadModel: simParameters];
    [self newInstanceForModel: simParameters];
    
    // save parameter set
    [self saveModelSpecification: simParameters forRun: sweepRun];
    //[self saveModelSpecification];

    //[self releaseModel];
    //[simPool drain];
    //[simParameters release];

    // next parameter values
    paramValue[0] += paramStep[0];
    for (i = 0; i < numParams; ++i) {
      if (paramValue[i] > paramEnd[i]) {
        if (i == (numParams - 1)) {
          done = YES;
          break;
        } else {
          paramValue[i+1] += paramStep[i+1];
          paramValue[i] = paramStart[i];
        }
      }
    }
    
    ++sweepRun;
  }
  
  // run simulation
  if (gpuFuncs) [self runGPUSimulation: (getGPUFunctions *)gpuFuncs];
  else [self runCPUSimulation];

  // save
  
  [pool drain];

  // load the base model back
  [self releaseModel];
  [self loadModel: baseModel];
  [baseModel release];
}

//
// Simulations with random search in parameter space
//
- (void)doRandomParameterSimulations
{
  NSAutoreleasePool *pool;
  
  // save the base model
  NSDictionary *baseModel = [model retain];
  
  pool = [NSAutoreleasePool new];

  outputRun = startRun;
  int i, j, k;
  for (i = 1; i < randomRuns; ++i) {
    NSMutableDictionary *simParameters = [NSMutableDictionary dictionaryWithDictionary: baseModel];
    NSMutableDictionary *simControl = [simParameters objectForKey: @"simulationControl"];
    NSDictionary *modelSet = [simParameters objectForKey: @"models"];
    NSArray *modelKeys = [modelSet allKeys];
    
    // go through each submodel
    // randomize each parameter
    NSArray *subModels = [modelInstances objectAtIndex: 0];
    for (j = 0; j < [subModels count]; ++j) {
      id aModel = [subModels objectAtIndex: j];
      int numParams = [aModel numOfParameters];
      NSString *key = [modelKeys objectAtIndex: j];
      NSString *val = [modelSet objectForKey: key];
      NSMutableDictionary *modelDict = [simParameters objectForKey: val];
      for (k = 0; k < numParams; ++k) {
        NSString *pn = [aModel nameOfParameter: k];
        if (randomParameters && ([randomParameters indexOfObject: pn] == NSNotFound)) continue;
        if (notRandomParameters && ([notRandomParameters indexOfObject: pn] != NSNotFound)) continue;
        double newValue = [aModel randomValueforParameter: k];
        [modelDict setObject: [NSNumber numberWithDouble: newValue] forKey: pn];
        if ([self isDebug]) printf("Randomize %s = %lf\n", [pn UTF8String], newValue);
      }
    }
    
    // new random seed
    [simControl setObject: [NSNumber numberWithInt: rand()] forKey: @"randomSeed"];
    [simControl setObject: [NSNumber numberWithInt: outputRun+i] forKey: @"outputRun"];

    // create random model and save parameter set
    subModels = [self newInstanceForModel: simParameters];
    [self saveModelSpecification: simParameters forRun: outputRun+i];
    //for (j = 0; j < [subModels count]; ++j) {
      //id aModel = [subModels objectAtIndex: j];
      //[aModel setRunNumber: outputRun+i];
    //}
  }
  outputRun = startRun;

  // run simulation
  [self doSimulationRun];
  
  [pool drain];
  
  // load the base model back
  [self releaseModel];
  [self loadModel: baseModel];
  [baseModel release];
}

//
// Fit parameters using evolutionary strategy
//
- (void)doEvolutionaryStrategy
{
  NSAutoreleasePool *pool;
  
  // save the base model
  NSDictionary *baseModel = [model retain];
  
  pool = [NSAutoreleasePool new];
  
  EvolutionaryStrategy *es = [[EvolutionaryStrategy alloc] initWithController: self];
  if (es) [es performEvolution];

  [pool drain];
  
  // load the base model back
  [self releaseModel];
  [self loadModel: baseModel];
  [baseModel release];
}

//
// Markov chain Monte Carlo for Bayesian analysis
//
- (void)doMCMC
{
  NSAutoreleasePool *pool;
  
  // save the base model
  NSDictionary *baseModel = [model retain];
  
  pool = [NSAutoreleasePool new];
  
  MCMC *mcmc = [MCMC newMCMCWithController: self];
  if (mcmc) [mcmc performMCMC];
  [mcmc release];
  
  [pool drain];
  
  // load the base model back
  [self releaseModel];
  [self loadModel: baseModel];
  [baseModel release];
}

- (void)startSimulation
{
  if (doSweep) {
    [self doSweepSimulations];
  } else if (doAutoRange) {
    //res = range_tongue(runParameters, startRun);                                                                                                  
  } else if (doRandom) {
    [self doRandomParameterSimulations];
  } else if (doEvolutionaryStrategy) {
    [self doEvolutionaryStrategy];
  } else if (doMCMC) {
    [self doMCMC];
  } else if (doRSearch) {
    //res = rsearch_tongue(runParameters, randomRuns, startRun);                                                                                    
  } else if (doAnalyze) {
    //res = analyze_tongue(runParameters, startRun, format);                                                                                        
  } else if (doLearn) {
    //res = learn_tongue(runParameters, trainFile);                                                                                                 
  } else if (doSample) {
    //res = sample_tongue(runParameters, paramSamples, paramSD, startRun);                                                                          
  } else if (doAllSample) {
    //res = asample_tongue(runParameters, allSamples, allSD, startRun);                                                                             
  } else if (doLatin) {
    //res = lhs_tongue(runParameters, latinSamples, latinSD, startRun);                                                                             
  } else {
    [self doSimulationRun];
  }
}

- (void)cleanupSimulation
{
}

#if 0
#pragma mark == SIMULATION REPLAY ==
#endif

//
// Load simulation data for replay and analysis
//

- (int)numOfIterations { return numOfIterations; }
- (int)currentIteration { return currentIteration; }
- (void)setCurrentIteration: (int)aNum
{
  currentIteration = aNum;
  if (currentIteration < 0) currentIteration = 0;
  if (currentIteration > numOfIterations) currentIteration = numOfIterations;
}
- (int)currentSpecies { return currentSpecies; }
- (void)setCurrentSpecies: (int)aNum { currentSpecies = aNum; }
- (float)speciesMin
{
#if 0
  float val;
  if (currentRange) val = currentMin[currentSpecies][currentIteration];
  else val = minSpecies[currentSpecies];
  //printf("speciesMin: (%d %d): %f\n", currentSpecies, currentIteration, val);
  return val;
#endif
  return 0.0;
}
- (float)speciesMax
{
#if 0
  float val;
  if (currentRange) val = currentMax[currentSpecies][currentIteration];
  else val = maxSpecies[currentSpecies];
  //printf("speciesMax: (%d %d): %f\n", currentSpecies, currentIteration, val);
  return val;
#endif
  return 0.0;
}
- (void)setCurrentRange: (int)aNum { currentRange = aNum; }

- (void)setupSimulationReplay
{
  [self allocateSimulationWithEncode: [Grid2DArray floatEncode] fileState: BCMODEL_READ_STATE];
  
  // determine the number of iterations
  numOfIterations = 0;
  BOOL more = YES;
  do {
    more = [self readData];
    if (more) ++numOfIterations;
  } while (more);
  printf("numOfIterations: %d\n", numOfIterations);

  // start at first iteration
  currentIteration = 0;
  [self rewindFiles];
  [self readData];
}

- (void)rewindFiles
{
  [self rewindFilesForModel: 0];
}

- (void)rewindFilesForModel:(int)modelIndex
{
  int i;
  
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    [m rewindFiles];
  }
}

- (void)nextIteration
{
  if (currentIteration == numOfIterations) return;
  ++currentIteration;
  [self readData];
}

#if 0
#pragma mark == RANDOM SEARCH ==
#endif

//
// Random search
//
- (NSArray *)randomParameters { return randomParameters; }
- (NSArray *)notRandomParameters { return notRandomParameters; }

#if 0
#pragma mark == PERTURBATIONS ==
#endif

- (BOOL)isPerturbation { return doPerturbation; }
- (NSDictionary *)perturbations { return perturbations; }

- (void)setPerturbation: (NSString *)pName
{
  if (pName) {
    if (![perturbations objectForKey: pName]) {
      printf("ERROR: Unknown perturbation: %s\n", [pName UTF8String]);
    } else {
      doPerturbation = YES;
      perturbationName = pName;
    }
  } else {
    doPerturbation = NO;
    perturbationName = nil;
  }
}

- (void)applyPerturbation: (NSString *)pName
{
  int i;

  NSDictionary *perturb = [perturbations objectForKey: pName];
  if (!perturb) {
    printf("ERROR: Unknown perturbation: %s\n", [pName UTF8String]);
    return;
  }

  for (i = 0; i < [modelInstances count]; ++i)
    [self applyPerturbation: pName forModel: i];
}

- (void)applyPerturbation: (NSString *)pName forModel: (int)modelIndex
{
  NSDictionary *perturb = [perturbations objectForKey: pName];
  NSString *mName = [perturb objectForKey: @"model"];
  
#if 0
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  NSDictionary *modelSpec = [modelSpecifications objectAtIndex: modelIndex];
  NSDictionary *modelSet = [modelSpec objectForKey: @"models"];
  NSArray *modelKeys = [modelSet allKeys];

  NSUInteger idx = [modelKeys indexOfObject: mName];
  id <BiologicalModel> aModel = [subModels objectAtIndex: idx];
#else
  id <BiologicalModel> aModel = [self getModel: modelIndex withName: mName];
  if (!aModel) {
    printf("ERROR: Unknown model (%s) for perturbation (%s).\n", [mName UTF8String], [pName UTF8String]);
    return;
  }
#endif
  
  id param, val;
  int i;
  NSString *desc = [perturb objectForKey: @"description"];  
  if ([self isDebug]) printf("Applying perturbation (%s): %s\n", [pName UTF8String], [desc UTF8String]);

  // change (absolute) value of all species in a model
  param = [perturb objectForKey: @"value"];
  if (param) {
    if (![aModel setValueForAllSpecies: [param doubleValue]]) {
      printf("WARNING: Could not apply perturbation (%s): value = %s\n", [pName UTF8String], [param UTF8String]);
    }
  }

  // change (absolute) value for specific species in a model
  param = [perturb objectForKey: @"values"];
  if (param) {
    NSArray *speciesKeys = [param allKeys];
    for (i = 0; i < [speciesKeys count]; ++i) {
      NSString *key = [speciesKeys objectAtIndex: i];
      val = [param objectForKey: key];
      if (![aModel setValue: [val doubleValue] forSpecies: key]) {
        printf("WARNING: Could not apply perturbation (%s): %s = %s\n", [pName UTF8String], [key UTF8String], [val UTF8String]);
      }
    }
  }

  // change (relative) value for specific species in a model
  param = [perturb objectForKey: @"adjust"];
  if (param) {
    NSArray *speciesKeys = [param allKeys];
    for (i = 0; i < [speciesKeys count]; ++i) {
      NSString *key = [speciesKeys objectAtIndex: i];
      val = [param objectForKey: key];
      if (![aModel adjustValue: [val doubleValue] forSpecies: key]) {
        printf("WARNING: Could not apply perturbation (%s): %s = %s\n", [pName UTF8String], [key UTF8String], [val UTF8String]);
      }
    }
  }
  
}

@end

#if 0
#pragma mark == GPU ==
#endif

//
// GPU
//

@implementation SimulationController (GPU)

- (BOOL)generateGPUCode: (NSString *)prefixName
{
  int i;
  NSString *outFile;
  NSMutableString *GPUCode;
  NSMutableString *functions;
  NSMutableString *code;
  NSMutableString *invokeCode;
  NSMutableString *execCode;
  
  code = [NSMutableString new];
  [code appendString: @"#include <stdio.h>\n"];
  [code appendString: @"#include <unistd.h>\n\n"];
  BOOL debugFlag = isDebug;
  NSArray *subModels = [modelInstances objectAtIndex: 0];
  for (i = 0; i < [subModels count]; ++i) debugFlag |= [[subModels objectAtIndex: i] isDebug];
  if (debugFlag) [code appendString: @"#include \"cuPrintf.cu.m\"\n\n"];
  [code appendString: @"extern \"C\" {\n"];
  [code appendString: @"#include <BioSwarm/GPUDefines.h>\n"];
  //[code appendString: @"#include <BioSwarm/GPU_sem.h>\n"];

  [code appendString: @"\nvoid cudacheck(const char *message) {\n"];
  [code appendString: @"  cudaError_t error = cudaGetLastError();\n"];
  [code appendString: @"  if (error!=cudaSuccess) printf(\"cudaERROR: %s : %i (%s)\\n\", message, error, cudaGetErrorString(error));\n"];
  [code appendString: @"}\n\n"];
  
  invokeCode = [NSMutableString new];
  [invokeCode appendString: @"\n// main execution function\n"];
  [invokeCode appendFormat: @"void %@_invokeGPUKernel(void *controller, ModelGPUData** data, double startTime, double nextTime)\n", prefixName];
  [invokeCode appendString: @"{\n"];
  
  functions = [NSMutableString new];
  [functions appendString: @"\n// Pointers to gpu functions for models\n"];
  [functions appendString: @"void *getGPUFunctionForModel(int aModel, int value)\n"];
  [functions appendString: @"{\n"];
  [functions appendString: @"  switch (aModel) {\n"];
  [functions appendString: @"  case -4:\n"];
  [functions appendString: @"    cudaSetDevice(value);\n"];
  [functions appendString: @"    cudacheck(\"set GPU device\");\n"];
  [functions appendString: @"    return NULL;\n"];
  [functions appendString: @"  case -3:\n"];
  [functions appendString: @"    cudaDeviceReset();\n"];
  [functions appendString: @"    return NULL;\n"];
  [functions appendString: @"  case -2:\n"];
  if (debugFlag) [functions appendString: @"    cudaPrintfInit();\n"];
  [functions appendString: @"    return NULL;\n"];
  [functions appendFormat: @"  case -1:\n", i];
  [functions appendFormat: @"    return (void *)&%@_invokeGPUKernel;\n", prefixName];

  execCode = [NSMutableString new];

  for (i = 0; i < [subModels count]; ++i) {
    NSString *newPrefix = [NSString stringWithFormat: @"%@_%d", prefixName, i];

    NSDictionary *GPUDictionary = [[subModels objectAtIndex: i] controllerGPUCode: newPrefix];
    if (!GPUDictionary) continue;
    
    GPUCode = [NSMutableString new];
    //[GPUCode appendString: [GPUDictionary objectForKey: @"header"]];
    [GPUCode appendString: [GPUDictionary objectForKey: @"defines"]];
    [GPUCode appendString: @"\n// Allocation\n"];
    [GPUCode appendString: @"void "];
    [GPUCode appendString: [GPUDictionary objectForKey: @"allocation function"]];
    [GPUCode appendString: [GPUDictionary objectForKey: @"allocation code"]];
    [GPUCode appendString: @"\n// Data Transfer\n"];
    [GPUCode appendString: @"void "];
    [GPUCode appendString: [GPUDictionary objectForKey: @"transfer function"]];
    [GPUCode appendString: [GPUDictionary objectForKey: @"transfer code"]];
    [GPUCode appendString: @"\n// Execution\n"];
    [GPUCode appendString: @"void "];
    [GPUCode appendString: [GPUDictionary objectForKey: @"execution function"]];
    [GPUCode appendString: @"\n{\n"];
    [GPUCode appendString: [GPUDictionary objectForKey: @"structure"]];
    [GPUCode appendString: [GPUDictionary objectForKey: @"execution variables"]];

    if ([GPUDictionary objectForKey: @"solo execution code"]) {
      [GPUCode appendString: [GPUDictionary objectForKey: @"solo execution code"]];
    } else {
      [GPUCode appendString: @"\n   // Invoke kernel\n"];
      [GPUCode appendString: @"   double currentTime = startTime;\n"];
      [GPUCode appendString: @"   int done = 0;\n"];
      [GPUCode appendString: @"   while (!done) {\n"];
      [GPUCode appendString: [GPUDictionary objectForKey: @"execution code"]];
      [GPUCode appendFormat: @"\n      currentTime += %@_ptrs->dt;\n", newPrefix];
      [GPUCode appendString: @"      if (TIME_EQUAL(currentTime, nextTime) || currentTime > nextTime) done = 1;\n"];
      [GPUCode appendString: @"   }\n"];
    }
    [GPUCode appendString: @"}\n"];
    
    [GPUCode appendString: @"\n// Release\n"];
    [GPUCode appendString: @"void "];
    [GPUCode appendString: [GPUDictionary objectForKey: @"release function"]];
    [GPUCode appendString: [GPUDictionary objectForKey: @"release code"]];
    [GPUCode appendString: [GPUDictionary objectForKey: @"footer"]];

    outFile = [NSString stringWithFormat: @"%@.cu", newPrefix];
    [GPUCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
    
    GPUCode = [NSMutableString new];
    
    GPUCode = [[subModels objectAtIndex: i] definitionsGPUCode: newPrefix];
    outFile = [NSString stringWithFormat: @"%@_defines.cu", newPrefix];
    [GPUCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
    
    GPUCode = [[subModels objectAtIndex: i] kernelGPUCode: newPrefix];
    outFile = [NSString stringWithFormat: @"%@_kernel.cu", newPrefix];
    [GPUCode writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];

    [code appendFormat: @"#include \"%@.cu\"\n", newPrefix];

    [invokeCode appendFormat: @"  void *%@_data = data[%d]->gpuPtrs;\n", newPrefix, i];
    [invokeCode appendString: [GPUDictionary objectForKey: @"structure"]];
    [invokeCode appendString: [GPUDictionary objectForKey: @"execution variables"]];

    [execCode appendString: [GPUDictionary objectForKey: @"execution code"]];

    [functions appendFormat: @"  case %d:\n", i];
    [functions appendFormat: @"    return (void *)&%@_gpuFunctions;\n", newPrefix];

  }

  [invokeCode appendString: @"\n   // Invoke kernel\n"];
  [invokeCode appendString: @"   double currentTime = startTime;\n"];
  [invokeCode appendString: @"   while (currentTime < nextTime) {\n"];
  [invokeCode appendString: execCode];
  [invokeCode appendFormat: @"      currentTime += %@_0_ptrs->dt;\n", prefixName];
  [invokeCode appendString: @"   }\n"];
  [invokeCode appendString: @"}\n"];

  [functions appendString: @"  default:\n"];
  [functions appendString: @"    return NULL;\n"];
  [functions appendString: @"  }\n"];
  [functions appendString: @"}\n"];

  [code appendString: invokeCode];
  [code appendString: @"\n"];
  [code appendString: functions];
  [code appendString: @"}\n"];

  outFile = [NSString stringWithFormat: @"%@.cu", prefixName];
  [code writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
  
  return YES;
}

@end

@implementation SimulationController (GPURun)

- (void)setGPUFunctions: (getGPUFunctions *)func
{
  gpuFuncs = (void *)func;
}

- (getGPUFunctions *)gpuFunctions { return gpuFuncs; }

- (void)runGPUSimulation: (getGPUFunctions *)func
{
  int i, modelIndex;
  NSNotificationCenter *noticeCenter = [NSNotificationCenter defaultCenter];
  
  // global initialization
  (func)(-2, 0);

  // allocate CPU data
  [self allocateSimulationWithEncode: [Grid2DArray floatEncode]];

  // initialize CPU data
  [self initializeSimulation];

  int numModels = [modelInstances count];
  NSArray *subModels = [modelInstances objectAtIndex: 0];
  int numSubModels = [subModels count];
  ModelGPUData **gpuData = (ModelGPUData **)malloc(sizeof(ModelGPUData *) * numSubModels);

  // set the GPU device
  if (gpuDevice) (func)(-4, gpuDevice);

  // allocate GPU data
  for (i = 0; i < numSubModels; ++i) {
    id m = [subModels objectAtIndex: i];
    void *gpuFunctions = (func)(i, 0);
    gpuData[i] = [m allocGPUData: numModels withGPUFunctions: gpuFunctions];
    [m allocGPUKernel: gpuData[i]];
  }
  
  // inform delegate that simulation finished initialization
  if (respondsToDidInit) [delegate didInitializeSimulation: self];

  double perturbStart = 0.0;
  if (doPerturbation) {
    NSDictionary *perturb = [perturbations objectForKey: perturbationName];
    NSString *s = [perturb objectForKey: @"perturbStart"];
    if (s) perturbStart = [s doubleValue];
  }
  
  // run it                                                                                                                                                                                        
  BOOL done = NO;
  double nextTime = 0;
  double nextOutput = 0;
  currentTime = 0;
  
  printf("time = %f\n", currentTime);
  
  if (doPerturbation && (perturbStart == currentTime)) [self applyPerturbation: perturbationName];
  
  // transfer CPU data to GPU
  for (i = 0; i < numSubModels; ++i) {
    id m;
    for (modelIndex = 0; modelIndex < numModels; ++modelIndex) {
      subModels = [modelInstances objectAtIndex: modelIndex];
      m = [subModels objectAtIndex: i];
      [m assignData: gpuData[i] ofNumber: modelIndex toGPU: YES];
      [m assignParameters: gpuData[i] ofNumber: modelIndex toGPU: YES];
    }

    subModels = [modelInstances objectAtIndex: 0];
    m = [subModels objectAtIndex: i];
    [m transferGPUKernel: gpuData[i] toGPU: YES];
  }

  if (respondsToDidTransfer) [delegate didTransferDataToGPU: YES sender: self];
  [noticeCenter postNotificationName: BioSwarmDidTransferDataNotification object: self userInfo: 
   [NSDictionary dictionaryWithObjectsAndKeys: @"YES", @"toGPU", nil]];

  // write initial data
  [self writeData];
  
  // get pointer to main invoke function
  invokeGPUKernelFunction *invokeGPUKernel = (func)(-1, 0);

  // inform delegate that simulation will start executing
  if (respondsToWillRun) [delegate willRunSimulation: self];

  while (!done) {
    // determine next time for simulation
    nextOutput += outputFrequency;
    nextTime = nextOutput;
    if (doPerturbation) {
      if (TIME_EQUAL(perturbStart, currentTime) && (perturbStart > 0)) {
        [self applyPerturbation: perturbationName];

        // transfer CPU data to GPU
        for (i = 0; i < numSubModels; ++i) {
          id m;
          for (modelIndex = 0; modelIndex < numModels; ++modelIndex) {
            subModels = [modelInstances objectAtIndex: modelIndex];
            m = [subModels objectAtIndex: i];
            [m assignData: gpuData[i] ofNumber: modelIndex toGPU: YES];
            [m assignParameters: gpuData[i] ofNumber: modelIndex toGPU: YES];
          }
          
          subModels = [modelInstances objectAtIndex: 0];
          m = [subModels objectAtIndex: i];
          [m transferGPUKernel: gpuData[i] toGPU: YES];
        }
        if (respondsToDidTransfer) [delegate didTransferDataToGPU: YES sender: self];
        [noticeCenter postNotificationName: BioSwarmDidTransferDataNotification object: self userInfo: 
         [NSDictionary dictionaryWithObjectsAndKeys: @"YES", @"toGPU", nil]];
      }

      if ((perturbStart > currentTime) && (perturbStart < nextTime)) nextTime = perturbStart;
    }
    if (nextTime > simulateTime) nextTime = simulateTime;

    // invoke GPU
    if (numSubModels == 1) {
      subModels = [modelInstances objectAtIndex: 0];
      id m = [subModels objectAtIndex: 0];
      [m invokeGPUKernel: gpuData[0] currentTime: currentTime endTime: nextTime];
    } else (invokeGPUKernel)(self, gpuData, currentTime, nextTime);

    // are we done?
    currentTime = nextTime;
    //if (currentTime >= simulateTime) done = YES;
    if (TIME_EQUAL(currentTime, simulateTime) || (currentTime >= simulateTime)) done = YES;
    
    // transfer GPU data to CPU
    for (i = 0; i < numSubModels; ++i) {
      subModels = [modelInstances objectAtIndex: 0];
      id m = [subModels objectAtIndex: i];
      [m transferGPUKernel: gpuData[i] toGPU: NO];

      for (modelIndex = 0; modelIndex < numModels; ++modelIndex) {
        subModels = [modelInstances objectAtIndex: modelIndex];
        m = [subModels objectAtIndex: i];
        [m assignData: gpuData[i] ofNumber: modelIndex toGPU: NO];
      }
    }
    
    printf("time = %f\n", currentTime);
    if (respondsToDidTransfer) [delegate didTransferDataToGPU: NO sender: self];
    [noticeCenter postNotificationName: BioSwarmDidTransferDataNotification object: self userInfo: 
     [NSDictionary dictionaryWithObjectsAndKeys: @"NO", @"toGPU", nil]];

    // write data
    [self writeData];
  }

  // inform delegate that simulation has finished executing
  if (respondsToDidFinish) [delegate didFinishSimulation: self];

  // cleanup GPU data
  for (i = 0; i < numSubModels; ++i) {
    subModels = [modelInstances objectAtIndex: 0];
    id m = [subModels objectAtIndex: i];
    [m releaseGPUKernel: gpuData[i]];
    [m freeGPUData: gpuData[i]];
  }
  free(gpuData);
  
  // GPU device reset
  (func)(-3, 0);
}

@end

//
// Visualization
//

@implementation SimulationController (Povray)

- (NSMutableString *)povrayCode
{
  return [self povrayCodeForModel: 0];
}

- (NSMutableString *)povrayCodeForModel:(int)modelIndex
{
  int i;
  
  NSMutableString *povray = [NSMutableString new];
  
  if (spatialModel) [povray appendString: [spatialModel povrayCode]];
  
  NSArray *subModels = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [subModels count]; ++i) {
    id m = [subModels objectAtIndex: i];
    [povray appendString: [m povrayCode]];
  }
  
  return povray;
}

@end
