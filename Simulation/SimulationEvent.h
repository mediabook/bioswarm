/*
 SimulationEvent.h
  
 Copyright (C) 2012 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: July 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>

@interface SimulationEvent : NSObject
{
@public
  NSInvocation *theInvocation;
  double startTime;
  double repeatInterval;

  double nextTime;
  BOOL requireTransfer;
}

+ eventWithInvocation: (NSInvocation *)anInv atTime: (double)aTime;

+ eventWithInvocation: (NSInvocation *)anInv atTime: (double)aTime repeat: (double)aRepeat;

- (void)setRequireTransfer: (BOOL)aFlag;

@end
