/*
 PovrayManager.m
 
 Copyright (C) 2012 Scott Christley
 Author: Scott Christley <schristley@mac.com>
 Date: June 2012
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "PovrayManager.h"

@implementation PovrayManager

- init
{
  [super init];
  
  doInfo = NO;
  doSingle = NO;
  singleScene = -1;
  doRange = NO;
  startScene = -1;
  endScene = -1;
  prefixName = nil;
  
  return self;
}

- (void)printUsage: (NSArray *)args
{
  printf("Usage:\n");
  printf("%s [options] model\n\n", [[[args objectAtIndex: 0] lastPathComponent] UTF8String]);
  
  printf("Options:\n");
  printf("--help         Display usage information.\n");
  printf("--info         Display info about model run, do not generate povray files.\n");
  printf("--prefix N     Attach prefix to filename for povray files.\n");
  printf("--start N      Start sequence number for povray files at N, default is timepoint.\n");
  printf("--single N     Generate single povray file for timepoint N.\n");
  printf("--range S E    Generate povray files for range of timepoints from (S)tart to (E)nd.\n");
  printf("model          Model run file.\n");
}

- (BOOL)processArguments:(NSArray *)args
{
  BOOL good = YES;
  NS_DURING {
    
    // process parameters
    NSArray *args = [[NSProcessInfo processInfo] arguments];
    int i, argsCount = [args count];
    for (i = 1; i < argsCount; ++i) {
      if ([[args objectAtIndex: i] isEqualToString: @"--help"]) {
        [self printUsage: args];
        good = YES;
        goto done;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--prefix"]) {
        ++i;
        prefixName = [args objectAtIndex: i];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--info"]) {
        doInfo = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--single"]) {
        ++i;
        doSingle = YES;
        singleScene = [[args objectAtIndex: i] intValue];
        continue;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--range"]) {
        ++i;
        doRange = YES;
        startScene = [[args objectAtIndex: i] intValue];
        ++i;
        endScene = [[args objectAtIndex: i] intValue];        
        continue;
      }

      modelFilename = [args objectAtIndex: i];
    }
    
  } NS_HANDLER {
    
    printf("ERROR: Invalid usage.\n\n");
    [self printUsage: args];
    good = NO;
    
  } NS_ENDHANDLER
  
  if (!good) goto done;
  
  // load model file
  theModel = [[NSMutableDictionary dictionaryWithContentsOfFile: modelFilename] retain];
  if (!theModel) {
    printf("ERROR: Could not open parameter file: %s\n", [modelFilename UTF8String]);
    good = NO;
    goto done;
  }
  
  NSString *modelDirectory = [modelFilename stringByDeletingLastPathComponent];
  if (modelDirectory && ([modelDirectory length] != 0))
    printf("model directory = %s/\n", [modelDirectory UTF8String]);
  
  // setup model
  modelController = [[BioSwarmController alloc] initWithModel: theModel andManager: self];  
  if (!modelController) {
    printf("ERROR: Could not load model.");
    good = NO;
    goto done;
  }
  [modelController loadModelHierarchy];
  [modelController setModelDirectory: modelDirectory];
  [modelController setupSimulationReplay];
  
  if (doInfo) {
    good = [self modelInfo];
  } else if (doSingle) {
    good = [self singleScene: singleScene];
  } else if (doRange) {
    good = [self sceneRange: startScene end: endScene];
  } else
    good = [self allScenes];
  
done:
  return good;
}

- (BOOL)singleScene: (int)scene
{
  int i;
  int numIterations = [modelController numOfIterations];
  if (scene > numIterations) {
    printf("ERROR: Scene number out of range %d > %d\n", scene, numIterations);
    return NO;
  }
  
  for (i = 0; i < scene; ++i) {
    [modelController readHierarchyData];
  }
  
  NSMutableString *povray = [modelController povrayCode];
  
  NSString *outFile;
  if (prefixName) outFile = [NSString stringWithFormat: @"%@%@_%d.pov", prefixName, [modelController modelName], scene];
  else outFile = [NSString stringWithFormat: @"%@_%d.pov", [modelController modelName], scene];
  [povray writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];
  
  return YES;
}

- (BOOL)sceneRange: (int)start end: (int)end
{
  int i;
  int numIterations = [modelController numOfIterations];
  if (start > numIterations) {
    printf("ERROR: Scene number out of range %d > %d\n", start, numIterations);
    return NO;
  }
  if (end > numIterations) {
    printf("ERROR: Scene number out of range %d > %d\n", end, numIterations);
    return NO;
  }

  for (i = 0; i < start; ++i) {
    [modelController readHierarchyData];
  }

  while (i < end) {
    if (i%100 == 0) printf("Scenes processed: %d\n", i);
    NSMutableString *povray = [modelController povrayCode];
    [modelController readHierarchyData];
    NSString *outFile;
    if (prefixName) outFile = [NSString stringWithFormat: @"%@%@_%d.pov", prefixName, [modelController modelName], i];
    else outFile = [NSString stringWithFormat: @"%@_%d.pov", [modelController modelName], i];
    [povray writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];    
    ++i;
  }
  
  return YES;
}

- (BOOL)allScenes
{
  int i;
  int numIterations = [modelController numOfIterations];
  
  for (i = 0; i < numIterations; ++i) {
    if (i%100 == 0) printf("Scenes processed: %d\n", i);
    NSMutableString *povray = [modelController povrayCode];
    [modelController readHierarchyData];
    NSString *outFile;
    if (prefixName) outFile = [NSString stringWithFormat: @"%@%@_%d.pov", prefixName, [modelController modelName], i];
    else outFile = [NSString stringWithFormat: @"%@_%d.pov", [modelController modelName], i];
    [povray writeToFile: outFile atomically: NO encoding: NSUTF8StringEncoding error: NULL];    
  }
  
  return YES;
}

- (BOOL)modelInfo
{
  printf("Model Info\n");
  printf("----------\n");
  printf("Model name: %s\n", [[modelController modelName] UTF8String]);
  printf("Output run: %d\n", [modelController outputRun]);
  int numOfIterations = [modelController numOfIterations];
  printf("Total time iterations: %d\n", numOfIterations);
  printf("\n");
  
  //int numOfCells = [sem numOfCells];
  //printf("numOfCells = %d\n", numOfCells);
  
  return YES;
}

@end
