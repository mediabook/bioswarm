/*
 BCOptimization.h
 
 Copyright (C) 2008-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: July 2008
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#ifndef _BC_OPTIMIZATION_H_
#define _BC_OPTIMIZATION_H_

#import <Foundation/Foundation.h>
#import <BioCocoa/BCDataMatrix.h>

@interface BCOptimization : NSObject {
	void *initialDataMatrix;
	void *initialResponseMatrix;
	void *dataMatrix;
	void *responseMatrix;
	int numOfVariables;
	int numOfObservations;

	NSMutableArray *dataHandlers;
	int *dataHandlerObservations;
	BOOL selfConnections;
}

- initWithDataMatrix: (void *)matrix andResponse: (void *)response numberOfVariables: (int)numVars numberOfObservations: (int)numObs;

- (int)addDataHandler: (id)anObject;
- (void)loadData: (int)anInt;

- (void)setAllowSelfConnections: (BOOL)aFlag;
- (BOOL)allowSelfConnections;

- (void)outputGraph: (void *)aMatrix;
- (void)outputDotGraph: (void *)aMatrix withGeneNames: (NSArray *)geneNames;
- (void)outputCytoscapeGraph: (void *)aMatrix withGeneNames: (NSArray *)geneNames;
- (void)outputCytoscapeEdgeAttributes: (void *)aMatrix withGeneNames: (NSArray *)geneNames;

- (double)maxGamma;
- (double)maxGammaForVariable: (int)varIndex;

- (void)normalizeData;
- (double)calculateError: (void *)coefficients;
- (double)calculateError: (void *)coefficients row: (int)aRow;
- (double)calculateError: (void *)coefficients row: (int)aRow column: (int)aColumn;
- (double)calculateError: (void *)coefficients startRow: (int)startRow endRow: (int)endRow;

- (BCDataMatrix *)optimizeWithConstraints: (double)gamma;
- (BCDataMatrix *)optimizeWithConstraints: (double)gamma andInitialMatrix: (void *)aMatrix;
- (BCDataMatrix *)optimizeWithConstraintMatrix: (BCDataMatrix *)constraints;
- (BCDataMatrix *)optimizeWithConstraintMatrix: (BCDataMatrix *)constraints andInitialMatrix: (void *)aMatrix;

- (BCDataMatrix *)optimizeVariable: (int)varNum withConstraints: (double)gamma;
- (BCDataMatrix *)optimizeVariable: (int)varNum withConstraints: (double)gamma andInitialMatrix: (void *)aMatrix;
- (BCDataMatrix *)optimizeVariable: (int)varNum withConstraintMatrix: (BCDataMatrix *)constraints;
- (BCDataMatrix *)optimizeVariable: (int)varNum withConstraintMatrix: (BCDataMatrix *)constraints andInitialMatrix: (void *)aMatrix;

- (id)loadBundle;

@end

typedef enum BCOptimizeCoefficient {
	BCConnectionCoefficient = 0,
	BCDiagonalCoefficient = 1,
	BCInterceptCoefficient = 2
} BCOptimizeCoefficient;

@protocol BCOptimizeDataHandler
- (void)loadDataSource: (int)aSource dataMatrix: (void **)data responseMatrix: (void **)response numberOfObservations: (int *)aCount;
- (BOOL)crossValidateDataSource: (int)aSource dataMatrix: (void **)data responseMatrix: (void **)response row: (int)aRow column: (int)aColumn;
- (double)valueFromDataSource: (int)aSource forType: (BCOptimizeCoefficient)aType matrix: (void *)aMatrix row: (int)aRow column: (int)aColumn;
@end

//
// Network print functions
//
void print_matrix(char *N, int I, int J, void *M);
void print_rmatrix(char *N, int I, int J, void *M);
void print_mmatrix(char *N, int I, int J, void *M);
void print_SIF(void *aMatrix, NSArray *geneNames, int numOfVariables);
void print_EDA(void *aMatrix, NSArray *geneNames, int numOfVariables);

NSString* strprint_SIF(void *aMatrix, NSArray *geneNames, int numOfVariables);
NSString* strprint_EDA(void *aMatrix, NSArray *geneNames, int numOfVariables);

#endif // _BC_OPTIMIZATION_H_
