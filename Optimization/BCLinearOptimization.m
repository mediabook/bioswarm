/*
 BCLinearOptimization.m
 
 Copyright (C) 2011 Scott Christley
 
 Written by: Scott Christley <schristley@mac.com>
 Created: February 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
*/

#import "BCLinearOptimization.h"


@implementation BCLinearOptimization

- init
{
  [super init];

  // check if GLPK bundle available
  optObject = nil;
  NSBundle *bundle = [NSBundle bundleForClass: [self class]];
  NSString *bundlePath = [[bundle resourcePath] stringByAppendingPathComponent:@"GLPK.bundle"];
  printf("%s\n", [bundlePath UTF8String]);
  bundle = [NSBundle bundleWithPath: bundlePath];


#if 0
  // check if CPLEX bundle available
  optObject = nil;
  NSBundle *bundle = [NSBundle bundleForClass: [self class]];
  NSString *bundlePath = [[bundle resourcePath] stringByAppendingPathComponent:@"CPLEX.bundle"];
  printf("%s\n", [bundlePath UTF8String]);
  bundle = [NSBundle bundleWithPath: bundlePath];
#endif
  
  if (bundle) {
    BOOL res = [bundle load];
    if (res) printf("bundle loaded\n");
    else printf("bundle did not load\n");
    
    Class principalClass = [bundle principalClass];
    //printf("%s\n", class_getName(principalClass));
    
    if (principalClass) {
      optObject = [[principalClass alloc] init];
    }
  }

  return self;
}

- (BOOL)addColumn: (NSString *)aName lowerBound:(double)lowerBound upperBound:(double)upperBound objective:(double)aVal
{
  return [optObject addColumn: aName lowerBound: lowerBound upperBound: upperBound objective: aVal];
}

- (BOOL)addRow: (NSString *)name rhs:(double)aRHS sense:(char)aSense index: (NSArray *)varIndex values: (NSArray *)varValue
{
  return [optObject addRow:name rhs:aRHS sense:aSense index:varIndex values:varValue];
}

- (NSArray *)doOptimize
{
  return [optObject doOptimize];
}

- (BOOL)writeToFile: (NSString *)fileName
{
  return [optObject writeToFile:fileName];
}

@end
