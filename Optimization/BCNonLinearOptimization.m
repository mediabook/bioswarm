/*
 BCNonLinearOptimization.m
 
 Copyright (C) 2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: February 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BCNonLinearOptimization.h"

@implementation BCNonLinearOptimization

- initWithDataModel: (BCOptimizationDataHandler *)aModel
{
  [super init];
  
  dataModel = [aModel retain];
  
  // load bundle with IPOPT
  optObject = nil;
  NSBundle *bundle = [NSBundle bundleForClass: [self class]];
  NSString *bundlePath = [[bundle resourcePath] stringByAppendingPathComponent:@"IPOPT.bundle"];
  printf("%s\n", [bundlePath UTF8String]);
  bundle = [NSBundle bundleWithPath: bundlePath];
    
  if (bundle) {
    BOOL res = [bundle load];
    if (res) printf("bundle loaded\n");
    else printf("bundle did not load\n");
      
    Class principalClass = [bundle principalClass];
    //printf("%s\n", class_getName(principalClass));
      
    if (principalClass) {
      optObject = [[principalClass alloc] init];
    }
  }
  
  return self;
}

- (void)dealloc
{
  if (dataModel) [dataModel release];
  [super dealloc];
}

- (void)doOptimize
{
	BCDataMatrix *rm = [dataModel dataMatrix];
	BCDataMatrix *rr = [dataModel responseMatrix];
  
  int numOfObservations = [rm numberOfRows];
	int numOfVariables = [rm numberOfColumns];
	printf("%d observations\n", numOfObservations);
	printf("%d variables\n", numOfVariables);
  
  print_matrix("X = ", numOfObservations, numOfVariables, [rm dataMatrix]);
  print_matrix("B = ", numOfObservations, numOfVariables, [rr dataMatrix]);
  
  [optObject performOptimizeWithDataModel: dataModel];
}

@end
