/*
 BCOptimizationController.m
 
 Copyright (C) 2008-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: August 2008
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BCOptimizationController.h"
//#import "Dream2.h"
//#import "Dream2Data.h"
#import "BCOptimizationDataHandler.h"

#include <math.h>

#if 0
// Currently no NSInvocationOperation in GNUstep
@interface NSInvocationOperation : NSOperation
{
    NSInvocation *anInvocation;
}
- (id)initWithInvocation:(NSInvocation *)inv;
- (id)initWithTarget:(id)target selector:(SEL)sel object:(id)arg;
- (NSInvocation *)invocation;
- (id)result;
@end

@implementation NSInvocationOperation
- (id)initWithInvocation:(NSInvocation *)inv
{
    if ((self = [super init]) != nil) {
        anInvocation = inv;
    }
    
    return self;
}

- (void)dealloc
{
    if (anInvocation) [anInvocation release];
    [super dealloc];
}

- (id)initWithTarget:(id)target selector:(SEL)sel object:(id)arg;
{
    if ((self = [super init]) != nil) {
        anInvocation = [[NSInvocation invocationWithMethodSignature: [target methodSignatureForSelector: sel]] retain];
        [anInvocation setTarget: target];
        [anInvocation setSelector: sel];
        if (arg) [anInvocation setArgument: arg atIndex: 2];
    }
    
    return self;
}

- (NSInvocation *)invocation { return anInvocation; }
- (id)result { return nil; }

- (void)main
{
    [anInvocation invoke];
}
@end
#endif

@implementation BCOptimizationController

#if 0
#pragma mark == INITIALIZATION METHODS ==
#endif

- initWithConstraintFile: (NSString *)filePath
{
    NSArray *anArray = nil;
    
    if (filePath) {
        NSString *s = [NSString stringWithContentsOfFile: filePath encoding: NSUTF8StringEncoding error: NULL];
        anArray = [s componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    }
    
    return [self initWithConstraintArray: anArray defaultConstraintMatrix: nil baselineMatrix: nil];
}

- initWithConstraintArray: (NSArray *)anArray
{
    return [self initWithConstraintArray: anArray defaultConstraintMatrix: nil baselineMatrix: nil];
}

- initWithConstraintArray: (NSArray *)anArray defaultConstraintMatrix: (BCDataMatrix *)aMatrix
{
    return [self initWithConstraintArray: anArray defaultConstraintMatrix: aMatrix baselineMatrix: nil];
}

- initWithConstraintArray: (NSArray *)anArray defaultConstraintMatrix: (BCDataMatrix *)aMatrix baselineMatrix: (BCDataMatrix *)bMatrix
{
    [super init];
    
    if (anArray) {
        constraintArray = anArray;
        [constraintArray retain];
        calcConstraints = NO;
    } else {
        constraintArray = [NSMutableArray new];
        calcConstraints = YES;
    }
    constraintMatrix = aMatrix;
    [constraintMatrix retain];
    baselineMatrix = bMatrix;
    [baselineMatrix retain];
    selfConnections = NO;
    crossValidateMethod = BCCrossValidateLeaveOneOut;
    
    constraintLock = [NSLock new];
    constraintType = 1;
    optimalAlpha = 0.0;
    optimalBeta = 0.0;
    optimalConstraint = 0.0;
    constraintExpSteps = 50;
    
    matrixLock = [NSLock new];
    finalMatrix = NULL;
    
    operationQueue = [NSOperationQueue new];
#ifdef GNUSTEP
#else
    [operationQueue setMaxConcurrentOperationCount: MPProcessorsScheduled()];
#endif
    
    return self;
}

- initWithDataModel: (BCOptimizationDataHandler *)aModel
{
    return [self initWithDataModel: aModel baselineMatrix: nil];
}

- initWithDataModel: (BCOptimizationDataHandler *)aModel baselineMatrix: (BCDataMatrix *)bMatrix
{
    [super init];
    
    dataModel = [aModel retain];
    //printf("NSExtraRefCount dataModel: %ld\n", NSExtraRefCount(dataModel));
    baselineMatrix = [bMatrix retain];
    //if (baselineMatrix) printf("NSExtraRefCount baselineMatrix: %ld\n", NSExtraRefCount(baselineMatrix));
    if (baselineMatrix)
        useBaselineMatrix = YES;
    else
        useBaselineMatrix = NO;
    
    constraintArray = [NSMutableArray new];
    calcConstraints = YES;
    selfConnections = NO;
    usePriorMatrix = NO;
    keepZeroAlpha = NO;
    crossValidateMethod = BCCrossValidateLeaveOneOut;

    constraintLock = [NSLock new];
    constraintType = 1;
    optimalAlpha = 0.0;
    optimalBeta = 0.0;
    optimalConstraint = 0.0;
    constraintExpSteps = 50;
    
    matrixLock = [NSLock new];
    finalMatrix = NULL;
    
    operationQueue = [NSOperationQueue new];
#ifdef GNUSTEP
#else
    [operationQueue setMaxConcurrentOperationCount: MPProcessorsScheduled()];
#endif
    
    return self;
}

- (void) dealloc
{
    [operationQueue release];
    [constraintArray release];
    [constraintLock release];
    [matrixLock release];
    [constraintMatrix release];
    [baselineMatrix release];
    [dataModel release];
    
    [super dealloc];
}

#if 0
#pragma mark == ACCESSOR AND OUTPUT METHODS ==
#endif

- (void)setAllowSelfConnections: (BOOL)aFlag { selfConnections = aFlag; }
- (BOOL)allowSelfConnections { return selfConnections; }
- (void)setUsePriorMatrix: (BOOL)aFlag { usePriorMatrix = aFlag; }
- (BOOL)usePriorMatrix { return usePriorMatrix; }
- (void)setUseBaselineMatrix: (BOOL)aFlag { useBaselineMatrix = aFlag; }
- (BOOL)useBaselineMatrix { return useBaselineMatrix; }
- (void)setKeepZeroAlpha: (BOOL)aFlag { keepZeroAlpha = aFlag; }
- (BOOL)keepZeroAlpha { return keepZeroAlpha; }
- (void)setCrossValidateMethod: (BCCrossValidateMethod)aMethod { crossValidateMethod = aMethod; }
- (BCCrossValidateMethod)crossValidateMethod { return crossValidateMethod; }

- (int)numOfGenes { return numOfGenes; }

- (void)rankEdges
{
    int i, j;
    double (*A)[numOfGenes][numOfGenes];
    double maxRank = 0.0;
    
    A = finalMatrix;
    for (i = 0; i < numOfGenes; ++i)
        for (j = 0; j < numOfGenes; ++j)
            if ((*A)[i][j] > maxRank) maxRank = (*A)[i][j];
    
    for (i = 0; i < numOfGenes; ++i)
        for (j = 0; j < numOfGenes; ++j)
            (*A)[i][j] /= maxRank;
}

- (void)outputGraph
{
    int i, j;
    double (*A)[numOfGenes][numOfGenes];
    
    A = finalMatrix;
    printf("digraph lasso {\n");
    for (i = 0; i < numOfGenes; ++i) {
        for (j = 0; j < numOfGenes; ++j) {
            if ((*A)[i][j] != 0) {
                printf("G%d   G%d   %le\n", (i + 1), (j + 1), (*A)[i][j]);
            }
        }
    }
    printf("}\n");
}

- (void)outputConstraints
{
    int i;
    for (i = 0; i < [constraintArray count]; ++i) {
        NSNumber *n = [constraintArray objectAtIndex: i];
        printf("%d %lf\n", i, [n doubleValue]);
    }
}

- (void)outputErrors
{
    int i;
    for (i = 0; i < [constraintArray count]; ++i) {
        NSNumber *n = [constraintArray objectAtIndex: i];
        printf("%d gamma: %lf ", i, [n doubleValue]);
        n = [errorArray objectAtIndex: i];
        printf("error: %lf\n", [n doubleValue]);
    }
}

#if 0
#pragma mark == CONSTRAINT ARRAY THREAD WORKER METHODS ==
#endif

//
// Thread worker methods when given constraint array
//
- (id)nextConstraint
{
    id next = nil;
    
    [constraintLock lock];
    
    if (currentConstraint == [constraintArray count]) goto done;
    
    NSString *s = [constraintArray objectAtIndex: currentConstraint];
    if ([s doubleValue] < 0.0) goto done;
    
    next = s;
    ++currentConstraint;
    
done:
    [constraintLock unlock];
    return next;
}

- (void)updateMatrix: (void *)aMatrix withGamma: (double)gamma
{
    int i, j;
    
    [matrixLock lock];
    
    double (*A)[numOfGenes][numOfGenes];
    A = aMatrix;
    double (*W)[numOfGenes][numOfGenes];
    W = finalMatrix;
    for (i = 0; i < numOfGenes; ++i)
        for (j = 0; j < numOfGenes; ++j)
            if (((*A)[i][j] != 0.0) && (gamma > (*W)[i][j]))
                (*W)[i][j] = gamma;
    
    [matrixLock unlock];
}

#if 0
#pragma mark == SPARSITY THREAD WORKER METHODS ==
#endif

//
// Thread worker methods for calculating the sparsity values
//
- (double)nextCalcConstraintWithMatrixIndex: (int *)anIndex
{
    int i, j, k;
    double next = 0.0;
    static int lastCount;
    static int convergeCount;
    
    [constraintLock lock];
    
    j = 0;
    for (i = 0; i < [constraintArray count]; ++i) {
        NSNumber *n = [constraintArray objectAtIndex: i];
        if ([n doubleValue] == -1.0) ++j;
    }
    
    // if making no more progress then stop
    if (j != lastCount) {
        convergeCount = 0;
        lastCount = j;
    } else {
        ++convergeCount;
    }
    if (convergeCount >= 100) {
        [constraintLock unlock];
        return -1.0;
    }
    
    printf("empty entries: %d\n", j);
    
    if (j == 0) {
        // optimize phase
    } else {
        // search phase
        int r = rand();
        double rd = (double)r / (double)(RAND_MAX - 1);
        j = rd * j;
        ++j;
        
        k = 0;
        for (i = 0; i < [constraintArray count]; ++i) {
            NSNumber *n = [constraintArray objectAtIndex: i];
            if ([n doubleValue] == -1.0) ++k;
            if (j == k) break;
        }
        if (j != k) NSLog(@"ERROR: did not pick random entry");
        
        printf("randomEntry: %d %d\n", j, i);
        
        double topEntry;
        for (k = i-1; k >= 0; --k) {
            NSNumber *n = [constraintArray objectAtIndex: k];
            if ([n doubleValue] != -1.0) {
                topEntry = [n doubleValue];
                break;
            }
        }
        printf("topEntry: %d %lf\n", k, topEntry);
        *anIndex = k;
        
        double botEntry;
        for (k = i+1; k < [constraintArray count]; ++k) {
            NSNumber *n = [constraintArray objectAtIndex: k];
            if ([n doubleValue] != -1.0) {
                botEntry = [n doubleValue];
                break;
            }
        }
        printf("botEntry: %d %lf\n", k, botEntry);
        
        if (topEntry < botEntry) {
            next = (botEntry - topEntry) * rd + topEntry;
            //abort();
        } else {
            next = (topEntry - botEntry) * rd + botEntry;
        }
    }
    
    [constraintLock unlock];
    
    return next;
}

- (void)updateConstraint: (double)gamma withMatrix: (void *)aMatrix
{
    int i, j;
    
    [constraintLock lock];
    
    double (*A)[numOfGenes][numOfGenes];
    A = aMatrix;
    double (*W)[numOfGenes][numOfGenes];
    W = finalMatrix;
    
    // count number of non-zero entries
    int cnt = 0;
    for (i = 0; i < numOfGenes; ++i)
        for (j = 0; j < numOfGenes; ++j)
            if ((*A)[i][j] != 0.0) ++cnt;
    
#if 0
    if ((gamma == 17.907597) || (cnt == 28)) {
        for (i = 0; i < numOfGenes; ++i) {
            for (j = 0; j < numOfGenes; ++j) {
                if ((*A)[i][j] != 0) {
                    printf("G%d -> G%d   %lf\n", (i + 1), (j + 1), (*A)[i][j]);
                }
            }
        }
    }
#endif
    
    printf("Update gamma: %lf count: %d\n", gamma, cnt);
    if (cnt == 0) cnt = 1;
    //if (matrixArray[cnt-1]) free(matrixArray[cnt-1]);
    //matrixArray[cnt-1] = aMatrix;
    
    // update constraint array
    NSNumber *n = [constraintArray objectAtIndex: cnt-1];
    //if (([n doubleValue] < 0.0) || (gamma > [n doubleValue])) {
    [constraintArray replaceObjectAtIndex: cnt-1 withObject: [NSNumber numberWithDouble: gamma]];
    for (i = 0; i < numOfGenes; ++i)
        for (j = 0; j < numOfGenes; ++j)
            if (((*A)[i][j] != 0.0) && (gamma > (*W)[i][j]))
                (*W)[i][j] = gamma;
    //}
    
    [constraintLock unlock];
}

- (void)threadedCalcConstraintWorker: (id)anObject
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    BCOptimization *bco = anObject;
    
    BOOL done = NO;
    double (*A)[numOfGenes][numOfGenes];
    A = NULL;
    double (*B)[numOfGenes][numOfGenes];
    B = NULL;
    while (!done) {
        int i;
        double gamma = [self nextCalcConstraintWithMatrixIndex: &i];
        if (gamma < 0.0) {
            done = YES;
            continue;
        }
        
        //B = matrixArray[i];
        printf("optimizing with gamma: %lf %p\n", gamma, B);
        BCDataMatrix *rMatrix = [bco optimizeWithConstraints: gamma andInitialMatrix: B];
        A = [rMatrix dataMatrix];
        
        [self updateConstraint: gamma withMatrix: A];
        if (B) free(B);
        B = A;
    }
    
    printf("thread done working\n");
    [pool release];
}

#if 0
#pragma mark == CROSS VALIDATE THREAD WORKER METHODS ==
#endif

//
//
//

- (void)updateError: (double)error forGamma: (double)gamma
{
    int i;
    
    [matrixLock lock];
    
    for (i = 0; i < [constraintArray count]; ++ i) {
        NSNumber *n = [constraintArray objectAtIndex: i];
        if ([n doubleValue] == gamma) {
            [errorArray replaceObjectAtIndex: i withObject: [NSNumber numberWithDouble: error]];
        }
    }
    
    [matrixLock unlock];
}


- (void)crossValidateWorker: (id)anObject
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    //BCOptimization *bco = anObject;
    BCOptimizationDataHandler *aDataSet = anObject;
    int i, j, k, l;
    
    int numOfObservations = [aDataSet numOfObservations];
    int numOfVariables = [aDataSet numOfVariables];
    
    // constraint matrix
    BCDataMatrix *cMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
    //[cMatrix autorelease];
    double (*CM)[numOfVariables][numOfVariables];
    CM = [cMatrix dataMatrix];
    
    BOOL done = NO;
    double totError = 0.0;
    double (*A)[numOfVariables][numOfVariables] = NULL;
    
    // an empty initial matrix corresponding to the max gamma
    double (*IM)[numOfVariables][numOfVariables];
    IM = malloc(sizeof(double) * numOfVariables * numOfVariables);
    double (*B1)[numOfVariables][numOfVariables];
    B1 = IM;
    for (i = 0; i < numOfVariables; ++i)
        for (j = 0; j < numOfVariables; ++j)
            (*B1)[i][j] = 0.0;
    
    // For doing error calculation
    BCOptimization *bco = [BCOptimization new];
    [bco autorelease];
    [bco setAllowSelfConnections: selfConnections];
    [bco addDataHandler: aDataSet];
    [bco loadData: numOfVariables];
    
    // for cross validation
    BCDataMatrix *cvXMatrix = [BCDataMatrix emptyDataMatrixWithRows: (numOfObservations - 1) andColumns: numOfVariables andEncode: BCdoubleEncode];
    //[cvXMatrix autorelease];
    double (*cvX)[numOfObservations - 1][numOfVariables];
    cvX = [cvXMatrix dataMatrix];
    BCDataMatrix *cvBMatrix = [BCDataMatrix emptyDataMatrixWithRows: (numOfObservations - 1) andColumns: numOfVariables andEncode: BCdoubleEncode];
    //[cvBMatrix autorelease];
    double (*cvB)[numOfObservations - 1][numOfVariables];
    cvB = [cvBMatrix dataMatrix];
    
    // until there are constraint values
    while (!done) {
        id c = [self nextConstraint];
        if (!c) {
            done = YES;
            continue;
        }
        
        // L1-norm constraint
        double alpha;
        if (constraintType == 1) alpha = [c doubleValue];
        else alpha = optimalAlpha;
        
        for (i = 0; i < numOfVariables; ++i)
            for (j = 0; j < numOfVariables; ++j)
                (*CM)[i][j] = alpha;
        
        // existing network constraint
        double beta;
        if (constraintType == 1) beta = optimalBeta;
        else beta = [c doubleValue];
        
        if (constraintMatrix) {
            double (*DCM)[numOfVariables][numOfVariables];
            DCM = [constraintMatrix dataMatrix];
            for (i = 0; i < numOfVariables; ++i)
                for (j = 0; j < numOfVariables; ++j)
                    if ((*DCM)[i][j] != 0.0) (*CM)[i][j] += beta;
        }
        
        //printf("cross validation with alpha: %lf beta: %lf\n", alpha, beta);
        
        // do cross-validation
        // for each observation
        totError = 0.0;
        for (i = 0; i < numOfObservations; ++i) {
            double (*X)[numOfObservations][numOfVariables];
            X = [[aDataSet dataMatrix] dataMatrix];
            double (*B)[numOfObservations][numOfVariables];
            B = [[aDataSet responseMatrix] dataMatrix];
            
            // construct data and response matrix
            // leave one row out
            int aRow = 0;
            for (j = 0; j < numOfObservations; ++j) {
                if (j == i) continue;
                for (k = 0; k < numOfVariables; ++k) {
                    (*cvX)[aRow][k] = (*X)[j][k];
                    (*cvB)[aRow][k] = (*B)[j][k];
                }
                ++aRow;
            }
            BCOptimizationDataHandler *cvModel = [[BCOptimizationDataHandler alloc] initWithDataMatrix: cvXMatrix andResponse: cvBMatrix];
            [cvModel autorelease];
            
            // perform optimization
            BCOptimization *cvBCO = [BCOptimization new];
            [cvBCO autorelease];
            [cvBCO setAllowSelfConnections: selfConnections];
            [cvBCO addDataHandler: cvModel];
            [cvBCO loadData: numOfVariables];
            
            if (A) free(A);
            A = [cvBCO optimizeWithConstraintMatrix: cMatrix andInitialMatrix: IM];
            
#if 0
            // now optimize where only the predicted connections can take a value
            // force the other connections to zero by using maxGamma as the constraint
            double maxGamma = [cvBCO maxGamma];
            for (k = 0; k < numOfVariables; ++k)
                for (l = 0; l < numOfVariables; ++l) {
                    if ((*A)[k][l] != 0.0) (*CM)[k][l] = 0.0;
                    else (*CM)[k][l] = maxGamma;
                }
            A = [cvBCO optimizeWithConstraintMatrix: cMatrix];
#endif
            
            // calculate error for left out row
            totError += [bco calculateError: A row: i];
        }
        
#if 0
        // use result as initial ???
        for (i = 0; i < numOfVariables; ++i)
            for (j = 0; j < numOfVariables; ++j)
                (*IM)[i][j] = (*A)[i][j];
#endif
        
        //printf("update error: %lf for alpha: %lf beta: %lf\n", totError, alpha, beta);
        
        if (constraintType == 1) [self updateError: totError forGamma: alpha];
        else [self updateError: totError forGamma: beta];
        
    }
    
#if 0
    CM = A;
    printf("A = \n");
    for (i = 0; i < numOfGenes; ++i) {
        for (j = 0; j < numOfGenes; ++j)
            printf("% 11.6lf, ", (*CM)[i][j]);
        printf("\n    ");
    }
    printf("\n");
#endif
    
    //printf("thread done working\n");
    free(A);
    free(IM);
    [pool release];
}

- (void)doCrossValidateVariable: (id)anObject
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    //BCOptimization *bco = anObject;
    BCOptimizationDataHandler *aDataSet = anObject;
    int i, j, k;
    
    int numOfObservations = [aDataSet numOfObservations];
    int numOfVariables = [aDataSet numOfVariables];
    
    // constraint matrix
    BCDataMatrix *cMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
    [cMatrix retain];
    //printf("NSExtraRefCount cMatrix: %ld\n", NSExtraRefCount(cMatrix));
    double (*CM)[numOfVariables][numOfVariables];
    CM = [cMatrix dataMatrix];
    
    BOOL done = NO;
    double totError = 0.0;
    BCDataMatrix *resMatrix = nil;
    double (*A)[numOfVariables][numOfVariables] = NULL;
    
    // an empty initial matrix corresponding to the max gamma
    double (*IM)[numOfVariables][numOfVariables];
    IM = malloc(sizeof(double) * numOfVariables * numOfVariables);
    double (*B1)[numOfVariables][numOfVariables];
    B1 = IM;
    for (i = 0; i < numOfVariables; ++i)
        for (j = 0; j < numOfVariables; ++j)
            (*B1)[i][j] = 0.0;
    
    // For doing error calculation
    BCOptimization *bco = [BCOptimization new];
    [bco autorelease];
    [bco setAllowSelfConnections: selfConnections];
    [bco addDataHandler: aDataSet];
    [bco loadData: numOfVariables];
    
    // for cross validation
    NSMutableArray *cvModels = [NSMutableArray array];
    switch (crossValidateMethod) {
        case BCCrossValidateTenFold: {
            int startRow = 0;
            int endRow = 0;
            int numRow;
            for (numRow = 1; numRow <= 10; ++numRow) {
                if (numRow == 10) endRow = numOfObservations - 1;
                else endRow = numRow * (numOfObservations / 10);
                //printf("startRow: %d endRow: %d\n", startRow, endRow);
                id m = [aDataSet newModelWithoutStart: startRow toEndObservation: endRow];
                [cvModels addObject: m];
                [m autorelease];
                startRow = endRow + 1;
            }
            break;
        }
        case BCCrossValidateLeaveOneOut:
            for (i = 0; i < numOfObservations; ++i) {
                id m = [aDataSet newModelWithoutObservation: i];
                [cvModels addObject: m];
                [m autorelease];
            }
            break;
        default:
            printf("ERROR: unknown cross validate method: %d\n", crossValidateMethod);
            return;
    }
    
    // until there are constraint values
    while (!done) {
	printf("doCrossValidateVariable pool: %d\n", [pool autoreleaseCount]);

        id c = [self nextConstraint];
        if (!c) {
            done = YES;
            continue;
        }
        
        // L1-norm constraint
        double alpha;
        if (constraintType == 1) alpha = [c doubleValue];
        else alpha = optimalAlpha;
        
        for (i = 0; i < numOfVariables; ++i)
            for (j = 0; j < numOfVariables; ++j)
                (*CM)[i][j] = alpha;
        
        // existing network constraint
        double beta;
        if (constraintType == 1) beta = optimalBeta;
        else beta = [c doubleValue];
        
        if (constraintMatrix) {
            double (*DCM)[numOfVariables][numOfVariables];
            DCM = [constraintMatrix dataMatrix];
            for (i = 0; i < numOfVariables; ++i)
                for (j = 0; j < numOfVariables; ++j)
                    if ((*DCM)[i][j] != 0.0) (*CM)[i][j] += beta;
        }
        
        // baseline matrix
        if ((useBaselineMatrix) && (baselineMatrix)) {
            double (*BM)[numOfVariables][numOfVariables] = [baselineMatrix dataMatrix];
            for (i = 0; i < numOfVariables; ++i)
                for (j = 0; j < numOfVariables; ++j)
                    (*CM)[i][j] += (*BM)[i][j];
        }
        
        //print_matrix("CM = \n", numOfVariables, numOfVariables, CM);
        //printf("cross validation for variable: %d with alpha: %lf beta: %lf\n", currentVariable, alpha, beta);
        
        // do cross-validation
        // for each observation
        totError = 0.0;
        NSMutableArray *jobList = [NSMutableArray array];
        for (i = 0; i < [cvModels count]; ++i) {
            id cvModel = [cvModels objectAtIndex: i];
            
            id optJob = [[BCOptimizationWorker alloc] initWithModel:cvModel constraintMatrix:cMatrix];
            [optJob autorelease];
            [optJob setAllowSelfConnections: selfConnections];
            [optJob setCurrentVariable: currentVariable];
            [jobList addObject: optJob];
	    //printf("NSExtraRefCount optJob: %ld\n", NSExtraRefCount(optJob));
            
            id io = [[[NSInvocationOperation alloc] initWithTarget: optJob selector: @selector(doVariableOptimize) object: nil] autorelease];
            [operationQueue addOperation: io];
        }
        [operationQueue waitUntilAllOperationsAreFinished];
        
        for (i = 0; i < [cvModels count]; ++i) {
            id optJob = [jobList objectAtIndex: i];
            resMatrix = [optJob getResult];
            A = [resMatrix dataMatrix];
            
            //if (i == 0) print_matrix("A = \n", numOfVariables, numOfVariables, A);
            
            switch (crossValidateMethod) {
                case BCCrossValidateTenFold: {
                    int startRow = 0;
                    int endRow = 0;
                    int numRow, errorRow;
                    for (numRow = 1; numRow <= 10; ++numRow) {
                        if (numRow == 10) endRow = numOfObservations - 1;
                        else endRow = numRow * (numOfObservations / 10);
                        //printf("startRow: %d endRow: %d\n", startRow, endRow);
                        if ((i+1) == numRow) {
                            for (errorRow = startRow; errorRow <= endRow; ++errorRow)
                                totError += [bco calculateError: A row: errorRow];
                            break;
                        }
                        startRow = endRow + 1;
                    }
                    break;
                }
                case BCCrossValidateLeaveOneOut:
                    // calculate error for left out row
                    totError += [bco calculateError: A row: i];
                    break;
            }
        }
        
        printf("cross validation for variable: %d with alpha: %lf beta: %lf error: %lf\n", currentVariable, alpha, beta, totError);
        //printf("update error: %lf for alpha: %lf beta: %lf\n", totError, alpha, beta);
        
        if (constraintType == 1) [self updateError: totError forGamma: alpha];
        else [self updateError: totError forGamma: beta];
        
	//    printf("NSExtraRefCount cvModels: %ld\n", NSExtraRefCount(cvModels));
	//    printf("NSExtraRefCount jobList: %ld\n", NSExtraRefCount(jobList));
    }


    //free(A);
    free(IM);
    [pool release];
}


#if 0
#pragma mark == CROSS VALIDATION CONTROLLER METHODS ==
#endif

//
// One alpha parameter for the whole matrix
//
- (double)crossValidateOptimizeWithData: (id)aData optimalAlpha: (double *)optA
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    int j;
    
    numOfGenes = [aData numOfVariables];
    
    optimalBeta = 0.0;
    
    BCOptimization *bco = [BCOptimization new];
    [bco autorelease];
    [bco setAllowSelfConnections: selfConnections];
    [bco addDataHandler: aData];
    //[bco addDataHandler: dd2];
    [bco loadData: numOfGenes];
    
    double maxGamma = [bco maxGamma];
    if (maxGamma < 0.0) {
        NSLog(@"ERROR: could not calculate max gamma.");
        [pool release];
        return -1.0;
    }
    
    double minError;
    
    // exponential search of alpha
    double upperGamma = maxGamma;
    double a = log(upperGamma) - log(.001);
    double b = a / constraintExpSteps;
    constraintArray = [NSMutableArray array];
    int checkCount = 0;
    double x = 0.0;
    while (checkCount < constraintExpSteps) {
        double aAlpha = upperGamma * exp(-x);
        [constraintArray addObject: [NSNumber numberWithDouble: aAlpha]];
        //printf("%lf %lf\n", aAlpha, a);
        x += b;
        ++checkCount;
    }
    [constraintArray addObject: [NSNumber numberWithDouble: 0.0]];
    
    // initialize error array
    errorArray = [NSMutableArray array];
    for (j = 0; j < [constraintArray count]; ++j)
        [errorArray addObject: [NSNumber numberWithDouble: 0.0]];
    
    // do the optimization
    constraintType = 1;
    currentConstraint = 0;
    //[self threadedCrossValidateWorker: bco];
    [self crossValidateWorker: aData];
    
    // find the minimal error
    NSNumber *n = [errorArray objectAtIndex: 0];
    minError = [n doubleValue];
    n = [constraintArray objectAtIndex: 0];
    optimalAlpha = [n doubleValue];
    for (j = 1; j < [errorArray count]; ++j) {
        n = [errorArray objectAtIndex: j];
        if ([n doubleValue] < minError) {
            minError = [n doubleValue];
            optimalAlpha = [[constraintArray objectAtIndex: j] doubleValue];
        }
    }
    printf("alpha: %lf error: %lf\n", optimalAlpha, minError);
    
    
    if (optA) *optA = optimalAlpha;
    constraintArray = nil;
    
    [pool release];
    
    return minError;
}

//
// One alpha and beta parameter for the whole matrix
//
- (double)crossValidateMatrixOptimizeWithData: (id)aData optimalAlpha: (double *)optA optimalBeta: (double *)optB
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    int i, j;
    
    numOfGenes = [aData numOfVariables];
    
    double finalOptimalAlpha = optimalAlpha;
    double finalOptimalBeta = optimalBeta;
    
    BCOptimization *bco = [BCOptimization new];
    [bco autorelease];
    [bco setAllowSelfConnections: selfConnections];
    [bco addDataHandler: aData];
    [bco loadData: numOfGenes];
    
    double maxGamma = [bco maxGamma];
    if (maxGamma < 0.0) {
        NSLog(@"ERROR: could not calculate max gamma.");
        [pool release];
        return -1.0;
    }
    
    // exponential search of alpha
    NSMutableArray *alphaArray = [NSMutableArray array];
    double upperGamma = maxGamma;
    double a = log(upperGamma) - log(.001);
    double b = a / constraintExpSteps;
    int checkCount = 0;
    double x = 0.0;
    while (checkCount < constraintExpSteps) {
        double aAlpha = upperGamma * exp(-x);
        [alphaArray addObject: [NSNumber numberWithDouble: aAlpha]];
        //printf("%lf %lf\n", aAlpha, a);
        x += b;
        ++checkCount;
    }
    [alphaArray addObject: [NSNumber numberWithDouble: 0.0]];
    
    double minError;
    for (i = 0; i < [alphaArray count]; ++i) {
        optimalAlpha = [[alphaArray objectAtIndex: i] doubleValue];
        
        // exponential search of beta
        upperGamma = maxGamma - optimalAlpha;
        a = log(upperGamma) - log(.001);
        b = a / constraintExpSteps;
        constraintArray = [NSMutableArray array];
        checkCount = 0;
        x = 0.0;
        while ((checkCount < constraintExpSteps) && (upperGamma != 0)) {
            double aBeta = upperGamma * exp(-x);
            [constraintArray addObject: [NSNumber numberWithDouble: aBeta]];
            //printf("%lf %lf\n", aBeta, a);
            x += b;
            ++checkCount;
        }
        [constraintArray addObject: [NSNumber numberWithDouble: 0.0]];
        
        // initialize error array
        errorArray = [NSMutableArray array];
        for (j = 0; j < [constraintArray count]; ++j)
            [errorArray addObject: [NSNumber numberWithDouble: 0.0]];
        
        // do the optimization
        constraintType = 2;
        currentConstraint = 0;
        //[self threadedCrossValidateWorker: bco];
        [self crossValidateWorker: aData];
        
        // find the minimal error
        NSNumber *n;
        if (i == 0) {
            n = [errorArray objectAtIndex: 0];
            minError = [n doubleValue];
            finalOptimalAlpha = optimalAlpha;
            n = [constraintArray objectAtIndex: 0];
            finalOptimalBeta = [n doubleValue];
        }
        
        for (j = 0; j < [errorArray count]; ++j) {
            n = [errorArray objectAtIndex: j];
            if ([n doubleValue] < minError) {
                minError = [n doubleValue];
                finalOptimalAlpha = optimalAlpha;
                finalOptimalBeta = [[constraintArray objectAtIndex: j] doubleValue];
            }
        }
        printf("alpha: %lf beta: %lf error: %lf\n", optimalAlpha, optimalBeta, minError);
    }
    
    if (optA) *optA = finalOptimalAlpha;
    if (optB) *optB = finalOptimalBeta;
    constraintArray = nil;
    
    [pool release];
    
    return minError;
}

//
// alpha parameter for each variable
//
- (double)crossValidateOptimizeVariable: (int)varIndex withData: (id)aData optimalAlpha: (double *)optA
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    int j;
    
    numOfGenes = [aData numOfVariables];
    
    optimalBeta = 0.0;
    
    BCOptimization *bco = [BCOptimization new];
    [bco autorelease];
    [bco setAllowSelfConnections: selfConnections];
    [bco addDataHandler: aData];
    //[bco addDataHandler: dd2];
    [bco loadData: numOfGenes];
    
    //double maxGamma = [bco maxGamma];
    double maxGamma = [bco maxGammaForVariable: varIndex];
    if (maxGamma < 0.0) {
        NSLog(@"ERROR: could not calculate max gamma.");
        [pool release];
        return -1.0;
    }
    
    double minError;
    
    // exponential search of alpha
    double upperGamma = maxGamma;
    double a = log(upperGamma) - log(.001);
    double b = a / constraintExpSteps;
    constraintArray = [NSMutableArray array];
    int checkCount = 0;
    double x = 0.0;
    while (checkCount < constraintExpSteps) {
        double aAlpha = upperGamma * exp(-x);
        [constraintArray addObject: [NSNumber numberWithDouble: aAlpha]];
        //printf("%lf %lf\n", aAlpha, a);
        x += b;
        ++checkCount;
    }
    [constraintArray addObject: [NSNumber numberWithDouble: 0.0]];
    
    // initialize error array
    errorArray = [NSMutableArray array];
    for (j = 0; j < [constraintArray count]; ++j)
        [errorArray addObject: [NSNumber numberWithDouble: 0.0]];
    
    // do the optimization
    constraintType = 1;
    currentConstraint = 0;
    currentVariable = varIndex;
    
    //[self crossValidateVariableWorker: aData];
    [self doCrossValidateVariable: aData];
    
    // find the minimal error
    NSNumber *n = [errorArray objectAtIndex: 0];
    minError = [n doubleValue];
    n = [constraintArray objectAtIndex: 0];
    optimalAlpha = [n doubleValue];
    for (j = 1; j < [errorArray count]; ++j) {
        n = [errorArray objectAtIndex: j];
        if ([n doubleValue] < minError) {
            minError = [n doubleValue];
            optimalAlpha = [[constraintArray objectAtIndex: j] doubleValue];
        }
    }
    printf("alpha: %lf error: %lf\n", optimalAlpha, minError);
    
    
    if (optA) *optA = optimalAlpha;
    constraintArray = nil;
    
    [pool release];
    
    return minError;
}

//
// alpha and beta parameter for each variable
//
- (double)crossValidateMatrixOptimizeVariable: (int)varIndex withData: (id)aData optimalAlpha: (double *)optA optimalBeta: (double *)optB
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    int i, j;
    
    numOfGenes = [aData numOfVariables];
    
    double finalOptimalAlpha = optimalAlpha;
    double finalOptimalBeta = optimalBeta;
    
    BCOptimization *bco = [BCOptimization new];
    [bco autorelease];
    [bco setAllowSelfConnections: selfConnections];
    [bco addDataHandler: aData];
    [bco loadData: numOfGenes];
    
    double maxGamma = [bco maxGammaForVariable: varIndex];
    if (maxGamma < 0.0) {
        NSLog(@"ERROR: could not calculate max gamma.");
        [pool release];
        return -1.0;
    }
    
    // exponential search of alpha
    NSMutableArray *alphaArray = [NSMutableArray array];
    double upperGamma = maxGamma;
    double a = log(upperGamma) - log(.001);
    double b = a / constraintExpSteps;
    int checkCount = 0;
    double x = 0.0;
    while (checkCount < constraintExpSteps) {
        double aAlpha = upperGamma * exp(-x);
        [alphaArray addObject: [NSNumber numberWithDouble: aAlpha]];
        //printf("%lf %lf\n", aAlpha, a);
        x += b;
        ++checkCount;
    }
    [alphaArray addObject: [NSNumber numberWithDouble: 0.0]];
    
    double minError;
    for (i = 0; i < [alphaArray count]; ++i) {
        optimalAlpha = [[alphaArray objectAtIndex: i] doubleValue];
        
        // exponential search of beta
        upperGamma = maxGamma - optimalAlpha;
        a = log(upperGamma) - log(.001);
        b = a / constraintExpSteps;
        constraintArray = [NSMutableArray array];
        checkCount = 0;
        x = 0.0;
        while ((checkCount < constraintExpSteps) && (upperGamma != 0)) {
            double aBeta = upperGamma * exp(-x);
            [constraintArray addObject: [NSNumber numberWithDouble: aBeta]];
            //printf("%lf %lf\n", aBeta, a);
            x += b;
            ++checkCount;
        }
        [constraintArray addObject: [NSNumber numberWithDouble: 0.0]];
        
        // initialize error array
        errorArray = [NSMutableArray array];
        for (j = 0; j < [constraintArray count]; ++j)
            [errorArray addObject: [NSNumber numberWithDouble: 0.0]];
        
        // do the optimization
        constraintType = 2;
        currentConstraint = 0;
        currentVariable = varIndex;
        
        //[self crossValidateVariableWorker: aData];
        [self doCrossValidateVariable: aData];
        
        // find the minimal error
        NSNumber *n;
        if (i == 0) {
            n = [errorArray objectAtIndex: 0];
            minError = [n doubleValue];
            finalOptimalAlpha = optimalAlpha;
            n = [constraintArray objectAtIndex: 0];
            finalOptimalBeta = [n doubleValue];
        }
        
        double localMinError = [[errorArray objectAtIndex: 0] doubleValue];
        optimalBeta = [[constraintArray objectAtIndex: 0] doubleValue];
        for (j = 1; j < [errorArray count]; ++j) {
            n = [errorArray objectAtIndex: j];
            if ([n doubleValue] < minError) {
                minError = [n doubleValue];
                finalOptimalAlpha = optimalAlpha;
                finalOptimalBeta = [[constraintArray objectAtIndex: j] doubleValue];
            }
            if ([n doubleValue] < localMinError) {
                localMinError = [n doubleValue];
                optimalBeta = [[constraintArray objectAtIndex: j] doubleValue];
            }
        }
        printf("current alpha: %lf beta: %lf error: %lf\n", optimalAlpha, optimalBeta, localMinError);
	printf("crossValidateMatrixOptimizeVariable pool: %d\n", [pool autoreleaseCount]);
    }
    
    if (optA) *optA = finalOptimalAlpha;
    if (optB) *optB = finalOptimalBeta;
    constraintArray = nil;
    
    [pool release];
    
    return minError;
}

#if 0
#pragma mark == OPTIMIZATION CONTROLLER METHODS ==
#endif

- (BCDataMatrix *)doMatrixOptimize
{
    return nil;
}

- (BCDataMatrix *)doVariableOptimize
{
    int i, j, varIdx;
    int numOfVariables = [dataModel numOfVariables];
    NSArray *geneNames = [dataModel geneNames];
    
    // perform cross-validation optimization
    double (*A)[numOfVariables][numOfVariables];
    double (*B)[numOfVariables][numOfVariables];
    
    // result matrix
    BCDataMatrix *resMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
    A = [resMatrix dataMatrix];
    for (i = 0; i < numOfVariables; ++i)
        for (j = 0; j < numOfVariables; ++j)
            (*A)[i][j] = 0.0;
    
    // constraint matrix
    BCDataMatrix *cMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
    double (*CM)[numOfVariables][numOfVariables];
    CM = [cMatrix dataMatrix];
    
    // baseline matrix
    double (*BM)[numOfVariables][numOfVariables];
    BM = [baselineMatrix dataMatrix];
    
    // prior network matrix
    double (*PNM)[numOfVariables][numOfVariables];
    BCDataMatrix *priorNetworkMatrix = [dataModel priorNetworkMatrix];
    PNM = [priorNetworkMatrix dataMatrix];
    if (usePriorMatrix) constraintMatrix = priorNetworkMatrix;
    else constraintMatrix = nil;
    
    // minimum error
    BCOptimization *bco = [BCOptimization new];
    [bco setAllowSelfConnections: selfConnections];
    [bco addDataHandler: dataModel];
    [bco loadData: numOfVariables];

    // optimize each gene separately
    BCDataMatrix *tmpMatrix;
    for (varIdx = 21; varIdx < numOfVariables; ++varIdx) {
        NSAutoreleasePool *pool = [NSAutoreleasePool new];
        
        double alpha = 0.0, beta = 0.0;
        if (usePriorMatrix) {
            [self crossValidateMatrixOptimizeVariable: varIdx withData: dataModel optimalAlpha: &alpha optimalBeta: &beta];
        } else {
            [self crossValidateOptimizeVariable: varIdx withData: dataModel optimalAlpha: &alpha];
        }
        
        // do optimization with final values
        for (i = 0; i < numOfVariables; ++i)
            for (j = 0; j < numOfVariables; ++j) {
                (*CM)[i][j] = alpha;
                if (usePriorMatrix && ((*PNM)[i][j] != 0.0)) (*CM)[i][j] += beta;
                if ((useBaselineMatrix) && (baselineMatrix)) (*CM)[i][j] += (*BM)[i][j];
            }
        //print_matrix("CM = \n", numOfVariables, numOfVariables, CM);
        
        tmpMatrix = [bco optimizeVariable: varIdx withConstraintMatrix: cMatrix];
        
        B = [tmpMatrix dataMatrix];
        if ((keepZeroAlpha) || (alpha > 0)) {
            [bco outputCytoscapeGraph: B withGeneNames: geneNames];
            for (i = 0; i < numOfVariables; ++i) (*A)[i][varIdx] = (*B)[i][varIdx];
            for (i = 0; i < numOfVariables; ++i) printf("%lf ", (*A)[i][varIdx]);
            printf("\n");
        }
        
        printf("variable: %d alpha = %lf beta = %lf\n\n", varIdx, alpha, beta);

	printf("pool: %d\n", [pool autoreleaseCount]);
        [pool drain];
	printf("pool: %d\n", [pool autoreleaseCount]);
    }
    
    return resMatrix;
}

- (BCDataMatrix *)doVariableOptimizeFor:(int)varIdx
{
    int i, j;
    int numOfVariables = [dataModel numOfVariables];
    NSArray *geneNames = [dataModel geneNames];
    
    // perform cross-validation optimization
    double (*A)[numOfVariables][numOfVariables];
    double (*B)[numOfVariables][numOfVariables];
    
    // result matrix
    BCDataMatrix *resMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
    A = [resMatrix dataMatrix];
    for (i = 0; i < numOfVariables; ++i)
        for (j = 0; j < numOfVariables; ++j)
            (*A)[i][j] = 0.0;
    
    // constraint matrix
    BCDataMatrix *cMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
    double (*CM)[numOfVariables][numOfVariables];
    CM = [cMatrix dataMatrix];
    
    // baseline matrix
    double (*BM)[numOfVariables][numOfVariables];
    BM = [baselineMatrix dataMatrix];
    
    // prior network matrix
    double (*PNM)[numOfVariables][numOfVariables];
    BCDataMatrix *priorNetworkMatrix = [dataModel priorNetworkMatrix];
    PNM = [priorNetworkMatrix dataMatrix];
    if (usePriorMatrix) constraintMatrix = priorNetworkMatrix;
    else constraintMatrix = nil;
    
    // minimum error
    BCOptimization *bco = [BCOptimization new];
    [bco setAllowSelfConnections: selfConnections];
    [bco addDataHandler: dataModel];
    [bco loadData: numOfVariables];

    // optimize each gene separately
    BCDataMatrix *tmpMatrix;
    {
        NSAutoreleasePool *pool = [NSAutoreleasePool new];
        
        double alpha = 0.0, beta = 0.0;
        if (usePriorMatrix) {
            [self crossValidateMatrixOptimizeVariable: varIdx withData: dataModel optimalAlpha: &alpha optimalBeta: &beta];
        } else {
            [self crossValidateOptimizeVariable: varIdx withData: dataModel optimalAlpha: &alpha];
        }
        
        // do optimization with final values
        for (i = 0; i < numOfVariables; ++i)
            for (j = 0; j < numOfVariables; ++j) {
                (*CM)[i][j] = alpha;
                if (usePriorMatrix && ((*PNM)[i][j] != 0.0)) (*CM)[i][j] += beta;
                if ((useBaselineMatrix) && (baselineMatrix)) (*CM)[i][j] += (*BM)[i][j];
            }
        //print_matrix("CM = \n", numOfVariables, numOfVariables, CM);
        
        tmpMatrix = [bco optimizeVariable: varIdx withConstraintMatrix: cMatrix];
        
        B = [tmpMatrix dataMatrix];
        if ((keepZeroAlpha) || (alpha > 0)) {
            [bco outputCytoscapeGraph: B withGeneNames: geneNames];
            for (i = 0; i < numOfVariables; ++i) (*A)[i][varIdx] = (*B)[i][varIdx];
            for (i = 0; i < numOfVariables; ++i) printf("%lf ", (*A)[i][varIdx]);
            printf("\n");
        }
        
        printf("variable: %d alpha = %lf beta = %lf\n\n", varIdx, alpha, beta);

	printf("pool: %d\n", [pool autoreleaseCount]);
        [pool drain];
	printf("pool: %d\n", [pool autoreleaseCount]);
    }
    
    return resMatrix;
}

#if 0
#pragma mark == ENUMERATE NETWORKS ==
#endif

- (void)enumerateNetworksForVariable: (int)varIndex withData: (id)aData withInitialNetwork: (BCDataMatrix *)aNetwork
{
}

@end


#if 0
#pragma mark == WORKER CLASS ==
#endif

//
// Worker class for doing concurrent operations
//

@implementation BCOptimizationWorker

- initWithModel: (BCOptimizationDataHandler *)aModel constraintMatrix: (BCDataMatrix *)aMatrix
{
    [super init];
    
    model = aModel;
    //cMatrix = [aMatrix retain];
    cMatrix = aMatrix;
    //printf("NSExtraRefCount cMatrix: %ld\n", NSExtraRefCount(cMatrix));
    resMatrix =nil;
    
    return self;
}

- (void)setAllowSelfConnections: (BOOL)aFlag { selfConnections = aFlag; }
- (void)setCurrentVariable: (int)aVar { currentVariable = aVar; }
- (BCDataMatrix *)getResult { return resMatrix; }

- (void)dealloc
{
    if (resMatrix) [resMatrix release];
    //[cMatrix release];
    //printf("dealloc NSExtraRefCount cMatrix: %ld\n", NSExtraRefCount(cMatrix));
    
    [super dealloc];
}

- (void)doMatrixOptimize
{
    // perform optimization
    BCOptimization *cvBCO = [BCOptimization new];
    [cvBCO setAllowSelfConnections: selfConnections];
    [cvBCO addDataHandler: model];
    [cvBCO loadData: [model numOfVariables]];
    
    //A = [cvBCO optimizeWithConstraintMatrix: cMatrix andInitialMatrix: IM];
    //A = [cvBCO optimizeWithConstraintMatrix: cMatrix];
    [cvBCO release];
}

- (void)doVariableOptimize
{
    // perform optimization
    BCOptimization *cvBCO = [BCOptimization new];
    //[cvBCO autorelease];
    [cvBCO setAllowSelfConnections: selfConnections];
    [cvBCO addDataHandler: model];
    [cvBCO loadData: [model numOfVariables]];
    
    resMatrix = [cvBCO optimizeVariable: currentVariable withConstraintMatrix: cMatrix];
    [resMatrix retain];
    //printf("NSExtraRefCount resMatrix: %ld\n", NSExtraRefCount(resMatrix));
    [cvBCO release];
}

@end

