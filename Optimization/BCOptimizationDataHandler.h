/*
 BCOptimizationDataHandler.h
 
 Copyright (C) 2008-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: October 2008
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#ifndef _BC_OPTIMIZATION_DATA_HANDLER_H_
#define _BC_OPTIMIZATION_DATA_HANDLER_H_

#import <Foundation/Foundation.h>
#import "BCOptimization.h"

@interface BCOptimizationDataHandler : NSObject <BCOptimizeDataHandler>
{
	// raw data
	int numOfVariables;
	int numOfObservations;
	BCDataMatrix *rawDataMatrix;
	BCDataMatrix *rawResponseMatrix;
	BCDataMatrix *missingDataMatrix;
  BCDataMatrix *priorNetworkMatrix;
  int *observationFlags;
  int *observationGenes;
  id *observationGeneList;
  NSArray *geneNames;
  BOOL complementPrior;

	// matrices actually used for optimization
	void *dataMatrix;
	void *responseMatrix;
  void **sumOfSquares;
}

- initWithDescriptorFile: (NSString *)descFile;
- initWithDataMatrix: (BCDataMatrix *)dMatrix andResponse: (BCDataMatrix *)rMatrix;
- initWithDataMatrix: (BCDataMatrix *)dMatrix andResponse: (BCDataMatrix *)rMatrix scaleData: (BOOL)aFlag;

- (void)setObservationFlags: (int *)flags genes: (int *)genes geneList: (id *)aList;

- (void)setupDataMatrix;
- (void)setupResponseMatrix;

- (void)scaleData: (void *)aMatrix;
- (void)normalizeData: (void *)aMatrix;

- (int)numOfVariables;
- (int)numOfObservations;
- (BCDataMatrix *)dataMatrix;
- (BCDataMatrix *)responseMatrix;
- (BCDataMatrix *)missingDataMatrix;
- (BCDataMatrix *)priorNetworkMatrix;
- (NSArray *)geneNames;
- (BOOL)complementPrior;

// BCOptimizeDataHandler protocol
- newModelWithoutObservation: (int)aRow;
- newModelWithoutStart: (int)startRow toEndObservation: (int)endRow;
- (void)loadDataSource: (int)aSource dataMatrix: (void **)data responseMatrix: (void **)response numberOfObservations: (int *)aCount;
- (BOOL)crossValidateDataSource: (int)aSource dataMatrix: (void **)data responseMatrix: (void **)response row: (int)aRow column: (int)aColumn;
- (double)valueFromDataSource: (int)aSource forType: (BCOptimizeCoefficient)aType matrix: (void *)aMatrix row: (int)aRow column: (int)aColumn;

@end

#endif // _BC_OPTIMIZATION_DATA_HANDLER_H_
