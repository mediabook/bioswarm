/*
 BCOptimizationDataHandler.m
 
 Copyright (C) 2008-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: October 2008
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BCOptimizationDataHandler.h"

#include <math.h>

@implementation BCOptimizationDataHandler

- initWithDescriptorFile: (NSString *)descFile
{
  int i, j, k, l, fileIdx;
  id s;
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  
  NSDictionary *dataDesc = [NSDictionary dictionaryWithContentsOfFile: descFile];
  if (!dataDesc) return nil;
  
  id geneSet = [dataDesc objectForKey: @"geneSet"];
  if (geneSet) geneNames = [geneSet retain];
  
  // go through each data file
  // determine total number of observations
  NSArray *dataFiles = [dataDesc objectForKey: @"dataFiles"];
  NSMutableArray *dataMatrices = [NSMutableArray array];
  NSMutableArray *datas = [NSMutableArray array];
  int no = 0;
  int ng = [geneSet count];
  for (fileIdx = 0; fileIdx < [dataFiles count]; ++fileIdx) {
    NSDictionary *data1 = [dataDesc objectForKey: [dataFiles objectAtIndex: fileIdx]];
    
    NSString *dataFile = [data1 objectForKey: @"dataFile"];
    if (!dataFile) return nil;
    
    dataFile = [[descFile stringByDeletingLastPathComponent] stringByAppendingPathComponent: dataFile];
    
    NSMutableDictionary *format = [data1 objectForKey: @"format"];
    
    BCDataMatrix *dm = [BCDataMatrix dataMatrixWithContentsOfFile: dataFile
                                                        andEncode: BCdoubleEncode
                                                        andFormat: format];
    
    if (!dm) return nil;
    
    s = [format objectForKey: @"transpose"];
    if (s && [s boolValue]) dm = [dm dataMatrixFromTranspose];
    
    [dataMatrices addObject: dm];
    [datas addObject: data1];

    id observations = [data1 objectForKey: @"observations"];
    if (geneSet || observations) {
      // TODO: determine num of genes
      
      if (observations) {
        id wildtype = [observations objectForKey: @"wildtype"];
        no += [wildtype count];
        
        id knockout = [observations objectForKey: @"knockout"];
        if (knockout) {
          for (i = 0; i < [knockout count]; ++i) {
            s = [NSString stringWithFormat: @"%@_knockout", [knockout objectAtIndex: i]];
            no += [[observations objectForKey: s] count];
          }
        }
      } else no = [dm numberOfRows];
    }
  }

  printf("ng = %d, no = %d\n", ng, no);

  // go through each data matrix
  // collect observations into single matrix
  BCDataMatrix *ndm = [BCDataMatrix emptyDataMatrixWithRows: no andColumns: ng andEncode: BCdoubleEncode];
  double (*NDM)[no][ng];
  NDM = [ndm dataMatrix];
  observationFlags = (int *)malloc(sizeof(int) * no);
  observationGenes = (int *)malloc(sizeof(int) * no);
  observationGeneList = (id *)malloc(sizeof(id) * no);
  for (i = 0; i < no; ++i) {
    observationFlags[i] = 0;
    observationGenes[i] = -1;
    observationGeneList[i] = nil;
  }
  int currentRow = 0;

  for (fileIdx = 0; fileIdx < [dataMatrices count]; ++fileIdx) {
    BCDataMatrix *dm = [dataMatrices objectAtIndex: fileIdx];
    NSDictionary *data1 = [datas objectAtIndex: fileIdx];
    
    NSArray *columnNames = [dm columnNames];
    int numRows = [dm numberOfRows];
    int numCols = [dm numberOfColumns];
    double (*DM)[numRows][numCols];
    DM = [dm dataMatrix];
    id observations = [data1 objectForKey: @"observations"];

    if (observations) {
      // wildtype observations
      id wildtype = [observations objectForKey: @"wildtype"];
      for (i = 0; i < [wildtype count]; ++i) {
        int ni = [[wildtype objectAtIndex:i] intValue];
        if (geneSet) {
          for (j = 0; j < ng; ++j) {
            NSUInteger nj = [columnNames indexOfObject: [geneSet objectAtIndex: j]];
            if (nj == NSNotFound) printf("ERROR: unknown gene %s\n", [[geneSet objectAtIndex: j] UTF8String]);
            else (*NDM)[currentRow][j] = (*DM)[ni][nj];
          }
        } else {
          for (j = 0; j < ng; ++j) (*NDM)[currentRow][j] = (*DM)[ni][j];
        }
        observationFlags[currentRow] = 0;
        observationGenes[currentRow] = -1;
        ++currentRow;
      }
      
      // knockout observations
      id knockout = [observations objectForKey: @"knockout"];
      for (k = 0; k < [knockout count]; ++k) {
        s = [NSString stringWithFormat: @"%@_knockout", [knockout objectAtIndex: k]];
        id sk = [observations objectForKey:s];

        // go through each observation
        for (l = 0; l < [sk count]; ++l) {
          int ni = [[sk objectAtIndex:l] intValue];
          if (geneSet) {
            for (j = 0; j < ng; ++j) {
              NSUInteger nj = [columnNames indexOfObject: [geneSet objectAtIndex: j]];
              if (nj == NSNotFound) printf("ERROR: unknown gene %s\n", [[geneSet objectAtIndex: j] UTF8String]);
              else (*NDM)[currentRow][j] = (*DM)[ni][nj];
            }
          } else {
            for (j = 0; j < ng; ++j) (*NDM)[currentRow][j] = (*DM)[ni][j];
          }

          // save knockout info
          s = [NSString stringWithFormat: @"%@_composite", [knockout objectAtIndex: k]];
          id csk = [observations objectForKey:s];
          if (csk) {
            // multiple knockout
            observationFlags[currentRow] = 3;
            observationGeneList[currentRow] = [NSMutableArray new];
            for (j = 0; j < [csk count]; ++j) {
              NSUInteger nj = [geneSet indexOfObject: [csk objectAtIndex: j]];
              if (nj == NSNotFound) printf("ERROR: unknown gene %s\n", [[csk objectAtIndex: j] UTF8String]);
              [observationGeneList[currentRow] addObject: [NSNumber numberWithUnsignedInteger: nj]];

              // zero out the knockout data?
              (*NDM)[currentRow][nj] = 0;
            }
          } else {
            // single knockout
            observationFlags[currentRow] = 1;
            NSUInteger skn = [geneSet indexOfObject: [knockout objectAtIndex:k]];
            if (skn == NSNotFound) printf("ERROR: knockout gene %s not listed in geneSet.\n", [[knockout objectAtIndex: k] UTF8String]);
            observationGenes[currentRow] = skn;

            // zero out the knockout data?
            (*NDM)[currentRow][skn] = 0;
          }
                    
          ++currentRow;
        }
      }
      
    } else {
      // all observations and subset of genes
      
      for (j = 0; j < ng; ++j) {
        NSUInteger n = [columnNames indexOfObject: [geneSet objectAtIndex: j]];
        if (n == NSNotFound) printf("ERROR: unknown gene %s\n", [[geneSet objectAtIndex: j] UTF8String]);
        else for (i = 0; i < no; ++i) (*NDM)[i][j] = (*DM)[i][n];
      }
      [ndm setColumnNames: geneSet];
    }
  }
  
  // Response matrix
  BCDataMatrix *rm = [BCDataMatrix emptyDataMatrixWithRows: no andColumns: ng andEncode: BCdoubleEncode];
  double (*RM)[no][ng];
  RM = [rm dataMatrix];
  
  for (i = 0; i < no; ++i)
    for (j = 0; j < ng; ++j)
      (*RM)[i][j] = (*NDM)[i][j];

  // Prior network information
  priorNetworkMatrix = nil;
  NSDictionary *priorNetwork = [dataDesc objectForKey: @"priorNetworkFile"];
  if (priorNetwork) {
    // get file path
    NSString *dataFile = [priorNetwork objectForKey: @"dataFile"];
    if (!dataFile) return nil;
    dataFile = [[descFile stringByDeletingLastPathComponent] stringByAppendingPathComponent: dataFile];
    
    // load data
    NSMutableDictionary *format = [NSMutableDictionary dictionary];
    [format setObject: geneNames forKey: BCRowNames];
    [format setObject: geneNames forKey: BCColumnNames];
    [format addEntriesFromDictionary: [priorNetwork objectForKey: @"format"]];    
    priorNetworkMatrix = [BCDataMatrix dataMatrixWithContentsOfFile: dataFile
                                                          andEncode: BCdoubleEncode
                                                          andFormat: format];
    if (!priorNetworkMatrix) return nil;
    [priorNetworkMatrix retain];
    
    // by default we complement the prior network matrix
    // so edges have weight = 0
    // and non-edges have weight = 1
    // unless asked not to invert
    complementPrior = YES;
    s = [priorNetwork objectForKey: @"complementPrior"];
    if (s) complementPrior = [s boolValue];
    if (complementPrior) {
      double (*PM)[ng][ng];
      PM = [priorNetworkMatrix dataMatrix];

      // complement the matrix
      for (i = 0; i < ng; ++i)
        for (j = 0; j < ng; ++j)
          if ((*PM)[i][j] == 0) (*PM)[i][j] = 1.0;
          else (*PM)[i][j] = 0.0;
    }
  }
  
  // scale data by default
  s = [dataDesc objectForKey: @"scaleData"];
  if (s && ![s boolValue])
    [self initWithDataMatrix: ndm andResponse: rm scaleData: NO];
  else
    [self initWithDataMatrix: ndm andResponse: rm scaleData: YES];

  [pool drain];
  return self;
}


- initWithDataMatrix: (BCDataMatrix *)dMatrix andResponse: (BCDataMatrix *)rMatrix
{
  return [self initWithDataMatrix: dMatrix andResponse: rMatrix scaleData: YES];
}

- initWithDataMatrix: (BCDataMatrix *)dMatrix andResponse: (BCDataMatrix *)rMatrix scaleData: (BOOL)aFlag
{
	[super init];
	
	rawDataMatrix = [dMatrix retain];
	rawResponseMatrix = [rMatrix retain];

	numOfVariables = [rawDataMatrix numberOfColumns];
	numOfObservations = [rawDataMatrix numberOfRows];

	missingDataMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfObservations andColumns: numOfVariables andEncode: BCboolEncode];
  [missingDataMatrix retain];
	BOOL (*MD)[numOfObservations][numOfVariables];
	MD = [missingDataMatrix dataMatrix];
	int i, j;
	for (i = 0; i < numOfObservations; ++i)
		for (j = 0; j < numOfVariables; ++j) {
			(*MD)[i][j] = NO;
		}

	dataMatrix = NULL;
	if (aFlag) [self scaleData: [rawDataMatrix dataMatrix]];
	[self setupDataMatrix];
	responseMatrix = NULL;
	if (aFlag) [self scaleData: [rawResponseMatrix dataMatrix]];
	[self setupResponseMatrix];

  //print_matrix("Stage 10 Data = \n", numOfObservations, numOfVariables, dataMatrix);

	return self;
}

- (void)dealloc
{
  if (geneNames) [geneNames release];
	if (dataMatrix) free(dataMatrix);
	if (responseMatrix) free(responseMatrix);
	if (missingDataMatrix) [missingDataMatrix release];
  if (rawDataMatrix) [rawDataMatrix release];
  if (rawResponseMatrix) [rawResponseMatrix release];
  if (priorNetworkMatrix) [priorNetworkMatrix release];
  if (observationFlags) free(observationFlags);
  if (observationGenes) free(observationGenes);
  if (observationGeneList) {
    int i;
    for (i = 0; i < numOfObservations; ++i)
      if (observationGeneList[i]) [observationGeneList[i] release];
    free(observationGeneList);
  }
	if (sumOfSquares) {
    int i;
    for (i = 0; i < numOfObservations; ++i)
      if (sumOfSquares[i]) free(sumOfSquares[i]);
    free(sumOfSquares);
  }

	[super dealloc];
}

- (void)setObservationFlags: (int *)flags genes: (int *)genes geneList: (id *)aList
{
  observationFlags = flags;
  observationGenes = genes;
  observationGeneList = aList;
}

- (void)setupSumOfSquares
{
  int i, j, k;
  
	if (!sumOfSquares) {
		sumOfSquares = malloc(numOfObservations * sizeof(void *));
		for (i = 0; i < numOfObservations; ++i) sumOfSquares[i] = NULL;
	}
  
	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = dataMatrix;

  // Di = X' * X  for observation i
  // even though not all observations are null mutants,
  // easier to calculate so we will just not use that data
	double (*Di)[numOfVariables][numOfVariables];
	for (i = 0; i < numOfObservations; ++i) {
		if (!sumOfSquares[i]) {
			Di = malloc(numOfVariables * numOfVariables * sizeof(double));
			sumOfSquares[i] = Di;
		} else Di = sumOfSquares[i];
    
		for (j = 0; j < numOfVariables; ++j) {
			for (k = 0; k < numOfVariables; ++k) {
				(*Di)[j][k] = (*X)[i][j] * (*X)[i][k];
			}
		}

    //printf("D_%d = ", i);
    //print_matrix("\n", numOfVariables, numOfVariables, Di);
	}
  
}

- (void)setupDataMatrix
{
	double (*X)[numOfObservations][numOfVariables];
	X = [rawDataMatrix dataMatrix];
	double (*nX)[numOfObservations][numOfVariables];
	if (!dataMatrix) dataMatrix = malloc(numOfObservations * numOfVariables * sizeof(double));
	nX = dataMatrix;
	
	int i, j;
	for (i = 0; i < numOfObservations; ++i)
		for (j = 0; j < numOfVariables; ++j) {
			(*nX)[i][j] = (*X)[i][j];
		}
	
  [self setupSumOfSquares];

	//[self normalizeData: dataMatrix];
	//[self scaleData: dataMatrix];

#if 0
	printf ("X = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*nX)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");
#endif
}

- (void)setupResponseMatrix
{
	double (*X)[numOfObservations][numOfVariables];
	X = [rawResponseMatrix dataMatrix];
	double (*nX)[numOfObservations][numOfVariables];
	if (!responseMatrix) responseMatrix = malloc(numOfObservations * numOfVariables * sizeof(double));
	nX = responseMatrix;
	
	int i, j;
	for (i = 0; i < numOfObservations; ++i)
		for (j = 0; j < numOfVariables; ++j) {
			(*nX)[i][j] = (*X)[i][j];
		}
	
	//[self normalizeData: responseMatrix];
	//[self scaleData: responseMatrix];

#if 0
	printf ("B = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*nX)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");
#endif
}

- (int)numOfVariables { return numOfVariables; }
- (int)numOfObservations { return numOfObservations; }
- (BCDataMatrix *)dataMatrix { return rawDataMatrix; }
- (BCDataMatrix *)responseMatrix { return rawResponseMatrix; }
- (BCDataMatrix *)missingDataMatrix { return missingDataMatrix; }
- (BCDataMatrix *)priorNetworkMatrix { return priorNetworkMatrix; }
- (NSArray *)geneNames { return geneNames; }
- (BOOL)complementPrior { return complementPrior; }


- (void)scaleData: (void *)aMatrix
{
	int i, j;

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = aMatrix;

	// divide each row by row maximum
	for (i = 0; i < numOfObservations; ++i) {
		double maxValue = 0.0;
		for (j = 0; j < numOfVariables; ++j)
			if (fabs((*X)[i][j]) > maxValue) maxValue = fabs((*X)[i][j]);
		if (maxValue != 0.0)
			for (j = 0; j < numOfVariables; ++j) (*X)[i][j] = (*X)[i][j] / maxValue;
		//printf("%d %lf\n", i, maxValue);
	}
}

- (void)normalizeData: (void *)aMatrix
{
	int i, j, cnt;

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)aMatrix;
	BOOL (*MD)[numOfObservations][numOfVariables];
	if (missingDataMatrix) MD = [missingDataMatrix dataMatrix];
	else MD = NULL;

	// temporary matrices
	double meanX[numOfVariables];
	double sdX[numOfVariables];

	// calculate means
	for (i = 0; i < numOfVariables; ++i) {
		meanX[i] = 0.0;
		cnt = 0;
		for (j = 0; j < numOfObservations; ++j) {
			if ((!MD) || (!(*MD)[j][i])) {
				meanX[i] += (*X)[j][i];
				++cnt;
			}
		}
		meanX[i] = meanX[i] / (double)cnt;
		//printf("mean[%d] = %lf count: %d\n", i, meanX[i], cnt);
	}

	// calculate sample standard deviations
	for (i = 0; i < numOfVariables; ++i) {
		sdX[i] = 0.0;
		cnt = 0;
		for (j = 0; j < numOfObservations; ++j) {
			if ((!MD) || (!(*MD)[j][i])) {
				sdX[i] += (*X)[j][i] * (*X)[j][i];
				++cnt;
			}
		}
		sdX[i] = sqrt(sdX[i] / (double)(cnt - 1));
		//printf("sd[%d] = %lf count: %d\n", i, sdX[i], cnt);
	}

	// mean shift
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfObservations; ++j) {
			if ((!MD) || (!(*MD)[j][i])) {
				(*X)[j][i] -= meanX[i];
			}
		}
	}

#if 0	
	printf ("X = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*X)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");
#endif

	// normalize deviation
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfObservations; ++j) {
			if ((!MD) || (!(*MD)[j][i])) {
				if (sdX[i] != 0.0) (*X)[j][i] = (*X)[j][i] / sdX[i];
			}
		}
	}

#if 0
	printf ("X = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*X)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");
#endif
}

#if 0
#pragma mark == BCOptimizeDataHandler PROTOCOL ==
#endif

- newModelWithoutObservation: (int)aRow
{
	BCDataMatrix *cvXMatrix = [BCDataMatrix emptyDataMatrixWithRows: (numOfObservations - 1) andColumns: numOfVariables andEncode: BCdoubleEncode];
	//[cvXMatrix autorelease];
	double (*cvX)[numOfObservations - 1][numOfVariables];
	cvX = [cvXMatrix dataMatrix];
	BCDataMatrix *cvBMatrix = [BCDataMatrix emptyDataMatrixWithRows: (numOfObservations - 1) andColumns: numOfVariables andEncode: BCdoubleEncode];
	//[cvBMatrix autorelease];
	double (*cvB)[numOfObservations - 1][numOfVariables];
	cvB = [cvBMatrix dataMatrix];

  double (*X)[numOfObservations][numOfVariables];
  X = [rawDataMatrix dataMatrix];
  double (*B)[numOfObservations][numOfVariables];
  B = [rawResponseMatrix dataMatrix];
    
  // construct data and response matrix
  // leave one row out
  int r = 0;
  int j, k;
  for (j = 0; j < numOfObservations; ++j) {
    if (j == aRow) continue;
    for (k = 0; k < numOfVariables; ++k) {
      (*cvX)[r][k] = (*X)[j][k];
      (*cvB)[r][k] = (*B)[j][k];
    }
    ++r;
  }

  id cvModel = [[[self class] alloc] initWithDataMatrix: cvXMatrix andResponse: cvBMatrix scaleData: NO];
    
  if (observationFlags) {
    int *newFlags = (int *)malloc(sizeof(int) * (numOfObservations - 1));
    int *newGenes = (int *)malloc(sizeof(int) * (numOfObservations - 1));
    id *newGeneList = (id *)malloc(sizeof(id) * (numOfObservations - 1));
    int r = 0;
    for (j = 0; j < numOfObservations; ++j) {
      if (j == aRow) continue;
      newFlags[r] = observationFlags[j];
      newGenes[r] = observationGenes[j];
      newGeneList[r] = nil;
      if (observationGeneList[j])
        newGeneList[r] = [[NSArray arrayWithArray: observationGeneList[j]] retain];
      ++r;
    }
    [cvModel setObservationFlags: newFlags genes: newGenes geneList: newGeneList];
  }

  return cvModel;
}

- newModelWithoutStart: (int)startRow toEndObservation: (int)endRow
{
    int newNumOfObservations = numOfObservations - (endRow - startRow + 1);
    BCDataMatrix *cvXMatrix = [BCDataMatrix emptyDataMatrixWithRows: newNumOfObservations andColumns: numOfVariables andEncode: BCdoubleEncode];
    //[cvXMatrix autorelease];
    double (*cvX)[newNumOfObservations][numOfVariables];
    cvX = [cvXMatrix dataMatrix];
    BCDataMatrix *cvBMatrix = [BCDataMatrix emptyDataMatrixWithRows: newNumOfObservations andColumns: numOfVariables andEncode: BCdoubleEncode];
    //[cvBMatrix autorelease];
    double (*cvB)[newNumOfObservations][numOfVariables];
    cvB = [cvBMatrix dataMatrix];
    
    double (*X)[numOfObservations][numOfVariables];
    X = [rawDataMatrix dataMatrix];
    double (*B)[numOfObservations][numOfVariables];
    B = [rawResponseMatrix dataMatrix];
    
    // construct data and response matrix
    // leave out rows
    int r = 0;
    int j, k;
    for (j = 0; j < numOfObservations; ++j) {
        if ((j >= startRow) && (j <= endRow)) continue;
        for (k = 0; k < numOfVariables; ++k) {
            (*cvX)[r][k] = (*X)[j][k];
            (*cvB)[r][k] = (*B)[j][k];
        }
        ++r;
    }
    
    id cvModel = [[[self class] alloc] initWithDataMatrix: cvXMatrix andResponse: cvBMatrix scaleData: NO];
    
    if (observationFlags) {
        int *newFlags = (int *)malloc(sizeof(int) * newNumOfObservations);
        int *newGenes = (int *)malloc(sizeof(int) * newNumOfObservations);
        id *newGeneList = (id *)malloc(sizeof(id) * newNumOfObservations);
        int r = 0;
        for (j = 0; j < numOfObservations; ++j) {
            if ((j >= startRow) && (j <= endRow)) continue;
            newFlags[r] = observationFlags[j];
            newGenes[r] = observationGenes[j];
            newGeneList[r] = nil;
            if (observationGeneList[j]) {
                newGeneList[r] = [[NSArray arrayWithArray: observationGeneList[j]] retain];
            }
            ++r;
        }
        [cvModel setObservationFlags: newFlags genes: newGenes geneList: newGeneList];
    }
    
    return cvModel;
}

- (void)loadDataSource: (int)aSource dataMatrix: (void **)data responseMatrix: (void **)response numberOfObservations: (int *)aCount
{
	*data = dataMatrix;
	*response = responseMatrix;
	*aCount = numOfObservations;
}

- (BOOL)crossValidateDataSource: (int)aSource dataMatrix: (void **)data responseMatrix: (void **)response row: (int)aRow column: (int)aColumn
{
	double (*X)[numOfObservations][numOfVariables];
	double (*cvX)[numOfObservations][numOfVariables];
	X = [rawDataMatrix dataMatrix];
	cvX = dataMatrix;
	BOOL (*MD)[numOfObservations][numOfVariables];
	MD = [missingDataMatrix dataMatrix];

	if ((aColumn != -1.0) && ((*MD)[aRow][aColumn])) return NO;
	if ((aColumn != -1.0) && ((*X)[aRow][aColumn] == 0.0)) return NO;

	// initialize data
	int i, j;
	for (i = 0; i < numOfObservations; ++i)
		for (j = 0; j < numOfVariables; ++j)
			(*cvX)[i][j] = (*X)[i][j];

	// cross out row
	if (aColumn == -1.0) {
		// remove whole row
		for (j = 0; j < numOfVariables; ++j) (*cvX)[aRow][j] = 0.0;
	} else {
		// remove single data point
		(*cvX)[aRow][aColumn] = 0.0;
		//(*MD)[aRow][aColumn] = YES;
	}

	//[self normalizeData: dataMatrix];
	//(*cvX)[aRow][aColumn] = 0.0;
	//(*MD)[aRow][aColumn] = NO;

	if (data) *data = dataMatrix;
	if (response) *response = responseMatrix;

	return YES;
}

- (double)valueFromDataSource: (int)aSource forType: (BCOptimizeCoefficient)aType matrix: (void *)aMatrix row: (int)aRow column: (int)aColumn
{
  int i, k;
	double aValue = 0.0;
	double (*X)[numOfObservations][numOfVariables];
	double (*R)[numOfObservations][numOfVariables];
	double (*A)[numOfVariables][numOfVariables];
	A = aMatrix;
  X = dataMatrix;
  R = responseMatrix;
  
  if (observationFlags) {
    for (i = 0; i < numOfObservations; ++i) {
      
      switch (observationFlags[i]) {
        // null-mutant data
        // single knockout
        case 1: {
          // if this observation is not for the knockout gene then keep it
          if (observationGenes[i] != aColumn) continue;

          double (*D)[numOfVariables][numOfVariables];
          D = sumOfSquares[i];
          
          switch (aType) {
            case BCConnectionCoefficient:
              for (k = 0; k < numOfVariables; ++k) {
                aValue += (*D)[aRow][k] * (*A)[k][aColumn];
                //if ((aRow == 5) && (aColumn == 33)) printf("(%d) %lf %lf %lf\n", i, aValue, (*D)[aRow][k], (*A)[k][aColumn]);
              }
              break;
            case BCDiagonalCoefficient:
              aValue += (*D)[aRow][aRow];
              break;
            case BCInterceptCoefficient:
              //aValue -= (*X)[aColumn][aRow] * (*X)[aColumn][aColumn];
              // really should be X[k,aRow] B[k,aColumn]
              aValue -= (*X)[i][aRow] * (*R)[i][aColumn];
              //if ((aRow == 5) && (aColumn == 33)) printf("(%d) IC = %lf %lf %lf\n", i, aValue, (*X)[i][aRow], (*X)[i][aColumn]);
              break;
          }
          break;
        }

        // null-mutant data
        // multiple knockout
        case 3: {
          // if this observation is not one of the knockout genes then keep it
          BOOL found = NO;
          for (k = 0; k < [observationGeneList[i] count]; ++k) {
            if ([[observationGeneList[i] objectAtIndex: k] unsignedIntegerValue] == aColumn) {
              found = YES;
              break;
            }
          }
          if (!found) continue;
          //NSNumber *n = [NSNumber numberWithInt: aColumn];
          //k = [observationGeneList[i] indexOfObject: n];
          //if (k == NSNotFound) continue;

          double (*D)[numOfVariables][numOfVariables];
          D = sumOfSquares[i];
          
          switch (aType) {
            case BCConnectionCoefficient:
              for (k = 0; k < numOfVariables; ++k) {
                aValue += (*D)[aRow][k] * (*A)[k][aColumn];
              }
              break;
            case BCDiagonalCoefficient:
              aValue += (*D)[aRow][aRow];
              break;
            case BCInterceptCoefficient:
              aValue -= (*X)[i][aRow] * (*R)[i][aColumn];
              break;
          }
          break;
        }

        case 2: {
          // heterozygous data
#if 0
          X = heteroDataMatrix;
          double (*D)[numOfGenes][numOfGenes];
          D = heteroSumOfSquares[aColumn];
          double (*P)[numOfGenes];
          P = heteroPerturb;
          
          switch (aType) {
            case BCConnectionCoefficient:
              for (k = 0; k < numOfGenes; ++k) {
                aValue += (*D)[aRow][k] * (*A)[k][aColumn];
              }
              break;
            case BCDiagonalCoefficient:
              aValue = (*D)[aRow][aRow];
              break;
            case BCInterceptCoefficient:
              aValue -= (*X)[aColumn][aRow] * ((*X)[aColumn][aColumn] - 0.5 * (*P)[aColumn]);
              break;
          }
#endif
          break;
        }
      }
    }
  } else {
    // standard form
    aValue = 0.0;
  }
  
  return aValue;
}

@end
