/*
 BCOptimization.m
 
 Copyright (C) 2008-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: July 2008
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "BCOptimization.h"
#include <math.h>
#include <objc/runtime.h>

#define EPS 0.0001
//#define EPS 0.2

#define DEBUG 0

void print_matrix(char *N, int I, int J, void *M)
{
    double (*A)[I][J] = M;
    int i, j;
    
    printf ("%s", N);
    for (i = 0; i < I; ++i) {
        printf("%lf", (*A)[i][0]);
        for (j = 1; j < J; ++j) {
            printf(" %lf", (*A)[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void print_rmatrix(char *N, int I, int J, void *M)
{
  double (*A)[I][J] = M;
  int i, j;
  
  printf ("%s", N);
  for (i = 0; i < I; ++i) {
    for (j = 0; j < J; ++j) {
      printf("% 11.6lf, ", (*A)[i][j]);
    }
    printf("\n    ");
  }
  printf("\n");
}

void print_mmatrix(char *N, int I, int J, void *M)
{
  double (*A)[I][J] = M;
  int i, j;
  
  printf ("%s [", N);
  for (i = 0; i < I; ++i) {
    printf("[");
    for (j = 0; j < J; ++j) {
      printf("% 11.6lf ", (*A)[i][j]);
    }
    printf("];\n    ");
  }
  printf("];\n");
}

void print_SIF(void *aMatrix, NSArray *geneNames, int numOfVariables)
{
	int i, j;
	double (*A)[numOfVariables][numOfVariables];
  
	A = aMatrix;
	printf("\n");
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			if ((*A)[i][j] == 0) continue;
      if ((*A)[i][j] > 0) {
				printf("%s activates %s\n", [[geneNames objectAtIndex: i] UTF8String],
               [[geneNames objectAtIndex: j] UTF8String]);
			} else {
				printf("%s inhibits %s\n", [[geneNames objectAtIndex: i] UTF8String],
               [[geneNames objectAtIndex: j] UTF8String]);
      }
		}
	}
	printf("\n");
}

void print_EDA(void *aMatrix, NSArray *geneNames, int numOfVariables)
{
	int i, j;
	double (*A)[numOfVariables][numOfVariables];
  
	A = aMatrix;
	printf("\nInteractionStrength\n");
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			if ((*A)[i][j] == 0) continue;
      if ((*A)[i][j] > 0) {
				printf("%s (activates) %s = %lf\n", [[geneNames objectAtIndex: i] UTF8String],
               [[geneNames objectAtIndex: j] UTF8String], (*A)[i][j]);
			} else {
				printf("%s (inhibits) %s = %lf\n", [[geneNames objectAtIndex: i] UTF8String],
               [[geneNames objectAtIndex: j] UTF8String], fabs((*A)[i][j]));
      }
		}
	}
	printf("\n");
}

NSString* strprint_SIF(void *aMatrix, NSArray *geneNames, int numOfVariables)
{
  NSMutableString *s = [[NSMutableString new] autorelease];
  int i, j;
	double (*A)[numOfVariables][numOfVariables];
  
	A = aMatrix;
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			if ((*A)[i][j] == 0) continue;
      if ((*A)[i][j] > 0) {
        [s appendFormat: @"%@ activates %@\n", [geneNames objectAtIndex: i], [geneNames objectAtIndex: j]];
			} else {
        [s appendFormat: @"%@ inhibits %@\n", [geneNames objectAtIndex: i], [geneNames objectAtIndex: j]];
      }
		}
	}
  
  return s;
}

NSString* strprint_EDA(void *aMatrix, NSArray *geneNames, int numOfVariables)
{
  NSMutableString *s = [[NSMutableString new] autorelease];
	int i, j;
	double (*A)[numOfVariables][numOfVariables];
  
	A = aMatrix;
	[s appendString: @"InteractionStrength\n"];
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			if ((*A)[i][j] == 0) continue;
      if ((*A)[i][j] > 0) {
        [s appendFormat: @"%@ (activates) %@ = %lf\n", [geneNames objectAtIndex: i], [geneNames objectAtIndex: j], (*A)[i][j]];
			} else {
        [s appendFormat: @"%@ (inhibits) %@ = %lf\n", [geneNames objectAtIndex: i], [geneNames objectAtIndex: j], fabs((*A)[i][j])];
      }
		}
	}

  return s;
}

@implementation BCOptimization

#if 0
#pragma mark == INITIALIZATION ==
#endif

- init
{
	[super init];
	
	dataHandlers = [NSMutableArray new];
	dataHandlerObservations = NULL;
	selfConnections = NO;

	return self;
}

- initWithDataMatrix: (void *)matrix andResponse: (void *)response numberOfVariables: (int)numVars numberOfObservations: (int)numObs
{
	[super init];
	
	dataMatrix = matrix;
	responseMatrix = response;
	numOfVariables = numVars;
	numOfObservations = numObs;
	
	return self;
}

- (void)dealloc
{
	if (initialDataMatrix) free(initialDataMatrix);
	if (initialResponseMatrix) free(initialResponseMatrix);
	if (dataMatrix) free(dataMatrix);
	if (responseMatrix) free(responseMatrix);
	if (dataHandlerObservations) free(dataHandlerObservations);
	if (dataHandlers) [dataHandlers release];

	[super dealloc];
}

- (void)setAllowSelfConnections: (BOOL)aFlag { selfConnections = aFlag; }
- (BOOL)allowSelfConnections { return selfConnections; }

#if 0
#pragma mark == DATA HANDLERS ==
#endif

- (int)addDataHandler: (id)anObject
{
	if (!anObject) return -1;
	[dataHandlers addObject: anObject];
	return [dataHandlers count] - 1;
}

- (void)loadData: (int)anInt
{
	int i, j, k;
	int cnt = [dataHandlers count];
	void *data[cnt];
	void *response[cnt];

	if (!dataHandlerObservations) dataHandlerObservations = malloc(cnt * sizeof(int));
	numOfVariables = anInt;
	numOfObservations = 0;

	// Have each data handler load their data
	for (i = 0; i < cnt; ++i) {
		id anObj = [dataHandlers objectAtIndex: i];
		[anObj loadDataSource: i dataMatrix: &(data[i]) responseMatrix: &(response[i]) numberOfObservations: &(dataHandlerObservations[i])];
		numOfObservations += dataHandlerObservations[i];
	}

	// allocate the final data and response matrices
	initialDataMatrix = malloc(sizeof(double) * numOfObservations * numOfVariables);
	initialResponseMatrix = malloc(sizeof(double) * numOfObservations * numOfVariables);
	dataMatrix = malloc(sizeof(double) * numOfObservations * numOfVariables);
	responseMatrix = malloc(sizeof(double) * numOfObservations * numOfVariables);

	double (*iX)[numOfObservations][numOfVariables];
	iX = (void *)initialDataMatrix;
	double (*iB)[numOfObservations][numOfVariables];
	iB = (void *)initialResponseMatrix;
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;
	int obvNum = 0;

	// copy data from each handler into final data and response matrices
	for (i = 0; i < cnt; ++i) {
		double (*X1)[dataHandlerObservations[i]][numOfVariables];
		X1 = data[i];
		double (*B1)[dataHandlerObservations[i]][numOfVariables];
		B1 = response[i];
		for (j = 0; j < dataHandlerObservations[i]; ++j, ++obvNum) {
			for (k = 0; k < numOfVariables; ++k) {
				(*iX)[obvNum][k] = (*X1)[j][k];
				(*X)[obvNum][k] = (*X1)[j][k];
				(*iB)[obvNum][k] = (*B1)[j][k];
				(*B)[obvNum][k] = (*B1)[j][k];
			}
		}
	}


#if 0
	printf ("X = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*X)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");
#endif
#if 0
	printf ("B = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*B)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");
#endif
}

- (BOOL)loadCrossValidateDataForRow: (int)aRow column: (int)aColumn
{
	int i, j, k;
	int cnt = [dataHandlers count];
	void *data = NULL;
	void *response = NULL;

	if (aRow >= numOfObservations) return NO;

	// find appropriate handler from observation row
	int dRow = 0;
	int r = aRow;
	for (i = 0; i < cnt; ++i) {
		if (r < dataHandlerObservations[i]) break;
		else {
			r -= dataHandlerObservations[i];
			dRow += dataHandlerObservations[i];
		}
	}

	// Have data handler load its cross validation data
	id anObj = [dataHandlers objectAtIndex: i];
	BOOL res = [anObj crossValidateDataSource: i dataMatrix: &data responseMatrix: &response row: r column: aColumn];
	if (!res) return NO;

	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;

	// copy data from each handler into final data and response matrices
	double (*X1)[dataHandlerObservations[i]][numOfVariables];
	X1 = data;
	double (*B1)[dataHandlerObservations[i]][numOfVariables];
	B1 = response;
	for (j = 0; j < dataHandlerObservations[i]; ++j, ++dRow) {
		for (k = 0; k < numOfVariables; ++k) {
			(*X)[dRow][k] = (*X1)[j][k];
			(*B)[dRow][k] = (*B1)[j][k];
		}
	}
	
	return YES;
}

- (void)outputGraph: (void *)aMatrix
{
	int i, j;
	double (*A)[numOfVariables][numOfVariables];

	A = aMatrix;
	printf("digraph lasso {\n");
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			if ((*A)[i][j] != 0) {
				printf("G%d\tG%d\t%lf\n", (i + 1), (j + 1), fabs((*A)[i][j]));
			}
		}
	}
	printf("}\n");
}

- (void)outputDotGraph: (void *)aMatrix withGeneNames: (NSArray *)geneNames;
{
	int i, j;
	double (*A)[numOfVariables][numOfVariables];

	A = aMatrix;
	printf("digraph lasso {\n");
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			if ((*A)[i][j] != 0) {
				printf("\"%s\" -> \"%s\";   %lf\n", [[geneNames objectAtIndex: i] UTF8String],
					[[geneNames objectAtIndex: j] UTF8String], (*A)[i][j]);
			}
		}
	}
	printf("}\n");
}

- (void)outputCytoscapeGraph: (void *)aMatrix withGeneNames: (NSArray *)geneNames
{
	int i, j;
	double (*A)[numOfVariables][numOfVariables];

	A = aMatrix;
	printf("\n");
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			if ((*A)[i][j] == 0) continue;
      if ((*A)[i][j] > 0) {
				printf("%s activates %s\n", [[geneNames objectAtIndex: i] UTF8String],
					[[geneNames objectAtIndex: j] UTF8String]);
			} else {
				printf("%s inhibits %s\n", [[geneNames objectAtIndex: i] UTF8String],
               [[geneNames objectAtIndex: j] UTF8String]);
      }
		}
	}
	printf("\n");
}

- (void)outputCytoscapeEdgeAttributes: (void *)aMatrix withGeneNames: (NSArray *)geneNames
{
	int i, j;
	double (*A)[numOfVariables][numOfVariables];
  
	A = aMatrix;
	printf("\nInteractionStrength\n");
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			if ((*A)[i][j] == 0) continue;
      if ((*A)[i][j] > 0) {
				printf("%s (activates) %s = %lf\n", [[geneNames objectAtIndex: i] UTF8String],
               [[geneNames objectAtIndex: j] UTF8String], (*A)[i][j]);
			} else {
				printf("%s (inhibits) %s = %lf\n", [[geneNames objectAtIndex: i] UTF8String],
               [[geneNames objectAtIndex: j] UTF8String], fabs((*A)[i][j]));
      }
		}
	}
	printf("\n");
}

- (double)maxGamma
{
	int i, j, k;
	
	if ((!dataHandlers) || ([dataHandlers count] == 0)) {
		NSLog(@"No data handlers registered.\n");
		return -1.0;
	}

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;

    //print_matrix("X = ", numOfObservations, numOfVariables, X);
    //print_matrix("B = ", numOfObservations, numOfVariables, B);

	// temporary matrices
	double (*Y)[numOfVariables][numOfVariables];
	Y = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*A)[numOfVariables][numOfVariables];
	A = malloc(numOfVariables * numOfVariables * sizeof(double));

	// Y = X' * B
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*Y)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*Y)[i][j] += (*X)[k][i] * (*B)[k][j];
			}
		}
	}

	// empty connection matrix
	for (i = 0; i < numOfVariables; ++i)
		for (j = 0; j < numOfVariables; ++j)
			(*A)[i][j] = 0.0;

	// maximum gamma
	double maxGamma = -1.0;
	for (i = 0; i < numOfVariables; ++i)
		for (j = 0; j < numOfVariables; ++j) {
			if ((!selfConnections) && (i == j)) continue;
			double interceptCoefficient = (*Y)[i][j];
			for (k = 0; k < [dataHandlers count]; ++k) {
				id anObject = [dataHandlers objectAtIndex: k];
				interceptCoefficient += [anObject valueFromDataSource: k forType: BCInterceptCoefficient matrix: A row: i column: j];
			}
			interceptCoefficient = fabs(interceptCoefficient);
			if (interceptCoefficient > maxGamma) {
				maxGamma = interceptCoefficient;
			}
            //printf("maxGamma(%d, %d) = %lf\n", i, j, interceptCoefficient);
		}

	free(A);
	free(Y);
	
	return maxGamma;
}

- (double)maxGammaForVariable: (int)varIndex
{
	int i, j, k;
	
	if ((!dataHandlers) || ([dataHandlers count] == 0)) {
		NSLog(@"No data handlers registered.\n");
		return -1.0;
	}
    
	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;
    
    //print_matrix("X = ", numOfObservations, numOfVariables, X);
    //print_matrix("B = ", numOfObservations, numOfVariables, B);
    
	// temporary matrices
	double (*Y)[numOfVariables][numOfVariables];
	Y = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*A)[numOfVariables][numOfVariables];
	A = malloc(numOfVariables * numOfVariables * sizeof(double));
    
	// Y = X' * B
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*Y)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*Y)[i][j] += (*X)[k][i] * (*B)[k][j];
			}
		}
	}
    
	// empty connection matrix
	for (i = 0; i < numOfVariables; ++i)
		for (j = 0; j < numOfVariables; ++j)
			(*A)[i][j] = 0.0;
    
	// maximum gamma
	double maxGamma = -1.0;
    j = varIndex;
	for (i = 0; i < numOfVariables; ++i) {
			if ((!selfConnections) && (i == j)) continue;
			double interceptCoefficient = (*Y)[i][j];
			for (k = 0; k < [dataHandlers count]; ++k) {
				id anObject = [dataHandlers objectAtIndex: k];
				interceptCoefficient += [anObject valueFromDataSource: k forType: BCInterceptCoefficient matrix: A row: i column: j];
			}
			interceptCoefficient = fabs(interceptCoefficient);
			if (interceptCoefficient > maxGamma) {
				maxGamma = interceptCoefficient;
			}
        //printf("maxGamma(%d, %d) = %lf\n", i, j, interceptCoefficient);
    }
    
	free(A);
	free(Y);
	
	return maxGamma;
}

- (void)normalizeData
{
	int i, j;

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;

	// temporary matrices
	double meanX[numOfVariables];
	//double meanB[numOfVariables];
	double sdX[numOfVariables];
	//double sdB[numOfVariables];

	// calculate means
	for (i = 0; i < numOfVariables; ++i) {
		meanX[i] = 0.0;
		//meanB[i] = 0.0;
		for (j = 0; j < numOfObservations; ++j) {
			meanX[i] += (*X)[j][i];
			//meanB[i] += (*B)[j][i];
		}
		meanX[i] = meanX[i] / (double)numOfObservations;
		//printf("mean[%d] = %lf\n", i, meanX[i]);
		//meanB[i] = meanB[i] / (double)numOfObservations;
	}

	// mean shift
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfObservations; ++j) {
			(*X)[j][i] -= meanX[i];
			//(*B)[j][i] -= meanB[i];
		}
	}

#if 0	
	printf ("X = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*X)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");
#endif

	// calculate sample standard deviations
	for (i = 0; i < numOfVariables; ++i) {
		sdX[i] = 0.0;
		//sdB[i] = 0.0;
		for (j = 0; j < numOfObservations; ++j) {
			sdX[i] += (*X)[j][i] * (*X)[j][i];
			//sdB[i] += (*B)[j][i] * (*B)[j][i];
		}
		sdX[i] = sqrt(sdX[i] / (double)(numOfObservations - 1));
		//printf("sd[%d] = %lf\n", i, sdX[i]);
		//sdB[i] = sqrt(sdB[i] / (double)(numOfObservations - 1));
	}

	// normalize deviation
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfObservations; ++j) {
			(*X)[j][i] = (*X)[j][i] / sdX[i];
			//(*B)[j][i] = (*B)[j][i] / sdB[i];
		}
	}

#if 0
	printf ("X = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*X)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");

	printf ("B = ");
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			printf("% 11.6lf, ", (*B)[i][j]);
		}
		printf("\n    ");
	}
	printf("\n");
#endif
}

// error is calculated with initial data matrice as optimization
// may be working on cross-validation matrices.
- (double)calculateError: (void *)coefficients
{
	int i, j, k;
	double totError = 0.0;

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)initialDataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)initialResponseMatrix;
	double (*A)[numOfVariables][numOfVariables];
	A = coefficients;

	// temporary matrices
	double (*XA1)[numOfObservations][numOfVariables];
	XA1 = malloc(numOfObservations * numOfVariables * sizeof(double));

	// sum((X * A - B) .* (X * A - B))

	// XA1 = X * A
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*XA1)[i][j] = 0.0;
			for (k = 0; k < numOfVariables; ++k) {
				(*XA1)[i][j] += (*X)[i][k] * (*A)[k][j];
			}
		}
	}

	// XA1 = XA1 - B
	for (i = 0; i < numOfObservations; ++i)
		for (j = 0; j < numOfVariables; ++j)
			(*XA1)[i][j] = (*XA1)[i][j] - (*B)[i][j];

	// sum(XA1 .* XA1)
	for (i = 0; i < numOfObservations; ++i)
		for (j = 0; j < numOfVariables; ++j)
			totError += (*XA1)[i][j] * (*XA1)[i][j];

	free(XA1);

	return totError;
}

// for leave one row out cross validation
// just calculate error that one row left out
- (double)calculateError: (void *)coefficients row: (int)aRow
{
	int j, k;
	double totError = 0.0;
	double value = 0.0;

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)initialDataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)initialResponseMatrix;
	double (*A)[numOfVariables][numOfVariables];
	A = coefficients;

	// sum( (sum(X_i * A_j) - B_ij)^2 )

	// sum(X_i * A_j)
	for (j = 0; j < numOfVariables; ++j) {
		value = 0.0;
		for (k = 0; k < numOfVariables; ++k) {
			value += (*X)[aRow][k] * (*A)[k][j];
			//printf("value % 11.6lf +=  % 11.6lf * % 11.6lf\n", value, (*X)[aRow][k], (*A)[k][j]);
		}
		value -= (*B)[aRow][j];
		//printf("value % 11.6lf -=  % 11.6lf\n", value, (*B)[aRow][j]);
		totError += value * value;
		//printf("totError =  % 11.6lf\n", totError);
	}

	return totError;
}

// for ten fold cross validation
// calculate error leaving out range of rows
- (double)calculateError: (void *)coefficients startRow: (int)startRow endRow: (int)endRow
{
    int j, k;
    double totError = 0.0;
    double value = 0.0;
    int numRow;
    
    // input data
    double (*X)[numOfObservations][numOfVariables];
    X = (void *)initialDataMatrix;
    double (*B)[numOfObservations][numOfVariables];
    B = (void *)initialResponseMatrix;
    double (*A)[numOfVariables][numOfVariables];
    A = coefficients;
    
    // sum( (sum(X_i * A_j) - B_ij)^2 )
    
    for (numRow = startRow; numRow <= endRow; ++numRow) {
        // sum(X_i * A_j)
        for (j = 0; j < numOfVariables; ++j) {
            value = 0.0;
            for (k = 0; k < numOfVariables; ++k) {
                value += (*X)[numRow][k] * (*A)[k][j];
                //printf("value % 11.6lf +=  % 11.6lf * % 11.6lf\n", value, (*X)[aRow][k], (*A)[k][j]);
            }
            value -= (*B)[numRow][j];
            //printf("value % 11.6lf -=  % 11.6lf\n", value, (*B)[aRow][j]);
            totError += value * value;
            //printf("totError =  % 11.6lf\n", totError);
        }
    }
    
    return totError;
}

// for leave one out data point cross validation
// just calculate error for that one data point left out
- (double)calculateError: (void *)coefficients row: (int)aRow column: (int)aColumn
{
	int k;
	double totError = 0.0;

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)initialDataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)initialResponseMatrix;
	double (*A)[numOfVariables][numOfVariables];
	A = coefficients;

	// (sum(X_i * A_j) - B_ij) ^2

	// sum(X_i * A_j)
	for (k = 0; k < numOfVariables; ++k) {
		totError += (*X)[aRow][k] * (*A)[k][aColumn];
	}
	totError -= (*B)[aRow][aColumn];
	totError = totError * totError;

	return totError;
}

#if 0
#pragma mark == OPTIMIZATION METHODS ==
#endif

- (BCDataMatrix *)optimizeWithConstraints: (double)gamma
{
	return [self optimizeWithConstraints: gamma andInitialMatrix: NULL];
}

- (BCDataMatrix *)optimizeWithConstraints: (double)gamma andInitialMatrix: (void *)aMatrix
{
	int i, j, k;

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;
	
	// temporary matrices
	double (*S)[numOfVariables][numOfVariables];
	S = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*Y)[numOfVariables][numOfVariables];
	Y = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*XA1)[numOfObservations][numOfVariables];
	XA1 = malloc(numOfObservations * numOfVariables * sizeof(double));
	
  // result matrix
  BCDataMatrix *rMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
	double (*A)[numOfVariables][numOfVariables];
	A = [rMatrix dataMatrix];
  
    //print_matrix("X = ", numOfObservations, numOfVariables, X);
    //print_matrix("B = ", numOfObservations, numOfVariables, B);

	// S = X' * X
	// correlation matrix
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*S)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*S)[i][j] += (*X)[k][i] * (*X)[k][j];
			}
		}
	}
    print_matrix("S = ", numOfVariables, numOfVariables, S);

	// Y = X' * B
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*Y)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*Y)[i][j] += (*X)[k][i] * (*B)[k][j];
			}
		}
	}
    //print_matrix("Y = ", numOfVariables, numOfVariables, Y);
	
	// initial matrix
	if (aMatrix != NULL) {
		double (*IM)[numOfVariables][numOfVariables];
		IM = aMatrix;
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
				(*A)[i][j] = (*IM)[i][j];
	} else {
		// A = 0
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
				//(*A)[i][j] = (*Y)[i][j];
				(*A)[i][j] = 0.0;
	}
    //print_matrix("A = ", numOfVariables, numOfVariables, A);

	//
	// Start of the main loop
	//
	int iter = 1;
	double F = 0.0, prevF = 0.0;
	
	do {

		// Calculate F
		// F = (X*A - B)^2 + gamma*sum(abs(A))

		// XA1 = X * A
		for (i = 0; i < numOfObservations; ++i) {
			for (j = 0; j < numOfVariables; ++j) {
				(*XA1)[i][j] = 0.0;
				for (k = 0; k < numOfVariables; ++k) {
					(*XA1)[i][j] += (*X)[i][k] * (*A)[k][j];
				}
			}
		}
        //print_matrix("XA1 = ", numOfObservations, numOfVariables, XA1);

		// XA1 = XA1 - B
		for (i = 0; i < numOfObservations; ++i)
			for (j = 0; j < numOfVariables; ++j)
				(*XA1)[i][j] = (*XA1)[i][j] - (*B)[i][j];

		// XA1 = XA1^2
		F = 0.0;
		for (i = 0; i < numOfObservations; ++i)
			for (j = 0; j < numOfVariables; ++j)
				F += (*XA1)[i][j] * (*XA1)[i][j];
		
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
				F += gamma * fabs((*A)[i][j]);

		printf("Iter %d  %lf  %lf\n", iter, F, fabs(F - prevF));

		if ((iter != 1) && (fabs(F - prevF) < EPS)) break;
		if (iter == 2) break;

		++iter;
		prevF = F;
		
		// t = (S(i,i) - diagonalCoefficient)*A(i,j) - S(i,:)*A(:,j) + connectionCoefficent + Y(i,j) + interceptCoefficient

		for (i = 0; i < numOfVariables; ++i) {
			for (j = 0; j < numOfVariables; ++j) {
			
				if ((!selfConnections) && (i == j)) {
					(*A)[i][j] = 0.0;
					continue;
				}

        //if (j == 33) continue;
        //if (j == 124) continue;

				double connectionCoefficient = 0.0;
				double diagonalCoefficient = 0.0;
				double interceptCoefficient = 0.0;
				for (k = 0; k < [dataHandlers count]; ++k) {
					id anObject = [dataHandlers objectAtIndex: k];
					connectionCoefficient += [anObject valueFromDataSource: k forType: BCConnectionCoefficient matrix: A row: i column: j];
					diagonalCoefficient += [anObject valueFromDataSource: k forType: BCDiagonalCoefficient matrix: A row: i column: j];
					interceptCoefficient += [anObject valueFromDataSource: k forType: BCInterceptCoefficient matrix: A row: i column: j];
				}
				double t = ((*S)[i][i] - diagonalCoefficient) * (*A)[i][j];
				//double t = (*S)[i][i] * (*A)[i][j];
				//if ((i == 5) && (j == 33)) printf("%d %d t = %lf %lf %lf\n", i, j, t, (*S)[i][i], (*A)[i][j]);

				for (k = 0; k < numOfVariables; ++k) {
					t -= (*S)[i][k] * (*A)[k][j];
          //if ((i == 5) && (j == 33)) printf("%d %d t = %lf %lf %lf\n", i, j, t, (*S)[i][k], (*A)[k][j]);
				}
				t += (*Y)[i][j];
        //if ((i == 5) && (j == 33)) printf("%d %d t = %lf %lf\n", i, j, t, (*Y)[i][j]);

				t += connectionCoefficient;
				t += interceptCoefficient;
				
        //if ((connectionCoefficient != 0.0) || (diagonalCoefficient != 0.0) || (interceptCoefficient != 0.0) || (j == 33))
          //printf("%d %d %lf %lf %lf %lf %lf %lf\n", i, j, t, (*A)[i][j], connectionCoefficient, (*S)[i][i], diagonalCoefficient, interceptCoefficient);
        
        if ((*S)[i][i] == diagonalCoefficient) {
          // if we do not have any observations then cannot predict
					(*A)[i][j] = 0.0;
					continue;
        }
        
				if (t > gamma)
					(*A)[i][j] = (t - gamma) / ((*S)[i][i] - diagonalCoefficient);
				else if (t < (-1.0 * gamma))
					(*A)[i][j] = (t + gamma) / ((*S)[i][i] - diagonalCoefficient);
				else
					(*A)[i][j] = 0.0;
			}
		}

	} while (1);

    //print_matrix("A = ", numOfVariables, numOfVariables, A);

	free(S);
	free(Y);
	free(XA1);
	
	return rMatrix;
}

- (BCDataMatrix *)optimizeWithConstraintMatrix: (BCDataMatrix *)constraints
{
	return [self optimizeWithConstraintMatrix: constraints andInitialMatrix: NULL];
}

- (BCDataMatrix *)optimizeWithConstraintMatrix: (BCDataMatrix *)constraints andInitialMatrix: (void *)aMatrix
{
	int i, j, k;

	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;

	double (*constraintMatrix)[[constraints numberOfRows]][[constraints numberOfColumns]];
	constraintMatrix = [constraints dataMatrix];

	// temporary matrices
	double (*S)[numOfVariables][numOfVariables];
	S = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*Y)[numOfVariables][numOfVariables];
	Y = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*XA1)[numOfObservations][numOfVariables];
	XA1 = malloc(numOfObservations * numOfVariables * sizeof(double));
	
  // result matrix
  BCDataMatrix *rMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
	double (*A)[numOfVariables][numOfVariables];
	A = [rMatrix dataMatrix];
  
    //print_matrix("X = ", numOfObservations, numOfVariables, X);
    //print_matrix("B = ", numOfObservations, numOfVariables, B);

	// S = X' * X
	// correlation matrix
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*S)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*S)[i][j] += (*X)[k][i] * (*X)[k][j];
			}
		}
	}

    //print_matrix("S = ", numOfVariables, numOfVariables, S);
	
	// Y = X' * B
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*Y)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*Y)[i][j] += (*X)[k][i] * (*B)[k][j];
			}
		}
	}

    //print_matrix("Y = ", numOfVariables, numOfVariables, Y);

	// initial matrix
	if (aMatrix != NULL) {
		double (*IM)[numOfVariables][numOfVariables];
		IM = aMatrix;
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
				(*A)[i][j] = (*IM)[i][j];
	} else {
		// A = Y
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
				(*A)[i][j] = (*Y)[i][j];
	}

    //print_matrix("A = ", numOfVariables, numOfVariables, A);

	//
	// Start of the main loop
	//
	int iter = 1;
	double F = 0.0, prevF = 0.0;
	
	do {

		// Calculate F
		// F = (X*A - B)^2 + gamma*sum(abs(A))

		// XA1 = X * A
		for (i = 0; i < numOfObservations; ++i) {
			for (j = 0; j < numOfVariables; ++j) {
				(*XA1)[i][j] = 0.0;
				for (k = 0; k < numOfVariables; ++k) {
					(*XA1)[i][j] += (*X)[i][k] * (*A)[k][j];
				}
			}
		}

        //print_matrix("XA1 = ", numOfObservations, numOfVariables, XA1);

		// XA1 = XA1 - B
		for (i = 0; i < numOfObservations; ++i)
			for (j = 0; j < numOfVariables; ++j)
				(*XA1)[i][j] = (*XA1)[i][j] - (*B)[i][j];

		// XA1 = XA1^2
		F = 0.0;
		for (i = 0; i < numOfObservations; ++i)
			for (j = 0; j < numOfVariables; ++j)
				F += (*XA1)[i][j] * (*XA1)[i][j];
		
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
				F += (*constraintMatrix)[i][j] * fabs((*A)[i][j]);

		//printf("Iter %d  %lf  %lf\n", iter, F, fabs(F - prevF));

		if ((iter != 1) && (fabs(F - prevF) < EPS)) break;
		//if (iter == 2000) break;

		++iter;
		prevF = F;
		
		// t = (S(i,i) - diagonalCoefficient)*A(i,j) - S(i,:)*A(:,j) + connectionCoefficent + Y(i,j) + interceptCoefficient

		for (i = 0; i < numOfVariables; ++i) {
			for (j = 0; j < numOfVariables; ++j) {
			
				if ((!selfConnections) && (i == j)) {
					(*A)[i][j] = 0.0;
					continue;
				}

				double connectionCoefficient = 0.0;
				double diagonalCoefficient = 0.0;
				double interceptCoefficient = 0.0;
				for (k = 0; k < [dataHandlers count]; ++k) {
					id anObject = [dataHandlers objectAtIndex: k];
					connectionCoefficient += [anObject valueFromDataSource: k forType: BCConnectionCoefficient matrix: A row: i column: j];
					diagonalCoefficient += [anObject valueFromDataSource: k forType: BCDiagonalCoefficient matrix: A row: i column: j];
					interceptCoefficient += [anObject valueFromDataSource: k forType: BCInterceptCoefficient matrix: A row: i column: j];
				}
				//printf("%d %d %lf %lf %lf\n", i, j, connectionCoefficient, diagonalCoefficient, interceptCoefficient);
				
				double t = ((*S)[i][i] - diagonalCoefficient) * (*A)[i][j];
				//double t = (*S)[i][i] * (*A)[i][j];
				
				for (k = 0; k < numOfVariables; ++k) {
					t -= (*S)[i][k] * (*A)[k][j];
				}
				t += (*Y)[i][j];

				t += connectionCoefficient;
				t += interceptCoefficient;
				
        if ((*S)[i][i] == diagonalCoefficient) {
          // if we do not have any observations then cannot predict
					(*A)[i][j] = 0.0;
					continue;
        }

				if (t > (*constraintMatrix)[i][j])
					(*A)[i][j] = (t - (*constraintMatrix)[i][j]) / ((*S)[i][i] - diagonalCoefficient);
				else if (t < (-1.0 * (*constraintMatrix)[i][j]))
					(*A)[i][j] = (t + (*constraintMatrix)[i][j]) / ((*S)[i][i] - diagonalCoefficient);
				else
					(*A)[i][j] = 0.0;
			}
		}

	} while (1);

    //print_matrix("A = ", numOfVariables, numOfVariables, A);

	free(S);
	free(Y);
	free(XA1);
	
	return rMatrix;
}

#if 0
#pragma mark == SINGLE VARIABLE OPTIMIZATION METHODS ==
#endif

- (BCDataMatrix *)optimizeVariable: (int)varIndex withConstraints: (double)gamma
{
	return [self optimizeVariable: varIndex withConstraints: gamma andInitialMatrix: NULL];
}

- (BCDataMatrix *)optimizeVariable: (int)varIndex withConstraints: (double)gamma andInitialMatrix: (void *)aMatrix
{
	int i, j, k;
	
	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;
	
	// temporary matrices
	double (*S)[numOfVariables][numOfVariables];
	S = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*Y)[numOfVariables][numOfVariables];
	Y = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*XA1)[numOfObservations][numOfVariables];
	XA1 = malloc(numOfObservations * numOfVariables * sizeof(double));
	
  // result matrix
  BCDataMatrix *rMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
	double (*A)[numOfVariables][numOfVariables];
	A = [rMatrix dataMatrix];
  
    //print_matrix("X = ", numOfObservations, numOfVariables, X);
    //print_matrix("B = ", numOfObservations, numOfVariables, B);
	
	// S = X' * X
	// correlation matrix
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*S)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*S)[i][j] += (*X)[k][i] * (*X)[k][j];
			}
		}
	}
    //print_matrix("S = ", numOfVariables, numOfVariables, S);
	
	// Y = X' * B
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*Y)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*Y)[i][j] += (*X)[k][i] * (*B)[k][j];
			}
		}
	}
    //print_matrix("Y = ", numOfVariables, numOfVariables, Y);

	// initial matrix
	if (aMatrix != NULL) {
		double (*IM)[numOfVariables][numOfVariables];
		IM = aMatrix;
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
                if (j == varIndex) (*A)[i][j] = (*IM)[i][j];
                else (*A)[i][j] = 0.0;
	} else {
		// A = 0
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
				//(*A)[i][j] = (*Y)[i][j];
				(*A)[i][j] = 0.0;
	}
    //print_matrix("A = ", numOfVariables, numOfVariables, A);

	//
	// Start of the main loop
	//
	int iter = 1;
	double F = 0.0, prevF = 0.0;

	do {
		
		// Calculate F
		// F = (X*A[column] - B[column])^2 + gamma*sum(abs(A[column]))
		
		// XA1 = X * A
		for (i = 0; i < numOfObservations; ++i) {
			for (j = 0; j < numOfVariables; ++j) {
				(*XA1)[i][j] = 0.0;
				for (k = 0; k < numOfVariables; ++k) {
					(*XA1)[i][j] += (*X)[i][k] * (*A)[k][j];
				}
			}
		}
        //print_matrix("XA1 = ", numOfObservations, numOfVariables, XA1);

        j = varIndex;

		// XA1 = XA1 - B
		for (i = 0; i < numOfObservations; ++i)
			(*XA1)[i][j] = (*XA1)[i][j] - (*B)[i][j];
		
		// XA1 = XA1^2
		F = 0.0;
		for (i = 0; i < numOfObservations; ++i)
			F += (*XA1)[i][j] * (*XA1)[i][j];
		
		for (i = 0; i < numOfVariables; ++i)
			F += gamma * fabs((*A)[i][j]);
		
		//printf("Iter %d  %lf  %lf\n", iter, F, fabs(F - prevF));
		
		if ((iter != 1) && (fabs(F - prevF) < (EPS / numOfVariables))) break;
		//if (iter == 2000) break;
		
		++iter;
		prevF = F;
		
		// t = (S(i,i) - diagonalCoefficient)*A(i,j) - S(i,:)*A(:,j) + connectionCoefficent + Y(i,j) + interceptCoefficient
		
		for (i = 0; i < numOfVariables; ++i) {

                if ((!selfConnections) && (i == j)) {
					(*A)[i][j] = 0.0;
					continue;
				}
				
				double connectionCoefficient = 0.0;
				double diagonalCoefficient = 0.0;
				double interceptCoefficient = 0.0;
				for (k = 0; k < [dataHandlers count]; ++k) {
					id anObject = [dataHandlers objectAtIndex: k];
					connectionCoefficient += [anObject valueFromDataSource: k forType: BCConnectionCoefficient matrix: A row: i column: j];
					diagonalCoefficient += [anObject valueFromDataSource: k forType: BCDiagonalCoefficient matrix: A row: i column: j];
					interceptCoefficient += [anObject valueFromDataSource: k forType: BCInterceptCoefficient matrix: A row: i column: j];
				}
				//printf("%d %d %lf %lf %lf\n", i, j, connectionCoefficient, diagonalCoefficient, interceptCoefficient);
				
				double t = ((*S)[i][i] - diagonalCoefficient) * (*A)[i][j];
				//double t = (*S)[i][i] * (*A)[i][j];
				
				for (k = 0; k < numOfVariables; ++k) {
					t -= (*S)[i][k] * (*A)[k][j];
				}
				t += (*Y)[i][j];
				
				t += connectionCoefficient;
				t += interceptCoefficient;
				
      if ((*S)[i][i] == diagonalCoefficient) {
        // if we do not have any observations then cannot predict
        (*A)[i][j] = 0.0;
        continue;
      }

      if (t > gamma)
					(*A)[i][j] = (t - gamma) / ((*S)[i][i] - diagonalCoefficient);
				else if (t < (-1.0 * gamma))
					(*A)[i][j] = (t + gamma) / ((*S)[i][i] - diagonalCoefficient);
				else
					(*A)[i][j] = 0.0;
		}
		
	} while (1);

    //print_matrix("A = ", numOfVariables, numOfVariables, A);
	
	free(S);
	free(Y);
	free(XA1);
	
	return rMatrix;
}

- (BCDataMatrix *)optimizeVariable: (int)varIndex withConstraintMatrix: (BCDataMatrix *)constraints
{
	return [self optimizeVariable: varIndex withConstraintMatrix: constraints andInitialMatrix: NULL];
}

- (BCDataMatrix *)optimizeVariable: (int)varIndex withConstraintMatrix: (BCDataMatrix *)constraints andInitialMatrix: (void *)aMatrix
{
	int i, j, k;
	
	// input data
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
	double (*B)[numOfObservations][numOfVariables];
	B = (void *)responseMatrix;
	
	double (*constraintMatrix)[[constraints numberOfRows]][[constraints numberOfColumns]];
	constraintMatrix = [constraints dataMatrix];
	
	// temporary matrices
	double (*S)[numOfVariables][numOfVariables];
	S = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*Y)[numOfVariables][numOfVariables];
	Y = malloc(numOfVariables * numOfVariables * sizeof(double));
	double (*XA1)[numOfObservations][numOfVariables];
	XA1 = malloc(numOfObservations * numOfVariables * sizeof(double));

  // result matrix
  BCDataMatrix *rMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
	double (*A)[numOfVariables][numOfVariables];
	A = [rMatrix dataMatrix];

    //print_matrix("X = ", numOfObservations, numOfVariables, X);
    //print_matrix("B = ", numOfObservations, numOfVariables, B);
	
	// S = X' * X
	// correlation matrix
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*S)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*S)[i][j] += (*X)[k][i] * (*X)[k][j];
			}
		}
	}
    //print_matrix("S = ", numOfVariables, numOfVariables, S);
	
	// Y = X' * B
	for (i = 0; i < numOfVariables; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
			(*Y)[i][j] = 0.0;
			for (k = 0; k < numOfObservations; ++k) {
				(*Y)[i][j] += (*X)[k][i] * (*B)[k][j];
			}
		}
	}
    //print_matrix("Y = ", numOfVariables, numOfVariables, Y);
	
	// initial matrix
	if (aMatrix != NULL) {
		double (*IM)[numOfVariables][numOfVariables];
		IM = aMatrix;
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
                if (j == varIndex) (*A)[i][j] = (*IM)[i][j];
                else (*A)[i][j] = 0.0;
	} else {
		// A = Y
		for (i = 0; i < numOfVariables; ++i)
			for (j = 0; j < numOfVariables; ++j)
				//(*A)[i][j] = (*Y)[i][j];
        (*A)[i][j] = 0.0;
	}
    //print_matrix("A = ", numOfVariables, numOfVariables, A);

	//
	// Start of the main loop
	//
	int iter = 1;
	double F = 0.0, prevF = 0.0;

	do {
		
		// Calculate F
		// F = (X*A - B)^2 + gamma*sum(abs(A))
		
		// XA1 = X * A
		for (i = 0; i < numOfObservations; ++i) {
			for (j = 0; j < numOfVariables; ++j) {
				(*XA1)[i][j] = 0.0;
				for (k = 0; k < numOfVariables; ++k) {
					(*XA1)[i][j] += (*X)[i][k] * (*A)[k][j];
				}
			}
		}
        //print_matrix("XA1 = ", numOfObservations, numOfVariables, XA1);

        j = varIndex;

        // XA1 = XA1 - B
		for (i = 0; i < numOfObservations; ++i)
			(*XA1)[i][j] = (*XA1)[i][j] - (*B)[i][j];
		
		// XA1 = XA1^2
		F = 0.0;
		for (i = 0; i < numOfObservations; ++i)
			F += (*XA1)[i][j] * (*XA1)[i][j];
		
		for (i = 0; i < numOfVariables; ++i)
			F += (*constraintMatrix)[i][j] * fabs((*A)[i][j]);
        
		//printf("Iter %d  %lf  %lf\n", iter, F, fabs(F - prevF));
		
		if ((iter != 1) && (fabs(F - prevF) < (EPS / numOfVariables))) break;
		//if (iter == 2000) break;
		
		++iter;
		prevF = F;
		
		// t = (S(i,i) - diagonalCoefficient)*A(i,j) - S(i,:)*A(:,j) + connectionCoefficent + Y(i,j) + interceptCoefficient
		
		for (i = 0; i < numOfVariables; ++i) {
				
				if ((!selfConnections) && (i == j)) {
					(*A)[i][j] = 0.0;
					continue;
				}
				
				double connectionCoefficient = 0.0;
				double diagonalCoefficient = 0.0;
				double interceptCoefficient = 0.0;
				for (k = 0; k < [dataHandlers count]; ++k) {
					id anObject = [dataHandlers objectAtIndex: k];
					connectionCoefficient += [anObject valueFromDataSource: k forType: BCConnectionCoefficient matrix: A row: i column: j];
					diagonalCoefficient += [anObject valueFromDataSource: k forType: BCDiagonalCoefficient matrix: A row: i column: j];
					interceptCoefficient += [anObject valueFromDataSource: k forType: BCInterceptCoefficient matrix: A row: i column: j];
				}
				//printf("%d %d %lf %lf %lf\n", i, j, connectionCoefficient, diagonalCoefficient, interceptCoefficient);
				
				double t = ((*S)[i][i] - diagonalCoefficient) * (*A)[i][j];
				//double t = (*S)[i][i] * (*A)[i][j];
				
				for (k = 0; k < numOfVariables; ++k) {
					t -= (*S)[i][k] * (*A)[k][j];
				}
				t += (*Y)[i][j];
				
				t += connectionCoefficient;
				t += interceptCoefficient;
				
        if ((*S)[i][i] == diagonalCoefficient) {
          // if we do not have any observations then cannot predict
          (*A)[i][j] = 0.0;
          continue;
        }

      if (t > (*constraintMatrix)[i][j])
					(*A)[i][j] = (t - (*constraintMatrix)[i][j]) / ((*S)[i][i] - diagonalCoefficient);
				else if (t < (-1.0 * (*constraintMatrix)[i][j]))
					(*A)[i][j] = (t + (*constraintMatrix)[i][j]) / ((*S)[i][i] - diagonalCoefficient);
				else
					(*A)[i][j] = 0.0;

		}
		
	} while (1);

    //print_matrix("A = ", numOfVariables, numOfVariables, A);
	
	free(S);
	free(Y);
	free(XA1);
	
	return rMatrix;
}

- (id)loadBundle
{
  id bundleObject = nil;
  
  //NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"IPOPT.bundle"];
  //printf("%s\n", [bundlePath UTF8String]);
  NSBundle *bundle = [NSBundle bundleForClass: [self class]];
  NSString *bundlePath = [[bundle resourcePath] stringByAppendingPathComponent:@"IPOPT.bundle"];
  printf("%s\n", [bundlePath UTF8String]);
  bundle = [NSBundle bundleWithPath: bundlePath];
  
  if(bundle)
  {
    BOOL res = [bundle load];
    if (res) printf("bundle loaded\n");
    else printf("bundle did not load\n");
    
    Class principalClass = [bundle principalClass];
    printf("%s\n", class_getName(principalClass));
    
    if(principalClass)
    {
      bundleObject = [[principalClass alloc] init];
    }
  }


  return bundleObject;
}

@end



//
// OLD CODE
//

#if 0
- (double)crossValidateWithConstraints: (double)gamma
{
	return [self crossValidateWithConstraints: gamma initialMatrix: NULL finalMatrix: NULL];
}

- (double)crossValidateWithConstraints: (double)gamma initialMatrix: (void *)inMatrix finalMatrix: (void **)outMatrix
{
	// leave-one-out cross validation
	// 1) leave out one cell in data matrix
	// 2) perform calculation
	// 3) use the result to predict the observation
	// 4) calculate error (square difference)
    
	int i, j;
	double totError = 0.0;
	double (*A)[numOfVariables][numOfVariables];
	A = NULL;
	double (*B)[numOfVariables][numOfVariables];
	if (inMatrix) B = inMatrix;
	else B = NULL;
	
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
    
	// for each data matrix cell
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
            
			//printf("%d %d\n", i, j);
            
			if (![self loadCrossValidateDataForRow: i column: j]) continue;
            
			A = [self optimizeWithConstraints: gamma andInitialMatrix: B];
            
			totError += [self calculateError: A];
			//printf("%lf\n", totError);
            
			if ((B) && (B != inMatrix)) free(B);
			B = A;
		}
	}
	
	if (outMatrix) *outMatrix = A;
	else free(A);
    
	return totError;
}

- (double)crossValidateWithConstraintMatrix: (BCDataMatrix *)constraints
{
	return [self crossValidateWithConstraintMatrix: constraints initialMatrix: NULL finalMatrix: NULL];
}

- (double)crossValidateWithConstraintMatrix: (BCDataMatrix *)constraints initialMatrix: (void *)inMatrix finalMatrix: (void **)outMatrix
{
	// leave-one-out cross validation
	// 1) leave out one cell in data matrix
	// 2) perform calculation
	// 3) use the result to predict the observation
	// 4) calculate error (square difference)
    
	int i, j;
	double totError = 0.0;
	double (*A)[numOfVariables][numOfVariables];
	A = NULL;
	double (*B)[numOfVariables][numOfVariables];
	if (inMatrix) B = inMatrix;
	else B = NULL;
	
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
    
	// for each data matrix cell
	for (i = 0; i < numOfObservations; ++i) {
		for (j = 0; j < numOfVariables; ++j) {
            
			//printf("%d %d\n", i, j);
            
			if (![self loadCrossValidateDataForRow: i column: j]) continue;
            
			A = [self optimizeWithConstraintMatrix: constraints andInitialMatrix: B];
            
			totError += [self calculateError: A row: i column: j];
			//totError += [self calculateError: A];
			//printf("%lf\n", totError);
            
			if ((B) && (B != inMatrix)) free(B);
			B = A;
		}
	}
	
	if (outMatrix) *outMatrix = A;
	else free(A);
    
	return totError;
}

- (double)crossValidateRowWithConstraintMatrix: (BCDataMatrix *)constraints initialMatrix: (void *)inMatrix finalMatrix: (void **)outMatrix
{
	// leave-one-out cross validation
	// 1) leave out one row in data matrix
	// 2) perform calculation
	// 3) use the result to predict the observation
	// 4) calculate error (square difference)
    
	int i;
	double totError = 0.0;
	double (*A)[numOfVariables][numOfVariables];
	A = NULL;
	double (*B)[numOfVariables][numOfVariables];
	if (inMatrix) B = inMatrix;
	else B = NULL;
	
	double (*X)[numOfObservations][numOfVariables];
	X = (void *)dataMatrix;
    
	// for each row in the data matrix
	for (i = 0; i < numOfObservations; ++i) {
        
		//printf("%d %d\n", i, j);
		if (![self loadCrossValidateDataForRow: i column: -1]) continue;
        
		A = [self optimizeWithConstraintMatrix: constraints andInitialMatrix: B];
        
		totError += [self calculateError: A row: i];
		printf("%lf\n", totError);
        
		if ((B) && (B != inMatrix)) free(B);
		B = A;
	}
	
	if (outMatrix) *outMatrix = A;
	else free(A);
    
	return totError;
}

- (double)crossValidateWithOptions: (int)anOption constraintMatrix: (BCDataMatrix *)constraints initialMatrix: (void *)inMatrix finalMatrix: (void **)outMatrix
{
	switch (anOption) {
		case 1:
			return [self crossValidateWithConstraintMatrix: constraints initialMatrix: inMatrix finalMatrix: outMatrix];
			break;
		case 2:
			return [self crossValidateRowWithConstraintMatrix: constraints initialMatrix: inMatrix finalMatrix: outMatrix];
			break;
	}
	
	return 0.0;
}
#endif

