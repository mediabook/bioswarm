/*
 BCOptimizationController.h
 
 Copyright (C) 2008-2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: August 2008
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import <Foundation/Foundation.h>
#import <BioCocoa/BCDataMatrix.h>
#import "BCOptimizationDataHandler.h"

typedef enum BCCrossValidateMethod {
    BCCrossValidateLeaveOneOut = 0,
    BCCrossValidateTenFold = 1
} BCCrossValidateMethod;

@interface BCOptimizationController : NSObject {
	id constraintArray;
	id betaConstraintArray;
	BCDataMatrix *constraintMatrix;
  BCDataMatrix *baselineMatrix;
	int currentConstraint;
	NSLock *constraintLock;
	BOOL calcConstraints;
	void **matrixArray;
	BOOL selfConnections;

  BCOptimizationDataHandler *dataModel;
  
  BOOL usePriorMatrix;
  BOOL useBaselineMatrix;
  BOOL keepZeroAlpha;
    BCCrossValidateMethod crossValidateMethod;

	int constraintType;
	double optimalAlpha;
	double optimalBeta;
	double optimalConstraint;
  int constraintExpSteps;
  int currentVariable;

	NSLock *matrixLock;
	int numOfGenes;
	void *finalMatrix;
	id errorArray;
  
  NSOperationQueue *operationQueue;
}

- initWithConstraintFile: (NSString *)filePath;
- initWithConstraintArray: (NSArray *)anArray;
- initWithConstraintArray: (NSArray *)anArray defaultConstraintMatrix: (BCDataMatrix *)aMatrix;
- initWithConstraintArray: (NSArray *)anArray defaultConstraintMatrix: (BCDataMatrix *)aMatrix baselineMatrix: (BCDataMatrix *)bMatrix;

- initWithDataModel: (BCOptimizationDataHandler *)aModel;
- initWithDataModel: (BCOptimizationDataHandler *)aModel baselineMatrix: (BCDataMatrix *)bMatrix;

- (void)setAllowSelfConnections: (BOOL)aFlag;
- (BOOL)allowSelfConnections;

- (void)setUsePriorMatrix: (BOOL)aFlag;
- (BOOL)usePriorMatrix;

- (void)setUseBaselineMatrix: (BOOL)aFlag;
- (BOOL)useBaselineMatrix;

- (void)setKeepZeroAlpha: (BOOL)aFlag;
- (BOOL)keepZeroAlpha;

- (void)setCrossValidateMethod: (BCCrossValidateMethod)aMethod;
- (BCCrossValidateMethod)crossValidateMethod;

- (int)numOfGenes;
- (void)rankEdges;
- (void)outputGraph;
- (void)outputConstraints;
- (void)outputErrors;

- (double)crossValidateOptimizeWithData: (id)aData optimalAlpha: (double *)optA;
- (double)crossValidateMatrixOptimizeWithData: (id)aData optimalAlpha: (double *)optA optimalBeta: (double *)optB;

- (double)crossValidateOptimizeVariable: (int)varIndex withData: (id)aData optimalAlpha: (double *)optA;
- (double)crossValidateMatrixOptimizeVariable: (int)varIndex withData: (id)aData optimalAlpha: (double *)optA optimalBeta: (double *)optB;

- (BCDataMatrix *)doMatrixOptimize;
- (BCDataMatrix *)doVariableOptimize;
- (BCDataMatrix *)doVariableOptimizeFor:(int)varIdx;

- (void)enumerateNetworksForVariable: (int)varIndex withData: (id)aData withInitialNetwork: (BCDataMatrix *)aNetwork;

@end


#import <BioSwarm/BCOptimizationDataHandler.h>

//
// Worker class for doing concurrent operations
//
@interface BCOptimizationWorker : NSObject
{
  BCOptimizationDataHandler *model;
  BCDataMatrix *cMatrix;
  BOOL selfConnections;
  int currentVariable;
  
  BCDataMatrix *resMatrix;
}

- initWithModel: (BCOptimizationDataHandler *)aModel constraintMatrix: (BCDataMatrix *)aMatrix;

- (void)setAllowSelfConnections: (BOOL)aFlag;
- (void)setCurrentVariable: (int)aVar;

- (void)doMatrixOptimize;
- (void)doVariableOptimize;

- (BCDataMatrix *)getResult;

@end

