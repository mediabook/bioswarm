/*
 GLPKOptimization.m
 
 Copyright (C) 2011 Scott Christley
 
 Written by: Scott Christley <schristley@mac.com>
 Created: February 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 
 */

#import "GLPKOptimization.h"


@implementation GLPKOptimization

- init
{
  [super init];
  
  /* Initialize the GLPK environment */
  
  /* Create the problem. */
  lp = glp_create_prob();
  if ( lp == NULL ) {
    fprintf (stderr, "Failed to create LP.\n");
    return nil;
  }
  
  glp_set_prob_name(lp, "BioSwarmLP");
  glp_set_obj_dir(lp, GLP_MAX); /* Problem is maximization */
  
  return self;
}

- (void)dealloc
{
  if (lp) glp_delete_prob(lp);

  [super dealloc];
}

- (BOOL)addColumn: (NSString *)aName lowerBound:(double)lowerBound upperBound:(double)upperBound objective:(double)aVal
{
  char *cname = (char *)[aName UTF8String];
  
  int newCol = glp_add_cols(lp, 1);
  glp_set_col_name(lp, newCol, cname);
  glp_set_col_bnds(lp, newCol, GLP_DB, lowerBound, upperBound);

  if (aVal != 0.0) glp_set_obj_coef(lp, newCol, aVal);

  return YES;
}

- (BOOL)addRow: (NSString *)name rhs:(double)aRHS sense:(char)aSense index: (NSArray *)varIndex values: (NSArray *)varValue
{
  int j;
  int numRules = [varIndex count];
  int ind[numRules+1];
  double val[numRules+1];
  char *rowname = (char *)[name UTF8String];
  
  int newRow = glp_add_rows(lp, 1);
  glp_set_row_name(lp, newRow, rowname);

  // TODO: check sense
  glp_set_row_bnds(lp, newRow, GLP_FX, aRHS, aRHS);

  for (j = 0; j < numRules; ++j) {
    ind[j+1] = [[varIndex objectAtIndex: j] intValue] + 1;
    val[j+1] = [[varValue objectAtIndex: j] doubleValue];
  }

  glp_set_mat_row(lp, newRow, numRules, ind, val);
  
  printf("Added row (%d: %s).\n", newRow, rowname);
  return YES;
}

- (NSArray *)doOptimize
{
  int i;
  
  /* Optimize the problem and obtain solution. */
  glp_simplex(lp, NULL);
  
  /* recover and display results */
  NSMutableArray *res = [NSMutableArray array];
  int numCols = glp_get_num_cols(lp);

  double objval = glp_get_obj_val(lp);
  printf ("Solution value  = %f\n\n", objval);

  for (i = 0; i < numCols; ++i) {
    double x1 = glp_get_col_prim(lp, i+1);
    [res addObject: [NSNumber numberWithDouble: x1]];
  }
  
  [self writeToFile: @"glpkprob.lp"];
  
  return res;
}

- (BOOL)writeToFile: (NSString *)fileName
{
  
  /* Write a copy of the problem to a file. */  
  glp_write_lp(lp, NULL, [fileName UTF8String]);

  return YES;
}

@end
