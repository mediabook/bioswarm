/*
 SigmoidNLP.cpp
 
 Copyright (C) 2011 Scott Christley
 
 Written by: Scott Christley <schristley@mac.com>
 Created: February 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 
 */

#include "SigmoidNLP.h"

#include <math.h>

//extern "C" void print_rmatrix(const char *N, int row, int column, void *M);
//extern "C" void print_mmatrix(const char *N, int row, int column, void *M);

using namespace Ipopt;

/* Constructor. */
SigmoidNLP::SigmoidNLP(int numObs, int numVars, int aVar, void *data, void *response)
{
  numObservations = numObs;
  //numObservations = 1;
  numVariables = numVars;
  currentVariable = aVar;
  dataMatrix = data;
  responseMatrix = response;
  gamma = 10.0;

  d[0] = 1.399541;
  d[1] = 1.494318;
  d[2] = 1.462297;
  d[3] = 0.688414;
  
  b[0] = 0.256666;
  b[1] = 0.202159;
  b[2] = 2.665174;
  b[3] = 0.699511;
  
  I[0] = -1.334871;
  I[1] = 0.100442;
  I[2] = 0.706746;
  I[3] = 0.527738;

  initJ[0] = 0.480637;
  initJ[1] = 0.863538;
  initJ[2] = -1.293063;
  initJ[3] = -1.037244;
  
  deltat = 0.01;
}

SigmoidNLP::~SigmoidNLP()
{}

double SigmoidNLP::sigmoidFunction(const Number* x, int anObs, int aVar)
{
  int k;
	//double (*D)[numObservations][numVariables];
	//double (*R)[numObservations][numVariables];
  
  auto D = (double (*)[numObservations][numVariables])dataMatrix;
  auto R = (double (*)[numObservations][numVariables])responseMatrix;
  auto J = (Number (*)[numVariables])x;

  //
  // sigmoid function for one observation
  // b_j / (1 + exp(-J*D + I)) - d_j * D_i - dD_i/dt
  //
  double val = 0.0;
  for (k = 0; k < numVariables; ++k) {
    Number x2 = (*J)[k];
    val += (*D)[anObs][k] * x2;
    //printf("(%d, %d) %f %f = %f\n", anObs, k, x2, (*D)[anObs][k], val);
  }
  val += I[aVar];
  //printf("%f\n", val);
  val = 1.0 / (1.0 + exp(-val));
  //printf("%f\n", val);
      
  return val;
}

void SigmoidNLP::initialValues(double J1, double J2, double J3, double J4)
{
  initJ[0] = J1;
  initJ[1] = J2;
  initJ[2] = J3;
  initJ[3] = J4;
}

bool SigmoidNLP::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                         Index& nnz_h_lag, IndexStyleEnum& index_style)
{
  //printf("get_nlp_info\n");

  // coefficients for one gene
  n = numVariables;
  
  // one equality constraint,
  //m = 2;
  //m = numObservations;
  m = 0;
  
  // 2 nonzeros in the jacobian (one for x1, and one for x2),
  //nnz_jac_g = 1;
  //nnz_jac_g = 2 * n;
  //nnz_jac_g = numObservations * n;
  nnz_jac_g = 0;
  
  // and 2 nonzeros in the hessian of the lagrangian
  // (one in the hessian of the objective for x2,
  //  and one in the hessian of the constraints for x1)
  //nnz_h_lag = 2;
  
  // We use the standard fortran index style for row/col entries
  index_style = TNLP::C_STYLE;
  
  return true;
}

bool SigmoidNLP::get_bounds_info(Index n, Number* x_l, Number* x_u,
                            Index m, Number* g_l, Number* g_u)
{
  //printf("get_bounds_info\n");
  
  // here, the n and m we gave IPOPT in get_nlp_info are passed back to us.
  // If desired, we could assert to make sure they are what we think they are.
  //assert(n == numVariables);
  assert(n == numVariables);
  //assert(m == numObservations);
  
  //Number (*X_l)[numVariables] = (Number (*)[numVariables])x_l;
  //Number (*X_u)[numVariables] = (Number (*)[numVariables])x_u;
  auto X_l = (Number (*)[numVariables])x_l;
  auto X_u = (Number (*)[numVariables])x_u;
  
  int i;
  for (i = 0; i < numVariables; ++i) {
    (*X_l)[i] = -1.0e19;
    (*X_u)[i] = +1.0e19;
    //if (i == currentVariable) {
    //  (*X_l)[i] = 0;
    //  (*X_u)[i] = 0;
    //}
  }
  
  return true;
}

bool SigmoidNLP::get_starting_point(Index n, bool init_x, Number* x,
                               bool init_z, Number* z_L, Number* z_U,
                               Index m, bool init_lambda,
                               Number* lambda)
{
  //printf("get_starting_point\n");
  
  // Here, we assume we only have starting values for x, if you code
  // your own NLP, you can provide starting values for the others if
  // you wish.
  assert(init_x == true);
  assert(init_z == false);
  assert(init_lambda == false);
  
  //Number (*J)[numVariables] = (Number (*)[numVariables])x;
  auto J = (Number (*)[numVariables])x;
  
  // we initialize x in bounds, in the upper right quadrant
  int i;
  for (i = 0; i < numVariables; ++i) {
    (*J)[i] = 1.0;
    //if (i == currentVariable) (*J)[i] = 0.0;
  }
  
  (*J)[0] = initJ[0];
  (*J)[1] = initJ[1];
  (*J)[2] = initJ[2];
  (*J)[3] = initJ[3];
  
  return true;
}

/* return the objective value */
bool SigmoidNLP::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
{
  //printf("eval_f\n");
  
  int i;
	//double (*D)[numObservations][numVariables];
	//double (*R)[numObservations][numVariables];
  
  //D = (double (*)[numObservations][numVariables])dataMatrix;
  //R = (double (*)[numObservations][numVariables])responseMatrix;
  auto D = (double (*)[numObservations][numVariables])dataMatrix;
  auto R = (double (*)[numObservations][numVariables])responseMatrix;
  
  //Number (*J)[numVariables] = (Number (*)[numVariables])x;
  auto J = (Number (*)[numVariables])x;
  
  // calculate value of the objective function
  obj_value = 0.0;
  for (i = 0; i < numObservations; ++i) {
    double val = b[currentVariable] * sigmoidFunction(x, i, currentVariable);
    //printf("val: %f %f %f\n", val, d[currentVariable]*(*D)[i][currentVariable], ((*D)[i+1][currentVariable] - (*D)[i][currentVariable])/deltat);
    val = val - d[currentVariable]*(*D)[i][currentVariable] - ((*D)[i+1][currentVariable] - (*D)[i][currentVariable])/deltat;
    obj_value += val * val;
    //printf("val, obj = %f %f\n", val, obj_value);
  }
  
  return true;
}

/* return the gradient of the objective */
bool SigmoidNLP::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
{
  //printf("eval_grad_f\n");
  
  int i, j;
	//double (*D)[numObservations][numVariables];
	//double (*R)[numObservations][numVariables];
  
  //D = (double (*)[numObservations][numVariables])dataMatrix;
  //R = (double (*)[numObservations][numVariables])responseMatrix;
  auto D = (double (*)[numObservations][numVariables])dataMatrix;
  auto R = (double (*)[numObservations][numVariables])responseMatrix;
  
  //Number (*J)[numVariables] = (Number (*)[numVariables])x;
  //Number (*F)[numVariables] = (Number (*)[numVariables])grad_f;
  auto F = (Number (*)[numVariables])grad_f;
  
  for (j = 0; j < n; ++j) {
    (*F)[j] = 0.0;
  }
  
  // calculate the gradient values of the objective function
  for (i = 0; i < numObservations; ++i) {
    for (j = 0; j < numVariables; ++j) {
      double val = sigmoidFunction(x, i, currentVariable);
      double val1 = b[currentVariable]*val - d[currentVariable]*(*D)[i][currentVariable] - ((*D)[i+1][currentVariable] - (*D)[i][currentVariable])/deltat;
      //printf("val1 = %f\n", val1);
      (*F)[j] += 2.0 * val1 * b[currentVariable] * val * val * (1.0/val - 1.0) * (*D)[i][j];
      //printf("grad_f[%d] = %f, %f\n", j, (*F)[j], (*D)[i][j]);
    }
  }
  
  return true;
}

/* return the constraint residuals */
bool SigmoidNLP::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
{
  //printf("eval_g\n");

  return true;
}

/* return:
     1) The structure of the jacobian (if "values" is NULL)
     2) The values of the jacobian (if "values" is not NULL)
   for the constraints.
 */
bool SigmoidNLP::eval_jac_g(Index n, const Number* x, bool new_x,
                       Index m, Index nele_jac, Index* iRow, Index *jCol,
                       Number* values)
{
  //printf("eval_jac_g\n");
  
  return true;
}

/* return:
     1) The structure of the hessian of the lagrangian (if "values" is NULL)
     2) The values of the hessian of the lagrangian (if "values" is not NULL)
 */
bool SigmoidNLP::eval_h(Index n, const Number* x, bool new_x,
                   Number obj_factor, Index m, const Number* lambda,
                   bool new_lambda, Index nele_hess, Index* iRow,
                   Index* jCol, Number* values)
{
  //printf("eval_h\n");
  
  return true;
}

void SigmoidNLP::finalize_solution(SolverReturn status,
                              Index n, const Number* x, const Number* z_L, const Number* z_U,
                              Index m, const Number* g, const Number* lambda,
                              Number obj_value,
                              const IpoptData* ip_data,
                              IpoptCalculatedQuantities* ip_cq)
{
  // here is where we would store the solution to variables, or write to a file, etc
  // so we could use the solution. Since the solution is displayed to the console,
  // we currently do nothing here.
  //Number (*J)[numVariables] = (Number (*)[numVariables])x;
  auto J = (Number (*)[numVariables])x;

  int j;
  //printf("Final values.\n");
  //for (j = 0; j < numVariables; ++j) printf("%f ", (*J)[j]);
  //printf("\n");
  //print_rmatrix("W = \n", numVariables, numVariables, (void *)x);
  //print_mmatrix("W = \n", numVariables, numVariables, (void *)x);
}





//
// Old code
//

#if 0

bool MyNLP::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                         Index& nnz_h_lag, IndexStyleEnum& index_style)
{
  // The problem described in MyNLP.hpp has 2 variables, x1, & x2,
  n = 2;
  
  // one equality constraint,
  m = 1;
  
  // 2 nonzeros in the jacobian (one for x1, and one for x2),
  nnz_jac_g = 2;
  
  // and 2 nonzeros in the hessian of the lagrangian
  // (one in the hessian of the objective for x2,
  //  and one in the hessian of the constraints for x1)
  nnz_h_lag = 2;
  
  // We use the standard fortran index style for row/col entries
  index_style = FORTRAN_STYLE;
  
  return true;
}

bool MyNLP::get_bounds_info(Index n, Number* x_l, Number* x_u,
                            Index m, Number* g_l, Number* g_u)
{
  // here, the n and m we gave IPOPT in get_nlp_info are passed back to us.
  // If desired, we could assert to make sure they are what we think they are.
  assert(n == 2);
  assert(m == 1);
  
  // x1 has a lower bound of -1 and an upper bound of 1
  x_l[0] = -1.0;
  x_u[0] = 1.0;
  
  // x2 has no upper or lower bound, so we set them to
  // a large negative and a large positive number.
  // The value that is interpretted as -/+infinity can be
  // set in the options, but it defaults to -/+1e19
  x_l[1] = -1.0e19;
  x_u[1] = +1.0e19;
  
  // we have one equality constraint, so we set the bounds on this constraint
  // to be equal (and zero).
  g_l[0] = g_u[0] = 0.0;
  
  return true;
}

bool MyNLP::get_starting_point(Index n, bool init_x, Number* x,
                               bool init_z, Number* z_L, Number* z_U,
                               Index m, bool init_lambda,
                               Number* lambda)
{
  // Here, we assume we only have starting values for x, if you code
  // your own NLP, you can provide starting values for the others if
  // you wish.
  assert(init_x == true);
  assert(init_z == false);
  assert(init_lambda == false);
  
  // we initialize x in bounds, in the upper right quadrant
  x[0] = 0.5;
  x[1] = 1.5;
  
  return true;
}

bool MyNLP::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
{
  // return the value of the objective function
  Number x2 = x[1];
  obj_value = -(x2 - 2.0) * (x2 - 2.0);
  return true;
}

bool MyNLP::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
{
  // return the gradient of the objective function grad_{x} f(x)
  
  // grad_{x1} f(x): x1 is not in the objective
  grad_f[0] = 0.0;
  
  // grad_{x2} f(x):
  Number x2 = x[1];
  grad_f[1] = -2.0*(x2 - 2.0);
  
  return true;
}

bool MyNLP::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
{
  // return the value of the constraints: g(x)
  Number x1 = x[0];
  Number x2 = x[1];
  
  g[0] = -(x1*x1 + x2 - 1.0);
  
  return true;
}

bool MyNLP::eval_jac_g(Index n, const Number* x, bool new_x,
                       Index m, Index nele_jac, Index* iRow, Index *jCol,
                       Number* values)
{
  if (values == NULL) {
    // return the structure of the jacobian of the constraints
    
    // element at 1,1: grad_{x1} g_{1}(x)
    iRow[0] = 1;
    jCol[0] = 1;
    
    // element at 1,2: grad_{x2} g_{1}(x)
    iRow[1] = 1;
    jCol[1] = 2;
  }
  else {
    // return the values of the jacobian of the constraints
    Number x1 = x[0];
    
    // element at 1,1: grad_{x1} g_{1}(x)
    values[0] = -2.0 * x1;
    
    // element at 1,2: grad_{x1} g_{1}(x)
    values[1] = -1.0;
  }
  
  return true;
}

bool MyNLP::eval_h(Index n, const Number* x, bool new_x,
                   Number obj_factor, Index m, const Number* lambda,
                   bool new_lambda, Index nele_hess, Index* iRow,
                   Index* jCol, Number* values)
{
  if (values == NULL) {
    // return the structure. This is a symmetric matrix, fill the lower left
    // triangle only.
    
    // element at 1,1: grad^2_{x1,x1} L(x,lambda)
    iRow[0] = 1;
    jCol[0] = 1;
    
    // element at 2,2: grad^2_{x2,x2} L(x,lambda)
    iRow[1] = 2;
    jCol[1] = 2;
    
    // Note: off-diagonal elements are zero for this problem
  }
  else {
    // return the values
    
    // element at 1,1: grad^2_{x1,x1} L(x,lambda)
    values[0] = -2.0 * lambda[0];
    
    // element at 2,2: grad^2_{x2,x2} L(x,lambda)
    values[1] = -2.0 * obj_factor;
    
    // Note: off-diagonal elements are zero for this problem
  }
  
  return true;
}

void MyNLP::finalize_solution(SolverReturn status,
                              Index n, const Number* x, const Number* z_L, const Number* z_U,
                              Index m, const Number* g, const Number* lambda,
                              Number obj_value,
                              const IpoptData* ip_data,
                              IpoptCalculatedQuantities* ip_cq)
{
  // here is where we would store the solution to variables, or write to a file, etc
  // so we could use the solution. Since the solution is displayed to the console,
  // we currently do nothing here.
}

#endif
