/*
 IPOptimization.mm
 
 Copyright (C) 2011 Scott Christley
 
 Written by: Scott Christley <schristley@mac.com>
 Created: February 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 
 */

#import "IPOptimization.h"
#include "SigmoidNLP.h"

#include <coin/IpIpoptApplication.hpp>
#include <coin/IpSolveStatistics.hpp>


@implementation IPOptimization

- (int)performOptimizeWithDataModel: (BCOptimizationDataHandler *)aModel
{
  // TODO: C++ in SigmoidNLP needs to be made portable
#if 0
  BCDataMatrix *rm = [aModel dataMatrix];
  void *dataMatrix = [rm dataMatrix];
	BCDataMatrix *rr = [aModel responseMatrix];
  void *responseMatrix = [rr dataMatrix];

  // substract at out last observation because doing dx/dt approximation
  int numObservations = [rm numberOfRows] - 1;
  //int numObservations = 1;
	int numVariables = [rm numberOfColumns];
	printf("%d observations\n", numObservations);
	printf("%d variables\n", numVariables);
  
  double initJ[4];
  initJ[0] = 0.480637;
  initJ[1] = 0.863538;
  initJ[2] = -1.293063;
  initJ[3] = -1.037244;
  
  // Create an instance of your nlp...
  SigmoidNLP *theNLP = new SigmoidNLP(numObservations, numVariables, 0, dataMatrix, responseMatrix);
  SmartPtr<TNLP> mynlp = theNLP;
  
  // Create an instance of the IpoptApplication
  //
  // We are using the factory, since this allows us to compile this
  // example with an Ipopt Windows DLL
  SmartPtr<IpoptApplication> app = IpoptApplicationFactory();
  
  // Change some options
  // Note: The following choices are only examples, they might not be
  //       suitable for your optimization problem.
  //app->Options()->SetNumericValue("tol", 1e-7);
  app->Options()->SetStringValue("hessian_approximation", "limited-memory");
  app->Options()->SetStringValue("derivative_test", "first-order");
  app->Options()->SetIntegerValue("print_level", 0);
  app->Options()->SetStringValue("cb", "yes");
  //app->Options()->SetStringValue("print_options_documentation", "yes");
  //app->Options()->SetStringValue("print_user_options", "yes");
  // The following overwrites the default name (ipopt.opt) of the
  // options file
  // app->Options()->SetStringValue("option_file_name", "hs071.opt");

  
  // Initialize the IpoptApplication and process the options
  ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Solve_Succeeded) {
    printf("\n\n*** Error during initialization!\n");
    return (int) status;
  }
  
  status = app->OptimizeTNLP(mynlp);
#if 0
  
  if (status == Solve_Succeeded) {
    // Retrieve some statistics about the solve
    Index iter_count = app->Statistics()->IterationCount();
    printf("\n\n*** The problem solved in %d iterations!\n", iter_count);
    
    Number final_obj = app->Statistics()->FinalObjective();
    printf("\n\n*** The final value of the objective function is %e.\n", final_obj);
  }

  initJ[0] = 0.480637 + 0.1;
  initJ[1] = 0.863538 + 0.1;
  initJ[2] = -1.293063 + 2;
  initJ[3] = -1.037244 + 0.1;
  
  theNLP->initialValues(initJ[0], initJ[1], initJ[2], initJ[3]);
  status = app->OptimizeTNLP(mynlp);
  return (int) status;
#endif
#endif
  return 0;
}

@end
