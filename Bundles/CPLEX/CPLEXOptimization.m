/*
 CPLEXOptimization.m
 
 Copyright (C) 2011 Scott Christley
 
 Written by: Scott Christley <schristley@mac.com>
 Created: February 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 
 */

#import "CPLEXOptimization.h"

static NSString *licenseFile = @"ILOG_LICENSE_FILE=/Users/scottc/Projects/tools/ILOG/CPLEX_Studio_AcademicResearch122/licenses/access.ilm";

@implementation CPLEXOptimization

+ (void)setLicense: (NSString *)aStr
{
  licenseFile = [aStr retain];
}

- init
{
  [super init];
  
  int status = 0;
  
  /* Initialize the CPLEX environment */
  CPXputenv ([licenseFile UTF8String]);
  env = CPXopenCPLEX (&status);
  if ( env == NULL ) {
    char  errmsg[1024];
    fprintf (stderr, "Could not open CPLEX environment.\n");
    CPXgeterrorstring (env, status, errmsg);
    fprintf (stderr, "%s", errmsg);
    return nil;
  }
  
  /* Turn on output to the screen */
  status = CPXsetintparam (env, CPX_PARAM_SCRIND, CPX_ON);
  if ( status ) {
    fprintf (stderr, "Failure to turn on screen indicator, error %d.\n", status);
    return nil;
  }
  
  /* Turn on data checking */
  status = CPXsetintparam (env, CPX_PARAM_DATACHECK, CPX_ON);
  if ( status ) {
    fprintf (stderr, "Failure to turn on data checking, error %d.\n", status);
    return nil;
  }
  
  /* Create the problem. */
  lp = CPXcreateprob (env, &status, "lpex1");
  if ( lp == NULL ) {
    fprintf (stderr, "Failed to create LP.\n");
    return nil;
  }
  
  CPXchgprobtype(env, lp, CPXPROB_LP);
  CPXchgobjsen (env, lp, CPX_MAX);  /* Problem is maximization */

  return self;
}

- (BOOL)addColumn: (NSString *)aName lowerBound:(double)lowerBound upperBound:(double)upperBound objective:(double)aVal
{
  int status = 0;
  double lb[1];
  double ub[1];
  double obj[1];
  char ctype[1];
  char *cname[1];

  lb[0] = lowerBound;
  ub[0] = upperBound;
  obj[0] = aVal;
  ctype[0] = 'C';
  cname[0] = (char *)[aName UTF8String];
  
  if (aVal != 0.0)
    status = CPXnewcols(env, lp, 1, obj, lb, ub, ctype, cname);
  else
    status = CPXnewcols(env, lp, 1, NULL, lb, ub, ctype, cname);
  if ( status ) {
    fprintf (stderr, "Failure to add new column, error %d.\n", status);
    return NO;
  }
  
  return YES;
}

- (BOOL)addRow: (NSString *)name rhs:(double)aRHS sense:(char)aSense index: (NSArray *)varIndex values: (NSArray *)varValue
{
  int status = 0;
  int j;
  int numRules = [varIndex count];
  double rhs[1];
  char sense[1];
  int rmatbeg[1];
  int rmatind[numRules];
  double rmatval[numRules];
  char *rowname[1];
  
  rhs[0] = aRHS;
  sense[0] = aSense;
  rmatbeg[0] = 0;
  rowname[0] = (char *)[name UTF8String];
  for (j = 0; j < numRules; ++j) {
    rmatind[j] = [[varIndex objectAtIndex: j] intValue];
    rmatval[j] = [[varValue objectAtIndex: j] doubleValue];
  }
  
  status = CPXaddrows(env, lp, 0, 1, numRules, rhs, sense, rmatbeg, rmatind, rmatval, NULL, rowname);
  if ( status ) {
    fprintf (stderr, "Failure to add new row (%s), error %d.\n", rowname[0], status);
    return NO;
  }
  printf("Added row (%s).\n", rowname[0]);
  return YES;
}

- (NSArray *)doOptimize
{
  int status = 0;

  CPXchgprobtype(env, lp, CPXPROB_LP);
  CPXchgobjsen (env, lp, CPX_MAX);  /* Problem is maximization */
  
  /* Optimize the problem and obtain solution. */
  
  status = CPXlpopt (env, lp);
  if ( status ) {
    fprintf (stderr, "Failed to optimize LP.\n");
    return nil;
  }
  
  /* The size of the problem should be obtained by asking CPLEX what
   the actual size is, rather than using sizes from when the problem
   was built.  cur_numrows and cur_numcols store the current number 
   of rows and columns, respectively.  */
  
  int cur_numrows = CPXgetnumrows (env, lp);
  int cur_numcols = CPXgetnumcols (env, lp);
  
  double *x = (double *) malloc (cur_numcols * sizeof(double));
  double *slack = (double *) malloc (cur_numrows * sizeof(double));
  double *dj = (double *) malloc (cur_numcols * sizeof(double));
  double *pi = (double *) malloc (cur_numrows * sizeof(double));
  int solstat;
  double objval;
  
  if ( x     == NULL ||
      slack == NULL ||
      dj    == NULL ||
      pi    == NULL   ) {
    status = CPXERR_NO_MEMORY;
    fprintf (stderr, "Could not allocate memory for solution.\n");
    return nil;
  }
  
  NSMutableArray *res = nil;
  status = CPXsolution (env, lp, &solstat, &objval, x, pi, slack, dj);
  if ( status ) {
    fprintf (stderr, "Failed to obtain solution.\n");
  } else {
    /* Write the output to the screen. */
    printf ("\nSolution status = %d\n", solstat);
    printf ("Solution value  = %f\n\n", objval);
  
    int i;
    res = [NSMutableArray array];
    for (i = 0; i < cur_numcols; ++i) {
      [res addObject: [NSNumber numberWithDouble: x[i]]];
    }
  }

  [self writeToFile: @"prob.lp"];
  
  free(x);
  free(slack);
  free(dj);
  free(pi);
  
  return res;
}

- (BOOL)writeToFile: (NSString *)fileName
{
  int status = 0;

  /* Write a copy of the problem to a file. */  
  status = CPXwriteprob (env, lp, [fileName UTF8String], NULL);
  if ( status ) {
    fprintf (stderr, "Failed to write LP to disk.\n");
    return NO;
  }
  
  return YES;
}

@end
