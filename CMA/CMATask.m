/*
 CMATask.m
 
 Copyright (C) 2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: July 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "CMATask.h"

@implementation CMATask

- init
{
  [super init];

  cmaTask = [[NSTask alloc] init];

  // make pipes & hook them up
  toPipe = [NSPipe pipe];
  fromPipe = [NSPipe pipe];
  errorPipe = [NSPipe pipe];
  writing = [toPipe fileHandleForWriting];
  reading = [fromPipe fileHandleForReading];
  readingError = [errorPipe fileHandleForReading];
  [cmaTask setStandardInput:toPipe];
  [cmaTask setStandardOutput:fromPipe];
  [cmaTask setStandardError:errorPipe];

  receivingOutput = YES;
  currentOutput = nil;
  receivingError = NO;
  currentError = nil;

  // set environment
  //NSDictionary *env = [NSDictionary dictionaryWithObjectsAndKeys:@"   ", @"XMLLINT_INDENT", nil];
  //[aTask setEnvironment:env];
    
  // set arguments
  NSMutableArray *args = [NSMutableArray array];
  [args addObject:@"-no-wrap"];
  [cmaTask setArguments:args];

  // Here we register as an observer of the NSFileHandleReadCompletionNotification, which lets
  // us know when there is data waiting for us to grab it in the task's file handle (the pipe
  // to which we connected stdout and stderr above).  -getData: will be called when there
  // is data waiting.  The reason we need to do this is because if the file handle gets
  // filled up, the task will block waiting to send data and we'll never get anywhere.
  // So we have to keep reading data from the file handle as we go.
  [[NSNotificationCenter defaultCenter] addObserver:self 
					selector:@selector(getData:) 
					name: NSFileHandleReadCompletionNotification 
					object: [[cmaTask standardOutput] fileHandleForReading]];
  // We tell the file handle to go ahead and read in the background asynchronously, and notify
  // us via the callback registered above when we signed up as an observer.  The file handle will
  // send a NSFileHandleReadCompletionNotification when it has data that is available.
  [[[cmaTask standardOutput] fileHandleForReading] readInBackgroundAndNotify];

  [[NSNotificationCenter defaultCenter] addObserver:self 
					selector:@selector(getErrorData:) 
					name: NSFileHandleReadCompletionNotification 
					object: [[cmaTask standardError] fileHandleForReading]];
  [[[cmaTask standardError] fileHandleForReading] readInBackgroundAndNotify];
    
  // get initial output
  //NSData *dataOut = [reading readDataToEndOfFile];
  //NSData *dataOut = [reading availableData];
  //printf("%s\n", [dataOut bytes]);

  return self;
}

- (void)dealloc
{
  // clean up task object
  [cmaTask terminate];
  [cmaTask release];

  [super dealloc];
}

// sends a generic command string
- (NSString *)sendCommand: (NSString *)aCommand
{
  // clear out old output
  // and get ready to receive response
  [currentOutput release];
  receivingOutput = YES;
  currentOutput = nil;
  receivingError = NO;
  currentError = nil;

  // send command
  NSData *aData = [NSData dataWithBytes: [aCommand UTF8String] length: [aCommand lengthOfBytesUsingEncoding: NSUTF8StringEncoding]];
  [writing writeData: aData];

  // wait and get all the output response from maude
  // TODO: We should add timeout to this runloop
  // and handle when we do not get proper termination
  while (receivingOutput || receivingError) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }

  // if there was an error, check that we get all of the error description
  if (currentError) {
    do {
      [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow: 1.0]];
    } while (receivingError);
  }

  return currentOutput;
}

- (NSString *)commandResponse { return currentOutput; }
- (NSString *)commandError { return currentError; }

- (NSString *)doReduce: (NSString *)aCommand
{
  NSMutableString *s = [NSMutableString stringWithString: @"reduce "];
  [s appendString: aCommand];
  [s appendString: @" .\n"];

  [self sendCommand: s];

  if (currentError) return nil;

  // extract result
  NSArray *a = [currentOutput componentsSeparatedByString: @"\n"];
  NSString *result = nil;
  int i;
  for (i = 0; i < [a count]; ++i) {
    NSString *s = [a objectAtIndex: i];
    NSRange r = [s rangeOfString: @"result "];
    if (r.location == NSNotFound) continue;
    if (r.location != 0) continue;

    r = [s rangeOfString: @": "];
    r.length = r.location - 7;
    r.location = 7;
    result = [s substringWithRange: r];
    break;
  }

  return result;
}

- (void)doRewrite: (NSString *)aCommand inModule: (NSString *)moduleName
{
  NSMutableString *s = [NSMutableString stringWithString: @"rewrite "];
  if (moduleName) {
    [s appendString: @"in "];
    [s appendString: moduleName];
    [s appendString: @" : "];
  }
  [s appendString: aCommand];
  [s appendString: @" .\n"];

  [self sendCommand: s];

  // extract result

}

- (void)doRewrite: (NSString *)aCommand
{
  [self doRewrite: aCommand inModule: nil];
}

- (void)doSearch: (NSString *)aCommand
{
}

- (void)startCMA
{
  // launch maude
  [cmaTask setLaunchPath:@"/Users/scottc/Projects/local/maude64bit/bin/maude"];
  [cmaTask launch];

  // wait and get all the output response from maude
  while (receivingOutput) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }

  [self sendCommand: @"load ontology\n"];
  [self sendCommand: @"load go_sorts\n"];
  [self sendCommand: @"load go_subsorts\n"];
  [self sendCommand: @"load biomodel\n"];
}

- (NSData *)produceSpecFromModel: (NSData *)bioModel
{
  // grab text of doc and write it
  [writing writeData:bioModel];
    
  // send EOF
  //[writing closeFile];
    
  // grab returned data and save it
  // NOTE: this is not the right way to do this in general, but may work okay in this context.
  // Basically, we are assuming that we don't need to read any output until all the input has
  // been written. Since, as far as I recall, pipes can only contain a relatively small amount
  // of data, this means we are assuming that no substantial output is done by the child process
  // until all the input data has been read. (Otherwise, the child process could block trying to
  // write output to a full pipe that no one is reading.) It is probably the case that 'xmllint'
  // reads all its input first, due to the structure of XML, but this would not be true in general.
  NSData *dataOut = [reading readDataToEndOfFile];
    
  return dataOut;
}

// This method is called asynchronously when data is available from the task's file handle.
// We just pass the data along to the controller as an NSString.
- (void) getData: (NSNotification *)aNotification
{
  NSData *data = [[aNotification userInfo] objectForKey:NSFileHandleNotificationDataItem];
  // If the length of the data is zero, then the task is basically over - there is nothing
  // more to get from the handle so we may as well shut down.
  if ([data length])
    {
      // Send the data on to the controller; we can't just use +stringWithUTF8String: here
      // because -[data bytes] is not necessarily a properly terminated string.
      // -initWithData:encoding: on the other hand checks -[data length]
      //[controller appendOutput: [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]];
      //printf("%s\n", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
      if (!currentOutput) currentOutput = [NSMutableString new];
      [currentOutput appendString: [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    } else {
    // got all the output
    printf("zero length\n");
    receivingOutput = NO;
  }

  // check if we got the maude prompt
  NSRange r = [currentOutput rangeOfString: @"Maude>"];
  if (r.location != NSNotFound) {
    //printf("%d %d\n", r.location, r.length);
    if (r.location == 0) currentOutput = [NSMutableString new];
    else {
      r.length = r.location - 1;
      r.location = 0;
      currentOutput = [[NSMutableString alloc] initWithString: [currentOutput substringWithRange: r]];
      //printf("%s\n", [currentOutput UTF8String]);
      //printf("%s\n", [[currentOutput substringWithRange: r] UTF8String]);
    }
    receivingOutput = NO;
  } else {
  }

  // we need to schedule the file handle go read more data in the background again.
  [[aNotification object] readInBackgroundAndNotify];  
}

// This method is called asynchronously when data is available from the task's file handle.
// We just pass the data along to the controller as an NSString.
- (void) getErrorData: (NSNotification *)aNotification
{
  //printf("getErrorData:\n");
  receivingError = YES;

  NSData *data = [[aNotification userInfo] objectForKey:NSFileHandleNotificationDataItem];
  // If the length of the data is zero, then the task is basically over - there is nothing
  // more to get from the handle so we may as well shut down.
  if ([data length])
    {
      if (!currentError) currentError = [NSMutableString new];
      [currentError appendString: [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    } else {
    // got all the output
    printf("zero length error\n");
    receivingError = NO;
  }

  // no real good way to determine if we got all output
  // so do a simple check to see if last character was a newline
  char c = [currentError characterAtIndex: [currentError length] - 1];
  if (c == '\n') {
    //printf("%s", [currentError UTF8String]);
    receivingError = NO;
  } else {
  }

  // we need to schedule the file handle go read more data in the background again.
  [[aNotification object] readInBackgroundAndNotify];  
}

@end
