/*
 CMA.m
 
 Copyright (C) 2011 Scott Christley
 
 Author: Scott Christley <schristley@mac.com>
 Date: July 2011
 
 This file is part of the BioSwarm Framework.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02111 USA.
 */

#import "CMA.h"


@implementation CMA

- init
{
  [super init];
  
  cmaTask = nil;
  resultSort = nil;
  resultStatement = nil;
  resultSpec = nil;
  
  return self;
}

- (BOOL)setupCMATask
{
  if (!cmaTask) {
    printf("Starting CMA.\n");
    cmaTask = [CMATask new];
    [cmaTask startCMA];
  }
  
  return YES;
}

- (NSMutableString *)maudeCodeForEntities: (NSDictionary *)entities
{
  int i;
  
  NSMutableString *module = [NSMutableString new];
  NSArray *allKeys = [entities allKeys];
  for (i = 0; i < [allKeys count]; ++i) {
    NSString *key = [allKeys objectAtIndex: i];
    [module appendFormat: @"    op %@ : -> \"%@\" .\n", key, [entities objectForKey: key]];
  }
  
  return module;
}

- (BOOL)sendRewrite: (NSString *)aCommand inModule: (NSString *)moduleName
{
  int i, j;
  NSString *e;
  
  resultSort = nil;
  resultStatement = nil;
  resultSpec = nil;
  
  [cmaTask doRewrite: aCommand inModule: moduleName];
  
  e = [cmaTask commandError];
  if (e) {
    printf("ERROR: problem with constructing model specification.\n");
    printf("%s", [e UTF8String]);
    return NO;
  }
  NSString *res = [cmaTask commandResponse];
  //printf("%s\n", [res UTF8String]);
  
  // extract result
  NSArray *a = [res componentsSeparatedByString: @"\n"];
  for (i = 0; i < [a count]; ++i) {
    NSString *s = [a objectAtIndex: i];
    NSRange r = [s rangeOfString: @"result "];
    if (r.location == NSNotFound) continue;
    if (r.location != 0) continue;
    
    r = [s rangeOfString: @": "];
    r.length = r.location - 7;
    r.location = 7;
    resultSort = [s substringWithRange: r];
    
    r.location += r.length + 2;
    resultStatement = [s substringFromIndex: r.location];
    
    NSError *err = nil;
    resultSpec = [[NSXMLDocument alloc] initWithXMLString: resultStatement options: 0 error: &err];
    if (err) {
      printf("ERROR: %s\n", [[err localizedDescription] UTF8String]);
      return NO;
    }
    
    // pull out bioKR and modelSpec from xml
    NSXMLNode *spec = [resultSpec childAtIndex: 0];
    for (j = 0; j < [spec childCount]; ++j) {
      NSXMLNode *c = [spec childAtIndex: j];
      //printf("c = %s\n", [[c name] UTF8String]);
      if ([[c name] isEqualToString: @"bioKR"]) {
        bioKR = [[c stringValue] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
      }
      if ([[c name] isEqualToString: @"modelSpecification"]) {
        modelSpec = c;
      }
    }
    
    break;
  }
  
  //printf("resultSort = %s.\n", [resultSort UTF8String]);
  //printf("resultStatement = %s.\n", [resultStatement UTF8String]);
  //printf("bioKR = %s.\n", [bioKR UTF8String]);
  //printf("modelSpec = %s.\n", [modelSpec UTF8String]);
  
  return YES;
}

- (BOOL)checkModel: (NSDictionary *)aModel
{
  int i;
  
  if (![self setupCMATask]) return NO;
  
  NSMutableString *module = [NSMutableString new];
  [module appendString: @"mod TEST_MODEL is\n\n"];
  [module appendString: @"    including BIOLOGY_KR .\n"];
  [module appendString: @"    including MAPPING_ODE .\n"];
  //[module appendString: @"    including MAPPING_PETRI_NET .\n"];
  //[module appendString: @"    including MAPPING_ABM .\n"];
  [module appendString: @"\n"];
  
#if 0
  NSDictionary *entities = [aModel objectForKey: @"entities"];
  NSArray *allKeys = [entities allKeys];
  for (i = 0; i < [allKeys count]; ++i) {
    NSString *key = [allKeys objectAtIndex: i];
    [module appendFormat: @"    op %@ : -> \"%@\" .\n", key, [entities objectForKey: key]];
  }
#endif
  [module appendString: [self maudeCodeForEntities: [aModel objectForKey: @"entities"]]];
  
  [module appendString: @"endm\n"];
  printf("%s\n", [module UTF8String]);
  
  [cmaTask sendCommand: module];
  NSString *e = [cmaTask commandError];
  if (e) {
    printf("ERROR: problem with entities.\n");
    printf("%s", [e UTF8String]);
  }
  
  NSArray *statements = [aModel objectForKey: @"statements"];
  for (i = 0; i < [statements count]; ++i) {
    NSString *st = [statements objectAtIndex: i];
    NSString *s = [cmaTask doReduce: st];
    if (s) {
      if ([s isEqualToString: @"statement"]) {
        printf("%s\n", [st UTF8String]);
        printf("is a valid %s.\n", [s UTF8String]);
      } else {
        printf("%s\n", [st UTF8String]);
        printf("is not a valid statement, parsed as a %s\n", [s UTF8String]);
      }
    } else {
      printf("ERROR: incorrect statement: %s\n", [st UTF8String]);
      NSString *e = [cmaTask commandError];
      printf("%s", [e UTF8String]);
    }
    printf("=====\n");
  }
  
  return YES;
}

- (NSXMLDocument *)generateModelSpecification: (NSDictionary *)aModel
{
  int i, j;
  
  if (![self setupCMATask]) return NO;
  
  NSMutableString *moduleHead = [NSMutableString new];
  [moduleHead appendString: @"mod TEST_MODEL is\n\n"];
  [moduleHead appendString: @"    including BIOLOGY_KR .\n"];
  [moduleHead appendString: @"    including MAPPING_ODE .\n"];
  //[module appendString: @"    including MAPPING_PETRI_NET .\n"];
  //[module appendString: @"    including MAPPING_ABM .\n"];
  [moduleHead appendString: @"\n"];
  
  [moduleHead appendString: [self maudeCodeForEntities: [aModel objectForKey: @"entities"]]];
  
  [moduleHead appendString: @"\n"];
  
  NSMutableString *krString = [NSMutableString new];
  [krString appendString: @"    op testKR : -> statementSet .\n"];
  [krString appendString: @"    eq testKR =\n"];
  
  NSArray *statements = [aModel objectForKey: @"statements"];
  for (i = 0; i < [statements count]; ++i) {
    NSString *st = [statements objectAtIndex: i];
    [krString appendFormat: @"        %@\n", st];
  }
  [krString appendString: @"    .\n"];
  
  NSMutableString *moduleFoot = [NSMutableString new];
  [moduleFoot appendString: @"    op testModel : -> specification .\n"];
  [moduleFoot appendString: @"    eq testModel = <spec> <bioKR> testKR </bioKR> MS(emptyModelSpec) </spec> .\n"];
  
  [moduleFoot appendString: @"\nendm\n"];
  
  NSMutableString *module = [NSMutableString new];
  [module appendString: moduleHead];
  [module appendString: krString];
  [module appendString: moduleFoot];
  //printf("%s\n", [module UTF8String]);
  
  [cmaTask sendCommand: module];
  NSString *e = [cmaTask commandError];
  if (e) {
    printf("ERROR: problem with model.\n");
    printf("%s", [e UTF8String]);
    return nil;
  }
  
  [self sendRewrite: @"testModel" inModule: nil];
  
  // if kind instead of sort then some statements did not rewrite
  // so test individual statements
  if (([resultSort characterAtIndex: 0] == '[') || (![bioKR isEqualToString: @"emptyStatement"])) {
    printf("Could not translate complete model, checking each statement individually.\n");
    printf("Untranslated bioKR = %s\n", [bioKR UTF8String]);
    
    NSArray *statements = [aModel objectForKey: @"statements"];
    for (i = 0; i < [statements count]; ++i) {
      NSString *st = [statements objectAtIndex: i];
      
      printf("%s ... ", [st UTF8String]);
      module = [NSMutableString new];
      [module appendString: moduleHead];
      
      [module appendString: @"    op testKR : -> statementSet .\n"];
      [module appendString: @"    eq testKR =\n"];
      
      [module appendFormat: @"        %@\n", st];
      [module appendString: @"    .\n"];
      
      [module appendString: moduleFoot];
      
      [cmaTask sendCommand: module];
      [self sendRewrite: @"testModel" inModule: nil];
      
      // check the xml
      if ([bioKR isEqualToString: @"emptyStatement"])
        printf("ok\n");
      else
        printf("no model specification\n");
    }
    
    return nil;
  }
  
  return resultSpec;
}

@end

